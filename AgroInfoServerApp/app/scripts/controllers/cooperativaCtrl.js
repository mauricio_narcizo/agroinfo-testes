'use strict';
(function () {
  var CooperativaCtrl = function (AutenticacaoService, AppService, NotificacaoService, Restangular, PostService,$state) {
    var vm = this;


    vm.sortType = 'id'; // set the default sort type
    vm.sortReverse = false;  // set the default sort order
    vm.searchFish = '';     // set the default search/filter term

    var restcoop = Restangular.all('cooperativa')

    vm.refresh = function () {
      restcoop.getList().then(function (results) {
        vm.Cooperativa = results;
      });
    };

    vm.reset = function () {
      vm.newCooperativa = {};
    };

    vm.register = function () {
      vm.successMessages = '';
      vm.errorMessages = '';
      vm.errors = {};
      vm.promes = PostService.cooperativa(vm.newCooperativa)
        .success(function(data) {
          NotificacaoService.showInfoMessage("Cooperativa cadastrada!")
          vm.refresh();
          vm.reset();
          $state.go('cooperativas');
        })
        .error(function(data, status) {
          vm.errors = data;
          NotificacaoService.showErrorMessage("Erro Ao cadastrar")
        });
     };


    vm.refresh();
    vm.reset();

    // Set the default orderBy to the name property
    vm.orderBy = 'name';
  }





  CooperativaCtrl.$inject = ['AutenticacaoService', 'AppService', 'NotificacaoService', 'Restangular', 'PostService','$state'];
  angular.module('agroinfoServer.cooperativas').controller("CooperativaCtrl", CooperativaCtrl);

} ());  
