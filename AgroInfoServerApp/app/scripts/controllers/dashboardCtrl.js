'use strict';
(function () {

    var controller = function ($state, $scope, AutenticacaoService, AppService, NotificacaoService, $window) {

        $scope.logout = function () {
            $window.localStorage.removeItem("auth");
            $state.go('login');;
        };
    }

    controller.$inject = ['$state', '$scope', 'AutenticacaoService', 'AppService', 'NotificacaoService', '$window'];
    angular.module('agroinfoServer.dashboard').controller("DashboardCtrl", controller);

} ());  