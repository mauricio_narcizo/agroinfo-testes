'use strict';
(function () {

    var controller = function ($scope) {
        var vm = this;
        vm.message = 'Não houve dados a listar';
        vm.labels = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        vm.series = ['Cooperativa Teste', 'Copagril'];
        vm.data = [[20, 39, 48, 50, 61, 80, 90, 105, 150, 120, 190, 200],
            [28, 48, 40, 60, 86, 79, 90, 130, 124, 152, 179, 210]];
        vm.data2 = [[77, 127, 89, 192, 85, 86, 79, 150, 121, 144, 162, 75],
            [74, 88, 97, 90, 157, 158, 71, 182, 91, 170, 180, 165]];

     

    }
    controller.$inject = ['$scope'];
    angular.module('agroinfoServer.relatorios').controller("RelatorioCtrl", controller);

} ());  