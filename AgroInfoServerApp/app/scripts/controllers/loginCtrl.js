'use strict';
(function () {

    var controller = function ($state, AutenticacaoService, AppService, NotificacaoService) {
        var self = this;

        function init() {
            self.login = null;
            self.senha = null;
        }
        self.executeLogin = function () {
            AutenticacaoService.autenticar(self.login, self.senha)
                .then(
                    function (response) {
                        NotificacaoService.clear();
                        $state.go('inicial');
                    },
                    function (error) {
                        NotificacaoService.showErrorMessage('Credenciais inválidas.');
                    }
                    );
        }
        init();
    }

    controller.$inject = ['$state', 'AutenticacaoService', 'AppService', 'NotificacaoService'];
    angular.module('agroinfoServer.login').controller("LoginCtrl", controller);

} ());  