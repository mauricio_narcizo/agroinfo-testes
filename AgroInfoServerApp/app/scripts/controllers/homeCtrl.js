(function () {

    var controller = function (AppSettings, AppService, $scope, AutenticacaoService, NotificacaoService, $window) {
        var self = this;




        self.isStateHome = function () {
            return AppService.isStateHome();
        }

        self.isUsuarioAutenticado = function () {
            return AppService.isUsuarioAutenticado();
        }

        self.executeLogout = function () {
            bootbox.dialog({
                message: AppSettings.MSG_CONFIRMACAO_LOGOUT,
                title: AppSettings.CONFIRMACAO,
                buttons: {
                    main: {
                        label: AppSettings.NAO,
                        className: "btn-default"
                    },
                    success: {
                        label: AppSettings.SIM,
                        className: "btn-primary",
                        callback: function () {
                            logout();
                        }
                    },

                }
            });
        }

        function logout() {
            $window.localStorage.removeItem("auth");
        }

        self.help = function () {
            NotificacaoService.showReportHelp();
        }



    }

    controller.$inject = ['AppSettings', 'AppService', '$scope', 'AutenticacaoService', 'NotificacaoService', '$window'];
    angular.module('agroinfoServer.home').controller("HomeCtrl", controller);
} ());