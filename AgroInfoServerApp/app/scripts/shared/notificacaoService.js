(function () {

    var service = function (toastr) {
        var self = this;

        self.clear = function () {
            toastr.clear();
        };

        self.showErrorMessage = function (msg) {
            toastr.error(msg);
        };

        self.showInfoMessage = function (msg) {
            toastr.info(msg);
        };
    };

    service.$inject = ['toastr'];
    angular.module('agroinfoServer.shared').service("NotificacaoService", service);
} ()); 