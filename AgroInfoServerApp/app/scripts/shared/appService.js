(function() {
    
    var service = function(jwtHelper, $location, $timeout, $window, AppSettings) {
        var self = this;
        var token;        
        var usuario;
        var stateHome = true;
        
        self.logout = function() {
            console.log('vlida')
            $timeout(function() { 
                self.setToken(null);
                self.setUsuario(null);                            
                $location.path('/login'); 
            }, 600);
        };
        
        self.setToken = function(token) {
        	self.token = token;
        };

        self.getToken = function() {
            return localStorage.getItem('auth')
        };

        self.setUsuario = function(usuario) {
            self.usuario = usuario;
        };

        self.getUsuario = function() {
            return self.usuario == null || angular.isUndefined(self.usuario) ? null : self.usuario;
        };

        self.validaLogin = function(token) {
            var tokenOK = token != null && token != undefined && !jwtHelper.isTokenExpired(token);
            if (tokenOK) {
                var tokenPayload = jwtHelper.decodeToken(token);
                makeUsuario(tokenPayload);
                self.setToken(token);
                $location.search('token', '');
                $location.path('/dashboard'); 
            } else {
                $location.search('token', '');
                $location.path('/login');
            }            
        };
        
        function makeUsuario(tokenPayload) {
            self.usuario = {id: tokenPayload.USER_ID,
                            login: tokenPayload.USER_LOGIN
                           };        
        }        
        
        self.isTokenValid = function() {
            return self.token != null && angular.isDefined(self.token) && !jwtHelper.isTokenExpired(self.token);
        };

        self.isTokenInvalid = function() {
            return !self.isTokenValid();
        };

        self.isUsuarioAutenticado = function() {
            return self.usuario != null && angular.isDefined(self.usuario) && self.filial != null && angular.isDefined(self.filial) && self.isTokenValid();
        };

        self.getTokenExpirationDate = function() {
            if (self.isUsuarioAutenticado()) {
                return jwtHelper.getTokenExpirationDate(self.token);
            }
            return new Date();
        };

        self.getTokenExpirationInSeconds = function() {            
            var tokenExpiration = self.getTokenExpirationDate();
            var millis = new Date();
            return (tokenExpiration - millis)/1000;    
        };
        
        self.isStateHome = function() {
            return self.stateHome != null && angular.isDefined(self.stateHome) && self.stateHome;
        };
        
        self.setStateHome = function(state) {
            self.stateHome = state === null || state === undefined ? false : state;
        };
        
 
    };
    
    service.$inject = ['jwtHelper', '$location', '$timeout', '$window', 'AppSettings'];
    angular.module('agroinfoServer.shared').service("AppService", service);
}()); 