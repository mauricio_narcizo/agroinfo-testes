(function () {
      var settings = {
            REST_BASE_URL_V1: 'http://localhost:8080/AgroInfoServer/rest',  //URL base usada pelo Restangular
            MSG_CONFIRMACAO_LOGOUT: "Confirma saída do sistema?",
            CONFIRMACAO: "Confirmação",
            SIM: "Sim",
            NAO: "Não",
            UM_MINUTO_IN_MILLIS: 60000,
            MIME_TYPE_PDF: 'application/pdf',
            MIME_TYPE_XLS: 'application/vnd.ms-excel'
      };

      angular.module('agroinfoServer.shared').constant("AppSettings", settings);
} ());    
