(function () {

    var requestRecoverer = function (AppService, jwtHelper, $q) {
        var service = {
            requestError: requestError
        };

        function requestError(rejectReason) {
            $q.reject(rejectReason);
        }

        return service;
    };

    requestRecoverer.$inject = ['AppService', 'jwtHelper', '$q'];
    angular.module('agroinfoServer.shared').factory("RequestRecoverer", requestRecoverer);
} ());
