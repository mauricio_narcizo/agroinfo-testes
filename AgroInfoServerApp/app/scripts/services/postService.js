(function () {

    var service = function ($http) {
        var self = this;
        var URLBASE = 'http://localhost:8080/AgroInfoServer/rest';
        self.cooperativa = function (coop) {
            $http.defaults.headers.post["Content-Type"] = "application/json";
            return $http.post(URLBASE + "/cooperativa/", coop);
        };
    }
    service.$inject = ['$http'];
    angular.module('agroinfoServer.shared').service("PostService", service);
} ()); 