'use strict';
(function () {
  var config = function (AppSettings, $stateProvider, $urlRouterProvider, RestangularProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/inicial');
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
      })
      .state('login', {
        url: '/login',
        parent: 'base',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl as LoginCtrl'
      })
      .state('dashboard', {
        url: '/dashboard',
        parent: 'base',
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl as dashboardCtrl'
      })
      .state('inicial', {
        url: '/inicial',
        parent: 'dashboard',
        templateUrl: 'views/dashboard/inicial.html',
        controller: 'HomeCtrl'
      })
      .state('cooperativas', {
        url: '/cooperativas',
        parent: 'dashboard',
        controller: 'CooperativaCtrl as cooperativaCtrl',
        templateUrl: 'views/dashboard/cooperativas.html'
      })
      .state('registrar', {
        url: '/registrar',
        parent: 'dashboard',
        controller: 'CooperativaCtrl as cooperativaCtrl',
        templateUrl: 'views/dashboard/cooperativas/registrar.html'
      })
      .state('relatorios', {
        url: '/relatorios',
        parent: 'dashboard',
        controller: 'RelatorioCtrl as relatorioCtrl',
        templateUrl: 'views/dashboard/relatorios.html'
      });
  };
     
  config.$inject = ['AppSettings', '$stateProvider', '$urlRouterProvider', 'RestangularProvider'];
  angular.module('agroinfoServer').config(config);
} ());    