'use strict';

<<<<<<< HEAD

=======
>>>>>>> origin/master
angular.module('agroinfoServer.cooperativas', []);
angular.module('agroinfoServer.login', []);
angular.module('agroinfoServer.dashboard', []);
angular.module('agroinfoServer.relatorios', []);
angular.module('agroinfoServer.shared', []);
angular.module('agroinfoServer.home', []);
angular.module('agroinfoServer.services', []);

angular.module('agroinfoServer', ['ui.router', //gerenciador de rotas
    'ngAnimate', //pacote de animacoes
//'ui.bootstrap', //pacote de componentes angular-bootstrap (datepicker, collapse, etc)
    'anim-in-out', //transicoes entre telas (states)
    'ngTable', //tabela com paginacao, agrupamentos, etc
    'ngMaterial',
    'chart.js', //graficos
    'restangular', //comunicacao com servidor REST						 
//'ui.bootstrap.showErrors', //validacao de forms     
    'angular-jwt', //Json Web Token
    'toastr', //angular-toastr (notificacoes)
    'agroinfoServer.home',
    'agroinfoServer.cooperativas',
    'agroinfoServer.login',
    'agroinfoServer.dashboard',
    'agroinfoServer.relatorios',
    'agroinfoServer.shared',
    'agroinfoServer.services'
]).config(function (RestangularProvider) {
    RestangularProvider.setBaseUrl('http://localhost:8080/AgroInfoServer/rest');
});;



angular.module('agroinfoServer').config(function ($provide) {
    $provide.decorator('$state', function ($delegate, $stateParams) {
        $delegate.forceReload = function () {
            return $delegate.go($delegate.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
});



angular.module('agroinfoServer').run(['Restangular', 'AppService', '$location', '$rootScope', 'jwtHelper', '$state', '$http', function (Restangular, AppService, $location, $rootScope, jwtHelper, $state, $http) {  

    Restangular.addRequestInterceptor(function (element, operation, what, url) {
        Restangular.setDefaultHeaders({ Authorization: localStorage.getItem('auth'),'Content-Type': 'application/json;charset=utf-8'})
    });


    
    Restangular.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
        var headers = response.headers();

        var token = localStorage.getItem('auth');
        if (token == null && headers.authorization != null) {
            token = headers.authorization;
            localStorage.setItem('auth', token);
            AppService.validaLogin(token);
            AppService.setUsuario(jwtHelper.decodeToken(token));
        }

        $rootScope.$on("$locationChangeStart",
            function (event, next, current) {
                var token = localStorage.getItem('auth');
                if (token != null && jwtHelper.isTokenExpired(token)) {
                    $rootScope.user = jwtHelper.decodeToken(token);
                    localStorage.setItem('auth', token);
                }
            });
        return data;
    });
    //Interceptando o 'response de erro' do server
    Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {
        $state.go('login');
    });
    //Evento disparado sempre que se inicia a transicao entre uma view (state) e outra

    $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
        var token = localStorage.getItem('auth');
        if (token == null || jwtHelper.isTokenExpired(token)) {
            if (to.url != '/login') {
                $state.go('login');
                ev.preventDefault();
            }
        } else {
            AppService.setStateHome(to.url === '/home');
            if (to.url == '/login') {
                $state.go('dashboard');
                ev.preventDefault();
            }
        }

    });
}]);	