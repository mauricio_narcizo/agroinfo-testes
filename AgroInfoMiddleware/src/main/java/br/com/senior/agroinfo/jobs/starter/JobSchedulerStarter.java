package br.com.senior.agroinfo.jobs.starter;

import java.io.Serializable;
import java.util.List;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;

public abstract class JobSchedulerStarter implements Serializable {

    private static final long serialVersionUID = 1L;

    public abstract void initScheduleJob(final Scheduler scheduler, final Agendamento agendamento,
            final Configuracoes configuracoes, final List<EmpresaFilial> filiais) throws SchedulerException;

	public abstract void unscheduleJob(Scheduler scheduler) throws SchedulerException;
}
