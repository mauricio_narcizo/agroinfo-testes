package br.com.senior.agroinfo.sync.services;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import br.com.senior.agroinfo.db.dao.ContaDAO;
import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.TitulosNotFoundException;
import br.com.senior.agroinfo.util.parsers.ContasReceberParser;
import br.com.senior.agroinfo.ws.client.ContasReceberClient;
import br.com.senior.services.TitulosExportarTitulosReceberIn;
import br.com.senior.services.TitulosExportarTitulosReceberOut;
import br.com.senior.services.TitulosExportarTitulosReceberOutTitulosReceber;

public class ContasReceberService {
	
	@Inject
	private ContaDAO contaDAO;

	@Inject
	private CooperadoDAO cooperadoDAO;

	@Inject
	private TipoTituloService tipoTituloService;

	public synchronized void sincronizarContasReceber(final String usuario, final String senha, final String url, final int codEmp,
			final Long codFilial, List<TipoTitulo> tipoTitulos, final String identificacaoSistema,
			final String tipoIntegracao) {

		List<Cooperado> cooperadosAtivos = cooperadoDAO.getCooperadosAtivos();
		cooperadosAtivos.forEach(cooperado -> {
			sincronizarContasReceberCooperado(usuario, senha, url, codEmp, codFilial, tipoTitulos, identificacaoSistema,
					tipoIntegracao, cooperado);
		});
	}

	private void sincronizarContasReceberCooperado(final String usuario, final String senha, final String url,
			final int codEmp, final Long codFilial, List<TipoTitulo> tipoTitulos, final String identificacaoSistema,
			final String tipoIntegracao, Cooperado cooperado) {

		boolean finalizouIntegracaoRegistros = false;
		do {
			Optional<TitulosExportarTitulosReceberOut> titulosOut = Optional.empty();
			try {
				titulosOut = ContasReceberClient.buscarContasReceber(usuario, senha, url,
						obterParamConsultaContasReceber(codEmp, codFilial, cooperado.getId().intValue(),
								identificacaoSistema, tipoIntegracao));

				persistirContasReceber(usuario, senha, url, codEmp, codFilial, tipoTitulos, identificacaoSistema,
						cooperado, titulosOut.get());

			} catch (Exception e) {
				// Não é necessário logar o erro, por não ter encontrado titulos
				// para o cooperado
				if (!(e instanceof TitulosNotFoundException)) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
				}
			}

			if (titulosOut.isPresent()) {
				finalizouIntegracaoRegistros = "S".equalsIgnoreCase(titulosOut.get().getFinalizaramRegistros());
			} else {
				finalizouIntegracaoRegistros = true;
			}
		} while (!finalizouIntegracaoRegistros);
	}

	private void persistirContasReceber(final String usuario, final String senha, final String url, final int codEmp,
			final Long codFilial, List<TipoTitulo> tipoTitulos, final String identificacaoSistema, Cooperado cooperado,
			TitulosExportarTitulosReceberOut titulosOut) throws EnumNotFoundException {

		for (TitulosExportarTitulosReceberOutTitulosReceber titulos : titulosOut.getTitulosReceber()) {

			Optional<TipoTitulo> tipoTitulo = obterTipoTitulo(usuario, senha, url, codEmp, codFilial, tipoTitulos,
					titulos.getCodTpt(), identificacaoSistema, TipoConta.RECEBER);

			tipoTitulo.ifPresent(tipo -> {
				Optional<Conta> representation = ContasReceberParser.fromRepresentation(titulos, cooperado,
						tipo);
				representation.ifPresent(contaReceber -> {
					try{
					contaDAO.create(contaReceber);
					}catch(Exception e){
						Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,e.getMessage());
						throw e;
					}
				});
			});
		}
	}

	private TitulosExportarTitulosReceberIn obterParamConsultaContasReceber(final int codEmp, final Long codFilial,
			final int codFornecedor, final String identificacaoSistema, final String tipoIntegracao) {
		TitulosExportarTitulosReceberIn titulos = new TitulosExportarTitulosReceberIn();
		titulos.setCodEmp(codEmp);
		titulos.setCodFil(codFilial);
		titulos.setIdentificadorSistema(identificacaoSistema);

		// titulos.setConsulta(consultas);
		titulos.setQuantidadeRegistros(1000);
		titulos.setTipoIntegracao(tipoIntegracao);

		return titulos;
	}

	private Optional<TipoTitulo> obterTipoTitulo(final String usuario, final String senha, final String url,
			final int codEmp, final Long codFilial, final List<TipoTitulo> tipoTitulos, final String codTipoTitulo,
			final String identificacaoSistema, TipoConta tipoConta) throws EnumNotFoundException {

		Optional<TipoTitulo> tipoTituloOpt = tipoTitulos.stream()
				.filter(tipoTitulo -> tipoTitulo.getCodTipoTitulo().equals(codTipoTitulo)).findFirst();

		if (!tipoTituloOpt.isPresent()) {
			tipoTituloOpt = tipoTituloService.obterTipoTitulo(usuario, senha, url, codEmp, codFilial, codTipoTitulo,
					identificacaoSistema, tipoConta);
			tipoTituloOpt.ifPresent(tipoTitulo -> {
				tipoTitulos.add(tipoTitulo);
			});
		}
		return tipoTituloOpt;
	}
}
