package br.com.senior.agroinfo.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.testng.log4testng.Logger;

import com.google.common.collect.Lists;

import br.com.senior.agroinfo.db.dao.ContaDAO;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.dto.ContaDTO;
import br.com.senior.agroinfo.exception.BaseException;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.RegistroNaoEncontradoException;

@Stateless
public class ContaService implements Serializable {

	private static final long serialVersionUID = 6739109009855360295L;

	@Inject
	private ContaDAO contaDAO;

	public List<ContaDTO> obterContaCooperadoDTO(Long idCooperado, Date dataInicio, Date dataFim) throws BaseException {

		List<Conta> contas = Collections.emptyList();
		if (dataInicio != null && dataFim != null) {
			contas = contaDAO.buscaContasPeriodo(idCooperado,dataInicio,dataFim);
		} else {
			contas = contaDAO.buscaContas(idCooperado);
		}
		
		List<ContaDTO> listContaDTO = new ArrayList<>(contas.size());
		contas.stream().forEach(conta -> {
			try {
				listContaDTO.add(ContaDTO.of(conta));
			} catch (Exception e) {
				Logger.getLogger(this.getClass()).warn(e.getMessage());
			}
		});
		return listContaDTO;
	}

	public ContaDTO obterDTO(String id) throws BaseException {

		Optional<Conta> optional = contaDAO.busca(id);

		Conta conta = optional.orElseThrow(() -> new RegistroNaoEncontradoException("Conta não encontrada."));
		return ContaDTO.of(conta);
	}

	public List<ContaDTO> obterDTOs() throws EnumNotFoundException {
		List<ContaDTO> dtos = Lists.newArrayList();
		for (Conta conta : obterTodasContas()) {
			ContaDTO dto = ContaDTO.of(conta);
			dtos.add(dto);
		}
		return dtos;
	}

	public List<Conta> obterTodasContas() {
		return contaDAO.getLista();
	}

}
