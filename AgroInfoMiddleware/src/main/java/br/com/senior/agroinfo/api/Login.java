package br.com.senior.agroinfo.api;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/login")
public class Login {

    @Context
    UriInfo uri;

    @POST
    @Consumes("text/plain")
    @Produces({ MediaType.TEXT_PLAIN })
    public Response validate() throws URISyntaxException {

        /*
         * ConfiguracoesId conf = new Gson().fromJson(configs,
         * ConfiguracoesId.class); // Parametros exporta
         * ExportacaoempresafilialExportarIn exportarIn = new
         * ExportacaoempresafilialExportarIn(null, "");
         * 
         * String iperp =conf.getEndereco(); String porta = conf.getPorta();
         * String usuario = conf.getUsuario(); String senha = conf.getSenha();
         * 
         * String url = "http://"+iperp+":"+porta; //Chamado Webservice try {
         * ExportacaoempresafilialExportarOut exportarOut =
         * UtilProxy.getServiceProxy(
         * Sapiens_Synccom_senior_g5_co_mct_ctb_exportacaoempresafilial.class,
         * url).exportar(usuario, senha, 0, exportarIn); } catch
         * (RemoteException | ServiceException e) { // TODO Auto-generated catch
         * block e.printStackTrace(); } System.out.println("TEste");
         * 
         * return null;
         * 
         * 
         * java.nio.file.Path path = Paths.get(uri.getBaseUri()); URI
         * targetURIForRedirection =path.getParent().toUri(); URI url2 = new
         * URI(targetURIForRedirection.getPath()+"/login.html"); return
         * Response.seeOther(url2).build();
         */
        return null;
    }

    @GET
    @Produces({ MediaType.TEXT_PLAIN })
    public Response redirectlogin() {
        java.nio.file.Path path = Paths.get(uri.getBaseUri());
        URI targetURIForRedirection = path.getParent().toUri();
        URI url2 = null;
        try {
            url2 = new URI(targetURIForRedirection.getPath() + "/login.html");
        } catch (URISyntaxException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return Response.seeOther(url2).build();
    }
}
