package br.com.senior.agroinfo.jobs;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.sms.CheckSMS;
import br.com.senior.agroinfo.sms.pojos.MessageSMS;

@Dependent
public class BuscaSMSJob implements Job {

	@Inject
	CheckSMS checkSMS;

	@Override
	public synchronized void execute(JobExecutionContext job) throws JobExecutionException {

		if (true) { /* colocar o tipo do sms aqui */
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Buscando SMS");
			Map<Contato, MessageSMS> contatoMensagem = checkSMS.BuscaSMS();
			contatoMensagem.forEach((key, value) -> {
				System.out.println(value.getMessage());
			});
			/*
			 * TODO Verificar a informação solicitada
			 * 
			 * Processar os dados
			 * 
			 * enviar a resposta
			 * 
			 * apagar a mensagem
			 * 
			 */

		}
	}

}
