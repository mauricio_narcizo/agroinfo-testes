package br.com.senior.agroinfo.dto;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

public class EmpresaDTO implements Serializable{
	
	private static final long serialVersionUID = 6839502800288934974L;	

	private Integer codigoEmpresa;
	
	private String descricaoEmpresa;
	
	private List<FilialDTO> filiais = Lists.newArrayList();
	
	public EmpresaDTO(){
		
	}
	
	public EmpresaDTO(Integer codigoEmpresa, String descricaoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
		this.descricaoEmpresa = descricaoEmpresa;
	}

	public Integer getCodigoEmpresa() {
		return codigoEmpresa;
	}

	public void setCodigoEmpresa(Integer codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getDescricaoEmpresa() {
		return descricaoEmpresa;
	}

	public void setDescricaoEmpresa(String descricaoEmprea) {
		this.descricaoEmpresa = descricaoEmprea;
	}

	public List<FilialDTO> getFiliais() {
		return filiais;
	}

	public void setFiliais(List<FilialDTO> filiais) {
		this.filiais = filiais;
	}
	
	public void addFilial(FilialDTO filial) {
		filiais.add(filial);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoEmpresa == null) ? 0 : codigoEmpresa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaDTO other = (EmpresaDTO) obj;
		if (codigoEmpresa == null) {
			if (other.codigoEmpresa != null)
				return false;
		} else if (!codigoEmpresa.equals(other.codigoEmpresa))
			return false;
		return true;
	}
	
	

}
