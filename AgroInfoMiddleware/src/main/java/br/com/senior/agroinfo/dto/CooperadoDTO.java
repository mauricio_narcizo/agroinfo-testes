package br.com.senior.agroinfo.dto;

import java.io.Serializable;

import br.com.senior.agroinfo.db.modal.Cooperado;

public class CooperadoDTO implements Serializable {
	
	private static final long serialVersionUID = 1294671087845664465L;	

	private Long id;
    private String cpfCnpj;
    private boolean atualizar;
    private String cidade;
    private boolean ativo;
    
    CooperadoDTO(){
    	
    }
    
    public static CooperadoDTO create(Cooperado cooperado) {
    	CooperadoDTO dto = new CooperadoDTO();
    	dto.setId(cooperado.getId());    	
    	dto.atualizar = cooperado.isAtualizar();
    	dto.setAtivo(cooperado.isAtivo());
    	dto.setCpfCnpj(cooperado.getCpfCnpj());
    	dto.setCidade(cooperado.getCidade());
    	return dto;
    }    

	public Long getId() {
		return id;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isAtivo() ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CooperadoDTO other = (CooperadoDTO) obj;
		if (isAtivo() != other.isAtivo())
			return false;
		return true;
	}

	public boolean isAtualizar() {
		return atualizar;
	}

	public void setAtualizar(boolean atualizar) {
		this.atualizar = atualizar;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
    
    

}
