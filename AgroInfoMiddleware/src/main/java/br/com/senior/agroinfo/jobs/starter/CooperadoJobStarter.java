package br.com.senior.agroinfo.jobs.starter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.enums.FormatoPeriodicidadeScheduler;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.jobs.CooperadoJob;

public class CooperadoJobStarter extends JobSchedulerStarter {

	private static final long serialVersionUID = 1L;

	public static final String CARGA_PARCIAL = "triggerCargaParcialCooperado";
	public static final String CARGA_COMPLETA = "triggerCargaCompletaCooperado";

	private static JobSchedulerStarter instance = new CooperadoJobStarter();

	private CooperadoJobStarter() {
	}

	@Override
	public void initScheduleJob(final Scheduler scheduler, final Agendamento agendamento,
			final Configuracoes configuracoes, final List<EmpresaFilial> filiais) throws SchedulerException {
		JobDetail jobDetail = JobBuilder.newJob(CooperadoJob.class).build();
		jobDetail.getJobDataMap().put("configuracoes", configuracoes);
		jobDetail.getJobDataMap().put("filiais", filiais);

		Set<SimpleTrigger> triggers = obterTrigger(agendamento);
		scheduler.scheduleJob(jobDetail, triggers, true);
	}

	@Override
	public void unscheduleJob(final Scheduler scheduler) throws SchedulerException {
		scheduler.unscheduleJob(new TriggerKey(CARGA_PARCIAL));
		scheduler.unscheduleJob(new TriggerKey(CARGA_COMPLETA));
	}

	private static Set<SimpleTrigger> obterTrigger(Agendamento agendamento) {

		Set<SimpleTrigger> triggers = new HashSet<>();
		triggers.add(obterTriggerCargaParcial(agendamento));
		triggers.add(obterTriggerCargaCompleta(agendamento));
		return triggers;
	}

	private static SimpleTrigger obterTriggerCargaParcial(Agendamento agendamento) {

		SimpleTrigger triggerCargaParcial = TriggerBuilder
				.newTrigger().withSchedule(FormatoPeriodicidadeScheduler
						.obterSchedulerBuilder(agendamento.getTipoPeriodicidade(), agendamento.getPeriodicidade()))
				.withIdentity(CARGA_PARCIAL).build();
		triggerCargaParcial.getJobDataMap().put("tipoIntegracao", TipoIntegracao.ALTERADOS);
		return triggerCargaParcial;
	}

	private static SimpleTrigger obterTriggerCargaCompleta(Agendamento agendamento) {

		SimpleTrigger triggerCargaCompleta = TriggerBuilder
				.newTrigger().withSchedule(FormatoPeriodicidadeScheduler
						.obterSchedulerBuilder(agendamento.getTipoCargaCompleta(), agendamento.getCargaCompleta()))
				.withIdentity(CARGA_COMPLETA).build();

		triggerCargaCompleta.getJobDataMap().put("tipoIntegracao", TipoIntegracao.TODOS);
		return triggerCargaCompleta;
	}

	public static JobSchedulerStarter getInstance() {
		return instance;
	}
}