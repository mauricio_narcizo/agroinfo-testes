package br.com.senior.agroinfo.sync.services;

import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.senior.agroinfo.db.dao.ContatoDAO;
import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.exceptions.FornecedorWSException;
import br.com.senior.agroinfo.util.parsers.ContatoParser;
import br.com.senior.agroinfo.util.parsers.CooperadoParser;
import br.com.senior.agroinfo.ws.client.CooperadoClient;
import br.com.senior.services.FornecedoresExportarIn;
import br.com.senior.services.FornecedoresExportarOut;
import br.com.senior.services.FornecedoresExportarOutGridFornecedores;

@Stateless
public class CooperadoService {

	@Inject
	private CooperadoDAO cooperadoDao;

	@Inject
	private ContatoDAO contatoDAO;

	public void atualizarCooperados(final String usuario, final String senha, final String url, final int codEmp,
			final Long codFilial, final String identificacaoSistema, final int qtdRegistros,
			final String tipoIntegracao, String dddPadrao) throws FornecedorWSException {

		FornecedoresExportarIn fornecedoresExportarIn = obterParamConsultaFornecedor(codEmp, codFilial,
				identificacaoSistema, qtdRegistros, tipoIntegracao);

		obterListaCooperados(usuario, senha, url, fornecedoresExportarIn, dddPadrao);

	}

	private void obterListaCooperados(final String usuario, final String senha, final String url,
			final FornecedoresExportarIn fornecedorIn, String dddPadrao) throws FornecedorWSException {

		boolean finalizouIntegracaoRegistros = false;
		do {
			FornecedoresExportarOut cooperadosWS = CooperadoClient.buscarCooperadosWS(usuario, senha, url,
					fornecedorIn);

			Optional.ofNullable(cooperadosWS.getGridFornecedores()).ifPresent(cooperados -> {
				for (FornecedoresExportarOutGridFornecedores fornecedor : cooperados) {

					Cooperado cooperado = CooperadoParser.fromRepresentation(fornecedor);

					persistirCooperados(cooperado);
					ContatoParser.fromRepresentation(fornecedor, cooperado).forEach(contato -> {
						if (contato.getTelefone() != null) {
							if (contato.getDdd() == null || contato.getDdd().trim().isEmpty()) {
								contato.setDdd(dddPadrao);
							}
							persitirContatos(contato);
						}
					});

				}
			});

			finalizouIntegracaoRegistros = "S".equalsIgnoreCase(cooperadosWS.getFinalizaramRegistros());
		} while (!finalizouIntegracaoRegistros);
	}

	private FornecedoresExportarIn obterParamConsultaFornecedor(final int codEmp, final Long codFilial,
			final String identificacaoSistema, final int qtdRegistros, final String tipoIntegracao) {

		return new FornecedoresExportarIn(codEmp, codFilial, "", "", null, identificacaoSistema, qtdRegistros,
				tipoIntegracao);
	}

	private void persistirCooperados(Cooperado cooperado) {
		cooperadoDao.create(cooperado);
	}

	private void persitirContatos(Contato contato) {
		contatoDAO.create(contato);
	}
}