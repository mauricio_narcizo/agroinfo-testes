package br.com.senior.agroinfo.jobs.starter;

import java.util.List;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.enums.FormatoPeriodicidadeScheduler;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.jobs.BuscaSMSJob;

public class BuscaSMSJobStarter extends JobSchedulerStarter {

	private static final long serialVersionUID = 1L;

	private static BuscaSMSJobStarter jobStarter = new BuscaSMSJobStarter();
	public static final String CARGA_PARCIAL = "triggerBuscaSMS";

	private BuscaSMSJobStarter() {
	}

	public static BuscaSMSJobStarter getInstance() {
		return jobStarter;
	}

	@Override
	public void initScheduleJob(Scheduler scheduler, Agendamento agendamento, Configuracoes configuracoes,
			List<EmpresaFilial> filiais) throws SchedulerException {
		JobDetail jobDetail = JobBuilder.newJob(BuscaSMSJob.class).build();
		jobDetail.getJobDataMap().put("configuracoes", configuracoes);
		jobDetail.getJobDataMap().put("filiais", filiais);

		SimpleTrigger triggers = obterTrigger(agendamento);

		scheduler.scheduleJob(jobDetail, triggers);

	}

	@Override
	public void unscheduleJob(final Scheduler scheduler) throws SchedulerException {
		scheduler.unscheduleJob(new TriggerKey(CARGA_PARCIAL));
	}

	private static SimpleTrigger obterTrigger(Agendamento agendamento) {

		SimpleTrigger triggerCargaParcial = TriggerBuilder
				.newTrigger().withSchedule(FormatoPeriodicidadeScheduler
						.obterSchedulerBuilder(agendamento.getTipoPeriodicidade(), agendamento.getPeriodicidade()))
				.withIdentity(CARGA_PARCIAL).build();
		triggerCargaParcial.getJobDataMap().put("tipoIntegracao", TipoIntegracao.TODOS);

		return triggerCargaParcial;
	}
}
