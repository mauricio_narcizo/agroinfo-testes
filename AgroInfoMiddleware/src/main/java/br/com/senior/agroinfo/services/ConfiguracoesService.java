package br.com.senior.agroinfo.services;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.senior.agroinfo.db.dao.ConfiguracoesDAO;
import br.com.senior.agroinfo.db.enums.TipoSMS;
import br.com.senior.agroinfo.db.enums.TipoSistema;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.dto.IntegracaoDTO;
import br.com.senior.agroinfo.exception.EnumNotFoundException;

@Stateless
public class ConfiguracoesService implements Serializable {
	

	private static final long serialVersionUID = 1L;
	
	
	@PersistenceContext
	private transient EntityManager em;
		
	@Inject
    private transient ConfiguracoesDAO configuracoesDAO;
	
	@Inject
	private transient EmpresaFilialService empresaFilialService;

    public Optional<Configuracoes> obterConfiguracao() {
        return configuracoesDAO.retrieve();
    }
    
    public IntegracaoDTO obterDTO() {
    	return IntegracaoDTO.of(obterConfiguracao(), empresaFilialService.obterFiliaisPorEmpresa());
    }

	public void salvarConfiguracaoIntegracao(IntegracaoDTO integracaoDTO) throws EnumNotFoundException {
		Configuracoes config = obterConfiguracao()
										.orElse(config = new Configuracoes(1L));
		config.setEndereco(integracaoDTO.getEndereco());
		config.setPorta(integracaoDTO.getPorta());
		config.setUsuario(integracaoDTO.getUsuario());
		config.setSenha(integracaoDTO.getSenha());
		config.setSigla(integracaoDTO.getSigla());
		config.setIpsms(integracaoDTO.getIpsms());
		config.setPortasms(integracaoDTO.getPortasms());
		config.setUsuariosms(integracaoDTO.getUsuariosms());
		config.setSenhasms(integracaoDTO.getSenhasms());
		config.setTiposistema(TipoSistema.getByDescricao(integracaoDTO.getTiposistema()));
		config.setTiposms(TipoSMS.getByDescricao(integracaoDTO.getTipoSistemaSMS()));
		config.setDtUpdate(new Date());
		integracaoDTO.getEmpresas()
						.forEach(dto -> {
								dto.getFiliais().forEach(filialDTO -> {
										EmpresaFilial empresaFilial = em.find(EmpresaFilial.class, filialDTO.getIdFilial());
										empresaFilial.setAtiva(filialDTO.isAtivo());
								});
						});
		em.merge(config);
	}

}
