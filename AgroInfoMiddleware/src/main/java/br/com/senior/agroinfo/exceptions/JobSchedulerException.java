package br.com.senior.agroinfo.exceptions;

import br.com.senior.agroinfo.exception.BaseException;

public class JobSchedulerException extends BaseException {

    private static final long serialVersionUID = 1L;

    public JobSchedulerException(final String msg) {
        super(msg);
    }

}
