package br.com.senior.agroinfo.jobs.starter;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.enums.Jobs;
import br.com.senior.agroinfo.exceptions.JobSchedulerException;
import br.com.senior.agroinfo.services.ConfiguracoesService;
import br.com.senior.agroinfo.services.EmpresaFilialService;

@Stateless
public class JobsConfig implements Serializable {

	private static final long serialVersionUID = 6937792303425286350L;

	@Inject
	private ConfiguracoesService configuracoesService;

	@Inject
	private EmpresaFilialService empresaFilialService;

	public void configJobs(final Scheduler scheduler, final List<Agendamento> listaAgendamentos)
			throws JobSchedulerException {
		try {
			Optional<Configuracoes> configuracao = configuracoesService.obterConfiguracao();
			List<EmpresaFilial> filiais = empresaFilialService.obterEmpresasFiliais();

			if (!configuracao.isPresent()) {
				return;
			}
			for (Agendamento agendamento : listaAgendamentos) {
				doScheduler(scheduler, configuracao, filiais, agendamento);
			}
		} catch (Exception e) {
			throw new JobSchedulerException(e.getMessage());
		}
	}

	private void doScheduler(Scheduler scheduler, Optional<Configuracoes> configuracao, List<EmpresaFilial> filiais,
			Agendamento agendamento) throws SchedulerException {
		Jobs job = Jobs.findJob(agendamento.getTipoAgendamento());
		if (agendamento.isAtivo()) {
			job.getClazz().initScheduleJob(scheduler, agendamento, configuracao.get(), filiais);
		} else {
			job.getClazz().unscheduleJob(scheduler);
		}

	}

}
