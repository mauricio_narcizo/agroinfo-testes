package br.com.senior.agroinfo.enums;

import org.quartz.SimpleScheduleBuilder;

import br.com.senior.agroinfo.db.enums.FormatoPeriodicidade;

public enum FormatoPeriodicidadeScheduler {

    MINUTOS(60), HORAS(MINUTOS.getTempoSegundos() * 60), 
    	DIAS(HORAS.getTempoSegundos() * 24), SEMANAS(DIAS.getTempoSegundos() * 7);

    private final int tempoSegundos;

    private FormatoPeriodicidadeScheduler(final int tempoSegundos) {
        this.tempoSegundos = tempoSegundos;
    }

    public int getTempoSegundos() {
        return tempoSegundos;
    }

    /**
     * Utiliza o metodo repeatSecondlyForever para obter o tempo necessario para
     * o agendamento
     * 
     * @param periodicidadeScheduler
     * @param time
     * @return Retorna um {@link SimpleScheduleBuilder} utilizando o metodo
     *         repeatSecondlyForever, multiplicando o {@value time} pelos
     *         segundos do periodo.
     */
    public static SimpleScheduleBuilder obterSchedulerBuilder(FormatoPeriodicidade periodicidade,
            int time) {
        return SimpleScheduleBuilder.repeatSecondlyForever(periodicidade.getTempoSegundos() * time);
    }
}
