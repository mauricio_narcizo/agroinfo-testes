package br.com.senior.agroinfo.jobs.starter;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;

import br.com.senior.agroinfo.exceptions.JobSchedulerException;
import br.com.senior.agroinfo.services.AgendamentoService;

@Startup
@Singleton
public class JobStarter {

	@Inject
	AgendamentoService agendamentoService;

	@Inject
	JobsConfig jobsInit;

	@Inject
	private JobFactory jobFactory;

	private static Scheduler scheduler;

	@PostConstruct
	public void init() throws Exception {
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.setJobFactory(jobFactory);
			agendamentoService.criarAgendamentosPadrao();
			jobsInit.configJobs(scheduler, agendamentoService.obterAgendamentosAtivos());
			scheduler.start();
		} catch (Exception e) {
			throw new JobSchedulerException(e.getMessage());
		}
	}

	@PreDestroy
	public void destroy() throws Exception {
		scheduler.shutdown(true);
	}

	public static Scheduler getScheduler() {
		return scheduler;
	}
}