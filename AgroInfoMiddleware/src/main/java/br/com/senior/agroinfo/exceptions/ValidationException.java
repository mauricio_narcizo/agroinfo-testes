package br.com.senior.agroinfo.exceptions;

import br.com.senior.agroinfo.exception.BaseException;

public class ValidationException extends BaseException{

	private static final long serialVersionUID = -7817392739716833400L;
	
	public ValidationException(String msg) {
		super(msg);
	}	

}
