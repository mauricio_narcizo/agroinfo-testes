package br.com.senior.agroinfo.enums;

import org.jboss.weld.exceptions.IllegalArgumentException;

import br.com.senior.agroinfo.db.enums.TipoAgendamento;
import br.com.senior.agroinfo.jobs.starter.BuscaSMSJobStarter;
import br.com.senior.agroinfo.jobs.starter.ContasPagarJobStarter;
import br.com.senior.agroinfo.jobs.starter.ContasReceberJobStarter;
import br.com.senior.agroinfo.jobs.starter.CooperadoJobStarter;
import br.com.senior.agroinfo.jobs.starter.JobSchedulerStarter;

public enum Jobs {

	COOPERADO(CooperadoJobStarter.getInstance(), TipoAgendamento.COOPERADO),

	BUSCASMS(BuscaSMSJobStarter.getInstance(), TipoAgendamento.BUSCASMS),

	CONTAS_PAGAR(ContasPagarJobStarter.getInstance(), TipoAgendamento.CONTAS_PAGAR),
	
	CONTAS_RECEBER(ContasReceberJobStarter.getInstance(),TipoAgendamento.CONTAS_RECEBER);

	private final JobSchedulerStarter clazz;

	private final TipoAgendamento tipoAgendamento;

	public TipoAgendamento getTipoAgendamento() {
		return tipoAgendamento;
	}

	private Jobs(final JobSchedulerStarter clazz, final TipoAgendamento tipoAgendamento) {
		this.clazz = clazz;
		this.tipoAgendamento = tipoAgendamento;
	}

	public static Jobs findJob(final TipoAgendamento tipoAgendamento) {
		for(Jobs job : values()){
			if(job.tipoAgendamento == tipoAgendamento){
				return job;
			}
		}
		throw new IllegalArgumentException("Não foi encontrado um valor para o enumerador: "+ tipoAgendamento);
	}

	public JobSchedulerStarter getClazz() {
		return clazz;
	}

}
