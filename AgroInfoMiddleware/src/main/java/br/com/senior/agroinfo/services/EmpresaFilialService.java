package br.com.senior.agroinfo.services;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.senior.agroinfo.db.dao.EmpresaFilialDAO;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.dto.EmpresaDTO;
import br.com.senior.agroinfo.dto.FilialDTO;

@Stateless
public class EmpresaFilialService implements Serializable {
    
	private static final long serialVersionUID = -1821828632663448096L;
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
    private EmpresaFilialDAO empresaFilialDAO;
    
	public List<EmpresaFilial> obterEmpresasFiliais(){
        return (List<EmpresaFilial>) empresaFilialDAO.getLista();
    }
    
    @SuppressWarnings("unchecked")
    public List<EmpresaDTO> obterFiliaisPorEmpresa() {
    	List<EmpresaDTO> empresas = 
				(List<EmpresaDTO>) em.createQuery("Select new br.com.senior.agroinfo.dto.EmpresaDTO(ef.codigoEmpresa, ef.nomeEmpresa) "
																				+ "From EmpresaFilial ef group by ef.codigoEmpresa, ef.nomeEmpresa")
    																												.getResultList();
		List<EmpresaFilial> empresaFiliais = (List<EmpresaFilial>) empresaFilialDAO.getLista();
		empresas.forEach(dto -> {
			List<EmpresaFilial> ef = empresaFiliais.stream()
												.filter(t -> t.getCodigoEmpresa().equals(dto.getCodigoEmpresa()))
												.collect(Collectors.toList());
			ef.forEach(filial -> dto.addFilial(new FilialDTO(filial.getId(), filial.getNomeFilial(), filial.isAtiva())));
		});

    	return empresas;
    }

}
