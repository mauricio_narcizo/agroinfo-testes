package br.com.senior.agroinfo.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.google.common.collect.Lists;

import br.com.senior.agroinfo.db.dao.AgendamentoDAO;
import br.com.senior.agroinfo.db.enums.FormatoPeriodicidade;
import br.com.senior.agroinfo.db.enums.TipoAgendamento;
import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.dto.AgendamentoDTO;
import br.com.senior.agroinfo.exceptions.JobSchedulerException;
import br.com.senior.agroinfo.jobs.starter.JobStarter;
import br.com.senior.agroinfo.jobs.starter.JobsConfig;

@Stateless
public class AgendamentoService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
    private transient AgendamentoDAO dao;
    
    @Inject
    private transient EntityManager em;
    
    @Inject
    JobsConfig jobsConfig;

    public List<Agendamento> obterAgendamentosAtivos() {
        return dao.getListaAtivo();
    }
    
	public List<Agendamento> obterTodosAgendamentos() {
        return dao.getLista();
    }    

    public List<AgendamentoDTO> obterDTOs() {
        List<AgendamentoDTO> dtos = Lists.newArrayList();
        obterTodosAgendamentos()
        				.stream()
        				.forEach(agendamento -> {
				            AgendamentoDTO dto = AgendamentoDTO.create(agendamento);
				            dtos.add(dto);
				        });
        return dtos;
    }

	public void criarAgendamentosPadrao() {
		List<Agendamento> agendamentos = obterTodosAgendamentos();
		for(TipoAgendamento tipo: TipoAgendamento.values()) {
			if (!agendamentos
							.stream()
							.anyMatch(a -> a.getTipoAgendamento().equals(tipo))) {
				
				dao.adiciona(new Agendamento.Builder()
											.ativo(false)
											.cargaCompleta(0)
											.executando(false)
											.periodicidade(0)
											.grupo(null)
											.periodicidade(0)
											.tempoUltimaExecucao(0L)
											.tipoAgendamento(tipo)
											.tipoCargaCompleta(FormatoPeriodicidade.SEMANAS)
											.tipoPeriodicidade(FormatoPeriodicidade.SEMANAS)
											.build());
			}
		}
		
	}

	public void salvarAgendamentos(List<AgendamentoDTO> dtos) throws Exception {

		List<Agendamento> agendamentos = new ArrayList<>(dtos.size());
		for (AgendamentoDTO agendamentoDTO : dtos) {

			Agendamento agendamento = new Agendamento.Builder()
						.id(agendamentoDTO.getId())
						.ativo(agendamentoDTO.isAtivo())
						.cargaCompleta(agendamentoDTO.getCargaCompleta())
						.executando(agendamentoDTO.isExecutando())
						.periodicidade(agendamentoDTO.getPeriodicidade())
						.grupo(agendamentoDTO.getGrupo())
						.periodicidade(agendamentoDTO.getPeriodicidade())
						.tempoCargaCompleta(agendamentoDTO.getTempoCargaCompleta())
						.tempoUltimaExecucao(agendamentoDTO.getTempoUltimaExecucao())
						.tipoAgendamento(Enum.valueOf(TipoAgendamento.class, agendamentoDTO.getNome()))
						.tipoCargaCompleta(Enum.valueOf(FormatoPeriodicidade.class, agendamentoDTO.getFormatoCargaCompleta().toUpperCase()))
						.tipoPeriodicidade(Enum.valueOf(FormatoPeriodicidade.class, agendamentoDTO.getFormatoPeriodicidade().toUpperCase()))
						.build();

			agendamentos.add(em.merge(agendamento));
		}
		
		reagendarJob(agendamentos);
	}

	private void reagendarJob(final List<Agendamento> agendamentos) throws JobSchedulerException {
			jobsConfig.configJobs(JobStarter.getScheduler(),agendamentos.stream()
					.filter(agendamento -> agendamento.isAtivo()).collect(Collectors.toList()));
	}
}
