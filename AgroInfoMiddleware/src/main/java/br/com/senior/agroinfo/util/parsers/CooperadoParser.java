package br.com.senior.agroinfo.util.parsers;

import java.util.Optional;

import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.services.FornecedoresExportarOutGridFornecedores;
import br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep;

public class CooperadoParser {

    private CooperadoParser() {
    }

    public static Cooperado fromRepresentation(FornecedoresExportarOutGridFornecedores fornecedor) {
        Cooperado cooperado = new Cooperado.Builder()
        										.id(fornecedor.getCodFor().longValue())
        										.cpfCnpj(fornecedor.getCgcCpf())
        										.nome(fornecedor.getNomFor())
        										.nomeFantasia(fornecedor.getApeFor())
        										.codigoMatricula(fornecedor.getCodFor())
        										.build();
        obterCep(fornecedor, cooperado);
        cooperado.setEmail(fornecedor.getIntNet());
        cooperado.setAtivo("A".equalsIgnoreCase(fornecedor.getSitFor()));
        return cooperado;
    }

    private static void obterCep(FornecedoresExportarOutGridFornecedores fornecedor, Cooperado cooperado) {

        Optional.ofNullable(fornecedor.getCep()).ifPresent(ceps -> {

            FornecedoresExportarOutGridFornecedoresCep cep = ceps[0];

            cooperado.setEndereco(cep.getEndFor());
            cooperado.setCep(cep.getCepFor() + "");
            cooperado.setBairro(cep.getBaiFor());
            cooperado.setCidade(cep.getCidFor());
            cooperado.setNumero(cep.getNenFor());
            cooperado.setCodIbge(cep.getCodRai());
        });
    }
}