package br.com.senior.agroinfo.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senior.agroinfo.dto.CooperadoDTO;
import br.com.senior.agroinfo.exceptions.RegistroNaoEncontradoException;
import br.com.senior.agroinfo.services.FornecedorServiceResource;
import br.com.senior.agroinfo.util.DateUtil;

@Path("cooperado")
public class CooperadoResource {

	@Inject
	private FornecedorServiceResource service;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarTodos() {
		List<CooperadoDTO> dtos = service.obterDTOs();
		return Response.status(Response.Status.OK).entity(dtos).build();
	}

	@GET
	@Path("/dtupdate/{dtUpdate}")
	@Produces({ "application/json" })
	public Response buscarPorDtUpdate(@PathParam("dtUpdate") String dtUpdate) throws ParseException {
		Date data = new SimpleDateFormat("dd-MM-yyyy").parse(dtUpdate);
		data = DateUtil.zerarHHMMSSMM(data);
		List<CooperadoDTO> dtos = service.obterDTOPorDtUpdate(data);
		return Response.status(Response.Status.OK).entity(dtos).build();

	}

	@GET
	@Path("/matricula/{codigoMatricula:[0-9]*}/cpfCnpj/{cpfCnpj:[0-9][0-9]*}")
	@Produces({ "application/json" })
	public Response buscarCooperado(@PathParam("codigoMatricula") int codigoMatricula,
			@PathParam("cpfCnpj") String cpfCnpj) throws RegistroNaoEncontradoException {
		
		Optional<CooperadoDTO> optional = service.obterCooperado(codigoMatricula,cpfCnpj);
		CooperadoDTO dto = optional.orElseThrow(() -> new RegistroNaoEncontradoException("Cooperado nao encontrado."));
		return Response.ok(dto).build();
	}

	@GET
	@Path("/matricula/{matricula:[0-9]*}")
	@Produces({ "application/json" })
	public Response buscarPorCodigoMatricula(@PathParam("matricula") int codigoMatricula)
			throws ParseException, RegistroNaoEncontradoException {
		Optional<CooperadoDTO> optional = service.obterDTOPorCodigoMatricula(codigoMatricula);
		CooperadoDTO dto = optional.orElseThrow(() -> new RegistroNaoEncontradoException("Cooperado nao encontrado."));
		return Response.status(Response.Status.OK).entity(dto).build();

	}
}
