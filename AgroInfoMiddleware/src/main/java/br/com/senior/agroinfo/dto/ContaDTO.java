package br.com.senior.agroinfo.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Objects;

import com.google.common.base.Strings;

import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.ValidationException;

public class ContaDTO implements Serializable{
	
	private static final long serialVersionUID = -232978795547150546L;

	private String codTitulo;

	private String nomeCooperado;
	
	private String nomeFilial;
    
	private String nomeEmpresa;
	
	private String descricao;		
	
	private BigDecimal valor;
	
	private BigDecimal juros;
	
	private BigDecimal multa;
	
	private String dtEmissao;
	
	private String dtVencimento;
	
	private String dtUpdate;
	
	private Boolean integrado;
	
	private String dtVencimentoOriginal;
	
	private String tipoTitulo;
	
	private BigDecimal valorAberto;
	
	private Boolean contaAberta;
	
	private String tipoConta;
	
	private String numeroTitulo;
	
	private ContaDTO() {
		
	}

	public static ContaDTO of(Conta conta) throws EnumNotFoundException {
		Objects.requireNonNull(conta, "Objeto conta pagar não informado");
		ContaDTO dto = new ContaDTO();
		dto.nomeCooperado = conta.getNomeCooperado();
		dto.nomeFilial = conta.getNomeFilial();
		dto.nomeEmpresa = conta.getNomeEmpresa();
		dto.descricao = conta.getDescricao();
		dto.valor = conta.getValor();
		dto.juros = conta.getJuros();
		dto.multa = conta.getMulta();
		dto.tipoConta = resolverTipoConta(conta);
		dto.integrado = conta.getIntegrado();
		dto.tipoTitulo = conta.getDescricaoTipoTitulo();
		dto.valorAberto = conta.getValorAberto();
		dto.contaAberta = conta.isContaAberta();
		dto.numeroTitulo = conta.getNumeroTitulo();
		dto.codTitulo = conta.getTipoTitulo().getCodTipoTitulo();
		if (Objects.nonNull(conta.getDtEmissao())) {
        	dto.dtEmissao = new SimpleDateFormat("dd-MM-yyyy")
					.format(conta.getDtEmissao());
		}
		
		if (Objects.nonNull(conta.getDtVencimento())) {
        	dto.dtVencimento = new SimpleDateFormat("dd-MM-yyyy")
					.format(conta.getDtVencimento());
		}
		
		if (Objects.nonNull(conta.getDtUpdate())) {
        	dto.dtUpdate = new SimpleDateFormat("dd-MM-yyyy")
					.format(conta.getDtUpdate());
		}
		
		if (Objects.nonNull(conta.getDtVencimentoOriginal())) {
        	dto.dtVencimentoOriginal = new SimpleDateFormat("dd-MM-yyyy")
					.format(conta.getDtVencimentoOriginal());
		}		
		return dto;
	}	

	/** Método para retorno do tipo da conta

     *   Segundo descrito no pipefy:
     *   
     *   Quando conta pagar:
     *   	Caso D - é um titulo a receber
     *   	Caso C - é um credito de contas a receber o mesmo deve ser considerado uma conta a pagar 
     *   	Outros casos considerar = C.
     *   
     *   Quanto Conta a Receber:
     *      Caso D- Debito ( se ele estiver nesta forma será realmente um contas a pagar)
     *      Caso C- Credito ( Será um credito que contas pagar ou seja um valor que o cooperado tem a recer)
	 * @throws TipoContaEnumeradoNaoEncontrado 
     */
	private static String resolverTipoConta(Conta conta) throws EnumNotFoundException {
		if (conta.getTipoConta().equals(TipoConta.PAGAR)) {
			switch (conta.getTipoTitulo().getSituacao()) {	
				case SomaDuplicatas: return TipoConta.RECEBER.name();
			default:
				case SomaCredito: return TipoConta.PAGAR.name();
			}
		}
		
		if (conta.getTipoConta().equals(TipoConta.RECEBER)) {
			switch (conta.getTipoTitulo().getSituacao()) {			
				case SomaDuplicatas: return TipoConta.PAGAR.name();
			default:
				case SomaCredito: return TipoConta.RECEBER.name();
			}
		}
		throw new EnumNotFoundException("Enumerado Situacao TipoConta "+conta.getTipoTitulo().getSituacao()+" Não encontrado");
	}
	
	public String getCodTitulo() {
		return codTitulo;
	}

	public void setCodTitulo(String codTitulo) {
		this.codTitulo = codTitulo;
	}

	public String getNomeCooperado() {
		return nomeCooperado;
	}

	public void setNomeCooperado(String nomeCooperado) {
		this.nomeCooperado = nomeCooperado;
	}

	public String getNomeFilial() {
		return nomeFilial;
	}

	public void setNomeFilial(String nomeFilial) {
		this.nomeFilial = nomeFilial;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getJuros() {
		return juros;
	}

	public void setJuros(BigDecimal juros) {
		this.juros = juros;
	}

	public BigDecimal getMulta() {
		return multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public String getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(String dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public String getDtVencimento() {
		return dtVencimento;
	}

	public void setDtVencimento(String dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public String getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(String dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Boolean getIntegrado() {
		return integrado;
	}

	public void setIntegrado(Boolean integrado) {
		this.integrado = integrado;
	}

	public String getDtVencimentoOriginal() {
		return dtVencimentoOriginal;
	}

	public void setDtVencimentoOriginal(String dtVencimentoOriginal) {
		this.dtVencimentoOriginal = dtVencimentoOriginal;
	}

	public String getTipoTitulo() {
		return tipoTitulo;
	}

	public void setTipoTitulo(String tipoTitulo) {
		this.tipoTitulo = tipoTitulo;
	}

	public BigDecimal getValorAberto() {
		return valorAberto;
	}

	public void setValorAberto(BigDecimal valorAberto) {
		this.valorAberto = valorAberto;
	}

	public Boolean getContaAberta() {
		return contaAberta;
	}

	public void setContaAberta(Boolean contaAberta) {
		this.contaAberta = contaAberta;
	}

	public void validar() throws ValidationException {
		if (Strings.isNullOrEmpty(nomeCooperado)) {
			throw new ValidationException("Nome Cooperado não informado");
		}
		
		if (Strings.isNullOrEmpty(nomeFilial)) {
			throw new ValidationException("Nome Filial não informado");
		}
		
		if (Strings.isNullOrEmpty(nomeEmpresa)) {
			throw new ValidationException("Nome Empresa não informado");
		}
		
		if (Strings.isNullOrEmpty(descricao)) {
			throw new ValidationException("Descrição não informado");
		}
		
		if (Strings.isNullOrEmpty(dtEmissao)) {
			throw new ValidationException("Data emissão não informado");
		}
		
		if (Strings.isNullOrEmpty(dtVencimento)) {
			throw new ValidationException("Data vencimento não informado");
		}
		
		if (Strings.isNullOrEmpty(dtVencimentoOriginal)) {
			throw new ValidationException("Data vencimento  original não informado");
		}
		
		if (Strings.isNullOrEmpty(tipoTitulo)) {
			throw new ValidationException("tipo titulo  original não informado");
		}		
		
		if (Strings.isNullOrEmpty(dtUpdate)) {
			throw new ValidationException("Data update não informado");
		}		
		
		Objects.requireNonNull(valor, "valor não informado");
		Objects.requireNonNull(juros, "juros não informado");
		Objects.requireNonNull(multa, "multa não informado");
		if(isContaPagar()) {
			Objects.requireNonNull(valorAberto, "valor aberto informado");	
		}		
		Objects.requireNonNull(contaAberta, "conta aberta não informada");
		Objects.requireNonNull(integrado, "integrado não informado");
				
	}
	
	private Boolean isContaPagar() {
		return getTipoConta().equals(TipoConta.PAGAR.name());
	}

	public String getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getNumeroTitulo() {
		return numeroTitulo;
	}

	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}
	

}
