package br.com.senior.agroinfo.sync.services;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import br.com.senior.agroinfo.db.dao.TipoTituloDAO;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.TipoTituloWSException;
import br.com.senior.agroinfo.util.parsers.TipoTituloParser;
import br.com.senior.agroinfo.ws.client.TipoTituloClient;
import br.com.senior.services.TipotituloExportarIn;
import br.com.senior.services.TipotituloExportarInConsulta;
import br.com.senior.services.TipotituloExportarOut;

public class TipoTituloService {

	@Inject
	TipoTituloDAO tipoTituloDAO;

	public Optional<TipoTitulo> obterTipoTitulo(final String usuario, final String senha, final String url,
			final int codEmp, final Long codFilial, final String codTipoTitulo, final String identificacaoSistema,
			TipoConta tipoConta) throws EnumNotFoundException {

		TipotituloExportarIn in = new TipotituloExportarIn();
		in.setCodEmp(codEmp);
		in.setCodFil(codFilial);
		in.setIdentificacaoSistema(identificacaoSistema);
		in.setTipoIntegracao(TipoIntegracao.ESPECIFICO.getSigla());

		TipotituloExportarInConsulta[] consulta = { new TipotituloExportarInConsulta(codTipoTitulo) };
		in.setConsulta(consulta);

		Optional<TipoTitulo> tipoTitulo = Optional.empty();
		try {
			TipotituloExportarOut titulo = TipoTituloClient.buscarTipoTitulo(usuario, senha, url, in);
			tipoTitulo = Optional.ofNullable(persistirTipoTitulo(titulo, tipoConta));
		} catch (TipoTituloWSException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
		}
		return tipoTitulo;
	}

	private TipoTitulo persistirTipoTitulo(final TipotituloExportarOut titulo, TipoConta tipoConta) throws EnumNotFoundException {
		TipoTitulo tipoTitulo = TipoTituloParser.fromRepresentation(titulo, tipoConta);
		return tipoTituloDAO.adiciona(tipoTitulo);
	}
}