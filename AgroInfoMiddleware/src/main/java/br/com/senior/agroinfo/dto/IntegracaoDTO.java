package br.com.senior.agroinfo.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.google.common.collect.Lists;

import br.com.senior.agroinfo.db.modal.Configuracoes;

public class IntegracaoDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
    private long id;
    
    private String tiposistema;
    
    private String tipoSistemaSMS;
    
    private String endereco;
    
    private String porta;
    
    private String usuario;
    
    private String senha;
    
    private String sigla;
    
    private String ipsms;
    
    private boolean utilizasms;
    
    private String portasms;
    
    private String tiposms;
    
    private String usuariosms;
    
    private String senhasms;
    
    private String dtUpdate;
    
    private List<EmpresaDTO> empresas = Lists.newArrayList();
    
    public IntegracaoDTO(){
    	
    }
    
    public List<EmpresaDTO> getEmpresas() {
    	return this.empresas;
    }
    
    public long getId() {
		return id;
	}

	public String getTiposistema() {
		return tiposistema;
	}

	public String getEndereco() {
		return endereco;
	}

	public String getPorta() {
		return porta;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getSenha() {
		return senha;
	}

	public String getSigla() {
		return sigla;
	}

	public String getIpsms() {
		return ipsms;
	}

	public boolean isUtilizasms() {
		return utilizasms;
	}

	public String getPortasms() {
		return portasms;
	}

	public String getTiposms() {
		return tiposms;
	}

	public String getUsuariosms() {
		return usuariosms;
	}

	public String getSenhasms() {
		return senhasms;
	}

	public String getDtUpdate() {
		return dtUpdate;
	}
    
    public static IntegracaoDTO of(Optional<Configuracoes> configuracoes, List<EmpresaDTO> empresas) {
		IntegracaoDTO dto = new IntegracaoDTO();
		configuracoes.ifPresent(c -> {
	    	dto.id = c.getId();
	    	dto.tiposistema = c.getTiposistema().getDescricao();
	    	dto.endereco = c.getEndereco();
	    	dto.porta = c.getPorta();
	    	dto.usuario = c.getUsuario();
	    	dto.senha = c.getSenha();
	    	dto.sigla = c.getSigla();
	    	dto.ipsms = c.getIpsms();
	    	dto.utilizasms = c.isUtilizasms();
	    	dto.portasms = c.getPortasms();
	    	dto.tiposms = c.getTiposms().getDescricao();
	    	dto.senhasms = c.getSenhasms();
	    	dto.usuariosms = c.getUsuariosms();
	    	dto.tipoSistemaSMS = c.getTiposms().getDescricao();
	    	if(Objects.nonNull(c.getDtUpdate())) {
	        	dto.dtUpdate = new SimpleDateFormat("dd-MM-yyyy")
	        								.format(c.getDtUpdate());    		
	    	}
	    	dto.empresas = empresas;
		});
		return dto;
    }
    
    public void addEmpresa(EmpresaDTO empresaDTO) {
    	empresas.add(empresaDTO);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegracaoDTO other = (IntegracaoDTO) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getTipoSistemaSMS() {
		return tipoSistemaSMS;
	}

	public void setTipoSistemaSMS(String tipoSistemaSMS) {
		this.tipoSistemaSMS = tipoSistemaSMS;
	}
    
    

}
