package br.com.senior.agroinfo.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senior.agroinfo.dto.AgendamentoDTO;
import br.com.senior.agroinfo.services.AgendamentoService;

@Path("agendamento")
public class ConfiguracaoAgendamentoResource {
	
	@Inject
	private AgendamentoService agendamentoService;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar() {
    	List<AgendamentoDTO> dtos = agendamentoService.obterDTOs();
	        return Response
	                .status(Response.Status.OK)
	                .entity(dtos)
	                .build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(List<AgendamentoDTO> agendamentos) throws Exception {
		agendamentoService.salvarAgendamentos(agendamentos);
	    return Response
    				.status(Response.Status.OK)
    				.build();

    }	
    
}
