package br.com.senior.agroinfo.dto;

import java.io.Serializable;

public class ErroDTO implements Serializable {
	
	private static final long serialVersionUID = 1165475162456547426L;
	
	private String tituloErro;
	
	private String mensagemErro;
	
	ErroDTO(){	
		
	}
	
    public static ErroDTO create(String titulo, String mensagem) {
    	ErroDTO e = new ErroDTO();
    	e.tituloErro = titulo;
    	e.mensagemErro = mensagem;
    	return e;
    } 	

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagem) {
        this.mensagemErro = mensagem;
    }
    
    public String getTituloErro() {
		return tituloErro;
	}

	public void setTituloErro(String titulo) {
		this.tituloErro = titulo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mensagemErro == null) ? 0 : mensagemErro.hashCode());
		result = prime * result + ((tituloErro == null) ? 0 : tituloErro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErroDTO other = (ErroDTO) obj;
		if (mensagemErro == null) {
			if (other.mensagemErro != null)
				return false;
		} else if (!mensagemErro.equals(other.mensagemErro))
			return false;
		if (tituloErro == null) {
			if (other.tituloErro != null)
				return false;
		} else if (!tituloErro.equals(other.tituloErro))
			return false;
		return true;
	}

	

}
