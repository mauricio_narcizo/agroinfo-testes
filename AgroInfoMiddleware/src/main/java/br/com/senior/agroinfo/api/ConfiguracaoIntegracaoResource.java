package br.com.senior.agroinfo.api;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senior.agroinfo.dto.IntegracaoDTO;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.services.ConfiguracoesService;

@Path("integracao")
public class ConfiguracaoIntegracaoResource {
	
	@Inject 
	private ConfiguracoesService service;
	
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar() {
    	IntegracaoDTO dto = service.obterDTO();
	        return Response
	                .status(Response.Status.OK)
	                .entity(dto)
	                .build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response salvar(IntegracaoDTO integracaoDTO) throws EnumNotFoundException {
		service.salvarConfiguracaoIntegracao(integracaoDTO);
		IntegracaoDTO dto = service.obterDTO();
    	return Response	    			
				.status(Response.Status.OK)
				.entity(dto)
				.build();
    }	
    
    

}
