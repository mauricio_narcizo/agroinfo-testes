package br.com.senior.agroinfo.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.senior.agroinfo.dto.ContaDTO;
import br.com.senior.agroinfo.exception.BaseException;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.ValidationException;
import br.com.senior.agroinfo.services.ContaService;

@Path("conta")
public class ContaResource {
	
	@Inject
	private ContaService service;
	
	@GET
    @Path("/cooperado/{id}")
    @Produces(MediaType.APPLICATION_JSON)
	public Response buscarContaCooperado(@PathParam("id") long id,@QueryParam("dtInicio") String dtInicio, @QueryParam("dtFim") String dtFim) throws BaseException {
		Date dataInicio = null;
		Date dataFim = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			dataInicio = sdf.parse(dtInicio);
			dataFim = sdf.parse(dtFim);
		} catch (Exception e) {
			Logger.getLogger(this.getClass().getSimpleName()).log(Level.SEVERE, e.getMessage());
			dataInicio = null;
			dataFim = null;
		}
		try {
			List<ContaDTO> dto = service.obterContaCooperadoDTO(id, dataInicio, dataFim);

	        return Response
	                .status(Response.Status.OK)
	                .entity(dto)
	                .build();

		} catch (NumberFormatException e) {
			throw new ValidationException("Id deve ser um número.");
		}
    }
	
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar(@PathParam("id") String id) throws BaseException {    	
		try {
			ContaDTO dto = service.obterDTO(id);

	        return Response
	                .status(Response.Status.OK)
	                .entity(dto)
	                .build();

		} catch (NumberFormatException e) {
			throw new ValidationException("Id deve ser um número.");
		}
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar() throws EnumNotFoundException {
    	List<ContaDTO> dtos;
		dtos = service.obterDTOs();
        return Response
                .status(Response.Status.OK)
                .entity(dtos)
                .build();			
    }

}
