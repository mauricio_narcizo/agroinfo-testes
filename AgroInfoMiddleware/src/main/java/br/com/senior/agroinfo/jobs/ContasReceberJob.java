package br.com.senior.agroinfo.jobs;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.dao.TipoTituloDAO;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.sync.services.ContasReceberService;

public class ContasReceberJob implements Job {

	@Inject
	CooperadoDAO cooperadoDao;

	@Inject
	ContasReceberService contasReceberService;

	@Inject
	TipoTituloDAO tipoTituloDAO;

	@SuppressWarnings("unchecked")
	@Override
	public synchronized void execute(JobExecutionContext job) throws JobExecutionException {
		final Configuracoes configuracao = (Configuracoes) job.getJobDetail().getJobDataMap().get("configuracoes");
		final List<EmpresaFilial> filiais = (List<EmpresaFilial>) job.getJobDetail().getJobDataMap().get("filiais");
		final TipoIntegracao tipoIntegracao = (TipoIntegracao) job.getTrigger().getJobDataMap().get("tipoIntegracao");

		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Tipo Integracao: " + tipoIntegracao);

		List<TipoTitulo> tipoTitulos = (List<TipoTitulo>) tipoTituloDAO.getLista();
		filiais.stream().forEach(filial -> {
			try {
				sincronizarCooperadosFilial(filial, configuracao, tipoTitulos, configuracao.getSigla(), tipoIntegracao);
			} catch (Exception e) {
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
			}
		});
	}

	private void sincronizarCooperadosFilial(final EmpresaFilial filial, final Configuracoes configuracao,
			List<TipoTitulo> tipoTitulos, final String identificaoSistema, TipoIntegracao tipoIntegracao)
					throws Exception {

		final String url = "http://"+configuracao.getEndereco() + ":" + configuracao.getPorta();
		contasReceberService.sincronizarContasReceber(configuracao.getUsuario(), configuracao.getSenha(), url,
				filial.getCodigoEmpresa(), filial.getId(), tipoTitulos, identificaoSistema,
				tipoIntegracao.getSigla());
	}

}
