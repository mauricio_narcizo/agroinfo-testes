package br.com.senior.agroinfo.jobs;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.sync.services.CooperadoService;

@Dependent
public class CooperadoJob implements Job {

    @Inject
    CooperadoService cooperadoService;

    @SuppressWarnings("unchecked")
    @Override
    public void execute(final JobExecutionContext job) throws JobExecutionException {

        final Configuracoes configuracao = (Configuracoes) job.getJobDetail().getJobDataMap().get("configuracoes");
        final List<EmpresaFilial> filiais = (List<EmpresaFilial>) job.getJobDetail().getJobDataMap().get("filiais");
        final TipoIntegracao tipoIntegracao = (TipoIntegracao) job.getTrigger().getJobDataMap().get("tipoIntegracao");
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Tipo Integracao: "+ tipoIntegracao);
        filiais.stream().forEach(filial -> {
            try {
                sincronizarCooperadosFilial(filial, configuracao, tipoIntegracao);
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        });
    }

    private synchronized void sincronizarCooperadosFilial(final EmpresaFilial filial, final Configuracoes configuracao,
            TipoIntegracao tipoIntegracao) throws Exception {

        final String url = "http://"+configuracao.getEndereco() + ":" + configuracao.getPorta();
        final int qtdRegistros = 500;

        cooperadoService.atualizarCooperados(configuracao.getUsuario(), configuracao.getSenha(), url,
                filial.getCodigoEmpresa(), filial.getId(), configuracao.getSigla(), qtdRegistros,
                tipoIntegracao.getSigla(), filial.getDddPadrao());
    }

}
