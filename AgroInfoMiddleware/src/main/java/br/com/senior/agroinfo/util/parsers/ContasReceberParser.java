package br.com.senior.agroinfo.util.parsers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.services.TitulosExportarTitulosReceberOutTitulosReceber;

public class ContasReceberParser {
	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	private ContasReceberParser() {
	}

	public static Optional<Conta> fromRepresentation(TitulosExportarTitulosReceberOutTitulosReceber titulos,
			Cooperado cooperado, TipoTitulo tipo) {
		Optional<Conta> contasOptional = Optional.empty();
		try {
			Conta contas = new Conta.Builder(TipoConta.RECEBER)
										.codTitulo(titulos.getCodTpt())
										.cooperado(cooperado)
										.numeroTitulo(titulos.getNumTit())
										.dtEmissao(SIMPLE_DATE_FORMAT.parse(titulos.getDatEmi()))
										.dtVencimentoOriginal(SIMPLE_DATE_FORMAT.parse(titulos.getVctOri()))
										.juros(BigDecimal.valueOf(titulos.getJrsNeg()))
										.multa(BigDecimal.valueOf(titulos.getMulNeg()))
										.valor(BigDecimal.valueOf(titulos.getVlrAbe()))
										.tipoTitulo(tipo)
										.descricao(titulos.getObsTcr())
										.contaAberta(BigDecimal.valueOf(titulos.getVlrAbe()).compareTo(BigDecimal.ZERO) == 1)
										.build();

			contasOptional = Optional.of(contas);
		} catch (ParseException | NullPointerException | IllegalArgumentException e) {
			Logger.getLogger("ContasPagarParser").log(Level.SEVERE, e.getMessage(), e);
		}

		return contasOptional;
	}
}
