package br.com.senior.agroinfo.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.senior.agroinfo.dto.ErroDTO;

@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception>{

	@Override
	public Response toResponse(Exception e) {
   		ErroDTO erro = ErroDTO.create("Erro Inesperado: ", e.getMessage());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(erro)
                .build();
	}

}
