package br.com.senior.agroinfo.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {
	
	
	public static int monthSpan(Date d1, Date d2) {
        int meses = 0;  
        boolean dataIniMaior = false;  
        GregorianCalendar gc1, gc2;  
           
        if (d2.after(d1)){  
            gc2 = (GregorianCalendar) d2.clone();  
            gc1 = (GregorianCalendar) d1.clone();  
        }  
        else{  
            dataIniMaior = true;  
            gc2 = (GregorianCalendar) d1.clone();  
            gc1 = (GregorianCalendar) d2.clone();  
        }  
          
        gc1.clear(Calendar.MILLISECOND);  
        gc1.clear(Calendar.SECOND);  
        gc1.clear(Calendar.MINUTE);  
        gc1.clear(Calendar.HOUR_OF_DAY);  
        gc1.clear(Calendar.DATE);  
        gc2.clear(Calendar.MILLISECOND);  
        gc2.clear(Calendar.SECOND);  
        gc2.clear(Calendar.MINUTE);  
        gc2.clear(Calendar.HOUR_OF_DAY);  
        gc2.clear(Calendar.DATE);  
          
        while(gc1.before(gc2)){  
            gc1.add(Calendar.MONTH, 1);  
            meses = dataIniMaior? --meses: ++meses;  
        }  
          
        return (meses==0 || meses < 0)? meses: meses - 1;  
    }  	
	
	public static long minuteSpan(Date d1, Date d2) {
		return (d2.getTime() - d1.getTime()) / 60000;  
	}
	
	public static long daySpan(Date d1, Date d2) {
		return (d2.getTime() - d1.getTime()) / 86400000;  
	}
	
	public static Date setDDMMAAAA(Date origem, Date destino) {
		Calendar c = Calendar.getInstance();
		c.setTime(origem);
		
		Calendar cDestino = Calendar.getInstance();
		cDestino.setTime(destino);
		cDestino.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH));
		cDestino.set(Calendar.MONTH, c.get(Calendar.MONTH));
		cDestino.set(Calendar.YEAR, c.get(Calendar.YEAR));
		
		return cDestino.getTime();
		
	}
	
	public static Date getDataComUltimoMilesegundoDoDia(Date data) {
		if(data != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(data);
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);
			c.set(Calendar.MILLISECOND, 999);
			return c.getTime();
		}
		return null;
	}
	
	public static Date subtractDayFromDate(Date data, int dias) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) - dias);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		return c.getTime();
		
	}
	
	public static Date zerarHHMMSSMM(Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);		
		return c.getTime();
	}
	
	public static Date getPrimeiroDiaMes(Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY,0);
		return c.getTime();
		
	}
	
	public static Date getPrimeiroDiaMesComHoraZero(Date data) {		
		return zerarHHMMSSMM(getPrimeiroDiaMes(data));
	}	
	
	public static Date getUltimoDiaMes(Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY,23);
		return c.getTime();
	}


	public static Date addDay(Date data, int i) {
		Calendar c = Calendar.getInstance();		
		c.setTime(data);
		c.add(Calendar.DAY_OF_MONTH, i);
		return c.getTime();

	}
	
	public static Date addMonth(Date data, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.add(Calendar.MONTH, i);
		return c.getTime();
	}
	
	public static Date addYear(Date data, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.add(Calendar.YEAR, i);
		return c.getTime();
	}	
	
	public static Date addMinute(Date data, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		c.add(Calendar.MINUTE, i);
		return c.getTime();
	}
	
	public static Date subMinute(Date data, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);		
		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) - i);
		return c.getTime();
	}	
	
	
	public static int getDiaSemana(Date data) {
		Calendar c = Calendar.getInstance();
		c.setTime(data);
		return c.get(Calendar.DAY_OF_WEEK);		
	}	
	
	public static Date nvl(Date data1, Date data2) {
		if (data1 != null && data2 == null)
			return data1;
		if (data1 == null && data2 != null)
			return data2;
		return null;
	}
	
	public static Date max(Date data1, Date data2) {
		if (data1 == null || data2 == null)
			return nvl(data1, data2);
		if (data1.after(data2))
			return data1;
		return data2;
	}

	public static Date min(Date data1, Date data2) {
		if (data1 == null || data2 == null)
			return nvl(data1, data2);		
		if (data1.before(data2))
			return data1;
		return data2;
	}

	public static Date getUltimoDiaMesComUltimaHora(Date date) {
		return getDataComUltimoMilesegundoDoDia(getUltimoDiaMes(date));
	}	

}
