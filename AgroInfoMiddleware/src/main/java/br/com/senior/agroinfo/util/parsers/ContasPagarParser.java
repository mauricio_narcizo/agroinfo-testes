package br.com.senior.agroinfo.util.parsers;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.services.TitulosExportarTitulosCPOutTitulos;

public class ContasPagarParser {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	private ContasPagarParser() {
	}

	public static Optional<Conta> fromRepresentation(TitulosExportarTitulosCPOutTitulos titulo,
			Cooperado cooperado, TipoTitulo tipo, EmpresaFilial empresaFilial) {
		Optional<Conta> contasOptional = Optional.empty();
		try {
			Conta contas = new Conta.Builder(TipoConta.PAGAR)
				.codTitulo(titulo.getCodTpt())
				.numeroTitulo(titulo.getNumTit())
				.cooperado(cooperado)
				.empresaFilial(empresaFilial)
				.dtEmissao(SIMPLE_DATE_FORMAT.parse(titulo.getDatEmi()))
				.dtVencimentoOriginal(SIMPLE_DATE_FORMAT.parse(titulo.getVctOri()))
				.juros(BigDecimal.valueOf(titulo.getJrsNeg()))
				.multa(BigDecimal.valueOf(titulo.getMulNeg()))
				.valor(BigDecimal.valueOf(titulo.getVlrAbe()))
				.tipoTitulo(tipo)
				.descricao(titulo.getObsTcp())
				.numeroTitulo(titulo.getNumTit())
				.contaAberta(BigDecimal.valueOf(titulo.getVlrAbe()).compareTo(BigDecimal.ZERO) == 1)
				.build();
			contasOptional = Optional.of(contas);
		} catch (Exception e) {
			Logger.getLogger("ContasPagarParser").log(Level.SEVERE, e.getMessage());
		}

		return contasOptional;
	}

}
