package br.com.senior.agroinfo.jobs.starter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.enums.FormatoPeriodicidadeScheduler;
import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.jobs.ContasReceberJob;

public class ContasReceberJobStarter extends JobSchedulerStarter {

	private static final long serialVersionUID = 1L;

	private static JobSchedulerStarter scheduler = new ContasReceberJobStarter();

	public static final String CARGA_PARCIAL = "triggerCargaContasReceber";
	public static final String CARGA_COMPLETA = "triggerCompletaContasReceber";

	private ContasReceberJobStarter() {
	}

	public static JobSchedulerStarter getInstance() {
		return scheduler;
	}

	@Override
	public void initScheduleJob(Scheduler scheduler, Agendamento agendamento, Configuracoes configuracoes,
			List<EmpresaFilial> filiais) throws SchedulerException {

		JobDetail jobDetail = JobBuilder.newJob(ContasReceberJob.class).build();
		jobDetail.getJobDataMap().put("configuracoes", configuracoes);
		jobDetail.getJobDataMap().put("filiais", filiais);

		Set<SimpleTrigger> triggers = obterTriggers(agendamento);

		scheduler.scheduleJob(jobDetail, triggers, true);
	}

	@Override
	public void unscheduleJob(final Scheduler scheduler) throws SchedulerException {
		scheduler.unscheduleJob(new TriggerKey(CARGA_PARCIAL));
		scheduler.unscheduleJob(new TriggerKey(CARGA_COMPLETA));
	}

	private static Set<SimpleTrigger> obterTriggers(Agendamento agendamento) {

		Set<SimpleTrigger> triggers = new HashSet<>();
		triggers.add(obterTriggerCargaParcial(agendamento));
		triggers.add(obterTriggerCargaCompleta(agendamento));
		return triggers;
	}

	private static SimpleTrigger obterTriggerCargaParcial(Agendamento agendamento) {

		SimpleTrigger triggerCargaParcial = TriggerBuilder
				.newTrigger().withSchedule(FormatoPeriodicidadeScheduler
						.obterSchedulerBuilder(agendamento.getTipoPeriodicidade(), agendamento.getPeriodicidade()))
				.withIdentity(CARGA_PARCIAL).build();
		triggerCargaParcial.getJobDataMap().put("tipoIntegracao", TipoIntegracao.ALTERADOS);
		return triggerCargaParcial;
	}

	private static SimpleTrigger obterTriggerCargaCompleta(Agendamento agendamento) {

		SimpleTrigger triggerCargaParcial = TriggerBuilder
				.newTrigger().withSchedule(FormatoPeriodicidadeScheduler
						.obterSchedulerBuilder(agendamento.getTipoCargaCompleta(), agendamento.getCargaCompleta()))
				.withIdentity(CARGA_COMPLETA).build();
		triggerCargaParcial.getJobDataMap().put("tipoIntegracao", TipoIntegracao.TODOS);
		return triggerCargaParcial;
	}

}
