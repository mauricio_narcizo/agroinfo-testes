package br.com.senior.agroinfo.util.parsers;

import java.util.ArrayList;
import java.util.List;

import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.sms.utils.Telefone;
import br.com.senior.services.FornecedoresExportarOutGridFornecedores;

public class ContatoParser {

    public static List<Contato> fromRepresentation(FornecedoresExportarOutGridFornecedores fornecedor, Cooperado cooperado) {

        String[] fones = { fornecedor.getFonFor(), fornecedor.getFonFo2(), fornecedor.getFonFo3(),
                fornecedor.getFaxFor() };

        List<Contato> contatos = new ArrayList<>();
        for (String fone : fones) {
            if (fone == null || fone.trim().isEmpty()) {
                continue;
            }
            Telefone telefone = new Telefone(fone);
            Contato contato = new Contato();
            contato.setCooperado(cooperado);
            contato.setDdd(telefone.getDdd());
            contato.setTelefone(telefone.getNumero());
            contatos.add(contato);
        }

        return contatos;
    }
}
