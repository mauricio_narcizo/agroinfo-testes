package br.com.senior.agroinfo.dto;

import java.io.Serializable;

public class FilialDTO implements Serializable{

	private static final long serialVersionUID = -712056867267757439L;
	
	private Long idFilial;
	
	private String descricaoFilial;
	
	private boolean ativo;
	
	public FilialDTO(){
		
	}
	
	public FilialDTO(Long idFilial, String descricaoFilial, boolean ativo) {
		this.idFilial = idFilial;
		this.descricaoFilial = descricaoFilial;
		this.ativo = ativo;
	}

	public String getDescricaoFilial() {
		return descricaoFilial;
	}

	public void setDescricaoFilial(String descricaoFilial) {
		this.descricaoFilial = descricaoFilial;
	}

	public Long getIdFilial() {
		return idFilial;
	}

	public void setIdFilial(Long idFilial) {
		this.idFilial = idFilial;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idFilial == null) ? 0 : idFilial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilialDTO other = (FilialDTO) obj;
		if (idFilial == null) {
			if (other.idFilial != null)
				return false;
		} else if (!idFilial.equals(other.idFilial))
			return false;
		return true;
	}

}
