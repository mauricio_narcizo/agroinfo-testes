package br.com.senior.agroinfo.util.parsers;

import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.services.TipotituloExportarOut;

public class TipoTituloParser {

	public static TipoTitulo fromRepresentation(TipotituloExportarOut titulo, TipoConta tipoConta) throws EnumNotFoundException {

		TipoTitulo tipoTitulo = new TipoTitulo.Builder()
												.codTipoTitulo(titulo.getTipoTitulo(0).getCodTpt())
												.abreviacaoTipoTitulo(titulo.getTipoTitulo(0).getAbrTpt())
												.descricaoTitulo(titulo.getTipoTitulo(0).getDesTpt())
												.build();		
		
		if (tipoConta.equals(TipoConta.PAGAR)) {
			tipoTitulo.setSituacao(SituacaoTipoTitulo.obtemTipoContaPelaSigla(titulo.getTipoTitulo(0).getPagSom()));
		} else {
			tipoTitulo.setSituacao(SituacaoTipoTitulo.obtemTipoContaPelaSigla(titulo.getTipoTitulo(0).getRecSom()));
		}
		return tipoTitulo;
	}

}
