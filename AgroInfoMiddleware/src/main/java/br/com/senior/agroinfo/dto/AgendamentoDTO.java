package br.com.senior.agroinfo.dto;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Objects;

import br.com.senior.agroinfo.db.modal.Agendamento;

public class AgendamentoDTO implements Serializable{

	
	private static final long serialVersionUID = 7930884965638920343L;

	private long id;
    
    private String nome;
    
    private String descricao;
    
    private Integer periodicidade;
    
    private Long tempoUltimaExecucao;
    
    private Integer cargaCompleta;
    
    private boolean ativo;
    
    private boolean executando;
    
    private String grupo;
    
    private String dtUpdate;
    
    private String formatoPeriodicidade;
    
    private String formatoCargaCompleta;
    
    private Integer tempoCargaCompleta;
    
	AgendamentoDTO() {	
		
	}
    
    public static AgendamentoDTO create(Agendamento agendamento) {
    	AgendamentoDTO dto = new AgendamentoDTO();
    	dto.id = agendamento.getId();
    	dto.nome = agendamento.getTipoAgendamento().name();
    	dto.descricao = agendamento.getTipoAgendamento().getDescricao();
    	dto.periodicidade = agendamento.getPeriodicidade();
    	dto.tempoUltimaExecucao = agendamento.getTempoUltimaExecucao();
    	dto.tempoCargaCompleta = agendamento.getTempoCargaCompleta();
    	dto.cargaCompleta = agendamento.getCargaCompleta();
    	dto.ativo = agendamento.isAtivo();
    	dto.executando = agendamento.isExecutando();
    	dto.grupo = agendamento.getGrupo();
    	if(Objects.nonNull(agendamento.getDtUpdate())) {
        	dto.dtUpdate = new SimpleDateFormat("dd-MM-yyyy")
        								.format(agendamento.getDtUpdate());    		
    	}
    	dto.formatoPeriodicidade = agendamento.getTipoPeriodicidade().getDescricao();
    	dto.formatoCargaCompleta = agendamento.getTipoCargaCompleta().getDescricao();
    	return dto;
    }    
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getPeriodicidade() {
		return periodicidade;
	}

	public void setPeriodicidade(Integer periodicidade) {
		this.periodicidade = periodicidade;
	}

	public Long getTempoUltimaExecucao() {
		return tempoUltimaExecucao;
	}

	public void setTempoUltimaExecucao(Long tempoUltimaExecucao) {
		this.tempoUltimaExecucao = tempoUltimaExecucao;
	}

	public Integer getCargaCompleta() {
		return cargaCompleta;
	}

	public void setCargaCompleta(Integer cargaCompleta) {
		this.cargaCompleta = cargaCompleta;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isExecutando() {
		return executando;
	}

	public void setExecutando(boolean executando) {
		this.executando = executando;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(String dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public String getFormatoPeriodicidade() {
		return formatoPeriodicidade;
	}

	public void setFormatoPeriodicidade(String formatoPeriodicidade) {
		this.formatoPeriodicidade = formatoPeriodicidade;
	}

	public String getFormatoCargaCompleta() {
		return formatoCargaCompleta;
	}

	public void setFormatoCargaCompleta(String formatoCargaCompleta) {
		this.formatoCargaCompleta = formatoCargaCompleta;
	}

	public Integer getTempoCargaCompleta() {
		return tempoCargaCompleta;
	}

	public void setTempoCargaCompleta(Integer tempoCargaCompleta) {
		this.tempoCargaCompleta = tempoCargaCompleta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgendamentoDTO other = (AgendamentoDTO) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
