package br.com.senior.agroinfo.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.senior.agroinfo.dto.ErroDTO;
import br.com.senior.agroinfo.exception.BaseException;

@Provider
public class BaseExceptionMapper implements ExceptionMapper<BaseException>{

	@Override
	public Response toResponse(BaseException e) {
   		ErroDTO erro = ErroDTO.create("Erro: ", e.getMessage());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(erro)
                .build();
	}

}
