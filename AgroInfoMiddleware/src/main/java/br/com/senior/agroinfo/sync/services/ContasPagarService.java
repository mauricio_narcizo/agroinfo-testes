package br.com.senior.agroinfo.sync.services;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import br.com.senior.agroinfo.db.dao.ContaDAO;
import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.TitulosNotFoundException;
import br.com.senior.agroinfo.util.parsers.ContasPagarParser;
import br.com.senior.agroinfo.ws.client.ContasPagarClient;
import br.com.senior.services.TitulosExportarTitulosCPIn;
import br.com.senior.services.TitulosExportarTitulosCPInConsulta;
import br.com.senior.services.TitulosExportarTitulosCPOut;
import br.com.senior.services.TitulosExportarTitulosCPOutTitulos;

public class ContasPagarService {

	@Inject
	ContaDAO contasPagarDAO;

	@Inject
	CooperadoDAO cooperadoDAO;

	@Inject
	TipoTituloService tipoTituloService;

	public synchronized void sincronizarContasPagar(final String usuario, final String senha, final String url, final EmpresaFilial empresaFilial, 
			List<TipoTitulo> tipoTitulos, final String identificacaoSistema,
			final String tipoIntegracao) {

		List<Cooperado> cooperadosAtivos = cooperadoDAO.getCooperadosAtivos();
		cooperadosAtivos.forEach(cooperado -> {
			sincronizarContasPagarCooperado(usuario, senha, url, empresaFilial, tipoTitulos, identificacaoSistema,
					tipoIntegracao, cooperado);
		});
	}

	private void sincronizarContasPagarCooperado(final String usuario, final String senha, final String url,
			final EmpresaFilial empresaFilial, List<TipoTitulo> tipoTitulos, final String identificacaoSistema,
			final String tipoIntegracao, Cooperado cooperado) {

		boolean finalizouIntegracaoRegistros = false;
		do {
			Optional<TitulosExportarTitulosCPOut> titulosOut = Optional.empty();
			try {
				titulosOut = ContasPagarClient.buscarContasPagar(usuario, senha, url, obterParamConsultaContasPagar(
						empresaFilial, cooperado.getId().intValue(), identificacaoSistema, tipoIntegracao));

				persistirContasPagar(usuario, senha, url, empresaFilial, tipoTitulos, identificacaoSistema,
						cooperado, titulosOut.get());

			} catch (Exception e) {
				// Não é necessário logar o erro, por não ter encontrado titulos
				// para o cooperado
				if (!(e instanceof TitulosNotFoundException)) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
				}
			}

			if (titulosOut.isPresent()) {
				finalizouIntegracaoRegistros = "S".equalsIgnoreCase(titulosOut.get().getFinalizaramRegistros());
			} else {
				finalizouIntegracaoRegistros = true;
			}
		} while (!finalizouIntegracaoRegistros);
	}

	private void persistirContasPagar(final String usuario, final String senha, final String url, final EmpresaFilial empresaFilial, 
			List<TipoTitulo> tipoTitulos, final String identificacaoSistema, Cooperado cooperado,
			TitulosExportarTitulosCPOut titulosOut) throws EnumNotFoundException {

		for (TitulosExportarTitulosCPOutTitulos titulos : titulosOut.getTitulos()) {

			Optional<TipoTitulo> tipoTitulo = obterTipoTitulo(usuario, senha, url, empresaFilial, tipoTitulos,
					titulos.getCodTpt(), identificacaoSistema, TipoConta.PAGAR);

			tipoTitulo.ifPresent(tipo -> {
				Optional<Conta> representation = ContasPagarParser.fromRepresentation(titulos, cooperado, tipo, empresaFilial);
				representation.ifPresent(contaPagar -> {
					contasPagarDAO.create(contaPagar);
				});
			});
		}
	}

	private TitulosExportarTitulosCPIn obterParamConsultaContasPagar(final EmpresaFilial empresaFilial,
			final int codFornecedor, final String identificacaoSistema, final String tipoIntegracao) {
		TitulosExportarTitulosCPIn titulos = new TitulosExportarTitulosCPIn();
		titulos.setCodEmp(empresaFilial.getCodigoEmpresa());
		titulos.setCodFil(empresaFilial.getId());
		titulos.setSistemaIntegracao(identificacaoSistema);

		TitulosExportarTitulosCPInConsulta[] consultas = {
				new TitulosExportarTitulosCPInConsulta(codFornecedor, null, null) };

		titulos.setConsulta(consultas);
		titulos.setQuantidadeRegistros(1000);
		titulos.setTipoIntegracao(tipoIntegracao);

		return titulos;
	}

	private Optional<TipoTitulo> obterTipoTitulo(final String usuario, final String senha, final String url,
			final EmpresaFilial empresaFilial, final List<TipoTitulo> tipoTitulos, final String codTipoTitulo,
			final String identificacaoSistema, TipoConta tipoConta) throws EnumNotFoundException {

		Optional<TipoTitulo> tipoTituloOpt = tipoTitulos.stream()
				.filter(tipoTitulo -> tipoTitulo.getCodTipoTitulo().equals(codTipoTitulo)).findFirst();

		if (!tipoTituloOpt.isPresent()) {
			tipoTituloOpt = tipoTituloService.obterTipoTitulo(usuario, senha, url, empresaFilial.getCodigoEmpresa(), empresaFilial.getId(), codTipoTitulo,
					identificacaoSistema, tipoConta);
			tipoTituloOpt.ifPresent(tipoTitulo -> {
				tipoTitulos.add(tipoTitulo);
			});
		}
		return tipoTituloOpt;
	}
}