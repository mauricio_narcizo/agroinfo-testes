package br.com.senior.agroinfo.dto;

import java.io.Serializable;

import br.com.senior.agroinfo.db.modal.EmpresaFilial;

public class EmpresaFilialDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1869718420926951595L;

	private Long id;
	
	private String nome;
	
	private boolean ativo;
	
	EmpresaFilialDTO(){
		
	}
	
	public static EmpresaFilialDTO of(EmpresaFilial filial) {
		EmpresaFilialDTO dto = new EmpresaFilialDTO();
		dto.id = filial.getId();
		dto.nome = filial.getNomeEmpresa();
		dto.ativo = filial.isAtiva();
		return dto;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaFilialDTO other = (EmpresaFilialDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
