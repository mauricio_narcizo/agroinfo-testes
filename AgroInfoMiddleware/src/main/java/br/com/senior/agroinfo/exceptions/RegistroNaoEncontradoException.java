package br.com.senior.agroinfo.exceptions;

import br.com.senior.agroinfo.exception.BaseException;

public class RegistroNaoEncontradoException extends BaseException{

	private static final long serialVersionUID = 1L;
	
	public RegistroNaoEncontradoException(String msg) {
		super(msg);
	}	

}
