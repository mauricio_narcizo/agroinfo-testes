package br.com.senior.agroinfo.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.dto.CooperadoDTO;

@Stateless
public class FornecedorServiceResource implements Serializable {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private transient EntityManager em;

	@Inject
	private transient CooperadoDAO dao;

	public List<Cooperado> obterCooperados() {
		return dao.getLista();
	}

	public List<CooperadoDTO> obterDTOs() {
		List<CooperadoDTO> dtos = new ArrayList<>();
		obterCooperados().stream().forEach(cooperado -> {
			CooperadoDTO dto = CooperadoDTO.create(cooperado);
			dtos.add(dto);
		});
		return dtos;
	}

	public Optional<CooperadoDTO> obterDTO(Long idCooperado) {
		Optional<Cooperado> optional = dao.busca(idCooperado);
		if (optional.isPresent()) {
			return Optional.ofNullable(CooperadoDTO.create(optional.get()));
		}
		return Optional.ofNullable(null);

	}

	public Optional<CooperadoDTO> obterDTOPorCodigoMatricula(Integer codigoMatricula) {
		Optional<Cooperado> optional = dao.buscaPorCodigoMatricula(codigoMatricula);
		if (optional.isPresent()) {
			return Optional.ofNullable(CooperadoDTO.create(optional.get()));
		}
		return Optional.ofNullable(null);

	}

	public List<CooperadoDTO> obterDTOPorDtUpdate(Date dtUpdate) {
		List<CooperadoDTO> dtos = new ArrayList<>();
		dao.obterCooperadosPorDtUpdate(dtUpdate).stream().forEach(cooperado -> {
			CooperadoDTO dto = CooperadoDTO.create(cooperado);
			dtos.add(dto);
		});
		return dtos;
	}

	public Optional<CooperadoDTO> obterCooperado(final int codigoMatricula, final String cpfCnpj) {
		final Optional<Cooperado> optional = dao.buscaPorCodigoCpfCnpj(codigoMatricula, cpfCnpj);
		if (optional.isPresent()) {
			return Optional.ofNullable(CooperadoDTO.create(optional.get()));
		}
		return Optional.ofNullable(null);
	}

}
