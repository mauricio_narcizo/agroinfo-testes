package br.com.senior.agroinfo.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.mock.Mock;

public class ContaDTOTest {
	
	@Test
	public void tipoContaDeveSerReceber() throws EnumNotFoundException {
		Conta conta = Mock.obterMockConta("1L", TipoConta.RECEBER, Mock.obterMockCooperado(), 
								Mock.obterMockEmpresaFilial(), Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaCredito));
		ContaDTO dto = ContaDTO.of(conta);
		assertEquals(TipoConta.RECEBER.name(), dto.getTipoConta());
		
		conta = Mock.obterMockConta("1L", TipoConta.PAGAR, Mock.obterMockCooperado(), Mock.obterMockEmpresaFilial(), 
								Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaDuplicatas));
		assertEquals(TipoConta.RECEBER.name(), dto.getTipoConta());
		
		conta = Mock.obterMockConta("1L", TipoConta.RECEBER, Mock.obterMockCooperado(), Mock.obterMockEmpresaFilial(), 
				Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaOutrosTitulos));
		
		assertEquals(TipoConta.RECEBER.name(), dto.getTipoConta());		
	}
	
	@Test
	public void tipoContaDeveSerPagar() throws EnumNotFoundException {
		Conta conta = Mock.obterMockConta("1L", TipoConta.RECEBER, 
										Mock.obterMockCooperado(), 
										Mock.obterMockEmpresaFilial(), 
										Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaDuplicatas));
		ContaDTO dto = ContaDTO.of(conta);
		assertEquals(TipoConta.PAGAR.name(), dto.getTipoConta());
		
		conta = Mock.obterMockConta("1L", TipoConta.PAGAR, 
										Mock.obterMockCooperado(), 
										Mock.obterMockEmpresaFilial(), 
										Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaCredito));
		assertEquals(TipoConta.PAGAR.name(), dto.getTipoConta());
		
		conta = Mock.obterMockConta("1L", TipoConta.PAGAR, 
										Mock.obterMockCooperado(), 
										Mock.obterMockEmpresaFilial(), 
										Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaOutrosTitulos));		
		assertEquals(TipoConta.PAGAR.name(), dto.getTipoConta());		
	}	

}
