package br.com.senior.agroinfo.util.parsers;

import java.util.Optional;
import static org.testng.Assert.*;

import org.testng.annotations.Test;

import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.services.TitulosExportarTitulosCPOutTitulos;

public class ContasPagarParserTest {

	@Test
	public void deveRetornarContasPagarIsPresent() {
		Optional<Conta> representation = ContasPagarParser.fromRepresentation(obterTitulosValido(),
				obterCooperado(), new TipoTitulo.Builder().build(), new EmpresaFilial.Builder().build());

		assertTrue(representation.isPresent());
	}

	@Test
	public void deveRetornarContasPagarNotPresent() {
		Optional<Conta> representation = ContasPagarParser.fromRepresentation(obterTitulosInvalido(),
				obterCooperado(), new TipoTitulo.Builder().build(), new EmpresaFilial.Builder().build());

		assertFalse(representation.isPresent());
	}

	@Test
	public void deveRetornarNotPresentDataInvalida() {
		TitulosExportarTitulosCPOutTitulos titulos = obterTitulosValido();
		titulos.setDatEmi("");
		Optional<Conta> representation = ContasPagarParser.fromRepresentation(titulos, obterCooperado(),
				new TipoTitulo.Builder().build(), new EmpresaFilial.Builder().build());
		assertFalse(representation.isPresent());
	}

	private static Cooperado obterCooperado() {
		Cooperado cooperado = new Cooperado.Builder()
											.id(1L)
											.build();
		return cooperado;
	}

	private static TitulosExportarTitulosCPOutTitulos obterTitulosValido() {
		final String data = "29/01/1990";
		TitulosExportarTitulosCPOutTitulos titulos = new TitulosExportarTitulosCPOutTitulos();

		titulos.setDatEmi(data);
		titulos.setVctOri(data);
		titulos.setJrsNeg(0D);
		titulos.setMulNeg(0D);
		titulos.setVlrAbe(0D);
		return titulos;
	}

	private static TitulosExportarTitulosCPOutTitulos obterTitulosInvalido() {
		TitulosExportarTitulosCPOutTitulos titulos = new TitulosExportarTitulosCPOutTitulos();
		return titulos;
	}	

}
