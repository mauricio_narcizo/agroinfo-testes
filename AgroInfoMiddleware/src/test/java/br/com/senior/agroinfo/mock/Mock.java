package br.com.senior.agroinfo.mock;

import java.math.BigDecimal;
import java.util.Date;

import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;

public class Mock {
	


	public static Cooperado obterMockCooperado() {
		return new Cooperado.Builder()
								.id(1L)
								.ativo(true)
								.atualizar(true)
								.bairro("Bela Vista")
								.cep("89110")
								.nome("Zezinho")
								.dtUpdate(new Date())
								.codigoMatricula(1)
								.build();		
	}
	
	public static EmpresaFilial obterMockEmpresaFilial() {
		return new EmpresaFilial.Builder()
									.id(5L)
									.ativa(true)
									.codigoEmpresa(1)
									.nomeFilial("Filial")
									.dddPadrao("047")
									.dtUpdate(new Date())
									.nomeEmpresa("Empresa 1")
									.build();		
	}
	
	public static Conta obterMockConta(String id, TipoConta tipoConta, Cooperado cooperado, EmpresaFilial empresaFilial, TipoTitulo tipoTitulo) {
		return new Conta.Builder(tipoConta)
							.id(id)
							.contaAberta(true)
							.cooperado(cooperado)
							.descricao("contareceber")
							.dtEmissao(new Date())
							.dtUpdate(new Date())
							.dtVencimento(new Date())
							.dtVencimentoOriginal(new Date())
							.empresaFilial(empresaFilial)
							.integrado(true)
							.juros(BigDecimal.ZERO)
							.multa(BigDecimal.ZERO)
							.tipoTitulo(tipoTitulo)
							.valor(BigDecimal.TEN)
							.valorAberto(BigDecimal.TEN)
							.build();
	}

	public static TipoTitulo obterMockTipoTitulo(SituacaoTipoTitulo situacaoTipoTitulo) {
		return new TipoTitulo.Builder()
								.abreviacaoTipoTitulo("Abrev")
								.codTipoTitulo("codigo")
								.descricaoTitulo("Descricao")
								.situacao(situacaoTipoTitulo)
								.build();
	}

}
