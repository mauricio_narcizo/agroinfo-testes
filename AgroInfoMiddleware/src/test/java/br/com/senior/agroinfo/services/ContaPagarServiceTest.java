package br.com.senior.agroinfo.services;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.senior.agroinfo.api.CooperadoResource;
import br.com.senior.agroinfo.db.dao.ContaDAO;
import br.com.senior.agroinfo.db.dao.CooperadoDAO;
import br.com.senior.agroinfo.db.dao.EmpresaFilialDAO;
import br.com.senior.agroinfo.db.dao.GenericDAO;
import br.com.senior.agroinfo.db.dao.TipoTituloDAO;
import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.BaseEntity;
import br.com.senior.agroinfo.db.modal.BaseEntityId;
import br.com.senior.agroinfo.db.modal.Builder;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.TipoTitulo;
import br.com.senior.agroinfo.dto.ContaDTO;
import br.com.senior.agroinfo.exception.BaseException;
import br.com.senior.agroinfo.exception.EnumNotFoundException;
import br.com.senior.agroinfo.exceptions.RegistroNaoEncontradoException;
import br.com.senior.agroinfo.exceptions.ValidationException;
import br.com.senior.agroinfo.mock.Mock;
import br.com.senior.agroinfo.util.DateUtil;

@RunWith(Arquillian.class)
public class ContaPagarServiceTest {
	
	@Inject
	private ContaDAO dao;
	
	@Inject
	private CooperadoDAO cooperadoDAO;
	
	@Inject
	private EmpresaFilialDAO empresaFilialDAO;
	
	@Inject
	private TipoTituloDAO tipoTituloDAO;
	
	@Inject
	private ContaService service;	
	
	private Cooperado cooperado = Mock.obterMockCooperado();

	private EmpresaFilial empresaFilial = Mock.obterMockEmpresaFilial();
	
	private TipoTitulo tipoTitulo = Mock.obterMockTipoTitulo(SituacaoTipoTitulo.SomaCredito);
	
	private Conta contaPagar = Mock.obterMockConta("6L", TipoConta.PAGAR, cooperado, empresaFilial, tipoTitulo);
	
	private Conta contaReceber = Mock.obterMockConta("1L", TipoConta.RECEBER, cooperado, empresaFilial, tipoTitulo);	
	
	
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(WebArchive.class, "ContaPagarServiceTeste.war")
				.addClass(ContaDTO.class)
				.addClass(Conta.class)
				.addClass(DateUtil.class)
				.addClass(BaseEntity.class)
				.addClass(BaseEntityId.class)
				.addClass(GenericDAO.class)
				.addClass(CooperadoResource.class)
				.addClass(Builder.class)
				.addClass(ContaService.class)
				.addClass(ValidationException.class)
				.addClass(Mock.class)
				.addClass(RegistroNaoEncontradoException.class)
				.addAsLibraries(Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies().resolve().withTransitivity().asFile())
				//.addAsResource("arquillian-ds.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}	
	
	@Before
	public void before() throws CloneNotSupportedException {		
		cooperado = cooperadoDAO.adiciona(cooperado);
		empresaFilial = empresaFilialDAO.adiciona(empresaFilial);
		tipoTitulo = tipoTituloDAO.adiciona(tipoTitulo);
		contaPagar = dao.adiciona(contaPagar);
		contaReceber = dao.adiciona(contaReceber);
	}
	
	@After
	public void tearDown() {
		dao.remove(contaPagar,contaPagar.getId());
		dao.remove(contaReceber,contaReceber.getId());
		cooperadoDAO.remove(cooperado);
		empresaFilialDAO.remove(empresaFilial);
		tipoTituloDAO.remove(tipoTitulo, tipoTitulo.getCodTipoTitulo());
	}	
	
	@Test
	public void deveObterDTOPorId() {		
		try {
			ContaDTO dto = service.obterDTO("1L");
			Assert.assertNotNull("DTO null", dto);
		} catch (BaseException e) {
			Assert.assertTrue("DTO não encontrado", false);
		}		
	}
	
	@Test
	public void deveObterDTOPreenchido() {		
		try {
			ContaDTO dto = service.obterDTO("1L");
			dto.valida();
			Assert.assertNotNull("DTO null", dto);
		} catch (Exception e) {
			Assert.assertTrue(e.getMessage(), false);
		}		
	}	
	
	@Test(expected = RegistroNaoEncontradoException.class)
	public void deveRetornarRegistroNaoEncontradoException() throws BaseException {		
		service.obterDTO("65L");
	}	

	@Test
    public void obterDTOs() throws EnumNotFoundException {
    	Assert.assertTrue("Lista de dtos não preenchida", service.obterDTOs().size() == 2);
    }
	
	@Test
	public void deveObterTodasContaPagar() {
		Assert.assertTrue("Lista de conta pagar não preenchida", service.obterTodasContas().size() == 2);
	}	

}
