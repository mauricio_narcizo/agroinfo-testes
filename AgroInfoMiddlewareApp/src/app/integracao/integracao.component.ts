import {Component, forwardRef, Inject} from 'angular2/angular2';
import {HTTP_PROVIDERS} from 'angular2/http';
import {IntegracaoService} from './IntegracaoService.ts';

@Component({
    selector: 'integracao',
    templateUrl: './src/app/integracao/integracao-form.component.html'

})
export class IntegracaoComponent {
    configuracao = "";
    withErrors = false;
    submitted = false;
    mensagemErro = "";
    mensagemSucesso = "Configurações salvas."

    constructor(service: IntegracaoService) {
		this.service = service;
        this.modelosSMS = this.service.modelosSMS;
        this.modelosSistema = this.service.modelosSistema;
        this.service.getConfiguracao()
            .subscribe(
                res => this.loadData(res),
                error => this.onError('Houve um erro ao conectar com o servidor')
            );
	}

    loadData(data) {
        this.configuracao = this.service.resolveTipoSistema(data);
        this.onSuccess();    
    }

    onError(msg) {
        this.withErrors = true;
        this.submitted = false;
        this.mensagemErro = msg;
	}

    onSuccess() {
        this.withErrors = false;
    }

    onSubmit() {
        this.service.salvar(this.configuracao)
            .subscribe(
                data => this.afterSubmit(data),
                err => this.onError('Erro ao salvar agendamentos')
            );
	 }

    afterSubmit(data) {
        if (data.status != 200) {
            this.onError(JSON.parse(data._body).mensagemErro);
            return;
        }       
        this.submitted = true;
        this.configuracao = JSON.parse(data._body);
        this.onSuccess();
   }
}
