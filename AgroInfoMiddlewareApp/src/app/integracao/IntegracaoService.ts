import {Injectable} from 'angular2/angular2';
import {Http, Headers} from 'angular2/http';
import {Config} from './../values/config.ts';
import {Integracao} from './Integracao.ts';
import {ModeloSistema} from './../values/modeloSistema.ts';
import {ModeloSMS} from './../values/modeloSMS.ts';    

@Injectable()
export class IntegracaoService{
    modelosSMS = new ModeloSMS().modelos;
    modelosSistema = new ModeloSistema().modelos;
    http: Http;
    //path = 'http://10.1.28.145:8080/AgroInfoMiddleware/api/integracao';//new Config().baseUri+'/integracao';
    path = new Config().baseUri+'/integracao';
    console.log(path);
    
    constructor(http: Http){
        this.http = http;
    }
    
    public getConfiguracao() {            
            return this.http.get(this.path)
                            .map(res => res.json());            
    }
    
    public resolveTipoSistema(value) {
        if (value.tipoSistemaSMS == null) {
            value.tipoSistemaSMS = this.modelosSMS[0];
        }
        if (value.tiposistema == null) {
            value.tiposistema = this.modelosSistema[0];
        }
        return value;
    }
    
    public salvar(dados) {
        var headers =new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.put(this.path, JSON.stringify(dados), {
            headers: headers
          });
    }
    
    
  }