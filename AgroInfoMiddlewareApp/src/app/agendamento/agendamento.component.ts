import {Component, forwardRef, Inject} from 'angular2/angular2';
import {HTTP_PROVIDERS} from 'angular2/http';
import {AgendamentoService} from './agendamentoService.ts';

@Component({
    selector: 'agendamento',
    templateUrl: './src/app/agendamento/agendamento-form.component.html'    

})
export class AgendamentoComponent {
    agendamentos;
    periodos;
    withErrors = false;
    submitted = false;
    mensagemErro = "";
    mensagemSucesso = "Agendamento efetuado."
    service: AgendamentoService;

	constructor(service: AgendamentoService) {
        this.service = service;
        this.periodo = service.periodos;
        this.service.getAgendamentos()
            .subscribe(
                res => {this.agendamentos = res, this.onSuccess()},
                error => this.onError('Houve um erro ao conectar com o servidor')
            );
    
	}

	onError(msg) {
        this.withErrors = true;
        this.submitted = false;
        this.mensagemErro = msg;
	}

    onSuccess(data) {
        this.withErrors = false;
    }

    onSubmit() {
        this.service.salvar(this.agendamentos)
            .subscribe(
                data => {this.afterSubmit(data)},
                err => this.onError('Erro ao salvar agendamentos')
            );
	 }
    
    afterSubmit(data) {        
        if (data.status != 200) {
            this.onError(JSON.parse(data._body).mensagemErro);
            return;
        }        
        this.submitted = true;
        this.onSuccess();
   }    
}
