export class Agendamento {
  constructor(
    public id: number,
    public nome: string,
    public descricao: string,
    public periodicidade?: number,
    public tempoUltimaExecucao?: number,
    public cargaCompleta?: number,
    public ativo: boolean,
    public executando: boolean,
    public grupo: string,
    public dtUpdate: string,
    public formatoPeriodicidade: string,
    public formatoCargaCompleta: string,
    public tempoCargaCompleta: number
  ) {  }
}
