import {Injectable} from 'angular2/angular2';
import {Http, Headers} from 'angular2/http';
import {Config} from './../values/config.ts';
import {FormatoPeriodoIntegracao} from './../values/formatoPeriodoIntegracao.ts';

@Injectable()
export class AgendamentoService{
    path = new Config().baseUri+'/agendamento';
    periodos = new FormatoPeriodoIntegracao().periodo;
    http: Http;
    
    constructor(private http: Http){
        this.http = http;
    }
    
    public getAgendamentos() {            
            return this.http.get(this.path)
                            .map(res => res.json());            
    }
    
    public salvar(dados) {
        return this.http.put(this.path, JSON.stringify(dados), {
            headers: new Headers({
              'Content-Type': 'application/json'
            })
          });
    }
  }