import {Component, bootstrap, provide} from 'angular2/angular2';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/angular2';
import {RouteConfig,  ROUTER_DIRECTIVES, ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {AgendamentoService} from 'src/app/agendamento/agendamentoService.ts';
import {IntegracaoService} from 'src/app/integracao/IntegracaoService.ts';
import {AgendamentoComponent} from 'src/app/agendamento/agendamento.component.ts';
import {IntegracaoComponent} from 'src/app/integracao/integracao.component.ts';
import {DashboardComponent} from 'src/app/dashboard/dashboard.component.ts';
import {AgricultorComponent} from 'src/app/agricultor/agricultor.component.ts';
import {MensagemComponent} from 'src/app/mensagem/mensagem.component.ts';


@Component({
    selector: 'agroInfo',
    templateUrl: 'template.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES, AgendamentoComponent, 
                IntegracaoComponent, DashboardComponent, AgricultorComponent, MensagemComponent],
    providers: [AgendamentoService, IntegracaoService]
    
})
@RouteConfig([
    {path: '/agendamento', component: AgendamentoComponent, as: 'Agendamento'},
    {path: '/integracao', component: IntegracaoComponent, as: 'Integracao'},
    {path: '/inicio', component: DashboardComponent, as: 'Inicio'  }
    {path: '/agricultor', component: AgricultorComponent, as: 'Agricultor'},
    {path: '/mensagem', component: MensagemComponent, as: 'Mensagem'}
])

class AppComponent{
}
bootstrap(AppComponent, [ROUTER_PROVIDERS, HTTP_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy})]);
