'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var bowerMain = require('bower-main');

var reload = browserSync.reload;

var jsBowerFiles = bowerMain('js', 'min.js');
var cssBowerFiles = bowerMain('css', 'min.css');

var config = {
  src: 'src',
  resources: 'resources',
  dist: '../AgroInfoMiddleware/src/main/webapp',
  javaResources: '../AgroInfoMiddleware/src/main/resources'
};

gulp.task('default', ['build']);

gulp.task('build', function(callback) {
  runSequence('clean', ['html', 'js:bower', 'css:bower', 'js', 'ts', 'resourcesApi', 'css:resources', 'js:resources', 'img:resources', 'font-awesome'], callback);
});

gulp.task('resourcesApi', function() {
  gulp.src(config.javaResources + '/**/*')
    .pipe(gulp.dest(config.dist));
});

gulp.task('html', function() {

  gulp.src('*.html')
    .pipe(gulp.dest(config.dist));

  gulp.src(config.src + '/**/*.html')
    .pipe(gulp.dest(config.dist + '/' + config.src));
});

gulp.task('js:bower', function() {
  gulp.src(jsBowerFiles.minified)
    .pipe(gulp.dest(config.dist + '/bower_components/js'));
});

gulp.task('js:resources', function() {
  gulp.src(config.resources + '/**/*.js')
    .pipe(gulp.dest(config.dist + '/' + config.resources));
});

gulp.task('js', function() {
  gulp.src('*.js')
    .pipe(gulp.dest(config.dist));
  gulp.src(config.src + '/**/*.js')
    .pipe(gulp.dest(config.dist + '/' + config.src));
});

gulp.task('css:bower', function() {
  gulp.src(cssBowerFiles.minified)
    .pipe(gulp.dest(config.dist + '/bower_components/css'));
});

gulp.task('css:resources', function() {
  gulp.src(config.resources + '/**/*.css')
    .pipe(gulp.dest(config.dist + '/' + config.resources));
});

gulp.task('img:resources', function() {
  gulp.src(config.resources + '/img/**/*')
    .pipe(gulp.dest(config.dist + '/' + config.resources + '/img'));
});

gulp.task('font-awesome', function() {
  gulp.src('./bower_components/font-awesome/fonts/**/*')
    .pipe(gulp.dest(config.dist + '/bower_components/fonts'));
});

gulp.task('ts', function() {
  gulp.src('*.ts')
    .pipe(gulp.dest(config.dist));
  gulp.src(config.src + '/**/*.ts')
    .pipe(gulp.dest(config.dist + '/' + config.src));
});

gulp.task('clean', function() {
  return gulp.src(config.dist, {
      read: false
    })
    .pipe(clean({
      force: true
    }));
});

gulp.task('serve', function(callback) {
  runSequence('watch', ['build'], callback);
  browserSync.init({
    server: {
      baseDir: config.dist
    }
  });
});

gulp.task('watch', function() {
  gulp.watch(['*.html', config.src + '/**/*.html'], ['html']).on('change', browserSync.reload);
  gulp.watch(['*.js', config.src + '/**/*.js'], ['js:bower', 'js']).on('change', browserSync.reload);
  gulp.watch(['*.ts', config.src + '/**/*.ts'], ['ts']).on('change', browserSync.reload);
  gulp.watch([config.resources + '/**/*.css'], ['css:resources', 'css:bower']).on('change', browserSync.reload);
  gulp.watch([config.resources + '/img/**/*'], ['img:resources']).on('change', browserSync.reload);
  gulp.watch([config.resources + '/js/**/*'], ['js:resources']).on('change', browserSync.reload);
});
