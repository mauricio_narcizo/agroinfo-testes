import {
Page, NavParams
}
from 'ionic/ionic';

import {
Http
}
from 'angular2/http';

import {
PrevisaoTempoService
}
from '../service/previsaoTempo';
import {
CooperadoService
}
from '../service/cooperado';
import {
ContasService
} from '../service/contas';

@
    Page({
        templateUrl: 'build/home/home.html'
    })
export class HomePage {
    constructor(http: Http, previsaoTempoService: PrevisaoTempoService, cooperadoService: CooperadoService, navParams: NavParams, contasService: ContasService) {
        this.http = http;
        this.previsaoTempo = previsaoTempoService.getPrevisoesTempo();
        this.cooperado = cooperadoService.getCooperado();
        this.previsaoTempoHoje = this.previsaoTempo[0];
        this.previsaoTempoAmanha = this.previsaoTempo[1];
        console.log(this.previsaoTempoAmanha.dia);
        // this.previsaoTempoAmanha.dia = this.parseToDate(this.previsaoTempoAmanha.dia); 
        
        this.previsaoTempoDoisDias = this.previsaoTempo[2];
        // this.previsaoTempoDoisDias.dia = this.parseToDate(this.previsaoTempoDoisDias.dia);
        this.contasService = contasService;
        this.totalPagar = 0;
        this.totalReceber = 0;
        this.saldo = 0;
        this.contabilizarContas(this.contasService.getContas());
    }
    
    parseToDate(data) {
        let param = data.split('-');
        return new Date(param);
    }

    contabilizarContas(contas) {
        for (let conta of contas) {
            if (conta.tipoConta === 'PAGAR') {
                this.totalPagar += conta.valor;
            } else if (conta.tipoConta === 'RECEBER') {
                this.totalReceber += conta.valor;
            }
        }
        this.saldo = this.totalReceber - this.totalPagar;
    }

    getTotalPagar() {

        return this._formatarValorMonetario(this.totalPagar);
    }

    getTotalReceber() {
        return this._formatarValorMonetario(this.totalReceber);
    }

    getSaldo() {
        return this._formatarValorMonetario(this.saldo);
    }

    _formatarValorMonetario(valor) {
        return valor.toFixed(2).replace('.', ',').replace(/./g, function (c, i, a) {
            return i && c !== "," && ((a.length - i) % 3 === 0) ? '.' + c : c;
        });
    }
}