import {
Page, ViewController
}
from 'ionic/ionic';
import {
DataService
}
from '../../service/data';

@Page({
    templateUrl: 'build/financeiro/filter/financeiro-filter.html'
})
export class FinanceiroFilterPage {
    constructor(dataService: DataService, viewCtrl: ViewController) {
        this.dataService = dataService;
        this.viewCtrl = viewCtrl;
        this.carregarOpcoesFiltro();
        this.tipoFiltro = this.opcoes[0];
    }

    resetarFiltros() {

    }

    aplicarFiltros() {
        this.fechar(this.tipoFiltro);
    }

    fechar(data) {
        this.viewCtrl.dismiss(data);
    }

    carregarOpcoesFiltro() {

        this.opcoes = [
            { id: 1, descricao: 'Extrato resumido', periodo: {} },
            { id: 2, descricao: 'Últimos 30 dias', periodo: this.obterPeriodoDias(30) },
            { id: 3, descricao: 'Últimos 60 dias', periodo: this.obterPeriodoDias(60) },
            { id: 4, descricao: 'Definir período', periodo: {} }
        ];
    }

    obterPeriodoDias(diasDescontar) {
        const data = new Date();
        const dataHoje = new Date();
        data.setDate(dataHoje.getDate() - diasDescontar);
        const dataAnterior = data;
        let periodo = {
            dtFim: dataHoje.getUTCFullYear() + '-' + (dataHoje.getUTCMonth() + 1) + '-' + dataHoje.getUTCDate(),
            dtInicio: dataAnterior.getUTCFullYear() + '-' + (dataAnterior.getUTCMonth() + 1) + '-' + dataAnterior.getUTCDate()
        };
        console.log(periodo);
        return periodo;
    }

}