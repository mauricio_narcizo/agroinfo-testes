import {NavController, Page, Modal} from 'ionic/ionic';
import {DataService} from '../service/data';
import {SessionFilterPage} from '../session-filter/session-filter';
import {ContasService} from '../service/contas';
import {FinanceiroFilterPage} from './filter/financeiro-filter';
import {CooperadoService} from '../service/cooperado';

@Page({
    templateUrl: 'build/financeiro/financeiro.html'
})
export class FinanceiroPage {
    constructor(nav: NavController, dataService: DataService, contasService: ContasService, cooperadoService: CooperadoService) {
        this.nav = nav;
        this.contasService = contasService;
        this.cooperadoService = cooperadoService;
        console.log('tem cooperado?', this.cooperadoService.getCooperado());
        console.log('tem cooperativa?', this.cooperadoService.getCooperativaCooperado());

        this.contas = null;
        this.contasMeses = null;
        this.carregarContas();
    }

    carregarContas() {
        this.contasMeses = [];
        this.contas = this.contasService.getContas();
        this.agruparContasMes(this.contas);
    }

    _formatarValorMonetario(valor) {
        return valor.toFixed(2).replace('.', ',').replace(/./g, function (c, i, a) {
            return i && c !== "," && ((a.length - i) % 3 === 0) ? '.' + c : c;
        });
    }

    agruparContasMes(contas) {
        for (let conta of contas) {
            conta.debito = conta.tipoConta === 'PAGAR';
            conta.valor = this._formatarValorMonetario(conta.valor);
            conta.data = this.toDate(conta.dtVencimentoOriginal);
            //Obtém o ano e o mês
            let anoMes = this.obterAnoMes(conta.data);

            let contaMes = this.contasMeses.find(c=> c.anoMes.mes.descricao === anoMes.mes.descricao && c.anoMes.ano === anoMes.ano);
            if (!contaMes) {
                contaMes = { anoMes: anoMes, contas: [] };
                this.contasMeses.push(contaMes);
            }

            contaMes.contas.push(conta);
        }
        this.ordenarMeses(this.contasMeses);
    }

    ordenarMeses(contasMeses) {
        contasMeses.sort((a, b) => a.anoMes.ano < b.anoMes.ano && a.anoMes.mes.numMes < b.anoMes.mes.numMes);
        contasMeses.forEach(contaMes => this.ordenarContasPorData(contaMes));
    }

    ordenarContasPorData(contaMes) {
        contaMes.contas.sort((a, b) => {
            return a.data.getTime() < b.data.getTime();
        });
    }

    obterAnoMes(data) {
        const meses = [
            { numMes: 1, descricao: 'Janeiro' }, { numMes: 2, descricao: 'Fevereiro' },
            { numMes: 3, descricao: 'Março' }, { numMes: 4, descricao: 'Abril' },
            { numMes: 5, descricao: 'Maio' }, { numMes: 6, descricao: 'Junho' },
            { numMes: 7, descricao: 'Julho' }, { numMes: 8, descricao: 'Agosto' },
            { numMes: 9, descricao: 'Setembro' }, { numMes: 10, descricao: 'Outubro' },
            { numMes: 11, descricao: 'Novembro' }, { numMes: 12, descricao: 'Dezembro' }
        ];

        let mes = meses.find(mes => mes.numMes === data.getMonth() + 1);
        return { ano: data.getUTCFullYear(), mes: mes };
    }

    toDate(data) {
        let dataParam = data.split('-');
        let day = dataParam[0];
        let month = dataParam[1] - 1;
        let year = dataParam[2];
        let dt = new Date(year, month, day);
        return dt;
    }

    presentFilter() {
        let modal = Modal.create(FinanceiroFilterPage, this.excludeTracks);
        modal.onDismiss(data => {
            console.log('filtro data', data);
            if (data) {
                this.atualizarContas(data);
            }
        });
        this.nav.present(modal);
    }

    atualizarContas(filtro) {
        this.contasService.carregarContas(this.cooperadoService.getCooperado().id, this.cooperadoService.getCooperativaCooperado().ip, filtro.periodo.dtInicio, filtro.periodo.dtFim)
            .then(() => {
                console.log('atualizando');
                this.carregarContas();
            });
    }

}