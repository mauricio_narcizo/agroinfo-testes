import {Page, NavController, IonicApp, Alert, Keyboard} from 'ionic/ionic';
import {TabsPage} from '../tabs/tabs';
import {CooperativaService} from '../service/cooperativa';
import {CooperadoService} from '../service/cooperado';
import {ContasService} from '../service/contas';
import {PrevisaoTempoService} from '../service/previsaoTempo';
import {Observable} from 'angular2/core'

@Page({
    templateUrl: 'build/login/login.html'
})
export class LoginPage {

    constructor(nav: NavController, previsaoTempoService: PrevisaoTempoService, app: IonicApp, cooperativaService: CooperativaService, keyboard: Keyboard,
        cooperadoService: CooperadoService, contasService: ContasService) {
        this.nav = nav;
        this.app = app;
        this.keyboard = keyboard;

        //Services
        this.previsaoTempoService = previsaoTempoService;
        this.cooperativaService = cooperativaService;
        this.cooperadoService = cooperadoService;
        this.contasService = contasService;

        //inputs
        this.cpfCNPJ = null;
        this.codigoCooperado = null;
        
        //resposta da requisição
        this.cooperativaSelecionada = null;
        this.cooperado = null;
    }

    entrar() {
        if (this.isLoginValido()) {
            this.carregarCooperado();
        } else {
            this.exibirPopupAlerta('Número da matrícula não foi encontrado para este CPF/CNPJ!');
        }
    }

    isLoginValido() {

        return this.isCooperativaEncontrada() && this.codigoCooperado && this.isCpfCnpjValido();
    }

    verificarCnpj() {
        if (!this.cpfCNPJ) {
            this.exibirPopupAlerta('CPF/CNPJ não informado!');
        } else {
            this.validarCpfCnpj();
        }
    }

    changeCpfCnpj() {
        if (this.cpfCNPJ) {
            this.validarCpfCnpj();
        } else {
            this.cooperativaSelecionada = null;
        }
    }

    validarCpfCnpj() {
        try {
            this.cooperativaService.carregarCooperativa(this.cpfCNPJ)
                .subscribe(
                    res=> {
                        this.cooperativa = null;
                        if (res._body && res._body !== '') {
                            let data = res.json();
                            this.cooperativaSelecionada = data;
                        }
                    },
                    error=> this.exibirPopupAlerta('Não foi possível validar este CPF/CNPJ.'),
                    () => {
                        if (!this.cooperativaSelecionada) {
                            this.exibirPopupAlerta('CPF/CNPJ não encontrado.');
                        } else {
                            this.cooperadoService.setCooperativaCooperado(this.cooperativaSelecionada);
                        }

                    });
        } catch (e) {
            console.log(e);
            this.exibirPopupAlerta(e);
        }
    }
    
    /**
     * Verifica se com o CPF/CNPJ e o código da matrícula está vinculado à alguma cooperativa. 
     */
    isCooperativaEncontrada() {
        return this.cooperativaSelecionada && this.cooperativaSelecionada !== null;
    }

    /*
    * Verifica se o CPF/CNPJ é valido ao procurar no servidor as cooperativas vinculadas
    */
    isCpfCnpjValido() {
        //TODO adicionar validações do tamanho de caracteres para CPF e CNPJ
        return true;
    }

    doLogin() {
        this.nav.push(TabsPage);
    }

    exibirPopupAlerta(msg) {
        this.keyboard.close();
        let alert = Alert.create({
            title: 'Aviso',
            body: msg,
            buttons: ['OK']
        });
        this.nav.present(alert);

    }

    carregarCooperado() {
        this.cooperadoService.carregarCooperado(this.codigoCooperado, this.cpfCNPJ, this.cooperativaSelecionada.ip)
            .subscribe(
                res=> {
                    this.cooperado = null;
                    if (res._body && res._body !== '') {
                        this.cooperado = res.json();
                    }
                },
                error=> this.exibirPopupAlerta('Não foi possível validar este número de matrícula.'),
                () => {
                    if (!this.cooperado) {
                        this.exibirPopupAlerta('Número da matrícula não foi encontrado para este CPF/CNPJ!');
                    } else {
                        this.carregarPrevisaoTempo();
                    }
                });
    }

    carregarPrevisaoTempo() {
        this.previsaoTempoService.carregarPrevisaoTempo(this.cooperado.cidade)
            .then(() => {
                this.carregarContasCooperado();
            });
    }

    carregarContasCooperado() {
        this.contasService.carregarContas(this.cooperado.id, this.cooperativaSelecionada.ip)
            .then(result => {
                this.doLogin();
            });
    }

}