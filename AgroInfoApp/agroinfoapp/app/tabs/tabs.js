import {NavController, Page} from 'ionic/ionic';
import {FinanceiroPage} from '../financeiro/financeiro';
import {MensagemPage} from '../mensagem/mensagem';
import {SairPage} from '../logout/logout';
import {HomePage} from '../home/home';

@Page({
    templateUrl: 'build/tabs/tabs.html'
})
export class TabsPage {
    constructor(nav: NavController) {
        // set the root pages for each tab
        this.nav = nav;
        this.tabFinanceiroRoot = FinanceiroPage;
        this.tabMensagemRoot = MensagemPage;
        this.tabSairRoot = SairPage;
        this.tabHomeRoot = HomePage;
    }
}