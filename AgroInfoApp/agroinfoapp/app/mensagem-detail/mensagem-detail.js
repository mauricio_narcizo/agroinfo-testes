import {
    NavController, NavParams, Page
}
from 'ionic/ionic';
import {
    DataService
}
from '../service/data';

@
Page({
    templateUrl: 'build/mensagem-detail/mensagem-detail.html',
})
export class MensagemDetailPage {
    constructor(nav: NavController, navParams: NavParams, dataService: DataService) {
        this.mensagem = navParams.data;
    }
}