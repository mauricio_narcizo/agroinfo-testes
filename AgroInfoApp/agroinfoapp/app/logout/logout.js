import {
    NavController, Page, IonicApp
}
from 'ionic/ionic';
import {
    LoginPage
}
from '../login/login';
@Page({
    templateUrl: 'build/logout/logout.html'
})
export class SairPage {
    constructor(nav: NavController, app: IonicApp) {
        this.app = app;
        this.root = LoginPage;
        this.nav = nav;
    }

    login() {
        let nav = this.app.getComponent('nav');
        nav.setRoot(this.root);
    }

    sair() {
        this.app._config._platform.exitApp();
    }
}