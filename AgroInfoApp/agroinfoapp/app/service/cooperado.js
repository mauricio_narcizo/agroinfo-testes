import {
Injectable
}
from 'angular2/core';

import {
Http
}
from 'angular2/http';

@Injectable()
export class CooperadoService {

    constructor(http: Http) {
        //const STATUS_RESPONSE_OK_WITH_CONTENT = 200;
        this.URL_COOPERADO_MATRICULA = '/AgroInfoMiddleware/api/cooperado/matricula/{codigoMatricula}/cpfCnpj/{cpfCnpj}';
        this.http = http;

        this.cooperado = null;
        this.cooperativa = null;
    }
	
	/**
	 * Faz a busca do cooperado pelo código da matricula e URL da base da cooperativa.
	 * 
	 * @return Retorna um observable, que é a resposta da requisição http ao serviço de cooperativas
	 */
    carregarCooperado(codigoMatricula, cpfCNPJ, urlConsulta) {
        let obs = this.http.get(urlConsulta + this.URL_COOPERADO_MATRICULA.replace('{codigoMatricula}', codigoMatricula).replace('{cpfCnpj}', cpfCNPJ));
        obs.subscribe(res=> {
            this.cooperado = null;
            if (res._body && res._body !== '') {
                this.cooperado = res.json();
            }
        });
        return obs;
    }
	
	/**
	 * Retorna o cooperado que foi consultada no serviço rest do AgroInfoMiddleware
	 **/
    getCooperado() {
        return this.cooperado;
    }

    setCooperativaCooperado(cooperativa) {
        this.cooperativa = cooperativa;
    }

    getCooperativaCooperado() {
        return this.cooperativa;
    }
}