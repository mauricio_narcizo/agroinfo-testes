import {
Injectable
}
from 'angular2/core';

import {
Http
}
from 'angular2/http';

@Injectable()
export class ContasService {

    constructor(http: Http) {
        this.STATUS_RESPONSE_OK_WITH_CONTENT = 200;
        this.URL_CONTAS_COOPERADO = '/AgroInfoMiddleware/api/conta/cooperado/{codigoMatricula}';
        this.http = http;
        this.contas = null;
    }
	
    // carregarContas(codigoMatricula, urlConsulta) {
    //     let obs = this.http.get(urlConsulta + this.URL_CONTAS_COOPERADO.replace('{codigoMatricula}', codigoMatricula));
    //     obs.subscribe(
    //         response=> {
    //             this.contas = response.status !== this.STATUS_RESPONSE_OK_WITH_CONTENT ? null : response.json();
    //         });

    //     return obs;
    // }
	
	/**
	 * Retorna o cooperado que foi consultada no serviço rest do AgroInfoMiddleware
	 **/
    getContas() {
        return this.contas;
    }

    /**
	 * Faz a busca do cooperado pelo código da matricula e URL da base da cooperativa.
	 * 
	 * @return Retorna um observable, que é a resposta da requisição http ao serviço de cooperativas
	 */
    carregarContas(codigoMatricula, urlConsulta, dtInicio, dtFim) {
        let promise = new Promise(resolve => {
            let url = urlConsulta + this.URL_CONTAS_COOPERADO.replace('{codigoMatricula}', codigoMatricula);

            if (dtInicio && dtFim) {
                url = url.concat('?dtInicio=' + dtInicio + '&dtFim=' + dtFim);
            }

            console.log(url);

            this.http.get(url)
                .subscribe(
                    response=> {
                        this.contas = response.status !== this.STATUS_RESPONSE_OK_WITH_CONTENT ? null : response.json();
                        console.log('resultado das contas', this.contas);
                        resolve(this.contas);
                    });
        });
        return promise;
    }
}