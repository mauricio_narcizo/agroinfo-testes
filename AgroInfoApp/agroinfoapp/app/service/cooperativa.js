import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';

@Injectable()
export class CooperativaService {

    constructor(http: Http) {
        this.http = http;
        this.cooperativa = null;
    }
	
	/**
	 * Faz a busca de cooperativas pelo cpfCnpj do cooperado informado.
	 * 
	 * @return Retorna um observable, que é a resposta da requisição http ao serviço de cooperativas
	 */
    carregarCooperativa(cpfCnpj) {
        /**
         * URL será direcionada para o AgroInfoServer, que estará hospedado no servidor da Senior
        **/
        const URL_COOPERATIVAS = 'http://10.1.28.145:8080/AgroInfoServer/rest/cooperativa/cnpj/{cpfCnpj}';
        
        //48254353000144 cooperado testes
        if (cpfCnpj && cpfCnpj.trim() !== '') {
            let obs = this.http.get(URL_COOPERATIVAS.replace('{cpfCnpj}', cpfCnpj));
            return obs;
        } else {
            throw Error('CPF/CNPJ não informado');
        }
    }
	
	/**
	 * Retorna a cooperativa que foi consultada no serviço rest do AgroInfoServer
	**/
    getCooperativa() {
        return this.cooperativa;
    }
}