import {
App, IonicApp, IonicPlatform
}
from 'ionic/ionic';
import {
Injectable, bind
}
from 'angular2/core';
import {
Http
}
from 'angular2/http';

@
    Injectable()
export class DataService {
    constructor(app: IonicApp, http: Http) {
        this.app = app;
        this.http = http;
        this.mensagens = null;
    }

    retrieveData() {
        //Under the hood we are using Angular http service.
        //This defaults to use the HTTP_BINDING for http requests.
        //Here, we're going to get a JSON data file, use the `map` call to parse json
        // and finally subscribe to the observable and set our data
        //to the value it provides once the http request is complete.
        
 
        this.http.get('data/mensagens.json')
            .subscribe(res => {
                let data = res.json();
                this.mensagens = data;
            });
    }

    getMensagens() {
        return this.mensagens;
    }

}