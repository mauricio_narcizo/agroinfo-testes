import {
App, IonicApp, IonicPlatform
}
from 'ionic/ionic';
import {
Injectable, bind
}
from 'angular2/core';
import {
Http
}
from 'angular2/http';
import {
XmlToJson
}
from './XmlToJson';

@
    Injectable()
export class PrevisaoTempoService {

    constructor(app: IonicApp, http: Http, xmlToJson: XmlToJson) {
        this.URL_LISTA_CIDADES = 'http://servicos.cptec.inpe.br/XML/listaCidades?city={nomeCidade}';
        this.URL_PREVISAO_TEMPO = 'http://servicos.cptec.inpe.br/XML/cidade/{codigoCidade}/previsao.xml';
        this.previsoesTempoDefault = null;
        this.condicoesTempo = null;
        this.app = app;
        this.http = http;
        this.previsoesTempo = [];
        this.xmlToJson = xmlToJson;
        this.http.get('data/condicaoTempo.json')
            .subscribe(res => {

                this.previsoesTempo = this.carregarPrevisoes(res.json());
            });
    }

    // carregarPrevisaoTempo(cidade) {
    //     if (cidade && cidade.trim() !== '') {
    //         let obs = this.http.get(this.URL_LISTA_CIDADES.replace('{nomeCidade}', cidade));

    //         obs.subscribe(res=> {

    //             if (res._body && res._body !== '') {
    //                 let cidade = this.xmlToJson.toJson(res.text()).cidades[0];
    //                 let codigoCidade = cidade.cidade[0].id[0]._text;

    //                 this.http.get(this.URL_PREVISAO_TEMPO.replace('{codigoCidade}', codigoCidade))
    //                     .subscribe(response => {
    //                         let res = this.xmlToJson.toJson(response.text());
    //                         this.previsoesTempo = res.cidade[0].previsao;
    //                         for (let previsao of this.previsoesTempo) {
    //                             previsao.dia = previsao.dia[0]._text;
    //                             previsao.iuv = previsao.iuv[0]._text;
    //                             previsao.maxima = previsao.maxima[0]._text;
    //                             previsao.minima = previsao.minima[0]._text;
    //                             previsao.tempo = previsao.tempo[0]._text;
    //                             previsao.condicaoTempo = this.condicoesTempo[previsao.tempo];
    //                         }
    //                     });
    //             }

    //         });
    //         return obs;
    //     }
    // }

    carregarPrevisaoTempo(cidade) {
        if (cidade && cidade.trim() !== '') {
            let promise = new Promise(resolve=> {
                this.buscarCodigoCidade(cidade)
                    .then(codigoCidade => {

                        this.http.get(this.URL_PREVISAO_TEMPO.replace('{codigoCidade}', codigoCidade))
                            .subscribe(response => {
                                let res = this.xmlToJson.toJson(response.text());
                                this.previsoesTempo = res.cidade[0].previsao;
                                for (let previsao of this.previsoesTempo) {
                                    previsao.dia = previsao.dia[0]._text;
                                    previsao.iuv = previsao.iuv[0]._text;
                                    previsao.maxima = previsao.maxima[0]._text;
                                    previsao.minima = previsao.minima[0]._text;
                                    previsao.tempo = previsao.tempo[0]._text;
                                    previsao.condicaoTempo = this.condicoesTempo[previsao.tempo];
                                }
                                resolve(this.previsoesTempo);
                            });
                    });
            });
            return promise;
        } else {
            return Promise.resolve({});
        }
    }

    buscarCodigoCidade(cidade) {
        let promise = new Promise(resolve=> {
            this.http.get(this.URL_LISTA_CIDADES.replace('{nomeCidade}', cidade))
                .subscribe(res=> {
                    if (res._body && res._body !== '') {
                        let cidade = this.xmlToJson.toJson(res.text()).cidades[0];
                        let codigoCidade = cidade.cidade[0].id[0]._text;
                        resolve(codigoCidade);
                    }
                });


        });

        return promise;
    }

    getPrevisoesTempo() {
        return this.previsoesTempo;
    }

    carregarPrevisoes(condicao) {
        this.condicoesTempo = condicao;
        return this.previsoesTempoDefault = [
            {
                condicaoTempo: condicao.nd,
                dia: '',
                iuv: 0,
                maxima: 0,
                minima: 0
            }
            , {
                condicaoTempo: condicao.nd,
                dia: '',
                iuv: 0,
                maxima: 0,
                minima: 0
            }
            , {
                condicaoTempo: condicao.nd,
                dia: '',
                iuv: 0,
                maxima: 0,
                minima: 0
            }];
    }
    
    /**
     *Carregar a previsão do tempo do localstorage
     *  
     */
    carregarPrevisaoTempoLocal() {
        //TODO carregar
        return null;
    }
    
    /**
     *Salvar a previsao no localstorage, para posterior consulta, caso o serviço esteja indisponível
     * 
    */
    salvarPrevisaoTempoLocal(previsoes) {
        //TODO Salvar
    }


}