import {
    NavController, Page, Modal, Searchbar
}
from 'ionic/ionic';

import {
    DataService
}
from '../service/data';

import {
    MensagemDetailPage
}
from '../mensagem-detail/mensagem-detail';

@
Page({
    templateUrl: 'build/mensagem/mensagem.html',
    directives: [Searchbar]
})
export class MensagemPage {
    constructor(nav: NavController, dataService: DataService) {
        this.nav = nav;
        this.mensagens = dataService.getMensagens();
    }

    abrirMensagem(mensagem) {
        this.nav.push(MensagemDetailPage, mensagem);
    }
}