package br.com.integradortools.util;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.integradortools.config.IntegradorToolsProperties;


public class IntegradorTools {

    private String arquivoListaIntegracoes;
    private int codigoIntegracao;
    private int codigoIntegracaoERP;
    private int codigoIntegracaoRetaguarda;
    private String identificacaoDescricaoIntegracao;
    private String classePrincipalTO[];
    private String classePrincipalParam;
    private DataFile dataClassePrincipalTO[];
    private DataFile dataClassePrincipalParam;
    private String diretorioClassesTO;
    private String diretorioClassesParam;
    private String classeIntermediariaERP;
    private String classeIntermediariaRetaguarda;
    private DataFile dataClasseIntermediariaERP;
    private DataFile dataClasseIntermediariaRetaguarda;
    private boolean addListaIntegracoes = true;
    private boolean addTransporte = true;
    private boolean addParam = true;
    private boolean addTrataPendencia = true;
    private boolean addIntegracoesProperties = true;
    private boolean addObjectFactory = true;
    private boolean addSuportaExporta = true;
    private boolean addSuportaAtualiza = true;
    private boolean dadosCarregados;

    public IntegradorTools() {

        dadosCarregados = false;
        arquivoListaIntegracoes = IntegradorToolsProperties.INSTANCE.LISTA_INTEGRACOES;
        try {
            dadosCarregados = buscaUltimaIntegracao();
        } catch (IOException ex) {
            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (dadosCarregados) {
                if (classePrincipalTO != null || !(classePrincipalTO.length == 0)) {
                    dataClassePrincipalTO = new DataFile[classePrincipalTO.length];
                    for (int i = 0; i < classePrincipalTO.length; i++) {
                        dataClassePrincipalTO[i] = new DataFile(new File(classePrincipalTO[i]));
                    }
                } else {
                    dataClassePrincipalTO = null;
                }
                if (!classePrincipalParam.isEmpty()) {
                    dataClassePrincipalParam = new DataFile(new File(classePrincipalParam));
                }
                if (!classeIntermediariaERP.isEmpty()) {
                    dataClasseIntermediariaERP = new DataFile(new File(classeIntermediariaERP));
                }
                if (!classeIntermediariaRetaguarda.isEmpty()) {
                    dataClasseIntermediariaRetaguarda = new DataFile(new File(classeIntermediariaRetaguarda));
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setVariables(int codigoIntegracao, int codigoIntegracaoERP, int codigoIntegracaoRetaguarda, String identificacaoDescricaoIntegracao, String classePrincipalTO[], String classePrincipalParam, String diretorioClassesTO, String diretorioClassesParam, String classeIntermediariaERP, String classeIntermediariaRetaguarda) throws IOException {
        this.codigoIntegracao = codigoIntegracao;
        this.codigoIntegracaoERP = codigoIntegracaoERP;
        this.codigoIntegracaoRetaguarda = codigoIntegracaoRetaguarda;
        this.identificacaoDescricaoIntegracao = identificacaoDescricaoIntegracao;
        this.classePrincipalTO = classePrincipalTO;
        this.classePrincipalParam = classePrincipalParam;
        this.diretorioClassesTO = diretorioClassesTO;
        this.diretorioClassesParam = diretorioClassesParam;
        this.classeIntermediariaERP = classeIntermediariaERP;
        this.classeIntermediariaRetaguarda = classeIntermediariaRetaguarda;

        iniciaClassesData();
    }

    public void setVariablesCondicionais(boolean addListaIntegracoes, boolean addTransporte, boolean addParam, boolean addTrataPendencia, boolean addIntegracoesProperties, boolean addObjectFactory, boolean addSuportaExporta, boolean addSuportaAtualiza, boolean addInsertDB) {
        this.addListaIntegracoes = addListaIntegracoes;
        this.addTransporte = addTransporte;
        this.addParam = addParam;
        this.addTrataPendencia = addTrataPendencia;
        this.addIntegracoesProperties = addIntegracoesProperties;
        this.addObjectFactory = addObjectFactory;
        this.addSuportaExporta = addSuportaExporta;
        this.addSuportaAtualiza = addSuportaAtualiza;

    }

    public void iniciaClassesData() throws IOException {
        if (classePrincipalTO != null || !(classePrincipalTO.length == 0)) {
            dataClassePrincipalTO = new DataFile[classePrincipalTO.length];
            for (int i = 0; i < classePrincipalTO.length; i++) {
                dataClassePrincipalTO[i] = new DataFile(new File(classePrincipalTO[i]));
            }
        } else {
            dataClassePrincipalTO = null;
        }
        if (!classePrincipalParam.equals("")) {
            dataClassePrincipalParam = new DataFile(new File(classePrincipalParam));
        } else {
            dataClassePrincipalParam = null;
        }
        dataClasseIntermediariaERP = new DataFile(new File(classeIntermediariaERP));
        dataClasseIntermediariaRetaguarda = new DataFile(new File(classeIntermediariaRetaguarda));

    }

    /**
     * @param args the command line arguments
     */
    //    public static void main2(String[] args) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    //        try {
    //            IntegradorTools.getINSTANCE().setVariables(555,777,555, 
    //                   "TESTE_EXPORT_PAIS_ERP", 
    //                   new String[]{"C:/wkspc-senior-geracao/IntegradorInterface/src/br/com/senior/integradorinterface/to/pais/PaisTO.java"},
    //                   "C:/wkspc-senior-geracao/IntegradorInterface/src/br/com/senior/integradorinterface/integracoes/pais/Param93.java",
    //                   "C:/wkspc-senior-geracao/IntegradorInterface/src/br/com/senior/integradorinterface/to/pais/",
    //                   "C:/wkspc-senior-geracao/IntegradorInterface/src/br/com/senior/integradorinterface/integracoes/pais/",
    //                   "C:/wkspc-senior-geracao/ERPAdapter/src/br/com/senior/erpadapter/intermediador/PaisInter.java",
    //                   "C:/wkspc-senior-geracao/RetaguardaAdapter/src/br/com/senior/retaguardaadapter/intermediador/PaisInter.java");
    //            
    //            Method m = IntegradorTools.getINSTANCE().getClass().getDeclaredMethod("criaNovaIntegracao");
    //            m.setAccessible(true);
    //            m.invoke(IntegradorTools.getINSTANCE());
    //        
    //        } catch (IOException ex) {
    //            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
    //        }
    //    }
    /**
     * @param args the command line arguments
     */
    //    public static void main(String[] args) throws IOException{
    //        if(verificaJaInserido(cfg.TRANSPORTE_PATH, new DataFile(new File(cfg.DEFAULT_PATH +"/IntegradorInterface/src/br/com/senior/integradorinterface/to/pais/PaisTO.java")), "xmlseealso")) {
    //			System.out.println("[PaisTO] J�? INSERIDO");
    //		} else {
    //			System.out.println("[PaisTO] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.PARAM_PATH, new DataFile(new File(cfg.DEFAULT_PATH +"/IntegradorInterface/src/br/com/senior/integradorinterface/integracoes/pais/Param93.java")), "xmlseealso")) {
    //			System.out.println("[Param93] J�? INSERIDO");
    //		} else {
    //			System.out.println("[Param93] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.RET_PROPERTIES_DEPARA_PATH, 105, "depara")) {
    //			System.out.println("[105] J�? INSERIDO");
    //		} else {
    //			System.out.println("[105] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.INTEGRADOR_WS_PATH, "EXPORTACAO_RETAGUARDA", "elseif_suportaatualiza")) {
    //			System.out.println("[EXPORTACAO_RETAGUARDA - suporta atualiza] J�? INSERIDO");
    //		} else {
    //			System.out.println("[EXPORTACAO_RETAGUARDA - suporta atualiza] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.INTEGRADOR_WS_PATH, "MOVIMENTO_ESTOQUE_ERP", "elseif_suportaexporta")) {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - suporta exporta] J�? INSERIDO");
    //		} else {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - suporta exporta] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.INTEGRADOR_WS_PATH, "MOVIMENTO_ESTOQUE_ERP", "elseif_suportaconsulta")) {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - suporta consulta] J�? INSERIDO");
    //		} else {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - suporta consulta] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.RETAGUARDA_ADAPTER_PATH, "MOVIMENTO_ESTOQUE_ERP", "elseif_atualiza")) {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - atualiza] J�? INSERIDO");
    //		} else {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - atualiza] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.RETAGUARDA_ADAPTER_PATH, "CEP_RETAGUARDA", "elseif_exporta")) {
    //			System.out.println("[CEP_RETAGUARDA - exporta] J�? INSERIDO");
    //		} else {
    //			System.out.println("[CEP_RETAGUARDA - exporta] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.RETAGUARDA_ADAPTER_PATH, "CONSULTA_PENDENCIAS", "elseif_consulta")) {
    //			System.out.println("[CONSULTA_PENDENCIAS - consulta] J�? INSERIDO");
    //		} else {
    //			System.out.println("[CONSULTA_PENDENCIAS - consulta] NÃO INSERIDO");
    //		}
    //        if(verificaJaInserido(cfg.TRATA_PENDENCIA_PATH, "MOVIMENTO_ESTOQUE_ERP", "elseif_tratapendencia")) {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - trata pendencia] J�? INSERIDO");
    //		} else {
    //			System.out.println("[MOVIMENTO_ESTOQUE_ERP - trata pendencia] NÃO INSERIDO");
    //		}
    //    }

    public void criaNovaIntegracaoERP_Retaguarda() {
        try {
            if (addListaIntegracoes) {
                adicionaAListaDeIntegracoes();
            }
            if (addTransporte) {
                adicionaTOSeeAlsoTransporte();
            }
            if (addParam) {
                ERP_RET_adicionaParamSeeAlsoParam();
            }
            if (addTrataPendencia) {
                ERP_RET_adicionaChamadoExportaTrataPendencia();
            }
            if (addIntegracoesProperties) {
                adicionaCodigoIntegracaoProperties(IntegradorToolsProperties.INSTANCE.ERP_PROPERTIES_DEPARA_PATH, codigoIntegracaoERP);
            }
            if (addObjectFactory) {
                adicionarAObjectFactory(diretorioClassesTO, diretorioClassesParam);
            }
            if (addSuportaExporta) {
                adicionarSuportaExportaTratamentoAoAdapter(IntegradorToolsProperties.INSTANCE.ERP_ADAPTER_PATH, dataClasseIntermediariaERP);
                adicionarSuportaExportaAoAdapter(IntegradorToolsProperties.INSTANCE.INTEGRADOR_WS_PATH);
            }
            if (addSuportaAtualiza) {
                adicionarSuportaAtualizaTratamentoAoAdapter(IntegradorToolsProperties.INSTANCE.RETAGUARDA_ADAPTER_PATH, dataClasseIntermediariaRetaguarda);
            }
        } catch (Exception ex) {
            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void criaNovaIntegracaoRetaguardaERP() {
        try {
            if (addListaIntegracoes) {
                adicionaAListaDeIntegracoes();
            }
            if (addTransporte) {
                adicionaTOSeeAlsoTransporte();
            }
            if (addIntegracoesProperties) {
                adicionaCodigoIntegracaoProperties(IntegradorToolsProperties.INSTANCE.RET_PROPERTIES_DEPARA_PATH, codigoIntegracaoRetaguarda);
            }
            if (addObjectFactory) {
                adicionarAObjectFactory(diretorioClassesTO);
            }
            if (addSuportaExporta) {
                adicionarSuportaExportaTratamentoAoAdapter(IntegradorToolsProperties.INSTANCE.RETAGUARDA_ADAPTER_PATH, dataClasseIntermediariaRetaguarda);
            }
            if (addSuportaAtualiza) {
                adicionarSuportaAtualizaAoAdapter(IntegradorToolsProperties.INSTANCE.INTEGRADOR_WS_PATH);
                adicionarSuportaAtualizaTratamentoAoAdapter(IntegradorToolsProperties.INSTANCE.ERP_ADAPTER_PATH, dataClasseIntermediariaERP);
            }

        } catch (Exception ex) {
            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void adicionaAListaDeIntegracoes() throws IOException {
        String replace = "\r\n";
        replace += codigoIntegracao + ",";
        replace += codigoIntegracaoERP + ",";
        replace += codigoIntegracaoRetaguarda + ",";
        replace += identificacaoDescricaoIntegracao + ",";
        replace += Util.arrayToString(classePrincipalTO, IntegradorToolsProperties.INSTANCE.SEPARADOR) + ",";
        replace += classePrincipalParam + ",";
        replace += diretorioClassesTO + ",";
        replace += diretorioClassesParam + ",";
        replace += classeIntermediariaERP + ",";
        replace += classeIntermediariaRetaguarda + "$1";
        if (!Util.existeConteudoArquivo(arquivoListaIntegracoes, identificacaoDescricaoIntegracao)) {
            Util.buscaInsere(arquivoListaIntegracoes, "([\\s]+/\\*nova integracao\\*/)", replace);
        }
    }

    private boolean buscaUltimaIntegracao() throws IOException {
        String conteudo = Util.lerArquivo(arquivoListaIntegracoes);
        String[] linhas = conteudo.split("\r\n");
        if (linhas.length > 1 && !linhas[linhas.length - 2].equals("")) {
            String[] colunas = linhas[linhas.length - 2].split(",");
            codigoIntegracao = Integer.parseInt(colunas[0]);
            codigoIntegracaoERP = Integer.parseInt(colunas[1]);
            codigoIntegracaoRetaguarda = Integer.parseInt(colunas[2]);
            identificacaoDescricaoIntegracao = colunas[3];
            classePrincipalTO = colunas[4].split(IntegradorToolsProperties.INSTANCE.SEPARADOR);
            classePrincipalParam = colunas[5];
            diretorioClassesTO = colunas[6];
            diretorioClassesParam = colunas[7];
            classeIntermediariaERP = colunas[8];
            classeIntermediariaRetaguarda = colunas[9];
        }
        return !conteudo.equals("");
    }

    /**
     * 4. Adicionar Classe"TO" ao "@XmlSeeAlso" da classe Transporte.
     * (“br.com.senior.integradorinterface.integracoes.Transporte�?)
     */
    private void adicionaTOSeeAlsoTransporte() throws IOException {
        System.out.println("Adicionando TO @XmlSeeAlso");
        if (!classePrincipalTO.equals("")) {
            for (int i = 0; i < dataClassePrincipalTO.length; i++) {
                DataFile dataFile = dataClassePrincipalTO[i];
                if (!verificaJaInserido(IntegradorToolsProperties.INSTANCE.TRANSPORTE_PATH, dataFile, "xmlseealso")) {
                    String replace = "$1$2,\n\t" + dataFile.getFullName() + ".class$7";
                    Util.buscaInsere(IntegradorToolsProperties.INSTANCE.TRANSPORTE_PATH, RegEx.REGEX_XMLSEEALSO2, replace);
                } else {
                    System.out.println("\tJá Inserido.(" + dataFile.getFileName() + ")");
                }
            }
        } else {
            if (classePrincipalTO.equals("")) {
                System.out.println("\t- Classe principal TO deve ser informada.");
            }
        }
        System.out.println("Adicionado @XmlSeeAlso TO");
    }

    /**
     * 5. Adicionar Param<CodIntegracao> ao "@XmlSeeAlso" de Param
     */
    private void ERP_RET_adicionaParamSeeAlsoParam() throws IOException {
        System.out.println("Adicionando Param @XmlSeeAlso");
        if (!classePrincipalParam.equals("")) {
            if (!verificaJaInserido(IntegradorToolsProperties.INSTANCE.PARAM_PATH, dataClassePrincipalParam, "xmlseealso")) {
                String replace = "$1$2,\n\t" + dataClassePrincipalParam.getFullName() + ".class$7";
                Util.buscaInsere(IntegradorToolsProperties.INSTANCE.PARAM_PATH, RegEx.REGEX_XMLSEEALSO2, replace);
            } else {
                System.out.println("\tJá Inserido.");
            }
        } else {
            if (classePrincipalParam.equals("")) {
                System.out.println("\t- Classe principal Param deve ser informada.");
            }
        }
        System.out.println("Adicionado @XmlSeeAlso Param");
    }

    /**
     * 6. Adicionar Tratamento nova Integração a thread de Tratamento de
     * pendências.(br.com.senior.integrador.pendencias.ThreadTrataPendencias, Método: buscaEnfileiraDadosPendentes)
     */
    private void ERP_RET_adicionaChamadoExportaTrataPendencia() throws IOException {
        System.out.println("Adicionando tratamento exporta Trata Pendência");
        if (!identificacaoDescricaoIntegracao.equals("")) {
            if (!classePrincipalParam.equals("")) {
                if (!verificaJaInserido(IntegradorToolsProperties.INSTANCE.TRATA_PENDENCIA_PATH, identificacaoDescricaoIntegracao, "elseif_tratapendencia")) {
                    String replace = "\n\t\t\t\t\telse if(tipoPendencia.getCodIntegracao() == TipoClass." + identificacaoDescricaoIntegracao + "){" + "\n\t\t\t\t\t\tparam = new " + dataClassePrincipalParam.getFullName() + "(codEmp,codFil,new " + dataClassePrincipalParam.getFullName() + "DadosAdicionais[0],tamanhoLote, \"A\");" + "\n\t\t\t\t\t}$1";
                    Util.buscaInsere(IntegradorToolsProperties.INSTANCE.TRATA_PENDENCIA_PATH, "([\\s]+" + RegEx.REGEX_ID_ELSEIF_TRATA_PENDENCIA + ")", replace);
                } else {
                    System.out.println("\t- Suporta exporta já foi inserido anteriormente.");
                }
            } else {
                if (classePrincipalParam.equals("")) {
                    System.out.println("\t- Classe principal Param deve ser informada.");
                }
            }
        } else {
            if (identificacaoDescricaoIntegracao.equals("")) {
                System.out.println("\t- Descrição da integração deve ser informada.");
            }
        }
        System.out.println("Adicionado exporta Trata Pendência");
    }

    /**
     * 7. Adicionar código de integração de para ao arquivo “Integracoes.properties�?.
     */
    private void adicionaCodigoIntegracaoProperties(String absolutePathDePara, int codigoIntegracaoAdapter) throws IOException {
        System.out.println("Adicionando Código de Integração ao Properties");
        if (codigoIntegracaoAdapter > 0 && codigoIntegracao > 0) {
            if (!verificaJaInserido(absolutePathDePara, codigoIntegracao, "depara")) {
                //                String replace = "$1$2\n" + codigoIntegracaoAdapter +" = " + codigoIntegracao;
                //                Util.buscaInsere(absolutePathDePara, "(#CODIGO ERP = CODIGO INTEGRADOR)?([\\s0-9=]+)", replace);
                adicionaArquivo(absolutePathDePara, null, "\n" + codigoIntegracaoAdapter + " = " + codigoIntegracao);
            } else {
                System.out.println("\t- Código Já Inserido.");
            }
        } else {
            if (!(codigoIntegracao > 0)) {
                System.out.println("\t- Código de integracão é inválido.");
            }
            if (!(codigoIntegracaoAdapter > 0)) {
                System.out.println("\t- Código de integracão Adapter é inválido.");
            }
        }
        System.out.println("Adicionado Integração ao Properties");
    }

    /**
     * 14. Adicionar <Classe>TO e Param<CodIntegração> (E FILHOS) ao “ObjectFactory�?.
     * (br.com.senior.integradorws.servicos.ObjectFactory)
     */
    private void adicionarAObjectFactory(String... diretorios) throws IOException {
        System.out.println("Adicionando a ObjectFactory");
        //        String replace = "\n\n\t/**"
        //                + "\n\t* Create an instance of {@link "+entidadeTransporte.getName()+" }"
        //                + "\n\t*"
        //                + "\n\t*/"
        //                + "\n\tpublic "+entidadeTransporte.getName()+" create"+entidadeTransporte.getName()+"() {"
        //                + "\n\t\treturn new "+entidadeTransporte.getName()+"();"
        //                + "\n\t}$1";
        //        Util.buscaInsere(OBJECT_FACTORY_PATH, "([\\s]+/\\*create instance of class TO\\*/)", replace);        
        //        replace = "\n\n\t/**"
        //                + "\n\t* Create an instance of {@link "+nomeParam.getName()+" }"
        //                + "\n\t*"
        //                + "\n\t*/"
        //                + "\n\tpublic "+nomeParam.getName()+" create"+nomeParam.getName()+"() {"
        //                + "\n\t\treturn new "+nomeParam.getName()+"();"
        //                + "\n\t}$1";
        //        Util.buscaInsere(OBJECT_FACTORY_PATH, "([\\s]+/\\*create instance of class Param\\*/)", replace);
        if (diretorios.length > 0) {
            ObjectFactory objectFactory = new ObjectFactory(true);
            String replace = objectFactory.generateCreateClasses(diretorios, IntegradorToolsProperties.INSTANCE.OBJECT_FACTORY_PATH);
            replace += "$1";
            Util.buscaInsere(IntegradorToolsProperties.INSTANCE.OBJECT_FACTORY_PATH, "([\\s]+" + RegEx.REGEX_ID_NEW_OBJECTFACTORY + ")", replace);
        } else {
            System.out.println("\t- Diretório deve-se informar ao menos um diretório.");
        }
        System.out.println("Adicionado ObjectFactory");
    }

  

    public Connection getConnection() throws SQLException {
        try {
            System.out.println("\tConectando ao banco");
            Class.forName(IntegradorToolsProperties.INSTANCE.DRIVER_DB);
            return DriverManager.getConnection(IntegradorToolsProperties.INSTANCE.URL_DB, IntegradorToolsProperties.INSTANCE.USUARIO_DB, IntegradorToolsProperties.INSTANCE.SENHA_DB);
        } catch (ClassNotFoundException e) {
            throw new SQLException(e.getMessage());
        }

    }

    private void adicionaArquivo(String fileName, String insereAntes, String insereDepois) throws IOException {
        String contentBefore = Util.lerArquivo(fileName);
        String contentAfter = (insereAntes != null ? insereAntes : "") + contentBefore + (insereDepois != null ? insereDepois : "");
        if (contentBefore.equals(contentAfter)) {
            System.out.println("\t- Não houve alteração no documento.");
        } else {
            Util.escreverArquivo(contentAfter, fileName);
        }
    }

    public int getCodigoIntegracao() {
        return codigoIntegracao;
    }

    public void setCodigoIntegracao(int codigoIntegracao) {
        this.codigoIntegracao = codigoIntegracao;
    }

    public String getDescricaoIntegracao() {
        return identificacaoDescricaoIntegracao;
    }

    public void setDescricaoIntegracao(String descricaoIntegracao) {
        identificacaoDescricaoIntegracao = descricaoIntegracao;
    }

    public int getCodigoIntegracaoERP() {
        return codigoIntegracaoERP;
    }

    public void setCodigoIntegracaoERP(int codigoIntegracaoERP) {
        this.codigoIntegracaoERP = codigoIntegracaoERP;
    }

    public String getClasseIntermediariaERP() {
        return classeIntermediariaERP;
    }

    public void setClasseIntermediariaERP(String classeIntermediariaERP) {
        this.classeIntermediariaERP = classeIntermediariaERP;
    }

    public String getClasseIntermediariaRetaguarda() {
        return classeIntermediariaRetaguarda;
    }

    public void setClasseIntermediariaRetaguarda(String classeIntermediariaRetaguarda) {
        this.classeIntermediariaRetaguarda = classeIntermediariaRetaguarda;
    }

    public String getClassePrincipalParam() {
        return classePrincipalParam;
    }

    public void setClassePrincipalParam(String classePrincipalParam) {
        this.classePrincipalParam = classePrincipalParam;
    }

    public String[] getClassePrincipalTO() {
        return classePrincipalTO;
    }

    public void setClassePrincipalTO(String[] classePrincipalTO) {
        this.classePrincipalTO = classePrincipalTO;
    }

    public DataFile[] getDataClassePrincipalTO() {
        return dataClassePrincipalTO;
    }

    public String getDiretorioClassesParam() {
        return diretorioClassesParam;
    }

    public void setDiretorioClassesParam(String diretorioClassesParam) {
        this.diretorioClassesParam = diretorioClassesParam;
    }

    public String getDiretorioClassesTO() {
        return diretorioClassesTO;
    }

    public void setDiretorioClassesTO(String diretorioClassesTO) {
        this.diretorioClassesTO = diretorioClassesTO;
    }

    public String getIdentificacaoDescricaoIntegracao() {
        return identificacaoDescricaoIntegracao;
    }

    public void setIdentificacaoDescricaoIntegracao(String identificacaoDescricaoIntegracao) {
        this.identificacaoDescricaoIntegracao = identificacaoDescricaoIntegracao;
    }

    public DataFile getDataClasseIntermediariaERP() {
        return dataClasseIntermediariaERP;
    }

    public DataFile getDataClasseIntermediariaRetaguarda() {
        return dataClasseIntermediariaRetaguarda;
    }

    public DataFile getDataClassePrincipalParam() {
        return dataClassePrincipalParam;
    }

    public int getCodigoIntegracaoRetaguarda() {
        return codigoIntegracaoRetaguarda;
    }

    public void setCodigoIntegracaoRetaguarda(int codigoIntegracaoRetaguarda) {
        this.codigoIntegracaoRetaguarda = codigoIntegracaoRetaguarda;
    }

    public static boolean verificaJaInserido(String absolutePathFile, Object objectCompare, String tipo) {
        boolean jaInserido = false;
        try {
            String contentFile = Util.lerArquivo(absolutePathFile);

            if (tipo.endsWith("xmlseealso")) {
                DataFile dataClasseFile = (DataFile) objectCompare;
                String imports = "";
                String xmlSeeAlso = "";
                Pattern patternXmlSeeAlso = Pattern.compile(RegEx.REGEX_XMLSEEALSO);
                Matcher matcherXmlSeeAlso = patternXmlSeeAlso.matcher(contentFile);
                Pattern patternImports = Pattern.compile(RegEx.REGEX_IMPORT);
                Matcher matcherImports = patternImports.matcher(contentFile);

                if (matcherXmlSeeAlso.find()) {
                    xmlSeeAlso = matcherXmlSeeAlso.group();
                }
                while (matcherImports.find()) {
                    imports += matcherImports.group();
                }

                if (xmlSeeAlso.indexOf(dataClasseFile.getFullName() + ".class") != -1) {
                    jaInserido = true;
                } else if (xmlSeeAlso.contains(dataClasseFile.getFileName() + ".class")) {
                    if (imports.contains(dataClasseFile.getFullName())) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("depara")) {
                Integer codigoIntegracao = (Integer) objectCompare;

                Pattern patternImports = Pattern.compile(RegEx.REGEX_DEPARA_INTEGRACOES, Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().replaceAll("^[\\d]+[\\s]*=?", "").trim().equals(codigoIntegracao.toString())) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_atualiza")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+" + RegEx.REGEX_ID_ELSEIF_ATUALIZA + ")", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_exporta")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+" + RegEx.REGEX_ID_ELSEIF_EXPORTA + ")", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_consulta")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+" + RegEx.REGEX_ID_ELSEIF_CONSULTA + ")", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_suportaatualiza")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+" + RegEx.REGEX_ID_ELSEIF_SUPORTA_ATUALIZA + ")", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_suportaconsulta") || tipo.equals("elseif_suportaexporta")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+(" + RegEx.REGEX_ID_ELSEIF_SUPORTA_EXPORTA + "|" + RegEx.REGEX_ID_ELSEIF_SUPORTA_CONSULTA + "))", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            } else if (tipo.equals("elseif_tratapendencia")) {
                String identificacaoDescricaoIntegracao = (String) objectCompare;

                Pattern patternImports = Pattern.compile("(" + RegEx.REGEX_ELSEIF_GERAL + "+" + RegEx.REGEX_ID_ELSEIF_TRATA_PENDENCIA + ")", Pattern.MULTILINE);
                Matcher matcherImports = patternImports.matcher(contentFile);

                while (matcherImports.find()) {
                    if (matcherImports.group().contains(identificacaoDescricaoIntegracao)) {
                        jaInserido = true;
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("ARQUIVO NAO EXISTE(" + absolutePathFile + ")");
            Logger.getLogger(IntegradorTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jaInserido;
    }

    private void adicionaSuportaAtualiza(String identificacaoDescricaoIntegracao, String absolutePathAdapter) throws IOException {
        String replace = "\n\t\t\telse if(param.getCodIntegracao() == TipoClass." + identificacaoDescricaoIntegracao + "){" + "\n\t\t\t\tsuporta = true;" + "\n\t\t\t}" + "$1";
        System.out.println("\t- Adicina suporta.");
        Util.buscaInsere(absolutePathAdapter, "([\\s]+" + RegEx.REGEX_ID_ELSEIF_SUPORTA_ATUALIZA + ")", replace);
    }

    private void adicionaSuportaExporta(String identificacaoDescricaoIntegracao, String absolutePathAdapter) throws IOException {
        String replace = "\n\t\t\telse if(param.getCodIntegracao() == TipoClass." + identificacaoDescricaoIntegracao + "){" + "\n\t\t\t\tsuporta = true;" + "\n\t\t\t}" + "$1";
        System.out.println("\t- Adicina suporta.");
        Util.buscaInsere(absolutePathAdapter, "([\\s]+" + RegEx.REGEX_ID_ELSEIF_SUPORTA_EXPORTA + ")", replace);
    }

    private void adicionarSuportaExportaTratamentoAoAdapter(String absolutePathAdapter, DataFile dataClasseIntermediaria) throws IOException {
        File fileAdapter = new File(absolutePathAdapter);
        System.out.println("Adicionando suporta exporta + tratamento. " + fileAdapter.getName());
        if (!identificacaoDescricaoIntegracao.equals("")) {
            if (!dataClasseIntermediaria.equals("")) {
                if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_exporta")) {
                    String replace = "\n\t\telse if(paramExporta.getCodIntegracao() == TipoClass." + identificacaoDescricaoIntegracao + "){" + "\n\t\t\tresult = " + dataClasseIntermediaria.getFullName() + ".exporta(paramExporta);" + "\n\t\t}" + "$1";
                    System.out.println("\t- Adicina tratamento.");
                    Util.buscaInsere(absolutePathAdapter, "([\\s]+" + RegEx.REGEX_ID_ELSEIF_EXPORTA + ")", replace);
                } else {
                    System.out.println("\t- Exporta já foi inserido anteriormente.");
                }
            } else {
                if (dataClasseIntermediaria.equals("")) {
                    System.out.println("\t- Classe intermediária deve ser informada.");
                }
            }
            if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_suportaexporta")) {
                adicionaSuportaExporta(identificacaoDescricaoIntegracao, absolutePathAdapter);
            } else {
                System.out.println("\t- Suporta exporta já foi inserido anteriormente.");
            }
        } else {
            if (identificacaoDescricaoIntegracao.equals("")) {
                System.out.println("\t- Descrição da integração deve ser informada.");
            }
        }
        System.out.println("Adicionado suporta exporta + tratamento. " + fileAdapter.getName());
    }

    private void adicionarSuportaAtualizaTratamentoAoAdapter(String absolutePathAdapter, DataFile dataClasseIntermediaria) throws IOException {
        File fileAdapter = new File(absolutePathAdapter);
        System.out.println("Adicionando suporta atualiza + tratamento. " + fileAdapter.getName());
        if (!identificacaoDescricaoIntegracao.equals("")) {
            if (!dataClasseIntermediaria.equals("")) {
                if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_atualiza")) {
                    System.out.println("\t- Adicina tratamento.");
                    String replace = "\n\t\telse if(paramAtualiza.getCodIntegracao() == TipoClass." + identificacaoDescricaoIntegracao + "){" + "\n\t\t\tresult = " + dataClasseIntermediaria.getFullName() + ".atualiza(paramAtualiza);" + "\n\t\t}" + "$1";
                    Util.buscaInsere(absolutePathAdapter, "([\\s]+" + RegEx.REGEX_ID_ELSEIF_ATUALIZA + ")", replace);
                } else {
                    System.out.println("\t- Atualiza já foi inserido anteriormente.");
                }
            } else {
                if (dataClasseIntermediaria.equals("")) {
                    System.out.println("\t- Classe intermediária deve ser informada.");
                }
            }
            if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_suportaatualiza")) {
                adicionaSuportaAtualiza(identificacaoDescricaoIntegracao, absolutePathAdapter);
            } else {
                System.out.println("\t- Suporta atualiza já foi inserido anteriormente.");
            }
        } else {
            if (identificacaoDescricaoIntegracao.equals("")) {
                System.out.println("\t- Descrição da integração deve ser informada.");
            }
        }
        System.out.println("Adicionado suporta atualiza + tratamento. " + fileAdapter.getName());
    }

    private void adicionarSuportaExportaAoAdapter(String absolutePathAdapter) throws IOException {
        File fileAdapter = new File(absolutePathAdapter);
        System.out.println("Adicionando suporta a " + fileAdapter.getName());
        if (!identificacaoDescricaoIntegracao.equals("")) {
            if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_suportaexporta")) {
                adicionaSuportaExporta(identificacaoDescricaoIntegracao, absolutePathAdapter);
            } else {
                System.out.println("\t- Suporta exporta já foi inserido anteriormente.");
            }
        } else {
            if (identificacaoDescricaoIntegracao.equals("")) {
                System.out.println("\t- Descrição da integração deve ser informada.");
            }
        }
        System.out.println("Adicionado suporta " + fileAdapter.getName());
    }

    private void adicionarSuportaAtualizaAoAdapter(String absolutePathAdapter) throws IOException {
        File fileAdapter = new File(absolutePathAdapter);
        System.out.println("Adicionando suporta atualiza a " + fileAdapter.getName());
        if (!identificacaoDescricaoIntegracao.equals("")) {
            if (!verificaJaInserido(absolutePathAdapter, identificacaoDescricaoIntegracao, "elseif_suportaatualiza")) {
                adicionaSuportaAtualiza(identificacaoDescricaoIntegracao, absolutePathAdapter);
            } else {
                System.out.println("\t- Suporta atualiza já foi inserido anteriormente.");
            }
        } else {
            if (identificacaoDescricaoIntegracao.equals("")) {
                System.out.println("\t- Descrição da integração deve ser informada.");
            }
        }
        System.out.println("Adicionado suporta atualiza " + fileAdapter.getName());
    }

}
