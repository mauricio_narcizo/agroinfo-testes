package br.com.integradortools.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;


public class ObjectFactory {
	private String contentFile = "";
    private boolean caminhosAbsolutos;

    public ObjectFactory(boolean caminhosAbsolutos) {
        this.caminhosAbsolutos = caminhosAbsolutos;
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ObjectFactory object = new ObjectFactory(true);
        String add = object.generateCreateClasses(new String[]{"C:\\wkspc-senior\\IntegradorInterface\\src\\br\\com\\senior\\integradorinterface\\integracoes",
                "C:\\wkspc-senior\\IntegradorInterface\\src\\br\\com\\senior\\integradorinterface\\to"},
                "C:\\wkspc-senior\\IntegradorWSAdapter\\src\\br\\com\\senior\\integradorws\\servicos\\ObjectFactory.java");
        System.out.println(add);
    }
    public String generateCreateClasses(String[] filesName, String fileCompare) throws FileNotFoundException, IOException{
        StringBuilder generatedCreateClasses = new StringBuilder();
        boolean copareclass = false;
        List<File> files = new ArrayList<File>();
        List<DataFile> filesWithoutCreateClass = new ArrayList<DataFile>();
        for (String fileName : filesName) {
            files.addAll(Util.listFilesByFolder(new File(fileName)));
        }
        if(fileCompare != null && !fileCompare.equals("")){
            copareclass = true;
            contentFile = IOUtils.toString(new FileInputStream(fileCompare), "UTF-8");
        }
        DataFile dataFile; 
        for (File file : files) {
            dataFile = new DataFile(file);
            if(copareclass){
                if(!contentFile.contains("create"+(dataFile.getXmlName()!= null?dataFile.getXmlName():dataFile.getFileName()).replace("_", ""))) {
					filesWithoutCreateClass.add(dataFile);
				}
            }
            else{
                filesWithoutCreateClass.add(dataFile);                
            }        }
        for (DataFile dataFileFor : filesWithoutCreateClass) {
            for (DataFile dataFileForTest : filesWithoutCreateClass) {
                if(dataFileForTest != dataFileFor &&
                        dataFileForTest.getFileName().equals(dataFileFor.getFileName()) &&
                        dataFileForTest.getXmlName() == null){
                    dataFileForTest.setXmlName(dataFileForTest.getFileName());
                }
            }
            generatedCreateClasses.append("\n");
            generatedCreateClasses.append("\n\t/**");
            generatedCreateClasses.append("\n\t* Create an instance of {@link "+(dataFileFor.getXmlName()!= null || caminhosAbsolutos?dataFileFor.filePackage+".":"")+dataFileFor.getFileName()+" }");
            generatedCreateClasses.append("\n\t*");
            generatedCreateClasses.append("\n\t*/");
            generatedCreateClasses.append("\n\tpublic "+(dataFileFor.getXmlName()!= null || caminhosAbsolutos?dataFileFor.filePackage+".":"")+dataFileFor.getFileName()+" create"+(dataFileFor.getXmlName()!= null?dataFileFor.getXmlName():dataFileFor.getFileName()).replace("_", "")+"() {");
            generatedCreateClasses.append("\n\t\treturn new "+(dataFileFor.getXmlName()!= null || caminhosAbsolutos?dataFileFor.filePackage+".":"")+dataFileFor.getFileName()+"();");
            generatedCreateClasses.append("\n\t}");
        }
        return generatedCreateClasses.toString();
    }

}
