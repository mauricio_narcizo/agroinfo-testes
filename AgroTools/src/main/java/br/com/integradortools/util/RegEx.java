package br.com.integradortools.util;


public class RegEx {
	public static final String REGEX_ELSEIF_GERAL = "(((else\\s*)?if)\\s*(\\([\\[\\]\"'\\w\\s_=.()|]+\\))\\s*(\\{[\\[\\]\"'\\w\\s_=.(),;]+\\})\\s*)";
    public static final String REGEX_ID_ELSEIF_ATUALIZA = "(/\\*else if atualiza\\*/)";
    public static final String REGEX_ID_ELSEIF_EXPORTA = "(/\\*else if exporta\\*/)";
    public static final String REGEX_ID_ELSEIF_CONSULTA = "(/\\*else if consulta\\*/)";
    public static final String REGEX_ID_ELSEIF_SUPORTA_ATUALIZA = "(/\\*else if suporta atualiza\\*/)";
    public static final String REGEX_ID_ELSEIF_SUPORTA_EXPORTA = "(/\\*else if suporta exporta\\*/)";
    public static final String REGEX_ID_ELSEIF_SUPORTA_CONSULTA = "(/\\*else if suporta consulta\\*/)";
    public static final String REGEX_ID_ELSEIF_TRATA_PENDENCIA = "(/\\*else if tratapendencia\\*/)";
    public static final String REGEX_ID_NEW_OBJECTFACTORY = "(/\\*news objects\\*/)";
    public static final String REGEX_XMLSEEALSO = "(@XmlSeeAlso\\(\\{)([A-z_.0-9\\s]+,?)*(\\}\\))";
    public static final String REGEX_XMLSEEALSO2 = "(@XmlSeeAlso\\s*\\(\\s*\\{)\\s*((\\w+\\s*(.\\s*\\w+\\s*)*.\\s*class)(\\s*,\\s*\\w+\\s*(.\\s*\\w+\\s*)*.\\s*class)*)?\\s*(\\}\\s*\\))";
    public static final String REGEX_IMPORT = "(import[\\s]+[A-z_.0-9\\s]+;)";
    public static final String REGEX_DEPARA_INTEGRACOES = "(^[\\d]+[\\s]*=?[\\s]*[\\d]+[\\s]*$)";
    public static final String REGEX_EXT_IMP = "class\\s+\\w+(\\s+extends\\s+\\w+)?(\\s+implements\\s+(\\w+(\\s*,\\s*\\w+)*))?\\s*\\{";
    public static final String REGEX_XMLTYPE = "(@XmlType\\s*\\(\\s*name\\s*=\\s*\")(\\w+\")(.+)(\\s*(public(\\s+final)?|abstract)?\\s+class)";
}
