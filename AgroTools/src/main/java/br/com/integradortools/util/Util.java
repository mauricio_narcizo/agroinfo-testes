package br.com.integradortools.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;


public class Util {
	public static String lerArquivo(String fileName) throws IOException{
        String conteudo = "";
        File file = new File(fileName);
        if(file.exists()){
	        FileInputStream fis = new FileInputStream(fileName);
	        conteudo = IOUtils.toString(fis, "UTF-8");     
	        fis.close();
        }
        return conteudo;
    }
	public static String[] lerLinhasArquivo(String fileName) throws IOException{
		String conteudo = lerArquivo(fileName);
		return conteudo.split("\r\n");
	}
    public static String arrayToString(Object[] array, String separador){
        StringBuilder builder = new StringBuilder();
        for (Object string : array) {
            if(builder.length() != 0) {
				builder.append(separador);
			}
            builder.append(string);
        }
        return builder.toString();
    }
    public static void escreverArquivo(String conteudo, String fileName) throws IOException{
        FileOutputStream fos = new FileOutputStream(fileName);
        IOUtils.write(conteudo, fos, "UTF-8");
        fos.close();
    }
    public static boolean existeConteudoArquivo(String fileName, String conteudoBusca) throws IOException{
        String content = lerArquivo(fileName);
        return content.matches(conteudoBusca);
    }
    public static List<File> listFilesByFolder(final File folder) {
        List<File> files = new ArrayList<File>();
        
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                if(!fileEntry.getName().equals("CVS")) {
					files.addAll(listFilesByFolder(fileEntry));
				}
            } else {
                    files.add(fileEntry);
            }
        }
        return files;
    }
    public static void buscaInsere(String fileName, String conteudoBusca, String conteudoInsere) throws IOException{
        String contentBefore = Util.lerArquivo(fileName);
        String contentAfter = contentBefore.replaceAll(conteudoBusca, conteudoInsere.replace("\\", "/"));
        
//        Pattern patternXmlSeeAlso = Pattern.compile(RegEx.REGEX_XMLSEEALSO);
//        Matcher matcherXmlSeeAlso = patternXmlSeeAlso.matcher(contentBefore);
//        String xmlSeeAlso = "";
//        if(matcherXmlSeeAlso.find()){
//          xmlSeeAlso = matcherXmlSeeAlso.group();
//        }
//        System.out.println(xmlSeeAlso);
        if(contentBefore.equals(contentAfter)){
            System.out.println("\t- Não houve alteração no documento.");
        } else {
			Util.escreverArquivo(contentAfter, fileName);
		}
    }
}
