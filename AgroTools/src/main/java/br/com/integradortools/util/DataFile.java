package br.com.integradortools.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;


public class DataFile {
	public String fileName;
    public String xmlName = null;
    public File file;
    public String fileContent;
    public String filePackage;
    public boolean loaded = false;
    public List<String> imports;
    public String extend;
    public List<String> implements_;
    
    public DataFile(String fileName) throws IOException {
        this(new File(fileName));
    }
    public DataFile(File file) throws IOException {
        fileName = file.getName().substring(0,file.getName().lastIndexOf('.'));
        this.file = file;   
        try {
            setFileContent(IOUtils.toString(new FileInputStream(this.file), "UTF-8"));
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        }
        if(fileContent != null){
            //@XmlType(name="ProdutoNotaTO") public class 
            Pattern pattern = Pattern.compile(RegEx.REGEX_XMLTYPE);
            Matcher matcher = pattern.matcher(fileContent);
            if(matcher.find()) {
                if(!matcher.group(2).equals("")) {
					xmlName = matcher.group(2);
				}
            }
            
            Pattern patternImports = Pattern.compile(RegEx.REGEX_IMPORT);
            Matcher matcherImports = patternImports.matcher(fileContent);
            imports = new ArrayList<String>();
            while(matcherImports.find()) {
                imports.add(matcherImports.group());
            }
            
            Pattern patternExtImp = Pattern.compile(RegEx.REGEX_EXT_IMP);
            Matcher matcherExtImp = patternExtImp.matcher(fileContent);
            implements_ = new ArrayList<String>();
            extend = "";
            if(matcherExtImp.find()) {
                extend = matcherExtImp.group(1);
                if(extend != null){
                    extend = extend.isEmpty()?"":extend.replace("extends", "").trim();
                }
                else{
                    extend = "";
                }
                String imp = matcherExtImp.group(2);
                if(imp != null){
                    imp = imp.isEmpty()?"":imp.replace("implements", "").trim();

                    Pattern patternImps = Pattern.compile("(\\w+)");
                    Matcher matcherImps = patternImps.matcher(imp);
                    while (matcherImps.find()) {                    
                        implements_.add(matcherImps.group());
                    }
                }
//                implements_;
            }
            
            Pattern patternPackge = Pattern.compile("package ([0-9._A-z]+)");
            Matcher matcherPackge = patternPackge.matcher(fileContent);
            if(matcherPackge.find()) {
                filePackage = matcherPackge.group(1);
            } 
        }
        else{
            throw new IOException("Nao foi possivel ler o arquivo.");
        }
    }
    
    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        loaded = true;
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public String getFilePackage() {
        return filePackage;
    }

    public void setFilePackage(String filePackage) {
        this.filePackage = filePackage;
    }

    public String getXmlName() {
        return xmlName;
    }

    public void setXmlName(String xmlName) {
        this.xmlName = xmlName;
    }    
    
    public String getFullName() {
        return getFilePackage()+"."+getFileName();
    }
}
