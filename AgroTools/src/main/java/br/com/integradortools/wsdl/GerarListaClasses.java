package br.com.integradortools.wsdl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import br.com.integradortools.config.IntegradorToolsProperties;


public class GerarListaClasses {
    private String caminhoRealPath = "br\\com\\senior\\services\\";
    private String caminhoTempPath = "br\\com\\senior\\services_\\";
    private boolean doFindReplace = false;

    public GerarListaClasses() {
    }
    
    public GerarListaClasses(String caminhoRealPath,String caminhoTempPath) {
        this.caminhoRealPath = caminhoRealPath;
        this.caminhoTempPath = caminhoTempPath;
    }
    
//    public static void main(String[] args) throws FileNotFoundException, IOException {
//        if(args == null || args.length == 0){
//            args = new String[3];
//            args[0] = "..\\ERPAdapter\\src\\";
//            args[1] = ".\\src\\WSDL_Gerar.csv";
//        }
//        String baseOutPut = args[0];
//        String fileWSDLPaths = args[1];
//        GerarListaClasses gerarListaClasses = new GerarListaClasses();
//        gerarListaClasses.initGenerateByFile(baseOutPut, fileWSDLPaths);    
//    }
    
//    public void initGenerate(String baseOutPut, String fileWSDLPaths) throws IOException {
//        BufferedReader in1   = new BufferedReader(new FileReader(fileWSDLPaths));
//        String str;
//        String[] lineColums;
//        
//        File realPath = new File(baseOutPut + caminhoRealPath);
//        File tempPath = new File(baseOutPut + caminhoTempPath);
//        if(realPath.exists())
//            deleteDir(realPath);
//        if(tempPath.exists())
//            deleteDir(tempPath);
//        
//        int qtde = 0;
//        while (in1.ready()) {
//            str = in1.readLine();
//            lineColums = str.split(",");
//            
//            if(lineColums[0].matches("^\\d$")){
//                System.out.println("Gerando : " + lineColums[1]);
//                WSDL2Java.main(new String[]{"-o",baseOutPut,""+lineColums[1].trim()});
//    //            WSDL2Java.main(new String[]{"-o",baseOutPut,"http://127.0.0.1:9090/g5-senior-services/sapiens_Synccom_senior_g5_co_int_varejo_bancos?wsdl"});
//
//                if(qtde > 0)
//                    MergeWS_2_0.principal(new String[]{baseOutPut+caminhoTempPath,baseOutPut+caminhoRealPath});
//                else{
//                    realPath.renameTo(tempPath);
//                }
//
//                qtde++;
//            }
//        }
//        in1.close();
//        realPath.delete();
//        tempPath.renameTo(realPath);
//        if (doFindReplace) {
//            findReplaceG5SeniorServicesLocator(baseOutPut);
//        }
//    }
    public void initGenerateByFile(String baseOutPut, String fileWSDLPath) throws IOException {
        BufferedReader in1   = new BufferedReader(new FileReader(fileWSDLPath));
        List<String> wsdls = new ArrayList<String>();
        while (in1.ready()) {
            String[] lineColums = in1.readLine().split(IntegradorToolsProperties.INSTANCE.CSV_SEPARATOR);
            
            if(lineColums[0].matches("^\\d$")){
                wsdls.add(lineColums[1]);
            }
        }
        in1.close();
        initGenerateByArrayWsdls(baseOutPut, wsdls.toArray(new String[0]));
    }
    public void initGenerateByArrayWsdls(String baseOutPut, String[] wsdls) throws IOException {
        File realPath = new File(baseOutPut + caminhoRealPath);
        File realPathCVS = new File(realPath.getAbsolutePath() + "/CVS");
        File tempPath = new File(baseOutPut + caminhoTempPath);
        File realPathDelete = new File(realPath.getAbsolutePath() + "temp_delete");
        File realPathDeleteCVS = new File(realPathDelete.getAbsolutePath() + "/CVS");
        if(realPath.exists()) {
            realPath.renameTo(realPathDelete);
		}
        if(tempPath.exists()) {
			deleteDir(tempPath);
		}
        
        try {
            int qtde = 0;
            for (String wsdl : wsdls) {
                System.out.println("Gerando : " + wsdl);
                WSDL2Java wsdl2Java = new WSDL2Java();
                    wsdl2Java.run(new String[]{"-o",baseOutPut,""+wsdl.trim()});
   
                if(qtde > 0) {
                	String[] strings = new String[]{baseOutPut+caminhoTempPath,baseOutPut+caminhoRealPath};
    				MergeWS_2_0.principal(strings);
    			} else{
                    realPath.renameTo(tempPath);
                }
    
                qtde++;            
            }
            
            deleteDir(realPath);
            tempPath.renameTo(realPath);
            if(realPathDeleteCVS.exists() && !realPathCVS.exists()){
                realPathDeleteCVS.renameTo(realPathCVS);
            }                
            deleteDir(realPathDelete);
        } catch (Exception e) {
            deleteDir(tempPath);
            deleteDir(realPath);
            realPathDelete.renameTo(realPath);
            // TODO Auto-generated catch block
            throw new RuntimeException(e);
        }
        
        if (doFindReplace) {
            findReplaceG5SeniorServicesLocator(baseOutPut);
        }
    }
    public void findReplaceG5SeniorServicesLocator(String baseOutPut) throws IOException {
        String fileName = baseOutPut + caminhoRealPath + "G5SeniorServicesLocator.java";
        String content = IOUtils.toString(new FileInputStream(fileName), "UTF-8");
        content = content.replaceAll("[\"][A-z:]*[/]{2}[A-z0-9:]*", "br.com.senior.erpadapter.ErpAdapterProperties.getHostPort(\"ERP_URL\") + \"");
        IOUtils.write(content, new FileOutputStream(fileName), "UTF-8");
    }
    
    public boolean deleteDir(File dir) {
            if (dir.exists() && dir.isDirectory()) {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++) {
                            boolean success = deleteDir(new File(dir, children[i]));
                            if (!success) {
                                    return false;
                            }
                    }
            }
            return dir.delete();
    }

    public boolean isDoFindReplace() {
        return doFindReplace;
    }

    public void setDoFindReplace(boolean doFindReplace) {
        this.doFindReplace = doFindReplace;
    }    
}
