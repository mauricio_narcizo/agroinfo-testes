package br.com.integradortools.wsdl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class MergeWS {
	private enum Tipo {
        INICIAL,PROXY,WSDD,PORT,PORT_IF,PORT2,PORT_IF2,END_POINT, PORTS, END_POINT_IF
    }
    public static void principal(String[] args) {
        if(args == null || args.length == 0){
            args = new String[3];
            args[0] = "C:\\wkspc-senior\\ERPAdapter\\src\\br\\com\\senior\\services_\\";//
            args[1] = "C:\\wkspc-senior\\ERPAdapter\\src\\br\\com\\senior\\services\\";
            args[2] = "C:\\Webservices\\";
        }
        mergeG5SeniorServicesLocator(args);
        mergeG5SeniorServices(args);
        copyFiles(args);
    }
    public static void mergeG5SeniorServices(String[] args) {
            
        try {
            BufferedWriter merge  = new BufferedWriter(new FileWriter(args[2] + "G5SeniorServices.java"));
            BufferedReader in1   = new BufferedReader(new FileReader(args[0] + "G5SeniorServices.java"));
//            BufferedReader in2   = new BufferedReader(new FileReader(args[1]));
                String str;
                while (in1.ready()) {
                    str = in1.readLine();
                    merge.write(str + "\n");
                    if(str.indexOf("{") != -1) {
						merge.write(getMetodosInterface(args[1]));
					}
                }
                in1.close();
                merge.close();

        } catch (IOException e) {
        }        
        // TODO code application logic here
    }
    private static String getMetodosInterface(String file2){
        String strRet = "";
        try {
            BufferedReader in1   = new BufferedReader(new FileReader(file2+"G5SeniorServices.java"));
//            BufferedReader in2   = new BufferedReader(new FileReader(args[1]));
                String str;
                while (in1.ready()) {
                    str = in1.readLine();
                    if(str.indexOf("{") != -1){
                        while (in1.ready()) {
                            str = in1.readLine();
                            if(str.indexOf("}") != -1) {
								break;
							}
                            strRet += str + "\n";
                        }
                        break;
                    }
                    
                }
                in1.close();
                
        } catch (IOException e) {
        }
        return strRet;
    }
    /**
     * @param args the command line arguments
     */
    public static void copyFiles(String[] args) {
        String newFolderUrl = args[2];
        String oldFolderUrl = args[1];
        File oldFolder = new File(oldFolderUrl);
        File[] listOfFiles = oldFolder.listFiles(); 
        File fileMoving;
 
        for (int i = 0; i < listOfFiles.length; i++) 
        {
            fileMoving = listOfFiles[i];
            if (fileMoving.isFile() && fileMoving.getName().endsWith(".java") &&
                    !fileMoving.getName().equals("G5SeniorServicesLocator.java") &&
                    !fileMoving.getName().equals("G5SeniorServices.java")) 
            {
                
                if(true){//COPY
                    copyfile(fileMoving,new File(newFolderUrl + fileMoving.getName()));
                }
                else{                    
                    if(fileMoving.renameTo(new File(newFolderUrl + fileMoving.getName()))){
                            System.out.println("File is moved successful!");
                    }else{
                            System.out.println("File is failed to move!");
                    }
                }
                
            }
        }
    }
    public static void mergeG5SeniorServicesLocator(String[] args) {
        Tipo tipoAtual = Tipo.INICIAL;
        int identPort = 0;
        boolean proxyAdded = false;
        boolean wsddAdded = false;
        boolean portIfAdded = false;
        boolean portIf2Added = false;
        boolean portsAdded = false;
        
        try {
            BufferedWriter merge  = new BufferedWriter(new FileWriter(args[2] + "G5SeniorServicesLocator.java"));
            BufferedReader in1   = new BufferedReader(new FileReader(args[0] + "G5SeniorServicesLocator.java"));
//            BufferedReader in2   = new BufferedReader(new FileReader(args[1]));
                String str;
                while (in1.ready()) {
                    str = in1.readLine();
                    if(str.indexOf("Use to get a proxy class for") != -1){
                        tipoAtual = Tipo.PROXY;                        
                    }
                    if(str.indexOf("The WSDD service name defaults to the port name") != -1){
                        tipoAtual = Tipo.WSDD;
                    }
                    if(str.indexOf("getPort") != -1 && identPort == 0){
                        tipoAtual = Tipo.PORT;
                    }
                    if(str.indexOf("getPort") != -1 && identPort == 1){
                        tipoAtual = Tipo.PORT2;
                    }
                    if(str.indexOf("ports.add") != -1){
                        tipoAtual = Tipo.PORTS;
                    }
                    if(str.indexOf("setEndpointAddress(java.lang.String") != -1){
                        tipoAtual = Tipo.END_POINT;
                    }
                    
                    if(tipoAtual.compareTo(Tipo.PROXY) == 0 && !proxyAdded){
                        merge.write(getTipo(args[1],Tipo.PROXY));
                        merge.write(str + "\n");
                        tipoAtual = Tipo.INICIAL;
                        proxyAdded  =true;
                    }
                    else if(tipoAtual.compareTo(Tipo.WSDD) == 0 && !wsddAdded){
                        merge.write(getTipo(args[1],Tipo.WSDD));
                        merge.write(str + "\n");
                        tipoAtual = Tipo.INICIAL;
                        wsddAdded = true;
                    }
                    else if((tipoAtual.compareTo(Tipo.PORT) == 0 ||
                            tipoAtual.compareTo(Tipo.PORT_IF) == 0) && identPort == 0){
                        if((str.indexOf("if (") != -1 || str.indexOf("if(") != -1) && !portIfAdded){
                            tipoAtual = Tipo.PORT_IF;
                            merge.write(getTipo(args[1],Tipo.PORT_IF));
                            str = "else " + str;
                            identPort++;
                            portIfAdded = true;
                        }                        
                        merge.write(str + "\n");
                    }
                    else if((tipoAtual.compareTo(Tipo.PORT2) == 0 ||
                            tipoAtual.compareTo(Tipo.PORT_IF2) == 0) && identPort == 1){
                        if((str.indexOf("if (\"") != -1 || str.indexOf("if(\"") != -1) && !portIf2Added){
                            tipoAtual = Tipo.PORT_IF2;
                            merge.write(getTipo(args[1],Tipo.PORT_IF2));
                            str = "else " + str;
                            identPort = 1;
                            portIf2Added = true;
                        }                        
                        merge.write(str + "\n");
                    }
                    else if(tipoAtual.compareTo(Tipo.PORTS) == 0 && !portsAdded){
                        tipoAtual = Tipo.PORTS;
                        merge.write(getTipo(args[1],Tipo.PORTS));
                        merge.write(str+"\n");
                        portsAdded = true;
                    }
                    else if(tipoAtual.compareTo(Tipo.END_POINT) == 0){
                        if(str.indexOf("if (\"") != -1 || str.indexOf("if(\"") != -1){
                            tipoAtual = Tipo.END_POINT_IF;
                            merge.write(getTipo(args[1],Tipo.END_POINT_IF));
                            str = "else " + str;
                            identPort = 1;
                        }
                        merge.write(str + "\n");
                    }
                    else{
                        merge.write(str + "\n");
                    }
                }
                in1.close();
                merge.close();

        } catch (IOException e) {
        }
//        try {
//        Process pro = Runtime.getRuntime().exec("cmd.exe /c  " + args[2]);   
//            pro.waitFor();
//        } catch (IOException ex) {
//            Logger.getLogger(MergeWS.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(MergeWS.class.getName()).log(Level.SEVERE, null, ex);
//        }
        // TODO code application logic here
    }
    
    private static String getTipo(String file2, Tipo tipo){
        Tipo tipoAtual = Tipo.INICIAL;
        int identPort = 0;
        try {
            BufferedReader in1   = new BufferedReader(new FileReader(file2+"G5SeniorServicesLocator.java"));
//            BufferedReader in2   = new BufferedReader(new FileReader(args[1]));
                String strRet = "";
                String str;
                while (in1.ready()) {
                    str = in1.readLine();
//                    if(tipo.compareTo(Tipo.END_POINT_IF) == 0)
//                        System.out.println(str+"\n");
                    if(str.indexOf("Use to get a proxy class for") != -1 && tipo.compareTo(Tipo.PROXY) == 0){
                        tipoAtual = Tipo.PROXY;
                        while(in1.ready() && !(str.indexOf("The WSDD service name defaults to the port name") != -1)){
                            
                            strRet += str+"\n";
                            str = in1.readLine();
                            if(str.indexOf("Use to get a proxy class for") != -1) {
								break;
							}
                        }
                        in1.close();
                        return strRet;
                    }
                    if(str.indexOf("The WSDD service name defaults to the port name") != -1 && tipo.compareTo(Tipo.WSDD) == 0){
                        tipoAtual = Tipo.WSDD;
                        while(in1.ready() && !(str.indexOf("getPort") != -1)){
                            
                            strRet += str+"\n";
                            str = in1.readLine();
                        }
                        in1.close();
                        return strRet;
                    }
                    if(str.indexOf("getPort") != -1 && identPort == 1){
                        tipoAtual = Tipo.PORT2;
                    }
                    if(str.indexOf("getPort") != -1 && identPort == 0){
                        tipoAtual = Tipo.PORT;
                    }
                    if(str.indexOf("ports.add") != -1 && tipo.compareTo(Tipo.PORTS)==0){
                        tipoAtual = Tipo.PORTS;
                        while(in1.ready() && str.indexOf("ports.add") != -1){
                            strRet += str+"\n";
                            str = in1.readLine();
                        }
                        in1.close();
                        return strRet;
                    }
                    if(str.indexOf("setEndpointAddress(java.lang.String") != -1){
                        tipoAtual = Tipo.END_POINT;
                    }
                    
                    if(tipoAtual.compareTo(Tipo.PROXY) == 0){
                    }
                    else if(tipoAtual.compareTo(Tipo.WSDD) == 0){
                        
                    }
                    else if(tipoAtual.compareTo(Tipo.PORT) == 0 ||
                            tipoAtual.compareTo(Tipo.PORT_IF) == 0){
                        if(tipo.compareTo(Tipo.PORT) == 0 ||
                            tipo.compareTo(Tipo.PORT_IF) == 0 ){
                            
                            tipoAtual = Tipo.PORT;
                            if(identPort == 0){
                                if(str.indexOf("if (") != -1 || str.indexOf("if(") != -1){
                                    while(in1.ready() && !(str.indexOf("}") != -1)){
                                        strRet += str+"\n";
                                        str = in1.readLine();                                
                                    }
                                    strRet +="}\n";
                                    in1.close();
                                    return strRet;
                                }
                            }
                        }
                        if(tipo.compareTo(Tipo.PORT_IF2) == 0){
                            identPort = 1;
                        }
                    }
                    else if((tipo.compareTo(Tipo.PORT2) == 0 ||
                            tipo.compareTo(Tipo.PORT_IF2) == 0 ) && (tipoAtual.compareTo(Tipo.PORT2) == 0 ||
                            tipoAtual.compareTo(Tipo.PORT_IF2) == 0 ) && identPort == 1){
                        tipoAtual = Tipo.PORT2;
                        
                        if(identPort == 1){
                            if(str.indexOf("if (\"") != -1 || str.indexOf("if(\"") != -1){
                                tipoAtual = Tipo.PORT_IF2;
                                while(in1.ready() && !(str.indexOf("}") != -1)){
                                    strRet += str+"\n";
                                    str = in1.readLine();                                
                                }
                                strRet +="}\n";
                                in1.close();
                                return strRet;
                            }
                        }
                    }
//                    else if(tipoAtual.compareTo(Tipo.PORTS) == 0){
//                        while(in1.ready() && !(str.indexOf("}") != -1)){
//                            if(str.indexOf("HashSet()"))
//                            strRet += str+"\n";
//                            str = in1.readLine();                                
//                        }
//                    }
                    else if((tipoAtual.compareTo(Tipo.END_POINT) == 0
                            || tipoAtual.compareTo(Tipo.END_POINT_IF) == 0) && (tipo.compareTo(Tipo.END_POINT) == 0
                            || tipo.compareTo(Tipo.END_POINT_IF) == 0)){
                        
                            
                            if(str.indexOf("if (\"") != -1 || str.indexOf("if(\"") != -1){
                            tipoAtual = Tipo.END_POINT_IF;
                                while(in1.ready() && !(str.indexOf("}") != -1)){
                                    strRet += str+"\n";
                                    str = in1.readLine();                                
                                }
                                strRet +="}\n";
                                in1.close();
                                return strRet;
                            }
                            
                        
                    }
                }
                in1.close();
                
        } catch (IOException e) {
        }
        return "";
    }
    private static void copyfile(File f1, File f2) {
        try {
            InputStream in = new FileInputStream(f1);

            //For Append the file.
//  OutputStream out = new FileOutputStream(f2,true);

            //For Overwrite the file.
            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            System.out.println("File copied.");
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage() + " in the specified directory.");
            System.exit(0);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
