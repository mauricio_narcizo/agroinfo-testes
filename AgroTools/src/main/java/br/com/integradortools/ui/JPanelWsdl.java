package br.com.integradortools.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import br.com.integradortools.config.IntegradorToolsProperties;
import br.com.integradortools.ui.component.CsvTableModel;
import br.com.integradortools.wsdl.GerarListaClasses;



public class JPanelWsdl extends JPanel {

    private JTable table;

    /**
     * Create the panel.
     */
    public JPanelWsdl() {

        JScrollPane scrollPane = new JScrollPane();

        JLabel lblAdicionarWsdl = new JLabel("Adicionar WSDL");

        JLabel lblCaminho = new JLabel("Caminho");

        textFieldCaminhoWsdl = new JTextField();
        textFieldCaminhoWsdl.setColumns(10);

        JButton btnAdicionar = new JButton("Adicionar");
        btnAdicionar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((DefaultTableModel) table.getModel()).addRow(new Object[] { true, textFieldCaminhoWsdl.getText() });
            }
        });

        JSeparator separator = new JSeparator();

        JCheckBox chckbxSobreEscreverClasses = new JCheckBox("Sobre escrever classes existentes");
        chckbxSobreEscreverClasses.setEnabled(false);
        chckbxSobreEscreverClasses.setSelected(true);

        JButton btnGerar = new JButton("Gerar");
        btnGerar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                GerarListaClasses gerarListaClasses = new GerarListaClasses();
                try {
                    gerarListaClasses.initGenerateByFile(IntegradorToolsProperties.INSTANCE.BASE_OUTPUT_WSDL_S, IntegradorToolsProperties.INSTANCE.LISTA_WSDL_S);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        JButton btnRemove = new JButton("Remove");
        btnRemove.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ((CsvTableModel) table.getModel()).removeSelectedRows(table.getSelectedRows());
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout.createSequentialGroup().addGap(20).addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE).addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(lblCaminho).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(textFieldCaminhoWsdl, GroupLayout.PREFERRED_SIZE, 516, GroupLayout.PREFERRED_SIZE)).addComponent(lblAdicionarWsdl)).addPreferredGap(ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(btnRemove).addComponent(btnAdicionar))))).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(separator, GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(chckbxSobreEscreverClasses).addGap(18).addComponent(btnGerar))).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(34).addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 443, GroupLayout.PREFERRED_SIZE).addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(18).addComponent(lblAdicionarWsdl)).addGroup(groupLayout.createSequentialGroup().addPreferredGap(ComponentPlacement.RELATED).addComponent(btnRemove))).addPreferredGap(ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblCaminho).addComponent(textFieldCaminhoWsdl, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnAdicionar)).addGap(18).addComponent(separator, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnGerar).addComponent(chckbxSobreEscreverClasses)).addContainerGap()));

        table = new JTable();
        Object[][] data = {};
        DefaultTableModel dtm = new CsvTableModel(IntegradorToolsProperties.INSTANCE.LISTA_WSDL_S);
        //	 	DefaultTableModel dtm = new CsvTableModel(IntegradorToolsProperties.LISTA_WSDL_S);

        table.setModel(dtm);

        table.setRowHeight(25);
        table.getColumnModel().getColumn(0).setPreferredWidth(60);
        table.getColumnModel().getColumn(0).setMinWidth(60);
        table.getColumnModel().getColumn(0).setMaxWidth(60);
        scrollPane.setViewportView(table);
        setLayout(groupLayout);

    }

    private JTextField textFieldCaminhoWsdl;
}
