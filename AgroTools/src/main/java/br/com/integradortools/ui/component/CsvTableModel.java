package br.com.integradortools.ui.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import br.com.integradortools.config.IntegradorToolsProperties;
import br.com.integradortools.util.Util;


public class CsvTableModel extends DefaultTableModel {
	private final String SEPARADOR = IntegradorToolsProperties.INSTANCE.CSV_SEPARATOR;
	private String csvFile = new String();
	private String[] header;
	private List<Object[]> dados;
	Class[] types = new Class [] {
		Boolean.class, java.lang.String.class
	};
	Class[] typesFile = new Class [] {
		Integer.class, java.lang.String.class
	};
	private boolean csvLoading = false;
	
	public CsvTableModel(String csvFile) {
		super();
		this.csvFile = csvFile;
		dados = new ArrayList<Object[]>();
		loadCSV();
	}


    @Override
	public Class getColumnClass(int columnIndex) {
        return types [columnIndex];
    }
	@Override
	public void setValueAt(Object aValue, int row, int column) {
		if(types[column] != typesFile[column]) {
			if(types[column] == Boolean.class && typesFile[column] == Integer.class) {
				super.setValueAt(aValue.equals("1")?Boolean.TRUE:Boolean.FALSE, row, column);
			}
		}		
		
		super.setValueAt(aValue, row, column);		
	}
	private void loadCSV(){
		try {
			csvLoading = true;
			String[] wsdl_s = Util.lerLinhasArquivo(csvFile);
			if(wsdl_s != null && wsdl_s.length > 0){
				header = wsdl_s[0].split(SEPARADOR);
				this.setColumnIdentifiers(header);
				for (int i = 1; i < wsdl_s.length; i++) {
					String string = wsdl_s[i];
					String columns[] = string.split(SEPARADOR);
					if(columns != null && columns[0].matches("\\d")){
						this.addRow(new Object[]{Integer.parseInt(columns[0])==1?true:false, columns[1]});
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		finally{
			csvLoading = false;
		}
	}
	private void gravarCSV(){
		try {
			StringBuilder wsdl_s = new StringBuilder(); 
			Vector<Vector> objects = getDataVector();
			for (int i = 0; i < getColumnCount(); i++) {
				if(i > 0) {
					wsdl_s.append(SEPARADOR);
				}
				wsdl_s.append(getColumnName(i));
			}
				
			for (Vector objects2 : objects) {
				wsdl_s.append("\r\n");
				for (int i = 0; i < objects2.size(); i++) {
					if(i > 0) {
						wsdl_s.append(SEPARADOR);
					}
					if(types[i] != typesFile[i]){
						wsdl_s.append(getValue(types[i],typesFile[i],objects2.get(i)));
					}
					else{
						wsdl_s.append(objects2.get(i));
					}
				}
			}
			Util.escreverArquivo(wsdl_s.toString(), csvFile);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	}
	
	private Object getValue(Class classOrig, Class classTo, Object value) {
		if(classOrig.equals(Boolean.class) && classTo.equals(Integer.class)){
			return (Boolean)value?1:0;
		}
		return null;
	}
	@Override
    public void fireTableCellUpdated(int arg0, int arg1) {
    	super.fireTableCellUpdated(arg0, arg1);
    	 System.out.println("["+arg0+","+arg1+"]");
    	 System.out.println(getDataVector().get(arg0));
    	 System.out.println(getColumnName(0)+","+getColumnName(1));
    	 gravarCSV();
    }
	@Override
	public void fireTableRowsInserted(int arg0, int arg1) {
		super.fireTableRowsInserted(arg0, arg1);
		if(!csvLoading) {
			gravarCSV();
		}
		
	}
	@Override
	public void fireTableRowsDeleted(int firstRow, int lastRow) {
		super.fireTableRowsDeleted(firstRow, lastRow);
		gravarCSV();
	}
	public void removeSelectedRows(int[] rows){
	   for(int i=0;i<rows.length;i++){
	     removeRow(rows[i]-i);
	   }
	}
	
}
