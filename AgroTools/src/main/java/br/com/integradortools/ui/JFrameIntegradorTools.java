package br.com.integradortools.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import br.com.integradortools.config.IntegradorToolsProperties;

public class JFrameIntegradorTools {

    private JFrame frmIntegradortools;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        //		UIManager.put("nimbusBase", new Color(1F,1F,1F));
        //		UIManager.put("nimbusBlueGrey", new Color(1F,1F,1F));
        //		UIManager.put("control", new Color(1F,1F,1F));
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    JFrameIntegradorTools window = new JFrameIntegradorTools();
                    window.frmIntegradortools.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public JFrameIntegradorTools() {
        initialize();

    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmIntegradortools = new JFrame();
        frmIntegradortools.setIconImage(Toolkit.getDefaultToolkit().getImage(IntegradorToolsProperties.INSTANCE.IMG_LOGO));
        frmIntegradortools.setTitle("IntegradorTools");
        frmIntegradortools.setBounds(100, 100, 856, 740);
        frmIntegradortools.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        frmIntegradortools.getContentPane().add(tabbedPane, BorderLayout.CENTER);

        JPanel panelGeradorWsdl = new JPanelWsdl();
        tabbedPane.addTab("Gerador WSDL", null, panelGeradorWsdl, null);

    }

}
