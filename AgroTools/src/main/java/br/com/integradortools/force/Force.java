package br.com.integradortools.force;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.com.integradortools.util.DataFile;
import br.com.integradortools.util.IntegradorTools;
import br.com.integradortools.util.RegEx;
import br.com.integradortools.util.Util;

public class Force {

    private static String DEFAULT_PATH = "C:/wkspc-senior/";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        //        seeAlso("TransferObject",TRANSPORTE_PATH,TOs_PATH);
        //		seeAlso("Param", PARAM_PATH, PARAMs_PATH);
    }

    public static void seeAlso(String classExtend, String paramPath, String paransPath) throws IOException {
        List<File> files = Util.listFilesByFolder(new File(paransPath));
        for (int i = 0; i < files.size(); i++) {
            File file = files.get(i);
            DataFile dataClassePrincipalParam = null;

            dataClassePrincipalParam = new DataFile(file);

            if (dataClassePrincipalParam.extend.equals(classExtend)) {
                if (!IntegradorTools.verificaJaInserido(paramPath, dataClassePrincipalParam, "xmlseealso")) {
                    String replace = "$1$2,\n\t" + dataClassePrincipalParam.getFullName() + ".class$7";
                    try {
                        Util.buscaInsere(paramPath, RegEx.REGEX_XMLSEEALSO2, replace);
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                    System.out.println("\tInserido " + dataClassePrincipalParam.getFileName() + ".");
                } else {
                    System.out.println("\tJá Inserido.[" + dataClassePrincipalParam.getFileName() + "]");
                }
            }
        }
    }
    //    private void objectFactory(){
    //        String ss[] = new String[]{"C:\\wkspc-senior\\IntegradorInterface\\src\\br\\com\\senior\\integradorinterface\\integracoes",
    //                "C:\\wkspc-senior\\IntegradorInterface\\src\\br\\com\\senior\\integradorinterface\\to"};
    //        Util.listFilesByFolder(new File(fileName));
    //    }
}
