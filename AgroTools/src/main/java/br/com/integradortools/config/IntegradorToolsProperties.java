package br.com.integradortools.config;

public enum IntegradorToolsProperties {
    INSTANCE;

    public String IMG_LOGO;

    public String LISTA_INTEGRACOES;
    public String LISTA_WSDL_S;

    public String BASE_OUTPUT_WSDL_S;
    public String CSV_SEPARATOR;

    public String DEFAULT_DIRECTORY_CLASSE_PRINCIPAL_TO;
    public String DEFAULT_DIRECTORY_CLASSE_PRINCIPAL_PARAM;
    public String DEFAULT_DIRECTORY_DIRETORIO_CLASSES_TO;
    public String DEFAULT_DIRECTORY_DIRETORIO_CLASSES_PARAM;
    public String DEFAULT_DIRECTORY_CLASSE_INTERMEDIARIA_ERP;
    public String DEFAULT_DIRECTORY_CLASSE_INTERMEDIARIA_RETAGUARDA;

    //    public String TIPO_CLASS_PATH      = DEFAULT_PATH + "IntegradorInterface/src/br/com/senior/integradorinterface/enums/TipoClass.java";
    public String TRANSPORTE_PATH;
    public String PARAM_PATH;
    public String TRATA_PENDENCIA_PATH;
    public String ERP_PROPERTIES_DEPARA_PATH;
    public String RET_PROPERTIES_DEPARA_PATH;
    public String ERP_ADAPTER_PATH;
    public String INTEGRADOR_WS_PATH;
    public String OBJECT_FACTORY_PATH;
    public String RETAGUARDA_ADAPTER_PATH;

    public String URL_DB;
    public String DRIVER_DB;
    public String USUARIO_DB;
    public String SENHA_DB;

    public String SEPARADOR;
    public String SEPARADOR_VISUAL;
    public String DEFAULT_DIRECTORY_CLASSES;
    public String DEFAULT_DIRECTORY_DESTINO;

    private IntegradorToolsProperties() {
        String basePath = System.getProperty("user.dir") + "/";
        IMG_LOGO = "C:\\git\\agroinfo\\AgroTools\\src\\main\\resource\\logoTools.png";//this.getClass().getClassLoader().getResource("logoTools.png").getPath();

        LISTA_INTEGRACOES ="C:\\git\\agroinfo\\AgroTools\\src\\main\\resource\\ListaIntegracoes.csv";// this.getClass().getClassLoader().getResource("ListaIntegracoes.csv").getPath();
        LISTA_WSDL_S = "C:\\git\\agroinfo\\AgroTools\\src\\main\\resource\\WSDL_Gerar.csv";//this.getClass().getClassLoader().getResource("WSDL_Gerar.csv").getPath();

        BASE_OUTPUT_WSDL_S = basePath + "../AgroInfoErpSenior/src/main/java/";

        CSV_SEPARATOR = ";";

        DEFAULT_DIRECTORY_CLASSE_PRINCIPAL_TO = basePath + "../AgroInfoErpSenior/src/main/java/br/com/senior/agroinfo/erpinterface/to/";
        DEFAULT_DIRECTORY_CLASSE_PRINCIPAL_PARAM = basePath + "../AgroInfoErpSenior/src/main/java/br/com/senior/agroinfo/erpinterface/param/";
        DEFAULT_DIRECTORY_CLASSE_INTERMEDIARIA_ERP = basePath + "../AgroInfoErpSenior/src/main/java/br/com/senior/agroinfo/erpinterface/intermediador/";

        SEPARADOR = "&#124";
        SEPARADOR_VISUAL = "|";
        DEFAULT_DIRECTORY_CLASSES = basePath + "../AgroInfoErpSenior/src/main/java/br/com/senior/agroinfo/erpinterface/services/";

    }

}
