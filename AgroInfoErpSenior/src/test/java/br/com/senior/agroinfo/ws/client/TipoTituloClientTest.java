package br.com.senior.agroinfo.ws.client;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.exceptions.TipoTituloWSException;
import br.com.senior.agroinfo.ws.util.TestClientUtil;
import br.com.senior.services.TipotituloExportarIn;
import br.com.senior.services.TipotituloExportarInConsulta;
import br.com.senior.services.TipotituloExportarOut;

public class TipoTituloClientTest {

	@Test
	public void deveRetornarTipoTitulo() throws Exception {
		TipotituloExportarInConsulta[] consulta = { new TipotituloExportarInConsulta("01") };
		TipotituloExportarOut titulo = TipoTituloClient.buscarTipoTitulo(TestClientUtil.USUARIO, TestClientUtil.SENHA,
				TestClientUtil.URL, obterConsultaTipoTitulo(consulta));

		assertNotNull(titulo);
		assertNotNull(titulo.getTipoTitulo());
	}

	@Test(expectedExceptions = { TipoTituloWSException.class })
	public void deveLancarExceptionTitpoTituloNaoEncontrado() throws Exception {
		TipoTituloClient.buscarTipoTitulo(TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL,
				obterConsultaTipoTitulo(null));
	}

	@Test(expectedExceptions = { TipoTituloWSException.class })
	public void deveLancarExceptionTipoTituloUrlNaoEncontrado() throws Exception {
		TipotituloExportarInConsulta[] consulta = { new TipotituloExportarInConsulta("01") };
		TipoTituloClient.buscarTipoTitulo(TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL_INVALIDO,
				obterConsultaTipoTitulo(consulta));
	}

	private TipotituloExportarIn obterConsultaTipoTitulo(final TipotituloExportarInConsulta[] consulta) {
		TipotituloExportarIn in = new TipotituloExportarIn();

		in.setCodEmp(1);
		in.setCodFil(1L);
		in.setIdentificacaoSistema("VAREJOEM");
		in.setTipoIntegracao(TipoIntegracao.ESPECIFICO.getSigla());

		in.setConsulta(consulta);
		return in;
	}
}
