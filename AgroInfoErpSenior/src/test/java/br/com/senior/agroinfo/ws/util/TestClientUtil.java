package br.com.senior.agroinfo.ws.util;

public final class TestClientUtil {

    public static final String URL = "http://teste33:8080";
    public static final String URL_INVALIDO = "http://teste33:8089";
    public static final String USUARIO = "suporte";
    public static final String SENHA = "suporte";

    private TestClientUtil() {
    }

}
