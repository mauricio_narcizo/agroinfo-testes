package br.com.senior.agroinfo.ws.client;

import org.testng.annotations.Test;

import br.com.senior.agroinfo.exceptions.ContasPagarWSException;

public class ContasReceberClientTest {

    @Test
    public void deveRetornarContasPagarCooperado() throws Exception {

//        TitulosConsultarTitulosAbertosCROut contasPagar = ContasReceberClient.buscarContasReceber(
//                TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL, obterConsultasPagarInCooperado("1"));
//
//        assertNotNull(contasPagar);

    }

    @Test(expectedExceptions = { ContasPagarWSException.class }, enabled = false)
    public void deveLancarExceptionContasPagarNaoEncontrado() throws Exception {

//        String codFornecedorInexistente = "-1111";
//        ContasReceberClient.buscarContasReceber(TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL,
//                obterConsultasPagarInCooperado(codFornecedorInexistente));

    }

    @Test(expectedExceptions = { ContasPagarWSException.class }, enabled = false)
    public void deveLancarExceptionContasPagarUrlNaoEncontrado() throws Exception {

//        ContasReceberClient.buscarContasReceber(TestClientUtil.USUARIO, TestClientUtil.SENHA,
//                TestClientUtil.URL_INVALIDO, obterConsultasPagarInCooperado("1"));
    }

//    private TitulosConsultarTitulosAbertosCRIn obterConsultasPagarInCooperado(final String codFornecedor) {
//        TitulosConsultarTitulosAbertosCRIn titulosIn = new TitulosConsultarTitulosAbertosCRIn();
//        titulosIn.setCodEmp(1);
//        titulosIn.setCodFil(1);
//        titulosIn.setCodCli(codFornecedor);
//        return titulosIn;
//    }

}
