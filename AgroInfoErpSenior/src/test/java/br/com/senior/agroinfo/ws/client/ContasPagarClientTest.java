package br.com.senior.agroinfo.ws.client;

import static org.testng.Assert.*;

import java.util.Optional;

import org.testng.annotations.Test;

import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.exceptions.ContasPagarWSException;
import br.com.senior.agroinfo.ws.util.TestClientUtil;
import br.com.senior.services.TitulosExportarTitulosCPIn;
import br.com.senior.services.TitulosExportarTitulosCPInConsulta;
import br.com.senior.services.TitulosExportarTitulosCPOut;

public class ContasPagarClientTest {

	@Test
	public void deveRetornarContasPagarCooperado() throws Exception {

		Optional<TitulosExportarTitulosCPOut> contasPagar = ContasPagarClient.buscarContasPagar(TestClientUtil.USUARIO,
				TestClientUtil.SENHA, TestClientUtil.URL, obterConsultasPagarInCooperado("1"));

		assertTrue(contasPagar.isPresent());
		assertNotNull(contasPagar.get().getTitulos());
	}

	@Test(expectedExceptions = { ContasPagarWSException.class })
	public void deveLancarExceptionContasPagarNaoEncontrado() throws Exception {

		String codFornecedorInexistente = "-1111";
		ContasPagarClient.buscarContasPagar(TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL,
				obterConsultasPagarInCooperado(codFornecedorInexistente));
	}

	@Test(expectedExceptions = { ContasPagarWSException.class })
	public void deveLancarExceptionContasPagarUrlNaoEncontrado() throws Exception {

		ContasPagarClient.buscarContasPagar(TestClientUtil.USUARIO, TestClientUtil.SENHA, TestClientUtil.URL_INVALIDO,
				obterConsultasPagarInCooperado("1"));
	}

	private TitulosExportarTitulosCPIn obterConsultasPagarInCooperado(final String codFornecedor) {
		TitulosExportarTitulosCPIn titulosIn = new TitulosExportarTitulosCPIn();
		titulosIn.setCodFil(1L);
		titulosIn.setCodEmp(Integer.valueOf(codFornecedor));
		TitulosExportarTitulosCPInConsulta[] consultas = { new TitulosExportarTitulosCPInConsulta(1, null, null) };
		titulosIn.setQuantidadeRegistros(10);
		titulosIn.setTipoIntegracao(TipoIntegracao.TODOS.getSigla());
		titulosIn.setSistemaIntegracao("VAREJOEM");
		titulosIn.setConsulta(consultas);
		return titulosIn;
	}
}