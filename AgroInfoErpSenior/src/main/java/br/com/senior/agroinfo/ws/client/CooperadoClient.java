package br.com.senior.agroinfo.ws.client;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.rpc.ServiceException;

import br.com.senior.agroinfo.exceptions.FornecedorWSException;
import br.com.senior.agroinfo.utils.UtilProxy;
import br.com.senior.services.FornecedoresExportarIn;
import br.com.senior.services.FornecedoresExportarOut;
import br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores;

public class CooperadoClient {

	private CooperadoClient() {
	}

	public static FornecedoresExportarOut buscarCooperadosWS(final String usuario, final String senha, final String url,
			final FornecedoresExportarIn fornecedorIn) throws FornecedorWSException {

		final int encryption = 0;

		FornecedoresExportarOut fornecedorOut;
		try {
			fornecedorOut = UtilProxy.getServiceProxy(Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores.class, url)
					.exportar(usuario, senha, encryption, fornecedorIn);

			if (fornecedorOut.getErroExecucao() != null && !fornecedorOut.getErroExecucao().isEmpty()
					|| fornecedorOut.getGridErros() != null) {
				String msgErro = fornecedorOut.getMensagemRetorno() != null ? fornecedorOut.getMensagemRetorno()
						: fornecedorOut.getErroExecucao();
				throw new FornecedorWSException(msgErro);
			}
		} catch (RemoteException | ServiceException e) {
			Logger.getLogger("CooperadoClient").log(Level.SEVERE, e.getMessage());
			throw new FornecedorWSException(e.getMessage());
		}

		return fornecedorOut;
	}
}