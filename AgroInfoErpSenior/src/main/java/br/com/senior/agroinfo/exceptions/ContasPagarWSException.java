package br.com.senior.agroinfo.exceptions;

public class ContasPagarWSException extends Exception {

    private static final long serialVersionUID = 1L;

    public ContasPagarWSException(final String msg) {
        super(msg);
    }

}
