package br.com.senior.agroinfo.exceptions;

public class TipoTituloWSException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public TipoTituloWSException(final String msg){
		super(msg);
	}

}
