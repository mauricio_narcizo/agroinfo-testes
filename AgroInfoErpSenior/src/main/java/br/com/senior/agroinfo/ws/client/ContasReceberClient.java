package br.com.senior.agroinfo.ws.client;

import java.rmi.RemoteException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.rpc.ServiceException;

import br.com.senior.agroinfo.exceptions.ContasPagarWSException;
import br.com.senior.agroinfo.exceptions.TitulosNotFoundException;
import br.com.senior.agroinfo.utils.UtilProxy;
import br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos;
import br.com.senior.services.TitulosExportarTitulosReceberIn;
import br.com.senior.services.TitulosExportarTitulosReceberOut;

public class ContasReceberClient {
	private ContasReceberClient() {
	}

	public static Optional<TitulosExportarTitulosReceberOut> buscarContasReceber(final String usuario, final String senha,
			final String url, final TitulosExportarTitulosReceberIn in) throws ContasPagarWSException {
		final int encryption = 0;
		Optional<TitulosExportarTitulosReceberOut> titulos = Optional.empty();
		try {
			titulos = Optional.of(UtilProxy.getServiceProxy(Sapiens_Synccom_senior_g5_co_int_varejo_titulos.class, url)
					.exportarTitulosReceber(usuario, senha, encryption, in));

			if (titulos.get().getErroExecucao() != null && !titulos.get().getErroExecucao().isEmpty()
					|| titulos.get().getErros() != null) {
				String msgErro = titulos.get().getMensagemRetorno() != null ? titulos.get().getMensagemRetorno()
						: titulos.get().getErroExecucao();
				throw new ContasPagarWSException(msgErro);
			}
			if (titulos.get().getTitulosReceber() == null) {
				throw new TitulosNotFoundException("Não foram encontrados titulos a pagar");
			}
		} catch (ServiceException | RemoteException e) {
			Logger.getLogger("ContasPagarClient").log(Level.SEVERE, e.getMessage(), e);
			throw new ContasPagarWSException(e.getMessage());
		}
		return titulos;
	}
}
