/**
 * TitulosExportarTitulosReceber2OutTitulosReceber.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosExportarTitulosReceber2OutTitulosReceber  implements java.io.Serializable {
    private java.lang.String catTef;

    private java.lang.String cheAge;

    private java.lang.String cheBan;

    private java.lang.String cheCta;

    private java.lang.String cheNum;

    private java.lang.String codCcu;

    private java.lang.Integer codCli;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFpg;

    private java.lang.Integer codFpj;

    private java.lang.String codMpt;

    private java.lang.Integer codNtg;

    private java.lang.String codPor;

    private java.lang.Integer codRep;

    private java.lang.String codSac;

    private java.lang.String codTpt;

    private java.lang.Double comRec;

    private java.lang.String cpgNeg;

    private java.lang.Integer ctaFin;

    private java.lang.Integer ctaRed;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.Double dscNeg;

    private java.lang.Double jrsDia;

    private java.lang.Double jrsNeg;

    private java.lang.String locTit;

    private java.lang.Double mulNeg;

    private java.lang.String nsuTef;

    private java.lang.Integer numPrj;

    private java.lang.String numTit;

    private java.lang.String obsTcr;

    private java.lang.Double outNeg;

    private java.lang.Double perCom;

    private java.lang.Double perDsc;

    private java.lang.Double perJrs;

    private java.lang.Double perMul;

    private java.lang.Integer proJrs;

    private java.lang.String seqInt;

    private java.lang.String sitTit;

    private java.lang.Double taxNeg;

    private java.lang.String tipJrs;

    private java.lang.Integer tolDsc;

    private java.lang.Integer tolJrs;

    private java.lang.Integer tolMul;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.Double vlrAbe;

    private java.lang.Double vlrBco;

    private java.lang.Double vlrCom;

    private java.lang.Double vlrDca;

    private java.lang.Double vlrDcb;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrOri;

    private java.lang.Double vlrOud;

    public TitulosExportarTitulosReceber2OutTitulosReceber() {
    }

    public TitulosExportarTitulosReceber2OutTitulosReceber(
           java.lang.String catTef,
           java.lang.String cheAge,
           java.lang.String cheBan,
           java.lang.String cheCta,
           java.lang.String cheNum,
           java.lang.String codCcu,
           java.lang.Integer codCli,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFpg,
           java.lang.Integer codFpj,
           java.lang.String codMpt,
           java.lang.Integer codNtg,
           java.lang.String codPor,
           java.lang.Integer codRep,
           java.lang.String codSac,
           java.lang.String codTpt,
           java.lang.Double comRec,
           java.lang.String cpgNeg,
           java.lang.Integer ctaFin,
           java.lang.Integer ctaRed,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.Double dscNeg,
           java.lang.Double jrsDia,
           java.lang.Double jrsNeg,
           java.lang.String locTit,
           java.lang.Double mulNeg,
           java.lang.String nsuTef,
           java.lang.Integer numPrj,
           java.lang.String numTit,
           java.lang.String obsTcr,
           java.lang.Double outNeg,
           java.lang.Double perCom,
           java.lang.Double perDsc,
           java.lang.Double perJrs,
           java.lang.Double perMul,
           java.lang.Integer proJrs,
           java.lang.String seqInt,
           java.lang.String sitTit,
           java.lang.Double taxNeg,
           java.lang.String tipJrs,
           java.lang.Integer tolDsc,
           java.lang.Integer tolJrs,
           java.lang.Integer tolMul,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.Double vlrAbe,
           java.lang.Double vlrBco,
           java.lang.Double vlrCom,
           java.lang.Double vlrDca,
           java.lang.Double vlrDcb,
           java.lang.Double vlrDsc,
           java.lang.Double vlrOri,
           java.lang.Double vlrOud) {
           this.catTef = catTef;
           this.cheAge = cheAge;
           this.cheBan = cheBan;
           this.cheCta = cheCta;
           this.cheNum = cheNum;
           this.codCcu = codCcu;
           this.codCli = codCli;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codMpt = codMpt;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codRep = codRep;
           this.codSac = codSac;
           this.codTpt = codTpt;
           this.comRec = comRec;
           this.cpgNeg = cpgNeg;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.dscNeg = dscNeg;
           this.jrsDia = jrsDia;
           this.jrsNeg = jrsNeg;
           this.locTit = locTit;
           this.mulNeg = mulNeg;
           this.nsuTef = nsuTef;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.obsTcr = obsTcr;
           this.outNeg = outNeg;
           this.perCom = perCom;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.proJrs = proJrs;
           this.seqInt = seqInt;
           this.sitTit = sitTit;
           this.taxNeg = taxNeg;
           this.tipJrs = tipJrs;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrAbe = vlrAbe;
           this.vlrBco = vlrBco;
           this.vlrCom = vlrCom;
           this.vlrDca = vlrDca;
           this.vlrDcb = vlrDcb;
           this.vlrDsc = vlrDsc;
           this.vlrOri = vlrOri;
           this.vlrOud = vlrOud;
    }


    /**
     * Gets the catTef value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return catTef
     */
    public java.lang.String getCatTef() {
        return catTef;
    }


    /**
     * Sets the catTef value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param catTef
     */
    public void setCatTef(java.lang.String catTef) {
        this.catTef = catTef;
    }


    /**
     * Gets the cheAge value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return cheAge
     */
    public java.lang.String getCheAge() {
        return cheAge;
    }


    /**
     * Sets the cheAge value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param cheAge
     */
    public void setCheAge(java.lang.String cheAge) {
        this.cheAge = cheAge;
    }


    /**
     * Gets the cheBan value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return cheBan
     */
    public java.lang.String getCheBan() {
        return cheBan;
    }


    /**
     * Sets the cheBan value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param cheBan
     */
    public void setCheBan(java.lang.String cheBan) {
        this.cheBan = cheBan;
    }


    /**
     * Gets the cheCta value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return cheCta
     */
    public java.lang.String getCheCta() {
        return cheCta;
    }


    /**
     * Sets the cheCta value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param cheCta
     */
    public void setCheCta(java.lang.String cheCta) {
        this.cheCta = cheCta;
    }


    /**
     * Gets the cheNum value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return cheNum
     */
    public java.lang.String getCheNum() {
        return cheNum;
    }


    /**
     * Sets the cheNum value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param cheNum
     */
    public void setCheNum(java.lang.String cheNum) {
        this.cheNum = cheNum;
    }


    /**
     * Gets the codCcu value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codCli value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCrp value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFpg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codMpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codMpt
     */
    public java.lang.String getCodMpt() {
        return codMpt;
    }


    /**
     * Sets the codMpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codMpt
     */
    public void setCodMpt(java.lang.String codMpt) {
        this.codMpt = codMpt;
    }


    /**
     * Gets the codNtg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codNtg
     */
    public java.lang.Integer getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.Integer codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codRep value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codRep
     */
    public java.lang.Integer getCodRep() {
        return codRep;
    }


    /**
     * Sets the codRep value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codRep
     */
    public void setCodRep(java.lang.Integer codRep) {
        this.codRep = codRep;
    }


    /**
     * Gets the codSac value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codSac
     */
    public java.lang.String getCodSac() {
        return codSac;
    }


    /**
     * Sets the codSac value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codSac
     */
    public void setCodSac(java.lang.String codSac) {
        this.codSac = codSac;
    }


    /**
     * Gets the codTpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the comRec value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return comRec
     */
    public java.lang.Double getComRec() {
        return comRec;
    }


    /**
     * Sets the comRec value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param comRec
     */
    public void setComRec(java.lang.Double comRec) {
        this.comRec = comRec;
    }


    /**
     * Gets the cpgNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return cpgNeg
     */
    public java.lang.String getCpgNeg() {
        return cpgNeg;
    }


    /**
     * Sets the cpgNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param cpgNeg
     */
    public void setCpgNeg(java.lang.String cpgNeg) {
        this.cpgNeg = cpgNeg;
    }


    /**
     * Gets the ctaFin value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return ctaFin
     */
    public java.lang.Integer getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.Integer ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the dscNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return dscNeg
     */
    public java.lang.Double getDscNeg() {
        return dscNeg;
    }


    /**
     * Sets the dscNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param dscNeg
     */
    public void setDscNeg(java.lang.Double dscNeg) {
        this.dscNeg = dscNeg;
    }


    /**
     * Gets the jrsDia value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return jrsDia
     */
    public java.lang.Double getJrsDia() {
        return jrsDia;
    }


    /**
     * Sets the jrsDia value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param jrsDia
     */
    public void setJrsDia(java.lang.Double jrsDia) {
        this.jrsDia = jrsDia;
    }


    /**
     * Gets the jrsNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return jrsNeg
     */
    public java.lang.Double getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.Double jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the locTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return locTit
     */
    public java.lang.String getLocTit() {
        return locTit;
    }


    /**
     * Sets the locTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param locTit
     */
    public void setLocTit(java.lang.String locTit) {
        this.locTit = locTit;
    }


    /**
     * Gets the mulNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return mulNeg
     */
    public java.lang.Double getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.Double mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the nsuTef value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return nsuTef
     */
    public java.lang.String getNsuTef() {
        return nsuTef;
    }


    /**
     * Sets the nsuTef value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param nsuTef
     */
    public void setNsuTef(java.lang.String nsuTef) {
        this.nsuTef = nsuTef;
    }


    /**
     * Gets the numPrj value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsTcr value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return obsTcr
     */
    public java.lang.String getObsTcr() {
        return obsTcr;
    }


    /**
     * Sets the obsTcr value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param obsTcr
     */
    public void setObsTcr(java.lang.String obsTcr) {
        this.obsTcr = obsTcr;
    }


    /**
     * Gets the outNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return outNeg
     */
    public java.lang.Double getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.Double outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perCom value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return perCom
     */
    public java.lang.Double getPerCom() {
        return perCom;
    }


    /**
     * Sets the perCom value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param perCom
     */
    public void setPerCom(java.lang.Double perCom) {
        this.perCom = perCom;
    }


    /**
     * Gets the perDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return perJrs
     */
    public java.lang.Double getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.Double perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return perMul
     */
    public java.lang.Double getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.Double perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the proJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return proJrs
     */
    public java.lang.Integer getProJrs() {
        return proJrs;
    }


    /**
     * Sets the proJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param proJrs
     */
    public void setProJrs(java.lang.Integer proJrs) {
        this.proJrs = proJrs;
    }


    /**
     * Gets the seqInt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return seqInt
     */
    public java.lang.String getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.String seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the sitTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return sitTit
     */
    public java.lang.String getSitTit() {
        return sitTit;
    }


    /**
     * Sets the sitTit value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param sitTit
     */
    public void setSitTit(java.lang.String sitTit) {
        this.sitTit = sitTit;
    }


    /**
     * Gets the taxNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return taxNeg
     */
    public java.lang.Double getTaxNeg() {
        return taxNeg;
    }


    /**
     * Sets the taxNeg value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param taxNeg
     */
    public void setTaxNeg(java.lang.Double taxNeg) {
        this.taxNeg = taxNeg;
    }


    /**
     * Gets the tipJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the tolDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return tolJrs
     */
    public java.lang.Integer getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.Integer tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return tolMul
     */
    public java.lang.Integer getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.Integer tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the vctOri value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrAbe value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrAbe
     */
    public java.lang.Double getVlrAbe() {
        return vlrAbe;
    }


    /**
     * Sets the vlrAbe value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrAbe
     */
    public void setVlrAbe(java.lang.Double vlrAbe) {
        this.vlrAbe = vlrAbe;
    }


    /**
     * Gets the vlrBco value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrBco
     */
    public java.lang.Double getVlrBco() {
        return vlrBco;
    }


    /**
     * Sets the vlrBco value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrBco
     */
    public void setVlrBco(java.lang.Double vlrBco) {
        this.vlrBco = vlrBco;
    }


    /**
     * Gets the vlrCom value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrCom
     */
    public java.lang.Double getVlrCom() {
        return vlrCom;
    }


    /**
     * Sets the vlrCom value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrCom
     */
    public void setVlrCom(java.lang.Double vlrCom) {
        this.vlrCom = vlrCom;
    }


    /**
     * Gets the vlrDca value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrDca
     */
    public java.lang.Double getVlrDca() {
        return vlrDca;
    }


    /**
     * Sets the vlrDca value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrDca
     */
    public void setVlrDca(java.lang.Double vlrDca) {
        this.vlrDca = vlrDca;
    }


    /**
     * Gets the vlrDcb value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrDcb
     */
    public java.lang.Double getVlrDcb() {
        return vlrDcb;
    }


    /**
     * Sets the vlrDcb value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrDcb
     */
    public void setVlrDcb(java.lang.Double vlrDcb) {
        this.vlrDcb = vlrDcb;
    }


    /**
     * Gets the vlrDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrOri value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrOri
     */
    public java.lang.Double getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.Double vlrOri) {
        this.vlrOri = vlrOri;
    }


    /**
     * Gets the vlrOud value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @return vlrOud
     */
    public java.lang.Double getVlrOud() {
        return vlrOud;
    }


    /**
     * Sets the vlrOud value for this TitulosExportarTitulosReceber2OutTitulosReceber.
     * 
     * @param vlrOud
     */
    public void setVlrOud(java.lang.Double vlrOud) {
        this.vlrOud = vlrOud;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExportarTitulosReceber2OutTitulosReceber)) return false;
        TitulosExportarTitulosReceber2OutTitulosReceber other = (TitulosExportarTitulosReceber2OutTitulosReceber) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.catTef==null && other.getCatTef()==null) || 
             (this.catTef!=null &&
              this.catTef.equals(other.getCatTef()))) &&
            ((this.cheAge==null && other.getCheAge()==null) || 
             (this.cheAge!=null &&
              this.cheAge.equals(other.getCheAge()))) &&
            ((this.cheBan==null && other.getCheBan()==null) || 
             (this.cheBan!=null &&
              this.cheBan.equals(other.getCheBan()))) &&
            ((this.cheCta==null && other.getCheCta()==null) || 
             (this.cheCta!=null &&
              this.cheCta.equals(other.getCheCta()))) &&
            ((this.cheNum==null && other.getCheNum()==null) || 
             (this.cheNum!=null &&
              this.cheNum.equals(other.getCheNum()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codMpt==null && other.getCodMpt()==null) || 
             (this.codMpt!=null &&
              this.codMpt.equals(other.getCodMpt()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codRep==null && other.getCodRep()==null) || 
             (this.codRep!=null &&
              this.codRep.equals(other.getCodRep()))) &&
            ((this.codSac==null && other.getCodSac()==null) || 
             (this.codSac!=null &&
              this.codSac.equals(other.getCodSac()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.comRec==null && other.getComRec()==null) || 
             (this.comRec!=null &&
              this.comRec.equals(other.getComRec()))) &&
            ((this.cpgNeg==null && other.getCpgNeg()==null) || 
             (this.cpgNeg!=null &&
              this.cpgNeg.equals(other.getCpgNeg()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.dscNeg==null && other.getDscNeg()==null) || 
             (this.dscNeg!=null &&
              this.dscNeg.equals(other.getDscNeg()))) &&
            ((this.jrsDia==null && other.getJrsDia()==null) || 
             (this.jrsDia!=null &&
              this.jrsDia.equals(other.getJrsDia()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.locTit==null && other.getLocTit()==null) || 
             (this.locTit!=null &&
              this.locTit.equals(other.getLocTit()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.nsuTef==null && other.getNsuTef()==null) || 
             (this.nsuTef!=null &&
              this.nsuTef.equals(other.getNsuTef()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsTcr==null && other.getObsTcr()==null) || 
             (this.obsTcr!=null &&
              this.obsTcr.equals(other.getObsTcr()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perCom==null && other.getPerCom()==null) || 
             (this.perCom!=null &&
              this.perCom.equals(other.getPerCom()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.proJrs==null && other.getProJrs()==null) || 
             (this.proJrs!=null &&
              this.proJrs.equals(other.getProJrs()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.sitTit==null && other.getSitTit()==null) || 
             (this.sitTit!=null &&
              this.sitTit.equals(other.getSitTit()))) &&
            ((this.taxNeg==null && other.getTaxNeg()==null) || 
             (this.taxNeg!=null &&
              this.taxNeg.equals(other.getTaxNeg()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrAbe==null && other.getVlrAbe()==null) || 
             (this.vlrAbe!=null &&
              this.vlrAbe.equals(other.getVlrAbe()))) &&
            ((this.vlrBco==null && other.getVlrBco()==null) || 
             (this.vlrBco!=null &&
              this.vlrBco.equals(other.getVlrBco()))) &&
            ((this.vlrCom==null && other.getVlrCom()==null) || 
             (this.vlrCom!=null &&
              this.vlrCom.equals(other.getVlrCom()))) &&
            ((this.vlrDca==null && other.getVlrDca()==null) || 
             (this.vlrDca!=null &&
              this.vlrDca.equals(other.getVlrDca()))) &&
            ((this.vlrDcb==null && other.getVlrDcb()==null) || 
             (this.vlrDcb!=null &&
              this.vlrDcb.equals(other.getVlrDcb()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri()))) &&
            ((this.vlrOud==null && other.getVlrOud()==null) || 
             (this.vlrOud!=null &&
              this.vlrOud.equals(other.getVlrOud())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCatTef() != null) {
            _hashCode += getCatTef().hashCode();
        }
        if (getCheAge() != null) {
            _hashCode += getCheAge().hashCode();
        }
        if (getCheBan() != null) {
            _hashCode += getCheBan().hashCode();
        }
        if (getCheCta() != null) {
            _hashCode += getCheCta().hashCode();
        }
        if (getCheNum() != null) {
            _hashCode += getCheNum().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodMpt() != null) {
            _hashCode += getCodMpt().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodRep() != null) {
            _hashCode += getCodRep().hashCode();
        }
        if (getCodSac() != null) {
            _hashCode += getCodSac().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getComRec() != null) {
            _hashCode += getComRec().hashCode();
        }
        if (getCpgNeg() != null) {
            _hashCode += getCpgNeg().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getDscNeg() != null) {
            _hashCode += getDscNeg().hashCode();
        }
        if (getJrsDia() != null) {
            _hashCode += getJrsDia().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getLocTit() != null) {
            _hashCode += getLocTit().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNsuTef() != null) {
            _hashCode += getNsuTef().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsTcr() != null) {
            _hashCode += getObsTcr().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerCom() != null) {
            _hashCode += getPerCom().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getProJrs() != null) {
            _hashCode += getProJrs().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSitTit() != null) {
            _hashCode += getSitTit().hashCode();
        }
        if (getTaxNeg() != null) {
            _hashCode += getTaxNeg().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrAbe() != null) {
            _hashCode += getVlrAbe().hashCode();
        }
        if (getVlrBco() != null) {
            _hashCode += getVlrBco().hashCode();
        }
        if (getVlrCom() != null) {
            _hashCode += getVlrCom().hashCode();
        }
        if (getVlrDca() != null) {
            _hashCode += getVlrDca().hashCode();
        }
        if (getVlrDcb() != null) {
            _hashCode += getVlrDcb().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        if (getVlrOud() != null) {
            _hashCode += getVlrOud().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExportarTitulosReceber2OutTitulosReceber.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarTitulosReceber2OutTitulosReceber"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comRec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "comRec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpgNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cpgNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nsuTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nsuTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taxNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAbe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAbe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDcb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDcb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOud");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOud"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
