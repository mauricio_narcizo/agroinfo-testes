/**
 * FornecedoresExportarOutGridFornecedoresINSS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportarOutGridFornecedoresINSS  implements java.io.Serializable {
    private java.lang.Integer codFor;

    private java.lang.String mesAno;

    private java.lang.Double vlrBie;

    private java.lang.Double vlrBit;

    private java.lang.Double vlrIne;

    private java.lang.Double vlrInt;

    public FornecedoresExportarOutGridFornecedoresINSS() {
    }

    public FornecedoresExportarOutGridFornecedoresINSS(
           java.lang.Integer codFor,
           java.lang.String mesAno,
           java.lang.Double vlrBie,
           java.lang.Double vlrBit,
           java.lang.Double vlrIne,
           java.lang.Double vlrInt) {
           this.codFor = codFor;
           this.mesAno = mesAno;
           this.vlrBie = vlrBie;
           this.vlrBit = vlrBit;
           this.vlrIne = vlrIne;
           this.vlrInt = vlrInt;
    }


    /**
     * Gets the codFor value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the mesAno value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return mesAno
     */
    public java.lang.String getMesAno() {
        return mesAno;
    }


    /**
     * Sets the mesAno value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param mesAno
     */
    public void setMesAno(java.lang.String mesAno) {
        this.mesAno = mesAno;
    }


    /**
     * Gets the vlrBie value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return vlrBie
     */
    public java.lang.Double getVlrBie() {
        return vlrBie;
    }


    /**
     * Sets the vlrBie value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param vlrBie
     */
    public void setVlrBie(java.lang.Double vlrBie) {
        this.vlrBie = vlrBie;
    }


    /**
     * Gets the vlrBit value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return vlrBit
     */
    public java.lang.Double getVlrBit() {
        return vlrBit;
    }


    /**
     * Sets the vlrBit value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param vlrBit
     */
    public void setVlrBit(java.lang.Double vlrBit) {
        this.vlrBit = vlrBit;
    }


    /**
     * Gets the vlrIne value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return vlrIne
     */
    public java.lang.Double getVlrIne() {
        return vlrIne;
    }


    /**
     * Sets the vlrIne value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param vlrIne
     */
    public void setVlrIne(java.lang.Double vlrIne) {
        this.vlrIne = vlrIne;
    }


    /**
     * Gets the vlrInt value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @return vlrInt
     */
    public java.lang.Double getVlrInt() {
        return vlrInt;
    }


    /**
     * Sets the vlrInt value for this FornecedoresExportarOutGridFornecedoresINSS.
     * 
     * @param vlrInt
     */
    public void setVlrInt(java.lang.Double vlrInt) {
        this.vlrInt = vlrInt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportarOutGridFornecedoresINSS)) return false;
        FornecedoresExportarOutGridFornecedoresINSS other = (FornecedoresExportarOutGridFornecedoresINSS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.mesAno==null && other.getMesAno()==null) || 
             (this.mesAno!=null &&
              this.mesAno.equals(other.getMesAno()))) &&
            ((this.vlrBie==null && other.getVlrBie()==null) || 
             (this.vlrBie!=null &&
              this.vlrBie.equals(other.getVlrBie()))) &&
            ((this.vlrBit==null && other.getVlrBit()==null) || 
             (this.vlrBit!=null &&
              this.vlrBit.equals(other.getVlrBit()))) &&
            ((this.vlrIne==null && other.getVlrIne()==null) || 
             (this.vlrIne!=null &&
              this.vlrIne.equals(other.getVlrIne()))) &&
            ((this.vlrInt==null && other.getVlrInt()==null) || 
             (this.vlrInt!=null &&
              this.vlrInt.equals(other.getVlrInt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getMesAno() != null) {
            _hashCode += getMesAno().hashCode();
        }
        if (getVlrBie() != null) {
            _hashCode += getVlrBie().hashCode();
        }
        if (getVlrBit() != null) {
            _hashCode += getVlrBit().hashCode();
        }
        if (getVlrIne() != null) {
            _hashCode += getVlrIne().hashCode();
        }
        if (getVlrInt() != null) {
            _hashCode += getVlrInt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportarOutGridFornecedoresINSS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresINSS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mesAno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mesAno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBie");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrIne");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrIne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
