/**
 * FornecedoresExportar2OutGridFornecedoresDefHis.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportar2OutGridFornecedoresDefHis  implements java.io.Serializable {
    private java.lang.String antDsc;

    private java.lang.String ccbFor;

    private java.lang.String cifFob;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codCpg;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.String codDep;

    private java.lang.Integer codEmp;

    private java.lang.Double codFav;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.Integer codFpg;

    private java.lang.String codPor;

    private java.lang.String codTpr;

    private java.lang.Integer codTra;

    private java.lang.String conEst;

    private java.lang.Integer cprCat;

    private java.lang.Integer cprCpe;

    private java.lang.Integer cprCql;

    private java.lang.String criEdv;

    private java.lang.Integer criRat;

    private java.lang.Integer ctaAad;

    private java.lang.Integer ctaAux;

    private java.lang.Integer ctaFcr;

    private java.lang.Integer ctaFdv;

    private java.lang.Integer ctaRcr;

    private java.lang.Integer ctaRed;

    private java.lang.String datAtr;

    private java.lang.String datMcp;

    private java.lang.String datUcp;

    private java.lang.String datUpe;

    private java.lang.String datUpg;

    private java.lang.String forMon;

    private java.lang.String indInd;

    private java.lang.Integer maiAtr;

    private java.lang.Integer medAtr;

    private java.lang.String numCcc;

    private java.lang.Integer pagDtj;

    private java.lang.Integer pagDtm;

    private java.lang.Double pagJmm;

    private java.lang.Double pagMul;

    private java.lang.String pagTir;

    private java.lang.Double perDs1;

    private java.lang.Double perDs2;

    private java.lang.Double perDs3;

    private java.lang.Double perDs4;

    private java.lang.Double perDs5;

    private java.lang.Double perDsc;

    private java.lang.Double perEmb;

    private java.lang.Double perEnc;

    private java.lang.Double perFre;

    private java.lang.Double perFun;

    private java.lang.Double perIne;

    private java.lang.Double perIns;

    private java.lang.Double perIrf;

    private java.lang.Double perIss;

    private java.lang.Double perOut;

    private java.lang.Double perSeg;

    private java.lang.String pgtFre;

    private java.lang.String pgtMon;

    private java.lang.Integer przEnt;

    private java.lang.Integer qtdDcv;

    private java.lang.Integer qtdPgt;

    private java.lang.String rvlCfr;

    private java.lang.String rvlDar;

    private java.lang.String rvlEmb;

    private java.lang.String rvlEnc;

    private java.lang.String rvlFei;

    private java.lang.String rvlFre;

    private java.lang.String rvlOui;

    private java.lang.String rvlOut;

    private java.lang.String rvlSeg;

    private java.lang.String rvlSei;

    private java.lang.Double salCre;

    private java.lang.Double salDup;

    private java.lang.Double salOut;

    private java.lang.Integer seqOrm;

    private java.lang.String serCur;

    private java.lang.Integer tolDsc;

    private java.lang.Double ultDup;

    private java.lang.Double vlrAtr;

    private java.lang.Double vlrMcp;

    private java.lang.Double vlrUcp;

    private java.lang.Double vlrUpe;

    private java.lang.Double vlrUpg;

    public FornecedoresExportar2OutGridFornecedoresDefHis() {
    }

    public FornecedoresExportar2OutGridFornecedoresDefHis(
           java.lang.String antDsc,
           java.lang.String ccbFor,
           java.lang.String cifFob,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codCpg,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.String codDep,
           java.lang.Integer codEmp,
           java.lang.Double codFav,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.Integer codFpg,
           java.lang.String codPor,
           java.lang.String codTpr,
           java.lang.Integer codTra,
           java.lang.String conEst,
           java.lang.Integer cprCat,
           java.lang.Integer cprCpe,
           java.lang.Integer cprCql,
           java.lang.String criEdv,
           java.lang.Integer criRat,
           java.lang.Integer ctaAad,
           java.lang.Integer ctaAux,
           java.lang.Integer ctaFcr,
           java.lang.Integer ctaFdv,
           java.lang.Integer ctaRcr,
           java.lang.Integer ctaRed,
           java.lang.String datAtr,
           java.lang.String datMcp,
           java.lang.String datUcp,
           java.lang.String datUpe,
           java.lang.String datUpg,
           java.lang.String forMon,
           java.lang.String indInd,
           java.lang.Integer maiAtr,
           java.lang.Integer medAtr,
           java.lang.String numCcc,
           java.lang.Integer pagDtj,
           java.lang.Integer pagDtm,
           java.lang.Double pagJmm,
           java.lang.Double pagMul,
           java.lang.String pagTir,
           java.lang.Double perDs1,
           java.lang.Double perDs2,
           java.lang.Double perDs3,
           java.lang.Double perDs4,
           java.lang.Double perDs5,
           java.lang.Double perDsc,
           java.lang.Double perEmb,
           java.lang.Double perEnc,
           java.lang.Double perFre,
           java.lang.Double perFun,
           java.lang.Double perIne,
           java.lang.Double perIns,
           java.lang.Double perIrf,
           java.lang.Double perIss,
           java.lang.Double perOut,
           java.lang.Double perSeg,
           java.lang.String pgtFre,
           java.lang.String pgtMon,
           java.lang.Integer przEnt,
           java.lang.Integer qtdDcv,
           java.lang.Integer qtdPgt,
           java.lang.String rvlCfr,
           java.lang.String rvlDar,
           java.lang.String rvlEmb,
           java.lang.String rvlEnc,
           java.lang.String rvlFei,
           java.lang.String rvlFre,
           java.lang.String rvlOui,
           java.lang.String rvlOut,
           java.lang.String rvlSeg,
           java.lang.String rvlSei,
           java.lang.Double salCre,
           java.lang.Double salDup,
           java.lang.Double salOut,
           java.lang.Integer seqOrm,
           java.lang.String serCur,
           java.lang.Integer tolDsc,
           java.lang.Double ultDup,
           java.lang.Double vlrAtr,
           java.lang.Double vlrMcp,
           java.lang.Double vlrUcp,
           java.lang.Double vlrUpe,
           java.lang.Double vlrUpg) {
           this.antDsc = antDsc;
           this.ccbFor = ccbFor;
           this.cifFob = cifFob;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codCpg = codCpg;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codDep = codDep;
           this.codEmp = codEmp;
           this.codFav = codFav;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codPor = codPor;
           this.codTpr = codTpr;
           this.codTra = codTra;
           this.conEst = conEst;
           this.cprCat = cprCat;
           this.cprCpe = cprCpe;
           this.cprCql = cprCql;
           this.criEdv = criEdv;
           this.criRat = criRat;
           this.ctaAad = ctaAad;
           this.ctaAux = ctaAux;
           this.ctaFcr = ctaFcr;
           this.ctaFdv = ctaFdv;
           this.ctaRcr = ctaRcr;
           this.ctaRed = ctaRed;
           this.datAtr = datAtr;
           this.datMcp = datMcp;
           this.datUcp = datUcp;
           this.datUpe = datUpe;
           this.datUpg = datUpg;
           this.forMon = forMon;
           this.indInd = indInd;
           this.maiAtr = maiAtr;
           this.medAtr = medAtr;
           this.numCcc = numCcc;
           this.pagDtj = pagDtj;
           this.pagDtm = pagDtm;
           this.pagJmm = pagJmm;
           this.pagMul = pagMul;
           this.pagTir = pagTir;
           this.perDs1 = perDs1;
           this.perDs2 = perDs2;
           this.perDs3 = perDs3;
           this.perDs4 = perDs4;
           this.perDs5 = perDs5;
           this.perDsc = perDsc;
           this.perEmb = perEmb;
           this.perEnc = perEnc;
           this.perFre = perFre;
           this.perFun = perFun;
           this.perIne = perIne;
           this.perIns = perIns;
           this.perIrf = perIrf;
           this.perIss = perIss;
           this.perOut = perOut;
           this.perSeg = perSeg;
           this.pgtFre = pgtFre;
           this.pgtMon = pgtMon;
           this.przEnt = przEnt;
           this.qtdDcv = qtdDcv;
           this.qtdPgt = qtdPgt;
           this.rvlCfr = rvlCfr;
           this.rvlDar = rvlDar;
           this.rvlEmb = rvlEmb;
           this.rvlEnc = rvlEnc;
           this.rvlFei = rvlFei;
           this.rvlFre = rvlFre;
           this.rvlOui = rvlOui;
           this.rvlOut = rvlOut;
           this.rvlSeg = rvlSeg;
           this.rvlSei = rvlSei;
           this.salCre = salCre;
           this.salDup = salDup;
           this.salOut = salOut;
           this.seqOrm = seqOrm;
           this.serCur = serCur;
           this.tolDsc = tolDsc;
           this.ultDup = ultDup;
           this.vlrAtr = vlrAtr;
           this.vlrMcp = vlrMcp;
           this.vlrUcp = vlrUcp;
           this.vlrUpe = vlrUpe;
           this.vlrUpg = vlrUpg;
    }


    /**
     * Gets the antDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the ccbFor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the cifFob value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the codAge value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codCpg
     */
    public java.lang.String getCodCpg() {
        return codCpg;
    }


    /**
     * Sets the codCpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codCpg
     */
    public void setCodCpg(java.lang.String codCpg) {
        this.codCpg = codCpg;
    }


    /**
     * Gets the codCrp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codDep value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codDep
     */
    public java.lang.String getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.String codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the codEmp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFav value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codFav
     */
    public java.lang.Double getCodFav() {
        return codFav;
    }


    /**
     * Sets the codFav value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codFav
     */
    public void setCodFav(java.lang.Double codFav) {
        this.codFav = codFav;
    }


    /**
     * Gets the codFil value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codPor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTpr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codTpr
     */
    public java.lang.String getCodTpr() {
        return codTpr;
    }


    /**
     * Sets the codTpr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codTpr
     */
    public void setCodTpr(java.lang.String codTpr) {
        this.codTpr = codTpr;
    }


    /**
     * Gets the codTra value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return codTra
     */
    public java.lang.Integer getCodTra() {
        return codTra;
    }


    /**
     * Sets the codTra value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param codTra
     */
    public void setCodTra(java.lang.Integer codTra) {
        this.codTra = codTra;
    }


    /**
     * Gets the conEst value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return conEst
     */
    public java.lang.String getConEst() {
        return conEst;
    }


    /**
     * Sets the conEst value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param conEst
     */
    public void setConEst(java.lang.String conEst) {
        this.conEst = conEst;
    }


    /**
     * Gets the cprCat value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return cprCat
     */
    public java.lang.Integer getCprCat() {
        return cprCat;
    }


    /**
     * Sets the cprCat value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param cprCat
     */
    public void setCprCat(java.lang.Integer cprCat) {
        this.cprCat = cprCat;
    }


    /**
     * Gets the cprCpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return cprCpe
     */
    public java.lang.Integer getCprCpe() {
        return cprCpe;
    }


    /**
     * Sets the cprCpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param cprCpe
     */
    public void setCprCpe(java.lang.Integer cprCpe) {
        this.cprCpe = cprCpe;
    }


    /**
     * Gets the cprCql value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return cprCql
     */
    public java.lang.Integer getCprCql() {
        return cprCql;
    }


    /**
     * Sets the cprCql value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param cprCql
     */
    public void setCprCql(java.lang.Integer cprCql) {
        this.cprCql = cprCql;
    }


    /**
     * Gets the criEdv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return criEdv
     */
    public java.lang.String getCriEdv() {
        return criEdv;
    }


    /**
     * Sets the criEdv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param criEdv
     */
    public void setCriEdv(java.lang.String criEdv) {
        this.criEdv = criEdv;
    }


    /**
     * Gets the criRat value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return criRat
     */
    public java.lang.Integer getCriRat() {
        return criRat;
    }


    /**
     * Sets the criRat value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param criRat
     */
    public void setCriRat(java.lang.Integer criRat) {
        this.criRat = criRat;
    }


    /**
     * Gets the ctaAad value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaAad
     */
    public java.lang.Integer getCtaAad() {
        return ctaAad;
    }


    /**
     * Sets the ctaAad value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaAad
     */
    public void setCtaAad(java.lang.Integer ctaAad) {
        this.ctaAad = ctaAad;
    }


    /**
     * Gets the ctaAux value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaAux
     */
    public java.lang.Integer getCtaAux() {
        return ctaAux;
    }


    /**
     * Sets the ctaAux value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaAux
     */
    public void setCtaAux(java.lang.Integer ctaAux) {
        this.ctaAux = ctaAux;
    }


    /**
     * Gets the ctaFcr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaFcr
     */
    public java.lang.Integer getCtaFcr() {
        return ctaFcr;
    }


    /**
     * Sets the ctaFcr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaFcr
     */
    public void setCtaFcr(java.lang.Integer ctaFcr) {
        this.ctaFcr = ctaFcr;
    }


    /**
     * Gets the ctaFdv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaFdv
     */
    public java.lang.Integer getCtaFdv() {
        return ctaFdv;
    }


    /**
     * Sets the ctaFdv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaFdv
     */
    public void setCtaFdv(java.lang.Integer ctaFdv) {
        this.ctaFdv = ctaFdv;
    }


    /**
     * Gets the ctaRcr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaRcr
     */
    public java.lang.Integer getCtaRcr() {
        return ctaRcr;
    }


    /**
     * Sets the ctaRcr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaRcr
     */
    public void setCtaRcr(java.lang.Integer ctaRcr) {
        this.ctaRcr = ctaRcr;
    }


    /**
     * Gets the ctaRed value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ctaRed
     */
    public java.lang.Integer getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.Integer ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return datAtr
     */
    public java.lang.String getDatAtr() {
        return datAtr;
    }


    /**
     * Sets the datAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param datAtr
     */
    public void setDatAtr(java.lang.String datAtr) {
        this.datAtr = datAtr;
    }


    /**
     * Gets the datMcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return datMcp
     */
    public java.lang.String getDatMcp() {
        return datMcp;
    }


    /**
     * Sets the datMcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param datMcp
     */
    public void setDatMcp(java.lang.String datMcp) {
        this.datMcp = datMcp;
    }


    /**
     * Gets the datUcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return datUcp
     */
    public java.lang.String getDatUcp() {
        return datUcp;
    }


    /**
     * Sets the datUcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param datUcp
     */
    public void setDatUcp(java.lang.String datUcp) {
        this.datUcp = datUcp;
    }


    /**
     * Gets the datUpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return datUpe
     */
    public java.lang.String getDatUpe() {
        return datUpe;
    }


    /**
     * Sets the datUpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param datUpe
     */
    public void setDatUpe(java.lang.String datUpe) {
        this.datUpe = datUpe;
    }


    /**
     * Gets the datUpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return datUpg
     */
    public java.lang.String getDatUpg() {
        return datUpg;
    }


    /**
     * Sets the datUpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param datUpg
     */
    public void setDatUpg(java.lang.String datUpg) {
        this.datUpg = datUpg;
    }


    /**
     * Gets the forMon value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return forMon
     */
    public java.lang.String getForMon() {
        return forMon;
    }


    /**
     * Sets the forMon value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param forMon
     */
    public void setForMon(java.lang.String forMon) {
        this.forMon = forMon;
    }


    /**
     * Gets the indInd value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return indInd
     */
    public java.lang.String getIndInd() {
        return indInd;
    }


    /**
     * Sets the indInd value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param indInd
     */
    public void setIndInd(java.lang.String indInd) {
        this.indInd = indInd;
    }


    /**
     * Gets the maiAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return maiAtr
     */
    public java.lang.Integer getMaiAtr() {
        return maiAtr;
    }


    /**
     * Sets the maiAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param maiAtr
     */
    public void setMaiAtr(java.lang.Integer maiAtr) {
        this.maiAtr = maiAtr;
    }


    /**
     * Gets the medAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return medAtr
     */
    public java.lang.Integer getMedAtr() {
        return medAtr;
    }


    /**
     * Sets the medAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param medAtr
     */
    public void setMedAtr(java.lang.Integer medAtr) {
        this.medAtr = medAtr;
    }


    /**
     * Gets the numCcc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return numCcc
     */
    public java.lang.String getNumCcc() {
        return numCcc;
    }


    /**
     * Sets the numCcc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param numCcc
     */
    public void setNumCcc(java.lang.String numCcc) {
        this.numCcc = numCcc;
    }


    /**
     * Gets the pagDtj value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pagDtj
     */
    public java.lang.Integer getPagDtj() {
        return pagDtj;
    }


    /**
     * Sets the pagDtj value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pagDtj
     */
    public void setPagDtj(java.lang.Integer pagDtj) {
        this.pagDtj = pagDtj;
    }


    /**
     * Gets the pagDtm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pagDtm
     */
    public java.lang.Integer getPagDtm() {
        return pagDtm;
    }


    /**
     * Sets the pagDtm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pagDtm
     */
    public void setPagDtm(java.lang.Integer pagDtm) {
        this.pagDtm = pagDtm;
    }


    /**
     * Gets the pagJmm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pagJmm
     */
    public java.lang.Double getPagJmm() {
        return pagJmm;
    }


    /**
     * Sets the pagJmm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pagJmm
     */
    public void setPagJmm(java.lang.Double pagJmm) {
        this.pagJmm = pagJmm;
    }


    /**
     * Gets the pagMul value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pagMul
     */
    public java.lang.Double getPagMul() {
        return pagMul;
    }


    /**
     * Sets the pagMul value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pagMul
     */
    public void setPagMul(java.lang.Double pagMul) {
        this.pagMul = pagMul;
    }


    /**
     * Gets the pagTir value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pagTir
     */
    public java.lang.String getPagTir() {
        return pagTir;
    }


    /**
     * Sets the pagTir value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pagTir
     */
    public void setPagTir(java.lang.String pagTir) {
        this.pagTir = pagTir;
    }


    /**
     * Gets the perDs1 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDs1
     */
    public java.lang.Double getPerDs1() {
        return perDs1;
    }


    /**
     * Sets the perDs1 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDs1
     */
    public void setPerDs1(java.lang.Double perDs1) {
        this.perDs1 = perDs1;
    }


    /**
     * Gets the perDs2 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDs2
     */
    public java.lang.Double getPerDs2() {
        return perDs2;
    }


    /**
     * Sets the perDs2 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDs2
     */
    public void setPerDs2(java.lang.Double perDs2) {
        this.perDs2 = perDs2;
    }


    /**
     * Gets the perDs3 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDs3
     */
    public java.lang.Double getPerDs3() {
        return perDs3;
    }


    /**
     * Sets the perDs3 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDs3
     */
    public void setPerDs3(java.lang.Double perDs3) {
        this.perDs3 = perDs3;
    }


    /**
     * Gets the perDs4 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDs4
     */
    public java.lang.Double getPerDs4() {
        return perDs4;
    }


    /**
     * Sets the perDs4 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDs4
     */
    public void setPerDs4(java.lang.Double perDs4) {
        this.perDs4 = perDs4;
    }


    /**
     * Gets the perDs5 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDs5
     */
    public java.lang.Double getPerDs5() {
        return perDs5;
    }


    /**
     * Sets the perDs5 value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDs5
     */
    public void setPerDs5(java.lang.Double perDs5) {
        this.perDs5 = perDs5;
    }


    /**
     * Gets the perDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perEmb value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perEmb
     */
    public java.lang.Double getPerEmb() {
        return perEmb;
    }


    /**
     * Sets the perEmb value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perEmb
     */
    public void setPerEmb(java.lang.Double perEmb) {
        this.perEmb = perEmb;
    }


    /**
     * Gets the perEnc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perEnc
     */
    public java.lang.Double getPerEnc() {
        return perEnc;
    }


    /**
     * Sets the perEnc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perEnc
     */
    public void setPerEnc(java.lang.Double perEnc) {
        this.perEnc = perEnc;
    }


    /**
     * Gets the perFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perFre
     */
    public java.lang.Double getPerFre() {
        return perFre;
    }


    /**
     * Sets the perFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perFre
     */
    public void setPerFre(java.lang.Double perFre) {
        this.perFre = perFre;
    }


    /**
     * Gets the perFun value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perFun
     */
    public java.lang.Double getPerFun() {
        return perFun;
    }


    /**
     * Sets the perFun value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perFun
     */
    public void setPerFun(java.lang.Double perFun) {
        this.perFun = perFun;
    }


    /**
     * Gets the perIne value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perIne
     */
    public java.lang.Double getPerIne() {
        return perIne;
    }


    /**
     * Sets the perIne value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perIne
     */
    public void setPerIne(java.lang.Double perIne) {
        this.perIne = perIne;
    }


    /**
     * Gets the perIns value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perIns
     */
    public java.lang.Double getPerIns() {
        return perIns;
    }


    /**
     * Sets the perIns value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perIns
     */
    public void setPerIns(java.lang.Double perIns) {
        this.perIns = perIns;
    }


    /**
     * Gets the perIrf value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perIrf
     */
    public java.lang.Double getPerIrf() {
        return perIrf;
    }


    /**
     * Sets the perIrf value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perIrf
     */
    public void setPerIrf(java.lang.Double perIrf) {
        this.perIrf = perIrf;
    }


    /**
     * Gets the perIss value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perIss
     */
    public java.lang.Double getPerIss() {
        return perIss;
    }


    /**
     * Sets the perIss value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perIss
     */
    public void setPerIss(java.lang.Double perIss) {
        this.perIss = perIss;
    }


    /**
     * Gets the perOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perOut
     */
    public java.lang.Double getPerOut() {
        return perOut;
    }


    /**
     * Sets the perOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perOut
     */
    public void setPerOut(java.lang.Double perOut) {
        this.perOut = perOut;
    }


    /**
     * Gets the perSeg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return perSeg
     */
    public java.lang.Double getPerSeg() {
        return perSeg;
    }


    /**
     * Sets the perSeg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param perSeg
     */
    public void setPerSeg(java.lang.Double perSeg) {
        this.perSeg = perSeg;
    }


    /**
     * Gets the pgtFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pgtFre
     */
    public java.lang.String getPgtFre() {
        return pgtFre;
    }


    /**
     * Sets the pgtFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pgtFre
     */
    public void setPgtFre(java.lang.String pgtFre) {
        this.pgtFre = pgtFre;
    }


    /**
     * Gets the pgtMon value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return pgtMon
     */
    public java.lang.String getPgtMon() {
        return pgtMon;
    }


    /**
     * Sets the pgtMon value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param pgtMon
     */
    public void setPgtMon(java.lang.String pgtMon) {
        this.pgtMon = pgtMon;
    }


    /**
     * Gets the przEnt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return przEnt
     */
    public java.lang.Integer getPrzEnt() {
        return przEnt;
    }


    /**
     * Sets the przEnt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param przEnt
     */
    public void setPrzEnt(java.lang.Integer przEnt) {
        this.przEnt = przEnt;
    }


    /**
     * Gets the qtdDcv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return qtdDcv
     */
    public java.lang.Integer getQtdDcv() {
        return qtdDcv;
    }


    /**
     * Sets the qtdDcv value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param qtdDcv
     */
    public void setQtdDcv(java.lang.Integer qtdDcv) {
        this.qtdDcv = qtdDcv;
    }


    /**
     * Gets the qtdPgt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return qtdPgt
     */
    public java.lang.Integer getQtdPgt() {
        return qtdPgt;
    }


    /**
     * Sets the qtdPgt value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param qtdPgt
     */
    public void setQtdPgt(java.lang.Integer qtdPgt) {
        this.qtdPgt = qtdPgt;
    }


    /**
     * Gets the rvlCfr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlCfr
     */
    public java.lang.String getRvlCfr() {
        return rvlCfr;
    }


    /**
     * Sets the rvlCfr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlCfr
     */
    public void setRvlCfr(java.lang.String rvlCfr) {
        this.rvlCfr = rvlCfr;
    }


    /**
     * Gets the rvlDar value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlDar
     */
    public java.lang.String getRvlDar() {
        return rvlDar;
    }


    /**
     * Sets the rvlDar value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlDar
     */
    public void setRvlDar(java.lang.String rvlDar) {
        this.rvlDar = rvlDar;
    }


    /**
     * Gets the rvlEmb value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlEmb
     */
    public java.lang.String getRvlEmb() {
        return rvlEmb;
    }


    /**
     * Sets the rvlEmb value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlEmb
     */
    public void setRvlEmb(java.lang.String rvlEmb) {
        this.rvlEmb = rvlEmb;
    }


    /**
     * Gets the rvlEnc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlEnc
     */
    public java.lang.String getRvlEnc() {
        return rvlEnc;
    }


    /**
     * Sets the rvlEnc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlEnc
     */
    public void setRvlEnc(java.lang.String rvlEnc) {
        this.rvlEnc = rvlEnc;
    }


    /**
     * Gets the rvlFei value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlFei
     */
    public java.lang.String getRvlFei() {
        return rvlFei;
    }


    /**
     * Sets the rvlFei value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlFei
     */
    public void setRvlFei(java.lang.String rvlFei) {
        this.rvlFei = rvlFei;
    }


    /**
     * Gets the rvlFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlFre
     */
    public java.lang.String getRvlFre() {
        return rvlFre;
    }


    /**
     * Sets the rvlFre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlFre
     */
    public void setRvlFre(java.lang.String rvlFre) {
        this.rvlFre = rvlFre;
    }


    /**
     * Gets the rvlOui value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlOui
     */
    public java.lang.String getRvlOui() {
        return rvlOui;
    }


    /**
     * Sets the rvlOui value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlOui
     */
    public void setRvlOui(java.lang.String rvlOui) {
        this.rvlOui = rvlOui;
    }


    /**
     * Gets the rvlOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlOut
     */
    public java.lang.String getRvlOut() {
        return rvlOut;
    }


    /**
     * Sets the rvlOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlOut
     */
    public void setRvlOut(java.lang.String rvlOut) {
        this.rvlOut = rvlOut;
    }


    /**
     * Gets the rvlSeg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlSeg
     */
    public java.lang.String getRvlSeg() {
        return rvlSeg;
    }


    /**
     * Sets the rvlSeg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlSeg
     */
    public void setRvlSeg(java.lang.String rvlSeg) {
        this.rvlSeg = rvlSeg;
    }


    /**
     * Gets the rvlSei value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return rvlSei
     */
    public java.lang.String getRvlSei() {
        return rvlSei;
    }


    /**
     * Sets the rvlSei value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param rvlSei
     */
    public void setRvlSei(java.lang.String rvlSei) {
        this.rvlSei = rvlSei;
    }


    /**
     * Gets the salCre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return salCre
     */
    public java.lang.Double getSalCre() {
        return salCre;
    }


    /**
     * Sets the salCre value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param salCre
     */
    public void setSalCre(java.lang.Double salCre) {
        this.salCre = salCre;
    }


    /**
     * Gets the salDup value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return salDup
     */
    public java.lang.Double getSalDup() {
        return salDup;
    }


    /**
     * Sets the salDup value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param salDup
     */
    public void setSalDup(java.lang.Double salDup) {
        this.salDup = salDup;
    }


    /**
     * Gets the salOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return salOut
     */
    public java.lang.Double getSalOut() {
        return salOut;
    }


    /**
     * Sets the salOut value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param salOut
     */
    public void setSalOut(java.lang.Double salOut) {
        this.salOut = salOut;
    }


    /**
     * Gets the seqOrm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return seqOrm
     */
    public java.lang.Integer getSeqOrm() {
        return seqOrm;
    }


    /**
     * Sets the seqOrm value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param seqOrm
     */
    public void setSeqOrm(java.lang.Integer seqOrm) {
        this.seqOrm = seqOrm;
    }


    /**
     * Gets the serCur value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return serCur
     */
    public java.lang.String getSerCur() {
        return serCur;
    }


    /**
     * Sets the serCur value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param serCur
     */
    public void setSerCur(java.lang.String serCur) {
        this.serCur = serCur;
    }


    /**
     * Gets the tolDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the ultDup value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return ultDup
     */
    public java.lang.Double getUltDup() {
        return ultDup;
    }


    /**
     * Sets the ultDup value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param ultDup
     */
    public void setUltDup(java.lang.Double ultDup) {
        this.ultDup = ultDup;
    }


    /**
     * Gets the vlrAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return vlrAtr
     */
    public java.lang.Double getVlrAtr() {
        return vlrAtr;
    }


    /**
     * Sets the vlrAtr value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param vlrAtr
     */
    public void setVlrAtr(java.lang.Double vlrAtr) {
        this.vlrAtr = vlrAtr;
    }


    /**
     * Gets the vlrMcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return vlrMcp
     */
    public java.lang.Double getVlrMcp() {
        return vlrMcp;
    }


    /**
     * Sets the vlrMcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param vlrMcp
     */
    public void setVlrMcp(java.lang.Double vlrMcp) {
        this.vlrMcp = vlrMcp;
    }


    /**
     * Gets the vlrUcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return vlrUcp
     */
    public java.lang.Double getVlrUcp() {
        return vlrUcp;
    }


    /**
     * Sets the vlrUcp value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param vlrUcp
     */
    public void setVlrUcp(java.lang.Double vlrUcp) {
        this.vlrUcp = vlrUcp;
    }


    /**
     * Gets the vlrUpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return vlrUpe
     */
    public java.lang.Double getVlrUpe() {
        return vlrUpe;
    }


    /**
     * Sets the vlrUpe value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param vlrUpe
     */
    public void setVlrUpe(java.lang.Double vlrUpe) {
        this.vlrUpe = vlrUpe;
    }


    /**
     * Gets the vlrUpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @return vlrUpg
     */
    public java.lang.Double getVlrUpg() {
        return vlrUpg;
    }


    /**
     * Sets the vlrUpg value for this FornecedoresExportar2OutGridFornecedoresDefHis.
     * 
     * @param vlrUpg
     */
    public void setVlrUpg(java.lang.Double vlrUpg) {
        this.vlrUpg = vlrUpg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportar2OutGridFornecedoresDefHis)) return false;
        FornecedoresExportar2OutGridFornecedoresDefHis other = (FornecedoresExportar2OutGridFornecedoresDefHis) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCpg==null && other.getCodCpg()==null) || 
             (this.codCpg!=null &&
              this.codCpg.equals(other.getCodCpg()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFav==null && other.getCodFav()==null) || 
             (this.codFav!=null &&
              this.codFav.equals(other.getCodFav()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTpr==null && other.getCodTpr()==null) || 
             (this.codTpr!=null &&
              this.codTpr.equals(other.getCodTpr()))) &&
            ((this.codTra==null && other.getCodTra()==null) || 
             (this.codTra!=null &&
              this.codTra.equals(other.getCodTra()))) &&
            ((this.conEst==null && other.getConEst()==null) || 
             (this.conEst!=null &&
              this.conEst.equals(other.getConEst()))) &&
            ((this.cprCat==null && other.getCprCat()==null) || 
             (this.cprCat!=null &&
              this.cprCat.equals(other.getCprCat()))) &&
            ((this.cprCpe==null && other.getCprCpe()==null) || 
             (this.cprCpe!=null &&
              this.cprCpe.equals(other.getCprCpe()))) &&
            ((this.cprCql==null && other.getCprCql()==null) || 
             (this.cprCql!=null &&
              this.cprCql.equals(other.getCprCql()))) &&
            ((this.criEdv==null && other.getCriEdv()==null) || 
             (this.criEdv!=null &&
              this.criEdv.equals(other.getCriEdv()))) &&
            ((this.criRat==null && other.getCriRat()==null) || 
             (this.criRat!=null &&
              this.criRat.equals(other.getCriRat()))) &&
            ((this.ctaAad==null && other.getCtaAad()==null) || 
             (this.ctaAad!=null &&
              this.ctaAad.equals(other.getCtaAad()))) &&
            ((this.ctaAux==null && other.getCtaAux()==null) || 
             (this.ctaAux!=null &&
              this.ctaAux.equals(other.getCtaAux()))) &&
            ((this.ctaFcr==null && other.getCtaFcr()==null) || 
             (this.ctaFcr!=null &&
              this.ctaFcr.equals(other.getCtaFcr()))) &&
            ((this.ctaFdv==null && other.getCtaFdv()==null) || 
             (this.ctaFdv!=null &&
              this.ctaFdv.equals(other.getCtaFdv()))) &&
            ((this.ctaRcr==null && other.getCtaRcr()==null) || 
             (this.ctaRcr!=null &&
              this.ctaRcr.equals(other.getCtaRcr()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datAtr==null && other.getDatAtr()==null) || 
             (this.datAtr!=null &&
              this.datAtr.equals(other.getDatAtr()))) &&
            ((this.datMcp==null && other.getDatMcp()==null) || 
             (this.datMcp!=null &&
              this.datMcp.equals(other.getDatMcp()))) &&
            ((this.datUcp==null && other.getDatUcp()==null) || 
             (this.datUcp!=null &&
              this.datUcp.equals(other.getDatUcp()))) &&
            ((this.datUpe==null && other.getDatUpe()==null) || 
             (this.datUpe!=null &&
              this.datUpe.equals(other.getDatUpe()))) &&
            ((this.datUpg==null && other.getDatUpg()==null) || 
             (this.datUpg!=null &&
              this.datUpg.equals(other.getDatUpg()))) &&
            ((this.forMon==null && other.getForMon()==null) || 
             (this.forMon!=null &&
              this.forMon.equals(other.getForMon()))) &&
            ((this.indInd==null && other.getIndInd()==null) || 
             (this.indInd!=null &&
              this.indInd.equals(other.getIndInd()))) &&
            ((this.maiAtr==null && other.getMaiAtr()==null) || 
             (this.maiAtr!=null &&
              this.maiAtr.equals(other.getMaiAtr()))) &&
            ((this.medAtr==null && other.getMedAtr()==null) || 
             (this.medAtr!=null &&
              this.medAtr.equals(other.getMedAtr()))) &&
            ((this.numCcc==null && other.getNumCcc()==null) || 
             (this.numCcc!=null &&
              this.numCcc.equals(other.getNumCcc()))) &&
            ((this.pagDtj==null && other.getPagDtj()==null) || 
             (this.pagDtj!=null &&
              this.pagDtj.equals(other.getPagDtj()))) &&
            ((this.pagDtm==null && other.getPagDtm()==null) || 
             (this.pagDtm!=null &&
              this.pagDtm.equals(other.getPagDtm()))) &&
            ((this.pagJmm==null && other.getPagJmm()==null) || 
             (this.pagJmm!=null &&
              this.pagJmm.equals(other.getPagJmm()))) &&
            ((this.pagMul==null && other.getPagMul()==null) || 
             (this.pagMul!=null &&
              this.pagMul.equals(other.getPagMul()))) &&
            ((this.pagTir==null && other.getPagTir()==null) || 
             (this.pagTir!=null &&
              this.pagTir.equals(other.getPagTir()))) &&
            ((this.perDs1==null && other.getPerDs1()==null) || 
             (this.perDs1!=null &&
              this.perDs1.equals(other.getPerDs1()))) &&
            ((this.perDs2==null && other.getPerDs2()==null) || 
             (this.perDs2!=null &&
              this.perDs2.equals(other.getPerDs2()))) &&
            ((this.perDs3==null && other.getPerDs3()==null) || 
             (this.perDs3!=null &&
              this.perDs3.equals(other.getPerDs3()))) &&
            ((this.perDs4==null && other.getPerDs4()==null) || 
             (this.perDs4!=null &&
              this.perDs4.equals(other.getPerDs4()))) &&
            ((this.perDs5==null && other.getPerDs5()==null) || 
             (this.perDs5!=null &&
              this.perDs5.equals(other.getPerDs5()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perEmb==null && other.getPerEmb()==null) || 
             (this.perEmb!=null &&
              this.perEmb.equals(other.getPerEmb()))) &&
            ((this.perEnc==null && other.getPerEnc()==null) || 
             (this.perEnc!=null &&
              this.perEnc.equals(other.getPerEnc()))) &&
            ((this.perFre==null && other.getPerFre()==null) || 
             (this.perFre!=null &&
              this.perFre.equals(other.getPerFre()))) &&
            ((this.perFun==null && other.getPerFun()==null) || 
             (this.perFun!=null &&
              this.perFun.equals(other.getPerFun()))) &&
            ((this.perIne==null && other.getPerIne()==null) || 
             (this.perIne!=null &&
              this.perIne.equals(other.getPerIne()))) &&
            ((this.perIns==null && other.getPerIns()==null) || 
             (this.perIns!=null &&
              this.perIns.equals(other.getPerIns()))) &&
            ((this.perIrf==null && other.getPerIrf()==null) || 
             (this.perIrf!=null &&
              this.perIrf.equals(other.getPerIrf()))) &&
            ((this.perIss==null && other.getPerIss()==null) || 
             (this.perIss!=null &&
              this.perIss.equals(other.getPerIss()))) &&
            ((this.perOut==null && other.getPerOut()==null) || 
             (this.perOut!=null &&
              this.perOut.equals(other.getPerOut()))) &&
            ((this.perSeg==null && other.getPerSeg()==null) || 
             (this.perSeg!=null &&
              this.perSeg.equals(other.getPerSeg()))) &&
            ((this.pgtFre==null && other.getPgtFre()==null) || 
             (this.pgtFre!=null &&
              this.pgtFre.equals(other.getPgtFre()))) &&
            ((this.pgtMon==null && other.getPgtMon()==null) || 
             (this.pgtMon!=null &&
              this.pgtMon.equals(other.getPgtMon()))) &&
            ((this.przEnt==null && other.getPrzEnt()==null) || 
             (this.przEnt!=null &&
              this.przEnt.equals(other.getPrzEnt()))) &&
            ((this.qtdDcv==null && other.getQtdDcv()==null) || 
             (this.qtdDcv!=null &&
              this.qtdDcv.equals(other.getQtdDcv()))) &&
            ((this.qtdPgt==null && other.getQtdPgt()==null) || 
             (this.qtdPgt!=null &&
              this.qtdPgt.equals(other.getQtdPgt()))) &&
            ((this.rvlCfr==null && other.getRvlCfr()==null) || 
             (this.rvlCfr!=null &&
              this.rvlCfr.equals(other.getRvlCfr()))) &&
            ((this.rvlDar==null && other.getRvlDar()==null) || 
             (this.rvlDar!=null &&
              this.rvlDar.equals(other.getRvlDar()))) &&
            ((this.rvlEmb==null && other.getRvlEmb()==null) || 
             (this.rvlEmb!=null &&
              this.rvlEmb.equals(other.getRvlEmb()))) &&
            ((this.rvlEnc==null && other.getRvlEnc()==null) || 
             (this.rvlEnc!=null &&
              this.rvlEnc.equals(other.getRvlEnc()))) &&
            ((this.rvlFei==null && other.getRvlFei()==null) || 
             (this.rvlFei!=null &&
              this.rvlFei.equals(other.getRvlFei()))) &&
            ((this.rvlFre==null && other.getRvlFre()==null) || 
             (this.rvlFre!=null &&
              this.rvlFre.equals(other.getRvlFre()))) &&
            ((this.rvlOui==null && other.getRvlOui()==null) || 
             (this.rvlOui!=null &&
              this.rvlOui.equals(other.getRvlOui()))) &&
            ((this.rvlOut==null && other.getRvlOut()==null) || 
             (this.rvlOut!=null &&
              this.rvlOut.equals(other.getRvlOut()))) &&
            ((this.rvlSeg==null && other.getRvlSeg()==null) || 
             (this.rvlSeg!=null &&
              this.rvlSeg.equals(other.getRvlSeg()))) &&
            ((this.rvlSei==null && other.getRvlSei()==null) || 
             (this.rvlSei!=null &&
              this.rvlSei.equals(other.getRvlSei()))) &&
            ((this.salCre==null && other.getSalCre()==null) || 
             (this.salCre!=null &&
              this.salCre.equals(other.getSalCre()))) &&
            ((this.salDup==null && other.getSalDup()==null) || 
             (this.salDup!=null &&
              this.salDup.equals(other.getSalDup()))) &&
            ((this.salOut==null && other.getSalOut()==null) || 
             (this.salOut!=null &&
              this.salOut.equals(other.getSalOut()))) &&
            ((this.seqOrm==null && other.getSeqOrm()==null) || 
             (this.seqOrm!=null &&
              this.seqOrm.equals(other.getSeqOrm()))) &&
            ((this.serCur==null && other.getSerCur()==null) || 
             (this.serCur!=null &&
              this.serCur.equals(other.getSerCur()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.ultDup==null && other.getUltDup()==null) || 
             (this.ultDup!=null &&
              this.ultDup.equals(other.getUltDup()))) &&
            ((this.vlrAtr==null && other.getVlrAtr()==null) || 
             (this.vlrAtr!=null &&
              this.vlrAtr.equals(other.getVlrAtr()))) &&
            ((this.vlrMcp==null && other.getVlrMcp()==null) || 
             (this.vlrMcp!=null &&
              this.vlrMcp.equals(other.getVlrMcp()))) &&
            ((this.vlrUcp==null && other.getVlrUcp()==null) || 
             (this.vlrUcp!=null &&
              this.vlrUcp.equals(other.getVlrUcp()))) &&
            ((this.vlrUpe==null && other.getVlrUpe()==null) || 
             (this.vlrUpe!=null &&
              this.vlrUpe.equals(other.getVlrUpe()))) &&
            ((this.vlrUpg==null && other.getVlrUpg()==null) || 
             (this.vlrUpg!=null &&
              this.vlrUpg.equals(other.getVlrUpg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCpg() != null) {
            _hashCode += getCodCpg().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFav() != null) {
            _hashCode += getCodFav().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTpr() != null) {
            _hashCode += getCodTpr().hashCode();
        }
        if (getCodTra() != null) {
            _hashCode += getCodTra().hashCode();
        }
        if (getConEst() != null) {
            _hashCode += getConEst().hashCode();
        }
        if (getCprCat() != null) {
            _hashCode += getCprCat().hashCode();
        }
        if (getCprCpe() != null) {
            _hashCode += getCprCpe().hashCode();
        }
        if (getCprCql() != null) {
            _hashCode += getCprCql().hashCode();
        }
        if (getCriEdv() != null) {
            _hashCode += getCriEdv().hashCode();
        }
        if (getCriRat() != null) {
            _hashCode += getCriRat().hashCode();
        }
        if (getCtaAad() != null) {
            _hashCode += getCtaAad().hashCode();
        }
        if (getCtaAux() != null) {
            _hashCode += getCtaAux().hashCode();
        }
        if (getCtaFcr() != null) {
            _hashCode += getCtaFcr().hashCode();
        }
        if (getCtaFdv() != null) {
            _hashCode += getCtaFdv().hashCode();
        }
        if (getCtaRcr() != null) {
            _hashCode += getCtaRcr().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatAtr() != null) {
            _hashCode += getDatAtr().hashCode();
        }
        if (getDatMcp() != null) {
            _hashCode += getDatMcp().hashCode();
        }
        if (getDatUcp() != null) {
            _hashCode += getDatUcp().hashCode();
        }
        if (getDatUpe() != null) {
            _hashCode += getDatUpe().hashCode();
        }
        if (getDatUpg() != null) {
            _hashCode += getDatUpg().hashCode();
        }
        if (getForMon() != null) {
            _hashCode += getForMon().hashCode();
        }
        if (getIndInd() != null) {
            _hashCode += getIndInd().hashCode();
        }
        if (getMaiAtr() != null) {
            _hashCode += getMaiAtr().hashCode();
        }
        if (getMedAtr() != null) {
            _hashCode += getMedAtr().hashCode();
        }
        if (getNumCcc() != null) {
            _hashCode += getNumCcc().hashCode();
        }
        if (getPagDtj() != null) {
            _hashCode += getPagDtj().hashCode();
        }
        if (getPagDtm() != null) {
            _hashCode += getPagDtm().hashCode();
        }
        if (getPagJmm() != null) {
            _hashCode += getPagJmm().hashCode();
        }
        if (getPagMul() != null) {
            _hashCode += getPagMul().hashCode();
        }
        if (getPagTir() != null) {
            _hashCode += getPagTir().hashCode();
        }
        if (getPerDs1() != null) {
            _hashCode += getPerDs1().hashCode();
        }
        if (getPerDs2() != null) {
            _hashCode += getPerDs2().hashCode();
        }
        if (getPerDs3() != null) {
            _hashCode += getPerDs3().hashCode();
        }
        if (getPerDs4() != null) {
            _hashCode += getPerDs4().hashCode();
        }
        if (getPerDs5() != null) {
            _hashCode += getPerDs5().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerEmb() != null) {
            _hashCode += getPerEmb().hashCode();
        }
        if (getPerEnc() != null) {
            _hashCode += getPerEnc().hashCode();
        }
        if (getPerFre() != null) {
            _hashCode += getPerFre().hashCode();
        }
        if (getPerFun() != null) {
            _hashCode += getPerFun().hashCode();
        }
        if (getPerIne() != null) {
            _hashCode += getPerIne().hashCode();
        }
        if (getPerIns() != null) {
            _hashCode += getPerIns().hashCode();
        }
        if (getPerIrf() != null) {
            _hashCode += getPerIrf().hashCode();
        }
        if (getPerIss() != null) {
            _hashCode += getPerIss().hashCode();
        }
        if (getPerOut() != null) {
            _hashCode += getPerOut().hashCode();
        }
        if (getPerSeg() != null) {
            _hashCode += getPerSeg().hashCode();
        }
        if (getPgtFre() != null) {
            _hashCode += getPgtFre().hashCode();
        }
        if (getPgtMon() != null) {
            _hashCode += getPgtMon().hashCode();
        }
        if (getPrzEnt() != null) {
            _hashCode += getPrzEnt().hashCode();
        }
        if (getQtdDcv() != null) {
            _hashCode += getQtdDcv().hashCode();
        }
        if (getQtdPgt() != null) {
            _hashCode += getQtdPgt().hashCode();
        }
        if (getRvlCfr() != null) {
            _hashCode += getRvlCfr().hashCode();
        }
        if (getRvlDar() != null) {
            _hashCode += getRvlDar().hashCode();
        }
        if (getRvlEmb() != null) {
            _hashCode += getRvlEmb().hashCode();
        }
        if (getRvlEnc() != null) {
            _hashCode += getRvlEnc().hashCode();
        }
        if (getRvlFei() != null) {
            _hashCode += getRvlFei().hashCode();
        }
        if (getRvlFre() != null) {
            _hashCode += getRvlFre().hashCode();
        }
        if (getRvlOui() != null) {
            _hashCode += getRvlOui().hashCode();
        }
        if (getRvlOut() != null) {
            _hashCode += getRvlOut().hashCode();
        }
        if (getRvlSeg() != null) {
            _hashCode += getRvlSeg().hashCode();
        }
        if (getRvlSei() != null) {
            _hashCode += getRvlSei().hashCode();
        }
        if (getSalCre() != null) {
            _hashCode += getSalCre().hashCode();
        }
        if (getSalDup() != null) {
            _hashCode += getSalDup().hashCode();
        }
        if (getSalOut() != null) {
            _hashCode += getSalOut().hashCode();
        }
        if (getSeqOrm() != null) {
            _hashCode += getSeqOrm().hashCode();
        }
        if (getSerCur() != null) {
            _hashCode += getSerCur().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getUltDup() != null) {
            _hashCode += getUltDup().hashCode();
        }
        if (getVlrAtr() != null) {
            _hashCode += getVlrAtr().hashCode();
        }
        if (getVlrMcp() != null) {
            _hashCode += getVlrMcp().hashCode();
        }
        if (getVlrUcp() != null) {
            _hashCode += getVlrUcp().hashCode();
        }
        if (getVlrUpe() != null) {
            _hashCode += getVlrUpe().hashCode();
        }
        if (getVlrUpg() != null) {
            _hashCode += getVlrUpg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportar2OutGridFornecedoresDefHis.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresDefHis"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFav");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFav"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cprCat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cprCat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cprCpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cprCpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cprCql");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cprCql"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criEdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criEdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criRat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "criRat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaAux");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaAux"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datUpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datUpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forMon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forMon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indInd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maiAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maiAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagTir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagTir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIne");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pgtFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pgtFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pgtMon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pgtMon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("przEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "przEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlCfr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlCfr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlDar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlDar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlFei");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlFei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlOui");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlOui"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvlSei");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvlSei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salDup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("salOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "salOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serCur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serCur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ultDup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ultDup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrUpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrUpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
