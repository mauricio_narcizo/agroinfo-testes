/**
 * FornecedoresExportarOutGridFornecedores.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportarOutGridFornecedores  implements java.io.Serializable {
    private java.lang.String apeFor;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE[] CNAE;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario[] campoUsuario;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas[] caracteristicas;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep[] cep;

    private java.lang.String cgcCpf;

    private java.lang.String cliFor;

    private java.lang.Integer codCli;

    private java.lang.Integer codFor;

    private java.lang.Integer codGre;

    private java.lang.Integer codMot;

    private java.lang.String codRam;

    private java.lang.String codRoe;

    private java.lang.Integer codRtr;

    private java.lang.String codSro;

    private java.lang.String codSuf;

    private java.lang.String codTri;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan[] conBan;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos[] contatos;

    private java.lang.Integer cxaPst;

    private java.lang.String datAtu;

    private java.lang.String datCad;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis[] defHis;

    private java.lang.String emaNfe;

    private java.lang.String faxFor;

    private java.lang.String faxVen;

    private java.lang.String fonFo2;

    private java.lang.String fonFo3;

    private java.lang.String fonFor;

    private java.lang.String fonVen;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag[] forPag;

    private java.lang.Integer forRep;

    private java.lang.Integer forTra;

    private java.lang.String forWms;

    private java.lang.String gerDir;

    private java.lang.String horAtu;

    private java.lang.String horCad;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS[] INSS;

    private java.lang.String indCoo;

    private java.lang.String indFor;

    private java.lang.Integer insAnp;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.String limRet;

    private java.lang.String marFor;

    private java.lang.String nomFor;

    private java.lang.String nomVen;

    private java.lang.Double notAfo;

    private java.lang.Double notFor;

    private java.lang.Double notSis;

    private java.lang.String numRge;

    private java.lang.String obsMot;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes[] observacoes;

    private br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer[] oriMer;

    private java.lang.Double perCod;

    private java.lang.Double perIcm;

    private java.lang.Double perPid;

    private java.lang.Double perRin;

    private java.lang.Double perRir;

    private java.lang.Integer qtdDep;

    private java.lang.String recCof;

    private java.lang.String recIcm;

    private java.lang.String recIpi;

    private java.lang.String recPis;

    private java.lang.Integer regEst;

    private java.lang.String retCof;

    private java.lang.String retCsl;

    private java.lang.String retIrf;

    private java.lang.String retOur;

    private java.lang.String retPis;

    private java.lang.String retPro;

    private java.lang.Integer rmlVen;

    private java.lang.Integer seqInt;

    private java.lang.Integer seqRoe;

    private java.lang.String sitFor;

    private java.lang.String temOrm;

    private java.lang.String tipFav;

    private java.lang.String tipFor;

    private java.lang.String tipMer;

    private java.lang.String tipPgt;

    private java.lang.String triIcm;

    private java.lang.String triIpi;

    private java.lang.String triIss;

    public FornecedoresExportarOutGridFornecedores() {
    }

    public FornecedoresExportarOutGridFornecedores(
           java.lang.String apeFor,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE[] CNAE,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario[] campoUsuario,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas[] caracteristicas,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep[] cep,
           java.lang.String cgcCpf,
           java.lang.String cliFor,
           java.lang.Integer codCli,
           java.lang.Integer codFor,
           java.lang.Integer codGre,
           java.lang.Integer codMot,
           java.lang.String codRam,
           java.lang.String codRoe,
           java.lang.Integer codRtr,
           java.lang.String codSro,
           java.lang.String codSuf,
           java.lang.String codTri,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan[] conBan,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos[] contatos,
           java.lang.Integer cxaPst,
           java.lang.String datAtu,
           java.lang.String datCad,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis[] defHis,
           java.lang.String emaNfe,
           java.lang.String faxFor,
           java.lang.String faxVen,
           java.lang.String fonFo2,
           java.lang.String fonFo3,
           java.lang.String fonFor,
           java.lang.String fonVen,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag[] forPag,
           java.lang.Integer forRep,
           java.lang.Integer forTra,
           java.lang.String forWms,
           java.lang.String gerDir,
           java.lang.String horAtu,
           java.lang.String horCad,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS[] INSS,
           java.lang.String indCoo,
           java.lang.String indFor,
           java.lang.Integer insAnp,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.String limRet,
           java.lang.String marFor,
           java.lang.String nomFor,
           java.lang.String nomVen,
           java.lang.Double notAfo,
           java.lang.Double notFor,
           java.lang.Double notSis,
           java.lang.String numRge,
           java.lang.String obsMot,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes[] observacoes,
           br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer[] oriMer,
           java.lang.Double perCod,
           java.lang.Double perIcm,
           java.lang.Double perPid,
           java.lang.Double perRin,
           java.lang.Double perRir,
           java.lang.Integer qtdDep,
           java.lang.String recCof,
           java.lang.String recIcm,
           java.lang.String recIpi,
           java.lang.String recPis,
           java.lang.Integer regEst,
           java.lang.String retCof,
           java.lang.String retCsl,
           java.lang.String retIrf,
           java.lang.String retOur,
           java.lang.String retPis,
           java.lang.String retPro,
           java.lang.Integer rmlVen,
           java.lang.Integer seqInt,
           java.lang.Integer seqRoe,
           java.lang.String sitFor,
           java.lang.String temOrm,
           java.lang.String tipFav,
           java.lang.String tipFor,
           java.lang.String tipMer,
           java.lang.String tipPgt,
           java.lang.String triIcm,
           java.lang.String triIpi,
           java.lang.String triIss) {
           this.apeFor = apeFor;
           this.CNAE = CNAE;
           this.campoUsuario = campoUsuario;
           this.caracteristicas = caracteristicas;
           this.cep = cep;
           this.cgcCpf = cgcCpf;
           this.cliFor = cliFor;
           this.codCli = codCli;
           this.codFor = codFor;
           this.codGre = codGre;
           this.codMot = codMot;
           this.codRam = codRam;
           this.codRoe = codRoe;
           this.codRtr = codRtr;
           this.codSro = codSro;
           this.codSuf = codSuf;
           this.codTri = codTri;
           this.conBan = conBan;
           this.contatos = contatos;
           this.cxaPst = cxaPst;
           this.datAtu = datAtu;
           this.datCad = datCad;
           this.defHis = defHis;
           this.emaNfe = emaNfe;
           this.faxFor = faxFor;
           this.faxVen = faxVen;
           this.fonFo2 = fonFo2;
           this.fonFo3 = fonFo3;
           this.fonFor = fonFor;
           this.fonVen = fonVen;
           this.forPag = forPag;
           this.forRep = forRep;
           this.forTra = forTra;
           this.forWms = forWms;
           this.gerDir = gerDir;
           this.horAtu = horAtu;
           this.horCad = horCad;
           this.INSS = INSS;
           this.indCoo = indCoo;
           this.indFor = indFor;
           this.insAnp = insAnp;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.limRet = limRet;
           this.marFor = marFor;
           this.nomFor = nomFor;
           this.nomVen = nomVen;
           this.notAfo = notAfo;
           this.notFor = notFor;
           this.notSis = notSis;
           this.numRge = numRge;
           this.obsMot = obsMot;
           this.observacoes = observacoes;
           this.oriMer = oriMer;
           this.perCod = perCod;
           this.perIcm = perIcm;
           this.perPid = perPid;
           this.perRin = perRin;
           this.perRir = perRir;
           this.qtdDep = qtdDep;
           this.recCof = recCof;
           this.recIcm = recIcm;
           this.recIpi = recIpi;
           this.recPis = recPis;
           this.regEst = regEst;
           this.retCof = retCof;
           this.retCsl = retCsl;
           this.retIrf = retIrf;
           this.retOur = retOur;
           this.retPis = retPis;
           this.retPro = retPro;
           this.rmlVen = rmlVen;
           this.seqInt = seqInt;
           this.seqRoe = seqRoe;
           this.sitFor = sitFor;
           this.temOrm = temOrm;
           this.tipFav = tipFav;
           this.tipFor = tipFor;
           this.tipMer = tipMer;
           this.tipPgt = tipPgt;
           this.triIcm = triIcm;
           this.triIpi = triIpi;
           this.triIss = triIss;
    }


    /**
     * Gets the apeFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return apeFor
     */
    public java.lang.String getApeFor() {
        return apeFor;
    }


    /**
     * Sets the apeFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param apeFor
     */
    public void setApeFor(java.lang.String apeFor) {
        this.apeFor = apeFor;
    }


    /**
     * Gets the CNAE value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return CNAE
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE[] getCNAE() {
        return CNAE;
    }


    /**
     * Sets the CNAE value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param CNAE
     */
    public void setCNAE(br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE[] CNAE) {
        this.CNAE = CNAE;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE getCNAE(int i) {
        return this.CNAE[i];
    }

    public void setCNAE(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE _value) {
        this.CNAE[i] = _value;
    }


    /**
     * Gets the campoUsuario value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return campoUsuario
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario[] getCampoUsuario() {
        return campoUsuario;
    }


    /**
     * Sets the campoUsuario value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param campoUsuario
     */
    public void setCampoUsuario(br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario[] campoUsuario) {
        this.campoUsuario = campoUsuario;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario getCampoUsuario(int i) {
        return this.campoUsuario[i];
    }

    public void setCampoUsuario(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario _value) {
        this.campoUsuario[i] = _value;
    }


    /**
     * Gets the caracteristicas value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return caracteristicas
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas[] getCaracteristicas() {
        return caracteristicas;
    }


    /**
     * Sets the caracteristicas value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param caracteristicas
     */
    public void setCaracteristicas(br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas[] caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas getCaracteristicas(int i) {
        return this.caracteristicas[i];
    }

    public void setCaracteristicas(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas _value) {
        this.caracteristicas[i] = _value;
    }


    /**
     * Gets the cep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return cep
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep[] getCep() {
        return cep;
    }


    /**
     * Sets the cep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param cep
     */
    public void setCep(br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep[] cep) {
        this.cep = cep;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep getCep(int i) {
        return this.cep[i];
    }

    public void setCep(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep _value) {
        this.cep[i] = _value;
    }


    /**
     * Gets the cgcCpf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the cliFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return cliFor
     */
    public java.lang.String getCliFor() {
        return cliFor;
    }


    /**
     * Sets the cliFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param cliFor
     */
    public void setCliFor(java.lang.String cliFor) {
        this.cliFor = cliFor;
    }


    /**
     * Gets the codCli value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codGre value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codGre
     */
    public java.lang.Integer getCodGre() {
        return codGre;
    }


    /**
     * Sets the codGre value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codGre
     */
    public void setCodGre(java.lang.Integer codGre) {
        this.codGre = codGre;
    }


    /**
     * Gets the codMot value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the codRam value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codRam
     */
    public java.lang.String getCodRam() {
        return codRam;
    }


    /**
     * Sets the codRam value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codRam
     */
    public void setCodRam(java.lang.String codRam) {
        this.codRam = codRam;
    }


    /**
     * Gets the codRoe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codRoe
     */
    public java.lang.String getCodRoe() {
        return codRoe;
    }


    /**
     * Sets the codRoe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codRoe
     */
    public void setCodRoe(java.lang.String codRoe) {
        this.codRoe = codRoe;
    }


    /**
     * Gets the codRtr value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codRtr
     */
    public java.lang.Integer getCodRtr() {
        return codRtr;
    }


    /**
     * Sets the codRtr value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codRtr
     */
    public void setCodRtr(java.lang.Integer codRtr) {
        this.codRtr = codRtr;
    }


    /**
     * Gets the codSro value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codSro
     */
    public java.lang.String getCodSro() {
        return codSro;
    }


    /**
     * Sets the codSro value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codSro
     */
    public void setCodSro(java.lang.String codSro) {
        this.codSro = codSro;
    }


    /**
     * Gets the codSuf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the codTri value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return codTri
     */
    public java.lang.String getCodTri() {
        return codTri;
    }


    /**
     * Sets the codTri value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param codTri
     */
    public void setCodTri(java.lang.String codTri) {
        this.codTri = codTri;
    }


    /**
     * Gets the conBan value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return conBan
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan[] getConBan() {
        return conBan;
    }


    /**
     * Sets the conBan value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param conBan
     */
    public void setConBan(br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan[] conBan) {
        this.conBan = conBan;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan getConBan(int i) {
        return this.conBan[i];
    }

    public void setConBan(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan _value) {
        this.conBan[i] = _value;
    }


    /**
     * Gets the contatos value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return contatos
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos[] getContatos() {
        return contatos;
    }


    /**
     * Sets the contatos value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param contatos
     */
    public void setContatos(br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos[] contatos) {
        this.contatos = contatos;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos getContatos(int i) {
        return this.contatos[i];
    }

    public void setContatos(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos _value) {
        this.contatos[i] = _value;
    }


    /**
     * Gets the cxaPst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return cxaPst
     */
    public java.lang.Integer getCxaPst() {
        return cxaPst;
    }


    /**
     * Sets the cxaPst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param cxaPst
     */
    public void setCxaPst(java.lang.Integer cxaPst) {
        this.cxaPst = cxaPst;
    }


    /**
     * Gets the datAtu value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return datAtu
     */
    public java.lang.String getDatAtu() {
        return datAtu;
    }


    /**
     * Sets the datAtu value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param datAtu
     */
    public void setDatAtu(java.lang.String datAtu) {
        this.datAtu = datAtu;
    }


    /**
     * Gets the datCad value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return datCad
     */
    public java.lang.String getDatCad() {
        return datCad;
    }


    /**
     * Sets the datCad value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param datCad
     */
    public void setDatCad(java.lang.String datCad) {
        this.datCad = datCad;
    }


    /**
     * Gets the defHis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return defHis
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis[] getDefHis() {
        return defHis;
    }


    /**
     * Sets the defHis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param defHis
     */
    public void setDefHis(br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis[] defHis) {
        this.defHis = defHis;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis getDefHis(int i) {
        return this.defHis[i];
    }

    public void setDefHis(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis _value) {
        this.defHis[i] = _value;
    }


    /**
     * Gets the emaNfe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the faxFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return faxFor
     */
    public java.lang.String getFaxFor() {
        return faxFor;
    }


    /**
     * Sets the faxFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param faxFor
     */
    public void setFaxFor(java.lang.String faxFor) {
        this.faxFor = faxFor;
    }


    /**
     * Gets the faxVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return faxVen
     */
    public java.lang.String getFaxVen() {
        return faxVen;
    }


    /**
     * Sets the faxVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param faxVen
     */
    public void setFaxVen(java.lang.String faxVen) {
        this.faxVen = faxVen;
    }


    /**
     * Gets the fonFo2 value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return fonFo2
     */
    public java.lang.String getFonFo2() {
        return fonFo2;
    }


    /**
     * Sets the fonFo2 value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param fonFo2
     */
    public void setFonFo2(java.lang.String fonFo2) {
        this.fonFo2 = fonFo2;
    }


    /**
     * Gets the fonFo3 value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return fonFo3
     */
    public java.lang.String getFonFo3() {
        return fonFo3;
    }


    /**
     * Sets the fonFo3 value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param fonFo3
     */
    public void setFonFo3(java.lang.String fonFo3) {
        this.fonFo3 = fonFo3;
    }


    /**
     * Gets the fonFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return fonFor
     */
    public java.lang.String getFonFor() {
        return fonFor;
    }


    /**
     * Sets the fonFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param fonFor
     */
    public void setFonFor(java.lang.String fonFor) {
        this.fonFor = fonFor;
    }


    /**
     * Gets the fonVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return fonVen
     */
    public java.lang.String getFonVen() {
        return fonVen;
    }


    /**
     * Sets the fonVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param fonVen
     */
    public void setFonVen(java.lang.String fonVen) {
        this.fonVen = fonVen;
    }


    /**
     * Gets the forPag value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return forPag
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag[] getForPag() {
        return forPag;
    }


    /**
     * Sets the forPag value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param forPag
     */
    public void setForPag(br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag[] forPag) {
        this.forPag = forPag;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag getForPag(int i) {
        return this.forPag[i];
    }

    public void setForPag(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag _value) {
        this.forPag[i] = _value;
    }


    /**
     * Gets the forRep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return forRep
     */
    public java.lang.Integer getForRep() {
        return forRep;
    }


    /**
     * Sets the forRep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param forRep
     */
    public void setForRep(java.lang.Integer forRep) {
        this.forRep = forRep;
    }


    /**
     * Gets the forTra value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return forTra
     */
    public java.lang.Integer getForTra() {
        return forTra;
    }


    /**
     * Sets the forTra value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param forTra
     */
    public void setForTra(java.lang.Integer forTra) {
        this.forTra = forTra;
    }


    /**
     * Gets the forWms value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return forWms
     */
    public java.lang.String getForWms() {
        return forWms;
    }


    /**
     * Sets the forWms value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param forWms
     */
    public void setForWms(java.lang.String forWms) {
        this.forWms = forWms;
    }


    /**
     * Gets the gerDir value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return gerDir
     */
    public java.lang.String getGerDir() {
        return gerDir;
    }


    /**
     * Sets the gerDir value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param gerDir
     */
    public void setGerDir(java.lang.String gerDir) {
        this.gerDir = gerDir;
    }


    /**
     * Gets the horAtu value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return horAtu
     */
    public java.lang.String getHorAtu() {
        return horAtu;
    }


    /**
     * Sets the horAtu value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param horAtu
     */
    public void setHorAtu(java.lang.String horAtu) {
        this.horAtu = horAtu;
    }


    /**
     * Gets the horCad value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return horCad
     */
    public java.lang.String getHorCad() {
        return horCad;
    }


    /**
     * Sets the horCad value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param horCad
     */
    public void setHorCad(java.lang.String horCad) {
        this.horCad = horCad;
    }


    /**
     * Gets the INSS value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return INSS
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS[] getINSS() {
        return INSS;
    }


    /**
     * Sets the INSS value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param INSS
     */
    public void setINSS(br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS[] INSS) {
        this.INSS = INSS;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS getINSS(int i) {
        return this.INSS[i];
    }

    public void setINSS(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS _value) {
        this.INSS[i] = _value;
    }


    /**
     * Gets the indCoo value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return indCoo
     */
    public java.lang.String getIndCoo() {
        return indCoo;
    }


    /**
     * Sets the indCoo value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param indCoo
     */
    public void setIndCoo(java.lang.String indCoo) {
        this.indCoo = indCoo;
    }


    /**
     * Gets the indFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return indFor
     */
    public java.lang.String getIndFor() {
        return indFor;
    }


    /**
     * Sets the indFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param indFor
     */
    public void setIndFor(java.lang.String indFor) {
        this.indFor = indFor;
    }


    /**
     * Gets the insAnp value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return insAnp
     */
    public java.lang.Integer getInsAnp() {
        return insAnp;
    }


    /**
     * Sets the insAnp value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param insAnp
     */
    public void setInsAnp(java.lang.Integer insAnp) {
        this.insAnp = insAnp;
    }


    /**
     * Gets the insEst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the limRet value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return limRet
     */
    public java.lang.String getLimRet() {
        return limRet;
    }


    /**
     * Sets the limRet value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param limRet
     */
    public void setLimRet(java.lang.String limRet) {
        this.limRet = limRet;
    }


    /**
     * Gets the marFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return marFor
     */
    public java.lang.String getMarFor() {
        return marFor;
    }


    /**
     * Sets the marFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param marFor
     */
    public void setMarFor(java.lang.String marFor) {
        this.marFor = marFor;
    }


    /**
     * Gets the nomFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return nomFor
     */
    public java.lang.String getNomFor() {
        return nomFor;
    }


    /**
     * Sets the nomFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param nomFor
     */
    public void setNomFor(java.lang.String nomFor) {
        this.nomFor = nomFor;
    }


    /**
     * Gets the nomVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return nomVen
     */
    public java.lang.String getNomVen() {
        return nomVen;
    }


    /**
     * Sets the nomVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param nomVen
     */
    public void setNomVen(java.lang.String nomVen) {
        this.nomVen = nomVen;
    }


    /**
     * Gets the notAfo value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return notAfo
     */
    public java.lang.Double getNotAfo() {
        return notAfo;
    }


    /**
     * Sets the notAfo value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param notAfo
     */
    public void setNotAfo(java.lang.Double notAfo) {
        this.notAfo = notAfo;
    }


    /**
     * Gets the notFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return notFor
     */
    public java.lang.Double getNotFor() {
        return notFor;
    }


    /**
     * Sets the notFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param notFor
     */
    public void setNotFor(java.lang.Double notFor) {
        this.notFor = notFor;
    }


    /**
     * Gets the notSis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return notSis
     */
    public java.lang.Double getNotSis() {
        return notSis;
    }


    /**
     * Sets the notSis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param notSis
     */
    public void setNotSis(java.lang.Double notSis) {
        this.notSis = notSis;
    }


    /**
     * Gets the numRge value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return numRge
     */
    public java.lang.String getNumRge() {
        return numRge;
    }


    /**
     * Sets the numRge value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param numRge
     */
    public void setNumRge(java.lang.String numRge) {
        this.numRge = numRge;
    }


    /**
     * Gets the obsMot value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return obsMot
     */
    public java.lang.String getObsMot() {
        return obsMot;
    }


    /**
     * Sets the obsMot value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param obsMot
     */
    public void setObsMot(java.lang.String obsMot) {
        this.obsMot = obsMot;
    }


    /**
     * Gets the observacoes value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return observacoes
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes[] getObservacoes() {
        return observacoes;
    }


    /**
     * Sets the observacoes value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param observacoes
     */
    public void setObservacoes(br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes[] observacoes) {
        this.observacoes = observacoes;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes getObservacoes(int i) {
        return this.observacoes[i];
    }

    public void setObservacoes(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes _value) {
        this.observacoes[i] = _value;
    }


    /**
     * Gets the oriMer value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return oriMer
     */
    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer[] getOriMer() {
        return oriMer;
    }


    /**
     * Sets the oriMer value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param oriMer
     */
    public void setOriMer(br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer[] oriMer) {
        this.oriMer = oriMer;
    }

    public br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer getOriMer(int i) {
        return this.oriMer[i];
    }

    public void setOriMer(int i, br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer _value) {
        this.oriMer[i] = _value;
    }


    /**
     * Gets the perCod value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return perCod
     */
    public java.lang.Double getPerCod() {
        return perCod;
    }


    /**
     * Sets the perCod value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param perCod
     */
    public void setPerCod(java.lang.Double perCod) {
        this.perCod = perCod;
    }


    /**
     * Gets the perIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return perIcm
     */
    public java.lang.Double getPerIcm() {
        return perIcm;
    }


    /**
     * Sets the perIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param perIcm
     */
    public void setPerIcm(java.lang.Double perIcm) {
        this.perIcm = perIcm;
    }


    /**
     * Gets the perPid value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return perPid
     */
    public java.lang.Double getPerPid() {
        return perPid;
    }


    /**
     * Sets the perPid value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param perPid
     */
    public void setPerPid(java.lang.Double perPid) {
        this.perPid = perPid;
    }


    /**
     * Gets the perRin value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return perRin
     */
    public java.lang.Double getPerRin() {
        return perRin;
    }


    /**
     * Sets the perRin value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param perRin
     */
    public void setPerRin(java.lang.Double perRin) {
        this.perRin = perRin;
    }


    /**
     * Gets the perRir value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return perRir
     */
    public java.lang.Double getPerRir() {
        return perRir;
    }


    /**
     * Sets the perRir value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param perRir
     */
    public void setPerRir(java.lang.Double perRir) {
        this.perRir = perRir;
    }


    /**
     * Gets the qtdDep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return qtdDep
     */
    public java.lang.Integer getQtdDep() {
        return qtdDep;
    }


    /**
     * Sets the qtdDep value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param qtdDep
     */
    public void setQtdDep(java.lang.Integer qtdDep) {
        this.qtdDep = qtdDep;
    }


    /**
     * Gets the recCof value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return recCof
     */
    public java.lang.String getRecCof() {
        return recCof;
    }


    /**
     * Sets the recCof value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param recCof
     */
    public void setRecCof(java.lang.String recCof) {
        this.recCof = recCof;
    }


    /**
     * Gets the recIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return recIcm
     */
    public java.lang.String getRecIcm() {
        return recIcm;
    }


    /**
     * Sets the recIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param recIcm
     */
    public void setRecIcm(java.lang.String recIcm) {
        this.recIcm = recIcm;
    }


    /**
     * Gets the recIpi value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return recIpi
     */
    public java.lang.String getRecIpi() {
        return recIpi;
    }


    /**
     * Sets the recIpi value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param recIpi
     */
    public void setRecIpi(java.lang.String recIpi) {
        this.recIpi = recIpi;
    }


    /**
     * Gets the recPis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return recPis
     */
    public java.lang.String getRecPis() {
        return recPis;
    }


    /**
     * Sets the recPis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param recPis
     */
    public void setRecPis(java.lang.String recPis) {
        this.recPis = recPis;
    }


    /**
     * Gets the regEst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return regEst
     */
    public java.lang.Integer getRegEst() {
        return regEst;
    }


    /**
     * Sets the regEst value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param regEst
     */
    public void setRegEst(java.lang.Integer regEst) {
        this.regEst = regEst;
    }


    /**
     * Gets the retCof value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retCof
     */
    public java.lang.String getRetCof() {
        return retCof;
    }


    /**
     * Sets the retCof value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retCof
     */
    public void setRetCof(java.lang.String retCof) {
        this.retCof = retCof;
    }


    /**
     * Gets the retCsl value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retCsl
     */
    public java.lang.String getRetCsl() {
        return retCsl;
    }


    /**
     * Sets the retCsl value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retCsl
     */
    public void setRetCsl(java.lang.String retCsl) {
        this.retCsl = retCsl;
    }


    /**
     * Gets the retIrf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retIrf
     */
    public java.lang.String getRetIrf() {
        return retIrf;
    }


    /**
     * Sets the retIrf value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retIrf
     */
    public void setRetIrf(java.lang.String retIrf) {
        this.retIrf = retIrf;
    }


    /**
     * Gets the retOur value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retOur
     */
    public java.lang.String getRetOur() {
        return retOur;
    }


    /**
     * Sets the retOur value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retOur
     */
    public void setRetOur(java.lang.String retOur) {
        this.retOur = retOur;
    }


    /**
     * Gets the retPis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retPis
     */
    public java.lang.String getRetPis() {
        return retPis;
    }


    /**
     * Sets the retPis value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retPis
     */
    public void setRetPis(java.lang.String retPis) {
        this.retPis = retPis;
    }


    /**
     * Gets the retPro value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return retPro
     */
    public java.lang.String getRetPro() {
        return retPro;
    }


    /**
     * Sets the retPro value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param retPro
     */
    public void setRetPro(java.lang.String retPro) {
        this.retPro = retPro;
    }


    /**
     * Gets the rmlVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return rmlVen
     */
    public java.lang.Integer getRmlVen() {
        return rmlVen;
    }


    /**
     * Sets the rmlVen value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param rmlVen
     */
    public void setRmlVen(java.lang.Integer rmlVen) {
        this.rmlVen = rmlVen;
    }


    /**
     * Gets the seqInt value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return seqInt
     */
    public java.lang.Integer getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.Integer seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the seqRoe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return seqRoe
     */
    public java.lang.Integer getSeqRoe() {
        return seqRoe;
    }


    /**
     * Sets the seqRoe value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param seqRoe
     */
    public void setSeqRoe(java.lang.Integer seqRoe) {
        this.seqRoe = seqRoe;
    }


    /**
     * Gets the sitFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return sitFor
     */
    public java.lang.String getSitFor() {
        return sitFor;
    }


    /**
     * Sets the sitFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param sitFor
     */
    public void setSitFor(java.lang.String sitFor) {
        this.sitFor = sitFor;
    }


    /**
     * Gets the temOrm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return temOrm
     */
    public java.lang.String getTemOrm() {
        return temOrm;
    }


    /**
     * Sets the temOrm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param temOrm
     */
    public void setTemOrm(java.lang.String temOrm) {
        this.temOrm = temOrm;
    }


    /**
     * Gets the tipFav value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return tipFav
     */
    public java.lang.String getTipFav() {
        return tipFav;
    }


    /**
     * Sets the tipFav value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param tipFav
     */
    public void setTipFav(java.lang.String tipFav) {
        this.tipFav = tipFav;
    }


    /**
     * Gets the tipFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return tipFor
     */
    public java.lang.String getTipFor() {
        return tipFor;
    }


    /**
     * Sets the tipFor value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param tipFor
     */
    public void setTipFor(java.lang.String tipFor) {
        this.tipFor = tipFor;
    }


    /**
     * Gets the tipMer value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return tipMer
     */
    public java.lang.String getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param tipMer
     */
    public void setTipMer(java.lang.String tipMer) {
        this.tipMer = tipMer;
    }


    /**
     * Gets the tipPgt value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return tipPgt
     */
    public java.lang.String getTipPgt() {
        return tipPgt;
    }


    /**
     * Sets the tipPgt value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param tipPgt
     */
    public void setTipPgt(java.lang.String tipPgt) {
        this.tipPgt = tipPgt;
    }


    /**
     * Gets the triIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return triIcm
     */
    public java.lang.String getTriIcm() {
        return triIcm;
    }


    /**
     * Sets the triIcm value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param triIcm
     */
    public void setTriIcm(java.lang.String triIcm) {
        this.triIcm = triIcm;
    }


    /**
     * Gets the triIpi value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return triIpi
     */
    public java.lang.String getTriIpi() {
        return triIpi;
    }


    /**
     * Sets the triIpi value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param triIpi
     */
    public void setTriIpi(java.lang.String triIpi) {
        this.triIpi = triIpi;
    }


    /**
     * Gets the triIss value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @return triIss
     */
    public java.lang.String getTriIss() {
        return triIss;
    }


    /**
     * Sets the triIss value for this FornecedoresExportarOutGridFornecedores.
     * 
     * @param triIss
     */
    public void setTriIss(java.lang.String triIss) {
        this.triIss = triIss;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportarOutGridFornecedores)) return false;
        FornecedoresExportarOutGridFornecedores other = (FornecedoresExportarOutGridFornecedores) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apeFor==null && other.getApeFor()==null) || 
             (this.apeFor!=null &&
              this.apeFor.equals(other.getApeFor()))) &&
            ((this.CNAE==null && other.getCNAE()==null) || 
             (this.CNAE!=null &&
              java.util.Arrays.equals(this.CNAE, other.getCNAE()))) &&
            ((this.campoUsuario==null && other.getCampoUsuario()==null) || 
             (this.campoUsuario!=null &&
              java.util.Arrays.equals(this.campoUsuario, other.getCampoUsuario()))) &&
            ((this.caracteristicas==null && other.getCaracteristicas()==null) || 
             (this.caracteristicas!=null &&
              java.util.Arrays.equals(this.caracteristicas, other.getCaracteristicas()))) &&
            ((this.cep==null && other.getCep()==null) || 
             (this.cep!=null &&
              java.util.Arrays.equals(this.cep, other.getCep()))) &&
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.cliFor==null && other.getCliFor()==null) || 
             (this.cliFor!=null &&
              this.cliFor.equals(other.getCliFor()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codGre==null && other.getCodGre()==null) || 
             (this.codGre!=null &&
              this.codGre.equals(other.getCodGre()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.codRam==null && other.getCodRam()==null) || 
             (this.codRam!=null &&
              this.codRam.equals(other.getCodRam()))) &&
            ((this.codRoe==null && other.getCodRoe()==null) || 
             (this.codRoe!=null &&
              this.codRoe.equals(other.getCodRoe()))) &&
            ((this.codRtr==null && other.getCodRtr()==null) || 
             (this.codRtr!=null &&
              this.codRtr.equals(other.getCodRtr()))) &&
            ((this.codSro==null && other.getCodSro()==null) || 
             (this.codSro!=null &&
              this.codSro.equals(other.getCodSro()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.codTri==null && other.getCodTri()==null) || 
             (this.codTri!=null &&
              this.codTri.equals(other.getCodTri()))) &&
            ((this.conBan==null && other.getConBan()==null) || 
             (this.conBan!=null &&
              java.util.Arrays.equals(this.conBan, other.getConBan()))) &&
            ((this.contatos==null && other.getContatos()==null) || 
             (this.contatos!=null &&
              java.util.Arrays.equals(this.contatos, other.getContatos()))) &&
            ((this.cxaPst==null && other.getCxaPst()==null) || 
             (this.cxaPst!=null &&
              this.cxaPst.equals(other.getCxaPst()))) &&
            ((this.datAtu==null && other.getDatAtu()==null) || 
             (this.datAtu!=null &&
              this.datAtu.equals(other.getDatAtu()))) &&
            ((this.datCad==null && other.getDatCad()==null) || 
             (this.datCad!=null &&
              this.datCad.equals(other.getDatCad()))) &&
            ((this.defHis==null && other.getDefHis()==null) || 
             (this.defHis!=null &&
              java.util.Arrays.equals(this.defHis, other.getDefHis()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.faxFor==null && other.getFaxFor()==null) || 
             (this.faxFor!=null &&
              this.faxFor.equals(other.getFaxFor()))) &&
            ((this.faxVen==null && other.getFaxVen()==null) || 
             (this.faxVen!=null &&
              this.faxVen.equals(other.getFaxVen()))) &&
            ((this.fonFo2==null && other.getFonFo2()==null) || 
             (this.fonFo2!=null &&
              this.fonFo2.equals(other.getFonFo2()))) &&
            ((this.fonFo3==null && other.getFonFo3()==null) || 
             (this.fonFo3!=null &&
              this.fonFo3.equals(other.getFonFo3()))) &&
            ((this.fonFor==null && other.getFonFor()==null) || 
             (this.fonFor!=null &&
              this.fonFor.equals(other.getFonFor()))) &&
            ((this.fonVen==null && other.getFonVen()==null) || 
             (this.fonVen!=null &&
              this.fonVen.equals(other.getFonVen()))) &&
            ((this.forPag==null && other.getForPag()==null) || 
             (this.forPag!=null &&
              java.util.Arrays.equals(this.forPag, other.getForPag()))) &&
            ((this.forRep==null && other.getForRep()==null) || 
             (this.forRep!=null &&
              this.forRep.equals(other.getForRep()))) &&
            ((this.forTra==null && other.getForTra()==null) || 
             (this.forTra!=null &&
              this.forTra.equals(other.getForTra()))) &&
            ((this.forWms==null && other.getForWms()==null) || 
             (this.forWms!=null &&
              this.forWms.equals(other.getForWms()))) &&
            ((this.gerDir==null && other.getGerDir()==null) || 
             (this.gerDir!=null &&
              this.gerDir.equals(other.getGerDir()))) &&
            ((this.horAtu==null && other.getHorAtu()==null) || 
             (this.horAtu!=null &&
              this.horAtu.equals(other.getHorAtu()))) &&
            ((this.horCad==null && other.getHorCad()==null) || 
             (this.horCad!=null &&
              this.horCad.equals(other.getHorCad()))) &&
            ((this.INSS==null && other.getINSS()==null) || 
             (this.INSS!=null &&
              java.util.Arrays.equals(this.INSS, other.getINSS()))) &&
            ((this.indCoo==null && other.getIndCoo()==null) || 
             (this.indCoo!=null &&
              this.indCoo.equals(other.getIndCoo()))) &&
            ((this.indFor==null && other.getIndFor()==null) || 
             (this.indFor!=null &&
              this.indFor.equals(other.getIndFor()))) &&
            ((this.insAnp==null && other.getInsAnp()==null) || 
             (this.insAnp!=null &&
              this.insAnp.equals(other.getInsAnp()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.limRet==null && other.getLimRet()==null) || 
             (this.limRet!=null &&
              this.limRet.equals(other.getLimRet()))) &&
            ((this.marFor==null && other.getMarFor()==null) || 
             (this.marFor!=null &&
              this.marFor.equals(other.getMarFor()))) &&
            ((this.nomFor==null && other.getNomFor()==null) || 
             (this.nomFor!=null &&
              this.nomFor.equals(other.getNomFor()))) &&
            ((this.nomVen==null && other.getNomVen()==null) || 
             (this.nomVen!=null &&
              this.nomVen.equals(other.getNomVen()))) &&
            ((this.notAfo==null && other.getNotAfo()==null) || 
             (this.notAfo!=null &&
              this.notAfo.equals(other.getNotAfo()))) &&
            ((this.notFor==null && other.getNotFor()==null) || 
             (this.notFor!=null &&
              this.notFor.equals(other.getNotFor()))) &&
            ((this.notSis==null && other.getNotSis()==null) || 
             (this.notSis!=null &&
              this.notSis.equals(other.getNotSis()))) &&
            ((this.numRge==null && other.getNumRge()==null) || 
             (this.numRge!=null &&
              this.numRge.equals(other.getNumRge()))) &&
            ((this.obsMot==null && other.getObsMot()==null) || 
             (this.obsMot!=null &&
              this.obsMot.equals(other.getObsMot()))) &&
            ((this.observacoes==null && other.getObservacoes()==null) || 
             (this.observacoes!=null &&
              java.util.Arrays.equals(this.observacoes, other.getObservacoes()))) &&
            ((this.oriMer==null && other.getOriMer()==null) || 
             (this.oriMer!=null &&
              java.util.Arrays.equals(this.oriMer, other.getOriMer()))) &&
            ((this.perCod==null && other.getPerCod()==null) || 
             (this.perCod!=null &&
              this.perCod.equals(other.getPerCod()))) &&
            ((this.perIcm==null && other.getPerIcm()==null) || 
             (this.perIcm!=null &&
              this.perIcm.equals(other.getPerIcm()))) &&
            ((this.perPid==null && other.getPerPid()==null) || 
             (this.perPid!=null &&
              this.perPid.equals(other.getPerPid()))) &&
            ((this.perRin==null && other.getPerRin()==null) || 
             (this.perRin!=null &&
              this.perRin.equals(other.getPerRin()))) &&
            ((this.perRir==null && other.getPerRir()==null) || 
             (this.perRir!=null &&
              this.perRir.equals(other.getPerRir()))) &&
            ((this.qtdDep==null && other.getQtdDep()==null) || 
             (this.qtdDep!=null &&
              this.qtdDep.equals(other.getQtdDep()))) &&
            ((this.recCof==null && other.getRecCof()==null) || 
             (this.recCof!=null &&
              this.recCof.equals(other.getRecCof()))) &&
            ((this.recIcm==null && other.getRecIcm()==null) || 
             (this.recIcm!=null &&
              this.recIcm.equals(other.getRecIcm()))) &&
            ((this.recIpi==null && other.getRecIpi()==null) || 
             (this.recIpi!=null &&
              this.recIpi.equals(other.getRecIpi()))) &&
            ((this.recPis==null && other.getRecPis()==null) || 
             (this.recPis!=null &&
              this.recPis.equals(other.getRecPis()))) &&
            ((this.regEst==null && other.getRegEst()==null) || 
             (this.regEst!=null &&
              this.regEst.equals(other.getRegEst()))) &&
            ((this.retCof==null && other.getRetCof()==null) || 
             (this.retCof!=null &&
              this.retCof.equals(other.getRetCof()))) &&
            ((this.retCsl==null && other.getRetCsl()==null) || 
             (this.retCsl!=null &&
              this.retCsl.equals(other.getRetCsl()))) &&
            ((this.retIrf==null && other.getRetIrf()==null) || 
             (this.retIrf!=null &&
              this.retIrf.equals(other.getRetIrf()))) &&
            ((this.retOur==null && other.getRetOur()==null) || 
             (this.retOur!=null &&
              this.retOur.equals(other.getRetOur()))) &&
            ((this.retPis==null && other.getRetPis()==null) || 
             (this.retPis!=null &&
              this.retPis.equals(other.getRetPis()))) &&
            ((this.retPro==null && other.getRetPro()==null) || 
             (this.retPro!=null &&
              this.retPro.equals(other.getRetPro()))) &&
            ((this.rmlVen==null && other.getRmlVen()==null) || 
             (this.rmlVen!=null &&
              this.rmlVen.equals(other.getRmlVen()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.seqRoe==null && other.getSeqRoe()==null) || 
             (this.seqRoe!=null &&
              this.seqRoe.equals(other.getSeqRoe()))) &&
            ((this.sitFor==null && other.getSitFor()==null) || 
             (this.sitFor!=null &&
              this.sitFor.equals(other.getSitFor()))) &&
            ((this.temOrm==null && other.getTemOrm()==null) || 
             (this.temOrm!=null &&
              this.temOrm.equals(other.getTemOrm()))) &&
            ((this.tipFav==null && other.getTipFav()==null) || 
             (this.tipFav!=null &&
              this.tipFav.equals(other.getTipFav()))) &&
            ((this.tipFor==null && other.getTipFor()==null) || 
             (this.tipFor!=null &&
              this.tipFor.equals(other.getTipFor()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              this.tipMer.equals(other.getTipMer()))) &&
            ((this.tipPgt==null && other.getTipPgt()==null) || 
             (this.tipPgt!=null &&
              this.tipPgt.equals(other.getTipPgt()))) &&
            ((this.triIcm==null && other.getTriIcm()==null) || 
             (this.triIcm!=null &&
              this.triIcm.equals(other.getTriIcm()))) &&
            ((this.triIpi==null && other.getTriIpi()==null) || 
             (this.triIpi!=null &&
              this.triIpi.equals(other.getTriIpi()))) &&
            ((this.triIss==null && other.getTriIss()==null) || 
             (this.triIss!=null &&
              this.triIss.equals(other.getTriIss())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApeFor() != null) {
            _hashCode += getApeFor().hashCode();
        }
        if (getCNAE() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCNAE());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCNAE(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCampoUsuario() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCampoUsuario());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCampoUsuario(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCaracteristicas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCaracteristicas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCaracteristicas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCep() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCep());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCep(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCliFor() != null) {
            _hashCode += getCliFor().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodGre() != null) {
            _hashCode += getCodGre().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCodRam() != null) {
            _hashCode += getCodRam().hashCode();
        }
        if (getCodRoe() != null) {
            _hashCode += getCodRoe().hashCode();
        }
        if (getCodRtr() != null) {
            _hashCode += getCodRtr().hashCode();
        }
        if (getCodSro() != null) {
            _hashCode += getCodSro().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getCodTri() != null) {
            _hashCode += getCodTri().hashCode();
        }
        if (getConBan() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConBan());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConBan(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getContatos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContatos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContatos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCxaPst() != null) {
            _hashCode += getCxaPst().hashCode();
        }
        if (getDatAtu() != null) {
            _hashCode += getDatAtu().hashCode();
        }
        if (getDatCad() != null) {
            _hashCode += getDatCad().hashCode();
        }
        if (getDefHis() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDefHis());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDefHis(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getFaxFor() != null) {
            _hashCode += getFaxFor().hashCode();
        }
        if (getFaxVen() != null) {
            _hashCode += getFaxVen().hashCode();
        }
        if (getFonFo2() != null) {
            _hashCode += getFonFo2().hashCode();
        }
        if (getFonFo3() != null) {
            _hashCode += getFonFo3().hashCode();
        }
        if (getFonFor() != null) {
            _hashCode += getFonFor().hashCode();
        }
        if (getFonVen() != null) {
            _hashCode += getFonVen().hashCode();
        }
        if (getForPag() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getForPag());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getForPag(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getForRep() != null) {
            _hashCode += getForRep().hashCode();
        }
        if (getForTra() != null) {
            _hashCode += getForTra().hashCode();
        }
        if (getForWms() != null) {
            _hashCode += getForWms().hashCode();
        }
        if (getGerDir() != null) {
            _hashCode += getGerDir().hashCode();
        }
        if (getHorAtu() != null) {
            _hashCode += getHorAtu().hashCode();
        }
        if (getHorCad() != null) {
            _hashCode += getHorCad().hashCode();
        }
        if (getINSS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getINSS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getINSS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIndCoo() != null) {
            _hashCode += getIndCoo().hashCode();
        }
        if (getIndFor() != null) {
            _hashCode += getIndFor().hashCode();
        }
        if (getInsAnp() != null) {
            _hashCode += getInsAnp().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getLimRet() != null) {
            _hashCode += getLimRet().hashCode();
        }
        if (getMarFor() != null) {
            _hashCode += getMarFor().hashCode();
        }
        if (getNomFor() != null) {
            _hashCode += getNomFor().hashCode();
        }
        if (getNomVen() != null) {
            _hashCode += getNomVen().hashCode();
        }
        if (getNotAfo() != null) {
            _hashCode += getNotAfo().hashCode();
        }
        if (getNotFor() != null) {
            _hashCode += getNotFor().hashCode();
        }
        if (getNotSis() != null) {
            _hashCode += getNotSis().hashCode();
        }
        if (getNumRge() != null) {
            _hashCode += getNumRge().hashCode();
        }
        if (getObsMot() != null) {
            _hashCode += getObsMot().hashCode();
        }
        if (getObservacoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getObservacoes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getObservacoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOriMer() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOriMer());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOriMer(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPerCod() != null) {
            _hashCode += getPerCod().hashCode();
        }
        if (getPerIcm() != null) {
            _hashCode += getPerIcm().hashCode();
        }
        if (getPerPid() != null) {
            _hashCode += getPerPid().hashCode();
        }
        if (getPerRin() != null) {
            _hashCode += getPerRin().hashCode();
        }
        if (getPerRir() != null) {
            _hashCode += getPerRir().hashCode();
        }
        if (getQtdDep() != null) {
            _hashCode += getQtdDep().hashCode();
        }
        if (getRecCof() != null) {
            _hashCode += getRecCof().hashCode();
        }
        if (getRecIcm() != null) {
            _hashCode += getRecIcm().hashCode();
        }
        if (getRecIpi() != null) {
            _hashCode += getRecIpi().hashCode();
        }
        if (getRecPis() != null) {
            _hashCode += getRecPis().hashCode();
        }
        if (getRegEst() != null) {
            _hashCode += getRegEst().hashCode();
        }
        if (getRetCof() != null) {
            _hashCode += getRetCof().hashCode();
        }
        if (getRetCsl() != null) {
            _hashCode += getRetCsl().hashCode();
        }
        if (getRetIrf() != null) {
            _hashCode += getRetIrf().hashCode();
        }
        if (getRetOur() != null) {
            _hashCode += getRetOur().hashCode();
        }
        if (getRetPis() != null) {
            _hashCode += getRetPis().hashCode();
        }
        if (getRetPro() != null) {
            _hashCode += getRetPro().hashCode();
        }
        if (getRmlVen() != null) {
            _hashCode += getRmlVen().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSeqRoe() != null) {
            _hashCode += getSeqRoe().hashCode();
        }
        if (getSitFor() != null) {
            _hashCode += getSitFor().hashCode();
        }
        if (getTemOrm() != null) {
            _hashCode += getTemOrm().hashCode();
        }
        if (getTipFav() != null) {
            _hashCode += getTipFav().hashCode();
        }
        if (getTipFor() != null) {
            _hashCode += getTipFor().hashCode();
        }
        if (getTipMer() != null) {
            _hashCode += getTipMer().hashCode();
        }
        if (getTipPgt() != null) {
            _hashCode += getTipPgt().hashCode();
        }
        if (getTriIcm() != null) {
            _hashCode += getTriIcm().hashCode();
        }
        if (getTriIpi() != null) {
            _hashCode += getTriIpi().hashCode();
        }
        if (getTriIss() != null) {
            _hashCode += getTriIss().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportarOutGridFornecedores.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedores"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNAE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNAE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCNAE"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campoUsuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "campoUsuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCampoUsuario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caracteristicas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "caracteristicas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCaracteristicas"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCep"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codGre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codGre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "conBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresConBan"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contatos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "contatos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresContatos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cxaPst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cxaPst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defHis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "defHis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresDefHis"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFo2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFo2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFo3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFo3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forPag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forPag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresForPag"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forWms");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forWms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerDir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerDir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INSS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "INSS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresINSS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indCoo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indCoo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insAnp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insAnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notAfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notAfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notSis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notSis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacoes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresObservacoes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oriMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oriMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresOriMer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perPid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perPid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perRin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perRin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perRir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perRir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recIpi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recIpi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCsl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCsl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retOur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retOur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rmlVen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rmlVen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "temOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipFav");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipFav"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIpi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIpi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
