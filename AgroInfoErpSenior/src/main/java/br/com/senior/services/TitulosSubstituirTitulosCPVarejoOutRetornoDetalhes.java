/**
 * TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codTpt;

    private java.lang.String numTit;

    private java.lang.Integer tipRet;

    private java.lang.String txtRet;

    public TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes() {
    }

    public TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codTpt,
           java.lang.String numTit,
           java.lang.Integer tipRet,
           java.lang.String txtRet) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codTpt = codTpt;
           this.numTit = numTit;
           this.tipRet = tipRet;
           this.txtRet = txtRet;
    }


    /**
     * Gets the codEmp value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codTpt value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the numTit value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the tipRet value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }


    /**
     * Gets the txtRet value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @return txtRet
     */
    public java.lang.String getTxtRet() {
        return txtRet;
    }


    /**
     * Sets the txtRet value for this TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.
     * 
     * @param txtRet
     */
    public void setTxtRet(java.lang.String txtRet) {
        this.txtRet = txtRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes)) return false;
        TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes other = (TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet()))) &&
            ((this.txtRet==null && other.getTxtRet()==null) || 
             (this.txtRet!=null &&
              this.txtRet.equals(other.getTxtRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        if (getTxtRet() != null) {
            _hashCode += getTxtRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCPVarejoOutRetornoDetalhes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPVarejoOutRetornoDetalhes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txtRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "txtRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
