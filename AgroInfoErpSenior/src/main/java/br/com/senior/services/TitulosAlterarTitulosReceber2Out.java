/**
 * TitulosAlterarTitulosReceber2Out.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosAlterarTitulosReceber2Out  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno[] gridRetorno;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer tipoRetorno;

    public TitulosAlterarTitulosReceber2Out() {
    }

    public TitulosAlterarTitulosReceber2Out(
           java.lang.String erroExecucao,
           br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno[] gridRetorno,
           java.lang.String mensagemRetorno,
           java.lang.Integer tipoRetorno) {
           this.erroExecucao = erroExecucao;
           this.gridRetorno = gridRetorno;
           this.mensagemRetorno = mensagemRetorno;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the erroExecucao value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the gridRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @return gridRetorno
     */
    public br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno[] getGridRetorno() {
        return gridRetorno;
    }


    /**
     * Sets the gridRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @param gridRetorno
     */
    public void setGridRetorno(br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno[] gridRetorno) {
        this.gridRetorno = gridRetorno;
    }

    public br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno getGridRetorno(int i) {
        return this.gridRetorno[i];
    }

    public void setGridRetorno(int i, br.com.senior.services.TitulosAlterarTitulosReceber2OutGridRetorno _value) {
        this.gridRetorno[i] = _value;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the tipoRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosAlterarTitulosReceber2Out.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosAlterarTitulosReceber2Out)) return false;
        TitulosAlterarTitulosReceber2Out other = (TitulosAlterarTitulosReceber2Out) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.gridRetorno==null && other.getGridRetorno()==null) || 
             (this.gridRetorno!=null &&
              java.util.Arrays.equals(this.gridRetorno, other.getGridRetorno()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getGridRetorno() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridRetorno());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridRetorno(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosAlterarTitulosReceber2Out.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosAlterarTitulosReceber2Out"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosAlterarTitulosReceber2OutGridRetorno"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
