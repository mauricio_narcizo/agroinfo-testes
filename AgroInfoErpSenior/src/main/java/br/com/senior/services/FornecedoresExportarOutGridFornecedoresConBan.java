/**
 * FornecedoresExportarOutGridFornecedoresConBan.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportarOutGridFornecedoresConBan  implements java.io.Serializable {
    private java.lang.String ccbFor;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.Integer codFor;

    private java.lang.Integer seqBan;

    private java.lang.String sisGer;

    public FornecedoresExportarOutGridFornecedoresConBan() {
    }

    public FornecedoresExportarOutGridFornecedoresConBan(
           java.lang.String ccbFor,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.Integer codFor,
           java.lang.Integer seqBan,
           java.lang.String sisGer) {
           this.ccbFor = ccbFor;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codFor = codFor;
           this.seqBan = seqBan;
           this.sisGer = sisGer;
    }


    /**
     * Gets the ccbFor value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the codAge value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codFor value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the seqBan value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return seqBan
     */
    public java.lang.Integer getSeqBan() {
        return seqBan;
    }


    /**
     * Sets the seqBan value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param seqBan
     */
    public void setSeqBan(java.lang.Integer seqBan) {
        this.seqBan = seqBan;
    }


    /**
     * Gets the sisGer value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @return sisGer
     */
    public java.lang.String getSisGer() {
        return sisGer;
    }


    /**
     * Sets the sisGer value for this FornecedoresExportarOutGridFornecedoresConBan.
     * 
     * @param sisGer
     */
    public void setSisGer(java.lang.String sisGer) {
        this.sisGer = sisGer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportarOutGridFornecedoresConBan)) return false;
        FornecedoresExportarOutGridFornecedoresConBan other = (FornecedoresExportarOutGridFornecedoresConBan) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.seqBan==null && other.getSeqBan()==null) || 
             (this.seqBan!=null &&
              this.seqBan.equals(other.getSeqBan()))) &&
            ((this.sisGer==null && other.getSisGer()==null) || 
             (this.sisGer!=null &&
              this.sisGer.equals(other.getSisGer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getSeqBan() != null) {
            _hashCode += getSeqBan().hashCode();
        }
        if (getSisGer() != null) {
            _hashCode += getSisGer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportarOutGridFornecedoresConBan.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresConBan"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sisGer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sisGer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
