/**
 * McwfUsersChangePasswordIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class McwfUsersChangePasswordIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String pmNewPassword;

    private java.lang.String pmNewPasswordConfirmation;

    private java.lang.String pmPassword;

    private java.lang.String pmUser;

    public McwfUsersChangePasswordIn() {
    }

    public McwfUsersChangePasswordIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String pmNewPassword,
           java.lang.String pmNewPasswordConfirmation,
           java.lang.String pmPassword,
           java.lang.String pmUser) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.pmNewPassword = pmNewPassword;
           this.pmNewPasswordConfirmation = pmNewPasswordConfirmation;
           this.pmPassword = pmPassword;
           this.pmUser = pmUser;
    }


    /**
     * Gets the flowInstanceID value for this McwfUsersChangePasswordIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this McwfUsersChangePasswordIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this McwfUsersChangePasswordIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this McwfUsersChangePasswordIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the pmNewPassword value for this McwfUsersChangePasswordIn.
     * 
     * @return pmNewPassword
     */
    public java.lang.String getPmNewPassword() {
        return pmNewPassword;
    }


    /**
     * Sets the pmNewPassword value for this McwfUsersChangePasswordIn.
     * 
     * @param pmNewPassword
     */
    public void setPmNewPassword(java.lang.String pmNewPassword) {
        this.pmNewPassword = pmNewPassword;
    }


    /**
     * Gets the pmNewPasswordConfirmation value for this McwfUsersChangePasswordIn.
     * 
     * @return pmNewPasswordConfirmation
     */
    public java.lang.String getPmNewPasswordConfirmation() {
        return pmNewPasswordConfirmation;
    }


    /**
     * Sets the pmNewPasswordConfirmation value for this McwfUsersChangePasswordIn.
     * 
     * @param pmNewPasswordConfirmation
     */
    public void setPmNewPasswordConfirmation(java.lang.String pmNewPasswordConfirmation) {
        this.pmNewPasswordConfirmation = pmNewPasswordConfirmation;
    }


    /**
     * Gets the pmPassword value for this McwfUsersChangePasswordIn.
     * 
     * @return pmPassword
     */
    public java.lang.String getPmPassword() {
        return pmPassword;
    }


    /**
     * Sets the pmPassword value for this McwfUsersChangePasswordIn.
     * 
     * @param pmPassword
     */
    public void setPmPassword(java.lang.String pmPassword) {
        this.pmPassword = pmPassword;
    }


    /**
     * Gets the pmUser value for this McwfUsersChangePasswordIn.
     * 
     * @return pmUser
     */
    public java.lang.String getPmUser() {
        return pmUser;
    }


    /**
     * Sets the pmUser value for this McwfUsersChangePasswordIn.
     * 
     * @param pmUser
     */
    public void setPmUser(java.lang.String pmUser) {
        this.pmUser = pmUser;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof McwfUsersChangePasswordIn)) return false;
        McwfUsersChangePasswordIn other = (McwfUsersChangePasswordIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.pmNewPassword==null && other.getPmNewPassword()==null) || 
             (this.pmNewPassword!=null &&
              this.pmNewPassword.equals(other.getPmNewPassword()))) &&
            ((this.pmNewPasswordConfirmation==null && other.getPmNewPasswordConfirmation()==null) || 
             (this.pmNewPasswordConfirmation!=null &&
              this.pmNewPasswordConfirmation.equals(other.getPmNewPasswordConfirmation()))) &&
            ((this.pmPassword==null && other.getPmPassword()==null) || 
             (this.pmPassword!=null &&
              this.pmPassword.equals(other.getPmPassword()))) &&
            ((this.pmUser==null && other.getPmUser()==null) || 
             (this.pmUser!=null &&
              this.pmUser.equals(other.getPmUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getPmNewPassword() != null) {
            _hashCode += getPmNewPassword().hashCode();
        }
        if (getPmNewPasswordConfirmation() != null) {
            _hashCode += getPmNewPasswordConfirmation().hashCode();
        }
        if (getPmPassword() != null) {
            _hashCode += getPmPassword().hashCode();
        }
        if (getPmUser() != null) {
            _hashCode += getPmUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(McwfUsersChangePasswordIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "mcwfUsersChangePasswordIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmNewPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmNewPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmNewPasswordConfirmation");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmNewPasswordConfirmation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmUser");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
