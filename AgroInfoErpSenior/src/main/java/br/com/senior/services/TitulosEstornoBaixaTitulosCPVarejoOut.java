/**
 * TitulosEstornoBaixaTitulosCPVarejoOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosEstornoBaixaTitulosCPVarejoOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno[] retorno;

    private java.lang.Integer tipoRetorno;

    public TitulosEstornoBaixaTitulosCPVarejoOut() {
    }

    public TitulosEstornoBaixaTitulosCPVarejoOut(
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno[] retorno,
           java.lang.Integer tipoRetorno) {
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.retorno = retorno;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the erroExecucao value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the retorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @return retorno
     */
    public br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno[] getRetorno() {
        return retorno;
    }


    /**
     * Sets the retorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @param retorno
     */
    public void setRetorno(br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno[] retorno) {
        this.retorno = retorno;
    }

    public br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno getRetorno(int i) {
        return this.retorno[i];
    }

    public void setRetorno(int i, br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOutRetorno _value) {
        this.retorno[i] = _value;
    }


    /**
     * Gets the tipoRetorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosEstornoBaixaTitulosCPVarejoOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosEstornoBaixaTitulosCPVarejoOut)) return false;
        TitulosEstornoBaixaTitulosCPVarejoOut other = (TitulosEstornoBaixaTitulosCPVarejoOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.retorno==null && other.getRetorno()==null) || 
             (this.retorno!=null &&
              java.util.Arrays.equals(this.retorno, other.getRetorno()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getRetorno() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRetorno());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRetorno(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosEstornoBaixaTitulosCPVarejoOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEstornoBaixaTitulosCPVarejoOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEstornoBaixaTitulosCPVarejoOutRetorno"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
