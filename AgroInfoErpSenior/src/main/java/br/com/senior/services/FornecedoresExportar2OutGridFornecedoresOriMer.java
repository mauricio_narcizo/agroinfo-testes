/**
 * FornecedoresExportar2OutGridFornecedoresOriMer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportar2OutGridFornecedoresOriMer  implements java.io.Serializable {
    private java.lang.String baiOrm;

    private java.lang.Integer cepOrm;

    private java.lang.Double cgcOrm;

    private java.lang.String cidOrm;

    private java.lang.Integer codFor;

    private java.lang.String cplOrm;

    private java.lang.String eenOrm;

    private java.lang.String endOrm;

    private java.lang.String estOrm;

    private java.lang.Integer iniOrm;

    private java.lang.String insOrm;

    private java.lang.String numOrm;

    private java.lang.String prxOrm;

    private java.lang.Integer seqOrm;

    private java.lang.String sitOrm;

    public FornecedoresExportar2OutGridFornecedoresOriMer() {
    }

    public FornecedoresExportar2OutGridFornecedoresOriMer(
           java.lang.String baiOrm,
           java.lang.Integer cepOrm,
           java.lang.Double cgcOrm,
           java.lang.String cidOrm,
           java.lang.Integer codFor,
           java.lang.String cplOrm,
           java.lang.String eenOrm,
           java.lang.String endOrm,
           java.lang.String estOrm,
           java.lang.Integer iniOrm,
           java.lang.String insOrm,
           java.lang.String numOrm,
           java.lang.String prxOrm,
           java.lang.Integer seqOrm,
           java.lang.String sitOrm) {
           this.baiOrm = baiOrm;
           this.cepOrm = cepOrm;
           this.cgcOrm = cgcOrm;
           this.cidOrm = cidOrm;
           this.codFor = codFor;
           this.cplOrm = cplOrm;
           this.eenOrm = eenOrm;
           this.endOrm = endOrm;
           this.estOrm = estOrm;
           this.iniOrm = iniOrm;
           this.insOrm = insOrm;
           this.numOrm = numOrm;
           this.prxOrm = prxOrm;
           this.seqOrm = seqOrm;
           this.sitOrm = sitOrm;
    }


    /**
     * Gets the baiOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return baiOrm
     */
    public java.lang.String getBaiOrm() {
        return baiOrm;
    }


    /**
     * Sets the baiOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param baiOrm
     */
    public void setBaiOrm(java.lang.String baiOrm) {
        this.baiOrm = baiOrm;
    }


    /**
     * Gets the cepOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return cepOrm
     */
    public java.lang.Integer getCepOrm() {
        return cepOrm;
    }


    /**
     * Sets the cepOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param cepOrm
     */
    public void setCepOrm(java.lang.Integer cepOrm) {
        this.cepOrm = cepOrm;
    }


    /**
     * Gets the cgcOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return cgcOrm
     */
    public java.lang.Double getCgcOrm() {
        return cgcOrm;
    }


    /**
     * Sets the cgcOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param cgcOrm
     */
    public void setCgcOrm(java.lang.Double cgcOrm) {
        this.cgcOrm = cgcOrm;
    }


    /**
     * Gets the cidOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return cidOrm
     */
    public java.lang.String getCidOrm() {
        return cidOrm;
    }


    /**
     * Sets the cidOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param cidOrm
     */
    public void setCidOrm(java.lang.String cidOrm) {
        this.cidOrm = cidOrm;
    }


    /**
     * Gets the codFor value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the cplOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return cplOrm
     */
    public java.lang.String getCplOrm() {
        return cplOrm;
    }


    /**
     * Sets the cplOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param cplOrm
     */
    public void setCplOrm(java.lang.String cplOrm) {
        this.cplOrm = cplOrm;
    }


    /**
     * Gets the eenOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return eenOrm
     */
    public java.lang.String getEenOrm() {
        return eenOrm;
    }


    /**
     * Sets the eenOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param eenOrm
     */
    public void setEenOrm(java.lang.String eenOrm) {
        this.eenOrm = eenOrm;
    }


    /**
     * Gets the endOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return endOrm
     */
    public java.lang.String getEndOrm() {
        return endOrm;
    }


    /**
     * Sets the endOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param endOrm
     */
    public void setEndOrm(java.lang.String endOrm) {
        this.endOrm = endOrm;
    }


    /**
     * Gets the estOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return estOrm
     */
    public java.lang.String getEstOrm() {
        return estOrm;
    }


    /**
     * Sets the estOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param estOrm
     */
    public void setEstOrm(java.lang.String estOrm) {
        this.estOrm = estOrm;
    }


    /**
     * Gets the iniOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return iniOrm
     */
    public java.lang.Integer getIniOrm() {
        return iniOrm;
    }


    /**
     * Sets the iniOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param iniOrm
     */
    public void setIniOrm(java.lang.Integer iniOrm) {
        this.iniOrm = iniOrm;
    }


    /**
     * Gets the insOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return insOrm
     */
    public java.lang.String getInsOrm() {
        return insOrm;
    }


    /**
     * Sets the insOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param insOrm
     */
    public void setInsOrm(java.lang.String insOrm) {
        this.insOrm = insOrm;
    }


    /**
     * Gets the numOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return numOrm
     */
    public java.lang.String getNumOrm() {
        return numOrm;
    }


    /**
     * Sets the numOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param numOrm
     */
    public void setNumOrm(java.lang.String numOrm) {
        this.numOrm = numOrm;
    }


    /**
     * Gets the prxOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return prxOrm
     */
    public java.lang.String getPrxOrm() {
        return prxOrm;
    }


    /**
     * Sets the prxOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param prxOrm
     */
    public void setPrxOrm(java.lang.String prxOrm) {
        this.prxOrm = prxOrm;
    }


    /**
     * Gets the seqOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return seqOrm
     */
    public java.lang.Integer getSeqOrm() {
        return seqOrm;
    }


    /**
     * Sets the seqOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param seqOrm
     */
    public void setSeqOrm(java.lang.Integer seqOrm) {
        this.seqOrm = seqOrm;
    }


    /**
     * Gets the sitOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @return sitOrm
     */
    public java.lang.String getSitOrm() {
        return sitOrm;
    }


    /**
     * Sets the sitOrm value for this FornecedoresExportar2OutGridFornecedoresOriMer.
     * 
     * @param sitOrm
     */
    public void setSitOrm(java.lang.String sitOrm) {
        this.sitOrm = sitOrm;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportar2OutGridFornecedoresOriMer)) return false;
        FornecedoresExportar2OutGridFornecedoresOriMer other = (FornecedoresExportar2OutGridFornecedoresOriMer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baiOrm==null && other.getBaiOrm()==null) || 
             (this.baiOrm!=null &&
              this.baiOrm.equals(other.getBaiOrm()))) &&
            ((this.cepOrm==null && other.getCepOrm()==null) || 
             (this.cepOrm!=null &&
              this.cepOrm.equals(other.getCepOrm()))) &&
            ((this.cgcOrm==null && other.getCgcOrm()==null) || 
             (this.cgcOrm!=null &&
              this.cgcOrm.equals(other.getCgcOrm()))) &&
            ((this.cidOrm==null && other.getCidOrm()==null) || 
             (this.cidOrm!=null &&
              this.cidOrm.equals(other.getCidOrm()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.cplOrm==null && other.getCplOrm()==null) || 
             (this.cplOrm!=null &&
              this.cplOrm.equals(other.getCplOrm()))) &&
            ((this.eenOrm==null && other.getEenOrm()==null) || 
             (this.eenOrm!=null &&
              this.eenOrm.equals(other.getEenOrm()))) &&
            ((this.endOrm==null && other.getEndOrm()==null) || 
             (this.endOrm!=null &&
              this.endOrm.equals(other.getEndOrm()))) &&
            ((this.estOrm==null && other.getEstOrm()==null) || 
             (this.estOrm!=null &&
              this.estOrm.equals(other.getEstOrm()))) &&
            ((this.iniOrm==null && other.getIniOrm()==null) || 
             (this.iniOrm!=null &&
              this.iniOrm.equals(other.getIniOrm()))) &&
            ((this.insOrm==null && other.getInsOrm()==null) || 
             (this.insOrm!=null &&
              this.insOrm.equals(other.getInsOrm()))) &&
            ((this.numOrm==null && other.getNumOrm()==null) || 
             (this.numOrm!=null &&
              this.numOrm.equals(other.getNumOrm()))) &&
            ((this.prxOrm==null && other.getPrxOrm()==null) || 
             (this.prxOrm!=null &&
              this.prxOrm.equals(other.getPrxOrm()))) &&
            ((this.seqOrm==null && other.getSeqOrm()==null) || 
             (this.seqOrm!=null &&
              this.seqOrm.equals(other.getSeqOrm()))) &&
            ((this.sitOrm==null && other.getSitOrm()==null) || 
             (this.sitOrm!=null &&
              this.sitOrm.equals(other.getSitOrm())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaiOrm() != null) {
            _hashCode += getBaiOrm().hashCode();
        }
        if (getCepOrm() != null) {
            _hashCode += getCepOrm().hashCode();
        }
        if (getCgcOrm() != null) {
            _hashCode += getCgcOrm().hashCode();
        }
        if (getCidOrm() != null) {
            _hashCode += getCidOrm().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCplOrm() != null) {
            _hashCode += getCplOrm().hashCode();
        }
        if (getEenOrm() != null) {
            _hashCode += getEenOrm().hashCode();
        }
        if (getEndOrm() != null) {
            _hashCode += getEndOrm().hashCode();
        }
        if (getEstOrm() != null) {
            _hashCode += getEstOrm().hashCode();
        }
        if (getIniOrm() != null) {
            _hashCode += getIniOrm().hashCode();
        }
        if (getInsOrm() != null) {
            _hashCode += getInsOrm().hashCode();
        }
        if (getNumOrm() != null) {
            _hashCode += getNumOrm().hashCode();
        }
        if (getPrxOrm() != null) {
            _hashCode += getPrxOrm().hashCode();
        }
        if (getSeqOrm() != null) {
            _hashCode += getSeqOrm().hashCode();
        }
        if (getSitOrm() != null) {
            _hashCode += getSitOrm().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportar2OutGridFornecedoresOriMer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresOriMer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eenOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eenOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prxOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prxOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitOrm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitOrm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
