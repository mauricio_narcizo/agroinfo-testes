/**
 * TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosBaixarTitulosCPVarejoInBaixaTituloPagar  implements java.io.Serializable {
    private java.lang.String agrPre;

    private java.lang.String ccbFor;

    private java.lang.String cnpjFilial;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codCcu;

    private java.lang.String codEmp;

    private java.lang.String codFil;

    private java.lang.String codFor;

    private java.lang.String codFpg;

    private java.lang.String codFpj;

    private java.lang.String codTns;

    private java.lang.String codTpt;

    private java.lang.String cotMcp;

    private java.lang.String ctaFin;

    private java.lang.String ctaRed;

    private java.lang.String datLib;

    private java.lang.String datMov;

    private java.lang.String datPgt;

    private java.lang.Integer ideExt;

    private java.lang.String indAbt;

    private java.lang.String indCan;

    private java.lang.String numCco;

    private java.lang.String numDoc;

    private java.lang.Integer numPdv;

    private java.lang.String numPrj;

    private java.lang.String numTit;

    private java.lang.String obsMcp;

    private java.lang.String seqChe;

    private java.lang.String tipPgt;

    private java.lang.String tnsCxb;

    private java.lang.String vlrCor;

    private java.lang.String vlrDsc;

    private java.lang.String vlrEnc;

    private java.lang.String vlrJrs;

    private java.lang.String vlrLiq;

    private java.lang.String vlrMov;

    private java.lang.String vlrMul;

    private java.lang.String vlrOac;

    private java.lang.String vlrOde;

    public TitulosBaixarTitulosCPVarejoInBaixaTituloPagar() {
    }

    public TitulosBaixarTitulosCPVarejoInBaixaTituloPagar(
           java.lang.String agrPre,
           java.lang.String ccbFor,
           java.lang.String cnpjFilial,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codCcu,
           java.lang.String codEmp,
           java.lang.String codFil,
           java.lang.String codFor,
           java.lang.String codFpg,
           java.lang.String codFpj,
           java.lang.String codTns,
           java.lang.String codTpt,
           java.lang.String cotMcp,
           java.lang.String ctaFin,
           java.lang.String ctaRed,
           java.lang.String datLib,
           java.lang.String datMov,
           java.lang.String datPgt,
           java.lang.Integer ideExt,
           java.lang.String indAbt,
           java.lang.String indCan,
           java.lang.String numCco,
           java.lang.String numDoc,
           java.lang.Integer numPdv,
           java.lang.String numPrj,
           java.lang.String numTit,
           java.lang.String obsMcp,
           java.lang.String seqChe,
           java.lang.String tipPgt,
           java.lang.String tnsCxb,
           java.lang.String vlrCor,
           java.lang.String vlrDsc,
           java.lang.String vlrEnc,
           java.lang.String vlrJrs,
           java.lang.String vlrLiq,
           java.lang.String vlrMov,
           java.lang.String vlrMul,
           java.lang.String vlrOac,
           java.lang.String vlrOde) {
           this.agrPre = agrPre;
           this.ccbFor = ccbFor;
           this.cnpjFilial = cnpjFilial;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codCcu = codCcu;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codTns = codTns;
           this.codTpt = codTpt;
           this.cotMcp = cotMcp;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.datLib = datLib;
           this.datMov = datMov;
           this.datPgt = datPgt;
           this.ideExt = ideExt;
           this.indAbt = indAbt;
           this.indCan = indCan;
           this.numCco = numCco;
           this.numDoc = numDoc;
           this.numPdv = numPdv;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.obsMcp = obsMcp;
           this.seqChe = seqChe;
           this.tipPgt = tipPgt;
           this.tnsCxb = tnsCxb;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrEnc = vlrEnc;
           this.vlrJrs = vlrJrs;
           this.vlrLiq = vlrLiq;
           this.vlrMov = vlrMov;
           this.vlrMul = vlrMul;
           this.vlrOac = vlrOac;
           this.vlrOde = vlrOde;
    }


    /**
     * Gets the agrPre value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return agrPre
     */
    public java.lang.String getAgrPre() {
        return agrPre;
    }


    /**
     * Sets the agrPre value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param agrPre
     */
    public void setAgrPre(java.lang.String agrPre) {
        this.agrPre = agrPre;
    }


    /**
     * Gets the ccbFor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the cnpjFilial value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return cnpjFilial
     */
    public java.lang.String getCnpjFilial() {
        return cnpjFilial;
    }


    /**
     * Sets the cnpjFilial value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param cnpjFilial
     */
    public void setCnpjFilial(java.lang.String cnpjFilial) {
        this.cnpjFilial = cnpjFilial;
    }


    /**
     * Gets the codAge value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCcu value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codEmp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codEmp
     */
    public java.lang.String getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.String codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codFil
     */
    public java.lang.String getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.String codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codFpg
     */
    public java.lang.String getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.String codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codFpj
     */
    public java.lang.String getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.String codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codTns value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codTns
     */
    public java.lang.String getCodTns() {
        return codTns;
    }


    /**
     * Sets the codTns value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codTns
     */
    public void setCodTns(java.lang.String codTns) {
        this.codTns = codTns;
    }


    /**
     * Gets the codTpt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the cotMcp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return cotMcp
     */
    public java.lang.String getCotMcp() {
        return cotMcp;
    }


    /**
     * Sets the cotMcp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param cotMcp
     */
    public void setCotMcp(java.lang.String cotMcp) {
        this.cotMcp = cotMcp;
    }


    /**
     * Gets the ctaFin value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return ctaFin
     */
    public java.lang.String getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.String ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return ctaRed
     */
    public java.lang.String getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.String ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datLib value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return datLib
     */
    public java.lang.String getDatLib() {
        return datLib;
    }


    /**
     * Sets the datLib value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param datLib
     */
    public void setDatLib(java.lang.String datLib) {
        this.datLib = datLib;
    }


    /**
     * Gets the datMov value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return datMov
     */
    public java.lang.String getDatMov() {
        return datMov;
    }


    /**
     * Sets the datMov value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param datMov
     */
    public void setDatMov(java.lang.String datMov) {
        this.datMov = datMov;
    }


    /**
     * Gets the datPgt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return datPgt
     */
    public java.lang.String getDatPgt() {
        return datPgt;
    }


    /**
     * Sets the datPgt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param datPgt
     */
    public void setDatPgt(java.lang.String datPgt) {
        this.datPgt = datPgt;
    }


    /**
     * Gets the ideExt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the indAbt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return indAbt
     */
    public java.lang.String getIndAbt() {
        return indAbt;
    }


    /**
     * Sets the indAbt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param indAbt
     */
    public void setIndAbt(java.lang.String indAbt) {
        this.indAbt = indAbt;
    }


    /**
     * Gets the indCan value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return indCan
     */
    public java.lang.String getIndCan() {
        return indCan;
    }


    /**
     * Sets the indCan value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param indCan
     */
    public void setIndCan(java.lang.String indCan) {
        this.indCan = indCan;
    }


    /**
     * Gets the numCco value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return numCco
     */
    public java.lang.String getNumCco() {
        return numCco;
    }


    /**
     * Sets the numCco value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param numCco
     */
    public void setNumCco(java.lang.String numCco) {
        this.numCco = numCco;
    }


    /**
     * Gets the numDoc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return numDoc
     */
    public java.lang.String getNumDoc() {
        return numDoc;
    }


    /**
     * Sets the numDoc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param numDoc
     */
    public void setNumDoc(java.lang.String numDoc) {
        this.numDoc = numDoc;
    }


    /**
     * Gets the numPdv value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return numPdv
     */
    public java.lang.Integer getNumPdv() {
        return numPdv;
    }


    /**
     * Sets the numPdv value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param numPdv
     */
    public void setNumPdv(java.lang.Integer numPdv) {
        this.numPdv = numPdv;
    }


    /**
     * Gets the numPrj value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return numPrj
     */
    public java.lang.String getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.String numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsMcp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return obsMcp
     */
    public java.lang.String getObsMcp() {
        return obsMcp;
    }


    /**
     * Sets the obsMcp value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param obsMcp
     */
    public void setObsMcp(java.lang.String obsMcp) {
        this.obsMcp = obsMcp;
    }


    /**
     * Gets the seqChe value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return seqChe
     */
    public java.lang.String getSeqChe() {
        return seqChe;
    }


    /**
     * Sets the seqChe value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param seqChe
     */
    public void setSeqChe(java.lang.String seqChe) {
        this.seqChe = seqChe;
    }


    /**
     * Gets the tipPgt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return tipPgt
     */
    public java.lang.String getTipPgt() {
        return tipPgt;
    }


    /**
     * Sets the tipPgt value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param tipPgt
     */
    public void setTipPgt(java.lang.String tipPgt) {
        this.tipPgt = tipPgt;
    }


    /**
     * Gets the tnsCxb value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return tnsCxb
     */
    public java.lang.String getTnsCxb() {
        return tnsCxb;
    }


    /**
     * Sets the tnsCxb value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param tnsCxb
     */
    public void setTnsCxb(java.lang.String tnsCxb) {
        this.tnsCxb = tnsCxb;
    }


    /**
     * Gets the vlrCor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrCor
     */
    public java.lang.String getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.String vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrDsc
     */
    public java.lang.String getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.String vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEnc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrEnc
     */
    public java.lang.String getVlrEnc() {
        return vlrEnc;
    }


    /**
     * Sets the vlrEnc value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrEnc
     */
    public void setVlrEnc(java.lang.String vlrEnc) {
        this.vlrEnc = vlrEnc;
    }


    /**
     * Gets the vlrJrs value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrJrs
     */
    public java.lang.String getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.String vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrLiq value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrLiq
     */
    public java.lang.String getVlrLiq() {
        return vlrLiq;
    }


    /**
     * Sets the vlrLiq value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrLiq
     */
    public void setVlrLiq(java.lang.String vlrLiq) {
        this.vlrLiq = vlrLiq;
    }


    /**
     * Gets the vlrMov value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrMov
     */
    public java.lang.String getVlrMov() {
        return vlrMov;
    }


    /**
     * Sets the vlrMov value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrMov
     */
    public void setVlrMov(java.lang.String vlrMov) {
        this.vlrMov = vlrMov;
    }


    /**
     * Gets the vlrMul value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrMul
     */
    public java.lang.String getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.String vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOac value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrOac
     */
    public java.lang.String getVlrOac() {
        return vlrOac;
    }


    /**
     * Sets the vlrOac value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrOac
     */
    public void setVlrOac(java.lang.String vlrOac) {
        this.vlrOac = vlrOac;
    }


    /**
     * Gets the vlrOde value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @return vlrOde
     */
    public java.lang.String getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.String vlrOde) {
        this.vlrOde = vlrOde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosBaixarTitulosCPVarejoInBaixaTituloPagar)) return false;
        TitulosBaixarTitulosCPVarejoInBaixaTituloPagar other = (TitulosBaixarTitulosCPVarejoInBaixaTituloPagar) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.agrPre==null && other.getAgrPre()==null) || 
             (this.agrPre!=null &&
              this.agrPre.equals(other.getAgrPre()))) &&
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.cnpjFilial==null && other.getCnpjFilial()==null) || 
             (this.cnpjFilial!=null &&
              this.cnpjFilial.equals(other.getCnpjFilial()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codTns==null && other.getCodTns()==null) || 
             (this.codTns!=null &&
              this.codTns.equals(other.getCodTns()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.cotMcp==null && other.getCotMcp()==null) || 
             (this.cotMcp!=null &&
              this.cotMcp.equals(other.getCotMcp()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datLib==null && other.getDatLib()==null) || 
             (this.datLib!=null &&
              this.datLib.equals(other.getDatLib()))) &&
            ((this.datMov==null && other.getDatMov()==null) || 
             (this.datMov!=null &&
              this.datMov.equals(other.getDatMov()))) &&
            ((this.datPgt==null && other.getDatPgt()==null) || 
             (this.datPgt!=null &&
              this.datPgt.equals(other.getDatPgt()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.indAbt==null && other.getIndAbt()==null) || 
             (this.indAbt!=null &&
              this.indAbt.equals(other.getIndAbt()))) &&
            ((this.indCan==null && other.getIndCan()==null) || 
             (this.indCan!=null &&
              this.indCan.equals(other.getIndCan()))) &&
            ((this.numCco==null && other.getNumCco()==null) || 
             (this.numCco!=null &&
              this.numCco.equals(other.getNumCco()))) &&
            ((this.numDoc==null && other.getNumDoc()==null) || 
             (this.numDoc!=null &&
              this.numDoc.equals(other.getNumDoc()))) &&
            ((this.numPdv==null && other.getNumPdv()==null) || 
             (this.numPdv!=null &&
              this.numPdv.equals(other.getNumPdv()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsMcp==null && other.getObsMcp()==null) || 
             (this.obsMcp!=null &&
              this.obsMcp.equals(other.getObsMcp()))) &&
            ((this.seqChe==null && other.getSeqChe()==null) || 
             (this.seqChe!=null &&
              this.seqChe.equals(other.getSeqChe()))) &&
            ((this.tipPgt==null && other.getTipPgt()==null) || 
             (this.tipPgt!=null &&
              this.tipPgt.equals(other.getTipPgt()))) &&
            ((this.tnsCxb==null && other.getTnsCxb()==null) || 
             (this.tnsCxb!=null &&
              this.tnsCxb.equals(other.getTnsCxb()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEnc==null && other.getVlrEnc()==null) || 
             (this.vlrEnc!=null &&
              this.vlrEnc.equals(other.getVlrEnc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrLiq==null && other.getVlrLiq()==null) || 
             (this.vlrLiq!=null &&
              this.vlrLiq.equals(other.getVlrLiq()))) &&
            ((this.vlrMov==null && other.getVlrMov()==null) || 
             (this.vlrMov!=null &&
              this.vlrMov.equals(other.getVlrMov()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOac==null && other.getVlrOac()==null) || 
             (this.vlrOac!=null &&
              this.vlrOac.equals(other.getVlrOac()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAgrPre() != null) {
            _hashCode += getAgrPre().hashCode();
        }
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCnpjFilial() != null) {
            _hashCode += getCnpjFilial().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodTns() != null) {
            _hashCode += getCodTns().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCotMcp() != null) {
            _hashCode += getCotMcp().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatLib() != null) {
            _hashCode += getDatLib().hashCode();
        }
        if (getDatMov() != null) {
            _hashCode += getDatMov().hashCode();
        }
        if (getDatPgt() != null) {
            _hashCode += getDatPgt().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getIndAbt() != null) {
            _hashCode += getIndAbt().hashCode();
        }
        if (getIndCan() != null) {
            _hashCode += getIndCan().hashCode();
        }
        if (getNumCco() != null) {
            _hashCode += getNumCco().hashCode();
        }
        if (getNumDoc() != null) {
            _hashCode += getNumDoc().hashCode();
        }
        if (getNumPdv() != null) {
            _hashCode += getNumPdv().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsMcp() != null) {
            _hashCode += getObsMcp().hashCode();
        }
        if (getSeqChe() != null) {
            _hashCode += getSeqChe().hashCode();
        }
        if (getTipPgt() != null) {
            _hashCode += getTipPgt().hashCode();
        }
        if (getTnsCxb() != null) {
            _hashCode += getTnsCxb().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEnc() != null) {
            _hashCode += getVlrEnc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrLiq() != null) {
            _hashCode += getVlrLiq().hashCode();
        }
        if (getVlrMov() != null) {
            _hashCode += getVlrMov().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOac() != null) {
            _hashCode += getVlrOac().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosBaixarTitulosCPVarejoInBaixaTituloPagar.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixarTitulosCPVarejoInBaixaTituloPagar"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agrPre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "agrPre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnpjFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cnpjFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cotMcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cotMcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLib");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLib"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAbt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAbt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indCan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indCan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqChe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqChe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCxb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCxb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLiq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLiq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
