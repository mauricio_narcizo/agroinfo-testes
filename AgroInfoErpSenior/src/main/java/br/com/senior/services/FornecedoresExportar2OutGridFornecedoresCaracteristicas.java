/**
 * FornecedoresExportar2OutGridFornecedoresCaracteristicas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportar2OutGridFornecedoresCaracteristicas  implements java.io.Serializable {
    private java.lang.Integer codCcc;

    private java.lang.String codCcl;

    private java.lang.Integer codFor;

    private java.lang.Integer seqOrd;

    public FornecedoresExportar2OutGridFornecedoresCaracteristicas() {
    }

    public FornecedoresExportar2OutGridFornecedoresCaracteristicas(
           java.lang.Integer codCcc,
           java.lang.String codCcl,
           java.lang.Integer codFor,
           java.lang.Integer seqOrd) {
           this.codCcc = codCcc;
           this.codCcl = codCcl;
           this.codFor = codFor;
           this.seqOrd = seqOrd;
    }


    /**
     * Gets the codCcc value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @return codCcc
     */
    public java.lang.Integer getCodCcc() {
        return codCcc;
    }


    /**
     * Sets the codCcc value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @param codCcc
     */
    public void setCodCcc(java.lang.Integer codCcc) {
        this.codCcc = codCcc;
    }


    /**
     * Gets the codCcl value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @return codCcl
     */
    public java.lang.String getCodCcl() {
        return codCcl;
    }


    /**
     * Sets the codCcl value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @param codCcl
     */
    public void setCodCcl(java.lang.String codCcl) {
        this.codCcl = codCcl;
    }


    /**
     * Gets the codFor value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the seqOrd value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @return seqOrd
     */
    public java.lang.Integer getSeqOrd() {
        return seqOrd;
    }


    /**
     * Sets the seqOrd value for this FornecedoresExportar2OutGridFornecedoresCaracteristicas.
     * 
     * @param seqOrd
     */
    public void setSeqOrd(java.lang.Integer seqOrd) {
        this.seqOrd = seqOrd;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportar2OutGridFornecedoresCaracteristicas)) return false;
        FornecedoresExportar2OutGridFornecedoresCaracteristicas other = (FornecedoresExportar2OutGridFornecedoresCaracteristicas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCcc==null && other.getCodCcc()==null) || 
             (this.codCcc!=null &&
              this.codCcc.equals(other.getCodCcc()))) &&
            ((this.codCcl==null && other.getCodCcl()==null) || 
             (this.codCcl!=null &&
              this.codCcl.equals(other.getCodCcl()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.seqOrd==null && other.getSeqOrd()==null) || 
             (this.seqOrd!=null &&
              this.seqOrd.equals(other.getSeqOrd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCcc() != null) {
            _hashCode += getCodCcc().hashCode();
        }
        if (getCodCcl() != null) {
            _hashCode += getCodCcl().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getSeqOrd() != null) {
            _hashCode += getSeqOrd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportar2OutGridFornecedoresCaracteristicas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresCaracteristicas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqOrd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqOrd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
