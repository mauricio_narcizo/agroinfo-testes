/**
 * FornecedoresExportarOutGridFornecedoresCNAE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportarOutGridFornecedoresCNAE  implements java.io.Serializable {
    private java.lang.Double codCna;

    private java.lang.Integer codFor;

    public FornecedoresExportarOutGridFornecedoresCNAE() {
    }

    public FornecedoresExportarOutGridFornecedoresCNAE(
           java.lang.Double codCna,
           java.lang.Integer codFor) {
           this.codCna = codCna;
           this.codFor = codFor;
    }


    /**
     * Gets the codCna value for this FornecedoresExportarOutGridFornecedoresCNAE.
     * 
     * @return codCna
     */
    public java.lang.Double getCodCna() {
        return codCna;
    }


    /**
     * Sets the codCna value for this FornecedoresExportarOutGridFornecedoresCNAE.
     * 
     * @param codCna
     */
    public void setCodCna(java.lang.Double codCna) {
        this.codCna = codCna;
    }


    /**
     * Gets the codFor value for this FornecedoresExportarOutGridFornecedoresCNAE.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportarOutGridFornecedoresCNAE.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportarOutGridFornecedoresCNAE)) return false;
        FornecedoresExportarOutGridFornecedoresCNAE other = (FornecedoresExportarOutGridFornecedoresCNAE) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCna==null && other.getCodCna()==null) || 
             (this.codCna!=null &&
              this.codCna.equals(other.getCodCna()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCna() != null) {
            _hashCode += getCodCna().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportarOutGridFornecedoresCNAE.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCNAE"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCna");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCna"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
