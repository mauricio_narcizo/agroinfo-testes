/**
 * TipotituloExportarIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TipotituloExportarIn  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Long codFil;

    private br.com.senior.services.TipotituloExportarInConsulta[] consulta;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String identificacaoSistema;

    private java.lang.Integer quantidadeRegistros;

    private java.lang.String tipoIntegracao;

    public TipotituloExportarIn() {
    }

    public TipotituloExportarIn(
           java.lang.Integer codEmp,
           java.lang.Long codFil,
           br.com.senior.services.TipotituloExportarInConsulta[] consulta,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String identificacaoSistema,
           java.lang.Integer quantidadeRegistros,
           java.lang.String tipoIntegracao) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.consulta = consulta;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.identificacaoSistema = identificacaoSistema;
           this.quantidadeRegistros = quantidadeRegistros;
           this.tipoIntegracao = tipoIntegracao;
    }


    /**
     * Gets the codEmp value for this TipotituloExportarIn.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TipotituloExportarIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TipotituloExportarIn.
     * 
     * @return codFil
     */
    public java.lang.Long getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TipotituloExportarIn.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Long codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the consulta value for this TipotituloExportarIn.
     * 
     * @return consulta
     */
    public br.com.senior.services.TipotituloExportarInConsulta[] getConsulta() {
        return consulta;
    }


    /**
     * Sets the consulta value for this TipotituloExportarIn.
     * 
     * @param consulta
     */
    public void setConsulta(br.com.senior.services.TipotituloExportarInConsulta[] consulta) {
        this.consulta = consulta;
    }

    public br.com.senior.services.TipotituloExportarInConsulta getConsulta(int i) {
        return this.consulta[i];
    }

    public void setConsulta(int i, br.com.senior.services.TipotituloExportarInConsulta _value) {
        this.consulta[i] = _value;
    }


    /**
     * Gets the flowInstanceID value for this TipotituloExportarIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TipotituloExportarIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TipotituloExportarIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TipotituloExportarIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the identificacaoSistema value for this TipotituloExportarIn.
     * 
     * @return identificacaoSistema
     */
    public java.lang.String getIdentificacaoSistema() {
        return identificacaoSistema;
    }


    /**
     * Sets the identificacaoSistema value for this TipotituloExportarIn.
     * 
     * @param identificacaoSistema
     */
    public void setIdentificacaoSistema(java.lang.String identificacaoSistema) {
        this.identificacaoSistema = identificacaoSistema;
    }


    /**
     * Gets the quantidadeRegistros value for this TipotituloExportarIn.
     * 
     * @return quantidadeRegistros
     */
    public java.lang.Integer getQuantidadeRegistros() {
        return quantidadeRegistros;
    }


    /**
     * Sets the quantidadeRegistros value for this TipotituloExportarIn.
     * 
     * @param quantidadeRegistros
     */
    public void setQuantidadeRegistros(java.lang.Integer quantidadeRegistros) {
        this.quantidadeRegistros = quantidadeRegistros;
    }


    /**
     * Gets the tipoIntegracao value for this TipotituloExportarIn.
     * 
     * @return tipoIntegracao
     */
    public java.lang.String getTipoIntegracao() {
        return tipoIntegracao;
    }


    /**
     * Sets the tipoIntegracao value for this TipotituloExportarIn.
     * 
     * @param tipoIntegracao
     */
    public void setTipoIntegracao(java.lang.String tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipotituloExportarIn)) return false;
        TipotituloExportarIn other = (TipotituloExportarIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.consulta==null && other.getConsulta()==null) || 
             (this.consulta!=null &&
              java.util.Arrays.equals(this.consulta, other.getConsulta()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.identificacaoSistema==null && other.getIdentificacaoSistema()==null) || 
             (this.identificacaoSistema!=null &&
              this.identificacaoSistema.equals(other.getIdentificacaoSistema()))) &&
            ((this.quantidadeRegistros==null && other.getQuantidadeRegistros()==null) || 
             (this.quantidadeRegistros!=null &&
              this.quantidadeRegistros.equals(other.getQuantidadeRegistros()))) &&
            ((this.tipoIntegracao==null && other.getTipoIntegracao()==null) || 
             (this.tipoIntegracao!=null &&
              this.tipoIntegracao.equals(other.getTipoIntegracao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getConsulta() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsulta());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsulta(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdentificacaoSistema() != null) {
            _hashCode += getIdentificacaoSistema().hashCode();
        }
        if (getQuantidadeRegistros() != null) {
            _hashCode += getQuantidadeRegistros().hashCode();
        }
        if (getTipoIntegracao() != null) {
            _hashCode += getTipoIntegracao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipotituloExportarIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "tipotituloExportarIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consulta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "consulta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "tipotituloExportarInConsulta"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificacaoSistema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identificacaoSistema"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantidadeRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoIntegracao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoIntegracao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
