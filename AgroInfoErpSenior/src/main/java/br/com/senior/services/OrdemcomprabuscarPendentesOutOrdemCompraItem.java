/**
 * OrdemcomprabuscarPendentesOutOrdemCompraItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class OrdemcomprabuscarPendentesOutOrdemCompraItem  implements java.io.Serializable {
    private java.lang.String descricao;

    private java.lang.String descricaoMoeda;

    private java.lang.String observacao;

    private java.lang.Double quantidadeSolicitada;

    private br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio[] rateio;

    private java.lang.String siglaMoeda;

    private java.lang.String unidadeMedida;

    private java.lang.Double valorLiquido;

    private java.lang.Double valorLiquidoMoeda;

    private java.lang.Double valorUnitario;

    public OrdemcomprabuscarPendentesOutOrdemCompraItem() {
    }

    public OrdemcomprabuscarPendentesOutOrdemCompraItem(
           java.lang.String descricao,
           java.lang.String descricaoMoeda,
           java.lang.String observacao,
           java.lang.Double quantidadeSolicitada,
           br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio[] rateio,
           java.lang.String siglaMoeda,
           java.lang.String unidadeMedida,
           java.lang.Double valorLiquido,
           java.lang.Double valorLiquidoMoeda,
           java.lang.Double valorUnitario) {
           this.descricao = descricao;
           this.descricaoMoeda = descricaoMoeda;
           this.observacao = observacao;
           this.quantidadeSolicitada = quantidadeSolicitada;
           this.rateio = rateio;
           this.siglaMoeda = siglaMoeda;
           this.unidadeMedida = unidadeMedida;
           this.valorLiquido = valorLiquido;
           this.valorLiquidoMoeda = valorLiquidoMoeda;
           this.valorUnitario = valorUnitario;
    }


    /**
     * Gets the descricao value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return descricao
     */
    public java.lang.String getDescricao() {
        return descricao;
    }


    /**
     * Sets the descricao value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param descricao
     */
    public void setDescricao(java.lang.String descricao) {
        this.descricao = descricao;
    }


    /**
     * Gets the descricaoMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return descricaoMoeda
     */
    public java.lang.String getDescricaoMoeda() {
        return descricaoMoeda;
    }


    /**
     * Sets the descricaoMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param descricaoMoeda
     */
    public void setDescricaoMoeda(java.lang.String descricaoMoeda) {
        this.descricaoMoeda = descricaoMoeda;
    }


    /**
     * Gets the observacao value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return observacao
     */
    public java.lang.String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param observacao
     */
    public void setObservacao(java.lang.String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the quantidadeSolicitada value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return quantidadeSolicitada
     */
    public java.lang.Double getQuantidadeSolicitada() {
        return quantidadeSolicitada;
    }


    /**
     * Sets the quantidadeSolicitada value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param quantidadeSolicitada
     */
    public void setQuantidadeSolicitada(java.lang.Double quantidadeSolicitada) {
        this.quantidadeSolicitada = quantidadeSolicitada;
    }


    /**
     * Gets the rateio value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return rateio
     */
    public br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio[] getRateio() {
        return rateio;
    }


    /**
     * Sets the rateio value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param rateio
     */
    public void setRateio(br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio[] rateio) {
        this.rateio = rateio;
    }

    public br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio getRateio(int i) {
        return this.rateio[i];
    }

    public void setRateio(int i, br.com.senior.services.OrdemcomprabuscarPendentesOutOrdemCompraItemRateio _value) {
        this.rateio[i] = _value;
    }


    /**
     * Gets the siglaMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return siglaMoeda
     */
    public java.lang.String getSiglaMoeda() {
        return siglaMoeda;
    }


    /**
     * Sets the siglaMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param siglaMoeda
     */
    public void setSiglaMoeda(java.lang.String siglaMoeda) {
        this.siglaMoeda = siglaMoeda;
    }


    /**
     * Gets the unidadeMedida value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return unidadeMedida
     */
    public java.lang.String getUnidadeMedida() {
        return unidadeMedida;
    }


    /**
     * Sets the unidadeMedida value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param unidadeMedida
     */
    public void setUnidadeMedida(java.lang.String unidadeMedida) {
        this.unidadeMedida = unidadeMedida;
    }


    /**
     * Gets the valorLiquido value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return valorLiquido
     */
    public java.lang.Double getValorLiquido() {
        return valorLiquido;
    }


    /**
     * Sets the valorLiquido value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param valorLiquido
     */
    public void setValorLiquido(java.lang.Double valorLiquido) {
        this.valorLiquido = valorLiquido;
    }


    /**
     * Gets the valorLiquidoMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return valorLiquidoMoeda
     */
    public java.lang.Double getValorLiquidoMoeda() {
        return valorLiquidoMoeda;
    }


    /**
     * Sets the valorLiquidoMoeda value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param valorLiquidoMoeda
     */
    public void setValorLiquidoMoeda(java.lang.Double valorLiquidoMoeda) {
        this.valorLiquidoMoeda = valorLiquidoMoeda;
    }


    /**
     * Gets the valorUnitario value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @return valorUnitario
     */
    public java.lang.Double getValorUnitario() {
        return valorUnitario;
    }


    /**
     * Sets the valorUnitario value for this OrdemcomprabuscarPendentesOutOrdemCompraItem.
     * 
     * @param valorUnitario
     */
    public void setValorUnitario(java.lang.Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdemcomprabuscarPendentesOutOrdemCompraItem)) return false;
        OrdemcomprabuscarPendentesOutOrdemCompraItem other = (OrdemcomprabuscarPendentesOutOrdemCompraItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.descricao==null && other.getDescricao()==null) || 
             (this.descricao!=null &&
              this.descricao.equals(other.getDescricao()))) &&
            ((this.descricaoMoeda==null && other.getDescricaoMoeda()==null) || 
             (this.descricaoMoeda!=null &&
              this.descricaoMoeda.equals(other.getDescricaoMoeda()))) &&
            ((this.observacao==null && other.getObservacao()==null) || 
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.quantidadeSolicitada==null && other.getQuantidadeSolicitada()==null) || 
             (this.quantidadeSolicitada!=null &&
              this.quantidadeSolicitada.equals(other.getQuantidadeSolicitada()))) &&
            ((this.rateio==null && other.getRateio()==null) || 
             (this.rateio!=null &&
              java.util.Arrays.equals(this.rateio, other.getRateio()))) &&
            ((this.siglaMoeda==null && other.getSiglaMoeda()==null) || 
             (this.siglaMoeda!=null &&
              this.siglaMoeda.equals(other.getSiglaMoeda()))) &&
            ((this.unidadeMedida==null && other.getUnidadeMedida()==null) || 
             (this.unidadeMedida!=null &&
              this.unidadeMedida.equals(other.getUnidadeMedida()))) &&
            ((this.valorLiquido==null && other.getValorLiquido()==null) || 
             (this.valorLiquido!=null &&
              this.valorLiquido.equals(other.getValorLiquido()))) &&
            ((this.valorLiquidoMoeda==null && other.getValorLiquidoMoeda()==null) || 
             (this.valorLiquidoMoeda!=null &&
              this.valorLiquidoMoeda.equals(other.getValorLiquidoMoeda()))) &&
            ((this.valorUnitario==null && other.getValorUnitario()==null) || 
             (this.valorUnitario!=null &&
              this.valorUnitario.equals(other.getValorUnitario())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescricao() != null) {
            _hashCode += getDescricao().hashCode();
        }
        if (getDescricaoMoeda() != null) {
            _hashCode += getDescricaoMoeda().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getQuantidadeSolicitada() != null) {
            _hashCode += getQuantidadeSolicitada().hashCode();
        }
        if (getRateio() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRateio());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRateio(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSiglaMoeda() != null) {
            _hashCode += getSiglaMoeda().hashCode();
        }
        if (getUnidadeMedida() != null) {
            _hashCode += getUnidadeMedida().hashCode();
        }
        if (getValorLiquido() != null) {
            _hashCode += getValorLiquido().hashCode();
        }
        if (getValorLiquidoMoeda() != null) {
            _hashCode += getValorLiquidoMoeda().hashCode();
        }
        if (getValorUnitario() != null) {
            _hashCode += getValorUnitario().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdemcomprabuscarPendentesOutOrdemCompraItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricaoMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricaoMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantidadeSolicitada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantidadeSolicitada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rateio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "ordemcomprabuscarPendentesOutOrdemCompraItemRateio"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("siglaMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "siglaMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unidadeMedida");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unidadeMedida"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquido");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorLiquido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorLiquidoMoeda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorLiquidoMoeda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valorUnitario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valorUnitario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
