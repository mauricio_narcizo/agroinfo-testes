/**
 * TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosBaixaCompensacaoCPCRVarejoInCompensacoes  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String datPgt;

    private java.lang.Integer ideExt;

    private java.lang.Integer numPdv;

    private br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar[] titulosPagar;

    private br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber[] titulosReceber;

    public TitulosBaixaCompensacaoCPCRVarejoInCompensacoes() {
    }

    public TitulosBaixaCompensacaoCPCRVarejoInCompensacoes(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String datPgt,
           java.lang.Integer ideExt,
           java.lang.Integer numPdv,
           br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar[] titulosPagar,
           br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber[] titulosReceber) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.datPgt = datPgt;
           this.ideExt = ideExt;
           this.numPdv = numPdv;
           this.titulosPagar = titulosPagar;
           this.titulosReceber = titulosReceber;
    }


    /**
     * Gets the codEmp value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the datPgt value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return datPgt
     */
    public java.lang.String getDatPgt() {
        return datPgt;
    }


    /**
     * Sets the datPgt value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param datPgt
     */
    public void setDatPgt(java.lang.String datPgt) {
        this.datPgt = datPgt;
    }


    /**
     * Gets the ideExt value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the numPdv value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return numPdv
     */
    public java.lang.Integer getNumPdv() {
        return numPdv;
    }


    /**
     * Sets the numPdv value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param numPdv
     */
    public void setNumPdv(java.lang.Integer numPdv) {
        this.numPdv = numPdv;
    }


    /**
     * Gets the titulosPagar value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return titulosPagar
     */
    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar[] getTitulosPagar() {
        return titulosPagar;
    }


    /**
     * Sets the titulosPagar value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param titulosPagar
     */
    public void setTitulosPagar(br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar[] titulosPagar) {
        this.titulosPagar = titulosPagar;
    }

    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar getTitulosPagar(int i) {
        return this.titulosPagar[i];
    }

    public void setTitulosPagar(int i, br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar _value) {
        this.titulosPagar[i] = _value;
    }


    /**
     * Gets the titulosReceber value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @return titulosReceber
     */
    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber[] getTitulosReceber() {
        return titulosReceber;
    }


    /**
     * Sets the titulosReceber value for this TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.
     * 
     * @param titulosReceber
     */
    public void setTitulosReceber(br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber[] titulosReceber) {
        this.titulosReceber = titulosReceber;
    }

    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber getTitulosReceber(int i) {
        return this.titulosReceber[i];
    }

    public void setTitulosReceber(int i, br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber _value) {
        this.titulosReceber[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosBaixaCompensacaoCPCRVarejoInCompensacoes)) return false;
        TitulosBaixaCompensacaoCPCRVarejoInCompensacoes other = (TitulosBaixaCompensacaoCPCRVarejoInCompensacoes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.datPgt==null && other.getDatPgt()==null) || 
             (this.datPgt!=null &&
              this.datPgt.equals(other.getDatPgt()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.numPdv==null && other.getNumPdv()==null) || 
             (this.numPdv!=null &&
              this.numPdv.equals(other.getNumPdv()))) &&
            ((this.titulosPagar==null && other.getTitulosPagar()==null) || 
             (this.titulosPagar!=null &&
              java.util.Arrays.equals(this.titulosPagar, other.getTitulosPagar()))) &&
            ((this.titulosReceber==null && other.getTitulosReceber()==null) || 
             (this.titulosReceber!=null &&
              java.util.Arrays.equals(this.titulosReceber, other.getTitulosReceber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDatPgt() != null) {
            _hashCode += getDatPgt().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getNumPdv() != null) {
            _hashCode += getNumPdv().hashCode();
        }
        if (getTitulosPagar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosPagar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosPagar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitulosReceber() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosReceber());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosReceber(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosBaixaCompensacaoCPCRVarejoInCompensacoes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixaCompensacaoCPCRVarejoInCompensacoes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosPagar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosPagar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosPagar"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosReceber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosReceber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixaCompensacaoCPCRVarejoInCompensacoesTitulosReceber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
