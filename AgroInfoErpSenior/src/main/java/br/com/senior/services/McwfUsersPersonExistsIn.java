/**
 * McwfUsersPersonExistsIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class McwfUsersPersonExistsIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String pmPersonKind;

    private java.lang.String pmPersonName;

    public McwfUsersPersonExistsIn() {
    }

    public McwfUsersPersonExistsIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String pmPersonKind,
           java.lang.String pmPersonName) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.pmPersonKind = pmPersonKind;
           this.pmPersonName = pmPersonName;
    }


    /**
     * Gets the flowInstanceID value for this McwfUsersPersonExistsIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this McwfUsersPersonExistsIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this McwfUsersPersonExistsIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this McwfUsersPersonExistsIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the pmPersonKind value for this McwfUsersPersonExistsIn.
     * 
     * @return pmPersonKind
     */
    public java.lang.String getPmPersonKind() {
        return pmPersonKind;
    }


    /**
     * Sets the pmPersonKind value for this McwfUsersPersonExistsIn.
     * 
     * @param pmPersonKind
     */
    public void setPmPersonKind(java.lang.String pmPersonKind) {
        this.pmPersonKind = pmPersonKind;
    }


    /**
     * Gets the pmPersonName value for this McwfUsersPersonExistsIn.
     * 
     * @return pmPersonName
     */
    public java.lang.String getPmPersonName() {
        return pmPersonName;
    }


    /**
     * Sets the pmPersonName value for this McwfUsersPersonExistsIn.
     * 
     * @param pmPersonName
     */
    public void setPmPersonName(java.lang.String pmPersonName) {
        this.pmPersonName = pmPersonName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof McwfUsersPersonExistsIn)) return false;
        McwfUsersPersonExistsIn other = (McwfUsersPersonExistsIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.pmPersonKind==null && other.getPmPersonKind()==null) || 
             (this.pmPersonKind!=null &&
              this.pmPersonKind.equals(other.getPmPersonKind()))) &&
            ((this.pmPersonName==null && other.getPmPersonName()==null) || 
             (this.pmPersonName!=null &&
              this.pmPersonName.equals(other.getPmPersonName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getPmPersonKind() != null) {
            _hashCode += getPmPersonKind().hashCode();
        }
        if (getPmPersonName() != null) {
            _hashCode += getPmPersonName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(McwfUsersPersonExistsIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "mcwfUsersPersonExistsIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmPersonKind");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmPersonKind"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmPersonName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmPersonName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
