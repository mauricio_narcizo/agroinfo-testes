/**
 * TitulosExportarTitulosReceber3OutErros.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosExportarTitulosReceber3OutErros  implements java.io.Serializable {
    private java.lang.String msgErr;

    public TitulosExportarTitulosReceber3OutErros() {
    }

    public TitulosExportarTitulosReceber3OutErros(
           java.lang.String msgErr) {
           this.msgErr = msgErr;
    }


    /**
     * Gets the msgErr value for this TitulosExportarTitulosReceber3OutErros.
     * 
     * @return msgErr
     */
    public java.lang.String getMsgErr() {
        return msgErr;
    }


    /**
     * Sets the msgErr value for this TitulosExportarTitulosReceber3OutErros.
     * 
     * @param msgErr
     */
    public void setMsgErr(java.lang.String msgErr) {
        this.msgErr = msgErr;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExportarTitulosReceber3OutErros)) return false;
        TitulosExportarTitulosReceber3OutErros other = (TitulosExportarTitulosReceber3OutErros) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msgErr==null && other.getMsgErr()==null) || 
             (this.msgErr!=null &&
              this.msgErr.equals(other.getMsgErr())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsgErr() != null) {
            _hashCode += getMsgErr().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExportarTitulosReceber3OutErros.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarTitulosReceber3OutErros"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgErr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgErr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
