/**
 * G5SeniorServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class G5SeniorServicesLocator extends org.apache.axis.client.Service implements br.com.senior.services.G5SeniorServices {

    public G5SeniorServicesLocator() {
    }


    public G5SeniorServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public G5SeniorServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_int_varejo_titulosPort
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_titulosPort_address = "http://teste33:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_int_varejo_titulos";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_titulosPortAddress() {
        return sapiens_Synccom_senior_g5_co_int_varejo_titulosPort_address;
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort_address = "http://teste33:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortAddress() {
        return sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort_address;
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort
    private java.lang.String sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort_address = "http://teste33:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra";

    public java.lang.String getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortAddress() {
        return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort_address;
    }

    // Use to get a proxy class for sapiens_SyncMCWFUsersPort
    private java.lang.String sapiens_SyncMCWFUsersPort_address = "http://teste33:8080/g5-senior-services/sapiens_SyncMCWFUsers";

    public java.lang.String getsapiens_SyncMCWFUsersPortAddress() {
        return sapiens_SyncMCWFUsersPort_address;
    }

    // Use to get a proxy class for sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort_address = "http://teste33:8080/g5-senior-services/sapiens_Synccom_senior_g5_co_int_varejo_fornecedores";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortAddress() {
        return sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_int_varejo_titulosPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName = name;
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_titulosPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort(endpoint);
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulosPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulosPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_titulosPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_int_varejo_titulosPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName = name;
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort(endpoint);
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName = name;
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort(endpoint);
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_SyncMCWFUsersPortWSDDServiceName = "sapiens_SyncMCWFUsersPort";

    public java.lang.String getsapiens_SyncMCWFUsersPortWSDDServiceName() {
        return sapiens_SyncMCWFUsersPortWSDDServiceName;
    }

    public void setsapiens_SyncMCWFUsersPortWSDDServiceName(java.lang.String name) {
        sapiens_SyncMCWFUsersPortWSDDServiceName = name;
    }

    public br.com.senior.services.Sapiens_SyncMCWFUsers getsapiens_SyncMCWFUsersPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_SyncMCWFUsersPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_SyncMCWFUsersPort(endpoint);
    }

    public br.com.senior.services.Sapiens_SyncMCWFUsers getsapiens_SyncMCWFUsersPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Sapiens_SyncMCWFUsersPortBindingStub _stub = new br.com.senior.services.Sapiens_SyncMCWFUsersPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_SyncMCWFUsersPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_SyncMCWFUsersPortEndpointAddress(java.lang.String address) {
        sapiens_SyncMCWFUsersPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    // The WSDD service name defaults to the port name.
    private java.lang.String sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName = "sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort";

    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName() {
        return sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName;
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName(java.lang.String name) {
        sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName = name;
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort(endpoint);
    }

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub(portAddress, this);
            _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortEndpointAddress(java.lang.String address) {
        sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulosPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulosPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_titulosPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_titulosPortWSDDServiceName());
                return _stub;
}
else             if (br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortWSDDServiceName());
                return _stub;
}
else             if (br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortWSDDServiceName());
                return _stub;
}
else             if (br.com.senior.services.Sapiens_SyncMCWFUsers.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Sapiens_SyncMCWFUsersPortBindingStub _stub = new br.com.senior.services.Sapiens_SyncMCWFUsersPortBindingStub(new java.net.URL(sapiens_SyncMCWFUsersPort_address), this);
                _stub.setPortName(getsapiens_SyncMCWFUsersPortWSDDServiceName());
                return _stub;
}
else             if (br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub _stub = new br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub(new java.net.URL(sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort_address), this);
                _stub.setPortName(getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("sapiens_Synccom_senior_g5_co_int_varejo_titulosPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort();
}
else         if ("sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort();
}
else         if ("sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort();
}
else         if ("sapiens_SyncMCWFUsersPort".equals(inputPortName)) {
            return getsapiens_SyncMCWFUsersPort();
}
else         if ("sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort".equals(inputPortName)) {
            return getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.senior.com.br", "g5-senior-services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_int_varejo_titulosPort"));
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort"));
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort"));
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_SyncMCWFUsersPort"));
            ports.add(new javax.xml.namespace.QName("http://services.senior.com.br", "sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("sapiens_Synccom_senior_g5_co_int_varejo_titulosPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_int_varejo_titulosPortEndpointAddress(address);
}
else if ("sapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortEndpointAddress(address);
}
else if ("sapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortEndpointAddress(address);
}
else if ("sapiens_SyncMCWFUsersPort".equals(portName)) {
            setsapiens_SyncMCWFUsersPortEndpointAddress(address);
}
else if ("sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort".equals(portName)) {
            setsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
