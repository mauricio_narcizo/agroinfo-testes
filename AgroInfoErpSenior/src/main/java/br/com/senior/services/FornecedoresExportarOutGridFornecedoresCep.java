/**
 * FornecedoresExportarOutGridFornecedoresCep.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportarOutGridFornecedoresCep  implements java.io.Serializable {
    private java.lang.String baiFor;

    private java.lang.Integer cepFor;

    private java.lang.Integer cepIni;

    private java.lang.String cidFor;

    private java.lang.String codPai;

    private java.lang.Integer codRai;

    private java.lang.String cplEnd;

    private java.lang.String endFor;

    private java.lang.String nenFor;

    private java.lang.String nomPai;

    private java.lang.String nomUfs;

    private java.lang.String sigUfs;

    public FornecedoresExportarOutGridFornecedoresCep() {
    }

    public FornecedoresExportarOutGridFornecedoresCep(
           java.lang.String baiFor,
           java.lang.Integer cepFor,
           java.lang.Integer cepIni,
           java.lang.String cidFor,
           java.lang.String codPai,
           java.lang.Integer codRai,
           java.lang.String cplEnd,
           java.lang.String endFor,
           java.lang.String nenFor,
           java.lang.String nomPai,
           java.lang.String nomUfs,
           java.lang.String sigUfs) {
           this.baiFor = baiFor;
           this.cepFor = cepFor;
           this.cepIni = cepIni;
           this.cidFor = cidFor;
           this.codPai = codPai;
           this.codRai = codRai;
           this.cplEnd = cplEnd;
           this.endFor = endFor;
           this.nenFor = nenFor;
           this.nomPai = nomPai;
           this.nomUfs = nomUfs;
           this.sigUfs = sigUfs;
    }


    /**
     * Gets the baiFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return baiFor
     */
    public java.lang.String getBaiFor() {
        return baiFor;
    }


    /**
     * Sets the baiFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param baiFor
     */
    public void setBaiFor(java.lang.String baiFor) {
        this.baiFor = baiFor;
    }


    /**
     * Gets the cepFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return cepFor
     */
    public java.lang.Integer getCepFor() {
        return cepFor;
    }


    /**
     * Sets the cepFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param cepFor
     */
    public void setCepFor(java.lang.Integer cepFor) {
        this.cepFor = cepFor;
    }


    /**
     * Gets the cepIni value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return cepIni
     */
    public java.lang.Integer getCepIni() {
        return cepIni;
    }


    /**
     * Sets the cepIni value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param cepIni
     */
    public void setCepIni(java.lang.Integer cepIni) {
        this.cepIni = cepIni;
    }


    /**
     * Gets the cidFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return cidFor
     */
    public java.lang.String getCidFor() {
        return cidFor;
    }


    /**
     * Sets the cidFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param cidFor
     */
    public void setCidFor(java.lang.String cidFor) {
        this.cidFor = cidFor;
    }


    /**
     * Gets the codPai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codRai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return codRai
     */
    public java.lang.Integer getCodRai() {
        return codRai;
    }


    /**
     * Sets the codRai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param codRai
     */
    public void setCodRai(java.lang.Integer codRai) {
        this.codRai = codRai;
    }


    /**
     * Gets the cplEnd value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the endFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return endFor
     */
    public java.lang.String getEndFor() {
        return endFor;
    }


    /**
     * Sets the endFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param endFor
     */
    public void setEndFor(java.lang.String endFor) {
        this.endFor = endFor;
    }


    /**
     * Gets the nenFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return nenFor
     */
    public java.lang.String getNenFor() {
        return nenFor;
    }


    /**
     * Sets the nenFor value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param nenFor
     */
    public void setNenFor(java.lang.String nenFor) {
        this.nenFor = nenFor;
    }


    /**
     * Gets the nomPai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return nomPai
     */
    public java.lang.String getNomPai() {
        return nomPai;
    }


    /**
     * Sets the nomPai value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param nomPai
     */
    public void setNomPai(java.lang.String nomPai) {
        this.nomPai = nomPai;
    }


    /**
     * Gets the nomUfs value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return nomUfs
     */
    public java.lang.String getNomUfs() {
        return nomUfs;
    }


    /**
     * Sets the nomUfs value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param nomUfs
     */
    public void setNomUfs(java.lang.String nomUfs) {
        this.nomUfs = nomUfs;
    }


    /**
     * Gets the sigUfs value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this FornecedoresExportarOutGridFornecedoresCep.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportarOutGridFornecedoresCep)) return false;
        FornecedoresExportarOutGridFornecedoresCep other = (FornecedoresExportarOutGridFornecedoresCep) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baiFor==null && other.getBaiFor()==null) || 
             (this.baiFor!=null &&
              this.baiFor.equals(other.getBaiFor()))) &&
            ((this.cepFor==null && other.getCepFor()==null) || 
             (this.cepFor!=null &&
              this.cepFor.equals(other.getCepFor()))) &&
            ((this.cepIni==null && other.getCepIni()==null) || 
             (this.cepIni!=null &&
              this.cepIni.equals(other.getCepIni()))) &&
            ((this.cidFor==null && other.getCidFor()==null) || 
             (this.cidFor!=null &&
              this.cidFor.equals(other.getCidFor()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codRai==null && other.getCodRai()==null) || 
             (this.codRai!=null &&
              this.codRai.equals(other.getCodRai()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.endFor==null && other.getEndFor()==null) || 
             (this.endFor!=null &&
              this.endFor.equals(other.getEndFor()))) &&
            ((this.nenFor==null && other.getNenFor()==null) || 
             (this.nenFor!=null &&
              this.nenFor.equals(other.getNenFor()))) &&
            ((this.nomPai==null && other.getNomPai()==null) || 
             (this.nomPai!=null &&
              this.nomPai.equals(other.getNomPai()))) &&
            ((this.nomUfs==null && other.getNomUfs()==null) || 
             (this.nomUfs!=null &&
              this.nomUfs.equals(other.getNomUfs()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaiFor() != null) {
            _hashCode += getBaiFor().hashCode();
        }
        if (getCepFor() != null) {
            _hashCode += getCepFor().hashCode();
        }
        if (getCepIni() != null) {
            _hashCode += getCepIni().hashCode();
        }
        if (getCidFor() != null) {
            _hashCode += getCidFor().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodRai() != null) {
            _hashCode += getCodRai().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getEndFor() != null) {
            _hashCode += getEndFor().hashCode();
        }
        if (getNenFor() != null) {
            _hashCode += getNenFor().hashCode();
        }
        if (getNomPai() != null) {
            _hashCode += getNomPai().hashCode();
        }
        if (getNomUfs() != null) {
            _hashCode += getNomUfs().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportarOutGridFornecedoresCep.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCep"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
