/**
 * Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub extends org.apache.axis.client.Stub implements br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarIn"), br.com.senior.services.FornecedoresExportarIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOut"));
        oper.setReturnClass(br.com.senior.services.FornecedoresExportarOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Importar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarIn"), br.com.senior.services.FornecedoresImportarIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOut"));
        oper.setReturnClass(br.com.senior.services.FornecedoresImportarOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2In"), br.com.senior.services.FornecedoresExportar2In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2Out"));
        oper.setReturnClass(br.com.senior.services.FornecedoresExportar2Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2InGridConsulta");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2InGridConsulta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedores");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedores.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresCampoUsuario");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresCampoUsuario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresCaracteristicas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresCaracteristicas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresCep");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresCep.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresCNAE");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresCNAE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresConBan");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresConBan.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresContatos");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresContatos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresDefHis");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresDefHis.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresForPag");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresForPag.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresINSS");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresINSS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresObservacoes");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresObservacoes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresOriMer");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportar2OutGridFornecedoresOriMer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarInGridConsulta");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarInGridConsulta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedores");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedores.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCampoUsuario");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresCampoUsuario.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCaracteristicas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresCaracteristicas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCep");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresCep.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresCNAE");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresCNAE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresConBan");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresConBan.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresContatos");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresContatos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresDefHis");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresDefHis.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresForPag");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresForPag.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresINSS");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresINSS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresObservacoes");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresObservacoes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportarOutGridFornecedoresOriMer");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresExportarOutGridFornecedoresOriMer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarInFornecedor");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarInFornecedor.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarInFornecedorDefinicao");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarInFornecedorDefinicao.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOutRetorno");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarOutRetorno.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOutRetornoDetalhe");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.FornecedoresImportarOutRetornoDetalhe.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public br.com.senior.services.FornecedoresExportarOut exportar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresExportarIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.FornecedoresExportarOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.FornecedoresExportarOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.FornecedoresExportarOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.FornecedoresImportarOut importar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresImportarIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Importar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.FornecedoresImportarOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.FornecedoresImportarOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.FornecedoresImportarOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.FornecedoresExportar2Out exportar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresExportar2In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.FornecedoresExportar2Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.FornecedoresExportar2Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.FornecedoresExportar2Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
