/**
 * TitulosConsultarTitulosCPIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosConsultarTitulosCPIn  implements java.io.Serializable {
    private java.lang.String cgcCpf;

    private java.lang.String codEmp;

    private java.lang.String codFor;

    private java.lang.String filExc;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String identificacaoSistema;

    private java.lang.String sitTit;

    private java.lang.String vctFim;

    private java.lang.String vctIni;

    public TitulosConsultarTitulosCPIn() {
    }

    public TitulosConsultarTitulosCPIn(
           java.lang.String cgcCpf,
           java.lang.String codEmp,
           java.lang.String codFor,
           java.lang.String filExc,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String identificacaoSistema,
           java.lang.String sitTit,
           java.lang.String vctFim,
           java.lang.String vctIni) {
           this.cgcCpf = cgcCpf;
           this.codEmp = codEmp;
           this.codFor = codFor;
           this.filExc = filExc;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.identificacaoSistema = identificacaoSistema;
           this.sitTit = sitTit;
           this.vctFim = vctFim;
           this.vctIni = vctIni;
    }


    /**
     * Gets the cgcCpf value for this TitulosConsultarTitulosCPIn.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this TitulosConsultarTitulosCPIn.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the codEmp value for this TitulosConsultarTitulosCPIn.
     * 
     * @return codEmp
     */
    public java.lang.String getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosConsultarTitulosCPIn.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.String codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFor value for this TitulosConsultarTitulosCPIn.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosConsultarTitulosCPIn.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the filExc value for this TitulosConsultarTitulosCPIn.
     * 
     * @return filExc
     */
    public java.lang.String getFilExc() {
        return filExc;
    }


    /**
     * Sets the filExc value for this TitulosConsultarTitulosCPIn.
     * 
     * @param filExc
     */
    public void setFilExc(java.lang.String filExc) {
        this.filExc = filExc;
    }


    /**
     * Gets the flowInstanceID value for this TitulosConsultarTitulosCPIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosConsultarTitulosCPIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosConsultarTitulosCPIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosConsultarTitulosCPIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the identificacaoSistema value for this TitulosConsultarTitulosCPIn.
     * 
     * @return identificacaoSistema
     */
    public java.lang.String getIdentificacaoSistema() {
        return identificacaoSistema;
    }


    /**
     * Sets the identificacaoSistema value for this TitulosConsultarTitulosCPIn.
     * 
     * @param identificacaoSistema
     */
    public void setIdentificacaoSistema(java.lang.String identificacaoSistema) {
        this.identificacaoSistema = identificacaoSistema;
    }


    /**
     * Gets the sitTit value for this TitulosConsultarTitulosCPIn.
     * 
     * @return sitTit
     */
    public java.lang.String getSitTit() {
        return sitTit;
    }


    /**
     * Sets the sitTit value for this TitulosConsultarTitulosCPIn.
     * 
     * @param sitTit
     */
    public void setSitTit(java.lang.String sitTit) {
        this.sitTit = sitTit;
    }


    /**
     * Gets the vctFim value for this TitulosConsultarTitulosCPIn.
     * 
     * @return vctFim
     */
    public java.lang.String getVctFim() {
        return vctFim;
    }


    /**
     * Sets the vctFim value for this TitulosConsultarTitulosCPIn.
     * 
     * @param vctFim
     */
    public void setVctFim(java.lang.String vctFim) {
        this.vctFim = vctFim;
    }


    /**
     * Gets the vctIni value for this TitulosConsultarTitulosCPIn.
     * 
     * @return vctIni
     */
    public java.lang.String getVctIni() {
        return vctIni;
    }


    /**
     * Sets the vctIni value for this TitulosConsultarTitulosCPIn.
     * 
     * @param vctIni
     */
    public void setVctIni(java.lang.String vctIni) {
        this.vctIni = vctIni;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosCPIn)) return false;
        TitulosConsultarTitulosCPIn other = (TitulosConsultarTitulosCPIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.filExc==null && other.getFilExc()==null) || 
             (this.filExc!=null &&
              this.filExc.equals(other.getFilExc()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.identificacaoSistema==null && other.getIdentificacaoSistema()==null) || 
             (this.identificacaoSistema!=null &&
              this.identificacaoSistema.equals(other.getIdentificacaoSistema()))) &&
            ((this.sitTit==null && other.getSitTit()==null) || 
             (this.sitTit!=null &&
              this.sitTit.equals(other.getSitTit()))) &&
            ((this.vctFim==null && other.getVctFim()==null) || 
             (this.vctFim!=null &&
              this.vctFim.equals(other.getVctFim()))) &&
            ((this.vctIni==null && other.getVctIni()==null) || 
             (this.vctIni!=null &&
              this.vctIni.equals(other.getVctIni())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getFilExc() != null) {
            _hashCode += getFilExc().hashCode();
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getIdentificacaoSistema() != null) {
            _hashCode += getIdentificacaoSistema().hashCode();
        }
        if (getSitTit() != null) {
            _hashCode += getSitTit().hashCode();
        }
        if (getVctFim() != null) {
            _hashCode += getVctFim().hashCode();
        }
        if (getVctIni() != null) {
            _hashCode += getVctIni().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosCPIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosCPIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filExc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filExc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificacaoSistema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identificacaoSistema"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctIni");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctIni"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
