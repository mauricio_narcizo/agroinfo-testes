/**
 * Sapiens_SyncMCWFUsers.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Sapiens_SyncMCWFUsers extends java.rmi.Remote {
    public br.com.senior.services.McwfUsersAuthenticateJAASOut authenticateJAAS(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersAuthenticateJAASIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersDiscoverUsersGroupsOut discoverUsersGroups(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersDiscoverUsersGroupsIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersUserReplicationOut userReplication(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersUserReplicationIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersChangePasswordOut changePassword(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersChangePasswordIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersGetPersonKindOut getPersonKind(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersGetPersonKindIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersPersonExistsOut personExists(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersPersonExistsIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.McwfUsersGetUserGroupsOut getUserGroups(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.McwfUsersGetUserGroupsIn parameters) throws java.rmi.RemoteException;
}
