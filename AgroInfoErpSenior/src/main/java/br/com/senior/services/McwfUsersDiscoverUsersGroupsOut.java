/**
 * McwfUsersDiscoverUsersGroupsOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class McwfUsersDiscoverUsersGroupsOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String pmReturn;

    public McwfUsersDiscoverUsersGroupsOut() {
    }

    public McwfUsersDiscoverUsersGroupsOut(
           java.lang.String erroExecucao,
           java.lang.String pmReturn) {
           this.erroExecucao = erroExecucao;
           this.pmReturn = pmReturn;
    }


    /**
     * Gets the erroExecucao value for this McwfUsersDiscoverUsersGroupsOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this McwfUsersDiscoverUsersGroupsOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the pmReturn value for this McwfUsersDiscoverUsersGroupsOut.
     * 
     * @return pmReturn
     */
    public java.lang.String getPmReturn() {
        return pmReturn;
    }


    /**
     * Sets the pmReturn value for this McwfUsersDiscoverUsersGroupsOut.
     * 
     * @param pmReturn
     */
    public void setPmReturn(java.lang.String pmReturn) {
        this.pmReturn = pmReturn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof McwfUsersDiscoverUsersGroupsOut)) return false;
        McwfUsersDiscoverUsersGroupsOut other = (McwfUsersDiscoverUsersGroupsOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.pmReturn==null && other.getPmReturn()==null) || 
             (this.pmReturn!=null &&
              this.pmReturn.equals(other.getPmReturn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getPmReturn() != null) {
            _hashCode += getPmReturn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(McwfUsersDiscoverUsersGroupsOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "mcwfUsersDiscoverUsersGroupsOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
