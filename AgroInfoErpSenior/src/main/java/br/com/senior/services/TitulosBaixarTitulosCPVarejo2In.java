/**
 * TitulosBaixarTitulosCPVarejo2In.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosBaixarTitulosCPVarejo2In  implements java.io.Serializable {
    private br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar[] baixaTituloPagar;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String sistemaIntegracao;

    public TitulosBaixarTitulosCPVarejo2In() {
    }

    public TitulosBaixarTitulosCPVarejo2In(
           br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar[] baixaTituloPagar,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String sistemaIntegracao) {
           this.baixaTituloPagar = baixaTituloPagar;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.sistemaIntegracao = sistemaIntegracao;
    }


    /**
     * Gets the baixaTituloPagar value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @return baixaTituloPagar
     */
    public br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar[] getBaixaTituloPagar() {
        return baixaTituloPagar;
    }


    /**
     * Sets the baixaTituloPagar value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @param baixaTituloPagar
     */
    public void setBaixaTituloPagar(br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar[] baixaTituloPagar) {
        this.baixaTituloPagar = baixaTituloPagar;
    }

    public br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar getBaixaTituloPagar(int i) {
        return this.baixaTituloPagar[i];
    }

    public void setBaixaTituloPagar(int i, br.com.senior.services.TitulosBaixarTitulosCPVarejo2InBaixaTituloPagar _value) {
        this.baixaTituloPagar[i] = _value;
    }


    /**
     * Gets the flowInstanceID value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the sistemaIntegracao value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @return sistemaIntegracao
     */
    public java.lang.String getSistemaIntegracao() {
        return sistemaIntegracao;
    }


    /**
     * Sets the sistemaIntegracao value for this TitulosBaixarTitulosCPVarejo2In.
     * 
     * @param sistemaIntegracao
     */
    public void setSistemaIntegracao(java.lang.String sistemaIntegracao) {
        this.sistemaIntegracao = sistemaIntegracao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosBaixarTitulosCPVarejo2In)) return false;
        TitulosBaixarTitulosCPVarejo2In other = (TitulosBaixarTitulosCPVarejo2In) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baixaTituloPagar==null && other.getBaixaTituloPagar()==null) || 
             (this.baixaTituloPagar!=null &&
              java.util.Arrays.equals(this.baixaTituloPagar, other.getBaixaTituloPagar()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.sistemaIntegracao==null && other.getSistemaIntegracao()==null) || 
             (this.sistemaIntegracao!=null &&
              this.sistemaIntegracao.equals(other.getSistemaIntegracao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaixaTituloPagar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBaixaTituloPagar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBaixaTituloPagar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getSistemaIntegracao() != null) {
            _hashCode += getSistemaIntegracao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosBaixarTitulosCPVarejo2In.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixarTitulosCPVarejo2In"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baixaTituloPagar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baixaTituloPagar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixarTitulosCPVarejo2InBaixaTituloPagar"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sistemaIntegracao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sistemaIntegracao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
