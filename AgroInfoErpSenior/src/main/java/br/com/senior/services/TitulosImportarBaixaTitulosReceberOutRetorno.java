/**
 * TitulosImportarBaixaTitulosReceberOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosImportarBaixaTitulosReceberOutRetorno  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codTpt;

    private java.lang.Integer empReq;

    private java.lang.Integer filReq;

    private java.lang.Integer ideExt;

    private java.lang.String msgRet;

    private java.lang.String numTit;

    private java.lang.Integer tipRet;

    public TitulosImportarBaixaTitulosReceberOutRetorno() {
    }

    public TitulosImportarBaixaTitulosReceberOutRetorno(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codTpt,
           java.lang.Integer empReq,
           java.lang.Integer filReq,
           java.lang.Integer ideExt,
           java.lang.String msgRet,
           java.lang.String numTit,
           java.lang.Integer tipRet) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codTpt = codTpt;
           this.empReq = empReq;
           this.filReq = filReq;
           this.ideExt = ideExt;
           this.msgRet = msgRet;
           this.numTit = numTit;
           this.tipRet = tipRet;
    }


    /**
     * Gets the codEmp value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codTpt value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the empReq value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return empReq
     */
    public java.lang.Integer getEmpReq() {
        return empReq;
    }


    /**
     * Sets the empReq value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param empReq
     */
    public void setEmpReq(java.lang.Integer empReq) {
        this.empReq = empReq;
    }


    /**
     * Gets the filReq value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return filReq
     */
    public java.lang.Integer getFilReq() {
        return filReq;
    }


    /**
     * Sets the filReq value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param filReq
     */
    public void setFilReq(java.lang.Integer filReq) {
        this.filReq = filReq;
    }


    /**
     * Gets the ideExt value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the msgRet value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the numTit value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the tipRet value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this TitulosImportarBaixaTitulosReceberOutRetorno.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosImportarBaixaTitulosReceberOutRetorno)) return false;
        TitulosImportarBaixaTitulosReceberOutRetorno other = (TitulosImportarBaixaTitulosReceberOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.empReq==null && other.getEmpReq()==null) || 
             (this.empReq!=null &&
              this.empReq.equals(other.getEmpReq()))) &&
            ((this.filReq==null && other.getFilReq()==null) || 
             (this.filReq!=null &&
              this.filReq.equals(other.getFilReq()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getEmpReq() != null) {
            _hashCode += getEmpReq().hashCode();
        }
        if (getFilReq() != null) {
            _hashCode += getFilReq().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosImportarBaixaTitulosReceberOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosImportarBaixaTitulosReceberOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empReq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filReq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filReq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
