/**
 * G5SeniorServices.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface G5SeniorServices extends javax.xml.rpc.Service {
    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_titulosPortAddress();

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_titulos getsapiens_Synccom_senior_g5_co_int_varejo_titulosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPortAddress();

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo getsapiens_Synccom_senior_g5_co_int_varejo_tipotituloPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPortAddress();

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra getsapiens_Synccom_senior_g5_co_mcm_est_ordemcompraPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getsapiens_SyncMCWFUsersPortAddress();

    public br.com.senior.services.Sapiens_SyncMCWFUsers getsapiens_SyncMCWFUsersPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.Sapiens_SyncMCWFUsers getsapiens_SyncMCWFUsersPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPortAddress();

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort() throws javax.xml.rpc.ServiceException;

    public br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores getsapiens_Synccom_senior_g5_co_int_varejo_fornecedoresPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
