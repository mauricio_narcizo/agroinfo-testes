/**
 * Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Sapiens_Synccom_senior_g5_co_mcm_est_ordemcompra extends java.rmi.Remote {
    public br.com.senior.services.OrdemcomprabuscarPendentesOut buscarPendentes(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.OrdemcomprabuscarPendentesIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.OrdemcompraaprovarOut aprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.OrdemcompraaprovarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.OrdemcomprareprovarOut reprovar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.OrdemcomprareprovarIn parameters) throws java.rmi.RemoteException;
}
