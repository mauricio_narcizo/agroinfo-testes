package br.com.senior.agroinfo.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.com.senior.agroinfo.db.enums.TipoSMS;
import br.com.senior.agroinfo.db.enums.TipoSistema;

@Entity
@Table(name = "configuracoes")
public class Configuracoes implements BaseEntityId {
	
    private static final long serialVersionUID = -9074999203767290134L;

    @Id
    @Column(name = "id", unique = true, nullable = false)    
    private Long id;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tiposistema", nullable=false)    
    private TipoSistema tiposistema;
    
    @Column(name = "endereco")
    private String endereco;
    
    @Column(name = "porta")
    private String porta;
    
    @Column(name = "usuario")
    private String usuario;
    
    @Column(name = "senha")
    private String senha;
    
    @Column(name = "sigla")
    private String sigla;
    
    @Column(name = "ipsms")
    private String ipsms;
    
    @Column(name = "utilizasms", nullable = false)
    private boolean utilizasms;
    
    @Column(name = "portasms")
    private String portasms;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tiposms", nullable=false)   
    private TipoSMS tiposms;
    
    @Column(name = "usuariosms")
    private String usuariosms;
    
    @Column(name = "senhasms")
    private String senhasms;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dtupdate", nullable = false, length = 29)
    private Date dtUpdate;

    public Configuracoes() {
    }

    public Configuracoes(TipoSistema tiposistema, String endereco, String porta, String usuario, String senha, String sigla,
            String ipsms, boolean utilizasms, String portasms, TipoSMS tiposms, String usuariosms, String senhasms,
            Date dtUpdate) {
        this.tiposistema = tiposistema;
        this.endereco = endereco;
        this.porta = porta;
        this.usuario = usuario;
        this.senha = senha;
        this.sigla = sigla;
        this.ipsms = ipsms;
        this.utilizasms = utilizasms;
        this.portasms = portasms;
        this.tiposms = tiposms;
        this.usuariosms = usuariosms;
        this.senhasms = senhasms;
        this.dtUpdate = dtUpdate;
    }

    public Configuracoes(Long id) {
    	this.id = id;
	}

	@Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    public TipoSistema getTiposistema() {
        return this.tiposistema;
    }

    public void setTiposistema(TipoSistema tiposistema) {
        this.tiposistema = tiposistema;
    }
    
    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    public String getPorta() {
        return this.porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }
    
    public String getUsuario() {
        return this.usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String getSenha() {
        return this.senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public String getSigla() {
        return this.sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    
    public String getIpsms() {
        return this.ipsms;
    }

    public void setIpsms(String ipsms) {
        this.ipsms = ipsms;
    }
    
    public boolean isUtilizasms() {
        return this.utilizasms;
    }

    public void setUtilizasms(boolean utilizasms) {
        this.utilizasms = utilizasms;
    }
    
    public String getPortasms() {
        return this.portasms;
    }

    public void setPortasms(String portasms) {
        this.portasms = portasms;
    }
    
    public TipoSMS getTiposms() {
        return this.tiposms;
    }

    public void setTiposms(TipoSMS tiposms) {
        this.tiposms = tiposms;
    }
    
    public String getUsuariosms() {
        return this.usuariosms;
    }

    public void setUsuariosms(String usuariosms) {
        this.usuariosms = usuariosms;
    }
    
    public String getSenhasms() {
        return this.senhasms;
    }

    public void setSenhasms(String senhasms) {
        this.senhasms = senhasms;
    }

    public Date getDtUpdate() {
        return this.dtUpdate;
    }

    public void setDtUpdate(Date dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        if (!(other instanceof Configuracoes))
            return false;
        Configuracoes castOther = (Configuracoes) other;

        return this.getTiposistema() == castOther.getTiposistema()
                && ((this.getEndereco() == castOther.getEndereco()) || (this.getEndereco() != null
                        && castOther.getEndereco() != null && this.getEndereco().equals(castOther.getEndereco())))
                && ((this.getPorta() == castOther.getPorta()) || (this.getPorta() != null
                        && castOther.getPorta() != null && this.getPorta().equals(castOther.getPorta())))
                && ((this.getUsuario() == castOther.getUsuario()) || (this.getUsuario() != null
                        && castOther.getUsuario() != null && this.getUsuario().equals(castOther.getUsuario())))
                && ((this.getSenha() == castOther.getSenha()) || (this.getSenha() != null
                        && castOther.getSenha() != null && this.getSenha().equals(castOther.getSenha())))
                && ((this.getSigla() == castOther.getSigla()) || (this.getSigla() != null
                        && castOther.getSigla() != null && this.getSigla().equals(castOther.getSigla())))
                && ((this.getIpsms() == castOther.getIpsms()) || (this.getIpsms() != null
                        && castOther.getIpsms() != null && this.getIpsms().equals(castOther.getIpsms())))
                && (this.isUtilizasms() == castOther.isUtilizasms())
                && ((this.getPortasms() == castOther.getPortasms()) || (this.getPortasms() != null
                        && castOther.getPortasms() != null && this.getPortasms().equals(castOther.getPortasms())))
                && ((this.getTiposms() == castOther.getTiposms()) || (this.getTiposms() != null
                        && castOther.getTiposms() != null && this.getTiposms().equals(castOther.getTiposms())))
                && ((this.getUsuariosms() == castOther.getUsuariosms()) || (this.getUsuariosms() != null
                        && castOther.getUsuariosms() != null && this.getUsuariosms().equals(castOther.getUsuariosms())))
                && ((this.getSenhasms() == castOther.getSenhasms()) || (this.getSenhasms() != null
                        && castOther.getSenhasms() != null && this.getSenhasms().equals(castOther.getSenhasms())))
                && ((this.getDtUpdate() == castOther.getDtUpdate()) || (this.getDtUpdate() != null
                        && castOther.getDtUpdate() != null && this.getDtUpdate().equals(castOther.getDtUpdate())));
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 37 * result + (this.getTiposistema() == null ? 0 : this.getTiposistema().hashCode());
        result = 37 * result + (getEndereco() == null ? 0 : this.getEndereco().hashCode());
        result = 37 * result + (getPorta() == null ? 0 : this.getPorta().hashCode());
        result = 37 * result + (getUsuario() == null ? 0 : this.getUsuario().hashCode());
        result = 37 * result + (getSenha() == null ? 0 : this.getSenha().hashCode());
        result = 37 * result + (getSigla() == null ? 0 : this.getSigla().hashCode());
        result = 37 * result + (getIpsms() == null ? 0 : this.getIpsms().hashCode());
        result = 37 * result + (this.isUtilizasms() ? 1 : 0);
        result = 37 * result + (getPortasms() == null ? 0 : this.getPortasms().hashCode());
        result = 37 * result + (getTiposms() == null ? 0 : this.getTiposms().hashCode());
        result = 37 * result + (getUsuariosms() == null ? 0 : this.getUsuariosms().hashCode());
        result = 37 * result + (getSenhasms() == null ? 0 : this.getSenhasms().hashCode());
        result = 37 * result + (getDtUpdate() == null ? 0 : this.getDtUpdate().hashCode());
        return result;
    }

}
