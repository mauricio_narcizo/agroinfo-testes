package br.com.senior.agroinfo.db.enums;

import br.com.senior.agroinfo.exception.EnumNotFoundException;

public enum TipoSistema {
	
    SENIOR_ERP_587("Senior ERP 5.8.7"), 
    SENIOR_ERP_588("Senior ERP 5.8.8");
    
    private final String descricao;

    private TipoSistema(final String descricao) {
        this.descricao = descricao;
    }

	public String getDescricao() {
		return descricao;
	}	
	
	public static TipoSistema getByDescricao(String descricao) throws EnumNotFoundException {
		for (TipoSistema t: TipoSistema.values()){
			if (t.getDescricao().equals(descricao)) {
				return t;
			}
		}
		throw new EnumNotFoundException(String.format("Tipo Sistema %s não encontrado.", descricao));
	}

}
