package br.com.senior.agroinfo.db.modal;

import java.util.Date;

// Generated 09/10/2015 15:53:13 by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Mensagens generated by hbm2java
 */
@Entity
@Table(name = "mensagens", schema = "public")
public class Mensagem implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Date dtVisualizacao;
    private Date dtUpdate;
    private long idMensagem;
    private Long id;
    @ManyToOne
    private Cooperado cooperado;
    @ManyToOne
    private Usuario usuario;

    public Mensagem() {
    }

    public Mensagem(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cooperado getCooperado() {
        return this.cooperado;
    }

    public void setCooperado(Cooperado cooperado) {
        this.cooperado = cooperado;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "dtvisualizacao", length = 29)
    public Date getDtVisualizacao() {
        return this.dtVisualizacao;
    }

    public void setDtVisualizacao(Date dtVisualizacao) {
        this.dtVisualizacao = dtVisualizacao;
    }

    @Column(name = "dtupdate", nullable = false, length = 29)
    public Date getDtupdate() {
        return this.dtUpdate;
    }

    public void setDtupdate(Date dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    @Column(name = "idmensagem", nullable = false)
    public long getIdMensagem() {
        return this.idMensagem;
    }

    public void setIdMensagem(long idMensagem) {
        this.idMensagem = idMensagem;
    }

}
