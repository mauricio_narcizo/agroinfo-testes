package br.com.senior.agroinfo.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Contato generated by hbm2java
 */
@Entity
@Table(name = "contato")
public class Contato implements BaseEntityId {

    private static final long serialVersionUID = -100964526254029854L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idcooperado", nullable = false)    
    private Cooperado cooperado;
    
    @Column(name = "ddd", nullable = false, length = 3)    
    private String ddd;
    
    @Column(name = "telefone", nullable = false, length = 15)    
    private String telefone;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dtupdate", nullable = false, length = 29)    
    private Date dtUpdate;
    
    @Column(name = "ativo")    
    private boolean ativo;

    public Contato() {
    }

    public Contato(long id, Cooperado cooperado, String ddd, String telefone, Date dtupdate, boolean ativo) {
        this.id = id;
        this.cooperado = cooperado;
        this.ddd = ddd;
        this.telefone = telefone;
        this.dtUpdate = dtupdate;
        this.setAtivo(ativo);
    }

    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Cooperado getCooperado() {
        return this.cooperado;
    }

    public void setCooperado(Cooperado cooperado) {
        this.cooperado = cooperado;
    }

    public String getDdd() {
        return this.ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Date getDtUpdate() {
        return this.dtUpdate;
    }

    public void setDtUpdate(Date dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    @PrePersist
    @PreUpdate
    protected void updateOrCreate() {
        dtUpdate = new Date();
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contato other = (Contato) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
    
}
