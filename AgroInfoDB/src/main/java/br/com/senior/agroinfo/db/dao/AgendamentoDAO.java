package br.com.senior.agroinfo.db.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.senior.agroinfo.db.modal.Agendamento;

@Stateless
public class AgendamentoDAO extends GenericDAO<Agendamento> {

	public List<Agendamento> getListaAtivo() {
		CriteriaBuilder criteria = em.getCriteriaBuilder();
		CriteriaQuery<Agendamento> query = criteria.createQuery(Agendamento.class);
		Root<Agendamento> from = query.from(Agendamento.class);
		Predicate predicate = criteria.isTrue(from.get("ativo"));
		query.select(from).where(predicate);
		TypedQuery<Agendamento> result = em.createQuery(query);
		return result.getResultList();
	}	
	
}