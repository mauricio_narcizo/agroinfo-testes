package br.com.senior.agroinfo.db.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.senior.agroinfo.db.modal.BaseEntity;
import br.com.senior.agroinfo.db.modal.BaseEntityId;

public class GenericDAO<T extends BaseEntity> implements InterfaceDAO<T>{

	@PersistenceContext
	protected EntityManager em;

	public T adiciona(T entity) {
		if (isEntityValid(entity)) {
			this.em.persist(entity);
		}
		return entity;
	}

	public void remove(T entity, Object id) {
		Optional<T> optional = busca(id);
		optional.ifPresent(e -> em.remove(e));	
	}
	
	public void remove(BaseEntityId entity) {
		Optional<T> optional = busca(entity.getId());
		optional.ifPresent(e -> em.remove(e));	
	}	

	public Optional<T> busca(Object id) {
		Objects.requireNonNull(id);
		return (Optional<T>) Optional.ofNullable(this.em.find(getEntityBeanType(), id));
	}
			

	public T atualiza(T entity) {
		return this.em.merge(entity);
	}

   	@SuppressWarnings("unchecked")
	public List<T> getLista() {
		List<T> t = this.em.createQuery("from " + getEntityBeanType().getSimpleName()).getResultList();
		return (List<T>) t;
	}

    @SuppressWarnings("unchecked")
	protected Class<T> getEntityBeanType() {
    	 return (Class<T>) ((ParameterizedType)(super.getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }    
	
	public Boolean isEntityValid(T entity) {
		return true;
	}

}