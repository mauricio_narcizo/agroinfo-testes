package br.com.senior.agroinfo.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import br.com.senior.agroinfo.db.enums.FormatoPeriodicidade;
import br.com.senior.agroinfo.db.enums.TipoAgendamento;

@Entity
@Table(
        name="agendamento",
        uniqueConstraints=
            @UniqueConstraint(columnNames={"tipoagendamento"})
    )
public class Agendamento implements BaseEntityId {

    private static final long serialVersionUID = 2788937702594571108L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)    
    private Long id;
    
    private Integer periodicidade;
    
    private Long tempoUltimaExecucao;
    
    private Integer cargaCompleta;
    
    private boolean ativo;
    
    private boolean executando;
    
    private String grupo;
    
    private Date dtUpdate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipoperiodicidade", nullable=false)
    private FormatoPeriodicidade tipoPeriodicidade;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipocargacompleta", nullable=false)
    private FormatoPeriodicidade tipoCargaCompleta;
    
    private Integer tempoCargaCompleta;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipoagendamento", nullable=false, unique = true)
    private TipoAgendamento tipoAgendamento;

    public Agendamento() {
    }

    private Agendamento(Builder builder) {
        this.id = builder.id;
        this.periodicidade = builder.periodicidade;
        this.tempoUltimaExecucao = builder.tempoUltimaExecucao;
        this.cargaCompleta = builder.cargaCompleta;
        this.ativo = builder.ativo;
        this.executando = builder.executando;
        this.grupo = builder.grupo;
        this.dtUpdate = builder.dtUpdate;
        this.tipoPeriodicidade = builder.tipoPeriodicidade;
        this.tipoCargaCompleta = builder.tipoCargaCompleta;
        this.tempoCargaCompleta = builder.tempoCargaCompleta;
        this.tipoAgendamento = builder.tipoAgendamento;
	}

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "periodicidade")
    public Integer getPeriodicidade() {
        return this.periodicidade;
    }

    public void setPeriodicidade(Integer periodicidade) {
        this.periodicidade = periodicidade;
    }

    @Column(name = "tempoultimaexecucao")
    public Long getTempoUltimaExecucao() {
        return this.tempoUltimaExecucao;
    }

    public void setTempoUltimaExecucao(Long tempoUltimaExecucao) {
        this.tempoUltimaExecucao = tempoUltimaExecucao;
    }

    @Column(name = "cargacompleta")
    public Integer getCargaCompleta() {
        return this.cargaCompleta;
    }

    public void setCargaCompleta(Integer cargaCompleta) {
        this.cargaCompleta = cargaCompleta;
    }

    @Column(name = "ativo", nullable = false)
    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Column(name = "executando", nullable = false)
    public boolean isExecutando() {
        return this.executando;
    }

    public void setExecutando(boolean executando) {
        this.executando = executando;
    }

    @Column(name = "grupo", length = 255)
    public String getGrupo() {
        return this.grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dtupdate", nullable = false, length = 29)
    public Date getDtUpdate() {
        return this.dtUpdate;
    }

    public void setDtUpdate(Date dtUpdate) {
        this.dtUpdate = dtUpdate;
    }
    
    public FormatoPeriodicidade getTipoPeriodicidade() {
        return this.tipoPeriodicidade;
    }

    public void setTipoPeriodicidade(FormatoPeriodicidade tipoPeriodicidade) {
        this.tipoPeriodicidade = tipoPeriodicidade;
    }

    
    public FormatoPeriodicidade getTipoCargaCompleta() {
        return this.tipoCargaCompleta;
    }

    public void setTipoCargaCompleta(FormatoPeriodicidade tipoCargaCompleta) {
        this.tipoCargaCompleta = tipoCargaCompleta;
    }

    @Column(name = "tempocargacompleta")
    public Integer getTempoCargaCompleta() {
        return this.tempoCargaCompleta;
    }

    public void setTempoCargaCompleta(Integer tempoCargaCompleta) {
        this.tempoCargaCompleta = tempoCargaCompleta;
    }

	public TipoAgendamento getTipoAgendamento() {
		return tipoAgendamento;
	}

	public void setTipoAgendamento(TipoAgendamento tipoAgendamento) {
		this.tipoAgendamento = tipoAgendamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agendamento other = (Agendamento) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public static class Builder {
	    
		Long id;	    
	    protected Integer periodicidade;	    
	    protected Long tempoUltimaExecucao;	    
	    protected Integer cargaCompleta;	    
	    protected boolean ativo;	    
	    protected boolean executando;	    
	    protected String grupo;	   
	    protected Date dtUpdate;
	    protected FormatoPeriodicidade tipoPeriodicidade;
	    protected FormatoPeriodicidade tipoCargaCompleta;    
	    protected Integer tempoCargaCompleta;
	    protected TipoAgendamento tipoAgendamento;

        public Builder id(Long val) { 
        	this.id = val;      
            return this; 
        }
        
        public Builder periodicidade(Integer val) { 
        	this.periodicidade = val;           
        	return this; 
        }
        
        public Builder tempoUltimaExecucao(Long val) { 
        	this.tempoUltimaExecucao = val;  
        	return this; 
        }
        
        public Builder cargaCompleta(Integer val) { 
        	this.cargaCompleta = val;        
        	return this; 
        }
        
        public Builder ativo(boolean val) { 
        	this.ativo = val;        
        	return this; 
        }
        
        public Builder executando(boolean val) { 
        	this.executando = val;        
        	return this; 
        }
        
        public Builder grupo(String val) { 
        	this.grupo = val;        
        	return this; 
        }
        
        public Builder dtUpdate(Date val) { 
        	this.dtUpdate = val;        
        	return this; 
        }
        
        public Builder tipoPeriodicidade(FormatoPeriodicidade val) { 
        	this.tipoPeriodicidade = val;
        	return this; 
        }
        
        public Builder tipoCargaCompleta(FormatoPeriodicidade val) { 
        	this.tipoCargaCompleta = val;
        	return this; 
        }
        
        public Builder tempoCargaCompleta(Integer val) { 
        	this.tempoCargaCompleta = val;        
        	return this; 
        }
        
        public Builder tipoAgendamento(TipoAgendamento val) { 
        	this.tipoAgendamento = val;        
        	return this; 
        }                

        public Agendamento build() {
            return new Agendamento(this);
        }
    }	

}
