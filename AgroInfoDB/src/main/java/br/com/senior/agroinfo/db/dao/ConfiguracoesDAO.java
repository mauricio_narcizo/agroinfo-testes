package br.com.senior.agroinfo.db.dao;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.senior.agroinfo.db.modal.Configuracoes;

public class ConfiguracoesDAO extends GenericDAO<Configuracoes> {

    /**
     * 
     * @return obtem {@link Configuracoes} do sistema da filial
     */
    public Optional<Configuracoes> retrieve() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Configuracoes> query = builder.createQuery(Configuracoes.class);
        Root<Configuracoes> from = query.from(Configuracoes.class);

        Optional<Configuracoes> optional = Optional.empty();
        try {
            optional = Optional.ofNullable(em.createQuery(query.select(from)).getSingleResult());
        } catch (NoResultException e) {
            Logger.getLogger("ConfiguracoesDAO").log(Level.SEVERE, e.getMessage());
        }
        return optional;
    }

}
