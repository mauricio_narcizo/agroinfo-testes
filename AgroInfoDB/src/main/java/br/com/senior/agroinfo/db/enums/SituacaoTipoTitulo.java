package br.com.senior.agroinfo.db.enums;

import br.com.senior.agroinfo.exception.EnumNotFoundException;

public enum SituacaoTipoTitulo {
	
    SomaDuplicatas("D"),
    SomaOutrosTitulos("O"),
    SomaCredito("C");

    private final String sigla;

    private SituacaoTipoTitulo(String sigla) {       
        this.sigla = sigla;
    }

    public String getSigla() {
    	return sigla;
    }	

    public static SituacaoTipoTitulo obtemTipoContaPelaSigla(String sigla) throws EnumNotFoundException {
    	switch (sigla) {
		case "C": return SituacaoTipoTitulo.SomaCredito;
		case "D": return SituacaoTipoTitulo.SomaDuplicatas;
		case "O": return SituacaoTipoTitulo.SomaOutrosTitulos;
		default:
			throw new EnumNotFoundException("situacao tipo titulo "+sigla+" não encontrado");
		}
    }
}
