package br.com.senior.agroinfo.db.dao;

import java.util.Optional;

import javax.ejb.Stateless;

import br.com.senior.agroinfo.db.modal.TipoTitulo;

@Stateless
public class TipoTituloDAO extends GenericDAO<TipoTitulo> {

	@Override
	public TipoTitulo adiciona(TipoTitulo entity) {
		Optional<TipoTitulo> busca = super.busca(entity.getCodTipoTitulo());
		if (busca.isPresent()) {
			return super.adiciona(entity);
		}
		return super.atualiza(entity);
	}

}