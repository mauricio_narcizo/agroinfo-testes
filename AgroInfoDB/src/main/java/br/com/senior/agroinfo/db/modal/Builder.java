package br.com.senior.agroinfo.db.modal;

public interface Builder<T extends BaseEntity> {
	
	public T build();

}
