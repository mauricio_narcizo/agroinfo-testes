package br.com.senior.agroinfo.db.modal;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.com.senior.agroinfo.db.enums.TipoConta;

@Entity
@Table(name = "conta")
public class Conta implements BaseEntity, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idcooperado")
	private Cooperado cooperado;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idempresafilial")
	private EmpresaFilial empresaFilial;

	@Column
	private String descricao;

	@Column(precision = 20)
	private BigDecimal valor;

	@Column(precision = 20)
	private BigDecimal juros;

	@Column(precision = 20)
	private BigDecimal multa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(length = 29)
	private Date dtEmissao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date dtVencimento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date dtUpdate;

	@Column
	private Boolean integrado;

	@Column
	private Date dtVencimentoOriginal;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idtipotitulo")
	private TipoTitulo tipoTitulo;

	@Column(precision = 15, scale = 2)
	private BigDecimal valorAberto;

	@Column
	private Boolean contaAberta;
	
	@Column
	private String numeroTitulo;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipoconta", nullable = false)
	private TipoConta tipoConta;

	Conta() {
	}

	@Override
	public Conta clone() throws CloneNotSupportedException {
		Conta contaPagar = (Conta) super.clone();
		contaPagar.id = null;
		return contaPagar;
	}

	public Conta(Builder builder) {
		this.cooperado = builder.cooperado;
		this.descricao = builder.descricao;
		this.valor = builder.valor;
		this.juros = builder.juros;
		this.multa = builder.multa;
		this.dtEmissao = builder.dtEmissao;
		this.dtVencimento = builder.dtVencimento;
		this.dtUpdate = builder.dtUpdate;
		this.integrado = builder.integrado;
		this.empresaFilial = builder.empresaFilial;
		this.tipoTitulo = builder.tipoTitulo;
		this.dtVencimentoOriginal = builder.dtVencimentoOriginal;
		this.valorAberto = builder.valorAberto;
		this.contaAberta = builder.contaAberta;
		this.numeroTitulo = builder.numeroTitulo;
		this.setTipoConta(builder.tipoConta);
	}
	
	public BigDecimal getValorAberto() {
		return valorAberto;
	}

	public void setValorAberto(BigDecimal valorAberto) {
		this.valorAberto = valorAberto;
	}

	public Cooperado getCooperado() {
		return this.cooperado;
	}

	public void setCooperado(Cooperado cooperado) {
		this.cooperado = cooperado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getJuros() {
		return this.juros;
	}

	public void setJuros(BigDecimal juros) {
		this.juros = juros;
	}

	public BigDecimal getMulta() {
		return this.multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public Date getDtEmissao() {
		return this.dtEmissao;
	}

	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public Date getDtVencimento() {
		return this.dtVencimento;
	}

	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	public Date getDtUpdate() {
		return this.dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public String getNumeroTitulo() {
		return numeroTitulo;
	}

	public void setNumeroTitulo(String numeroTitulo) {
		this.numeroTitulo = numeroTitulo;
	}

	public Boolean getIntegrado() {
		return this.integrado;
	}

	public void setIntegrado(Boolean integrado) {
		this.integrado = integrado;
	}

	public Date getDtVencimentoOriginal() {
		return dtVencimentoOriginal;
	}

	public void setDtVencimentoOriginal(Date dtVencimentoOriginal) {
		this.dtVencimentoOriginal = dtVencimentoOriginal;
	}

	public TipoTitulo getTipoTitulo() {
		return tipoTitulo;
	}

	public void setTipoTitulo(TipoTitulo tipoTitulo) {
		this.tipoTitulo = tipoTitulo;
	}

	public Boolean isContaAberta() {
		return contaAberta;
	}

	public void setContaAberta(Boolean contaAberta) {
		this.contaAberta = contaAberta;
	}

	public EmpresaFilial getEmpresaFilial() {
		return empresaFilial;
	}

	public void setEmpresaFilial(EmpresaFilial empresaFilial) {
		this.empresaFilial = empresaFilial;
	}

	public String getNomeCooperado() {
		Objects.requireNonNull(cooperado, "Conta Pagar deve ter cooperado");
		return cooperado.getNome();
	}

	public String getNomeFilial() {
//		Objects.requireNonNull(empresaFilial, "Conta Pagar deve possuir uma empresa");
		if(empresaFilial == null)
		{
			return "";
		}
		return empresaFilial.getNomeFilial();
	}

	public String getNomeEmpresa() {
//		Objects.requireNonNull(empresaFilial, "Conta Pagar deve possuir uma empresa");
		if(empresaFilial == null)
		{
			return "";
		}
		return empresaFilial.getNomeEmpresa();
	}

	public String getDescricaoTipoTitulo() {
		Objects.requireNonNull(tipoTitulo, "Conta Pagar deve possuir tipo titulo");
		return tipoTitulo.getDescricaoTitulo();
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public static class Builder implements br.com.senior.agroinfo.db.modal.Builder<Conta> {

		String codTitulo;
		Cooperado cooperado;
		EmpresaFilial empresaFilial;
		String descricao;
		BigDecimal valor;
		BigDecimal juros;
		BigDecimal multa;
		Date dtEmissao;
		Date dtVencimento;
		Date dtUpdate;
		Boolean integrado;
		Date dtVencimentoOriginal;
		TipoTitulo tipoTitulo;
		BigDecimal valorAberto;
		Boolean contaAberta;
		TipoConta tipoConta;
		String numeroTitulo;

		public Builder(TipoConta tipoConta) {
			this.tipoConta = tipoConta;
		}

		public Builder codTitulo(String value) {
			this.codTitulo = value;
			return this;
		}

		public Builder cooperado(Cooperado value) {
			this.cooperado = value;
			return this;
		}

		public Builder empresaFilial(EmpresaFilial value) {
			this.empresaFilial = value;
			return this;
		}

		public Builder descricao(String value) {
			this.descricao = value;
			return this;
		}

		public Builder valor(BigDecimal value) {
			this.valor = value;
			return this;
		}

		public Builder juros(BigDecimal value) {
			this.juros = value;
			return this;
		}
		
		public Builder numeroTitulo(String numeroTitulo){
			this.numeroTitulo = numeroTitulo;
			return this;
		}

		public Builder multa(BigDecimal value) {
			this.multa = value;
			return this;
		}

		public Builder dtEmissao(Date value) {
			this.dtEmissao = value;
			return this;
		}

		public Builder dtVencimento(Date value) {
			this.dtVencimento = value;
			return this;
		}

		public Builder dtUpdate(Date value) {
			this.dtUpdate = value;
			return this;
		}

		public Builder integrado(Boolean value) {
			this.integrado = value;
			return this;
		}

		public Builder dtVencimentoOriginal(Date value) {
			this.dtVencimentoOriginal = value;
			return this;
		}

		public Builder tipoTitulo(TipoTitulo value) {
			this.tipoTitulo = value;
			return this;
		}

		public Builder valorAberto(BigDecimal valor) {
			this.valorAberto = valor;
			return this;
		}

		public Builder contaAberta(Boolean value) {
			this.contaAberta = value;
			return this;
		}

		@Override
		public Conta build() {
			return new Conta(this);
		}

	}
}