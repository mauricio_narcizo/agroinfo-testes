package br.com.senior.agroinfo.db.enums;

public enum TipoAgendamento {
	
    COOPERADO("Cooperado"),     
    BUSCASMS("Buscar SMS"),     
    CONTAS_PAGAR("Contas a Pagar"), CONTAS_RECEBER("Contas a Receber");
    
    private final String descricao;

    private TipoAgendamento(final String descricao) {
        this.descricao = descricao;
    }

	public String getDescricao() {
		return descricao;
	}		

}
