package br.com.senior.agroinfo.db.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.senior.agroinfo.db.modal.EmpresaFilial;

@Stateless
public class EmpresaFilialDAO extends GenericDAO<EmpresaFilial> {

    public List<EmpresaFilial> getListaAtiva() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<EmpresaFilial> query = builder.createQuery(EmpresaFilial.class);
        Root<EmpresaFilial> from = query.from(EmpresaFilial.class);
        Predicate predicate = builder.isTrue(from.get("ativa"));
        query.select(from).where(predicate);
        TypedQuery<EmpresaFilial> result = em.createQuery(query);

        return result.getResultList();
    }

}
