package br.com.senior.agroinfo.db.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.senior.agroinfo.db.modal.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario> {


    public Usuario recuperaPorLogin(String login) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
        Root<Usuario> root = criteria.from(Usuario.class);
        Predicate predicate = builder.equal(root.get("login"), login);

        criteria.select(root).where(predicate);

        return em.createQuery(criteria).getSingleResult();
    }
}