package br.com.senior.agroinfo.db.enums;

public enum FormatoPeriodicidade {

    MINUTOS(60, "Minutos"),
    HORAS(MINUTOS.getTempoSegundos() * 60, "Horas"),
   	DIAS(HORAS.getTempoSegundos() * 24, "Dias"), 
   	SEMANAS(DIAS.getTempoSegundos() * 7, "Semanas");

    private final int tempoSegundos;
    private final String descricao;

    private FormatoPeriodicidade(final int tempoSegundos, String descricao) {
        this.tempoSegundos = tempoSegundos;        
        this.descricao = descricao;
    }

    public int getTempoSegundos() {
        return tempoSegundos;
    }
    
    public String getDescricao() {
    	return descricao;
    }

}
