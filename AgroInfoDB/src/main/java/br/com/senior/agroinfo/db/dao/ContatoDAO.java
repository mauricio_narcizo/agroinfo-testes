package br.com.senior.agroinfo.db.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.senior.agroinfo.db.modal.Contato;

public class ContatoDAO extends GenericDAO<Contato> {


    public Contato create(Contato contato) {
        Optional<Contato> optional = buscaPorDDDTelefone(contato.getDdd(), contato.getTelefone());
        if (!optional.isPresent()) {
            adiciona(contato);
        }
        contato.setDtUpdate(new Date());
        return contato;
    }

    public Optional<Contato> buscaPorDDDTelefone(String ddd, String telefone) {
        CriteriaBuilder criterio = em.getCriteriaBuilder();
        CriteriaQuery<Contato> query = criterio.createQuery(Contato.class);
        Root<Contato> tabelaContato = query.from(Contato.class);

        Path<String> pathTelefone = tabelaContato.get("telefone");
        Path<String> pathDDD = tabelaContato.get("ddd");

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criterio.like(pathTelefone, telefone));
        predicates.add(criterio.like(pathDDD, ddd));

        query.select(tabelaContato).where(predicates.toArray(new Predicate[] {}));
        TypedQuery<Contato> typedQuery = em.createQuery(query);
        Optional<Contato> optional = Optional.empty();
        try {
            optional = Optional.ofNullable(typedQuery.getSingleResult());
        } catch (NoResultException e) {
            Logger.getLogger("ContatoDAO").log(Level.SEVERE, e.getMessage());
        }
        return optional;
    }

}
