package br.com.senior.agroinfo.db.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;

import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.BaseEntityId;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.QConta;
import br.com.senior.agroinfo.db.modal.QEmpresaFilial;

@Stateless
public class ContaDAO extends GenericDAO<Conta> {

	public Conta create(Conta conta) {
		
		conta.setDtUpdate(new Date());
		Optional<Conta> busca = buscarConta(conta.getTipoTitulo().getCodTipoTitulo(),conta.getCooperado().getId(),conta.getTipoConta() );
		if (busca.isPresent()) {
			conta.setId(busca.get().getId());
			atualiza(conta);
		} else {
			adiciona(conta);
		}
		return conta;
	}
	
	public Optional<Conta> buscarConta(final String codigoTitulo, final Long idCooperado, TipoConta tipoConta ) {
		JPAQuery<Conta> query = new JPAQuery<Conta>(em).from(QConta.conta)
				.where(QConta.conta.tipoTitulo.codTipoTitulo.eq(codigoTitulo)
				.and(QConta.conta.cooperado.id.eq(idCooperado))
				.and(QConta.conta.tipoConta.eq(tipoConta)));
				

		return Optional.ofNullable(query.fetchOne());
	}
	
	public List<Conta> getListaContaAberta() {
		List<Conta> listaContaAberta = getLista()
											.stream()
											.filter(conta -> conta.isContaAberta())
											.collect(Collectors.toList());
		return listaContaAberta;
	}

	public List<Conta> buscaContaCooperado(Long idCooperado) {
		JPAQuery<Conta> query = new JPAQuery<Conta>(em)
				.from(QConta.conta, QEmpresaFilial.empresaFilial)
				.where(QConta.conta.empresaFilial.id.eq(QEmpresaFilial.empresaFilial.id))
				.where(QConta.conta.cooperado.id.eq(idCooperado));

		return query.fetchAll().fetch();
	}
	
	public List<Conta> buscaContas(Long idCooperado) {
		List<Conta> lista = getLista();

		return lista.stream().filter(conta -> conta.getCooperado().getId().equals(idCooperado))
				.collect(Collectors.toList());
	}
	
	@Override
	public void remove(BaseEntityId entity) {
		// TODO Auto-generated method stub
		super.remove(entity);
	}
	
	@Override
	public void remove(Conta entity, Object id) {
		// TODO Auto-generated method stub
		super.remove(entity, id);
	}
	
/*
	@Override
	protected boolean isEntityValid(ContaPagar entity) {
		return entity.isContaAberta();
	}
*/
}
