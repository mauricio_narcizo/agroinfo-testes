package br.com.senior.agroinfo.db.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;

@Entity
@Table(schema = "public")
public class TipoTitulo implements BaseEntity {

    private static final long serialVersionUID = 1L;

	@Id
    private String codTipoTitulo;
    
    @Column
    private String descricaoTitulo;
    
    @Column
    private String abreviacaoTipoTitulo;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "situacao", nullable=false)    
    private SituacaoTipoTitulo situacao;
    
    TipoTitulo() {}

    public TipoTitulo(Builder builder) {
    	this.codTipoTitulo = builder.codTipoTitulo;
    	this.descricaoTitulo = builder.descricaoTitulo;
    	this.abreviacaoTipoTitulo = builder.abreviacaoTipoTitulo;
    	this.situacao = builder.situacao;
	}
    

    public String getCodTipoTitulo() {
        return codTipoTitulo;
    }

    public void setCodTipoTitulo(String codTipoTitulo) {
        this.codTipoTitulo = codTipoTitulo;
    }

    public String getDescricaoTitulo() {
        return descricaoTitulo;
    }

    public void setDescricaoTitulo(String descricaoTitulo) {
        this.descricaoTitulo = descricaoTitulo;
    }

    public String getAbreviacaoTipoTitulo() {
        return abreviacaoTipoTitulo;
    }

    public void setAbreviacaoTipoTitulo(String abreviacaoTipoTitulo) {
        this.abreviacaoTipoTitulo = abreviacaoTipoTitulo;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codTipoTitulo == null) ? 0 : codTipoTitulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoTitulo other = (TipoTitulo) obj;
		if (codTipoTitulo == null) {
			if (other.codTipoTitulo != null)
				return false;
		} else if (!codTipoTitulo.equals(other.codTipoTitulo))
			return false;
		return true;
	}
	
	public SituacaoTipoTitulo getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoTipoTitulo situacao) {
		this.situacao = situacao;
	}

	public static class Builder implements br.com.senior.agroinfo.db.modal.Builder<TipoTitulo>{

	    String codTipoTitulo;
	    String descricaoTitulo;
	    String abreviacaoTipoTitulo;
	    SituacaoTipoTitulo situacao;	    
		
		public Builder codTipoTitulo(String value) {
			this.codTipoTitulo = value;
			return this;
		}
		
		public Builder situacao(SituacaoTipoTitulo value) {
			this.situacao = value;
			return this;
		}
		
		public Builder descricaoTitulo(String value) {
			this.descricaoTitulo = value;
			return this;
		}		
	    
		public Builder abreviacaoTipoTitulo(String value) {
			this.abreviacaoTipoTitulo = value;
			return this;
		}
		
		@Override
		public TipoTitulo build() {
			return new TipoTitulo(this);
		}
		
	}
    
    

}
