package br.com.senior.agroinfo.db.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.QCooperado;

@Stateless
public class CooperadoDAO extends GenericDAO<Cooperado> {

    @PersistenceContext
    private EntityManager em;

    public Cooperado create(Cooperado cooperado) {
        cooperado.setDtUpdate(new Date());
        Optional<Cooperado> busca = busca(cooperado.getId());
        if (busca.isPresent()) {
            atualiza(cooperado);
        } else {
            adiciona(cooperado);
        }
        return cooperado;
    }

    public List<Cooperado> getCooperadosAtivos() {
        List<Cooperado> lista = (List<Cooperado>) getLista();
        return lista.stream().filter(cooperado -> cooperado.isAtivo()).collect(Collectors.toList());
    }

    public List<Cooperado> obterCooperadosPorDtUpdate(Date data) {
        JPAQuery<Cooperado> query = new JPAQuery<Cooperado>(em).from(QCooperado.cooperado)
                .where(QCooperado.cooperado.dtUpdate.after(data));
        return query.fetch();
        // return query.list(QCooperado.cooperado);
    }

    public Optional<Cooperado> buscaPorCodigoMatricula(Integer codigoMatricula) {
        JPAQuery<Cooperado> query = new JPAQuery<Cooperado>(em).from(QCooperado.cooperado)
                .where(QCooperado.cooperado.codigoMatricula.eq(codigoMatricula));
        return Optional.ofNullable(query.fetchFirst());

    }

	public Optional<Cooperado> buscaPorCodigoCpfCnpj(int codigoMatricula, String cpfCnpj) {
		JPAQuery<Cooperado> query = new JPAQuery<Cooperado>(em).from(QCooperado.cooperado)
		.where(QCooperado.cooperado.codigoMatricula.eq(codigoMatricula)
				.and(QCooperado.cooperado.cpfCnpj.eq(cpfCnpj)));
		return Optional.ofNullable(query.fetchFirst());
	}

}