package br.com.senior.agroinfo.db.modal;

public interface BaseEntityId  extends BaseEntity {

	void setId(Long id);
	
	Long getId();	
	
}
