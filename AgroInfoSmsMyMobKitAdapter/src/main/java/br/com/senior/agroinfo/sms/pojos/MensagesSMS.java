package br.com.senior.agroinfo.sms.pojos;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MensagesSMS {

	@SerializedName("messages")
	@Expose
	private List<MessageSMS> MessageSMSs = new ArrayList<MessageSMS>();
	@SerializedName("description")
	@Expose
	private String description;
	@SerializedName("isSuccessful")
	@Expose
	private Boolean isSuccessful;
	@SerializedName("requestMethod")
	@Expose
	private String requestMethod;

	/**
	* 
	* @return
	* The MessageSMSs
	*/
	public List<MessageSMS> getMessageSMSs() {
	return MessageSMSs;
	}

	/**
	* 
	* @param MessageSMSs
	* The MessageSMSs
	*/
	public void setMessageSMSs(List<MessageSMS> MessageSMSs) {
	this.MessageSMSs = MessageSMSs;
	}

	/**
	* 
	* @return
	* The description
	*/
	public String getDescription() {
	return description;
	}

	/**
	* 
	* @param description
	* The description
	*/
	public void setDescription(String description) {
	this.description = description;
	}

	/**
	* 
	* @return
	* The isSuccessful
	*/
	public Boolean getIsSuccessful() {
	return isSuccessful;
	}

	/**
	* 
	* @param isSuccessful
	* The isSuccessful
	*/
	public void setIsSuccessful(Boolean isSuccessful) {
	this.isSuccessful = isSuccessful;
	}

	/**
	* 
	* @return
	* The requestMethod
	*/
	public String getRequestMethod() {
	return requestMethod;
	}

	/**
	* 
	* @param requestMethod
	* The requestMethod
	*/
	public void setRequestMethod(String requestMethod) {
	this.requestMethod = requestMethod;
	}

	}