package br.com.senior.agroinfo.sms.utils;

public class Telefone {
	private String numero;
	private String ddd;


	public Telefone(String Telefone) {
		setTelefone(Telefone);
	}

	private boolean inRange(int num, int min, int max) {
		return num >= min && num <= max;
	}

	public boolean temNonoDigito() {
		Integer i = Integer.parseInt(numero.substring(0, 2));
		return inRange(i, 11, 19) || inRange(i, 21, 24) || i == 27 || i == 28 || inRange(i, 91, 99);
	}

	protected String apenasNumeros(String s) {
		if (s == null)
			return "";
		String result = "";
		for (char c : s.toCharArray()) {
			if (c >= '0' && c <= '9')
				result += c;
		}
		return result;
	}

	public String getDdd() {
		return ddd;
	}

	public String getNumero() {
		return numero;
	}

	private void setTelefone(String telefone) {
		
		if (telefone == null) {
			return;
		}
		if (telefone.startsWith("+55")) {
			telefone = telefone.substring(3);
		}
		if (telefone.trim().isEmpty()){
			return;	
		}
		telefone = apenasNumeros(telefone);
		int len = telefone.length();
		switch (len) {
		case 8:
			this.numero = telefone;
			break;
		case 9:
			this.numero = telefone;
			break;
		case 10:
			this.numero = telefone.substring(2, len);
			this.ddd = telefone.substring(0, 2);
			break;
		case 11:
			if (telefone.startsWith("0")) {
				telefone = telefone.replaceFirst("0", "");
				this.numero = telefone.substring(2, len-1);
				this.ddd = telefone.substring(0, 2);
			} else {
				this.numero = telefone.substring(2, len);
				this.ddd = telefone.substring(0, 2);
			}
			break;
		case 12:
			if (telefone.startsWith("0")) {
				telefone = telefone.replaceFirst("0", "");
				this.numero = telefone.substring(2, len-1);
			} else {
				this.numero = telefone.substring(2, len);
			}
			this.ddd = telefone.substring(0, 2);
			break;
		}
	}

}