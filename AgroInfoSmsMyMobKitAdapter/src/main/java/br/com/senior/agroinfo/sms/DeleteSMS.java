package br.com.senior.agroinfo.sms;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.senior.agroinfo.db.dao.ConfiguracoesDAO;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.sms.pojos.DeleteReturn;

public class DeleteSMS {

	@Inject
	ConfiguracoesDAO cDao;

	public boolean DeletaSMS(String id) {
		// Map<Contato, MessageSMS> ContatoMensagem = new Hashtable<Contato,
		// MessageSMS>();

		Optional<Configuracoes> config = cDao.retrieve();
		if (!config.isPresent()) {
			return false;
		}
		String ip = config.get().getIpsms();
		String porta = config.get().getPortasms();

		Client client = Client.create();
		WebResource webResource = client.resource("http://" + ip + ":" + porta
				+ "/services/api/messaging/" + id);

		ClientResponse response = webResource
				.accept(MediaType.APPLICATION_JSON)
				.delete(ClientResponse.class);
		String resp = response.getEntity(String.class);

		DeleteReturn delete = new Gson().fromJson(resp, DeleteReturn.class);
		return delete.getIsSuccessful();
	}

}
