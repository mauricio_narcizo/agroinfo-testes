package br.com.senior.agroinfo.sms.pojos;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DeleteReturn {

@SerializedName("count")
@Expose
private Integer count;
@SerializedName("description")
@Expose
private String description;
@SerializedName("requestMethod")
@Expose
private String requestMethod;
@SerializedName("isSuccessful")
@Expose
private Boolean isSuccessful;

/**
* 
* @return
* The count
*/
public Integer getCount() {
return count;
}

/**
* 
* @param count
* The count
*/
public void setCount(Integer count) {
this.count = count;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The requestMethod
*/
public String getRequestMethod() {
return requestMethod;
}

/**
* 
* @param requestMethod
* The requestMethod
*/
public void setRequestMethod(String requestMethod) {
this.requestMethod = requestMethod;
}

/**
* 
* @return
* The isSuccessful
*/
public Boolean getIsSuccessful() {
return isSuccessful;
}

/**
* 
* @param isSuccessful
* The isSuccessful
*/
public void setIsSuccessful(Boolean isSuccessful) {
this.isSuccessful = isSuccessful;
}

}