package br.com.senior.agroinfo.sms.pojos;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Message {

@SerializedName("date")
@Expose
private String date;
@SerializedName("to")
@Expose
private String to;
@SerializedName("id")
@Expose
private String id;
@SerializedName("number")
@Expose
private String number;
@SerializedName("message")
@Expose
private String message;
@SerializedName("read")
@Expose
private Boolean read;

/**
* 
* @return
* The date
*/
public String getDate() {
return date;
}

/**
* 
* @param date
* The date
*/
public void setDate(String date) {
this.date = date;
}

/**
* 
* @return
* The to
*/
public String getTo() {
return to;
}

/**
* 
* @param to
* The to
*/
public void setTo(String to) {
this.to = to;
}

/**
* 
* @return
* The id
*/
public String getId() {
return id;
}

/**
* 
* @param id
* The id
*/
public void setId(String id) {
this.id = id;
}

/**
* 
* @return
* The number
*/
public String getNumber() {
return number;
}

/**
* 
* @param number
* The number
*/
public void setNumber(String number) {
this.number = number;
}

/**
* 
* @return
* The message
*/
public String getMessage() {
return message;
}

/**
* 
* @param message
* The message
*/
public void setMessage(String message) {
this.message = message;
}

/**
* 
* @return
* The read
*/
public Boolean getRead() {
return read;
}

/**
* 
* @param read
* The read
*/
public void setRead(Boolean read) {
this.read = read;
}

}
