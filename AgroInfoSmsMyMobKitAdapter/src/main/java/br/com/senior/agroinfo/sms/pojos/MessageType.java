package br.com.senior.agroinfo.sms.pojos;

public enum MessageType {
	
	ENVIADA(1, "MESSAGE_TYPE_SENT"),
	RECEBIDA(1, "MESSAGE_TYPE_INBOX");

private final int codigo;
private final String Tipo;

private MessageType(final int codigo, final String Tipo) {
    this.codigo = codigo;
    this.Tipo = Tipo;
}

public int getCodigo() {
    return codigo;
}

public String getTipo() {
    return Tipo;
}

}