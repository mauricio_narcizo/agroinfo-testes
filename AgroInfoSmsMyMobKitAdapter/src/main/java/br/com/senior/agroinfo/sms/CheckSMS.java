package br.com.senior.agroinfo.sms;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.senior.agroinfo.db.dao.ConfiguracoesDAO;
import br.com.senior.agroinfo.db.dao.ContatoDAO;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.sms.pojos.MensagesSMS;
import br.com.senior.agroinfo.sms.pojos.MessageSMS;
import br.com.senior.agroinfo.sms.pojos.MessageType;
import br.com.senior.agroinfo.sms.utils.Telefone;

public class CheckSMS {

    @Inject
    ContatoDAO ctDao;

    @Inject 
    ConfiguracoesDAO cDao;
    public Map<Contato, MessageSMS> BuscaSMS() {
        Map<Contato, MessageSMS> contatoMensagem = new Hashtable<Contato, MessageSMS>();

         Optional<Configuracoes> config = cDao.retrieve();
         if (!config.isPresent()){
        	 return null;
         }
        String ip = config.get().getIpsms();
        String porta = config.get().getPortasms();

        Client client = Client.create();
        try {
            WebResource webResource = client.resource("http://" + ip + ":" + porta + "/services/api/messaging/");

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String resp = response.getEntity(String.class);

            MensagesSMS mensagens = new Gson().fromJson(resp, MensagesSMS.class);
            for (MessageSMS mensagem : mensagens.getMessageSMSs()) {
                if (!mensagem.getMessageType().equalsIgnoreCase(MessageType.ENVIADA.getTipo())) {
                    Telefone tel = new Telefone(mensagem.getNumber());
                    if (tel.getDdd() != null && tel.getNumero() != null) {
                        ctDao.buscaPorDDDTelefone(tel.getDdd(), tel.getNumero())
                                .ifPresent(contato -> contatoMensagem.put(contato, mensagem));
                    }
                }
            }
            return contatoMensagem;
        } catch (Exception e) {
            return Collections.emptyMap();
        }
    }

}
