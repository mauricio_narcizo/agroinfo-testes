package br.com.senior.agroinfo.sms.pojos;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SendReturn {

@SerializedName("message")
@Expose
private Message message;
@SerializedName("description")
@Expose
private String description;
@SerializedName("requestMethod")
@Expose
private String requestMethod;
@SerializedName("isSuccessful")
@Expose
private Boolean isSuccessful;

/**
* 
* @return
* The message
*/
public Message getMessage() {
return message;
}

/**
* 
* @param message
* The message
*/
public void setMessage(Message message) {
this.message = message;
}

/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The requestMethod
*/
public String getRequestMethod() {
return requestMethod;
}

/**
* 
* @param requestMethod
* The requestMethod
*/
public void setRequestMethod(String requestMethod) {
this.requestMethod = requestMethod;
}

/**
* 
* @return
* The isSuccessful
*/
public Boolean getIsSuccessful() {
return isSuccessful;
}

/**
* 
* @param isSuccessful
* The isSuccessful
*/
public void setIsSuccessful(Boolean isSuccessful) {
this.isSuccessful = isSuccessful;
}

}