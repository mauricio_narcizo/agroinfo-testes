package br.com.senior.agroinfo.sms.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageSMS {
	@SerializedName("date")
	@Expose
	private String date;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("messageType")
	@Expose
	private String messageType;
	@SerializedName("number")
	@Expose
	private String number;
	@SerializedName("read")
	@Expose
	private Boolean read;
	@SerializedName("receiver")
	@Expose
	private String receiver;
	@SerializedName("sender")
	@Expose
	private String sender;
	@SerializedName("threadID")
	@Expose
	private Integer threadID;

	/**
	 * 
	 * @return The date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * 
	 * @param date
	 *            The date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * 
	 * @return The id
	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 *            The message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return The messageType
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * 
	 * @param messageType
	 *            The messageType
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * 
	 * @return The number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * 
	 * @param number
	 *            The number
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * 
	 * @return The read
	 */
	public Boolean getRead() {
		return read;
	}

	/**
	 * 
	 * @param read
	 *            The read
	 */
	public void setRead(Boolean read) {
		this.read = read;
	}

	/**
	 * 
	 * @return The receiver
	 */
	public String getReceiver() {
		return receiver;
	}

	/**
	 * 
	 * @param receiver
	 *            The receiver
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	/**
	 * 
	 * @return The sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * 
	 * @param sender
	 *            The sender
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * 
	 * @return The threadID
	 */
	public Integer getThreadID() {
		return threadID;
	}

	/**
	 * 
	 * @param threadID
	 *            The threadID
	 */
	public void setThreadID(Integer threadID) {
		this.threadID = threadID;
	}

}
