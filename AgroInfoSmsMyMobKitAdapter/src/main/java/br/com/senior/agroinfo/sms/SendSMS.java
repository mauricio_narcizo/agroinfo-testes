package br.com.senior.agroinfo.sms;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import br.com.senior.agroinfo.db.dao.ConfiguracoesDAO;
import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.sms.pojos.SendReturn;

public class SendSMS {
	
	@Inject
	ConfiguracoesDAO cDao;

    public boolean EnviaSMS(Contato contato, String mensagem) {

		Optional<Configuracoes> config = cDao.retrieve();
		if (!config.isPresent()) {
			return false;
		}
		String ip = config.get().getIpsms();
		String porta = config.get().getPortasms();


        Client client = Client.create();
        WebResource webResource = client.resource("http://" + ip + ":" + porta + "/services/api/messaging/");

        MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
        formData.add("to", contato.getDdd() + contato.getTelefone());
        formData.add("DeliveryReport", "0");
        formData.add("Message", mensagem);
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                .post(ClientResponse.class, formData);

        String resp = response.getEntity(String.class);
        SendReturn envio = new Gson().fromJson(resp, SendReturn.class);

        return envio.getIsSuccessful();
    }

}
