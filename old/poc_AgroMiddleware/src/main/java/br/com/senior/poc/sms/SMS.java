package br.com.senior.poc.sms;

import java.util.List;

public class SMS {
	/*{"messages":[{"date":"2014-11-22T18:13:01GMT+08:00","id":"2175","message":"hi how are you?", 
		"messageType":"MESSAGE_TYPE_INBOX","number":"+1234567890","receiver":"Me","sender":"Alice" 
		"threadID":301, "read":true}], 
		"description":"","requestMethod":"GET","isSuccessful":true}*/
	
	private String description;
	
	private String requestMethod;
	
	private String isSuccessful;
	
	private Mensagem[] messages;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(String isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public Mensagem[] getMessages() {
		return messages;
	}

	public void setMessages(Mensagem[] messages) {
		this.messages = messages;
	}
	
	
}
