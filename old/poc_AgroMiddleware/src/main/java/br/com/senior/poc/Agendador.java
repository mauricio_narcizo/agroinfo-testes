package br.com.senior.poc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import br.com.senior.poc.RN.ConfigRN;
import br.com.senior.poc.model.Config;

@Startup
@Singleton
public class Agendador implements ServletContextListener {
	public static Scheduler sched;
	private Logger log;

	@PostConstruct
	protected void init() throws ObjectAlreadyExistsException {

		try {
			/* Fábrica para criar instância de Scheduler */
			
			StdSchedulerFactory schedFactory = new StdSchedulerFactory();

			getLog().info("---------------- Criar Agendador ----------------");
			Agendador.sched = schedFactory.getScheduler();

			/*************************Inicio Job*****************************/
			getLog().info(
					"---------------- Criando Tarefa: JobHora para ser executada a cada 3 minutos ----------------");
			int tempo = 1;
			ConfigRN crn = new ConfigRN();
			Config config = crn.buscarPorNome("tempo_busca_fornecedor");
			if (config == null){
				config = new Config();
				config.setNome("tempo_busca_fornecedor");
				config.setValor("1");
			}
			tempo = Integer.valueOf(config.getValor());
			JobDetail testjob = JobBuilder.newJob(testjob.class).withIdentity("testjob", "testgroup").build();

			SimpleTrigger trigger = TriggerBuilder.newTrigger().withIdentity("testtrigger", "testgroup")
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(tempo).repeatForever())
					.build();
			
			
			config = crn.buscarPorNome("tempo_busca_SMS");
			if (config == null){
				config = new Config();
				config.setNome("tempo_busca_SMS");
				config.setValor("30");
			}
			
			tempo = Integer.valueOf(config.getValor());
			JobDetail buscasms = JobBuilder.newJob(buscasms.class).withIdentity("buscasms", "testgroup").build();

			SimpleTrigger trigger2 = TriggerBuilder.newTrigger().withIdentity("testtrigger", "group2")
					.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(tempo).repeatForever())
					.build();
			
			sched.scheduleJob(testjob, trigger);
			sched.scheduleJob(buscasms, trigger2);

			/* Iniciar execução do Scheduler */
			Agendador.sched.start();

		} catch (Exception e) {
			System.out.println("\n\nErro ao tentar iniciar Scheduler .\n\n");
			e.printStackTrace();
		}
	}

	public void contextInitialized(ServletContextEvent ce) {
		try {
			init();
		} catch (Exception ex) {
			ex.getStackTrace();
		}
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		// Finaliza o schedule
		restart();
	}

	public void restart() {
		try {
			Agendador.sched.shutdown(true);
		} catch (Exception ex) {
			getLog().error("Erro ao tentar reiniciar as tarefas");
			getLog().error(ex.getMessage(), ex);
		}
	}

	protected Logger getLog() {
		if (this.log == null) {
			this.log = Logger.getLogger("AÇÃO : ");
		}
		return this.log;
	}

}