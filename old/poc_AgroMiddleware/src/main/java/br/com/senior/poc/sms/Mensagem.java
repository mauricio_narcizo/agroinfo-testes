package br.com.senior.poc.sms;

import java.util.Date;

public class Mensagem {
	/*{"date":"2014-11-22T18:13:01GMT+08:00","id":"2175","message":"hi how are you?", 
		"messageType":"MESSAGE_TYPE_INBOX","number":"+1234567890","receiver":"Me","sender":"Alice" 
		"threadID":301, "read":true}
	*/
	
	private String date;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public Integer getThreadID() {
		return threadID;
	}
	public void setThreadID(Integer threadID) {
		this.threadID = threadID;
	}
	public Boolean getRead() {
		return read;
	}
	public void setRead(Boolean read) {
		this.read = read;
	}
	private String id;
	private String message;
	private String messageType;
	private String number;
	private String receiver;
	private String sender;
	private Integer threadID;
	private Boolean read;
	
	
}
