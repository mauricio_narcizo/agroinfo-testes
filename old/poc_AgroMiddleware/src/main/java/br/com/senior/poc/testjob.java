package br.com.senior.poc;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.com.senior.Consultas.BuscaContasPagar;
import br.com.senior.Consultas.buscaFornecedor;
import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.sessao.sessao;

@ApplicationScoped
@Transactional
public class testjob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			sessao session = sessao.getInstance();
			List<Fornecedor> fornecedores = buscaFornecedor.buscaSalvaFornecedores(session);
			for (Fornecedor fornecedor : fornecedores) {
				if (fornecedor.getNomeFantasia().toLowerCase().contains("zandonai")){
					//busca titulos a pagar  e receber
					BuscaContasPagar.buscaSalvaContasPagar(fornecedor,session);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}