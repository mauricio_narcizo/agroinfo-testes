package br.com.senior.poc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Type;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.senior.poc.RN.FornecedorRN;
import br.com.senior.poc.RN.TituloRN;
import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.model.Titulo;
import br.com.senior.poc.sms.Mensagem;
import br.com.senior.poc.sms.SMS;

@ApplicationScoped
@Transactional
public class buscasms implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		String strTemp = "";
		String msg = null;
		try {
			URL url;
			url = new URL("http://192.168.42.129:1688/services/api/messaging/?&status=0");
			BufferedReader br;
			br = new BufferedReader(new InputStreamReader(url.openStream()));
		
			while (null != (strTemp = br.readLine())) {
				System.out.println(strTemp);
				msg =strTemp;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Type listType = new TypeToken<SMS>() {
		}.getType();
		SMS SMSs = new Gson().fromJson(msg, listType);

		for (Mensagem sms2 : SMSs.getMessages()) {

			try {

				HttpClient httpclient;
				HttpPost httppost;
				ArrayList<NameValuePair> postParameters;
				httpclient = new DefaultHttpClient();
				httppost = new HttpPost("http://192.168.42.129:1688/services/api/messaging/");

				postParameters = new ArrayList<NameValuePair>();
				postParameters.add(new BasicNameValuePair("to", sms2.getNumber()));
				
				postParameters.add(new BasicNameValuePair("DeliveryReport", "0"));
				FornecedorRN frn = new FornecedorRN();
				Fornecedor forne = frn.buscaFornecedorTel(sms2.getNumber());
				TituloRN tit = new TituloRN();
				List<Titulo> titulo = tit.pesquisaFornecedor(forne);
						
				if (forne != null){
					StringBuilder titulos = new StringBuilder( "ola "+forne.getNomeFantasia()+"   ");
					for (Titulo titulo2 : titulo) {
						titulos.append("Data:"+titulo2.getDataLancamento());
						titulos.append("Valor:"+titulo2.getValor());
					}
					postParameters.add(new BasicNameValuePair("Message",titulos.toString()));	
				}else {
					postParameters.add(new BasicNameValuePair("Message", "Não foi possivel localizar o seu cadastro verifique com sua cooperativa"));
				}
				httppost.setEntity(new UrlEncodedFormEntity(postParameters));

				HttpResponse response = httpclient.execute(httppost);

			} catch (Exception ex)

			{
				// handle exception here
			} finally

			{
				httpClient.getConnectionManager().shutdown();
			}


			try {
				HttpClient httpclient;
				HttpDelete deletar = new HttpDelete("http://192.168.42.129:1688/services/api/messaging/" + sms2.getId());
				httpclient = new DefaultHttpClient();
				httpclient.execute(deletar);	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
