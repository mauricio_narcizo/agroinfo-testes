package br.com.senior.poc.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("/valida/fornecedor")
public class ValidaFornecedor {
    @Inject
    restService service;

    @POST
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"}) 
    @Path("/json")
    @Produces("application/json")
    public String getValidaFornecedor(@FormParam("telefone") String telefone,@FormParam("cpf") String cpf) {
        return "{\"result\":\"" + service.ValidaUser(telefone,cpf) + "\"}";
    }

    @POST
    @Consumes({"application/xml", "application/json", "application/x-www-form-urlencoded"}) 
    @Path("/xml")
    @Produces("application/xml")
    public String getHelloWorldXML(@FormParam("telefone") String telefone,@FormParam("cpf") String cpf) {
        return "<xml><result>" + service.ValidaUser(telefone,cpf) + "</result></xml>";
    }
}