
package br.com.senior.services.contasReceber;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.senior.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ContasreceberBaixarOutResultadoNumTit_QNAME = new QName("", "numTit");
    private final static QName _ContasreceberBaixarOutResultadoCnpjFilial_QNAME = new QName("", "cnpjFilial");
    private final static QName _ContasreceberBaixarOutResultadoResultado_QNAME = new QName("", "resultado");
    private final static QName _ContasreceberBaixarOutResultadoCodEmp_QNAME = new QName("", "codEmp");
    private final static QName _ContasreceberBaixarOutResultadoCodTpt_QNAME = new QName("", "codTpt");
    private final static QName _ContasreceberBaixarOutResultadoCodFil_QNAME = new QName("", "codFil");
    private final static QName _ContasreceberGerarOutMensagemRetorno_QNAME = new QName("", "mensagemRetorno");
    private final static QName _ContasreceberGerarOutTipoRetorno_QNAME = new QName("", "tipoRetorno");
    private final static QName _ContasreceberGerarOutErroExecucao_QNAME = new QName("", "erroExecucao");
    private final static QName _ContasreceberGerarInTitulosCodPor_QNAME = new QName("", "codPor");
    private final static QName _ContasreceberGerarInTitulosPerMul_QNAME = new QName("", "perMul");
    private final static QName _ContasreceberGerarInTitulosCodSac_QNAME = new QName("", "codSac");
    private final static QName _ContasreceberGerarInTitulosCheCta_QNAME = new QName("", "cheCta");
    private final static QName _ContasreceberGerarInTitulosJrsDia_QNAME = new QName("", "jrsDia");
    private final static QName _ContasreceberGerarInTitulosMulNeg_QNAME = new QName("", "mulNeg");
    private final static QName _ContasreceberGerarInTitulosCheBan_QNAME = new QName("", "cheBan");
    private final static QName _ContasreceberGerarInTitulosSeqCob_QNAME = new QName("", "seqCob");
    private final static QName _ContasreceberGerarInTitulosNumPrj_QNAME = new QName("", "numPrj");
    private final static QName _ContasreceberGerarInTitulosVlrDsc_QNAME = new QName("", "vlrDsc");
    private final static QName _ContasreceberGerarInTitulosCodTns_QNAME = new QName("", "codTns");
    private final static QName _ContasreceberGerarInTitulosDatEmi_QNAME = new QName("", "datEmi");
    private final static QName _ContasreceberGerarInTitulosDscNeg_QNAME = new QName("", "dscNeg");
    private final static QName _ContasreceberGerarInTitulosCheNum_QNAME = new QName("", "cheNum");
    private final static QName _ContasreceberGerarInTitulosCtaRed_QNAME = new QName("", "ctaRed");
    private final static QName _ContasreceberGerarInTitulosTitBan_QNAME = new QName("", "titBan");
    private final static QName _ContasreceberGerarInTitulosVlrOud_QNAME = new QName("", "vlrOud");
    private final static QName _ContasreceberGerarInTitulosCatTef_QNAME = new QName("", "catTef");
    private final static QName _ContasreceberGerarInTitulosCodMpt_QNAME = new QName("", "codMpt");
    private final static QName _ContasreceberGerarInTitulosCtaFin_QNAME = new QName("", "ctaFin");
    private final static QName _ContasreceberGerarInTitulosVlrDca_QNAME = new QName("", "vlrDca");
    private final static QName _ContasreceberGerarInTitulosVlrDcb_QNAME = new QName("", "vlrDcb");
    private final static QName _ContasreceberGerarInTitulosProJrs_QNAME = new QName("", "proJrs");
    private final static QName _ContasreceberGerarInTitulosDatDsc_QNAME = new QName("", "datDsc");
    private final static QName _ContasreceberGerarInTitulosPerJrs_QNAME = new QName("", "perJrs");
    private final static QName _ContasreceberGerarInTitulosCodIn1_QNAME = new QName("", "codIn1");
    private final static QName _ContasreceberGerarInTitulosCodIn2_QNAME = new QName("", "codIn2");
    private final static QName _ContasreceberGerarInTitulosVlrBco_QNAME = new QName("", "vlrBco");
    private final static QName _ContasreceberGerarInTitulosCodCli_QNAME = new QName("", "codCli");
    private final static QName _ContasreceberGerarInTitulosNumArb_QNAME = new QName("", "numArb");
    private final static QName _ContasreceberGerarInTitulosPerDsc_QNAME = new QName("", "perDsc");
    private final static QName _ContasreceberGerarInTitulosJrsNeg_QNAME = new QName("", "jrsNeg");
    private final static QName _ContasreceberGerarInTitulosCodRep_QNAME = new QName("", "codRep");
    private final static QName _ContasreceberGerarInTitulosPerCom_QNAME = new QName("", "perCom");
    private final static QName _ContasreceberGerarInTitulosCodCcu_QNAME = new QName("", "codCcu");
    private final static QName _ContasreceberGerarInTitulosObsTcr_QNAME = new QName("", "obsTcr");
    private final static QName _ContasreceberGerarInTitulosDatNeg_QNAME = new QName("", "datNeg");
    private final static QName _ContasreceberGerarInTitulosVlrOri_QNAME = new QName("", "vlrOri");
    private final static QName _ContasreceberGerarInTitulosCodMoe_QNAME = new QName("", "codMoe");
    private final static QName _ContasreceberGerarInTitulosTolJrs_QNAME = new QName("", "tolJrs");
    private final static QName _ContasreceberGerarInTitulosCpgNeg_QNAME = new QName("", "cpgNeg");
    private final static QName _ContasreceberGerarInTitulosTolDsc_QNAME = new QName("", "tolDsc");
    private final static QName _ContasreceberGerarInTitulosDatEnt_QNAME = new QName("", "datEnt");
    private final static QName _ContasreceberGerarInTitulosNsuTef_QNAME = new QName("", "nsuTef");
    private final static QName _ContasreceberGerarInTitulosTaxNeg_QNAME = new QName("", "taxNeg");
    private final static QName _ContasreceberGerarInTitulosCheAge_QNAME = new QName("", "cheAge");
    private final static QName _ContasreceberGerarInTitulosVlrCom_QNAME = new QName("", "vlrCom");
    private final static QName _ContasreceberGerarInTitulosDatPpt_QNAME = new QName("", "datPpt");
    private final static QName _ContasreceberGerarInTitulosComRec_QNAME = new QName("", "comRec");
    private final static QName _ContasreceberGerarInTitulosCodFpj_QNAME = new QName("", "codFpj");
    private final static QName _ContasreceberGerarInTitulosOutNeg_QNAME = new QName("", "outNeg");
    private final static QName _ContasreceberGerarInTitulosCodCrp_QNAME = new QName("", "codCrp");
    private final static QName _ContasreceberGerarInTitulosCodNtg_QNAME = new QName("", "codNtg");
    private final static QName _ContasreceberGerarInTitulosTolMul_QNAME = new QName("", "tolMul");
    private final static QName _ContasreceberGerarInTitulosCodCrt_QNAME = new QName("", "codCrt");
    private final static QName _ContasreceberGerarInTitulosVctOri_QNAME = new QName("", "vctOri");
    private final static QName _ContasreceberGerarInTitulosTipJrs_QNAME = new QName("", "tipJrs");
    private final static QName _ContasreceberGerarInTitulosCodFpg_QNAME = new QName("", "codFpg");
    private final static QName _ContasreceberConsultarOutTitulosNumNff_QNAME = new QName("", "numNff");
    private final static QName _ContasreceberConsultarOutTitulosFilNff_QNAME = new QName("", "filNff");
    private final static QName _ContasreceberBaixarInDataBuild_QNAME = new QName("", "dataBuild");
    private final static QName _ContasreceberBaixarInFlowName_QNAME = new QName("", "flowName");
    private final static QName _ContasreceberBaixarInFlowInstanceID_QNAME = new QName("", "flowInstanceID");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberTptRlc_QNAME = new QName("", "tptRlc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDatCco_QNAME = new QName("", "datCco");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberNumDoc_QNAME = new QName("", "numDoc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberSeqRlc_QNAME = new QName("", "seqRlc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDatMov_QNAME = new QName("", "datMov");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrJrs_QNAME = new QName("", "vlrJrs");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDiaJrs_QNAME = new QName("", "diaJrs");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrOde_QNAME = new QName("", "vlrOde");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDiaAtr_QNAME = new QName("", "diaAtr");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberNumRlc_QNAME = new QName("", "numRlc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDatLib_QNAME = new QName("", "datLib");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberTnsBxa_QNAME = new QName("", "tnsBxa");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberFilRlc_QNAME = new QName("", "filRlc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrCor_QNAME = new QName("", "vlrCor");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberEmpCco_QNAME = new QName("", "empCco");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrMul_QNAME = new QName("", "vlrMul");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberSeqCco_QNAME = new QName("", "seqCco");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberNumCco_QNAME = new QName("", "numCco");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberSeqMov_QNAME = new QName("", "seqMov");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberCotMcr_QNAME = new QName("", "cotMcr");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberDatPgt_QNAME = new QName("", "datPgt");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrOac_QNAME = new QName("", "vlrOac");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrLiq_QNAME = new QName("", "vlrLiq");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrMov_QNAME = new QName("", "vlrMov");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberVlrEnc_QNAME = new QName("", "vlrEnc");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberRateiosObsRat_QNAME = new QName("", "obsRat");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberRateiosPerCta_QNAME = new QName("", "perCta");
    private final static QName _ContasreceberBaixarInBaixaTituloReceberRateiosPerRat_QNAME = new QName("", "perRat");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.senior.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ContasreceberGerarOut }
     * 
     */
    public ContasreceberGerarOut createContasreceberGerarOut() {
        return new ContasreceberGerarOut();
    }

    /**
     * Create an instance of {@link ContasreceberGerarOutResultado }
     * 
     */
    public ContasreceberGerarOutResultado createContasreceberGerarOutResultado() {
        return new ContasreceberGerarOutResultado();
    }

    /**
     * Create an instance of {@link ContasreceberBaixarIn }
     * 
     */
    public ContasreceberBaixarIn createContasreceberBaixarIn() {
        return new ContasreceberBaixarIn();
    }

    /**
     * Create an instance of {@link ContasreceberConsultarOut }
     * 
     */
    public ContasreceberConsultarOut createContasreceberConsultarOut() {
        return new ContasreceberConsultarOut();
    }

    /**
     * Create an instance of {@link ContasreceberConsultarOutTitulos }
     * 
     */
    public ContasreceberConsultarOutTitulos createContasreceberConsultarOutTitulos() {
        return new ContasreceberConsultarOutTitulos();
    }

    /**
     * Create an instance of {@link ContasreceberConsultarIn }
     * 
     */
    public ContasreceberConsultarIn createContasreceberConsultarIn() {
        return new ContasreceberConsultarIn();
    }

    /**
     * Create an instance of {@link ContasreceberBaixarOutResultado }
     * 
     */
    public ContasreceberBaixarOutResultado createContasreceberBaixarOutResultado() {
        return new ContasreceberBaixarOutResultado();
    }

    /**
     * Create an instance of {@link ContasreceberBaixarInBaixaTituloReceberRateios }
     * 
     */
    public ContasreceberBaixarInBaixaTituloReceberRateios createContasreceberBaixarInBaixaTituloReceberRateios() {
        return new ContasreceberBaixarInBaixaTituloReceberRateios();
    }

    /**
     * Create an instance of {@link ContasreceberGerarIn }
     * 
     */
    public ContasreceberGerarIn createContasreceberGerarIn() {
        return new ContasreceberGerarIn();
    }

    /**
     * Create an instance of {@link ContasreceberBaixarOut }
     * 
     */
    public ContasreceberBaixarOut createContasreceberBaixarOut() {
        return new ContasreceberBaixarOut();
    }

    /**
     * Create an instance of {@link ContasreceberGerarInTitulos }
     * 
     */
    public ContasreceberGerarInTitulos createContasreceberGerarInTitulos() {
        return new ContasreceberGerarInTitulos();
    }

    /**
     * Create an instance of {@link ContasreceberGerarInTitulosRateios }
     * 
     */
    public ContasreceberGerarInTitulosRateios createContasreceberGerarInTitulosRateios() {
        return new ContasreceberGerarInTitulosRateios();
    }

    /**
     * Create an instance of {@link ContasreceberBaixarInBaixaTituloReceber }
     * 
     */
    public ContasreceberBaixarInBaixaTituloReceber createContasreceberBaixarInBaixaTituloReceber() {
        return new ContasreceberBaixarInBaixaTituloReceber();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<String> createContasreceberBaixarOutResultadoNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<String> createContasreceberBaixarOutResultadoCnpjFilial(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCnpjFilial_QNAME, String.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resultado", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<String> createContasreceberBaixarOutResultadoResultado(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoResultado_QNAME, String.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<Integer> createContasreceberBaixarOutResultadoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, Integer.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<String> createContasreceberBaixarOutResultadoCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberBaixarOutResultado.class)
    public JAXBElement<String> createContasreceberBaixarOutResultadoCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContasreceberGerarOut.class)
    public JAXBElement<String> createContasreceberGerarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutMensagemRetorno_QNAME, String.class, ContasreceberGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContasreceberGerarOut.class)
    public JAXBElement<String> createContasreceberGerarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutTipoRetorno_QNAME, String.class, ContasreceberGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContasreceberGerarOut.class)
    public JAXBElement<String> createContasreceberGerarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutErroExecucao_QNAME, String.class, ContasreceberGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<String> createContasreceberGerarOutResultadoNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<String> createContasreceberGerarOutResultadoCnpjFilial(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCnpjFilial_QNAME, String.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resultado", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<String> createContasreceberGerarOutResultadoResultado(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoResultado_QNAME, String.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<Integer> createContasreceberGerarOutResultadoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, Integer.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<String> createContasreceberGerarOutResultadoCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberGerarOutResultado.class)
    public JAXBElement<String> createContasreceberGerarOutResultadoCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodPor(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodPor_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perMul", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosPerMul(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerMul_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSac", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodSac(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodSac_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheCta", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCheCta(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheCta_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<Integer> createContasreceberGerarInTitulosCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, Integer.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsDia", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosJrsDia(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosJrsDia_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosMulNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosMulNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheBan", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCheBan(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheBan_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqCob", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosSeqCob(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosSeqCob_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosNumPrj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumPrj_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDsc_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodTns(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodTns_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEmi", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDatEmi(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatEmi_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dscNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDscNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDscNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheNum", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCheNum(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheNum_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCtaRed(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaRed_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titBan", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTitBan(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTitBan_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOud", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrOud(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrOud_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "catTef", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCatTef(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCatTef_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMpt", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodMpt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodMpt_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCnpjFilial(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCnpjFilial_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCtaFin(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaFin_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDca", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrDca(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDca_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDcb", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrDcb(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDcb_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "proJrs", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosProJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosProJrs_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datDsc", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDatDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatDsc_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perJrs", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosPerJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerJrs_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codIn1", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodIn1(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodIn1_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codIn2", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodIn2(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodIn2_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBco", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrBco(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrBco_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodCli(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCli_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numArb", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosNumArb(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumArb_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosPerDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerDsc_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosJrsNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosJrsNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRep", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodRep(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodRep_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCom", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosPerCom(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerCom_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodCcu(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCcu_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsTcr", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosObsTcr(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosObsTcr_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDatNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOri", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrOri(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrOri_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMoe", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodMoe(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodMoe_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolJrs", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTolJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolJrs_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cpgNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCpgNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCpgNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTolDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolDsc_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEnt", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDatEnt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatEnt_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nsuTef", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosNsuTef(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNsuTef_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "taxNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTaxNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTaxNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheAge", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCheAge(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheAge_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrCom", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVlrCom(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrCom_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPpt", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosDatPpt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatPpt_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "comRec", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosComRec(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosComRec_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodFpj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpj_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "outNeg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosOutNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosOutNeg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodCrp(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCrp_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNtg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodNtg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodNtg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolMul", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTolMul(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolMul_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodCrt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCrt_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vctOri", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosVctOri(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVctOri_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipJrs", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosTipJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTipJrs_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContasreceberGerarInTitulos.class)
    public JAXBElement<String> createContasreceberGerarInTitulosCodFpg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpg_QNAME, String.class, ContasreceberGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodPor(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodPor_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perMul", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosPerMul(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerMul_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSac", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodSac(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodSac_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheCta", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCheCta(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheCta_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<Integer> createContasreceberConsultarOutTitulosCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, Integer.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsDia", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosJrsDia(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosJrsDia_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosMulNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosMulNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numNff", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosNumNff(String value) {
        return new JAXBElement<String>(_ContasreceberConsultarOutTitulosNumNff_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheBan", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCheBan(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheBan_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosNumPrj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumPrj_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDsc_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodTns(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodTns_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEmi", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDatEmi(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatEmi_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dscNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDscNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDscNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheNum", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCheNum(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheNum_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCtaRed(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaRed_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titBan", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTitBan(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTitBan_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOud", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrOud(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrOud_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "catTef", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCatTef(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCatTef_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMpt", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodMpt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodMpt_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCnpjFilial(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCnpjFilial_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCtaFin(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaFin_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDca", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrDca(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDca_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDcb", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrDcb(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDcb_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "proJrs", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosProJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosProJrs_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datDsc", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDatDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatDsc_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perJrs", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosPerJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerJrs_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codIn1", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodIn1(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodIn1_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codIn2", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodIn2(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodIn2_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBco", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrBco(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrBco_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodCli(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCli_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numArb", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosNumArb(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumArb_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosPerDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerDsc_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosJrsNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosJrsNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRep", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodRep(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodRep_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCom", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosPerCom(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosPerCom_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodCcu(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCcu_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsTcr", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosObsTcr(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosObsTcr_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDatNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOri", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrOri(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrOri_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMoe", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodMoe(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodMoe_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolJrs", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTolJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolJrs_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cpgNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCpgNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCpgNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTolDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolDsc_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEnt", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDatEnt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatEnt_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nsuTef", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosNsuTef(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNsuTef_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "taxNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTaxNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTaxNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cheAge", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCheAge(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCheAge_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrCom", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVlrCom(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrCom_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPpt", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosDatPpt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosDatPpt_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "filNff", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosFilNff(String value) {
        return new JAXBElement<String>(_ContasreceberConsultarOutTitulosFilNff_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "comRec", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosComRec(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosComRec_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodFpj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpj_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "outNeg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosOutNeg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosOutNeg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodCrp(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCrp_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNtg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodNtg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodNtg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolMul", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTolMul(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTolMul_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodCrt(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCrt_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vctOri", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosVctOri(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVctOri_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipJrs", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosTipJrs(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosTipJrs_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContasreceberConsultarOutTitulos.class)
    public JAXBElement<String> createContasreceberConsultarOutTitulosCodFpg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpg_QNAME, String.class, ContasreceberConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContasreceberConsultarOut.class)
    public JAXBElement<String> createContasreceberConsultarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutMensagemRetorno_QNAME, String.class, ContasreceberConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContasreceberConsultarOut.class)
    public JAXBElement<String> createContasreceberConsultarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutTipoRetorno_QNAME, String.class, ContasreceberConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContasreceberConsultarOut.class)
    public JAXBElement<String> createContasreceberConsultarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutErroExecucao_QNAME, String.class, ContasreceberConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContasreceberBaixarIn.class)
    public JAXBElement<String> createContasreceberBaixarInDataBuild(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInDataBuild_QNAME, String.class, ContasreceberBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContasreceberBaixarIn.class)
    public JAXBElement<String> createContasreceberBaixarInFlowName(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowName_QNAME, String.class, ContasreceberBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContasreceberBaixarIn.class)
    public JAXBElement<String> createContasreceberBaixarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowInstanceID_QNAME, String.class, ContasreceberBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numNff", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInNumNff(String value) {
        return new JAXBElement<String>(_ContasreceberConsultarOutTitulosNumNff_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInCodCli(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCli_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInCodEmp(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInDataBuild(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInDataBuild_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "filNff", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInFilNff(String value) {
        return new JAXBElement<String>(_ContasreceberConsultarOutTitulosFilNff_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInFlowName(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowName_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContasreceberConsultarIn.class)
    public JAXBElement<String> createContasreceberConsultarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowInstanceID_QNAME, String.class, ContasreceberConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tptRlc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberTptRlc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberTptRlc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datCco", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDatCco(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDatCco_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodEmp(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodEmp_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numDoc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberNumDoc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberNumDoc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodFil(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodFil_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqRlc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberSeqRlc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberSeqRlc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datMov", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDatMov(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDatMov_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrJrs", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrJrs(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrJrs_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "diaJrs", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDiaJrs(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDiaJrs_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOde", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrOde(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrOde_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodTpt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCodTpt_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "diaAtr", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDiaAtr(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDiaAtr_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberNumPrj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumPrj_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrDsc(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrDsc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodTns(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodTns_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numRlc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberNumRlc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberNumRlc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datLib", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDatLib(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDatLib_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberNumTit(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoNumTit_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCnpjFilial(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarOutResultadoCnpjFilial_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tnsBxa", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberTnsBxa(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberTnsBxa_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrCom", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrCom(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrCom_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "filRlc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberFilRlc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberFilRlc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrCor", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrCor(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrCor_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "empCco", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberEmpCco(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberEmpCco_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMul", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrMul(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrMul_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqCco", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberSeqCco(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberSeqCco_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBco", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrBco(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosVlrBco_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numCco", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberNumCco(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberNumCco_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodFpj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpj_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqMov", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberSeqMov(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberSeqMov_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotMcr", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCotMcr(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberCotMcr_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPgt", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberDatPgt(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberDatPgt_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOac", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrOac(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrOac_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrLiq", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrLiq(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrLiq_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMov", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrMov(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrMov_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrEnc", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberVlrEnc(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberVlrEnc_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContasreceberBaixarInBaixaTituloReceber.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberCodFpg(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpg_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContasreceberBaixarOut.class)
    public JAXBElement<String> createContasreceberBaixarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutMensagemRetorno_QNAME, String.class, ContasreceberBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContasreceberBaixarOut.class)
    public JAXBElement<String> createContasreceberBaixarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutTipoRetorno_QNAME, String.class, ContasreceberBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContasreceberBaixarOut.class)
    public JAXBElement<String> createContasreceberBaixarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContasreceberGerarOutErroExecucao_QNAME, String.class, ContasreceberBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContasreceberGerarIn.class)
    public JAXBElement<String> createContasreceberGerarInDataBuild(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInDataBuild_QNAME, String.class, ContasreceberGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContasreceberGerarIn.class)
    public JAXBElement<String> createContasreceberGerarInFlowName(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowName_QNAME, String.class, ContasreceberGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContasreceberGerarIn.class)
    public JAXBElement<String> createContasreceberGerarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInFlowInstanceID_QNAME, String.class, ContasreceberGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsRat", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosObsRat(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosObsRat_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCta", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosPerCta(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosPerCta_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosCodFpj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpj_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosCtaFin(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaFin_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosCodCcu(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCcu_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosNumPrj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumPrj_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRat", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosPerRat(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosPerRat_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContasreceberBaixarInBaixaTituloReceberRateios.class)
    public JAXBElement<String> createContasreceberBaixarInBaixaTituloReceberRateiosCtaRed(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaRed_QNAME, String.class, ContasreceberBaixarInBaixaTituloReceberRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsRat", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosObsRat(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosObsRat_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCta", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosPerCta(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosPerCta_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosCodFpj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodFpj_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosCtaFin(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaFin_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosCodCcu(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCodCcu_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosNumPrj(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosNumPrj_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRat", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosPerRat(String value) {
        return new JAXBElement<String>(_ContasreceberBaixarInBaixaTituloReceberRateiosPerRat_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContasreceberGerarInTitulosRateios.class)
    public JAXBElement<String> createContasreceberGerarInTitulosRateiosCtaRed(String value) {
        return new JAXBElement<String>(_ContasreceberGerarInTitulosCtaRed_QNAME, String.class, ContasreceberGerarInTitulosRateios.class, value);
    }

}
