package br.com.senior.Consultas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.senior.Utils.utils;
import br.com.senior.poc.RN.TituloRN;
import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.model.TipoTitulo;
import br.com.senior.poc.model.Titulo;
import br.com.senior.poc.sessao.sessao;
import br.com.senior.services.contasPagar.ContaspagarConsultarIn;
import br.com.senior.services.contasPagar.ContaspagarConsultarOut;
import br.com.senior.services.contasPagar.ContaspagarConsultarOutTitulos;
import br.com.senior.services.contasPagar.G5SeniorServices;
import br.com.senior.services.contasPagar.SapiensSynccomSeniorG5CoIntPadraoTitulosContaspagar;


public class BuscaContasPagar {

	public static G5SeniorServices service = new G5SeniorServices();

	public static void buscaSalvaContasPagar(Fornecedor fornecedor, sessao session) {
		SapiensSynccomSeniorG5CoIntPadraoTitulosContaspagar envio = service.getSapiensSynccomSeniorG5CoIntPadraoTitulosContaspagarPort();
		ContaspagarConsultarIn contas = new ContaspagarConsultarIn();
		contas.setCodEmp(utils.getSTRJAX(String.valueOf(session.getEmpresa()), "codEmp"));
		contas.setCodFil(utils.getSTRJAX(String.valueOf(session.getFilial()), "codFil"));
		contas.setCodFor(utils.getSTRJAX(String.valueOf(fornecedor.getCodigo()), "codFor"));
		ContaspagarConsultarOut retorno = envio.consultar(session.getUsuario(), session.getSenha(),	session.getEncript(), contas);
		 for (ContaspagarConsultarOutTitulos titulos : retorno.getTitulos()) {
			TituloRN rn = new TituloRN();
			Titulo tit = new Titulo();
			tit.setValor(Double.parseDouble(titulos.getVlrOri().getValue()));
			tit.setPessoa(fornecedor);
			tit.setTipoTitulo(TipoTitulo.PAGAR);
			tit.setIdTitulo(Long.parseLong(titulos.getNumTit().getValue()));
			try {
				tit.setDataLancamento(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(titulos.getDatEmi().getValue()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			try {
				rn.salvar(tit);
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	}

}
