
package br.com.senior.services.contasPagar;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.senior.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ContaspagarConsultarOutTitulosCodPor_QNAME = new QName("", "codPor");
    private final static QName _ContaspagarConsultarOutTitulosPerMul_QNAME = new QName("", "perMul");
    private final static QName _ContaspagarConsultarOutTitulosVlrApr_QNAME = new QName("", "vlrApr");
    private final static QName _ContaspagarConsultarOutTitulosCodEmp_QNAME = new QName("", "codEmp");
    private final static QName _ContaspagarConsultarOutTitulosCodFor_QNAME = new QName("", "codFor");
    private final static QName _ContaspagarConsultarOutTitulosCtaApr_QNAME = new QName("", "ctaApr");
    private final static QName _ContaspagarConsultarOutTitulosJrsDia_QNAME = new QName("", "jrsDia");
    private final static QName _ContaspagarConsultarOutTitulosMulNeg_QNAME = new QName("", "mulNeg");
    private final static QName _ContaspagarConsultarOutTitulosHorApr_QNAME = new QName("", "horApr");
    private final static QName _ContaspagarConsultarOutTitulosNumPrj_QNAME = new QName("", "numPrj");
    private final static QName _ContaspagarConsultarOutTitulosVlrDsc_QNAME = new QName("", "vlrDsc");
    private final static QName _ContaspagarConsultarOutTitulosCodTns_QNAME = new QName("", "codTns");
    private final static QName _ContaspagarConsultarOutTitulosDatEmi_QNAME = new QName("", "datEmi");
    private final static QName _ContaspagarConsultarOutTitulosMulApr_QNAME = new QName("", "mulApr");
    private final static QName _ContaspagarConsultarOutTitulosCtaRed_QNAME = new QName("", "ctaRed");
    private final static QName _ContaspagarConsultarOutTitulosLibApr_QNAME = new QName("", "libApr");
    private final static QName _ContaspagarConsultarOutTitulosTitBan_QNAME = new QName("", "titBan");
    private final static QName _ContaspagarConsultarOutTitulosCodMpt_QNAME = new QName("", "codMpt");
    private final static QName _ContaspagarConsultarOutTitulosCnpjFilial_QNAME = new QName("", "cnpjFilial");
    private final static QName _ContaspagarConsultarOutTitulosCtaFin_QNAME = new QName("", "ctaFin");
    private final static QName _ContaspagarConsultarOutTitulosJrsApr_QNAME = new QName("", "jrsApr");
    private final static QName _ContaspagarConsultarOutTitulosCodTri_QNAME = new QName("", "codTri");
    private final static QName _ContaspagarConsultarOutTitulosDatDsc_QNAME = new QName("", "datDsc");
    private final static QName _ContaspagarConsultarOutTitulosPerJrs_QNAME = new QName("", "perJrs");
    private final static QName _ContaspagarConsultarOutTitulosNumArb_QNAME = new QName("", "numArb");
    private final static QName _ContaspagarConsultarOutTitulosPerDsc_QNAME = new QName("", "perDsc");
    private final static QName _ContaspagarConsultarOutTitulosEmpApr_QNAME = new QName("", "empApr");
    private final static QName _ContaspagarConsultarOutTitulosJrsNeg_QNAME = new QName("", "jrsNeg");
    private final static QName _ContaspagarConsultarOutTitulosCotApr_QNAME = new QName("", "cotApr");
    private final static QName _ContaspagarConsultarOutTitulosDatApr_QNAME = new QName("", "datApr");
    private final static QName _ContaspagarConsultarOutTitulosCodBar_QNAME = new QName("", "codBar");
    private final static QName _ContaspagarConsultarOutTitulosSeqApr_QNAME = new QName("", "seqApr");
    private final static QName _ContaspagarConsultarOutTitulosCodUsu_QNAME = new QName("", "codUsu");
    private final static QName _ContaspagarConsultarOutTitulosObsTcp_QNAME = new QName("", "obsTcp");
    private final static QName _ContaspagarConsultarOutTitulosCodCcu_QNAME = new QName("", "codCcu");
    private final static QName _ContaspagarConsultarOutTitulosDatNeg_QNAME = new QName("", "datNeg");
    private final static QName _ContaspagarConsultarOutTitulosVlrOri_QNAME = new QName("", "vlrOri");
    private final static QName _ContaspagarConsultarOutTitulosOdeApr_QNAME = new QName("", "odeApr");
    private final static QName _ContaspagarConsultarOutTitulosCodFil_QNAME = new QName("", "codFil");
    private final static QName _ContaspagarConsultarOutTitulosCodBan_QNAME = new QName("", "codBan");
    private final static QName _ContaspagarConsultarOutTitulosCodMoe_QNAME = new QName("", "codMoe");
    private final static QName _ContaspagarConsultarOutTitulosCotEmi_QNAME = new QName("", "cotEmi");
    private final static QName _ContaspagarConsultarOutTitulosCodTpt_QNAME = new QName("", "codTpt");
    private final static QName _ContaspagarConsultarOutTitulosTolJrs_QNAME = new QName("", "tolJrs");
    private final static QName _ContaspagarConsultarOutTitulosCodAge_QNAME = new QName("", "codAge");
    private final static QName _ContaspagarConsultarOutTitulosNumTit_QNAME = new QName("", "numTit");
    private final static QName _ContaspagarConsultarOutTitulosTolDsc_QNAME = new QName("", "tolDsc");
    private final static QName _ContaspagarConsultarOutTitulosDatEnt_QNAME = new QName("", "datEnt");
    private final static QName _ContaspagarConsultarOutTitulosDscApr_QNAME = new QName("", "dscApr");
    private final static QName _ContaspagarConsultarOutTitulosDatPpt_QNAME = new QName("", "datPpt");
    private final static QName _ContaspagarConsultarOutTitulosFpgApr_QNAME = new QName("", "fpgApr");
    private final static QName _ContaspagarConsultarOutTitulosCodFpj_QNAME = new QName("", "codFpj");
    private final static QName _ContaspagarConsultarOutTitulosOutNeg_QNAME = new QName("", "outNeg");
    private final static QName _ContaspagarConsultarOutTitulosCodCrp_QNAME = new QName("", "codCrp");
    private final static QName _ContaspagarConsultarOutTitulosCodNtg_QNAME = new QName("", "codNtg");
    private final static QName _ContaspagarConsultarOutTitulosTolMul_QNAME = new QName("", "tolMul");
    private final static QName _ContaspagarConsultarOutTitulosUsuSit_QNAME = new QName("", "usuSit");
    private final static QName _ContaspagarConsultarOutTitulosCcbFor_QNAME = new QName("", "ccbFor");
    private final static QName _ContaspagarConsultarOutTitulosCodCrt_QNAME = new QName("", "codCrt");
    private final static QName _ContaspagarConsultarOutTitulosPgtApr_QNAME = new QName("", "pgtApr");
    private final static QName _ContaspagarConsultarOutTitulosPorAnt_QNAME = new QName("", "porAnt");
    private final static QName _ContaspagarConsultarOutTitulosVctOri_QNAME = new QName("", "vctOri");
    private final static QName _ContaspagarConsultarOutTitulosEncApr_QNAME = new QName("", "encApr");
    private final static QName _ContaspagarConsultarOutTitulosOacApr_QNAME = new QName("", "oacApr");
    private final static QName _ContaspagarConsultarOutTitulosCorApr_QNAME = new QName("", "corApr");
    private final static QName _ContaspagarConsultarOutTitulosTipJrs_QNAME = new QName("", "tipJrs");
    private final static QName _ContaspagarConsultarOutTitulosCodFpg_QNAME = new QName("", "codFpg");
    private final static QName _ContaspagarGerarInDataBuild_QNAME = new QName("", "dataBuild");
    private final static QName _ContaspagarGerarInFlowName_QNAME = new QName("", "flowName");
    private final static QName _ContaspagarGerarInFlowInstanceID_QNAME = new QName("", "flowInstanceID");
    private final static QName _ContaspagarGerarOutResultadoMsgRet_QNAME = new QName("", "msgRet");
    private final static QName _ContaspagarGerarOutResultadoResultado_QNAME = new QName("", "resultado");
    private final static QName _ContaspagarGerarOutResultadoIdeExt_QNAME = new QName("", "ideExt");
    private final static QName _ContaspagarGerarOutResultadoTipRet_QNAME = new QName("", "tipRet");
    private final static QName _ContaspagarConsultarOutMensagemRetorno_QNAME = new QName("", "mensagemRetorno");
    private final static QName _ContaspagarConsultarOutTipoRetorno_QNAME = new QName("", "tipoRetorno");
    private final static QName _ContaspagarConsultarOutErroExecucao_QNAME = new QName("", "erroExecucao");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarSeqChe_QNAME = new QName("", "seqChe");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarNumPdv_QNAME = new QName("", "numPdv");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarNumDoc_QNAME = new QName("", "numDoc");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarDatMov_QNAME = new QName("", "datMov");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrJrs_QNAME = new QName("", "vlrJrs");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarAgrPre_QNAME = new QName("", "agrPre");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrOde_QNAME = new QName("", "vlrOde");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarIndCan_QNAME = new QName("", "indCan");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarDatLib_QNAME = new QName("", "datLib");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarTnsCxb_QNAME = new QName("", "tnsCxb");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrCor_QNAME = new QName("", "vlrCor");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrMul_QNAME = new QName("", "vlrMul");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarTipPgt_QNAME = new QName("", "tipPgt");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarNumCco_QNAME = new QName("", "numCco");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarCotMcp_QNAME = new QName("", "cotMcp");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarDatPgt_QNAME = new QName("", "datPgt");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrOac_QNAME = new QName("", "vlrOac");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrLiq_QNAME = new QName("", "vlrLiq");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrMov_QNAME = new QName("", "vlrMov");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarVlrEnc_QNAME = new QName("", "vlrEnc");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarIndAbt_QNAME = new QName("", "indAbt");
    private final static QName _ContaspagarBaixarInBaixaTituloPagarObsMcp_QNAME = new QName("", "obsMcp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.senior.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ContaspagarConsultarOutTitulos }
     * 
     */
    public ContaspagarConsultarOutTitulos createContaspagarConsultarOutTitulos() {
        return new ContaspagarConsultarOutTitulos();
    }

    /**
     * Create an instance of {@link ContaspagarBaixarOut }
     * 
     */
    public ContaspagarBaixarOut createContaspagarBaixarOut() {
        return new ContaspagarBaixarOut();
    }

    /**
     * Create an instance of {@link ContaspagarGerarInTitulos }
     * 
     */
    public ContaspagarGerarInTitulos createContaspagarGerarInTitulos() {
        return new ContaspagarGerarInTitulos();
    }

    /**
     * Create an instance of {@link ContaspagarBaixarIn }
     * 
     */
    public ContaspagarBaixarIn createContaspagarBaixarIn() {
        return new ContaspagarBaixarIn();
    }

    /**
     * Create an instance of {@link ContaspagarGerarOutResultado }
     * 
     */
    public ContaspagarGerarOutResultado createContaspagarGerarOutResultado() {
        return new ContaspagarGerarOutResultado();
    }

    /**
     * Create an instance of {@link ContaspagarBaixarInBaixaTituloPagar }
     * 
     */
    public ContaspagarBaixarInBaixaTituloPagar createContaspagarBaixarInBaixaTituloPagar() {
        return new ContaspagarBaixarInBaixaTituloPagar();
    }

    /**
     * Create an instance of {@link ContaspagarConsultarOut }
     * 
     */
    public ContaspagarConsultarOut createContaspagarConsultarOut() {
        return new ContaspagarConsultarOut();
    }

    /**
     * Create an instance of {@link ContaspagarConsultarIn }
     * 
     */
    public ContaspagarConsultarIn createContaspagarConsultarIn() {
        return new ContaspagarConsultarIn();
    }

    /**
     * Create an instance of {@link ContaspagarBaixarOutResultado }
     * 
     */
    public ContaspagarBaixarOutResultado createContaspagarBaixarOutResultado() {
        return new ContaspagarBaixarOutResultado();
    }

    /**
     * Create an instance of {@link ContaspagarGerarOut }
     * 
     */
    public ContaspagarGerarOut createContaspagarGerarOut() {
        return new ContaspagarGerarOut();
    }

    /**
     * Create an instance of {@link ContaspagarGerarIn }
     * 
     */
    public ContaspagarGerarIn createContaspagarGerarIn() {
        return new ContaspagarGerarIn();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodPor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodPor_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perMul", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosPerMul(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerMul_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosVlrApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodEmp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCtaApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsDia", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosJrsDia(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsDia_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulNeg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosMulNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosMulNeg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosHorApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosHorApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosNumPrj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumPrj_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosVlrDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrDsc_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodTns(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTns_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEmi", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatEmi(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatEmi_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosMulApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosMulApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCtaRed(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaRed_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "libApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosLibApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosLibApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titBan", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosTitBan(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTitBan_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMpt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodMpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodMpt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCnpjFilial(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCnpjFilial_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCtaFin(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaFin_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosJrsApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTri", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodTri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTri_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datDsc", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatDsc_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perJrs", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosPerJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerJrs_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numArb", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosNumArb(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumArb_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosPerDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerDsc_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "empApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosEmpApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosEmpApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsNeg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosJrsNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsNeg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCotApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCotApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBar", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodBar(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodBar_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosSeqApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosSeqApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codUsu", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodUsu(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodUsu_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsTcp", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosObsTcp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosObsTcp_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodCcu(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCcu_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNeg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatNeg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOri", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosVlrOri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrOri_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "odeApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosOdeApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOdeApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodBan(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodBan_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMoe", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodMoe(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodMoe_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotEmi", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCotEmi(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCotEmi_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodTpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTpt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolJrs", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosTolJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolJrs_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodAge(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodAge_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosNumTit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumTit_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosTolDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolDsc_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEnt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatEnt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatEnt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dscApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDscApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDscApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPpt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosDatPpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatPpt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fpgApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosFpgApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosFpgApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodFpj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpj_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "outNeg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosOutNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOutNeg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodCrp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCrp_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNtg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodNtg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodNtg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolMul", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosTolMul(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolMul_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "usuSit", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosUsuSit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosUsuSit_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCcbFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCcbFor_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodCrt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCrt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosPgtApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPgtApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "porAnt", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosPorAnt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPorAnt_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vctOri", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosVctOri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVctOri_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "encApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosEncApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosEncApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "oacApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosOacApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOacApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "corApr", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCorApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCorApr_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipJrs", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosTipJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTipJrs_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContaspagarConsultarOutTitulos.class)
    public JAXBElement<String> createContaspagarConsultarOutTitulosCodFpg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpg_QNAME, String.class, ContaspagarConsultarOutTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContaspagarGerarIn.class)
    public JAXBElement<String> createContaspagarGerarInDataBuild(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInDataBuild_QNAME, String.class, ContaspagarGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContaspagarGerarIn.class)
    public JAXBElement<String> createContaspagarGerarInFlowName(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowName_QNAME, String.class, ContaspagarGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContaspagarGerarIn.class)
    public JAXBElement<String> createContaspagarGerarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowInstanceID_QNAME, String.class, ContaspagarGerarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoNumTit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumTit_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoCnpjFilial(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCnpjFilial_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgRet", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoMsgRet(String value) {
        return new JAXBElement<String>(_ContaspagarGerarOutResultadoMsgRet_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resultado", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoResultado(String value) {
        return new JAXBElement<String>(_ContaspagarGerarOutResultadoResultado_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<Integer> createContaspagarGerarOutResultadoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, Integer.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoCodTpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTpt_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<Integer> createContaspagarGerarOutResultadoIdeExt(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoIdeExt_QNAME, Integer.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<String> createContaspagarGerarOutResultadoCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipRet", scope = ContaspagarGerarOutResultado.class)
    public JAXBElement<Integer> createContaspagarGerarOutResultadoTipRet(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoTipRet_QNAME, Integer.class, ContaspagarGerarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContaspagarConsultarOut.class)
    public JAXBElement<String> createContaspagarConsultarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutMensagemRetorno_QNAME, String.class, ContaspagarConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContaspagarConsultarOut.class)
    public JAXBElement<String> createContaspagarConsultarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTipoRetorno_QNAME, String.class, ContaspagarConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContaspagarConsultarOut.class)
    public JAXBElement<String> createContaspagarConsultarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutErroExecucao_QNAME, String.class, ContaspagarConsultarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContaspagarBaixarIn.class)
    public JAXBElement<String> createContaspagarBaixarInDataBuild(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInDataBuild_QNAME, String.class, ContaspagarBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContaspagarBaixarIn.class)
    public JAXBElement<String> createContaspagarBaixarInFlowName(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowName_QNAME, String.class, ContaspagarBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContaspagarBaixarIn.class)
    public JAXBElement<String> createContaspagarBaixarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowInstanceID_QNAME, String.class, ContaspagarBaixarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContaspagarGerarOut.class)
    public JAXBElement<String> createContaspagarGerarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutMensagemRetorno_QNAME, String.class, ContaspagarGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContaspagarGerarOut.class)
    public JAXBElement<String> createContaspagarGerarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTipoRetorno_QNAME, String.class, ContaspagarGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContaspagarGerarOut.class)
    public JAXBElement<String> createContaspagarGerarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutErroExecucao_QNAME, String.class, ContaspagarGerarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqChe", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarSeqChe(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarSeqChe_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPdv", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<Integer> createContaspagarBaixarInBaixaTituloPagarNumPdv(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarBaixarInBaixaTituloPagarNumPdv_QNAME, Integer.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodCcu(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCcu_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodEmp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<Integer> createContaspagarBaixarInBaixaTituloPagarIdeExt(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoIdeExt_QNAME, Integer.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numDoc", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarNumDoc(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarNumDoc_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datMov", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarDatMov(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarDatMov_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodBan(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodBan_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrJrs", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrJrs(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrJrs_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "agrPre", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarAgrPre(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarAgrPre_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOde", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrOde(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrOde_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indCan", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarIndCan(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarIndCan_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodTpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTpt_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarNumPrj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumPrj_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrDsc_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodTns(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTns_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodAge(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodAge_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCtaRed(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaRed_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datLib", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarDatLib(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarDatLib_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarNumTit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumTit_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tnsCxb", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarTnsCxb(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarTnsCxb_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCnpjFilial(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCnpjFilial_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCtaFin(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaFin_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrCor", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrCor(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrCor_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMul", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrMul(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrMul_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipPgt", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarTipPgt(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarTipPgt_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numCco", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarNumCco(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarNumCco_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodFpj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpj_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotMcp", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCotMcp(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarCotMcp_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCcbFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCcbFor_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPgt", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarDatPgt(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarDatPgt_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOac", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrOac(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrOac_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrLiq", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrLiq(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrLiq_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMov", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrMov(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrMov_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrEnc", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarVlrEnc(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarVlrEnc_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indAbt", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarIndAbt(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarIndAbt_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsMcp", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarObsMcp(String value) {
        return new JAXBElement<String>(_ContaspagarBaixarInBaixaTituloPagarObsMcp_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContaspagarBaixarInBaixaTituloPagar.class)
    public JAXBElement<String> createContaspagarBaixarInBaixaTituloPagarCodFpg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpg_QNAME, String.class, ContaspagarBaixarInBaixaTituloPagar.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoNumTit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumTit_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoCnpjFilial(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCnpjFilial_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgRet", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoMsgRet(String value) {
        return new JAXBElement<String>(_ContaspagarGerarOutResultadoMsgRet_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "resultado", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoResultado(String value) {
        return new JAXBElement<String>(_ContaspagarGerarOutResultadoResultado_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<Integer> createContaspagarBaixarOutResultadoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, Integer.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoCodTpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTpt_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<Integer> createContaspagarBaixarOutResultadoIdeExt(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoIdeExt_QNAME, Integer.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<String> createContaspagarBaixarOutResultadoCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipRet", scope = ContaspagarBaixarOutResultado.class)
    public JAXBElement<Integer> createContaspagarBaixarOutResultadoTipRet(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoTipRet_QNAME, Integer.class, ContaspagarBaixarOutResultado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = ContaspagarBaixarOut.class)
    public JAXBElement<String> createContaspagarBaixarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutMensagemRetorno_QNAME, String.class, ContaspagarBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = ContaspagarBaixarOut.class)
    public JAXBElement<String> createContaspagarBaixarOutTipoRetorno(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTipoRetorno_QNAME, String.class, ContaspagarBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = ContaspagarBaixarOut.class)
    public JAXBElement<String> createContaspagarBaixarOutErroExecucao(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutErroExecucao_QNAME, String.class, ContaspagarBaixarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInCodEmp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dataBuild", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInDataBuild(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInDataBuild_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInFlowName(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowName_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = ContaspagarConsultarIn.class)
    public JAXBElement<String> createContaspagarConsultarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_ContaspagarGerarInFlowInstanceID_QNAME, String.class, ContaspagarConsultarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodPor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodPor_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perMul", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosPerMul(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerMul_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosVlrApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodEmp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodEmp_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFor_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCtaApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsDia", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosJrsDia(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsDia_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulNeg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosMulNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosMulNeg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosHorApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosHorApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numPrj", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosNumPrj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumPrj_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrDsc", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosVlrDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrDsc_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTns", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodTns(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTns_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEmi", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatEmi(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatEmi_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mulApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosMulApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosMulApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCtaRed(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaRed_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "libApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosLibApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosLibApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "titBan", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosTitBan(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTitBan_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMpt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodMpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodMpt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cnpjFilial", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCnpjFilial(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCnpjFilial_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFin", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCtaFin(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCtaFin_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosJrsApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTri", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodTri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTri_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datDsc", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatDsc_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perJrs", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosPerJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerJrs_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numArb", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosNumArb(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumArb_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosPerDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPerDsc_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "empApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosEmpApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosEmpApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "jrsNeg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosJrsNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosJrsNeg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCotApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCotApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBar", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodBar(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodBar_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosSeqApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosSeqApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codUsu", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodUsu(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodUsu_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsTcp", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosObsTcp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosObsTcp_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcu", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodCcu(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCcu_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNeg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatNeg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrOri", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosVlrOri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVlrOri_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "odeApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosOdeApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOdeApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<Integer> createContaspagarGerarInTitulosIdeExt(Integer value) {
        return new JAXBElement<Integer>(_ContaspagarGerarOutResultadoIdeExt_QNAME, Integer.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodFil(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFil_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodBan(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodBan_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMoe", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodMoe(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodMoe_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cotEmi", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCotEmi(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCotEmi_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodTpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodTpt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolJrs", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosTolJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolJrs_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodAge(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodAge_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numTit", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosNumTit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosNumTit_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosTolDsc(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolDsc_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datEnt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatEnt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatEnt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "dscApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDscApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDscApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datPpt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosDatPpt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosDatPpt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fpgApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosFpgApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosFpgApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpj", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodFpj(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpj_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "outNeg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosOutNeg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOutNeg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodCrp(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCrp_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNtg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodNtg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodNtg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolMul", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosTolMul(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTolMul_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "usuSit", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosUsuSit(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosUsuSit_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCcbFor(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCcbFor_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodCrt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodCrt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosPgtApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPgtApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "porAnt", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosPorAnt(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosPorAnt_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vctOri", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosVctOri(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosVctOri_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "encApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosEncApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosEncApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "oacApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosOacApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosOacApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "corApr", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCorApr(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCorApr_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipJrs", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosTipJrs(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosTipJrs_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = ContaspagarGerarInTitulos.class)
    public JAXBElement<String> createContaspagarGerarInTitulosCodFpg(String value) {
        return new JAXBElement<String>(_ContaspagarConsultarOutTitulosCodFpg_QNAME, String.class, ContaspagarGerarInTitulos.class, value);
    }

}
