
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedoresCep complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedoresCep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baiFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cepIni" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cidFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPai" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRai" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cplEnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nenFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomPai" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomUfs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sigUfs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedoresCep", propOrder = {
    "baiFor",
    "cepFor",
    "cepIni",
    "cidFor",
    "codPai",
    "codRai",
    "cplEnd",
    "endFor",
    "nenFor",
    "nomPai",
    "nomUfs",
    "sigUfs"
})
public class FornecedoresExportar2OutGridFornecedoresCep {

    @XmlElementRef(name = "baiFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baiFor;
    @XmlElementRef(name = "cepFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cepFor;
    @XmlElementRef(name = "cepIni", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cepIni;
    @XmlElementRef(name = "cidFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cidFor;
    @XmlElementRef(name = "codPai", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPai;
    @XmlElementRef(name = "codRai", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codRai;
    @XmlElementRef(name = "cplEnd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cplEnd;
    @XmlElementRef(name = "endFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> endFor;
    @XmlElementRef(name = "nenFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nenFor;
    @XmlElementRef(name = "nomPai", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomPai;
    @XmlElementRef(name = "nomUfs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomUfs;
    @XmlElementRef(name = "sigUfs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sigUfs;

    /**
     * Obtem o valor da propriedade baiFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaiFor() {
        return baiFor;
    }

    /**
     * Define o valor da propriedade baiFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaiFor(JAXBElement<String> value) {
        this.baiFor = value;
    }

    /**
     * Obtem o valor da propriedade cepFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCepFor() {
        return cepFor;
    }

    /**
     * Define o valor da propriedade cepFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCepFor(JAXBElement<Integer> value) {
        this.cepFor = value;
    }

    /**
     * Obtem o valor da propriedade cepIni.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCepIni() {
        return cepIni;
    }

    /**
     * Define o valor da propriedade cepIni.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCepIni(JAXBElement<Integer> value) {
        this.cepIni = value;
    }

    /**
     * Obtem o valor da propriedade cidFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCidFor() {
        return cidFor;
    }

    /**
     * Define o valor da propriedade cidFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCidFor(JAXBElement<String> value) {
        this.cidFor = value;
    }

    /**
     * Obtem o valor da propriedade codPai.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPai() {
        return codPai;
    }

    /**
     * Define o valor da propriedade codPai.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPai(JAXBElement<String> value) {
        this.codPai = value;
    }

    /**
     * Obtem o valor da propriedade codRai.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodRai() {
        return codRai;
    }

    /**
     * Define o valor da propriedade codRai.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodRai(JAXBElement<Integer> value) {
        this.codRai = value;
    }

    /**
     * Obtem o valor da propriedade cplEnd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCplEnd() {
        return cplEnd;
    }

    /**
     * Define o valor da propriedade cplEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCplEnd(JAXBElement<String> value) {
        this.cplEnd = value;
    }

    /**
     * Obtem o valor da propriedade endFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEndFor() {
        return endFor;
    }

    /**
     * Define o valor da propriedade endFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEndFor(JAXBElement<String> value) {
        this.endFor = value;
    }

    /**
     * Obtem o valor da propriedade nenFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNenFor() {
        return nenFor;
    }

    /**
     * Define o valor da propriedade nenFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNenFor(JAXBElement<String> value) {
        this.nenFor = value;
    }

    /**
     * Obtem o valor da propriedade nomPai.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomPai() {
        return nomPai;
    }

    /**
     * Define o valor da propriedade nomPai.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomPai(JAXBElement<String> value) {
        this.nomPai = value;
    }

    /**
     * Obtem o valor da propriedade nomUfs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomUfs() {
        return nomUfs;
    }

    /**
     * Define o valor da propriedade nomUfs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomUfs(JAXBElement<String> value) {
        this.nomUfs = value;
    }

    /**
     * Obtem o valor da propriedade sigUfs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSigUfs() {
        return sigUfs;
    }

    /**
     * Define o valor da propriedade sigUfs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSigUfs(JAXBElement<String> value) {
        this.sigUfs = value;
    }

}
