
package br.com.senior.services.contasPagar;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contaspagarBaixarIn complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contaspagarBaixarIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baixaTituloPagar" type="{http://services.senior.com.br}contaspagarBaixarInBaixaTituloPagar" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dataBuild" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flowInstanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flowName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contaspagarBaixarIn", propOrder = {
    "baixaTituloPagar",
    "dataBuild",
    "flowInstanceID",
    "flowName"
})
public class ContaspagarBaixarIn {

    @XmlElement(nillable = true)
    protected List<ContaspagarBaixarInBaixaTituloPagar> baixaTituloPagar;
    @XmlElementRef(name = "dataBuild", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataBuild;
    @XmlElementRef(name = "flowInstanceID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> flowInstanceID;
    @XmlElementRef(name = "flowName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> flowName;

    /**
     * Gets the value of the baixaTituloPagar property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the baixaTituloPagar property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBaixaTituloPagar().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContaspagarBaixarInBaixaTituloPagar }
     * 
     * 
     */
    public List<ContaspagarBaixarInBaixaTituloPagar> getBaixaTituloPagar() {
        if (baixaTituloPagar == null) {
            baixaTituloPagar = new ArrayList<ContaspagarBaixarInBaixaTituloPagar>();
        }
        return this.baixaTituloPagar;
    }

    /**
     * Obtém o valor da propriedade dataBuild.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataBuild() {
        return dataBuild;
    }

    /**
     * Define o valor da propriedade dataBuild.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataBuild(JAXBElement<String> value) {
        this.dataBuild = value;
    }

    /**
     * Obtém o valor da propriedade flowInstanceID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFlowInstanceID() {
        return flowInstanceID;
    }

    /**
     * Define o valor da propriedade flowInstanceID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFlowInstanceID(JAXBElement<String> value) {
        this.flowInstanceID = value;
    }

    /**
     * Obtém o valor da propriedade flowName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFlowName() {
        return flowName;
    }

    /**
     * Define o valor da propriedade flowName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFlowName(JAXBElement<String> value) {
        this.flowName = value;
    }

}
