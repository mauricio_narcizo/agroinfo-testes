
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedoresObservacoes complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedoresObservacoes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="obsDat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsHor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsUsu" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="seqObs" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sitObs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solDat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solHor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solObs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solUsu" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="tipObs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedoresObservacoes", propOrder = {
    "codFor",
    "obsDat",
    "obsFor",
    "obsHor",
    "obsUsu",
    "seqObs",
    "sitObs",
    "solDat",
    "solHor",
    "solObs",
    "solUsu",
    "tipObs"
})
public class FornecedoresExportar2OutGridFornecedoresObservacoes {

    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "obsDat", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsDat;
    @XmlElementRef(name = "obsFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsFor;
    @XmlElementRef(name = "obsHor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsHor;
    @XmlElementRef(name = "obsUsu", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> obsUsu;
    @XmlElementRef(name = "seqObs", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqObs;
    @XmlElementRef(name = "sitObs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sitObs;
    @XmlElementRef(name = "solDat", type = JAXBElement.class, required = false)
    protected JAXBElement<String> solDat;
    @XmlElementRef(name = "solHor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> solHor;
    @XmlElementRef(name = "solObs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> solObs;
    @XmlElementRef(name = "solUsu", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> solUsu;
    @XmlElementRef(name = "tipObs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipObs;

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade obsDat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsDat() {
        return obsDat;
    }

    /**
     * Define o valor da propriedade obsDat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsDat(JAXBElement<String> value) {
        this.obsDat = value;
    }

    /**
     * Obtem o valor da propriedade obsFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsFor() {
        return obsFor;
    }

    /**
     * Define o valor da propriedade obsFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsFor(JAXBElement<String> value) {
        this.obsFor = value;
    }

    /**
     * Obtem o valor da propriedade obsHor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsHor() {
        return obsHor;
    }

    /**
     * Define o valor da propriedade obsHor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsHor(JAXBElement<String> value) {
        this.obsHor = value;
    }

    /**
     * Obtem o valor da propriedade obsUsu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getObsUsu() {
        return obsUsu;
    }

    /**
     * Define o valor da propriedade obsUsu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setObsUsu(JAXBElement<Double> value) {
        this.obsUsu = value;
    }

    /**
     * Obtem o valor da propriedade seqObs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqObs() {
        return seqObs;
    }

    /**
     * Define o valor da propriedade seqObs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqObs(JAXBElement<Integer> value) {
        this.seqObs = value;
    }

    /**
     * Obtem o valor da propriedade sitObs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSitObs() {
        return sitObs;
    }

    /**
     * Define o valor da propriedade sitObs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSitObs(JAXBElement<String> value) {
        this.sitObs = value;
    }

    /**
     * Obtem o valor da propriedade solDat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSolDat() {
        return solDat;
    }

    /**
     * Define o valor da propriedade solDat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSolDat(JAXBElement<String> value) {
        this.solDat = value;
    }

    /**
     * Obtem o valor da propriedade solHor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSolHor() {
        return solHor;
    }

    /**
     * Define o valor da propriedade solHor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSolHor(JAXBElement<String> value) {
        this.solHor = value;
    }

    /**
     * Obtem o valor da propriedade solObs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSolObs() {
        return solObs;
    }

    /**
     * Define o valor da propriedade solObs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSolObs(JAXBElement<String> value) {
        this.solObs = value;
    }

    /**
     * Obtem o valor da propriedade solUsu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSolUsu() {
        return solUsu;
    }

    /**
     * Define o valor da propriedade solUsu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSolUsu(JAXBElement<Double> value) {
        this.solUsu = value;
    }

    /**
     * Obtem o valor da propriedade tipObs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipObs() {
        return tipObs;
    }

    /**
     * Define o valor da propriedade tipObs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipObs(JAXBElement<String> value) {
        this.tipObs = value;
    }

}
