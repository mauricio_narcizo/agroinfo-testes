
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedoresCaracteristicas complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedoresCaracteristicas">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCcc" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codCcl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seqOrd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedoresCaracteristicas", propOrder = {
    "codCcc",
    "codCcl",
    "codFor",
    "seqOrd"
})
public class FornecedoresExportar2OutGridFornecedoresCaracteristicas {

    @XmlElementRef(name = "codCcc", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codCcc;
    @XmlElementRef(name = "codCcl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCcl;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "seqOrd", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqOrd;

    /**
     * Obtem o valor da propriedade codCcc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodCcc() {
        return codCcc;
    }

    /**
     * Define o valor da propriedade codCcc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodCcc(JAXBElement<Integer> value) {
        this.codCcc = value;
    }

    /**
     * Obtem o valor da propriedade codCcl.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCcl() {
        return codCcl;
    }

    /**
     * Define o valor da propriedade codCcl.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCcl(JAXBElement<String> value) {
        this.codCcl = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade seqOrd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqOrd() {
        return seqOrd;
    }

    /**
     * Define o valor da propriedade seqOrd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqOrd(JAXBElement<Integer> value) {
        this.seqOrd = value;
    }

}
