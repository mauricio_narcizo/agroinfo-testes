
package br.com.senior.services.contasPagar;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contaspagarBaixarInBaixaTituloPagar complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contaspagarBaixarInBaixaTituloPagar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agrPre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ccbFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjFilial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCcu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cotMcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaRed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datLib" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datMov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datPgt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ideExt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="indAbt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indCan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPdv" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numPrj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsMcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqChe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipPgt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tnsCxb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrCor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrEnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrLiq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrMov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contaspagarBaixarInBaixaTituloPagar", propOrder = {
    "agrPre",
    "ccbFor",
    "cnpjFilial",
    "codAge",
    "codBan",
    "codCcu",
    "codEmp",
    "codFil",
    "codFor",
    "codFpg",
    "codFpj",
    "codTns",
    "codTpt",
    "cotMcp",
    "ctaFin",
    "ctaRed",
    "datLib",
    "datMov",
    "datPgt",
    "ideExt",
    "indAbt",
    "indCan",
    "numCco",
    "numDoc",
    "numPdv",
    "numPrj",
    "numTit",
    "obsMcp",
    "seqChe",
    "tipPgt",
    "tnsCxb",
    "vlrCor",
    "vlrDsc",
    "vlrEnc",
    "vlrJrs",
    "vlrLiq",
    "vlrMov",
    "vlrMul",
    "vlrOac",
    "vlrOde"
})
public class ContaspagarBaixarInBaixaTituloPagar {

    @XmlElementRef(name = "agrPre", type = JAXBElement.class, required = false)
    protected JAXBElement<String> agrPre;
    @XmlElementRef(name = "ccbFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccbFor;
    @XmlElementRef(name = "cnpjFilial", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cnpjFilial;
    @XmlElementRef(name = "codAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAge;
    @XmlElementRef(name = "codBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBan;
    @XmlElementRef(name = "codCcu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCcu;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFil;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFor;
    @XmlElementRef(name = "codFpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpg;
    @XmlElementRef(name = "codFpj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpj;
    @XmlElementRef(name = "codTns", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTns;
    @XmlElementRef(name = "codTpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTpt;
    @XmlElementRef(name = "cotMcp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cotMcp;
    @XmlElementRef(name = "ctaFin", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaFin;
    @XmlElementRef(name = "ctaRed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaRed;
    @XmlElementRef(name = "datLib", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datLib;
    @XmlElementRef(name = "datMov", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datMov;
    @XmlElementRef(name = "datPgt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datPgt;
    @XmlElementRef(name = "ideExt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ideExt;
    @XmlElementRef(name = "indAbt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indAbt;
    @XmlElementRef(name = "indCan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indCan;
    @XmlElementRef(name = "numCco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numCco;
    @XmlElementRef(name = "numDoc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numDoc;
    @XmlElementRef(name = "numPdv", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numPdv;
    @XmlElementRef(name = "numPrj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numPrj;
    @XmlElementRef(name = "numTit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numTit;
    @XmlElementRef(name = "obsMcp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsMcp;
    @XmlElementRef(name = "seqChe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqChe;
    @XmlElementRef(name = "tipPgt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipPgt;
    @XmlElementRef(name = "tnsCxb", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tnsCxb;
    @XmlElementRef(name = "vlrCor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrCor;
    @XmlElementRef(name = "vlrDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDsc;
    @XmlElementRef(name = "vlrEnc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrEnc;
    @XmlElementRef(name = "vlrJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrJrs;
    @XmlElementRef(name = "vlrLiq", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrLiq;
    @XmlElementRef(name = "vlrMov", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrMov;
    @XmlElementRef(name = "vlrMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrMul;
    @XmlElementRef(name = "vlrOac", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOac;
    @XmlElementRef(name = "vlrOde", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOde;

    /**
     * Obtém o valor da propriedade agrPre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAgrPre() {
        return agrPre;
    }

    /**
     * Define o valor da propriedade agrPre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAgrPre(JAXBElement<String> value) {
        this.agrPre = value;
    }

    /**
     * Obtém o valor da propriedade ccbFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCcbFor() {
        return ccbFor;
    }

    /**
     * Define o valor da propriedade ccbFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCcbFor(JAXBElement<String> value) {
        this.ccbFor = value;
    }

    /**
     * Obtém o valor da propriedade cnpjFilial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCnpjFilial() {
        return cnpjFilial;
    }

    /**
     * Define o valor da propriedade cnpjFilial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCnpjFilial(JAXBElement<String> value) {
        this.cnpjFilial = value;
    }

    /**
     * Obtém o valor da propriedade codAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAge() {
        return codAge;
    }

    /**
     * Define o valor da propriedade codAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAge(JAXBElement<String> value) {
        this.codAge = value;
    }

    /**
     * Obtém o valor da propriedade codBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBan() {
        return codBan;
    }

    /**
     * Define o valor da propriedade codBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBan(JAXBElement<String> value) {
        this.codBan = value;
    }

    /**
     * Obtém o valor da propriedade codCcu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCcu() {
        return codCcu;
    }

    /**
     * Define o valor da propriedade codCcu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCcu(JAXBElement<String> value) {
        this.codCcu = value;
    }

    /**
     * Obtém o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<String> value) {
        this.codEmp = value;
    }

    /**
     * Obtém o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<String> value) {
        this.codFil = value;
    }

    /**
     * Obtém o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<String> value) {
        this.codFor = value;
    }

    /**
     * Obtém o valor da propriedade codFpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpg() {
        return codFpg;
    }

    /**
     * Define o valor da propriedade codFpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpg(JAXBElement<String> value) {
        this.codFpg = value;
    }

    /**
     * Obtém o valor da propriedade codFpj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpj() {
        return codFpj;
    }

    /**
     * Define o valor da propriedade codFpj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpj(JAXBElement<String> value) {
        this.codFpj = value;
    }

    /**
     * Obtém o valor da propriedade codTns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTns() {
        return codTns;
    }

    /**
     * Define o valor da propriedade codTns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTns(JAXBElement<String> value) {
        this.codTns = value;
    }

    /**
     * Obtém o valor da propriedade codTpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTpt() {
        return codTpt;
    }

    /**
     * Define o valor da propriedade codTpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTpt(JAXBElement<String> value) {
        this.codTpt = value;
    }

    /**
     * Obtém o valor da propriedade cotMcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCotMcp() {
        return cotMcp;
    }

    /**
     * Define o valor da propriedade cotMcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCotMcp(JAXBElement<String> value) {
        this.cotMcp = value;
    }

    /**
     * Obtém o valor da propriedade ctaFin.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaFin() {
        return ctaFin;
    }

    /**
     * Define o valor da propriedade ctaFin.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaFin(JAXBElement<String> value) {
        this.ctaFin = value;
    }

    /**
     * Obtém o valor da propriedade ctaRed.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaRed() {
        return ctaRed;
    }

    /**
     * Define o valor da propriedade ctaRed.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaRed(JAXBElement<String> value) {
        this.ctaRed = value;
    }

    /**
     * Obtém o valor da propriedade datLib.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatLib() {
        return datLib;
    }

    /**
     * Define o valor da propriedade datLib.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatLib(JAXBElement<String> value) {
        this.datLib = value;
    }

    /**
     * Obtém o valor da propriedade datMov.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatMov() {
        return datMov;
    }

    /**
     * Define o valor da propriedade datMov.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatMov(JAXBElement<String> value) {
        this.datMov = value;
    }

    /**
     * Obtém o valor da propriedade datPgt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatPgt() {
        return datPgt;
    }

    /**
     * Define o valor da propriedade datPgt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatPgt(JAXBElement<String> value) {
        this.datPgt = value;
    }

    /**
     * Obtém o valor da propriedade ideExt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdeExt() {
        return ideExt;
    }

    /**
     * Define o valor da propriedade ideExt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdeExt(JAXBElement<Integer> value) {
        this.ideExt = value;
    }

    /**
     * Obtém o valor da propriedade indAbt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndAbt() {
        return indAbt;
    }

    /**
     * Define o valor da propriedade indAbt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndAbt(JAXBElement<String> value) {
        this.indAbt = value;
    }

    /**
     * Obtém o valor da propriedade indCan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndCan() {
        return indCan;
    }

    /**
     * Define o valor da propriedade indCan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndCan(JAXBElement<String> value) {
        this.indCan = value;
    }

    /**
     * Obtém o valor da propriedade numCco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumCco() {
        return numCco;
    }

    /**
     * Define o valor da propriedade numCco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumCco(JAXBElement<String> value) {
        this.numCco = value;
    }

    /**
     * Obtém o valor da propriedade numDoc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumDoc() {
        return numDoc;
    }

    /**
     * Define o valor da propriedade numDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumDoc(JAXBElement<String> value) {
        this.numDoc = value;
    }

    /**
     * Obtém o valor da propriedade numPdv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumPdv() {
        return numPdv;
    }

    /**
     * Define o valor da propriedade numPdv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumPdv(JAXBElement<Integer> value) {
        this.numPdv = value;
    }

    /**
     * Obtém o valor da propriedade numPrj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumPrj() {
        return numPrj;
    }

    /**
     * Define o valor da propriedade numPrj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumPrj(JAXBElement<String> value) {
        this.numPrj = value;
    }

    /**
     * Obtém o valor da propriedade numTit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumTit() {
        return numTit;
    }

    /**
     * Define o valor da propriedade numTit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumTit(JAXBElement<String> value) {
        this.numTit = value;
    }

    /**
     * Obtém o valor da propriedade obsMcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsMcp() {
        return obsMcp;
    }

    /**
     * Define o valor da propriedade obsMcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsMcp(JAXBElement<String> value) {
        this.obsMcp = value;
    }

    /**
     * Obtém o valor da propriedade seqChe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqChe() {
        return seqChe;
    }

    /**
     * Define o valor da propriedade seqChe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqChe(JAXBElement<String> value) {
        this.seqChe = value;
    }

    /**
     * Obtém o valor da propriedade tipPgt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipPgt() {
        return tipPgt;
    }

    /**
     * Define o valor da propriedade tipPgt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipPgt(JAXBElement<String> value) {
        this.tipPgt = value;
    }

    /**
     * Obtém o valor da propriedade tnsCxb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTnsCxb() {
        return tnsCxb;
    }

    /**
     * Define o valor da propriedade tnsCxb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTnsCxb(JAXBElement<String> value) {
        this.tnsCxb = value;
    }

    /**
     * Obtém o valor da propriedade vlrCor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrCor() {
        return vlrCor;
    }

    /**
     * Define o valor da propriedade vlrCor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrCor(JAXBElement<String> value) {
        this.vlrCor = value;
    }

    /**
     * Obtém o valor da propriedade vlrDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDsc() {
        return vlrDsc;
    }

    /**
     * Define o valor da propriedade vlrDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDsc(JAXBElement<String> value) {
        this.vlrDsc = value;
    }

    /**
     * Obtém o valor da propriedade vlrEnc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrEnc() {
        return vlrEnc;
    }

    /**
     * Define o valor da propriedade vlrEnc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrEnc(JAXBElement<String> value) {
        this.vlrEnc = value;
    }

    /**
     * Obtém o valor da propriedade vlrJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrJrs() {
        return vlrJrs;
    }

    /**
     * Define o valor da propriedade vlrJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrJrs(JAXBElement<String> value) {
        this.vlrJrs = value;
    }

    /**
     * Obtém o valor da propriedade vlrLiq.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrLiq() {
        return vlrLiq;
    }

    /**
     * Define o valor da propriedade vlrLiq.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrLiq(JAXBElement<String> value) {
        this.vlrLiq = value;
    }

    /**
     * Obtém o valor da propriedade vlrMov.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrMov() {
        return vlrMov;
    }

    /**
     * Define o valor da propriedade vlrMov.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrMov(JAXBElement<String> value) {
        this.vlrMov = value;
    }

    /**
     * Obtém o valor da propriedade vlrMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrMul() {
        return vlrMul;
    }

    /**
     * Define o valor da propriedade vlrMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrMul(JAXBElement<String> value) {
        this.vlrMul = value;
    }

    /**
     * Obtém o valor da propriedade vlrOac.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOac() {
        return vlrOac;
    }

    /**
     * Define o valor da propriedade vlrOac.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOac(JAXBElement<String> value) {
        this.vlrOac = value;
    }

    /**
     * Obtém o valor da propriedade vlrOde.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOde() {
        return vlrOde;
    }

    /**
     * Define o valor da propriedade vlrOde.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOde(JAXBElement<String> value) {
        this.vlrOde = value;
    }

}
