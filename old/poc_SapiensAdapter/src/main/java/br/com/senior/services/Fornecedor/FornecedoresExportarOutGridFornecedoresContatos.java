
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportarOutGridFornecedoresContatos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportarOutGridFornecedoresContatos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="carCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codNiv" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cpfCto" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="datNas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hobCon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intNet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nivCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ramCto" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seqCto" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="setCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sitCto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timCon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipInt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportarOutGridFornecedoresContatos", propOrder = {
    "carCto",
    "codFor",
    "codNiv",
    "cpfCto",
    "datNas",
    "faxCto",
    "fonCto",
    "hobCon",
    "intNet",
    "nivCto",
    "nomCto",
    "ramCto",
    "seqCto",
    "setCto",
    "sitCto",
    "timCon",
    "tipInt"
})
public class FornecedoresExportarOutGridFornecedoresContatos {

    @XmlElementRef(name = "carCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carCto;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "codNiv", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codNiv;
    @XmlElementRef(name = "cpfCto", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> cpfCto;
    @XmlElementRef(name = "datNas", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datNas;
    @XmlElementRef(name = "faxCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> faxCto;
    @XmlElementRef(name = "fonCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonCto;
    @XmlElementRef(name = "hobCon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> hobCon;
    @XmlElementRef(name = "intNet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> intNet;
    @XmlElementRef(name = "nivCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nivCto;
    @XmlElementRef(name = "nomCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomCto;
    @XmlElementRef(name = "ramCto", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ramCto;
    @XmlElementRef(name = "seqCto", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqCto;
    @XmlElementRef(name = "setCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> setCto;
    @XmlElementRef(name = "sitCto", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sitCto;
    @XmlElementRef(name = "timCon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timCon;
    @XmlElementRef(name = "tipInt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tipInt;

    /**
     * Obtem o valor da propriedade carCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarCto() {
        return carCto;
    }

    /**
     * Define o valor da propriedade carCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarCto(JAXBElement<String> value) {
        this.carCto = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade codNiv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodNiv() {
        return codNiv;
    }

    /**
     * Define o valor da propriedade codNiv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodNiv(JAXBElement<Integer> value) {
        this.codNiv = value;
    }

    /**
     * Obtem o valor da propriedade cpfCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCpfCto() {
        return cpfCto;
    }

    /**
     * Define o valor da propriedade cpfCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCpfCto(JAXBElement<Double> value) {
        this.cpfCto = value;
    }

    /**
     * Obtem o valor da propriedade datNas.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatNas() {
        return datNas;
    }

    /**
     * Define o valor da propriedade datNas.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatNas(JAXBElement<String> value) {
        this.datNas = value;
    }

    /**
     * Obtem o valor da propriedade faxCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFaxCto() {
        return faxCto;
    }

    /**
     * Define o valor da propriedade faxCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFaxCto(JAXBElement<String> value) {
        this.faxCto = value;
    }

    /**
     * Obtem o valor da propriedade fonCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonCto() {
        return fonCto;
    }

    /**
     * Define o valor da propriedade fonCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonCto(JAXBElement<String> value) {
        this.fonCto = value;
    }

    /**
     * Obtem o valor da propriedade hobCon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHobCon() {
        return hobCon;
    }

    /**
     * Define o valor da propriedade hobCon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHobCon(JAXBElement<String> value) {
        this.hobCon = value;
    }

    /**
     * Obtem o valor da propriedade intNet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIntNet() {
        return intNet;
    }

    /**
     * Define o valor da propriedade intNet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIntNet(JAXBElement<String> value) {
        this.intNet = value;
    }

    /**
     * Obtem o valor da propriedade nivCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNivCto() {
        return nivCto;
    }

    /**
     * Define o valor da propriedade nivCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNivCto(JAXBElement<String> value) {
        this.nivCto = value;
    }

    /**
     * Obtem o valor da propriedade nomCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomCto() {
        return nomCto;
    }

    /**
     * Define o valor da propriedade nomCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomCto(JAXBElement<String> value) {
        this.nomCto = value;
    }

    /**
     * Obtem o valor da propriedade ramCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRamCto() {
        return ramCto;
    }

    /**
     * Define o valor da propriedade ramCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRamCto(JAXBElement<Integer> value) {
        this.ramCto = value;
    }

    /**
     * Obtem o valor da propriedade seqCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqCto() {
        return seqCto;
    }

    /**
     * Define o valor da propriedade seqCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqCto(JAXBElement<Integer> value) {
        this.seqCto = value;
    }

    /**
     * Obtem o valor da propriedade setCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSetCto() {
        return setCto;
    }

    /**
     * Define o valor da propriedade setCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSetCto(JAXBElement<String> value) {
        this.setCto = value;
    }

    /**
     * Obtem o valor da propriedade sitCto.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSitCto() {
        return sitCto;
    }

    /**
     * Define o valor da propriedade sitCto.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSitCto(JAXBElement<String> value) {
        this.sitCto = value;
    }

    /**
     * Obtem o valor da propriedade timCon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTimCon() {
        return timCon;
    }

    /**
     * Define o valor da propriedade timCon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTimCon(JAXBElement<String> value) {
        this.timCon = value;
    }

    /**
     * Obtem o valor da propriedade tipInt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTipInt() {
        return tipInt;
    }

    /**
     * Define o valor da propriedade tipInt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTipInt(JAXBElement<Integer> value) {
        this.tipInt = value;
    }

}
