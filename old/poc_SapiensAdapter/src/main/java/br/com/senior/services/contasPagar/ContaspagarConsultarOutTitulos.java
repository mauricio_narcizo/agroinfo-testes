
package br.com.senior.services.contasPagar;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contaspagarConsultarOutTitulos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contaspagarConsultarOutTitulos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ccbFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjFilial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCcu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMoe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNtg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUsu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="corApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cotApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cotEmi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaRed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datEmi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datEnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datPpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dscApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="empApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="encApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fpgApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jrsApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jrsDia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jrsNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="libApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mulApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mulNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numArb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPrj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oacApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsTcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="odeApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="outNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pgtApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="porAnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuSit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vctOri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrApr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contaspagarConsultarOutTitulos", propOrder = {
    "ccbFor",
    "cnpjFilial",
    "codAge",
    "codBan",
    "codBar",
    "codCcu",
    "codCrp",
    "codCrt",
    "codEmp",
    "codFil",
    "codFor",
    "codFpg",
    "codFpj",
    "codMoe",
    "codMpt",
    "codNtg",
    "codPor",
    "codTns",
    "codTpt",
    "codTri",
    "codUsu",
    "corApr",
    "cotApr",
    "cotEmi",
    "ctaApr",
    "ctaFin",
    "ctaRed",
    "datApr",
    "datDsc",
    "datEmi",
    "datEnt",
    "datNeg",
    "datPpt",
    "dscApr",
    "empApr",
    "encApr",
    "fpgApr",
    "horApr",
    "jrsApr",
    "jrsDia",
    "jrsNeg",
    "libApr",
    "mulApr",
    "mulNeg",
    "numArb",
    "numPrj",
    "numTit",
    "oacApr",
    "obsTcp",
    "odeApr",
    "outNeg",
    "perDsc",
    "perJrs",
    "perMul",
    "pgtApr",
    "porAnt",
    "seqApr",
    "tipJrs",
    "titBan",
    "tolDsc",
    "tolJrs",
    "tolMul",
    "usuSit",
    "vctOri",
    "vlrApr",
    "vlrDsc",
    "vlrOri"
})
public class ContaspagarConsultarOutTitulos {

    @XmlElementRef(name = "ccbFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccbFor;
    @XmlElementRef(name = "cnpjFilial", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cnpjFilial;
    @XmlElementRef(name = "codAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAge;
    @XmlElementRef(name = "codBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBan;
    @XmlElementRef(name = "codBar", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBar;
    @XmlElementRef(name = "codCcu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCcu;
    @XmlElementRef(name = "codCrp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrp;
    @XmlElementRef(name = "codCrt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrt;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFil;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFor;
    @XmlElementRef(name = "codFpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpg;
    @XmlElementRef(name = "codFpj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpj;
    @XmlElementRef(name = "codMoe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codMoe;
    @XmlElementRef(name = "codMpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codMpt;
    @XmlElementRef(name = "codNtg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codNtg;
    @XmlElementRef(name = "codPor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPor;
    @XmlElementRef(name = "codTns", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTns;
    @XmlElementRef(name = "codTpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTpt;
    @XmlElementRef(name = "codTri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTri;
    @XmlElementRef(name = "codUsu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codUsu;
    @XmlElementRef(name = "corApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> corApr;
    @XmlElementRef(name = "cotApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cotApr;
    @XmlElementRef(name = "cotEmi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cotEmi;
    @XmlElementRef(name = "ctaApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaApr;
    @XmlElementRef(name = "ctaFin", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaFin;
    @XmlElementRef(name = "ctaRed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaRed;
    @XmlElementRef(name = "datApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datApr;
    @XmlElementRef(name = "datDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datDsc;
    @XmlElementRef(name = "datEmi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datEmi;
    @XmlElementRef(name = "datEnt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datEnt;
    @XmlElementRef(name = "datNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datNeg;
    @XmlElementRef(name = "datPpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datPpt;
    @XmlElementRef(name = "dscApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dscApr;
    @XmlElementRef(name = "empApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> empApr;
    @XmlElementRef(name = "encApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> encApr;
    @XmlElementRef(name = "fpgApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fpgApr;
    @XmlElementRef(name = "horApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> horApr;
    @XmlElementRef(name = "jrsApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jrsApr;
    @XmlElementRef(name = "jrsDia", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jrsDia;
    @XmlElementRef(name = "jrsNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jrsNeg;
    @XmlElementRef(name = "libApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> libApr;
    @XmlElementRef(name = "mulApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mulApr;
    @XmlElementRef(name = "mulNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mulNeg;
    @XmlElementRef(name = "numArb", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numArb;
    @XmlElementRef(name = "numPrj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numPrj;
    @XmlElementRef(name = "numTit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numTit;
    @XmlElementRef(name = "oacApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oacApr;
    @XmlElementRef(name = "obsTcp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsTcp;
    @XmlElementRef(name = "odeApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> odeApr;
    @XmlElementRef(name = "outNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outNeg;
    @XmlElementRef(name = "perDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perDsc;
    @XmlElementRef(name = "perJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perJrs;
    @XmlElementRef(name = "perMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perMul;
    @XmlElementRef(name = "pgtApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pgtApr;
    @XmlElementRef(name = "porAnt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> porAnt;
    @XmlElementRef(name = "seqApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqApr;
    @XmlElementRef(name = "tipJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipJrs;
    @XmlElementRef(name = "titBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> titBan;
    @XmlElementRef(name = "tolDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolDsc;
    @XmlElementRef(name = "tolJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolJrs;
    @XmlElementRef(name = "tolMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolMul;
    @XmlElementRef(name = "usuSit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> usuSit;
    @XmlElementRef(name = "vctOri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vctOri;
    @XmlElementRef(name = "vlrApr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrApr;
    @XmlElementRef(name = "vlrDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDsc;
    @XmlElementRef(name = "vlrOri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOri;

    /**
     * Obtém o valor da propriedade ccbFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCcbFor() {
        return ccbFor;
    }

    /**
     * Define o valor da propriedade ccbFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCcbFor(JAXBElement<String> value) {
        this.ccbFor = value;
    }

    /**
     * Obtém o valor da propriedade cnpjFilial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCnpjFilial() {
        return cnpjFilial;
    }

    /**
     * Define o valor da propriedade cnpjFilial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCnpjFilial(JAXBElement<String> value) {
        this.cnpjFilial = value;
    }

    /**
     * Obtém o valor da propriedade codAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAge() {
        return codAge;
    }

    /**
     * Define o valor da propriedade codAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAge(JAXBElement<String> value) {
        this.codAge = value;
    }

    /**
     * Obtém o valor da propriedade codBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBan() {
        return codBan;
    }

    /**
     * Define o valor da propriedade codBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBan(JAXBElement<String> value) {
        this.codBan = value;
    }

    /**
     * Obtém o valor da propriedade codBar.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBar() {
        return codBar;
    }

    /**
     * Define o valor da propriedade codBar.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBar(JAXBElement<String> value) {
        this.codBar = value;
    }

    /**
     * Obtém o valor da propriedade codCcu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCcu() {
        return codCcu;
    }

    /**
     * Define o valor da propriedade codCcu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCcu(JAXBElement<String> value) {
        this.codCcu = value;
    }

    /**
     * Obtém o valor da propriedade codCrp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrp() {
        return codCrp;
    }

    /**
     * Define o valor da propriedade codCrp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrp(JAXBElement<String> value) {
        this.codCrp = value;
    }

    /**
     * Obtém o valor da propriedade codCrt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrt() {
        return codCrt;
    }

    /**
     * Define o valor da propriedade codCrt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrt(JAXBElement<String> value) {
        this.codCrt = value;
    }

    /**
     * Obtém o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<String> value) {
        this.codEmp = value;
    }

    /**
     * Obtém o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<String> value) {
        this.codFil = value;
    }

    /**
     * Obtém o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<String> value) {
        this.codFor = value;
    }

    /**
     * Obtém o valor da propriedade codFpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpg() {
        return codFpg;
    }

    /**
     * Define o valor da propriedade codFpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpg(JAXBElement<String> value) {
        this.codFpg = value;
    }

    /**
     * Obtém o valor da propriedade codFpj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpj() {
        return codFpj;
    }

    /**
     * Define o valor da propriedade codFpj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpj(JAXBElement<String> value) {
        this.codFpj = value;
    }

    /**
     * Obtém o valor da propriedade codMoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodMoe() {
        return codMoe;
    }

    /**
     * Define o valor da propriedade codMoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodMoe(JAXBElement<String> value) {
        this.codMoe = value;
    }

    /**
     * Obtém o valor da propriedade codMpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodMpt() {
        return codMpt;
    }

    /**
     * Define o valor da propriedade codMpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodMpt(JAXBElement<String> value) {
        this.codMpt = value;
    }

    /**
     * Obtém o valor da propriedade codNtg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodNtg() {
        return codNtg;
    }

    /**
     * Define o valor da propriedade codNtg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodNtg(JAXBElement<String> value) {
        this.codNtg = value;
    }

    /**
     * Obtém o valor da propriedade codPor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPor() {
        return codPor;
    }

    /**
     * Define o valor da propriedade codPor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPor(JAXBElement<String> value) {
        this.codPor = value;
    }

    /**
     * Obtém o valor da propriedade codTns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTns() {
        return codTns;
    }

    /**
     * Define o valor da propriedade codTns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTns(JAXBElement<String> value) {
        this.codTns = value;
    }

    /**
     * Obtém o valor da propriedade codTpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTpt() {
        return codTpt;
    }

    /**
     * Define o valor da propriedade codTpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTpt(JAXBElement<String> value) {
        this.codTpt = value;
    }

    /**
     * Obtém o valor da propriedade codTri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTri() {
        return codTri;
    }

    /**
     * Define o valor da propriedade codTri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTri(JAXBElement<String> value) {
        this.codTri = value;
    }

    /**
     * Obtém o valor da propriedade codUsu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodUsu() {
        return codUsu;
    }

    /**
     * Define o valor da propriedade codUsu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodUsu(JAXBElement<String> value) {
        this.codUsu = value;
    }

    /**
     * Obtém o valor da propriedade corApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCorApr() {
        return corApr;
    }

    /**
     * Define o valor da propriedade corApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCorApr(JAXBElement<String> value) {
        this.corApr = value;
    }

    /**
     * Obtém o valor da propriedade cotApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCotApr() {
        return cotApr;
    }

    /**
     * Define o valor da propriedade cotApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCotApr(JAXBElement<String> value) {
        this.cotApr = value;
    }

    /**
     * Obtém o valor da propriedade cotEmi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCotEmi() {
        return cotEmi;
    }

    /**
     * Define o valor da propriedade cotEmi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCotEmi(JAXBElement<String> value) {
        this.cotEmi = value;
    }

    /**
     * Obtém o valor da propriedade ctaApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaApr() {
        return ctaApr;
    }

    /**
     * Define o valor da propriedade ctaApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaApr(JAXBElement<String> value) {
        this.ctaApr = value;
    }

    /**
     * Obtém o valor da propriedade ctaFin.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaFin() {
        return ctaFin;
    }

    /**
     * Define o valor da propriedade ctaFin.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaFin(JAXBElement<String> value) {
        this.ctaFin = value;
    }

    /**
     * Obtém o valor da propriedade ctaRed.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaRed() {
        return ctaRed;
    }

    /**
     * Define o valor da propriedade ctaRed.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaRed(JAXBElement<String> value) {
        this.ctaRed = value;
    }

    /**
     * Obtém o valor da propriedade datApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatApr() {
        return datApr;
    }

    /**
     * Define o valor da propriedade datApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatApr(JAXBElement<String> value) {
        this.datApr = value;
    }

    /**
     * Obtém o valor da propriedade datDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatDsc() {
        return datDsc;
    }

    /**
     * Define o valor da propriedade datDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatDsc(JAXBElement<String> value) {
        this.datDsc = value;
    }

    /**
     * Obtém o valor da propriedade datEmi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatEmi() {
        return datEmi;
    }

    /**
     * Define o valor da propriedade datEmi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatEmi(JAXBElement<String> value) {
        this.datEmi = value;
    }

    /**
     * Obtém o valor da propriedade datEnt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatEnt() {
        return datEnt;
    }

    /**
     * Define o valor da propriedade datEnt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatEnt(JAXBElement<String> value) {
        this.datEnt = value;
    }

    /**
     * Obtém o valor da propriedade datNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatNeg() {
        return datNeg;
    }

    /**
     * Define o valor da propriedade datNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatNeg(JAXBElement<String> value) {
        this.datNeg = value;
    }

    /**
     * Obtém o valor da propriedade datPpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatPpt() {
        return datPpt;
    }

    /**
     * Define o valor da propriedade datPpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatPpt(JAXBElement<String> value) {
        this.datPpt = value;
    }

    /**
     * Obtém o valor da propriedade dscApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDscApr() {
        return dscApr;
    }

    /**
     * Define o valor da propriedade dscApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDscApr(JAXBElement<String> value) {
        this.dscApr = value;
    }

    /**
     * Obtém o valor da propriedade empApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmpApr() {
        return empApr;
    }

    /**
     * Define o valor da propriedade empApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmpApr(JAXBElement<String> value) {
        this.empApr = value;
    }

    /**
     * Obtém o valor da propriedade encApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEncApr() {
        return encApr;
    }

    /**
     * Define o valor da propriedade encApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEncApr(JAXBElement<String> value) {
        this.encApr = value;
    }

    /**
     * Obtém o valor da propriedade fpgApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFpgApr() {
        return fpgApr;
    }

    /**
     * Define o valor da propriedade fpgApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFpgApr(JAXBElement<String> value) {
        this.fpgApr = value;
    }

    /**
     * Obtém o valor da propriedade horApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHorApr() {
        return horApr;
    }

    /**
     * Define o valor da propriedade horApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHorApr(JAXBElement<String> value) {
        this.horApr = value;
    }

    /**
     * Obtém o valor da propriedade jrsApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJrsApr() {
        return jrsApr;
    }

    /**
     * Define o valor da propriedade jrsApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJrsApr(JAXBElement<String> value) {
        this.jrsApr = value;
    }

    /**
     * Obtém o valor da propriedade jrsDia.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJrsDia() {
        return jrsDia;
    }

    /**
     * Define o valor da propriedade jrsDia.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJrsDia(JAXBElement<String> value) {
        this.jrsDia = value;
    }

    /**
     * Obtém o valor da propriedade jrsNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJrsNeg() {
        return jrsNeg;
    }

    /**
     * Define o valor da propriedade jrsNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJrsNeg(JAXBElement<String> value) {
        this.jrsNeg = value;
    }

    /**
     * Obtém o valor da propriedade libApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLibApr() {
        return libApr;
    }

    /**
     * Define o valor da propriedade libApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLibApr(JAXBElement<String> value) {
        this.libApr = value;
    }

    /**
     * Obtém o valor da propriedade mulApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMulApr() {
        return mulApr;
    }

    /**
     * Define o valor da propriedade mulApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMulApr(JAXBElement<String> value) {
        this.mulApr = value;
    }

    /**
     * Obtém o valor da propriedade mulNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMulNeg() {
        return mulNeg;
    }

    /**
     * Define o valor da propriedade mulNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMulNeg(JAXBElement<String> value) {
        this.mulNeg = value;
    }

    /**
     * Obtém o valor da propriedade numArb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumArb() {
        return numArb;
    }

    /**
     * Define o valor da propriedade numArb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumArb(JAXBElement<String> value) {
        this.numArb = value;
    }

    /**
     * Obtém o valor da propriedade numPrj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumPrj() {
        return numPrj;
    }

    /**
     * Define o valor da propriedade numPrj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumPrj(JAXBElement<String> value) {
        this.numPrj = value;
    }

    /**
     * Obtém o valor da propriedade numTit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumTit() {
        return numTit;
    }

    /**
     * Define o valor da propriedade numTit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumTit(JAXBElement<String> value) {
        this.numTit = value;
    }

    /**
     * Obtém o valor da propriedade oacApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOacApr() {
        return oacApr;
    }

    /**
     * Define o valor da propriedade oacApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOacApr(JAXBElement<String> value) {
        this.oacApr = value;
    }

    /**
     * Obtém o valor da propriedade obsTcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsTcp() {
        return obsTcp;
    }

    /**
     * Define o valor da propriedade obsTcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsTcp(JAXBElement<String> value) {
        this.obsTcp = value;
    }

    /**
     * Obtém o valor da propriedade odeApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOdeApr() {
        return odeApr;
    }

    /**
     * Define o valor da propriedade odeApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOdeApr(JAXBElement<String> value) {
        this.odeApr = value;
    }

    /**
     * Obtém o valor da propriedade outNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutNeg() {
        return outNeg;
    }

    /**
     * Define o valor da propriedade outNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutNeg(JAXBElement<String> value) {
        this.outNeg = value;
    }

    /**
     * Obtém o valor da propriedade perDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerDsc() {
        return perDsc;
    }

    /**
     * Define o valor da propriedade perDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerDsc(JAXBElement<String> value) {
        this.perDsc = value;
    }

    /**
     * Obtém o valor da propriedade perJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerJrs() {
        return perJrs;
    }

    /**
     * Define o valor da propriedade perJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerJrs(JAXBElement<String> value) {
        this.perJrs = value;
    }

    /**
     * Obtém o valor da propriedade perMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerMul() {
        return perMul;
    }

    /**
     * Define o valor da propriedade perMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerMul(JAXBElement<String> value) {
        this.perMul = value;
    }

    /**
     * Obtém o valor da propriedade pgtApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPgtApr() {
        return pgtApr;
    }

    /**
     * Define o valor da propriedade pgtApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPgtApr(JAXBElement<String> value) {
        this.pgtApr = value;
    }

    /**
     * Obtém o valor da propriedade porAnt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPorAnt() {
        return porAnt;
    }

    /**
     * Define o valor da propriedade porAnt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPorAnt(JAXBElement<String> value) {
        this.porAnt = value;
    }

    /**
     * Obtém o valor da propriedade seqApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqApr() {
        return seqApr;
    }

    /**
     * Define o valor da propriedade seqApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqApr(JAXBElement<String> value) {
        this.seqApr = value;
    }

    /**
     * Obtém o valor da propriedade tipJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipJrs() {
        return tipJrs;
    }

    /**
     * Define o valor da propriedade tipJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipJrs(JAXBElement<String> value) {
        this.tipJrs = value;
    }

    /**
     * Obtém o valor da propriedade titBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitBan() {
        return titBan;
    }

    /**
     * Define o valor da propriedade titBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitBan(JAXBElement<String> value) {
        this.titBan = value;
    }

    /**
     * Obtém o valor da propriedade tolDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolDsc() {
        return tolDsc;
    }

    /**
     * Define o valor da propriedade tolDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolDsc(JAXBElement<String> value) {
        this.tolDsc = value;
    }

    /**
     * Obtém o valor da propriedade tolJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolJrs() {
        return tolJrs;
    }

    /**
     * Define o valor da propriedade tolJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolJrs(JAXBElement<String> value) {
        this.tolJrs = value;
    }

    /**
     * Obtém o valor da propriedade tolMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolMul() {
        return tolMul;
    }

    /**
     * Define o valor da propriedade tolMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolMul(JAXBElement<String> value) {
        this.tolMul = value;
    }

    /**
     * Obtém o valor da propriedade usuSit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUsuSit() {
        return usuSit;
    }

    /**
     * Define o valor da propriedade usuSit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUsuSit(JAXBElement<String> value) {
        this.usuSit = value;
    }

    /**
     * Obtém o valor da propriedade vctOri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVctOri() {
        return vctOri;
    }

    /**
     * Define o valor da propriedade vctOri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVctOri(JAXBElement<String> value) {
        this.vctOri = value;
    }

    /**
     * Obtém o valor da propriedade vlrApr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrApr() {
        return vlrApr;
    }

    /**
     * Define o valor da propriedade vlrApr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrApr(JAXBElement<String> value) {
        this.vlrApr = value;
    }

    /**
     * Obtém o valor da propriedade vlrDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDsc() {
        return vlrDsc;
    }

    /**
     * Define o valor da propriedade vlrDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDsc(JAXBElement<String> value) {
        this.vlrDsc = value;
    }

    /**
     * Obtém o valor da propriedade vlrOri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOri() {
        return vlrOri;
    }

    /**
     * Define o valor da propriedade vlrOri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOri(JAXBElement<String> value) {
        this.vlrOri = value;
    }

}
