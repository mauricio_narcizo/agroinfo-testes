
package br.com.senior.services.Fornecedor;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresImportarIn complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresImportarIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flowInstanceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flowName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fornecedor" type="{http://services.senior.com.br}fornecedoresImportarInFornecedor" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sistemaIntegracao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresImportarIn", propOrder = {
    "flowInstanceID",
    "flowName",
    "fornecedor",
    "sistemaIntegracao"
})
public class FornecedoresImportarIn {

    @XmlElementRef(name = "flowInstanceID", type = JAXBElement.class, required = false)
    protected JAXBElement<String> flowInstanceID;
    @XmlElementRef(name = "flowName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> flowName;
    @XmlElement(nillable = true)
    protected List<FornecedoresImportarInFornecedor> fornecedor;
    @XmlElementRef(name = "sistemaIntegracao", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sistemaIntegracao;

    /**
     * Obtem o valor da propriedade flowInstanceID.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFlowInstanceID() {
        return flowInstanceID;
    }

    /**
     * Define o valor da propriedade flowInstanceID.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFlowInstanceID(JAXBElement<String> value) {
        this.flowInstanceID = value;
    }

    /**
     * Obtem o valor da propriedade flowName.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFlowName() {
        return flowName;
    }

    /**
     * Define o valor da propriedade flowName.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFlowName(JAXBElement<String> value) {
        this.flowName = value;
    }

    /**
     * Gets the value of the fornecedor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fornecedor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFornecedor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresImportarInFornecedor }
     * 
     * 
     */
    public List<FornecedoresImportarInFornecedor> getFornecedor() {
        if (fornecedor == null) {
            fornecedor = new ArrayList<FornecedoresImportarInFornecedor>();
        }
        return this.fornecedor;
    }

    /**
     * Obtem o valor da propriedade sistemaIntegracao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSistemaIntegracao() {
        return sistemaIntegracao;
    }

    /**
     * Define o valor da propriedade sistemaIntegracao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSistemaIntegracao(JAXBElement<String> value) {
        this.sistemaIntegracao = value;
    }

}
