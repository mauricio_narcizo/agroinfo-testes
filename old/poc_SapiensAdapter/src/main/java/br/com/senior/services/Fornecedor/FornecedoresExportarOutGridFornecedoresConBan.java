
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportarOutGridFornecedoresConBan complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportarOutGridFornecedoresConBan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ccbFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seqBan" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sisGer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportarOutGridFornecedoresConBan", propOrder = {
    "ccbFor",
    "codAge",
    "codBan",
    "codFor",
    "seqBan",
    "sisGer"
})
public class FornecedoresExportarOutGridFornecedoresConBan {

    @XmlElementRef(name = "ccbFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccbFor;
    @XmlElementRef(name = "codAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAge;
    @XmlElementRef(name = "codBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBan;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "seqBan", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqBan;
    @XmlElementRef(name = "sisGer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sisGer;

    /**
     * Obtem o valor da propriedade ccbFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCcbFor() {
        return ccbFor;
    }

    /**
     * Define o valor da propriedade ccbFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCcbFor(JAXBElement<String> value) {
        this.ccbFor = value;
    }

    /**
     * Obtem o valor da propriedade codAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAge() {
        return codAge;
    }

    /**
     * Define o valor da propriedade codAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAge(JAXBElement<String> value) {
        this.codAge = value;
    }

    /**
     * Obtem o valor da propriedade codBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBan() {
        return codBan;
    }

    /**
     * Define o valor da propriedade codBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBan(JAXBElement<String> value) {
        this.codBan = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade seqBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqBan() {
        return seqBan;
    }

    /**
     * Define o valor da propriedade seqBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqBan(JAXBElement<Integer> value) {
        this.seqBan = value;
    }

    /**
     * Obtem o valor da propriedade sisGer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSisGer() {
        return sisGer;
    }

    /**
     * Define o valor da propriedade sisGer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSisGer(JAXBElement<String> value) {
        this.sisGer = value;
    }

}
