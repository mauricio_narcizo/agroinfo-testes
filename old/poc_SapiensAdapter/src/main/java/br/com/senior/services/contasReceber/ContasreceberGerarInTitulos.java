
package br.com.senior.services.contasReceber;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contasreceberGerarInTitulos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contasreceberGerarInTitulos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="catTef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cheAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cheBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cheCta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cheNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjFilial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCcu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCli" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codIn1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codIn2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMoe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNtg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comRec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpgNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ctaRed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datEmi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datEnt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datPpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dscNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jrsDia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jrsNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mulNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nsuTef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numArb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPrj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsTcr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="outNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perCom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rateios" type="{http://services.senior.com.br}contasreceberGerarInTitulosRateios" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seqCob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxNeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vctOri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrBco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrCom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDcb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contasreceberGerarInTitulos", propOrder = {
    "catTef",
    "cheAge",
    "cheBan",
    "cheCta",
    "cheNum",
    "cnpjFilial",
    "codCcu",
    "codCli",
    "codCrp",
    "codCrt",
    "codEmp",
    "codFil",
    "codFpg",
    "codFpj",
    "codIn1",
    "codIn2",
    "codMoe",
    "codMpt",
    "codNtg",
    "codPor",
    "codRep",
    "codSac",
    "codTns",
    "codTpt",
    "comRec",
    "cpgNeg",
    "ctaFin",
    "ctaRed",
    "datDsc",
    "datEmi",
    "datEnt",
    "datNeg",
    "datPpt",
    "dscNeg",
    "jrsDia",
    "jrsNeg",
    "mulNeg",
    "nsuTef",
    "numArb",
    "numPrj",
    "numTit",
    "obsTcr",
    "outNeg",
    "perCom",
    "perDsc",
    "perJrs",
    "perMul",
    "proJrs",
    "rateios",
    "seqCob",
    "taxNeg",
    "tipJrs",
    "titBan",
    "tolDsc",
    "tolJrs",
    "tolMul",
    "vctOri",
    "vlrBco",
    "vlrCom",
    "vlrDca",
    "vlrDcb",
    "vlrDsc",
    "vlrOri",
    "vlrOud"
})
public class ContasreceberGerarInTitulos {

    @XmlElementRef(name = "catTef", type = JAXBElement.class, required = false)
    protected JAXBElement<String> catTef;
    @XmlElementRef(name = "cheAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cheAge;
    @XmlElementRef(name = "cheBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cheBan;
    @XmlElementRef(name = "cheCta", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cheCta;
    @XmlElementRef(name = "cheNum", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cheNum;
    @XmlElementRef(name = "cnpjFilial", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cnpjFilial;
    @XmlElementRef(name = "codCcu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCcu;
    @XmlElementRef(name = "codCli", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCli;
    @XmlElementRef(name = "codCrp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrp;
    @XmlElementRef(name = "codCrt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrt;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFil;
    @XmlElementRef(name = "codFpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpg;
    @XmlElementRef(name = "codFpj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpj;
    @XmlElementRef(name = "codIn1", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codIn1;
    @XmlElementRef(name = "codIn2", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codIn2;
    @XmlElementRef(name = "codMoe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codMoe;
    @XmlElementRef(name = "codMpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codMpt;
    @XmlElementRef(name = "codNtg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codNtg;
    @XmlElementRef(name = "codPor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPor;
    @XmlElementRef(name = "codRep", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRep;
    @XmlElementRef(name = "codSac", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSac;
    @XmlElementRef(name = "codTns", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTns;
    @XmlElementRef(name = "codTpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTpt;
    @XmlElementRef(name = "comRec", type = JAXBElement.class, required = false)
    protected JAXBElement<String> comRec;
    @XmlElementRef(name = "cpgNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cpgNeg;
    @XmlElementRef(name = "ctaFin", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaFin;
    @XmlElementRef(name = "ctaRed", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ctaRed;
    @XmlElementRef(name = "datDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datDsc;
    @XmlElementRef(name = "datEmi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datEmi;
    @XmlElementRef(name = "datEnt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datEnt;
    @XmlElementRef(name = "datNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datNeg;
    @XmlElementRef(name = "datPpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datPpt;
    @XmlElementRef(name = "dscNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dscNeg;
    @XmlElementRef(name = "jrsDia", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jrsDia;
    @XmlElementRef(name = "jrsNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> jrsNeg;
    @XmlElementRef(name = "mulNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mulNeg;
    @XmlElementRef(name = "nsuTef", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nsuTef;
    @XmlElementRef(name = "numArb", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numArb;
    @XmlElementRef(name = "numPrj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numPrj;
    @XmlElementRef(name = "numTit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numTit;
    @XmlElementRef(name = "obsTcr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsTcr;
    @XmlElementRef(name = "outNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outNeg;
    @XmlElementRef(name = "perCom", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perCom;
    @XmlElementRef(name = "perDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perDsc;
    @XmlElementRef(name = "perJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perJrs;
    @XmlElementRef(name = "perMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> perMul;
    @XmlElementRef(name = "proJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> proJrs;
    @XmlElement(nillable = true)
    protected List<ContasreceberGerarInTitulosRateios> rateios;
    @XmlElementRef(name = "seqCob", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqCob;
    @XmlElementRef(name = "taxNeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> taxNeg;
    @XmlElementRef(name = "tipJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipJrs;
    @XmlElementRef(name = "titBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> titBan;
    @XmlElementRef(name = "tolDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolDsc;
    @XmlElementRef(name = "tolJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolJrs;
    @XmlElementRef(name = "tolMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tolMul;
    @XmlElementRef(name = "vctOri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vctOri;
    @XmlElementRef(name = "vlrBco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrBco;
    @XmlElementRef(name = "vlrCom", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrCom;
    @XmlElementRef(name = "vlrDca", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDca;
    @XmlElementRef(name = "vlrDcb", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDcb;
    @XmlElementRef(name = "vlrDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDsc;
    @XmlElementRef(name = "vlrOri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOri;
    @XmlElementRef(name = "vlrOud", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOud;

    /**
     * Obtem o valor da propriedade catTef.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCatTef() {
        return catTef;
    }

    /**
     * Define o valor da propriedade catTef.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCatTef(JAXBElement<String> value) {
        this.catTef = value;
    }

    /**
     * Obtem o valor da propriedade cheAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheAge() {
        return cheAge;
    }

    /**
     * Define o valor da propriedade cheAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheAge(JAXBElement<String> value) {
        this.cheAge = value;
    }

    /**
     * Obtem o valor da propriedade cheBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheBan() {
        return cheBan;
    }

    /**
     * Define o valor da propriedade cheBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheBan(JAXBElement<String> value) {
        this.cheBan = value;
    }

    /**
     * Obtem o valor da propriedade cheCta.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheCta() {
        return cheCta;
    }

    /**
     * Define o valor da propriedade cheCta.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheCta(JAXBElement<String> value) {
        this.cheCta = value;
    }

    /**
     * Obtem o valor da propriedade cheNum.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheNum() {
        return cheNum;
    }

    /**
     * Define o valor da propriedade cheNum.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheNum(JAXBElement<String> value) {
        this.cheNum = value;
    }

    /**
     * Obtem o valor da propriedade cnpjFilial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCnpjFilial() {
        return cnpjFilial;
    }

    /**
     * Define o valor da propriedade cnpjFilial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCnpjFilial(JAXBElement<String> value) {
        this.cnpjFilial = value;
    }

    /**
     * Obtem o valor da propriedade codCcu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCcu() {
        return codCcu;
    }

    /**
     * Define o valor da propriedade codCcu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCcu(JAXBElement<String> value) {
        this.codCcu = value;
    }

    /**
     * Obtem o valor da propriedade codCli.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCli() {
        return codCli;
    }

    /**
     * Define o valor da propriedade codCli.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCli(JAXBElement<String> value) {
        this.codCli = value;
    }

    /**
     * Obtem o valor da propriedade codCrp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrp() {
        return codCrp;
    }

    /**
     * Define o valor da propriedade codCrp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrp(JAXBElement<String> value) {
        this.codCrp = value;
    }

    /**
     * Obtem o valor da propriedade codCrt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrt() {
        return codCrt;
    }

    /**
     * Define o valor da propriedade codCrt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrt(JAXBElement<String> value) {
        this.codCrt = value;
    }

    /**
     * Obtem o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<Integer> value) {
        this.codEmp = value;
    }

    /**
     * Obtem o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<String> value) {
        this.codFil = value;
    }

    /**
     * Obtem o valor da propriedade codFpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpg() {
        return codFpg;
    }

    /**
     * Define o valor da propriedade codFpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpg(JAXBElement<String> value) {
        this.codFpg = value;
    }

    /**
     * Obtem o valor da propriedade codFpj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpj() {
        return codFpj;
    }

    /**
     * Define o valor da propriedade codFpj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpj(JAXBElement<String> value) {
        this.codFpj = value;
    }

    /**
     * Obtem o valor da propriedade codIn1.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodIn1() {
        return codIn1;
    }

    /**
     * Define o valor da propriedade codIn1.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodIn1(JAXBElement<String> value) {
        this.codIn1 = value;
    }

    /**
     * Obtem o valor da propriedade codIn2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodIn2() {
        return codIn2;
    }

    /**
     * Define o valor da propriedade codIn2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodIn2(JAXBElement<String> value) {
        this.codIn2 = value;
    }

    /**
     * Obtem o valor da propriedade codMoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodMoe() {
        return codMoe;
    }

    /**
     * Define o valor da propriedade codMoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodMoe(JAXBElement<String> value) {
        this.codMoe = value;
    }

    /**
     * Obtem o valor da propriedade codMpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodMpt() {
        return codMpt;
    }

    /**
     * Define o valor da propriedade codMpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodMpt(JAXBElement<String> value) {
        this.codMpt = value;
    }

    /**
     * Obtem o valor da propriedade codNtg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodNtg() {
        return codNtg;
    }

    /**
     * Define o valor da propriedade codNtg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodNtg(JAXBElement<String> value) {
        this.codNtg = value;
    }

    /**
     * Obtem o valor da propriedade codPor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPor() {
        return codPor;
    }

    /**
     * Define o valor da propriedade codPor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPor(JAXBElement<String> value) {
        this.codPor = value;
    }

    /**
     * Obtem o valor da propriedade codRep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodRep() {
        return codRep;
    }

    /**
     * Define o valor da propriedade codRep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodRep(JAXBElement<String> value) {
        this.codRep = value;
    }

    /**
     * Obtem o valor da propriedade codSac.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSac() {
        return codSac;
    }

    /**
     * Define o valor da propriedade codSac.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSac(JAXBElement<String> value) {
        this.codSac = value;
    }

    /**
     * Obtem o valor da propriedade codTns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTns() {
        return codTns;
    }

    /**
     * Define o valor da propriedade codTns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTns(JAXBElement<String> value) {
        this.codTns = value;
    }

    /**
     * Obtem o valor da propriedade codTpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTpt() {
        return codTpt;
    }

    /**
     * Define o valor da propriedade codTpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTpt(JAXBElement<String> value) {
        this.codTpt = value;
    }

    /**
     * Obtem o valor da propriedade comRec.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComRec() {
        return comRec;
    }

    /**
     * Define o valor da propriedade comRec.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComRec(JAXBElement<String> value) {
        this.comRec = value;
    }

    /**
     * Obtem o valor da propriedade cpgNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCpgNeg() {
        return cpgNeg;
    }

    /**
     * Define o valor da propriedade cpgNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCpgNeg(JAXBElement<String> value) {
        this.cpgNeg = value;
    }

    /**
     * Obtem o valor da propriedade ctaFin.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaFin() {
        return ctaFin;
    }

    /**
     * Define o valor da propriedade ctaFin.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaFin(JAXBElement<String> value) {
        this.ctaFin = value;
    }

    /**
     * Obtem o valor da propriedade ctaRed.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCtaRed() {
        return ctaRed;
    }

    /**
     * Define o valor da propriedade ctaRed.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCtaRed(JAXBElement<String> value) {
        this.ctaRed = value;
    }

    /**
     * Obtem o valor da propriedade datDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatDsc() {
        return datDsc;
    }

    /**
     * Define o valor da propriedade datDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatDsc(JAXBElement<String> value) {
        this.datDsc = value;
    }

    /**
     * Obtem o valor da propriedade datEmi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatEmi() {
        return datEmi;
    }

    /**
     * Define o valor da propriedade datEmi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatEmi(JAXBElement<String> value) {
        this.datEmi = value;
    }

    /**
     * Obtem o valor da propriedade datEnt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatEnt() {
        return datEnt;
    }

    /**
     * Define o valor da propriedade datEnt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatEnt(JAXBElement<String> value) {
        this.datEnt = value;
    }

    /**
     * Obtem o valor da propriedade datNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatNeg() {
        return datNeg;
    }

    /**
     * Define o valor da propriedade datNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatNeg(JAXBElement<String> value) {
        this.datNeg = value;
    }

    /**
     * Obtem o valor da propriedade datPpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatPpt() {
        return datPpt;
    }

    /**
     * Define o valor da propriedade datPpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatPpt(JAXBElement<String> value) {
        this.datPpt = value;
    }

    /**
     * Obtem o valor da propriedade dscNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDscNeg() {
        return dscNeg;
    }

    /**
     * Define o valor da propriedade dscNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDscNeg(JAXBElement<String> value) {
        this.dscNeg = value;
    }

    /**
     * Obtem o valor da propriedade jrsDia.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJrsDia() {
        return jrsDia;
    }

    /**
     * Define o valor da propriedade jrsDia.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJrsDia(JAXBElement<String> value) {
        this.jrsDia = value;
    }

    /**
     * Obtem o valor da propriedade jrsNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getJrsNeg() {
        return jrsNeg;
    }

    /**
     * Define o valor da propriedade jrsNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setJrsNeg(JAXBElement<String> value) {
        this.jrsNeg = value;
    }

    /**
     * Obtem o valor da propriedade mulNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMulNeg() {
        return mulNeg;
    }

    /**
     * Define o valor da propriedade mulNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMulNeg(JAXBElement<String> value) {
        this.mulNeg = value;
    }

    /**
     * Obtem o valor da propriedade nsuTef.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNsuTef() {
        return nsuTef;
    }

    /**
     * Define o valor da propriedade nsuTef.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNsuTef(JAXBElement<String> value) {
        this.nsuTef = value;
    }

    /**
     * Obtem o valor da propriedade numArb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumArb() {
        return numArb;
    }

    /**
     * Define o valor da propriedade numArb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumArb(JAXBElement<String> value) {
        this.numArb = value;
    }

    /**
     * Obtem o valor da propriedade numPrj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumPrj() {
        return numPrj;
    }

    /**
     * Define o valor da propriedade numPrj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumPrj(JAXBElement<String> value) {
        this.numPrj = value;
    }

    /**
     * Obtem o valor da propriedade numTit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumTit() {
        return numTit;
    }

    /**
     * Define o valor da propriedade numTit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumTit(JAXBElement<String> value) {
        this.numTit = value;
    }

    /**
     * Obtem o valor da propriedade obsTcr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsTcr() {
        return obsTcr;
    }

    /**
     * Define o valor da propriedade obsTcr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsTcr(JAXBElement<String> value) {
        this.obsTcr = value;
    }

    /**
     * Obtem o valor da propriedade outNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutNeg() {
        return outNeg;
    }

    /**
     * Define o valor da propriedade outNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutNeg(JAXBElement<String> value) {
        this.outNeg = value;
    }

    /**
     * Obtem o valor da propriedade perCom.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerCom() {
        return perCom;
    }

    /**
     * Define o valor da propriedade perCom.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerCom(JAXBElement<String> value) {
        this.perCom = value;
    }

    /**
     * Obtem o valor da propriedade perDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerDsc() {
        return perDsc;
    }

    /**
     * Define o valor da propriedade perDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerDsc(JAXBElement<String> value) {
        this.perDsc = value;
    }

    /**
     * Obtem o valor da propriedade perJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerJrs() {
        return perJrs;
    }

    /**
     * Define o valor da propriedade perJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerJrs(JAXBElement<String> value) {
        this.perJrs = value;
    }

    /**
     * Obtem o valor da propriedade perMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPerMul() {
        return perMul;
    }

    /**
     * Define o valor da propriedade perMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPerMul(JAXBElement<String> value) {
        this.perMul = value;
    }

    /**
     * Obtem o valor da propriedade proJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProJrs() {
        return proJrs;
    }

    /**
     * Define o valor da propriedade proJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProJrs(JAXBElement<String> value) {
        this.proJrs = value;
    }

    /**
     * Gets the value of the rateios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContasreceberGerarInTitulosRateios }
     * 
     * 
     */
    public List<ContasreceberGerarInTitulosRateios> getRateios() {
        if (rateios == null) {
            rateios = new ArrayList<ContasreceberGerarInTitulosRateios>();
        }
        return this.rateios;
    }

    /**
     * Obtem o valor da propriedade seqCob.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqCob() {
        return seqCob;
    }

    /**
     * Define o valor da propriedade seqCob.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqCob(JAXBElement<String> value) {
        this.seqCob = value;
    }

    /**
     * Obtem o valor da propriedade taxNeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTaxNeg() {
        return taxNeg;
    }

    /**
     * Define o valor da propriedade taxNeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTaxNeg(JAXBElement<String> value) {
        this.taxNeg = value;
    }

    /**
     * Obtem o valor da propriedade tipJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipJrs() {
        return tipJrs;
    }

    /**
     * Define o valor da propriedade tipJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipJrs(JAXBElement<String> value) {
        this.tipJrs = value;
    }

    /**
     * Obtem o valor da propriedade titBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitBan() {
        return titBan;
    }

    /**
     * Define o valor da propriedade titBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitBan(JAXBElement<String> value) {
        this.titBan = value;
    }

    /**
     * Obtem o valor da propriedade tolDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolDsc() {
        return tolDsc;
    }

    /**
     * Define o valor da propriedade tolDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolDsc(JAXBElement<String> value) {
        this.tolDsc = value;
    }

    /**
     * Obtem o valor da propriedade tolJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolJrs() {
        return tolJrs;
    }

    /**
     * Define o valor da propriedade tolJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolJrs(JAXBElement<String> value) {
        this.tolJrs = value;
    }

    /**
     * Obtem o valor da propriedade tolMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTolMul() {
        return tolMul;
    }

    /**
     * Define o valor da propriedade tolMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTolMul(JAXBElement<String> value) {
        this.tolMul = value;
    }

    /**
     * Obtem o valor da propriedade vctOri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVctOri() {
        return vctOri;
    }

    /**
     * Define o valor da propriedade vctOri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVctOri(JAXBElement<String> value) {
        this.vctOri = value;
    }

    /**
     * Obtem o valor da propriedade vlrBco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrBco() {
        return vlrBco;
    }

    /**
     * Define o valor da propriedade vlrBco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrBco(JAXBElement<String> value) {
        this.vlrBco = value;
    }

    /**
     * Obtem o valor da propriedade vlrCom.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrCom() {
        return vlrCom;
    }

    /**
     * Define o valor da propriedade vlrCom.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrCom(JAXBElement<String> value) {
        this.vlrCom = value;
    }

    /**
     * Obtem o valor da propriedade vlrDca.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDca() {
        return vlrDca;
    }

    /**
     * Define o valor da propriedade vlrDca.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDca(JAXBElement<String> value) {
        this.vlrDca = value;
    }

    /**
     * Obtem o valor da propriedade vlrDcb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDcb() {
        return vlrDcb;
    }

    /**
     * Define o valor da propriedade vlrDcb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDcb(JAXBElement<String> value) {
        this.vlrDcb = value;
    }

    /**
     * Obtem o valor da propriedade vlrDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDsc() {
        return vlrDsc;
    }

    /**
     * Define o valor da propriedade vlrDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDsc(JAXBElement<String> value) {
        this.vlrDsc = value;
    }

    /**
     * Obtem o valor da propriedade vlrOri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOri() {
        return vlrOri;
    }

    /**
     * Define o valor da propriedade vlrOri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOri(JAXBElement<String> value) {
        this.vlrOri = value;
    }

    /**
     * Obtem o valor da propriedade vlrOud.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOud() {
        return vlrOud;
    }

    /**
     * Define o valor da propriedade vlrOud.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOud(JAXBElement<String> value) {
        this.vlrOud = value;
    }

}
