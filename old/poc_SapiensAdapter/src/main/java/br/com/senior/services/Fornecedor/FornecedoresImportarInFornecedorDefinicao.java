
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresImportarInFornecedorDefinicao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresImportarInFornecedorDefinicao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="antDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ccbFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cifFob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTra" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="forMon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagDtj" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pagDtm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pagEev" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pagJmm" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pagMul" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pagTir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perDs1" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs2" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs3" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs4" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs5" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDsc" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perEmb" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perEnc" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perFre" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perFun" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIns" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIrf" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIss" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perOut" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perSeg" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pgtFre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pgtMon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="przEnt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="qtdDcv" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tnsPro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tnsSer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolDsc" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresImportarInFornecedorDefinicao", propOrder = {
    "antDsc",
    "ccbFor",
    "cifFob",
    "codAge",
    "codBan",
    "codCrt",
    "codDep",
    "codEmp",
    "codFil",
    "codPor",
    "codTra",
    "forMon",
    "indInd",
    "numCcc",
    "pagDtj",
    "pagDtm",
    "pagEev",
    "pagJmm",
    "pagMul",
    "pagTir",
    "perDs1",
    "perDs2",
    "perDs3",
    "perDs4",
    "perDs5",
    "perDsc",
    "perEmb",
    "perEnc",
    "perFre",
    "perFun",
    "perIns",
    "perIrf",
    "perIss",
    "perOut",
    "perSeg",
    "pgtFre",
    "pgtMon",
    "przEnt",
    "qtdDcv",
    "tnsPro",
    "tnsSer",
    "tolDsc"
})
public class FornecedoresImportarInFornecedorDefinicao {

    @XmlElementRef(name = "antDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> antDsc;
    @XmlElementRef(name = "ccbFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccbFor;
    @XmlElementRef(name = "cifFob", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cifFob;
    @XmlElementRef(name = "codAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAge;
    @XmlElementRef(name = "codBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBan;
    @XmlElementRef(name = "codCrt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrt;
    @XmlElementRef(name = "codDep", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codDep;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFil;
    @XmlElementRef(name = "codPor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPor;
    @XmlElementRef(name = "codTra", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codTra;
    @XmlElementRef(name = "forMon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forMon;
    @XmlElementRef(name = "indInd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indInd;
    @XmlElementRef(name = "numCcc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numCcc;
    @XmlElementRef(name = "pagDtj", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> pagDtj;
    @XmlElementRef(name = "pagDtm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> pagDtm;
    @XmlElementRef(name = "pagEev", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> pagEev;
    @XmlElementRef(name = "pagJmm", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> pagJmm;
    @XmlElementRef(name = "pagMul", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> pagMul;
    @XmlElementRef(name = "pagTir", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pagTir;
    @XmlElementRef(name = "perDs1", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs1;
    @XmlElementRef(name = "perDs2", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs2;
    @XmlElementRef(name = "perDs3", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs3;
    @XmlElementRef(name = "perDs4", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs4;
    @XmlElementRef(name = "perDs5", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs5;
    @XmlElementRef(name = "perDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDsc;
    @XmlElementRef(name = "perEmb", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perEmb;
    @XmlElementRef(name = "perEnc", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perEnc;
    @XmlElementRef(name = "perFre", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perFre;
    @XmlElementRef(name = "perFun", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perFun;
    @XmlElementRef(name = "perIns", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIns;
    @XmlElementRef(name = "perIrf", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIrf;
    @XmlElementRef(name = "perIss", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIss;
    @XmlElementRef(name = "perOut", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perOut;
    @XmlElementRef(name = "perSeg", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perSeg;
    @XmlElementRef(name = "pgtFre", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pgtFre;
    @XmlElementRef(name = "pgtMon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pgtMon;
    @XmlElementRef(name = "przEnt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> przEnt;
    @XmlElementRef(name = "qtdDcv", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> qtdDcv;
    @XmlElementRef(name = "tnsPro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tnsPro;
    @XmlElementRef(name = "tnsSer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tnsSer;
    @XmlElementRef(name = "tolDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tolDsc;

    /**
     * Obtem o valor da propriedade antDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAntDsc() {
        return antDsc;
    }

    /**
     * Define o valor da propriedade antDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAntDsc(JAXBElement<String> value) {
        this.antDsc = value;
    }

    /**
     * Obtem o valor da propriedade ccbFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCcbFor() {
        return ccbFor;
    }

    /**
     * Define o valor da propriedade ccbFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCcbFor(JAXBElement<String> value) {
        this.ccbFor = value;
    }

    /**
     * Obtem o valor da propriedade cifFob.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCifFob() {
        return cifFob;
    }

    /**
     * Define o valor da propriedade cifFob.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCifFob(JAXBElement<String> value) {
        this.cifFob = value;
    }

    /**
     * Obtem o valor da propriedade codAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAge() {
        return codAge;
    }

    /**
     * Define o valor da propriedade codAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAge(JAXBElement<String> value) {
        this.codAge = value;
    }

    /**
     * Obtem o valor da propriedade codBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBan() {
        return codBan;
    }

    /**
     * Define o valor da propriedade codBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBan(JAXBElement<String> value) {
        this.codBan = value;
    }

    /**
     * Obtem o valor da propriedade codCrt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrt() {
        return codCrt;
    }

    /**
     * Define o valor da propriedade codCrt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrt(JAXBElement<String> value) {
        this.codCrt = value;
    }

    /**
     * Obtem o valor da propriedade codDep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodDep() {
        return codDep;
    }

    /**
     * Define o valor da propriedade codDep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodDep(JAXBElement<String> value) {
        this.codDep = value;
    }

    /**
     * Obtem o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<Integer> value) {
        this.codEmp = value;
    }

    /**
     * Obtem o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<Integer> value) {
        this.codFil = value;
    }

    /**
     * Obtem o valor da propriedade codPor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPor() {
        return codPor;
    }

    /**
     * Define o valor da propriedade codPor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPor(JAXBElement<String> value) {
        this.codPor = value;
    }

    /**
     * Obtem o valor da propriedade codTra.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodTra() {
        return codTra;
    }

    /**
     * Define o valor da propriedade codTra.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodTra(JAXBElement<Integer> value) {
        this.codTra = value;
    }

    /**
     * Obtem o valor da propriedade forMon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForMon() {
        return forMon;
    }

    /**
     * Define o valor da propriedade forMon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForMon(JAXBElement<String> value) {
        this.forMon = value;
    }

    /**
     * Obtem o valor da propriedade indInd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndInd() {
        return indInd;
    }

    /**
     * Define o valor da propriedade indInd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndInd(JAXBElement<String> value) {
        this.indInd = value;
    }

    /**
     * Obtem o valor da propriedade numCcc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumCcc() {
        return numCcc;
    }

    /**
     * Define o valor da propriedade numCcc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumCcc(JAXBElement<String> value) {
        this.numCcc = value;
    }

    /**
     * Obtem o valor da propriedade pagDtj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPagDtj() {
        return pagDtj;
    }

    /**
     * Define o valor da propriedade pagDtj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPagDtj(JAXBElement<Integer> value) {
        this.pagDtj = value;
    }

    /**
     * Obtem o valor da propriedade pagDtm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPagDtm() {
        return pagDtm;
    }

    /**
     * Define o valor da propriedade pagDtm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPagDtm(JAXBElement<Integer> value) {
        this.pagDtm = value;
    }

    /**
     * Obtem o valor da propriedade pagEev.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPagEev() {
        return pagEev;
    }

    /**
     * Define o valor da propriedade pagEev.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPagEev(JAXBElement<Integer> value) {
        this.pagEev = value;
    }

    /**
     * Obtem o valor da propriedade pagJmm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPagJmm() {
        return pagJmm;
    }

    /**
     * Define o valor da propriedade pagJmm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPagJmm(JAXBElement<Double> value) {
        this.pagJmm = value;
    }

    /**
     * Obtem o valor da propriedade pagMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPagMul() {
        return pagMul;
    }

    /**
     * Define o valor da propriedade pagMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPagMul(JAXBElement<Double> value) {
        this.pagMul = value;
    }

    /**
     * Obtem o valor da propriedade pagTir.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPagTir() {
        return pagTir;
    }

    /**
     * Define o valor da propriedade pagTir.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPagTir(JAXBElement<String> value) {
        this.pagTir = value;
    }

    /**
     * Obtem o valor da propriedade perDs1.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs1() {
        return perDs1;
    }

    /**
     * Define o valor da propriedade perDs1.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs1(JAXBElement<Double> value) {
        this.perDs1 = value;
    }

    /**
     * Obtem o valor da propriedade perDs2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs2() {
        return perDs2;
    }

    /**
     * Define o valor da propriedade perDs2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs2(JAXBElement<Double> value) {
        this.perDs2 = value;
    }

    /**
     * Obtem o valor da propriedade perDs3.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs3() {
        return perDs3;
    }

    /**
     * Define o valor da propriedade perDs3.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs3(JAXBElement<Double> value) {
        this.perDs3 = value;
    }

    /**
     * Obtem o valor da propriedade perDs4.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs4() {
        return perDs4;
    }

    /**
     * Define o valor da propriedade perDs4.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs4(JAXBElement<Double> value) {
        this.perDs4 = value;
    }

    /**
     * Obtem o valor da propriedade perDs5.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs5() {
        return perDs5;
    }

    /**
     * Define o valor da propriedade perDs5.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs5(JAXBElement<Double> value) {
        this.perDs5 = value;
    }

    /**
     * Obtem o valor da propriedade perDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDsc() {
        return perDsc;
    }

    /**
     * Define o valor da propriedade perDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDsc(JAXBElement<Double> value) {
        this.perDsc = value;
    }

    /**
     * Obtem o valor da propriedade perEmb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerEmb() {
        return perEmb;
    }

    /**
     * Define o valor da propriedade perEmb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerEmb(JAXBElement<Double> value) {
        this.perEmb = value;
    }

    /**
     * Obtem o valor da propriedade perEnc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerEnc() {
        return perEnc;
    }

    /**
     * Define o valor da propriedade perEnc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerEnc(JAXBElement<Double> value) {
        this.perEnc = value;
    }

    /**
     * Obtem o valor da propriedade perFre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerFre() {
        return perFre;
    }

    /**
     * Define o valor da propriedade perFre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerFre(JAXBElement<Double> value) {
        this.perFre = value;
    }

    /**
     * Obtem o valor da propriedade perFun.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerFun() {
        return perFun;
    }

    /**
     * Define o valor da propriedade perFun.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerFun(JAXBElement<Double> value) {
        this.perFun = value;
    }

    /**
     * Obtem o valor da propriedade perIns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIns() {
        return perIns;
    }

    /**
     * Define o valor da propriedade perIns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIns(JAXBElement<Double> value) {
        this.perIns = value;
    }

    /**
     * Obtem o valor da propriedade perIrf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIrf() {
        return perIrf;
    }

    /**
     * Define o valor da propriedade perIrf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIrf(JAXBElement<Double> value) {
        this.perIrf = value;
    }

    /**
     * Obtem o valor da propriedade perIss.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIss() {
        return perIss;
    }

    /**
     * Define o valor da propriedade perIss.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIss(JAXBElement<Double> value) {
        this.perIss = value;
    }

    /**
     * Obtem o valor da propriedade perOut.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerOut() {
        return perOut;
    }

    /**
     * Define o valor da propriedade perOut.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerOut(JAXBElement<Double> value) {
        this.perOut = value;
    }

    /**
     * Obtem o valor da propriedade perSeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerSeg() {
        return perSeg;
    }

    /**
     * Define o valor da propriedade perSeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerSeg(JAXBElement<Double> value) {
        this.perSeg = value;
    }

    /**
     * Obtem o valor da propriedade pgtFre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPgtFre() {
        return pgtFre;
    }

    /**
     * Define o valor da propriedade pgtFre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPgtFre(JAXBElement<String> value) {
        this.pgtFre = value;
    }

    /**
     * Obtem o valor da propriedade pgtMon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPgtMon() {
        return pgtMon;
    }

    /**
     * Define o valor da propriedade pgtMon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPgtMon(JAXBElement<String> value) {
        this.pgtMon = value;
    }

    /**
     * Obtem o valor da propriedade przEnt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPrzEnt() {
        return przEnt;
    }

    /**
     * Define o valor da propriedade przEnt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPrzEnt(JAXBElement<Integer> value) {
        this.przEnt = value;
    }

    /**
     * Obtem o valor da propriedade qtdDcv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getQtdDcv() {
        return qtdDcv;
    }

    /**
     * Define o valor da propriedade qtdDcv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setQtdDcv(JAXBElement<Integer> value) {
        this.qtdDcv = value;
    }

    /**
     * Obtem o valor da propriedade tnsPro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTnsPro() {
        return tnsPro;
    }

    /**
     * Define o valor da propriedade tnsPro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTnsPro(JAXBElement<String> value) {
        this.tnsPro = value;
    }

    /**
     * Obtem o valor da propriedade tnsSer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTnsSer() {
        return tnsSer;
    }

    /**
     * Define o valor da propriedade tnsSer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTnsSer(JAXBElement<String> value) {
        this.tnsSer = value;
    }

    /**
     * Obtem o valor da propriedade tolDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTolDsc() {
        return tolDsc;
    }

    /**
     * Define o valor da propriedade tolDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTolDsc(JAXBElement<Integer> value) {
        this.tolDsc = value;
    }

}
