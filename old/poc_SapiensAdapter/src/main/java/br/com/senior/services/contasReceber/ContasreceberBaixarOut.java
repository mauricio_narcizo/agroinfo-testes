
package br.com.senior.services.contasReceber;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contasreceberBaixarOut complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contasreceberBaixarOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="erroExecucao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mensagemRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resultado" type="{http://services.senior.com.br}contasreceberBaixarOutResultado" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipoRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contasreceberBaixarOut", propOrder = {
    "erroExecucao",
    "mensagemRetorno",
    "resultado",
    "tipoRetorno"
})
public class ContasreceberBaixarOut {

    @XmlElementRef(name = "erroExecucao", type = JAXBElement.class, required = false)
    protected JAXBElement<String> erroExecucao;
    @XmlElementRef(name = "mensagemRetorno", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mensagemRetorno;
    @XmlElement(nillable = true)
    protected List<ContasreceberBaixarOutResultado> resultado;
    @XmlElementRef(name = "tipoRetorno", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipoRetorno;

    /**
     * Obtem o valor da propriedade erroExecucao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErroExecucao() {
        return erroExecucao;
    }

    /**
     * Define o valor da propriedade erroExecucao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErroExecucao(JAXBElement<String> value) {
        this.erroExecucao = value;
    }

    /**
     * Obtem o valor da propriedade mensagemRetorno.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMensagemRetorno() {
        return mensagemRetorno;
    }

    /**
     * Define o valor da propriedade mensagemRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMensagemRetorno(JAXBElement<String> value) {
        this.mensagemRetorno = value;
    }

    /**
     * Gets the value of the resultado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resultado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResultado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContasreceberBaixarOutResultado }
     * 
     * 
     */
    public List<ContasreceberBaixarOutResultado> getResultado() {
        if (resultado == null) {
            resultado = new ArrayList<ContasreceberBaixarOutResultado>();
        }
        return this.resultado;
    }

    /**
     * Obtem o valor da propriedade tipoRetorno.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipoRetorno() {
        return tipoRetorno;
    }

    /**
     * Define o valor da propriedade tipoRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipoRetorno(JAXBElement<String> value) {
        this.tipoRetorno = value;
    }

}
