package br.com.senior.Consultas;

import java.util.ArrayList;
import java.util.List;

import br.com.senior.Utils.utils;
import br.com.senior.poc.RN.FornecedorRN;
import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.sessao.sessao;
import br.com.senior.services.Fornecedor.FornecedoresExportar2In;
import br.com.senior.services.Fornecedor.FornecedoresExportar2Out;
import br.com.senior.services.Fornecedor.FornecedoresExportar2OutGridFornecedores;
import br.com.senior.services.Fornecedor.G5SeniorServices;
import br.com.senior.services.Fornecedor.SapiensSynccomSeniorG5CoIntVarejoFornecedores;

public class buscaFornecedor {
	
	
	public static G5SeniorServices service = new G5SeniorServices();
	
	public static List<Fornecedor> buscaSalvaFornecedores(sessao session){
	ArrayList<Fornecedor> lista = new ArrayList<Fornecedor>();
	SapiensSynccomSeniorG5CoIntVarejoFornecedores envio = service.getSapiensSynccomSeniorG5CoIntVarejoFornecedoresPort();
	FornecedoresExportar2In fornecedores = new FornecedoresExportar2In();
	fornecedores.setCodEmp(utils.getIntJAX(session.getEmpresa(), "codEmp"));
	fornecedores.setCodFil(utils.getIntJAX(session.getFilial(), "codFil"));
	fornecedores.setIdentificacaoSistema(utils.getSTRJAX(session.getSigla(), "identificacaoSistema"));
	fornecedores.setTipoIntegracao(utils.getSTRJAX("T", "tipoIntegracao"));
	fornecedores.setQuantidadeRegistros(utils.getIntJAX(100, "quantidadeRegistros"));
	FornecedoresExportar2Out retorno = envio.exportar2(session.getUsuario(),session.getSenha(), session.getEncript(), fornecedores);
	List<FornecedoresExportar2OutGridFornecedores> listaFornecedores = retorno.getGridFornecedores();
	for (FornecedoresExportar2OutGridFornecedores fornecedoresExportar2OutGridFornecedores : listaFornecedores) {
		FornecedorRN forRN = new FornecedorRN();
		Fornecedor agricultor = forRN.buscaOuIniciaFornecedor(fornecedoresExportar2OutGridFornecedores.getCodFor().getValue()); 
		agricultor.setNomeFantasia(fornecedoresExportar2OutGridFornecedores.getNomFor().getValue());
		agricultor.setEmail(fornecedoresExportar2OutGridFornecedores.getEmaNfe().getValue());
		String telefone = fornecedoresExportar2OutGridFornecedores.getFonFor().getValue();
		agricultor.setDddTelefone(utils.RetornaDDDTelefone(telefone));
		agricultor.setTelefone(utils.RetornaTelefone(telefone));
		agricultor.setCpf(fornecedoresExportar2OutGridFornecedores.getCgcCpf().getValue());
		forRN.salvar(agricultor);
		lista.add(agricultor);
	}
	return lista;
	}	
	
}
