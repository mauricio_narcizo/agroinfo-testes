
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.senior.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FornecedoresExportarInQuantidadeRegistros_QNAME = new QName("", "quantidadeRegistros");
    private final static QName _FornecedoresExportarInIdentificacaoSistema_QNAME = new QName("", "identificacaoSistema");
    private final static QName _FornecedoresExportarInTipoIntegracao_QNAME = new QName("", "tipoIntegracao");
    private final static QName _FornecedoresExportarInCodEmp_QNAME = new QName("", "codEmp");
    private final static QName _FornecedoresExportarInCodFil_QNAME = new QName("", "codFil");
    private final static QName _FornecedoresExportarInFlowName_QNAME = new QName("", "flowName");
    private final static QName _FornecedoresExportarInFlowInstanceID_QNAME = new QName("", "flowInstanceID");
    private final static QName _FornecedoresImportarInFornecedorRetPro_QNAME = new QName("", "retPro");
    private final static QName _FornecedoresImportarInFornecedorTriIcm_QNAME = new QName("", "triIcm");
    private final static QName _FornecedoresImportarInFornecedorIndFor_QNAME = new QName("", "indFor");
    private final static QName _FornecedoresImportarInFornecedorInsMun_QNAME = new QName("", "insMun");
    private final static QName _FornecedoresImportarInFornecedorCodRoe_QNAME = new QName("", "codRoe");
    private final static QName _FornecedoresImportarInFornecedorCodFor_QNAME = new QName("", "codFor");
    private final static QName _FornecedoresImportarInFornecedorApeFor_QNAME = new QName("", "apeFor");
    private final static QName _FornecedoresImportarInFornecedorNotSis_QNAME = new QName("", "notSis");
    private final static QName _FornecedoresImportarInFornecedorRecCof_QNAME = new QName("", "recCof");
    private final static QName _FornecedoresImportarInFornecedorRegEst_QNAME = new QName("", "regEst");
    private final static QName _FornecedoresImportarInFornecedorCodSuf_QNAME = new QName("", "codSuf");
    private final static QName _FornecedoresImportarInFornecedorFonFor_QNAME = new QName("", "fonFor");
    private final static QName _FornecedoresImportarInFornecedorCliFor_QNAME = new QName("", "cliFor");
    private final static QName _FornecedoresImportarInFornecedorBaiFor_QNAME = new QName("", "baiFor");
    private final static QName _FornecedoresImportarInFornecedorCxaPst_QNAME = new QName("", "cxaPst");
    private final static QName _FornecedoresImportarInFornecedorNumIdf_QNAME = new QName("", "numIdf");
    private final static QName _FornecedoresImportarInFornecedorDatCad_QNAME = new QName("", "datCad");
    private final static QName _FornecedoresImportarInFornecedorHorAtu_QNAME = new QName("", "horAtu");
    private final static QName _FornecedoresImportarInFornecedorTriIpi_QNAME = new QName("", "triIpi");
    private final static QName _FornecedoresImportarInFornecedorUsuCad_QNAME = new QName("", "usuCad");
    private final static QName _FornecedoresImportarInFornecedorRecIcm_QNAME = new QName("", "recIcm");
    private final static QName _FornecedoresImportarInFornecedorRetOur_QNAME = new QName("", "retOur");
    private final static QName _FornecedoresImportarInFornecedorCepFor_QNAME = new QName("", "cepFor");
    private final static QName _FornecedoresImportarInFornecedorCodTri_QNAME = new QName("", "codTri");
    private final static QName _FornecedoresImportarInFornecedorRecPis_QNAME = new QName("", "recPis");
    private final static QName _FornecedoresImportarInFornecedorRetIrf_QNAME = new QName("", "retIrf");
    private final static QName _FornecedoresImportarInFornecedorSitFor_QNAME = new QName("", "sitFor");
    private final static QName _FornecedoresImportarInFornecedorCodCli_QNAME = new QName("", "codCli");
    private final static QName _FornecedoresImportarInFornecedorEmaNfe_QNAME = new QName("", "emaNfe");
    private final static QName _FornecedoresImportarInFornecedorHorCad_QNAME = new QName("", "horCad");
    private final static QName _FornecedoresImportarInFornecedorNumRge_QNAME = new QName("", "numRge");
    private final static QName _FornecedoresImportarInFornecedorLimRet_QNAME = new QName("", "limRet");
    private final static QName _FornecedoresImportarInFornecedorRecIpi_QNAME = new QName("", "recIpi");
    private final static QName _FornecedoresImportarInFornecedorTriIss_QNAME = new QName("", "triIss");
    private final static QName _FornecedoresImportarInFornecedorCodPai_QNAME = new QName("", "codPai");
    private final static QName _FornecedoresImportarInFornecedorDatAtu_QNAME = new QName("", "datAtu");
    private final static QName _FornecedoresImportarInFornecedorUsuAtu_QNAME = new QName("", "usuAtu");
    private final static QName _FornecedoresImportarInFornecedorCodRam_QNAME = new QName("", "codRam");
    private final static QName _FornecedoresImportarInFornecedorZipCod_QNAME = new QName("", "zipCod");
    private final static QName _FornecedoresImportarInFornecedorCodMot_QNAME = new QName("", "codMot");
    private final static QName _FornecedoresImportarInFornecedorTipFor_QNAME = new QName("", "tipFor");
    private final static QName _FornecedoresImportarInFornecedorSigUfs_QNAME = new QName("", "sigUfs");
    private final static QName _FornecedoresImportarInFornecedorForTra_QNAME = new QName("", "forTra");
    private final static QName _FornecedoresImportarInFornecedorNenFor_QNAME = new QName("", "nenFor");
    private final static QName _FornecedoresImportarInFornecedorInsEst_QNAME = new QName("", "insEst");
    private final static QName _FornecedoresImportarInFornecedorIdeExt_QNAME = new QName("", "ideExt");
    private final static QName _FornecedoresImportarInFornecedorForRep_QNAME = new QName("", "forRep");
    private final static QName _FornecedoresImportarInFornecedorPerPid_QNAME = new QName("", "perPid");
    private final static QName _FornecedoresImportarInFornecedorCidFor_QNAME = new QName("", "cidFor");
    private final static QName _FornecedoresImportarInFornecedorTipMer_QNAME = new QName("", "tipMer");
    private final static QName _FornecedoresImportarInFornecedorCgcCpf_QNAME = new QName("", "cgcCpf");
    private final static QName _FornecedoresImportarInFornecedorPerIcm_QNAME = new QName("", "perIcm");
    private final static QName _FornecedoresImportarInFornecedorRetCof_QNAME = new QName("", "retCof");
    private final static QName _FornecedoresImportarInFornecedorRetCsl_QNAME = new QName("", "retCsl");
    private final static QName _FornecedoresImportarInFornecedorFonFo3_QNAME = new QName("", "fonFo3");
    private final static QName _FornecedoresImportarInFornecedorFonFo2_QNAME = new QName("", "fonFo2");
    private final static QName _FornecedoresImportarInFornecedorPerRin_QNAME = new QName("", "perRin");
    private final static QName _FornecedoresImportarInFornecedorPerRir_QNAME = new QName("", "perRir");
    private final static QName _FornecedoresImportarInFornecedorIntNet_QNAME = new QName("", "intNet");
    private final static QName _FornecedoresImportarInFornecedorNomFor_QNAME = new QName("", "nomFor");
    private final static QName _FornecedoresImportarInFornecedorCodRtr_QNAME = new QName("", "codRtr");
    private final static QName _FornecedoresImportarInFornecedorCodSro_QNAME = new QName("", "codSro");
    private final static QName _FornecedoresImportarInFornecedorSeqRoe_QNAME = new QName("", "seqRoe");
    private final static QName _FornecedoresImportarInFornecedorCplEnd_QNAME = new QName("", "cplEnd");
    private final static QName _FornecedoresImportarInFornecedorFaxFor_QNAME = new QName("", "faxFor");
    private final static QName _FornecedoresImportarInFornecedorNotFor_QNAME = new QName("", "notFor");
    private final static QName _FornecedoresImportarInFornecedorRetPis_QNAME = new QName("", "retPis");
    private final static QName _FornecedoresImportarInFornecedorObsMot_QNAME = new QName("", "obsMot");
    private final static QName _FornecedoresImportarInFornecedorEndFor_QNAME = new QName("", "endFor");
    private final static QName _FornecedoresImportarInFornecedorPerCod_QNAME = new QName("", "perCod");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCepNomUfs_QNAME = new QName("", "nomUfs");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCepCepIni_QNAME = new QName("", "cepIni");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCepCodRai_QNAME = new QName("", "codRai");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCepNomPai_QNAME = new QName("", "nomPai");
    private final static QName _FornecedoresImportarOutRetornoMsgRet_QNAME = new QName("", "msgRet");
    private final static QName _FornecedoresImportarOutRetornoTipRet_QNAME = new QName("", "tipRet");
    private final static QName _FornecedoresExportarOutGridFornecedoresConBanSeqBan_QNAME = new QName("", "seqBan");
    private final static QName _FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME = new QName("", "ccbFor");
    private final static QName _FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME = new QName("", "codAge");
    private final static QName _FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME = new QName("", "codBan");
    private final static QName _FornecedoresExportarOutGridFornecedoresConBanSisGer_QNAME = new QName("", "sisGer");
    private final static QName _FornecedoresExportarOutGridFornecedoresCNAECodCna_QNAME = new QName("", "codCna");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerBaiOrm_QNAME = new QName("", "baiOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerCgcOrm_QNAME = new QName("", "cgcOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerCplOrm_QNAME = new QName("", "cplOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerEenOrm_QNAME = new QName("", "eenOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerEstOrm_QNAME = new QName("", "estOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerPrxOrm_QNAME = new QName("", "prxOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerCidOrm_QNAME = new QName("", "cidOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerSeqOrm_QNAME = new QName("", "seqOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerInsOrm_QNAME = new QName("", "insOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerIniOrm_QNAME = new QName("", "iniOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerCepOrm_QNAME = new QName("", "cepOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerSitOrm_QNAME = new QName("", "sitOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerEndOrm_QNAME = new QName("", "endOrm");
    private final static QName _FornecedoresExportarOutGridFornecedoresOriMerNumOrm_QNAME = new QName("", "numOrm");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodPor_QNAME = new QName("", "codPor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisVlrAtr_QNAME = new QName("", "vlrAtr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPagTir_QNAME = new QName("", "pagTir");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisAntDsc_QNAME = new QName("", "antDsc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisVlrUcp_QNAME = new QName("", "vlrUcp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisSalDup_QNAME = new QName("", "salDup");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCriRat_QNAME = new QName("", "criRat");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisVlrUpe_QNAME = new QName("", "vlrUpe");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisMaiAtr_QNAME = new QName("", "maiAtr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisMedAtr_QNAME = new QName("", "medAtr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPrzEnt_QNAME = new QName("", "przEnt");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerOut_QNAME = new QName("", "perOut");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisQtdDcv_QNAME = new QName("", "qtdDcv");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisVlrUpg_QNAME = new QName("", "vlrUpg");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCprCql_QNAME = new QName("", "cprCql");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisQtdPgt_QNAME = new QName("", "qtdPgt");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCriEdv_QNAME = new QName("", "criEdv");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisConEst_QNAME = new QName("", "conEst");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerSeg_QNAME = new QName("", "perSeg");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaRed_QNAME = new QName("", "ctaRed");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlCfr_QNAME = new QName("", "rvlCfr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerEnc_QNAME = new QName("", "perEnc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisDatUpe_QNAME = new QName("", "datUpe");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisDatUpg_QNAME = new QName("", "datUpg");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlEnc_QNAME = new QName("", "rvlEnc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodTra_QNAME = new QName("", "codTra");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPagJmm_QNAME = new QName("", "pagJmm");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisDatMcp_QNAME = new QName("", "datMcp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlSeg_QNAME = new QName("", "rvlSeg");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDsc_QNAME = new QName("", "perDsc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlSei_QNAME = new QName("", "rvlSei");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaFdv_QNAME = new QName("", "ctaFdv");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodCpg_QNAME = new QName("", "codCpg");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodFav_QNAME = new QName("", "codFav");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisDatAtr_QNAME = new QName("", "datAtr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisDatUcp_QNAME = new QName("", "datUcp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCifFob_QNAME = new QName("", "cifFob");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCprCat_QNAME = new QName("", "cprCat");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerEmb_QNAME = new QName("", "perEmb");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlOut_QNAME = new QName("", "rvlOut");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerFun_QNAME = new QName("", "perFun");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPagDtj_QNAME = new QName("", "pagDtj");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisSerCur_QNAME = new QName("", "serCur");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPagDtm_QNAME = new QName("", "pagDtm");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlOui_QNAME = new QName("", "rvlOui");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDs2_QNAME = new QName("", "perDs2");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisNumCcc_QNAME = new QName("", "numCcc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDs3_QNAME = new QName("", "perDs3");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDs1_QNAME = new QName("", "perDs1");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDs4_QNAME = new QName("", "perDs4");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerDs5_QNAME = new QName("", "perDs5");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlEmb_QNAME = new QName("", "rvlEmb");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisUltDup_QNAME = new QName("", "ultDup");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodDep_QNAME = new QName("", "codDep");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaRcr_QNAME = new QName("", "ctaRcr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerIss_QNAME = new QName("", "perIss");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisSalOut_QNAME = new QName("", "salOut");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaFcr_QNAME = new QName("", "ctaFcr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisForMon_QNAME = new QName("", "forMon");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerFre_QNAME = new QName("", "perFre");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisSalCre_QNAME = new QName("", "salCre");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerIne_QNAME = new QName("", "perIne");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerIrf_QNAME = new QName("", "perIrf");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisTolDsc_QNAME = new QName("", "tolDsc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodTpr_QNAME = new QName("", "codTpr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlFre_QNAME = new QName("", "rvlFre");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisIndInd_QNAME = new QName("", "indInd");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaAad_QNAME = new QName("", "ctaAad");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCtaAux_QNAME = new QName("", "ctaAux");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPagMul_QNAME = new QName("", "pagMul");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPgtMon_QNAME = new QName("", "pgtMon");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodCrp_QNAME = new QName("", "codCrp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPerIns_QNAME = new QName("", "perIns");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlDar_QNAME = new QName("", "rvlDar");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodCrt_QNAME = new QName("", "codCrt");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisRvlFei_QNAME = new QName("", "rvlFei");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisPgtFre_QNAME = new QName("", "pgtFre");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisVlrMcp_QNAME = new QName("", "vlrMcp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCprCpe_QNAME = new QName("", "cprCpe");
    private final static QName _FornecedoresExportar2OutGridFornecedoresDefHisCodFpg_QNAME = new QName("", "codFpg");
    private final static QName _FornecedoresExportarOutMensagemRetorno_QNAME = new QName("", "mensagemRetorno");
    private final static QName _FornecedoresExportarOutFinalizaramRegistros_QNAME = new QName("", "finalizaramRegistros");
    private final static QName _FornecedoresExportarOutNumeroLote_QNAME = new QName("", "numeroLote");
    private final static QName _FornecedoresExportarOutTipoRetorno_QNAME = new QName("", "tipoRetorno");
    private final static QName _FornecedoresExportarOutErroExecucao_QNAME = new QName("", "erroExecucao");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosFonCto_QNAME = new QName("", "fonCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosHobCon_QNAME = new QName("", "hobCon");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosTimCon_QNAME = new QName("", "timCon");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosCpfCto_QNAME = new QName("", "cpfCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosNivCto_QNAME = new QName("", "nivCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosCarCto_QNAME = new QName("", "carCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosNomCto_QNAME = new QName("", "nomCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosRamCto_QNAME = new QName("", "ramCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosDatNas_QNAME = new QName("", "datNas");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosCodNiv_QNAME = new QName("", "codNiv");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosTipInt_QNAME = new QName("", "tipInt");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosSitCto_QNAME = new QName("", "sitCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosFaxCto_QNAME = new QName("", "faxCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosSetCto_QNAME = new QName("", "setCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresContatosSeqCto_QNAME = new QName("", "seqCto");
    private final static QName _FornecedoresExportarOutGridFornecedoresINSSVlrBie_QNAME = new QName("", "vlrBie");
    private final static QName _FornecedoresExportarOutGridFornecedoresINSSVlrInt_QNAME = new QName("", "vlrInt");
    private final static QName _FornecedoresExportarOutGridFornecedoresINSSMesAno_QNAME = new QName("", "mesAno");
    private final static QName _FornecedoresExportarOutGridFornecedoresINSSVlrBit_QNAME = new QName("", "vlrBit");
    private final static QName _FornecedoresExportarOutGridFornecedoresINSSVlrIne_QNAME = new QName("", "vlrIne");
    private final static QName _FornecedoresExportar2OutGridErrosMsgErr_QNAME = new QName("", "msgErr");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSeqObs_QNAME = new QName("", "seqObs");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSolHor_QNAME = new QName("", "solHor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesObsDat_QNAME = new QName("", "obsDat");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesObsUsu_QNAME = new QName("", "obsUsu");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSitObs_QNAME = new QName("", "sitObs");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesTipObs_QNAME = new QName("", "tipObs");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSolObs_QNAME = new QName("", "solObs");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSolDat_QNAME = new QName("", "solDat");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesSolUsu_QNAME = new QName("", "solUsu");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesObsFor_QNAME = new QName("", "obsFor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresObservacoesObsHor_QNAME = new QName("", "obsHor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCampoUsuarioValor_QNAME = new QName("", "valor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCampoUsuarioCampo_QNAME = new QName("", "campo");
    private final static QName _FornecedoresExportarInGridConsultaTipo_QNAME = new QName("", "tipo");
    private final static QName _FornecedoresExportar2OutGridFornecedoresMarFor_QNAME = new QName("", "marFor");
    private final static QName _FornecedoresExportar2OutGridFornecedoresForWms_QNAME = new QName("", "forWms");
    private final static QName _FornecedoresExportar2OutGridFornecedoresNotAfo_QNAME = new QName("", "notAfo");
    private final static QName _FornecedoresExportar2OutGridFornecedoresSeqInt_QNAME = new QName("", "seqInt");
    private final static QName _FornecedoresExportar2OutGridFornecedoresTemOrm_QNAME = new QName("", "temOrm");
    private final static QName _FornecedoresExportar2OutGridFornecedoresGerDir_QNAME = new QName("", "gerDir");
    private final static QName _FornecedoresExportar2OutGridFornecedoresTipPgt_QNAME = new QName("", "tipPgt");
    private final static QName _FornecedoresExportar2OutGridFornecedoresNomVen_QNAME = new QName("", "nomVen");
    private final static QName _FornecedoresExportar2OutGridFornecedoresFonVen_QNAME = new QName("", "fonVen");
    private final static QName _FornecedoresExportar2OutGridFornecedoresQtdDep_QNAME = new QName("", "qtdDep");
    private final static QName _FornecedoresExportar2OutGridFornecedoresIndCoo_QNAME = new QName("", "indCoo");
    private final static QName _FornecedoresExportar2OutGridFornecedoresFaxVen_QNAME = new QName("", "faxVen");
    private final static QName _FornecedoresExportar2OutGridFornecedoresInsAnp_QNAME = new QName("", "insAnp");
    private final static QName _FornecedoresExportar2OutGridFornecedoresTipFav_QNAME = new QName("", "tipFav");
    private final static QName _FornecedoresExportar2OutGridFornecedoresRmlVen_QNAME = new QName("", "rmlVen");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCodGre_QNAME = new QName("", "codGre");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcc_QNAME = new QName("", "codCcc");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCaracteristicasSeqOrd_QNAME = new QName("", "seqOrd");
    private final static QName _FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcl_QNAME = new QName("", "codCcl");
    private final static QName _FornecedoresImportarInFornecedorDefinicaoTnsPro_QNAME = new QName("", "tnsPro");
    private final static QName _FornecedoresImportarInFornecedorDefinicaoPagEev_QNAME = new QName("", "pagEev");
    private final static QName _FornecedoresImportarInFornecedorDefinicaoTnsSer_QNAME = new QName("", "tnsSer");
    private final static QName _FornecedoresImportarInSistemaIntegracao_QNAME = new QName("", "sistemaIntegracao");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.senior.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresCaracteristicas }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresCaracteristicas createFornecedoresExportar2OutGridFornecedoresCaracteristicas() {
        return new FornecedoresExportar2OutGridFornecedoresCaracteristicas();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOut }
     * 
     */
    public FornecedoresExportarOut createFornecedoresExportarOut() {
        return new FornecedoresExportarOut();
    }

    /**
     * Create an instance of {@link FornecedoresExportarInGridConsulta }
     * 
     */
    public FornecedoresExportarInGridConsulta createFornecedoresExportarInGridConsulta() {
        return new FornecedoresExportarInGridConsulta();
    }

    /**
     * Create an instance of {@link FornecedoresImportarInFornecedor }
     * 
     */
    public FornecedoresImportarInFornecedor createFornecedoresImportarInFornecedor() {
        return new FornecedoresImportarInFornecedor();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedores }
     * 
     */
    public FornecedoresExportarOutGridFornecedores createFornecedoresExportarOutGridFornecedores() {
        return new FornecedoresExportarOutGridFornecedores();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresCampoUsuario }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresCampoUsuario createFornecedoresExportar2OutGridFornecedoresCampoUsuario() {
        return new FornecedoresExportar2OutGridFornecedoresCampoUsuario();
    }

    /**
     * Create an instance of {@link FornecedoresImportarOut }
     * 
     */
    public FornecedoresImportarOut createFornecedoresImportarOut() {
        return new FornecedoresImportarOut();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2In }
     * 
     */
    public FornecedoresExportar2In createFornecedoresExportar2In() {
        return new FornecedoresExportar2In();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedores }
     * 
     */
    public FornecedoresExportar2OutGridFornecedores createFornecedoresExportar2OutGridFornecedores() {
        return new FornecedoresExportar2OutGridFornecedores();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresContatos }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresContatos createFornecedoresExportarOutGridFornecedoresContatos() {
        return new FornecedoresExportarOutGridFornecedoresContatos();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresOriMer }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresOriMer createFornecedoresExportar2OutGridFornecedoresOriMer() {
        return new FornecedoresExportar2OutGridFornecedoresOriMer();
    }

    /**
     * Create an instance of {@link FornecedoresImportarInFornecedorDefinicao }
     * 
     */
    public FornecedoresImportarInFornecedorDefinicao createFornecedoresImportarInFornecedorDefinicao() {
        return new FornecedoresImportarInFornecedorDefinicao();
    }

    /**
     * Create an instance of {@link FornecedoresImportarOutRetorno }
     * 
     */
    public FornecedoresImportarOutRetorno createFornecedoresImportarOutRetorno() {
        return new FornecedoresImportarOutRetorno();
    }

    /**
     * Create an instance of {@link FornecedoresExportarIn }
     * 
     */
    public FornecedoresExportarIn createFornecedoresExportarIn() {
        return new FornecedoresExportarIn();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridErros }
     * 
     */
    public FornecedoresExportarOutGridErros createFornecedoresExportarOutGridErros() {
        return new FornecedoresExportarOutGridErros();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresDefHis }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresDefHis createFornecedoresExportarOutGridFornecedoresDefHis() {
        return new FornecedoresExportarOutGridFornecedoresDefHis();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresCNAE }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresCNAE createFornecedoresExportar2OutGridFornecedoresCNAE() {
        return new FornecedoresExportar2OutGridFornecedoresCNAE();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresCaracteristicas }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresCaracteristicas createFornecedoresExportarOutGridFornecedoresCaracteristicas() {
        return new FornecedoresExportarOutGridFornecedoresCaracteristicas();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresConBan }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresConBan createFornecedoresExportarOutGridFornecedoresConBan() {
        return new FornecedoresExportarOutGridFornecedoresConBan();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresForPag }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresForPag createFornecedoresExportarOutGridFornecedoresForPag() {
        return new FornecedoresExportarOutGridFornecedoresForPag();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresCampoUsuario }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresCampoUsuario createFornecedoresExportarOutGridFornecedoresCampoUsuario() {
        return new FornecedoresExportarOutGridFornecedoresCampoUsuario();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresObservacoes }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresObservacoes createFornecedoresExportarOutGridFornecedoresObservacoes() {
        return new FornecedoresExportarOutGridFornecedoresObservacoes();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresINSS }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresINSS createFornecedoresExportar2OutGridFornecedoresINSS() {
        return new FornecedoresExportar2OutGridFornecedoresINSS();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresCep }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresCep createFornecedoresExportar2OutGridFornecedoresCep() {
        return new FornecedoresExportar2OutGridFornecedoresCep();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresObservacoes }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresObservacoes createFornecedoresExportar2OutGridFornecedoresObservacoes() {
        return new FornecedoresExportar2OutGridFornecedoresObservacoes();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2InGridConsulta }
     * 
     */
    public FornecedoresExportar2InGridConsulta createFornecedoresExportar2InGridConsulta() {
        return new FornecedoresExportar2InGridConsulta();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresCNAE }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresCNAE createFornecedoresExportarOutGridFornecedoresCNAE() {
        return new FornecedoresExportarOutGridFornecedoresCNAE();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresINSS }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresINSS createFornecedoresExportarOutGridFornecedoresINSS() {
        return new FornecedoresExportarOutGridFornecedoresINSS();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2Out }
     * 
     */
    public FornecedoresExportar2Out createFornecedoresExportar2Out() {
        return new FornecedoresExportar2Out();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresContatos }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresContatos createFornecedoresExportar2OutGridFornecedoresContatos() {
        return new FornecedoresExportar2OutGridFornecedoresContatos();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresOriMer }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresOriMer createFornecedoresExportarOutGridFornecedoresOriMer() {
        return new FornecedoresExportarOutGridFornecedoresOriMer();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresDefHis }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresDefHis createFornecedoresExportar2OutGridFornecedoresDefHis() {
        return new FornecedoresExportar2OutGridFornecedoresDefHis();
    }

    /**
     * Create an instance of {@link FornecedoresExportarOutGridFornecedoresCep }
     * 
     */
    public FornecedoresExportarOutGridFornecedoresCep createFornecedoresExportarOutGridFornecedoresCep() {
        return new FornecedoresExportarOutGridFornecedoresCep();
    }

    /**
     * Create an instance of {@link FornecedoresImportarIn }
     * 
     */
    public FornecedoresImportarIn createFornecedoresImportarIn() {
        return new FornecedoresImportarIn();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresConBan }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresConBan createFornecedoresExportar2OutGridFornecedoresConBan() {
        return new FornecedoresExportar2OutGridFornecedoresConBan();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridFornecedoresForPag }
     * 
     */
    public FornecedoresExportar2OutGridFornecedoresForPag createFornecedoresExportar2OutGridFornecedoresForPag() {
        return new FornecedoresExportar2OutGridFornecedoresForPag();
    }

    /**
     * Create an instance of {@link FornecedoresImportarOutRetornoDetalhe }
     * 
     */
    public FornecedoresImportarOutRetornoDetalhe createFornecedoresImportarOutRetornoDetalhe() {
        return new FornecedoresImportarOutRetornoDetalhe();
    }

    /**
     * Create an instance of {@link FornecedoresExportar2OutGridErros }
     * 
     */
    public FornecedoresExportar2OutGridErros createFornecedoresExportar2OutGridErros() {
        return new FornecedoresExportar2OutGridErros();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "quantidadeRegistros", scope = FornecedoresExportarIn.class)
    public JAXBElement<Integer> createFornecedoresExportarInQuantidadeRegistros(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInQuantidadeRegistros_QNAME, Integer.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "identificacaoSistema", scope = FornecedoresExportarIn.class)
    public JAXBElement<String> createFornecedoresExportarInIdentificacaoSistema(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInIdentificacaoSistema_QNAME, String.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoIntegracao", scope = FornecedoresExportarIn.class)
    public JAXBElement<String> createFornecedoresExportarInTipoIntegracao(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInTipoIntegracao_QNAME, String.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportarIn.class)
    public JAXBElement<Integer> createFornecedoresExportarInCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportarIn.class)
    public JAXBElement<Integer> createFornecedoresExportarInCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = FornecedoresExportarIn.class)
    public JAXBElement<String> createFornecedoresExportarInFlowName(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowName_QNAME, String.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = FornecedoresExportarIn.class)
    public JAXBElement<String> createFornecedoresExportarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowInstanceID_QNAME, String.class, FornecedoresExportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPro", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetPro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPro_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIcm", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorTriIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIcm_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorIndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIndFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insMun", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorInsMun(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsMun_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRoe", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodRoe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRoe_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "apeFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorApeFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorApeFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notSis", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorNotSis(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotSis_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recCof", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRecCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecCof_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "regEst", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorRegEst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorRegEst_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSuf", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodSuf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSuf_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorFonFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cliFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCliFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCliFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "baiFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorBaiFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorBaiFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cxaPst", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCxaPst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCxaPst_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numIdf", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorNumIdf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNumIdf_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datCad", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDatCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatCad_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horAtu", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorHorAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorAtu_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIpi", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorTriIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIpi_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "usuCad", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorUsuCad(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorUsuCad_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIcm", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRecIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIcm_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retOur", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetOur(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetOur_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCepFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCepFor_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTri", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodTri(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodTri_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recPis", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRecPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecPis_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retIrf", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetIrf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetIrf_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorSitFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSitFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodCli(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodCli_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "emaNfe", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorEmaNfe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEmaNfe_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horCad", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorHorCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorCad_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numRge", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorNumRge(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNumRge_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "limRet", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorLimRet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorLimRet_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIpi", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRecIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIpi_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIss", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorTriIss(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIss_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPai", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodPai(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodPai_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datAtu", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDatAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatAtu_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "usuAtu", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorUsuAtu(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorUsuAtu_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRam", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodRam(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRam_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "zipCod", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorZipCod(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorZipCod_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMot", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodMot(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodMot_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorTipFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sigUfs", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorSigUfs(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSigUfs_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forTra", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorForTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForTra_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nenFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorNenFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNenFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insEst", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorInsEst(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsEst_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorIdeExt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorIdeExt_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forRep", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorForRep(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForRep_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perPid", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorPerPid(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerPid_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cidFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCidFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCidFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipMer", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorTipMer(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipMer_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcCpf", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorCgcCpf(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorCgcCpf_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIcm", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorPerIcm(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerIcm_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCof", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCof_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCsl", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetCsl(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCsl_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo3", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorFonFo3(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo3_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo2", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorFonFo2(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo2_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRin", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorPerRin(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRin_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRir", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorPerRir(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRir_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "intNet", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorIntNet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIntNet_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorNomFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNomFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRtr", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorCodRtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodRtr_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSro", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCodSro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSro_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqRoe", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorSeqRoe(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorSeqRoe_QNAME, Integer.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cplEnd", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorCplEnd(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCplEnd_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorFaxFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFaxFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorNotFor(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotFor_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPis", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorRetPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPis_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsMot", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorObsMot(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorObsMot_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endFor", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorEndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEndFor_QNAME, String.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCod", scope = FornecedoresImportarInFornecedor.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorPerCod(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerCod_QNAME, Double.class, FornecedoresImportarInFornecedor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cplEnd", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepCplEnd(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCplEnd_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomUfs", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepNomUfs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCepNomUfs_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sigUfs", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepSigUfs(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSigUfs_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepIni", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCepCepIni(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCepCepIni_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nenFor", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepNenFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNenFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepFor", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCepCepFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCepFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRai", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCepCodRai(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCepCodRai_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endFor", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepEndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEndFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomPai", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepNomPai(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCepNomPai_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPai", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepCodPai(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodPai_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "baiFor", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepBaiFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorBaiFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cidFor", scope = FornecedoresExportar2OutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCepCidFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCidFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcCpf", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<String> createFornecedoresImportarOutRetornoCgcCpf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCgcCpf_QNAME, String.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgRet", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<String> createFornecedoresImportarOutRetornoMsgRet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarOutRetornoMsgRet_QNAME, String.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ideExt", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoIdeExt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorIdeExt_QNAME, Integer.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipRet", scope = FornecedoresImportarOutRetorno.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoTipRet(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarOutRetornoTipRet_QNAME, Integer.class, FornecedoresImportarOutRetorno.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqBan", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresConBanSeqBan(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresConBanSeqBan_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresConBanCcbFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresConBanCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresConBanCodAge(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME, String.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresConBanCodBan(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME, String.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sisGer", scope = FornecedoresExportarOutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresConBanSisGer(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanSisGer_QNAME, String.class, FornecedoresExportarOutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCna", scope = FornecedoresExportarOutGridFornecedoresCNAE.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresCNAECodCna(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresCNAECodCna_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresCNAE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresCNAE.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCNAECodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCNAE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "baiOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerBaiOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerBaiOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresOriMerCgcOrm(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresOriMerCgcOrm_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cplOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerCplOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerCplOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "eenOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerEenOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEenOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "estOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerEstOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEstOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "prxOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerPrxOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerPrxOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cidOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerCidOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerCidOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresOriMerSeqOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerSeqOrm_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresOriMerCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerInsOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerInsOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "iniOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresOriMerIniOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerIniOrm_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresOriMerCepOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerCepOrm_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerSitOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerSitOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerEndOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEndOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numOrm", scope = FornecedoresExportarOutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresOriMerNumOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerNumOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodPor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodPor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrAtr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisVlrAtr(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrAtr_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagTir", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisPagTir(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPagTir_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "antDsc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisAntDsc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisAntDsc_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUcp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisVlrUcp(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUcp_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salDup", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisSalDup(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalDup_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrm", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisSeqOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerSeqOrm_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "criRat", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCriRat(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCriRat_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUpe", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisVlrUpe(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUpe_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "maiAtr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisMaiAtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisMaiAtr_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "medAtr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisMedAtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisMedAtr_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "przEnt", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisPrzEnt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPrzEnt_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perOut", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerOut(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerOut_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdDcv", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisQtdDcv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisQtdDcv_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUpg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisVlrUpg(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUpg_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCql", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCprCql(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCql_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdPgt", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisQtdPgt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisQtdPgt_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "criEdv", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCriEdv(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCriEdv_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "conEst", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisConEst(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisConEst_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perSeg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerSeg(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerSeg_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaRed(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaRed_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlCfr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlCfr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlCfr_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEnc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerEnc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEnc_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUpe", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisDatUpe(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUpe_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUpg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisDatUpg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUpg_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlEnc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlEnc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlEnc_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTra", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCodTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodTra_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagJmm", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPagJmm(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagJmm_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datMcp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisDatMcp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatMcp_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlSeg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlSeg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlSeg_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDsc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDsc_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlSei", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlSei(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlSei_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFdv", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaFdv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaFdv_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCpg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodCpg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCpg_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFav", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisCodFav(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFav_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datAtr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisDatAtr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatAtr_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUcp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisDatUcp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUcp_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cifFob", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCifFob(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCifFob_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCat", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCprCat(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCat_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEmb", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerEmb(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEmb_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlOut", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlOut(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlOut_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFun", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerFun(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFun_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtj", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisPagDtj(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtj_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "serCur", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisSerCur(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisSerCur_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtm", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisPagDtm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtm_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlOui", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlOui(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlOui_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs2", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDs2(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs2_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numCcc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisNumCcc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisNumCcc_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs3", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDs3(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs3_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs1", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDs1(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs1_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs4", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDs4(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs4_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs5", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerDs5(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs5_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlEmb", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlEmb(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlEmb_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ultDup", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisUltDup(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisUltDup_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodBan(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codDep", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodDep(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodDep_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRcr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaRcr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaRcr_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIss", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerIss(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIss_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salOut", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisSalOut(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalOut_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFcr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaFcr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaFcr_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forMon", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisForMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisForMon_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodAge(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFre", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerFre(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFre_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salCre", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisSalCre(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalCre_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIne", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerIne(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIne_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIrf", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerIrf(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIrf_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisTolDsc(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisTolDsc_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpr", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodTpr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodTpr_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlFre", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlFre(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlFre_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indInd", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisIndInd(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisIndInd_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaAad", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaAad(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaAad_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaAux", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCtaAux(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaAux_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagMul", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPagMul(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagMul_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtMon", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisPgtMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtMon_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodCrp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCrp_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIns", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisPerIns(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIns_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlDar", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlDar(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlDar_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCcbFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisCodCrt(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCrt_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlFei", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisRvlFei(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlFei_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtFre", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDefHisPgtFre(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtFre_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMcp", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresDefHisVlrMcp(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrMcp_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCpe", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCprCpe(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCpe_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = FornecedoresExportar2OutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresDefHisCodFpg(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFpg_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCna", scope = FornecedoresExportar2OutGridFornecedoresCNAE.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresCNAECodCna(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresCNAECodCna_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresCNAE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresCNAE.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCNAECodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCNAE.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = FornecedoresExportarOut.class)
    public JAXBElement<String> createFornecedoresExportarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutMensagemRetorno_QNAME, String.class, FornecedoresExportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "finalizaramRegistros", scope = FornecedoresExportarOut.class)
    public JAXBElement<String> createFornecedoresExportarOutFinalizaramRegistros(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutFinalizaramRegistros_QNAME, String.class, FornecedoresExportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numeroLote", scope = FornecedoresExportarOut.class)
    public JAXBElement<Integer> createFornecedoresExportarOutNumeroLote(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutNumeroLote_QNAME, Integer.class, FornecedoresExportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = FornecedoresExportarOut.class)
    public JAXBElement<Integer> createFornecedoresExportarOutTipoRetorno(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutTipoRetorno_QNAME, Integer.class, FornecedoresExportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = FornecedoresExportarOut.class)
    public JAXBElement<String> createFornecedoresExportarOutErroExecucao(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutErroExecucao_QNAME, String.class, FornecedoresExportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosFonCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosFonCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "hobCon", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosHobCon(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosHobCon_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "timCon", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosTimCon(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosTimCon_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresContatosCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cpfCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresContatosCpfCto(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresContatosCpfCto_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nivCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosNivCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosNivCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "carCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosCarCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosCarCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosNomCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosNomCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ramCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresContatosRamCto(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosRamCto_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNas", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosDatNas(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosDatNas_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNiv", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresContatosCodNiv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosCodNiv_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipInt", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresContatosTipInt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosTipInt_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosSitCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosSitCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosFaxCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosFaxCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "intNet", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosIntNet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIntNet_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "setCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresContatosSetCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosSetCto_QNAME, String.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqCto", scope = FornecedoresExportarOutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresContatosSeqCto(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosSeqCto_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportar2OutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForPagCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportar2OutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForPagCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForPagCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = FornecedoresExportar2OutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForPagCodFpg(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFpg_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBie", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresINSSVlrBie(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrBie_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrInt", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresINSSVlrInt(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrInt_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mesAno", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresINSSMesAno(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresINSSMesAno_QNAME, String.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBit", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresINSSVlrBit(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrBit_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrIne", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresINSSVlrIne(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrIne_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresINSS.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresINSSCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportarOutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForPagCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportarOutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForPagCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForPagCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = FornecedoresExportarOutGridFornecedoresForPag.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForPagCodFpg(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFpg_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresForPag.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgErr", scope = FornecedoresExportar2OutGridErros.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridErrosMsgErr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridErrosMsgErr_QNAME, String.class, FornecedoresExportar2OutGridErros.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqObs", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresObservacoesSeqObs(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresObservacoesSeqObs_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solHor", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesSolHor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolHor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsDat", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesObsDat(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsDat_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsUsu", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresObservacoesObsUsu(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsUsu_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitObs", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesSitObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSitObs_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipObs", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesTipObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesTipObs_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresObservacoesCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solObs", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesSolObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolObs_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solDat", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesSolDat(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolDat_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solUsu", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresObservacoesSolUsu(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolUsu_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsFor", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesObsFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsHor", scope = FornecedoresExportar2OutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObservacoesObsHor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsHor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodPor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodPor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrAtr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisVlrAtr(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrAtr_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagTir", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisPagTir(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPagTir_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "antDsc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisAntDsc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisAntDsc_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUcp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisVlrUcp(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUcp_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salDup", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisSalDup(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalDup_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrm", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisSeqOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerSeqOrm_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "criRat", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCriRat(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCriRat_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUpe", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisVlrUpe(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUpe_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "maiAtr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisMaiAtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisMaiAtr_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "medAtr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisMedAtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisMedAtr_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "przEnt", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisPrzEnt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPrzEnt_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perOut", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerOut(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerOut_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdDcv", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisQtdDcv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisQtdDcv_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrUpg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisVlrUpg(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrUpg_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCql", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCprCql(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCql_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdPgt", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisQtdPgt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisQtdPgt_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "criEdv", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCriEdv(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCriEdv_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "conEst", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisConEst(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisConEst_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perSeg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerSeg(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerSeg_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRed", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaRed(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaRed_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlCfr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlCfr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlCfr_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEnc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerEnc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEnc_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUpe", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisDatUpe(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUpe_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUpg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisDatUpg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUpg_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlEnc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlEnc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlEnc_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTra", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCodTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodTra_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagJmm", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPagJmm(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagJmm_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datMcp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisDatMcp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatMcp_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlSeg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlSeg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlSeg_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDsc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDsc_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlSei", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlSei(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlSei_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFdv", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaFdv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaFdv_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCpg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodCpg(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCpg_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFav", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisCodFav(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFav_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datAtr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisDatAtr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatAtr_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datUcp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisDatUcp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisDatUcp_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cifFob", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCifFob(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCifFob_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCat", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCprCat(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCat_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEmb", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerEmb(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEmb_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlOut", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlOut(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlOut_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFun", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerFun(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFun_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtj", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisPagDtj(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtj_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "serCur", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisSerCur(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisSerCur_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtm", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisPagDtm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtm_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlOui", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlOui(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlOui_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs2", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDs2(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs2_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numCcc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisNumCcc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisNumCcc_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs3", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDs3(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs3_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs1", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDs1(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs1_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs4", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDs4(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs4_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs5", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerDs5(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs5_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlEmb", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlEmb(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlEmb_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ultDup", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisUltDup(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisUltDup_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodBan(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codDep", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodDep(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodDep_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaRcr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaRcr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaRcr_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIss", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerIss(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIss_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salOut", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisSalOut(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalOut_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaFcr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaFcr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaFcr_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forMon", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisForMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisForMon_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodAge(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFre", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerFre(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFre_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "salCre", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisSalCre(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisSalCre_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIne", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerIne(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIne_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIrf", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerIrf(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIrf_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisTolDsc(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisTolDsc_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTpr", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodTpr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodTpr_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlFre", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlFre(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlFre_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indInd", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisIndInd(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisIndInd_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaAad", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaAad(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaAad_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ctaAux", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCtaAux(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCtaAux_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagMul", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPagMul(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagMul_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtMon", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisPgtMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtMon_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodCrp(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCrp_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIns", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisPerIns(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIns_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlDar", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlDar(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlDar_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCcbFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisCodCrt(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCrt_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rvlFei", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisRvlFei(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisRvlFei_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtFre", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDefHisPgtFre(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtFre_QNAME, String.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrMcp", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresDefHisVlrMcp(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisVlrMcp_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cprCpe", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCprCpe(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCprCpe_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFpg", scope = FornecedoresExportarOutGridFornecedoresDefHis.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresDefHisCodFpg(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodFpg_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresDefHis.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = FornecedoresImportarOut.class)
    public JAXBElement<String> createFornecedoresImportarOutMensagemRetorno(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutMensagemRetorno_QNAME, String.class, FornecedoresImportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = FornecedoresImportarOut.class)
    public JAXBElement<Integer> createFornecedoresImportarOutTipoRetorno(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutTipoRetorno_QNAME, Integer.class, FornecedoresImportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = FornecedoresImportarOut.class)
    public JAXBElement<String> createFornecedoresImportarOutErroExecucao(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutErroExecucao_QNAME, String.class, FornecedoresImportarOut.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqObs", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresObservacoesSeqObs(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresObservacoesSeqObs_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solHor", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesSolHor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolHor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsDat", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesObsDat(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsDat_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsUsu", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresObservacoesObsUsu(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsUsu_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitObs", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesSitObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSitObs_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipObs", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesTipObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesTipObs_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresObservacoesCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solObs", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesSolObs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolObs_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solDat", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesSolDat(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolDat_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "solUsu", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresObservacoesSolUsu(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresObservacoesSolUsu_QNAME, Double.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsFor", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesObsFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsHor", scope = FornecedoresExportarOutGridFornecedoresObservacoes.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObservacoesObsHor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresObservacoesObsHor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresObservacoes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "valor", scope = FornecedoresExportar2OutGridFornecedoresCampoUsuario.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCampoUsuarioValor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioValor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCampoUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "campo", scope = FornecedoresExportar2OutGridFornecedoresCampoUsuario.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCampoUsuarioCampo(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioCampo_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCampoUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipo", scope = FornecedoresExportarInGridConsulta.class)
    public JAXBElement<Integer> createFornecedoresExportarInGridConsultaTipo(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInGridConsultaTipo_QNAME, Integer.class, FornecedoresExportarInGridConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "valor", scope = FornecedoresExportarInGridConsulta.class)
    public JAXBElement<String> createFornecedoresExportarInGridConsultaValor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioValor_QNAME, String.class, FornecedoresExportarInGridConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqBan", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresConBanSeqBan(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresConBanSeqBan_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresConBanCcbFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresConBanCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresConBanCodAge(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresConBanCodBan(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sisGer", scope = FornecedoresExportar2OutGridFornecedoresConBan.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresConBanSisGer(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanSisGer_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresConBan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mensagemRetorno", scope = FornecedoresExportar2Out.class)
    public JAXBElement<String> createFornecedoresExportar2OutMensagemRetorno(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutMensagemRetorno_QNAME, String.class, FornecedoresExportar2Out.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "finalizaramRegistros", scope = FornecedoresExportar2Out.class)
    public JAXBElement<String> createFornecedoresExportar2OutFinalizaramRegistros(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutFinalizaramRegistros_QNAME, String.class, FornecedoresExportar2Out.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numeroLote", scope = FornecedoresExportar2Out.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutNumeroLote(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutNumeroLote_QNAME, Integer.class, FornecedoresExportar2Out.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoRetorno", scope = FornecedoresExportar2Out.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutTipoRetorno(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutTipoRetorno_QNAME, Integer.class, FornecedoresExportar2Out.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "erroExecucao", scope = FornecedoresExportar2Out.class)
    public JAXBElement<String> createFornecedoresExportar2OutErroExecucao(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutErroExecucao_QNAME, String.class, FornecedoresExportar2Out.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "quantidadeRegistros", scope = FornecedoresExportar2In.class)
    public JAXBElement<Integer> createFornecedoresExportar2InQuantidadeRegistros(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInQuantidadeRegistros_QNAME, Integer.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "identificacaoSistema", scope = FornecedoresExportar2In.class)
    public JAXBElement<String> createFornecedoresExportar2InIdentificacaoSistema(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInIdentificacaoSistema_QNAME, String.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipoIntegracao", scope = FornecedoresExportar2In.class)
    public JAXBElement<String> createFornecedoresExportar2InTipoIntegracao(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInTipoIntegracao_QNAME, String.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresExportar2In.class)
    public JAXBElement<Integer> createFornecedoresExportar2InCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresExportar2In.class)
    public JAXBElement<Integer> createFornecedoresExportar2InCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = FornecedoresExportar2In.class)
    public JAXBElement<String> createFornecedoresExportar2InFlowName(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowName_QNAME, String.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = FornecedoresExportar2In.class)
    public JAXBElement<String> createFornecedoresExportar2InFlowInstanceID(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowInstanceID_QNAME, String.class, FornecedoresExportar2In.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPro", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetPro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPro_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIcm", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTriIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIcm_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresIndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIndFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "marFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresMarFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresMarFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insMun", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresInsMun(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsMun_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRoe", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCodRoe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRoe_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forWms", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresForWms(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresForWms_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "apeFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresApeFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorApeFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notSis", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresNotSis(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotSis_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recCof", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRecCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecCof_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notAfo", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresNotAfo(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresNotAfo_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqInt", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresSeqInt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresSeqInt_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "regEst", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresRegEst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorRegEst_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSuf", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCodSuf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSuf_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFonFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "temOrm", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTemOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTemOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cliFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCliFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCliFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cxaPst", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCxaPst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCxaPst_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datCad", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDatCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatCad_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gerDir", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresGerDir(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresGerDir_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horAtu", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresHorAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorAtu_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIpi", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTriIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIpi_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIcm", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRecIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIcm_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retOur", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetOur(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetOur_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTri", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCodTri(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodTri_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recPis", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRecPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecPis_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipPgt", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTipPgt(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTipPgt_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retIrf", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetIrf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetIrf_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresSitFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSitFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCodCli(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodCli_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "emaNfe", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresEmaNfe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEmaNfe_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horCad", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresHorCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorCad_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numRge", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresNumRge(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNumRge_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "limRet", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresLimRet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorLimRet_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomVen", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresNomVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresNomVen_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIpi", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRecIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIpi_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIss", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTriIss(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIss_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datAtu", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresDatAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatAtu_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRam", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCodRam(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRam_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMot", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCodMot(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodMot_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTipFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forTra", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForTra_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonVen", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFonVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresFonVen_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insEst", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresInsEst(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsEst_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdDep", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresQtdDep(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresQtdDep_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forRep", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresForRep(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForRep_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indCoo", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresIndCoo(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresIndCoo_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perPid", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresPerPid(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerPid_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipMer", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTipMer(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipMer_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcCpf", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCgcCpf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCgcCpf_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIcm", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresPerIcm(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerIcm_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCof", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCof_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCsl", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetCsl(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCsl_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo3", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFonFo3(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo3_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo2", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFonFo2(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo2_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRin", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresPerRin(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRin_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRir", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresPerRir(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRir_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "intNet", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresIntNet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIntNet_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresNomFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNomFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxVen", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFaxVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresFaxVen_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRtr", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCodRtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodRtr_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSro", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCodSro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSro_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insAnp", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresInsAnp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresInsAnp_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqRoe", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresSeqRoe(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorSeqRoe_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipFav", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresTipFav(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTipFav_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresFaxFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFaxFor_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notFor", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresNotFor(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotFor_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPis", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresRetPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPis_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rmlVen", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresRmlVen(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresRmlVen_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsMot", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresObsMot(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorObsMot_QNAME, String.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codGre", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCodGre(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCodGre_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCod", scope = FornecedoresExportar2OutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresPerCod(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerCod_QNAME, Double.class, FornecedoresExportar2OutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcc", scope = FornecedoresExportar2OutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcc(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcc_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrd", scope = FornecedoresExportar2OutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCaracteristicasSeqOrd(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasSeqOrd_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresCaracteristicasCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcl", scope = FornecedoresExportar2OutGridFornecedoresCaracteristicas.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcl(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcl_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFun", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerFun(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFun_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPor", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCodPor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodPor_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtj", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoPagDtj(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtj_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tnsPro", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoTnsPro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDefinicaoTnsPro_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagDtm", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoPagDtm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPagDtm_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagTir", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoPagTir(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPagTir_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "antDsc", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoAntDsc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisAntDsc_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs2", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDs2(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs2_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numCcc", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoNumCcc(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisNumCcc_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagEev", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoPagEev(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorDefinicaoPagEev_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs3", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDs3(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs3_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs1", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDs1(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs1_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs4", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDs4(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs4_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDs5", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDs5(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDs5_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codBan", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCodBan(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodBan_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codDep", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCodDep(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodDep_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIss", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerIss(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIss_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "przEnt", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoPrzEnt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisPrzEnt_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perOut", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerOut(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerOut_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdDcv", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoQtdDcv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisQtdDcv_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tnsSer", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoTnsSer(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDefinicaoTnsSer_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perSeg", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerSeg(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerSeg_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forMon", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoForMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisForMon_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codAge", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCodAge(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCodAge_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perFre", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerFre(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerFre_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEnc", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerEnc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEnc_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIrf", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerIrf(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIrf_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tolDsc", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoTolDsc(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisTolDsc_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indInd", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoIndInd(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisIndInd_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagMul", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPagMul(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagMul_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtMon", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoPgtMon(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtMon_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTra", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Integer> createFornecedoresImportarInFornecedorDefinicaoCodTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresDefHisCodTra_QNAME, Integer.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pagJmm", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPagJmm(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPagJmm_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIns", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerIns(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerIns_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perDsc", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerDsc(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerDsc_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ccbFor", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCcbFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresConBanCcbFor_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCrt", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCodCrt(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCodCrt_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "pgtFre", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoPgtFre(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisPgtFre_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cifFob", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<String> createFornecedoresImportarInFornecedorDefinicaoCifFob(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresDefHisCifFob_QNAME, String.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perEmb", scope = FornecedoresImportarInFornecedorDefinicao.class)
    public JAXBElement<Double> createFornecedoresImportarInFornecedorDefinicaoPerEmb(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresDefHisPerEmb_QNAME, Double.class, FornecedoresImportarInFornecedorDefinicao.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipo", scope = FornecedoresExportar2InGridConsulta.class)
    public JAXBElement<Integer> createFornecedoresExportar2InGridConsultaTipo(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInGridConsultaTipo_QNAME, Integer.class, FornecedoresExportar2InGridConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "valor", scope = FornecedoresExportar2InGridConsulta.class)
    public JAXBElement<String> createFornecedoresExportar2InGridConsultaValor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioValor_QNAME, String.class, FornecedoresExportar2InGridConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "valor", scope = FornecedoresExportarOutGridFornecedoresCampoUsuario.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCampoUsuarioValor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioValor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCampoUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "campo", scope = FornecedoresExportarOutGridFornecedoresCampoUsuario.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCampoUsuarioCampo(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCampoUsuarioCampo_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCampoUsuario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosFonCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosFonCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "hobCon", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosHobCon(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosHobCon_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "timCon", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosTimCon(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosTimCon_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresContatosCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cpfCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresContatosCpfCto(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresContatosCpfCto_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nivCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosNivCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosNivCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "carCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosCarCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosCarCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosNomCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosNomCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ramCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresContatosRamCto(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosRamCto_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datNas", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosDatNas(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosDatNas_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codNiv", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresContatosCodNiv(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosCodNiv_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipInt", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresContatosTipInt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosTipInt_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosSitCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosSitCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosFaxCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosFaxCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "intNet", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosIntNet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIntNet_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "setCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresContatosSetCto(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresContatosSetCto_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqCto", scope = FornecedoresExportar2OutGridFornecedoresContatos.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresContatosSeqCto(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresContatosSeqCto_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresContatos.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowName", scope = FornecedoresImportarIn.class)
    public JAXBElement<String> createFornecedoresImportarInFlowName(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowName_QNAME, String.class, FornecedoresImportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "flowInstanceID", scope = FornecedoresImportarIn.class)
    public JAXBElement<String> createFornecedoresImportarInFlowInstanceID(String value) {
        return new JAXBElement<String>(_FornecedoresExportarInFlowInstanceID_QNAME, String.class, FornecedoresImportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sistemaIntegracao", scope = FornecedoresImportarIn.class)
    public JAXBElement<String> createFornecedoresImportarInSistemaIntegracao(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInSistemaIntegracao_QNAME, String.class, FornecedoresImportarIn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "baiOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerBaiOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerBaiOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresOriMerCgcOrm(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresOriMerCgcOrm_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cplOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerCplOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerCplOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "eenOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerEenOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEenOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "estOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerEstOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEstOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "prxOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerPrxOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerPrxOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cidOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerCidOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerCidOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresOriMerSeqOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerSeqOrm_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresOriMerCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerInsOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerInsOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "iniOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresOriMerIniOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerIniOrm_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresOriMerCepOrm(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarOutGridFornecedoresOriMerCepOrm_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerSitOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerSitOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerEndOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerEndOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numOrm", scope = FornecedoresExportar2OutGridFornecedoresOriMer.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresOriMerNumOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresOriMerNumOrm_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresOriMer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgRet", scope = FornecedoresImportarOutRetornoDetalhe.class)
    public JAXBElement<String> createFornecedoresImportarOutRetornoDetalheMsgRet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarOutRetornoMsgRet_QNAME, String.class, FornecedoresImportarOutRetornoDetalhe.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codEmp", scope = FornecedoresImportarOutRetornoDetalhe.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoDetalheCodEmp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodEmp_QNAME, Integer.class, FornecedoresImportarOutRetornoDetalhe.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFil", scope = FornecedoresImportarOutRetornoDetalhe.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoDetalheCodFil(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportarInCodFil_QNAME, Integer.class, FornecedoresImportarOutRetornoDetalhe.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipRet", scope = FornecedoresImportarOutRetornoDetalhe.class)
    public JAXBElement<Integer> createFornecedoresImportarOutRetornoDetalheTipRet(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarOutRetornoTipRet_QNAME, Integer.class, FornecedoresImportarOutRetornoDetalhe.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPro", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetPro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPro_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIcm", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTriIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIcm_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresIndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIndFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "marFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresMarFor(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresMarFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insMun", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresInsMun(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsMun_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRoe", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCodRoe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRoe_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forWms", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresForWms(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresForWms_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "apeFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresApeFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorApeFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notSis", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresNotSis(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotSis_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recCof", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRecCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecCof_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notAfo", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresNotAfo(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportar2OutGridFornecedoresNotAfo_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqInt", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresSeqInt(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresSeqInt_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "regEst", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresRegEst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorRegEst_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSuf", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCodSuf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSuf_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFonFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "temOrm", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTemOrm(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTemOrm_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cliFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCliFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCliFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cxaPst", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCxaPst(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCxaPst_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datCad", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDatCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatCad_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "gerDir", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresGerDir(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresGerDir_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horAtu", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresHorAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorAtu_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIpi", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTriIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIpi_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIcm", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRecIcm(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIcm_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retOur", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetOur(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetOur_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codTri", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCodTri(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodTri_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recPis", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRecPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecPis_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipPgt", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTipPgt(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTipPgt_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retIrf", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetIrf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetIrf_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sitFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresSitFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSitFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCli", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCodCli(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodCli_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "emaNfe", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresEmaNfe(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEmaNfe_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "horCad", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresHorCad(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorHorCad_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "numRge", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresNumRge(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNumRge_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "limRet", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresLimRet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorLimRet_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomVen", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresNomVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresNomVen_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "recIpi", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRecIpi(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRecIpi_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "triIss", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTriIss(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTriIss_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "datAtu", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresDatAtu(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorDatAtu_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRam", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCodRam(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodRam_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codMot", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCodMot(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodMot_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTipFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forTra", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForTra(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForTra_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonVen", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFonVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresFonVen_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insEst", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresInsEst(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorInsEst_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "qtdDep", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresQtdDep(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresQtdDep_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "forRep", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresForRep(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorForRep_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "indCoo", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresIndCoo(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresIndCoo_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perPid", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresPerPid(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerPid_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipMer", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTipMer(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorTipMer_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cgcCpf", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCgcCpf(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCgcCpf_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perIcm", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresPerIcm(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerIcm_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCof", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetCof(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCof_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retCsl", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetCsl(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetCsl_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo3", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFonFo3(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo3_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "fonFo2", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFonFo2(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFonFo2_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRin", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresPerRin(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRin_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perRir", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresPerRir(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerRir_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "intNet", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresIntNet(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorIntNet_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresNomFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNomFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxVen", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFaxVen(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresFaxVen_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRtr", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCodRtr(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodRtr_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codSro", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCodSro(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodSro_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "insAnp", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresInsAnp(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresInsAnp_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqRoe", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresSeqRoe(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorSeqRoe_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tipFav", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresTipFav(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresTipFav_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "faxFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresFaxFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorFaxFor_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "notFor", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresNotFor(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorNotFor_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "retPis", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresRetPis(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorRetPis_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "rmlVen", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresRmlVen(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresRmlVen_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "obsMot", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresObsMot(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorObsMot_QNAME, String.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codGre", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCodGre(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCodGre_QNAME, Integer.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "perCod", scope = FornecedoresExportarOutGridFornecedores.class)
    public JAXBElement<Double> createFornecedoresExportarOutGridFornecedoresPerCod(Double value) {
        return new JAXBElement<Double>(_FornecedoresImportarInFornecedorPerCod_QNAME, Double.class, FornecedoresExportarOutGridFornecedores.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cplEnd", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepCplEnd(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCplEnd_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomUfs", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepNomUfs(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCepNomUfs_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "sigUfs", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepSigUfs(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorSigUfs_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepIni", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCepCepIni(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCepCepIni_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nenFor", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepNenFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorNenFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cepFor", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCepCepFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCepFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codRai", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCepCodRai(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCepCodRai_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "endFor", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepEndFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorEndFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "nomPai", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepNomPai(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCepNomPai_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codPai", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepCodPai(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCodPai_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "baiFor", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepBaiFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorBaiFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "cidFor", scope = FornecedoresExportarOutGridFornecedoresCep.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCepCidFor(String value) {
        return new JAXBElement<String>(_FornecedoresImportarInFornecedorCidFor_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBie", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresINSSVlrBie(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrBie_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrInt", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresINSSVlrInt(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrInt_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mesAno", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<String> createFornecedoresExportar2OutGridFornecedoresINSSMesAno(String value) {
        return new JAXBElement<String>(_FornecedoresExportarOutGridFornecedoresINSSMesAno_QNAME, String.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrBit", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresINSSVlrBit(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrBit_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "vlrIne", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<Double> createFornecedoresExportar2OutGridFornecedoresINSSVlrIne(Double value) {
        return new JAXBElement<Double>(_FornecedoresExportarOutGridFornecedoresINSSVlrIne_QNAME, Double.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportar2OutGridFornecedoresINSS.class)
    public JAXBElement<Integer> createFornecedoresExportar2OutGridFornecedoresINSSCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportar2OutGridFornecedoresINSS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "msgErr", scope = FornecedoresExportarOutGridErros.class)
    public JAXBElement<String> createFornecedoresExportarOutGridErrosMsgErr(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridErrosMsgErr_QNAME, String.class, FornecedoresExportarOutGridErros.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcc", scope = FornecedoresExportarOutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCaracteristicasCodCcc(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcc_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "seqOrd", scope = FornecedoresExportarOutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCaracteristicasSeqOrd(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasSeqOrd_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codFor", scope = FornecedoresExportarOutGridFornecedoresCaracteristicas.class)
    public JAXBElement<Integer> createFornecedoresExportarOutGridFornecedoresCaracteristicasCodFor(Integer value) {
        return new JAXBElement<Integer>(_FornecedoresImportarInFornecedorCodFor_QNAME, Integer.class, FornecedoresExportarOutGridFornecedoresCaracteristicas.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "codCcl", scope = FornecedoresExportarOutGridFornecedoresCaracteristicas.class)
    public JAXBElement<String> createFornecedoresExportarOutGridFornecedoresCaracteristicasCodCcl(String value) {
        return new JAXBElement<String>(_FornecedoresExportar2OutGridFornecedoresCaracteristicasCodCcl_QNAME, String.class, FornecedoresExportarOutGridFornecedoresCaracteristicas.class, value);
    }

}
