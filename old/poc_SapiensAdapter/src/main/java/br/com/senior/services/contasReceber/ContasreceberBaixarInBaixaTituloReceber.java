
package br.com.senior.services.contasReceber;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de contasreceberBaixarInBaixaTituloReceber complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="contasreceberBaixarInBaixaTituloReceber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cnpjFilial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFpj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTpt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cotMcr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datCco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datLib" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datMov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datPgt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diaAtr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="diaJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="empCco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="filRlc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPrj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numRlc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rateios" type="{http://services.senior.com.br}contasreceberBaixarInBaixaTituloReceberRateios" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="seqCco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqMov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqRlc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tnsBxa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tptRlc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrBco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrCom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrCor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrEnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrJrs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrLiq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrMov" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrMul" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrOde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "contasreceberBaixarInBaixaTituloReceber", propOrder = {
    "cnpjFilial",
    "codEmp",
    "codFil",
    "codFpg",
    "codFpj",
    "codTns",
    "codTpt",
    "cotMcr",
    "datCco",
    "datLib",
    "datMov",
    "datPgt",
    "diaAtr",
    "diaJrs",
    "empCco",
    "filRlc",
    "numCco",
    "numDoc",
    "numPrj",
    "numRlc",
    "numTit",
    "rateios",
    "seqCco",
    "seqMov",
    "seqRlc",
    "tnsBxa",
    "tptRlc",
    "vlrBco",
    "vlrCom",
    "vlrCor",
    "vlrDsc",
    "vlrEnc",
    "vlrJrs",
    "vlrLiq",
    "vlrMov",
    "vlrMul",
    "vlrOac",
    "vlrOde"
})
public class ContasreceberBaixarInBaixaTituloReceber {

    @XmlElementRef(name = "cnpjFilial", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cnpjFilial;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFil;
    @XmlElementRef(name = "codFpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpg;
    @XmlElementRef(name = "codFpj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codFpj;
    @XmlElementRef(name = "codTns", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTns;
    @XmlElementRef(name = "codTpt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTpt;
    @XmlElementRef(name = "cotMcr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cotMcr;
    @XmlElementRef(name = "datCco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datCco;
    @XmlElementRef(name = "datLib", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datLib;
    @XmlElementRef(name = "datMov", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datMov;
    @XmlElementRef(name = "datPgt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datPgt;
    @XmlElementRef(name = "diaAtr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> diaAtr;
    @XmlElementRef(name = "diaJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> diaJrs;
    @XmlElementRef(name = "empCco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> empCco;
    @XmlElementRef(name = "filRlc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> filRlc;
    @XmlElementRef(name = "numCco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numCco;
    @XmlElementRef(name = "numDoc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numDoc;
    @XmlElementRef(name = "numPrj", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numPrj;
    @XmlElementRef(name = "numRlc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numRlc;
    @XmlElementRef(name = "numTit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numTit;
    @XmlElement(nillable = true)
    protected List<ContasreceberBaixarInBaixaTituloReceberRateios> rateios;
    @XmlElementRef(name = "seqCco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqCco;
    @XmlElementRef(name = "seqMov", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqMov;
    @XmlElementRef(name = "seqRlc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> seqRlc;
    @XmlElementRef(name = "tnsBxa", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tnsBxa;
    @XmlElementRef(name = "tptRlc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tptRlc;
    @XmlElementRef(name = "vlrBco", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrBco;
    @XmlElementRef(name = "vlrCom", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrCom;
    @XmlElementRef(name = "vlrCor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrCor;
    @XmlElementRef(name = "vlrDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrDsc;
    @XmlElementRef(name = "vlrEnc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrEnc;
    @XmlElementRef(name = "vlrJrs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrJrs;
    @XmlElementRef(name = "vlrLiq", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrLiq;
    @XmlElementRef(name = "vlrMov", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrMov;
    @XmlElementRef(name = "vlrMul", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrMul;
    @XmlElementRef(name = "vlrOac", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOac;
    @XmlElementRef(name = "vlrOde", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vlrOde;

    /**
     * Obtem o valor da propriedade cnpjFilial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCnpjFilial() {
        return cnpjFilial;
    }

    /**
     * Define o valor da propriedade cnpjFilial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCnpjFilial(JAXBElement<String> value) {
        this.cnpjFilial = value;
    }

    /**
     * Obtem o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<String> value) {
        this.codEmp = value;
    }

    /**
     * Obtem o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<String> value) {
        this.codFil = value;
    }

    /**
     * Obtem o valor da propriedade codFpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpg() {
        return codFpg;
    }

    /**
     * Define o valor da propriedade codFpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpg(JAXBElement<String> value) {
        this.codFpg = value;
    }

    /**
     * Obtem o valor da propriedade codFpj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodFpj() {
        return codFpj;
    }

    /**
     * Define o valor da propriedade codFpj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodFpj(JAXBElement<String> value) {
        this.codFpj = value;
    }

    /**
     * Obtem o valor da propriedade codTns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTns() {
        return codTns;
    }

    /**
     * Define o valor da propriedade codTns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTns(JAXBElement<String> value) {
        this.codTns = value;
    }

    /**
     * Obtem o valor da propriedade codTpt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTpt() {
        return codTpt;
    }

    /**
     * Define o valor da propriedade codTpt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTpt(JAXBElement<String> value) {
        this.codTpt = value;
    }

    /**
     * Obtem o valor da propriedade cotMcr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCotMcr() {
        return cotMcr;
    }

    /**
     * Define o valor da propriedade cotMcr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCotMcr(JAXBElement<String> value) {
        this.cotMcr = value;
    }

    /**
     * Obtem o valor da propriedade datCco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatCco() {
        return datCco;
    }

    /**
     * Define o valor da propriedade datCco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatCco(JAXBElement<String> value) {
        this.datCco = value;
    }

    /**
     * Obtem o valor da propriedade datLib.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatLib() {
        return datLib;
    }

    /**
     * Define o valor da propriedade datLib.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatLib(JAXBElement<String> value) {
        this.datLib = value;
    }

    /**
     * Obtem o valor da propriedade datMov.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatMov() {
        return datMov;
    }

    /**
     * Define o valor da propriedade datMov.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatMov(JAXBElement<String> value) {
        this.datMov = value;
    }

    /**
     * Obtem o valor da propriedade datPgt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatPgt() {
        return datPgt;
    }

    /**
     * Define o valor da propriedade datPgt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatPgt(JAXBElement<String> value) {
        this.datPgt = value;
    }

    /**
     * Obtem o valor da propriedade diaAtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiaAtr() {
        return diaAtr;
    }

    /**
     * Define o valor da propriedade diaAtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiaAtr(JAXBElement<String> value) {
        this.diaAtr = value;
    }

    /**
     * Obtem o valor da propriedade diaJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDiaJrs() {
        return diaJrs;
    }

    /**
     * Define o valor da propriedade diaJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDiaJrs(JAXBElement<String> value) {
        this.diaJrs = value;
    }

    /**
     * Obtem o valor da propriedade empCco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmpCco() {
        return empCco;
    }

    /**
     * Define o valor da propriedade empCco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmpCco(JAXBElement<String> value) {
        this.empCco = value;
    }

    /**
     * Obtem o valor da propriedade filRlc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFilRlc() {
        return filRlc;
    }

    /**
     * Define o valor da propriedade filRlc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFilRlc(JAXBElement<String> value) {
        this.filRlc = value;
    }

    /**
     * Obtem o valor da propriedade numCco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumCco() {
        return numCco;
    }

    /**
     * Define o valor da propriedade numCco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumCco(JAXBElement<String> value) {
        this.numCco = value;
    }

    /**
     * Obtem o valor da propriedade numDoc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumDoc() {
        return numDoc;
    }

    /**
     * Define o valor da propriedade numDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumDoc(JAXBElement<String> value) {
        this.numDoc = value;
    }

    /**
     * Obtem o valor da propriedade numPrj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumPrj() {
        return numPrj;
    }

    /**
     * Define o valor da propriedade numPrj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumPrj(JAXBElement<String> value) {
        this.numPrj = value;
    }

    /**
     * Obtem o valor da propriedade numRlc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumRlc() {
        return numRlc;
    }

    /**
     * Define o valor da propriedade numRlc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumRlc(JAXBElement<String> value) {
        this.numRlc = value;
    }

    /**
     * Obtem o valor da propriedade numTit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumTit() {
        return numTit;
    }

    /**
     * Define o valor da propriedade numTit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumTit(JAXBElement<String> value) {
        this.numTit = value;
    }

    /**
     * Gets the value of the rateios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContasreceberBaixarInBaixaTituloReceberRateios }
     * 
     * 
     */
    public List<ContasreceberBaixarInBaixaTituloReceberRateios> getRateios() {
        if (rateios == null) {
            rateios = new ArrayList<ContasreceberBaixarInBaixaTituloReceberRateios>();
        }
        return this.rateios;
    }

    /**
     * Obtem o valor da propriedade seqCco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqCco() {
        return seqCco;
    }

    /**
     * Define o valor da propriedade seqCco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqCco(JAXBElement<String> value) {
        this.seqCco = value;
    }

    /**
     * Obtem o valor da propriedade seqMov.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqMov() {
        return seqMov;
    }

    /**
     * Define o valor da propriedade seqMov.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqMov(JAXBElement<String> value) {
        this.seqMov = value;
    }

    /**
     * Obtem o valor da propriedade seqRlc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSeqRlc() {
        return seqRlc;
    }

    /**
     * Define o valor da propriedade seqRlc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSeqRlc(JAXBElement<String> value) {
        this.seqRlc = value;
    }

    /**
     * Obtem o valor da propriedade tnsBxa.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTnsBxa() {
        return tnsBxa;
    }

    /**
     * Define o valor da propriedade tnsBxa.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTnsBxa(JAXBElement<String> value) {
        this.tnsBxa = value;
    }

    /**
     * Obtem o valor da propriedade tptRlc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTptRlc() {
        return tptRlc;
    }

    /**
     * Define o valor da propriedade tptRlc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTptRlc(JAXBElement<String> value) {
        this.tptRlc = value;
    }

    /**
     * Obtem o valor da propriedade vlrBco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrBco() {
        return vlrBco;
    }

    /**
     * Define o valor da propriedade vlrBco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrBco(JAXBElement<String> value) {
        this.vlrBco = value;
    }

    /**
     * Obtem o valor da propriedade vlrCom.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrCom() {
        return vlrCom;
    }

    /**
     * Define o valor da propriedade vlrCom.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrCom(JAXBElement<String> value) {
        this.vlrCom = value;
    }

    /**
     * Obtem o valor da propriedade vlrCor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrCor() {
        return vlrCor;
    }

    /**
     * Define o valor da propriedade vlrCor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrCor(JAXBElement<String> value) {
        this.vlrCor = value;
    }

    /**
     * Obtem o valor da propriedade vlrDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrDsc() {
        return vlrDsc;
    }

    /**
     * Define o valor da propriedade vlrDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrDsc(JAXBElement<String> value) {
        this.vlrDsc = value;
    }

    /**
     * Obtem o valor da propriedade vlrEnc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrEnc() {
        return vlrEnc;
    }

    /**
     * Define o valor da propriedade vlrEnc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrEnc(JAXBElement<String> value) {
        this.vlrEnc = value;
    }

    /**
     * Obtem o valor da propriedade vlrJrs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrJrs() {
        return vlrJrs;
    }

    /**
     * Define o valor da propriedade vlrJrs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrJrs(JAXBElement<String> value) {
        this.vlrJrs = value;
    }

    /**
     * Obtem o valor da propriedade vlrLiq.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrLiq() {
        return vlrLiq;
    }

    /**
     * Define o valor da propriedade vlrLiq.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrLiq(JAXBElement<String> value) {
        this.vlrLiq = value;
    }

    /**
     * Obtem o valor da propriedade vlrMov.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrMov() {
        return vlrMov;
    }

    /**
     * Define o valor da propriedade vlrMov.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrMov(JAXBElement<String> value) {
        this.vlrMov = value;
    }

    /**
     * Obtem o valor da propriedade vlrMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrMul() {
        return vlrMul;
    }

    /**
     * Define o valor da propriedade vlrMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrMul(JAXBElement<String> value) {
        this.vlrMul = value;
    }

    /**
     * Obtem o valor da propriedade vlrOac.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOac() {
        return vlrOac;
    }

    /**
     * Define o valor da propriedade vlrOac.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOac(JAXBElement<String> value) {
        this.vlrOac = value;
    }

    /**
     * Obtem o valor da propriedade vlrOde.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVlrOde() {
        return vlrOde;
    }

    /**
     * Define o valor da propriedade vlrOde.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVlrOde(JAXBElement<String> value) {
        this.vlrOde = value;
    }

}
