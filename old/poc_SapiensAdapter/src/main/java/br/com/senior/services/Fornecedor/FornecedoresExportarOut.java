
package br.com.senior.services.Fornecedor;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportarOut complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportarOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="erroExecucao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="finalizaramRegistros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gridErros" type="{http://services.senior.com.br}fornecedoresExportarOutGridErros" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="gridFornecedores" type="{http://services.senior.com.br}fornecedoresExportarOutGridFornecedores" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mensagemRetorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroLote" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tipoRetorno" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportarOut", propOrder = {
    "erroExecucao",
    "finalizaramRegistros",
    "gridErros",
    "gridFornecedores",
    "mensagemRetorno",
    "numeroLote",
    "tipoRetorno"
})
public class FornecedoresExportarOut {

    @XmlElementRef(name = "erroExecucao", type = JAXBElement.class, required = false)
    protected JAXBElement<String> erroExecucao;
    @XmlElementRef(name = "finalizaramRegistros", type = JAXBElement.class, required = false)
    protected JAXBElement<String> finalizaramRegistros;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportarOutGridErros> gridErros;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportarOutGridFornecedores> gridFornecedores;
    @XmlElementRef(name = "mensagemRetorno", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mensagemRetorno;
    @XmlElementRef(name = "numeroLote", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> numeroLote;
    @XmlElementRef(name = "tipoRetorno", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tipoRetorno;

    /**
     * Obtem o valor da propriedade erroExecucao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErroExecucao() {
        return erroExecucao;
    }

    /**
     * Define o valor da propriedade erroExecucao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErroExecucao(JAXBElement<String> value) {
        this.erroExecucao = value;
    }

    /**
     * Obtem o valor da propriedade finalizaramRegistros.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFinalizaramRegistros() {
        return finalizaramRegistros;
    }

    /**
     * Define o valor da propriedade finalizaramRegistros.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFinalizaramRegistros(JAXBElement<String> value) {
        this.finalizaramRegistros = value;
    }

    /**
     * Gets the value of the gridErros property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gridErros property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGridErros().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportarOutGridErros }
     * 
     * 
     */
    public List<FornecedoresExportarOutGridErros> getGridErros() {
        if (gridErros == null) {
            gridErros = new ArrayList<FornecedoresExportarOutGridErros>();
        }
        return this.gridErros;
    }

    /**
     * Gets the value of the gridFornecedores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gridFornecedores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGridFornecedores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportarOutGridFornecedores }
     * 
     * 
     */
    public List<FornecedoresExportarOutGridFornecedores> getGridFornecedores() {
        if (gridFornecedores == null) {
            gridFornecedores = new ArrayList<FornecedoresExportarOutGridFornecedores>();
        }
        return this.gridFornecedores;
    }

    /**
     * Obtem o valor da propriedade mensagemRetorno.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMensagemRetorno() {
        return mensagemRetorno;
    }

    /**
     * Define o valor da propriedade mensagemRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMensagemRetorno(JAXBElement<String> value) {
        this.mensagemRetorno = value;
    }

    /**
     * Obtem o valor da propriedade numeroLote.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getNumeroLote() {
        return numeroLote;
    }

    /**
     * Define o valor da propriedade numeroLote.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setNumeroLote(JAXBElement<Integer> value) {
        this.numeroLote = value;
    }

    /**
     * Obtem o valor da propriedade tipoRetorno.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTipoRetorno() {
        return tipoRetorno;
    }

    /**
     * Define o valor da propriedade tipoRetorno.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTipoRetorno(JAXBElement<Integer> value) {
        this.tipoRetorno = value;
    }

}
