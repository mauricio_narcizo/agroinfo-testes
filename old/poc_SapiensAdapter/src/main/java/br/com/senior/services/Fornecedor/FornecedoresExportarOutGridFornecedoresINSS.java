
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportarOutGridFornecedoresINSS complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportarOutGridFornecedoresINSS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="mesAno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vlrBie" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrBit" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrIne" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrInt" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportarOutGridFornecedoresINSS", propOrder = {
    "codFor",
    "mesAno",
    "vlrBie",
    "vlrBit",
    "vlrIne",
    "vlrInt"
})
public class FornecedoresExportarOutGridFornecedoresINSS {

    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "mesAno", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mesAno;
    @XmlElementRef(name = "vlrBie", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrBie;
    @XmlElementRef(name = "vlrBit", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrBit;
    @XmlElementRef(name = "vlrIne", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrIne;
    @XmlElementRef(name = "vlrInt", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrInt;

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade mesAno.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMesAno() {
        return mesAno;
    }

    /**
     * Define o valor da propriedade mesAno.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMesAno(JAXBElement<String> value) {
        this.mesAno = value;
    }

    /**
     * Obtem o valor da propriedade vlrBie.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrBie() {
        return vlrBie;
    }

    /**
     * Define o valor da propriedade vlrBie.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrBie(JAXBElement<Double> value) {
        this.vlrBie = value;
    }

    /**
     * Obtem o valor da propriedade vlrBit.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrBit() {
        return vlrBit;
    }

    /**
     * Define o valor da propriedade vlrBit.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrBit(JAXBElement<Double> value) {
        this.vlrBit = value;
    }

    /**
     * Obtem o valor da propriedade vlrIne.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrIne() {
        return vlrIne;
    }

    /**
     * Define o valor da propriedade vlrIne.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrIne(JAXBElement<Double> value) {
        this.vlrIne = value;
    }

    /**
     * Obtem o valor da propriedade vlrInt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrInt() {
        return vlrInt;
    }

    /**
     * Define o valor da propriedade vlrInt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrInt(JAXBElement<Double> value) {
        this.vlrInt = value;
    }

}
