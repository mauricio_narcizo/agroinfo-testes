
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedoresDefHis complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedoresDefHis">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="antDsc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ccbFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cifFob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codBan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFav" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFpg" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codPor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTpr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTra" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="conEst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cprCat" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cprCpe" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cprCql" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="criEdv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="criRat" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaAad" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaAux" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaFcr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaFdv" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaRcr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ctaRed" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="datAtr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datMcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datUcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datUpe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datUpg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="forMon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maiAtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="medAtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numCcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagDtj" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pagDtm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="pagJmm" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pagMul" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pagTir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perDs1" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs2" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs3" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs4" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDs5" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perDsc" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perEmb" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perEnc" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perFre" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perFun" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIne" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIns" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIrf" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIss" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perOut" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perSeg" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="pgtFre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pgtMon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="przEnt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="qtdDcv" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="qtdPgt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rvlCfr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlDar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlEmb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlEnc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlFei" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlFre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlOui" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlOut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlSeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rvlSei" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="salCre" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="salDup" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="salOut" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="seqOrm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="serCur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tolDsc" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ultDup" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrAtr" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrMcp" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrUcp" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrUpe" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="vlrUpg" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedoresDefHis", propOrder = {
    "antDsc",
    "ccbFor",
    "cifFob",
    "codAge",
    "codBan",
    "codCpg",
    "codCrp",
    "codCrt",
    "codDep",
    "codEmp",
    "codFav",
    "codFil",
    "codFor",
    "codFpg",
    "codPor",
    "codTpr",
    "codTra",
    "conEst",
    "cprCat",
    "cprCpe",
    "cprCql",
    "criEdv",
    "criRat",
    "ctaAad",
    "ctaAux",
    "ctaFcr",
    "ctaFdv",
    "ctaRcr",
    "ctaRed",
    "datAtr",
    "datMcp",
    "datUcp",
    "datUpe",
    "datUpg",
    "forMon",
    "indInd",
    "maiAtr",
    "medAtr",
    "numCcc",
    "pagDtj",
    "pagDtm",
    "pagJmm",
    "pagMul",
    "pagTir",
    "perDs1",
    "perDs2",
    "perDs3",
    "perDs4",
    "perDs5",
    "perDsc",
    "perEmb",
    "perEnc",
    "perFre",
    "perFun",
    "perIne",
    "perIns",
    "perIrf",
    "perIss",
    "perOut",
    "perSeg",
    "pgtFre",
    "pgtMon",
    "przEnt",
    "qtdDcv",
    "qtdPgt",
    "rvlCfr",
    "rvlDar",
    "rvlEmb",
    "rvlEnc",
    "rvlFei",
    "rvlFre",
    "rvlOui",
    "rvlOut",
    "rvlSeg",
    "rvlSei",
    "salCre",
    "salDup",
    "salOut",
    "seqOrm",
    "serCur",
    "tolDsc",
    "ultDup",
    "vlrAtr",
    "vlrMcp",
    "vlrUcp",
    "vlrUpe",
    "vlrUpg"
})
public class FornecedoresExportar2OutGridFornecedoresDefHis {

    @XmlElementRef(name = "antDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> antDsc;
    @XmlElementRef(name = "ccbFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ccbFor;
    @XmlElementRef(name = "cifFob", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cifFob;
    @XmlElementRef(name = "codAge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAge;
    @XmlElementRef(name = "codBan", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codBan;
    @XmlElementRef(name = "codCpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCpg;
    @XmlElementRef(name = "codCrp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrp;
    @XmlElementRef(name = "codCrt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCrt;
    @XmlElementRef(name = "codDep", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codDep;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codEmp;
    @XmlElementRef(name = "codFav", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> codFav;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFil;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "codFpg", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFpg;
    @XmlElementRef(name = "codPor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPor;
    @XmlElementRef(name = "codTpr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTpr;
    @XmlElementRef(name = "codTra", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codTra;
    @XmlElementRef(name = "conEst", type = JAXBElement.class, required = false)
    protected JAXBElement<String> conEst;
    @XmlElementRef(name = "cprCat", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cprCat;
    @XmlElementRef(name = "cprCpe", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cprCpe;
    @XmlElementRef(name = "cprCql", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cprCql;
    @XmlElementRef(name = "criEdv", type = JAXBElement.class, required = false)
    protected JAXBElement<String> criEdv;
    @XmlElementRef(name = "criRat", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> criRat;
    @XmlElementRef(name = "ctaAad", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaAad;
    @XmlElementRef(name = "ctaAux", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaAux;
    @XmlElementRef(name = "ctaFcr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaFcr;
    @XmlElementRef(name = "ctaFdv", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaFdv;
    @XmlElementRef(name = "ctaRcr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaRcr;
    @XmlElementRef(name = "ctaRed", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ctaRed;
    @XmlElementRef(name = "datAtr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datAtr;
    @XmlElementRef(name = "datMcp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datMcp;
    @XmlElementRef(name = "datUcp", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datUcp;
    @XmlElementRef(name = "datUpe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datUpe;
    @XmlElementRef(name = "datUpg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datUpg;
    @XmlElementRef(name = "forMon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forMon;
    @XmlElementRef(name = "indInd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indInd;
    @XmlElementRef(name = "maiAtr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> maiAtr;
    @XmlElementRef(name = "medAtr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> medAtr;
    @XmlElementRef(name = "numCcc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numCcc;
    @XmlElementRef(name = "pagDtj", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> pagDtj;
    @XmlElementRef(name = "pagDtm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> pagDtm;
    @XmlElementRef(name = "pagJmm", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> pagJmm;
    @XmlElementRef(name = "pagMul", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> pagMul;
    @XmlElementRef(name = "pagTir", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pagTir;
    @XmlElementRef(name = "perDs1", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs1;
    @XmlElementRef(name = "perDs2", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs2;
    @XmlElementRef(name = "perDs3", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs3;
    @XmlElementRef(name = "perDs4", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs4;
    @XmlElementRef(name = "perDs5", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDs5;
    @XmlElementRef(name = "perDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perDsc;
    @XmlElementRef(name = "perEmb", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perEmb;
    @XmlElementRef(name = "perEnc", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perEnc;
    @XmlElementRef(name = "perFre", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perFre;
    @XmlElementRef(name = "perFun", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perFun;
    @XmlElementRef(name = "perIne", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIne;
    @XmlElementRef(name = "perIns", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIns;
    @XmlElementRef(name = "perIrf", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIrf;
    @XmlElementRef(name = "perIss", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIss;
    @XmlElementRef(name = "perOut", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perOut;
    @XmlElementRef(name = "perSeg", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perSeg;
    @XmlElementRef(name = "pgtFre", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pgtFre;
    @XmlElementRef(name = "pgtMon", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pgtMon;
    @XmlElementRef(name = "przEnt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> przEnt;
    @XmlElementRef(name = "qtdDcv", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> qtdDcv;
    @XmlElementRef(name = "qtdPgt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> qtdPgt;
    @XmlElementRef(name = "rvlCfr", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlCfr;
    @XmlElementRef(name = "rvlDar", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlDar;
    @XmlElementRef(name = "rvlEmb", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlEmb;
    @XmlElementRef(name = "rvlEnc", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlEnc;
    @XmlElementRef(name = "rvlFei", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlFei;
    @XmlElementRef(name = "rvlFre", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlFre;
    @XmlElementRef(name = "rvlOui", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlOui;
    @XmlElementRef(name = "rvlOut", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlOut;
    @XmlElementRef(name = "rvlSeg", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlSeg;
    @XmlElementRef(name = "rvlSei", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rvlSei;
    @XmlElementRef(name = "salCre", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> salCre;
    @XmlElementRef(name = "salDup", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> salDup;
    @XmlElementRef(name = "salOut", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> salOut;
    @XmlElementRef(name = "seqOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqOrm;
    @XmlElementRef(name = "serCur", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serCur;
    @XmlElementRef(name = "tolDsc", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> tolDsc;
    @XmlElementRef(name = "ultDup", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> ultDup;
    @XmlElementRef(name = "vlrAtr", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrAtr;
    @XmlElementRef(name = "vlrMcp", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrMcp;
    @XmlElementRef(name = "vlrUcp", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrUcp;
    @XmlElementRef(name = "vlrUpe", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrUpe;
    @XmlElementRef(name = "vlrUpg", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> vlrUpg;

    /**
     * Obtem o valor da propriedade antDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAntDsc() {
        return antDsc;
    }

    /**
     * Define o valor da propriedade antDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAntDsc(JAXBElement<String> value) {
        this.antDsc = value;
    }

    /**
     * Obtem o valor da propriedade ccbFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCcbFor() {
        return ccbFor;
    }

    /**
     * Define o valor da propriedade ccbFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCcbFor(JAXBElement<String> value) {
        this.ccbFor = value;
    }

    /**
     * Obtem o valor da propriedade cifFob.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCifFob() {
        return cifFob;
    }

    /**
     * Define o valor da propriedade cifFob.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCifFob(JAXBElement<String> value) {
        this.cifFob = value;
    }

    /**
     * Obtem o valor da propriedade codAge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAge() {
        return codAge;
    }

    /**
     * Define o valor da propriedade codAge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAge(JAXBElement<String> value) {
        this.codAge = value;
    }

    /**
     * Obtem o valor da propriedade codBan.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodBan() {
        return codBan;
    }

    /**
     * Define o valor da propriedade codBan.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodBan(JAXBElement<String> value) {
        this.codBan = value;
    }

    /**
     * Obtem o valor da propriedade codCpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCpg() {
        return codCpg;
    }

    /**
     * Define o valor da propriedade codCpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCpg(JAXBElement<String> value) {
        this.codCpg = value;
    }

    /**
     * Obtem o valor da propriedade codCrp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrp() {
        return codCrp;
    }

    /**
     * Define o valor da propriedade codCrp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrp(JAXBElement<String> value) {
        this.codCrp = value;
    }

    /**
     * Obtem o valor da propriedade codCrt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCrt() {
        return codCrt;
    }

    /**
     * Define o valor da propriedade codCrt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCrt(JAXBElement<String> value) {
        this.codCrt = value;
    }

    /**
     * Obtem o valor da propriedade codDep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodDep() {
        return codDep;
    }

    /**
     * Define o valor da propriedade codDep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodDep(JAXBElement<String> value) {
        this.codDep = value;
    }

    /**
     * Obtem o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<Integer> value) {
        this.codEmp = value;
    }

    /**
     * Obtem o valor da propriedade codFav.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCodFav() {
        return codFav;
    }

    /**
     * Define o valor da propriedade codFav.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCodFav(JAXBElement<Double> value) {
        this.codFav = value;
    }

    /**
     * Obtem o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<Integer> value) {
        this.codFil = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade codFpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFpg() {
        return codFpg;
    }

    /**
     * Define o valor da propriedade codFpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFpg(JAXBElement<Integer> value) {
        this.codFpg = value;
    }

    /**
     * Obtem o valor da propriedade codPor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPor() {
        return codPor;
    }

    /**
     * Define o valor da propriedade codPor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPor(JAXBElement<String> value) {
        this.codPor = value;
    }

    /**
     * Obtem o valor da propriedade codTpr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTpr() {
        return codTpr;
    }

    /**
     * Define o valor da propriedade codTpr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTpr(JAXBElement<String> value) {
        this.codTpr = value;
    }

    /**
     * Obtem o valor da propriedade codTra.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodTra() {
        return codTra;
    }

    /**
     * Define o valor da propriedade codTra.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodTra(JAXBElement<Integer> value) {
        this.codTra = value;
    }

    /**
     * Obtem o valor da propriedade conEst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getConEst() {
        return conEst;
    }

    /**
     * Define o valor da propriedade conEst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setConEst(JAXBElement<String> value) {
        this.conEst = value;
    }

    /**
     * Obtem o valor da propriedade cprCat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCprCat() {
        return cprCat;
    }

    /**
     * Define o valor da propriedade cprCat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCprCat(JAXBElement<Integer> value) {
        this.cprCat = value;
    }

    /**
     * Obtem o valor da propriedade cprCpe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCprCpe() {
        return cprCpe;
    }

    /**
     * Define o valor da propriedade cprCpe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCprCpe(JAXBElement<Integer> value) {
        this.cprCpe = value;
    }

    /**
     * Obtem o valor da propriedade cprCql.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCprCql() {
        return cprCql;
    }

    /**
     * Define o valor da propriedade cprCql.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCprCql(JAXBElement<Integer> value) {
        this.cprCql = value;
    }

    /**
     * Obtem o valor da propriedade criEdv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCriEdv() {
        return criEdv;
    }

    /**
     * Define o valor da propriedade criEdv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCriEdv(JAXBElement<String> value) {
        this.criEdv = value;
    }

    /**
     * Obtem o valor da propriedade criRat.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCriRat() {
        return criRat;
    }

    /**
     * Define o valor da propriedade criRat.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCriRat(JAXBElement<Integer> value) {
        this.criRat = value;
    }

    /**
     * Obtem o valor da propriedade ctaAad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaAad() {
        return ctaAad;
    }

    /**
     * Define o valor da propriedade ctaAad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaAad(JAXBElement<Integer> value) {
        this.ctaAad = value;
    }

    /**
     * Obtem o valor da propriedade ctaAux.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaAux() {
        return ctaAux;
    }

    /**
     * Define o valor da propriedade ctaAux.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaAux(JAXBElement<Integer> value) {
        this.ctaAux = value;
    }

    /**
     * Obtem o valor da propriedade ctaFcr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaFcr() {
        return ctaFcr;
    }

    /**
     * Define o valor da propriedade ctaFcr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaFcr(JAXBElement<Integer> value) {
        this.ctaFcr = value;
    }

    /**
     * Obtem o valor da propriedade ctaFdv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaFdv() {
        return ctaFdv;
    }

    /**
     * Define o valor da propriedade ctaFdv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaFdv(JAXBElement<Integer> value) {
        this.ctaFdv = value;
    }

    /**
     * Obtem o valor da propriedade ctaRcr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaRcr() {
        return ctaRcr;
    }

    /**
     * Define o valor da propriedade ctaRcr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaRcr(JAXBElement<Integer> value) {
        this.ctaRcr = value;
    }

    /**
     * Obtem o valor da propriedade ctaRed.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCtaRed() {
        return ctaRed;
    }

    /**
     * Define o valor da propriedade ctaRed.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCtaRed(JAXBElement<Integer> value) {
        this.ctaRed = value;
    }

    /**
     * Obtem o valor da propriedade datAtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatAtr() {
        return datAtr;
    }

    /**
     * Define o valor da propriedade datAtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatAtr(JAXBElement<String> value) {
        this.datAtr = value;
    }

    /**
     * Obtem o valor da propriedade datMcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatMcp() {
        return datMcp;
    }

    /**
     * Define o valor da propriedade datMcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatMcp(JAXBElement<String> value) {
        this.datMcp = value;
    }

    /**
     * Obtem o valor da propriedade datUcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatUcp() {
        return datUcp;
    }

    /**
     * Define o valor da propriedade datUcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatUcp(JAXBElement<String> value) {
        this.datUcp = value;
    }

    /**
     * Obtem o valor da propriedade datUpe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatUpe() {
        return datUpe;
    }

    /**
     * Define o valor da propriedade datUpe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatUpe(JAXBElement<String> value) {
        this.datUpe = value;
    }

    /**
     * Obtem o valor da propriedade datUpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatUpg() {
        return datUpg;
    }

    /**
     * Define o valor da propriedade datUpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatUpg(JAXBElement<String> value) {
        this.datUpg = value;
    }

    /**
     * Obtem o valor da propriedade forMon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForMon() {
        return forMon;
    }

    /**
     * Define o valor da propriedade forMon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForMon(JAXBElement<String> value) {
        this.forMon = value;
    }

    /**
     * Obtem o valor da propriedade indInd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndInd() {
        return indInd;
    }

    /**
     * Define o valor da propriedade indInd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndInd(JAXBElement<String> value) {
        this.indInd = value;
    }

    /**
     * Obtem o valor da propriedade maiAtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMaiAtr() {
        return maiAtr;
    }

    /**
     * Define o valor da propriedade maiAtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMaiAtr(JAXBElement<Integer> value) {
        this.maiAtr = value;
    }

    /**
     * Obtem o valor da propriedade medAtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMedAtr() {
        return medAtr;
    }

    /**
     * Define o valor da propriedade medAtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMedAtr(JAXBElement<Integer> value) {
        this.medAtr = value;
    }

    /**
     * Obtem o valor da propriedade numCcc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumCcc() {
        return numCcc;
    }

    /**
     * Define o valor da propriedade numCcc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumCcc(JAXBElement<String> value) {
        this.numCcc = value;
    }

    /**
     * Obtem o valor da propriedade pagDtj.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPagDtj() {
        return pagDtj;
    }

    /**
     * Define o valor da propriedade pagDtj.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPagDtj(JAXBElement<Integer> value) {
        this.pagDtj = value;
    }

    /**
     * Obtem o valor da propriedade pagDtm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPagDtm() {
        return pagDtm;
    }

    /**
     * Define o valor da propriedade pagDtm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPagDtm(JAXBElement<Integer> value) {
        this.pagDtm = value;
    }

    /**
     * Obtem o valor da propriedade pagJmm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPagJmm() {
        return pagJmm;
    }

    /**
     * Define o valor da propriedade pagJmm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPagJmm(JAXBElement<Double> value) {
        this.pagJmm = value;
    }

    /**
     * Obtem o valor da propriedade pagMul.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPagMul() {
        return pagMul;
    }

    /**
     * Define o valor da propriedade pagMul.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPagMul(JAXBElement<Double> value) {
        this.pagMul = value;
    }

    /**
     * Obtem o valor da propriedade pagTir.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPagTir() {
        return pagTir;
    }

    /**
     * Define o valor da propriedade pagTir.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPagTir(JAXBElement<String> value) {
        this.pagTir = value;
    }

    /**
     * Obtem o valor da propriedade perDs1.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs1() {
        return perDs1;
    }

    /**
     * Define o valor da propriedade perDs1.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs1(JAXBElement<Double> value) {
        this.perDs1 = value;
    }

    /**
     * Obtem o valor da propriedade perDs2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs2() {
        return perDs2;
    }

    /**
     * Define o valor da propriedade perDs2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs2(JAXBElement<Double> value) {
        this.perDs2 = value;
    }

    /**
     * Obtem o valor da propriedade perDs3.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs3() {
        return perDs3;
    }

    /**
     * Define o valor da propriedade perDs3.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs3(JAXBElement<Double> value) {
        this.perDs3 = value;
    }

    /**
     * Obtem o valor da propriedade perDs4.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs4() {
        return perDs4;
    }

    /**
     * Define o valor da propriedade perDs4.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs4(JAXBElement<Double> value) {
        this.perDs4 = value;
    }

    /**
     * Obtem o valor da propriedade perDs5.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDs5() {
        return perDs5;
    }

    /**
     * Define o valor da propriedade perDs5.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDs5(JAXBElement<Double> value) {
        this.perDs5 = value;
    }

    /**
     * Obtem o valor da propriedade perDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerDsc() {
        return perDsc;
    }

    /**
     * Define o valor da propriedade perDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerDsc(JAXBElement<Double> value) {
        this.perDsc = value;
    }

    /**
     * Obtem o valor da propriedade perEmb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerEmb() {
        return perEmb;
    }

    /**
     * Define o valor da propriedade perEmb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerEmb(JAXBElement<Double> value) {
        this.perEmb = value;
    }

    /**
     * Obtem o valor da propriedade perEnc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerEnc() {
        return perEnc;
    }

    /**
     * Define o valor da propriedade perEnc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerEnc(JAXBElement<Double> value) {
        this.perEnc = value;
    }

    /**
     * Obtem o valor da propriedade perFre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerFre() {
        return perFre;
    }

    /**
     * Define o valor da propriedade perFre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerFre(JAXBElement<Double> value) {
        this.perFre = value;
    }

    /**
     * Obtem o valor da propriedade perFun.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerFun() {
        return perFun;
    }

    /**
     * Define o valor da propriedade perFun.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerFun(JAXBElement<Double> value) {
        this.perFun = value;
    }

    /**
     * Obtem o valor da propriedade perIne.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIne() {
        return perIne;
    }

    /**
     * Define o valor da propriedade perIne.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIne(JAXBElement<Double> value) {
        this.perIne = value;
    }

    /**
     * Obtem o valor da propriedade perIns.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIns() {
        return perIns;
    }

    /**
     * Define o valor da propriedade perIns.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIns(JAXBElement<Double> value) {
        this.perIns = value;
    }

    /**
     * Obtem o valor da propriedade perIrf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIrf() {
        return perIrf;
    }

    /**
     * Define o valor da propriedade perIrf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIrf(JAXBElement<Double> value) {
        this.perIrf = value;
    }

    /**
     * Obtem o valor da propriedade perIss.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIss() {
        return perIss;
    }

    /**
     * Define o valor da propriedade perIss.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIss(JAXBElement<Double> value) {
        this.perIss = value;
    }

    /**
     * Obtem o valor da propriedade perOut.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerOut() {
        return perOut;
    }

    /**
     * Define o valor da propriedade perOut.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerOut(JAXBElement<Double> value) {
        this.perOut = value;
    }

    /**
     * Obtem o valor da propriedade perSeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerSeg() {
        return perSeg;
    }

    /**
     * Define o valor da propriedade perSeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerSeg(JAXBElement<Double> value) {
        this.perSeg = value;
    }

    /**
     * Obtem o valor da propriedade pgtFre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPgtFre() {
        return pgtFre;
    }

    /**
     * Define o valor da propriedade pgtFre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPgtFre(JAXBElement<String> value) {
        this.pgtFre = value;
    }

    /**
     * Obtem o valor da propriedade pgtMon.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPgtMon() {
        return pgtMon;
    }

    /**
     * Define o valor da propriedade pgtMon.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPgtMon(JAXBElement<String> value) {
        this.pgtMon = value;
    }

    /**
     * Obtem o valor da propriedade przEnt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getPrzEnt() {
        return przEnt;
    }

    /**
     * Define o valor da propriedade przEnt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setPrzEnt(JAXBElement<Integer> value) {
        this.przEnt = value;
    }

    /**
     * Obtem o valor da propriedade qtdDcv.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getQtdDcv() {
        return qtdDcv;
    }

    /**
     * Define o valor da propriedade qtdDcv.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setQtdDcv(JAXBElement<Integer> value) {
        this.qtdDcv = value;
    }

    /**
     * Obtem o valor da propriedade qtdPgt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getQtdPgt() {
        return qtdPgt;
    }

    /**
     * Define o valor da propriedade qtdPgt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setQtdPgt(JAXBElement<Integer> value) {
        this.qtdPgt = value;
    }

    /**
     * Obtem o valor da propriedade rvlCfr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlCfr() {
        return rvlCfr;
    }

    /**
     * Define o valor da propriedade rvlCfr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlCfr(JAXBElement<String> value) {
        this.rvlCfr = value;
    }

    /**
     * Obtem o valor da propriedade rvlDar.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlDar() {
        return rvlDar;
    }

    /**
     * Define o valor da propriedade rvlDar.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlDar(JAXBElement<String> value) {
        this.rvlDar = value;
    }

    /**
     * Obtem o valor da propriedade rvlEmb.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlEmb() {
        return rvlEmb;
    }

    /**
     * Define o valor da propriedade rvlEmb.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlEmb(JAXBElement<String> value) {
        this.rvlEmb = value;
    }

    /**
     * Obtem o valor da propriedade rvlEnc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlEnc() {
        return rvlEnc;
    }

    /**
     * Define o valor da propriedade rvlEnc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlEnc(JAXBElement<String> value) {
        this.rvlEnc = value;
    }

    /**
     * Obtem o valor da propriedade rvlFei.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlFei() {
        return rvlFei;
    }

    /**
     * Define o valor da propriedade rvlFei.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlFei(JAXBElement<String> value) {
        this.rvlFei = value;
    }

    /**
     * Obtem o valor da propriedade rvlFre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlFre() {
        return rvlFre;
    }

    /**
     * Define o valor da propriedade rvlFre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlFre(JAXBElement<String> value) {
        this.rvlFre = value;
    }

    /**
     * Obtem o valor da propriedade rvlOui.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlOui() {
        return rvlOui;
    }

    /**
     * Define o valor da propriedade rvlOui.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlOui(JAXBElement<String> value) {
        this.rvlOui = value;
    }

    /**
     * Obtem o valor da propriedade rvlOut.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlOut() {
        return rvlOut;
    }

    /**
     * Define o valor da propriedade rvlOut.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlOut(JAXBElement<String> value) {
        this.rvlOut = value;
    }

    /**
     * Obtem o valor da propriedade rvlSeg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlSeg() {
        return rvlSeg;
    }

    /**
     * Define o valor da propriedade rvlSeg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlSeg(JAXBElement<String> value) {
        this.rvlSeg = value;
    }

    /**
     * Obtem o valor da propriedade rvlSei.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRvlSei() {
        return rvlSei;
    }

    /**
     * Define o valor da propriedade rvlSei.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRvlSei(JAXBElement<String> value) {
        this.rvlSei = value;
    }

    /**
     * Obtem o valor da propriedade salCre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSalCre() {
        return salCre;
    }

    /**
     * Define o valor da propriedade salCre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSalCre(JAXBElement<Double> value) {
        this.salCre = value;
    }

    /**
     * Obtem o valor da propriedade salDup.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSalDup() {
        return salDup;
    }

    /**
     * Define o valor da propriedade salDup.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSalDup(JAXBElement<Double> value) {
        this.salDup = value;
    }

    /**
     * Obtem o valor da propriedade salOut.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getSalOut() {
        return salOut;
    }

    /**
     * Define o valor da propriedade salOut.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setSalOut(JAXBElement<Double> value) {
        this.salOut = value;
    }

    /**
     * Obtem o valor da propriedade seqOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqOrm() {
        return seqOrm;
    }

    /**
     * Define o valor da propriedade seqOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqOrm(JAXBElement<Integer> value) {
        this.seqOrm = value;
    }

    /**
     * Obtem o valor da propriedade serCur.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerCur() {
        return serCur;
    }

    /**
     * Define o valor da propriedade serCur.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerCur(JAXBElement<String> value) {
        this.serCur = value;
    }

    /**
     * Obtem o valor da propriedade tolDsc.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getTolDsc() {
        return tolDsc;
    }

    /**
     * Define o valor da propriedade tolDsc.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setTolDsc(JAXBElement<Integer> value) {
        this.tolDsc = value;
    }

    /**
     * Obtem o valor da propriedade ultDup.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getUltDup() {
        return ultDup;
    }

    /**
     * Define o valor da propriedade ultDup.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setUltDup(JAXBElement<Double> value) {
        this.ultDup = value;
    }

    /**
     * Obtem o valor da propriedade vlrAtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrAtr() {
        return vlrAtr;
    }

    /**
     * Define o valor da propriedade vlrAtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrAtr(JAXBElement<Double> value) {
        this.vlrAtr = value;
    }

    /**
     * Obtem o valor da propriedade vlrMcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrMcp() {
        return vlrMcp;
    }

    /**
     * Define o valor da propriedade vlrMcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrMcp(JAXBElement<Double> value) {
        this.vlrMcp = value;
    }

    /**
     * Obtem o valor da propriedade vlrUcp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrUcp() {
        return vlrUcp;
    }

    /**
     * Define o valor da propriedade vlrUcp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrUcp(JAXBElement<Double> value) {
        this.vlrUcp = value;
    }

    /**
     * Obtem o valor da propriedade vlrUpe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrUpe() {
        return vlrUpe;
    }

    /**
     * Define o valor da propriedade vlrUpe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrUpe(JAXBElement<Double> value) {
        this.vlrUpe = value;
    }

    /**
     * Obtem o valor da propriedade vlrUpg.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getVlrUpg() {
        return vlrUpg;
    }

    /**
     * Define o valor da propriedade vlrUpg.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setVlrUpg(JAXBElement<Double> value) {
        this.vlrUpg = value;
    }

}
