package br.com.senior.Utils;

import java.net.URL;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import br.com.senior.poc.RN.ConfigRN;
import br.com.senior.poc.model.Config;

public class utils {
	public static JAXBElement<Integer> getIntJAX(Integer in, String NAME) {

		JAXBElement<Integer> inteiro = new JAXBElement<Integer>(new QName(NAME), Integer.class, new Integer(in));

		return inteiro;

	}

	public static JAXBElement<String> getSTRJAX(String in, String NAME) {
		JAXBElement<String> texto = new JAXBElement<String>(new QName(NAME), String.class, new String(in));
		return texto;
	}
	public static String RetornaDDDTelefone(String telefone){
		if (telefone.startsWith("+")){
			telefone = telefone.substring(3);
		}
		if (telefone.startsWith("0")){
			telefone = telefone.substring(1);
		}
		telefone = telefone.replaceAll("[^0-9]", "");  
		if (telefone.length() < 10){
			return "00";
		}
		return telefone.substring(0, 2);
	}

	public static String RetornaTelefone(String telefone) {
		if (telefone.startsWith("+")){
			telefone = telefone.substring(3);
		}
		if (telefone.startsWith("0")){
			telefone = telefone.substring(1);
		}
		telefone = telefone.replaceAll("[^0-9]", "");  
		if (telefone.length() < 10){
			return "00";
		}
		return telefone.substring(2);
	}

	public static String buscaUrl() {
		ConfigRN crn = new ConfigRN();
		Config config = crn.buscarPorNome("url_erp");
		if (config == null){
			config = new Config();
			config.setNome("url_erp");
			config.setValor("http://localhost:8080");
		}
		return null;
	}

}
