
package br.com.senior.services.Fornecedor;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresImportarInFornecedor complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresImportarInFornecedor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apeFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="baiFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cgcCpf" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="cidFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCli" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codEmp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFil" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codMot" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codPai" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRoe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codSro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSuf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cplEnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cxaPst" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="datAtu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datCad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="definicao" type="{http://services.senior.com.br}fornecedoresImportarInFornecedorDefinicao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="emaNfe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFo3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="forRep" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="forTra" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="horAtu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horCad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ideExt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="indFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insEst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insMun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intNet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limRet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nenFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notFor" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="notSis" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="numIdf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numRge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsMot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="perCod" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIcm" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perPid" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perRin" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perRir" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="recCof" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recIcm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recIpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recPis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="regEst" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="retCof" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retCsl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retIrf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retOur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retPis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retPro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqRoe" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sigUfs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sitFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipMer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIcm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIss" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="usuAtu" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="usuCad" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="zipCod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresImportarInFornecedor", propOrder = {
    "apeFor",
    "baiFor",
    "cepFor",
    "cgcCpf",
    "cidFor",
    "cliFor",
    "codCli",
    "codEmp",
    "codFil",
    "codFor",
    "codMot",
    "codPai",
    "codRam",
    "codRoe",
    "codRtr",
    "codSro",
    "codSuf",
    "codTri",
    "cplEnd",
    "cxaPst",
    "datAtu",
    "datCad",
    "definicao",
    "emaNfe",
    "endFor",
    "faxFor",
    "fonFo2",
    "fonFo3",
    "fonFor",
    "forRep",
    "forTra",
    "horAtu",
    "horCad",
    "ideExt",
    "indFor",
    "insEst",
    "insMun",
    "intNet",
    "limRet",
    "nenFor",
    "nomFor",
    "notFor",
    "notSis",
    "numIdf",
    "numRge",
    "obsMot",
    "perCod",
    "perIcm",
    "perPid",
    "perRin",
    "perRir",
    "recCof",
    "recIcm",
    "recIpi",
    "recPis",
    "regEst",
    "retCof",
    "retCsl",
    "retIrf",
    "retOur",
    "retPis",
    "retPro",
    "seqRoe",
    "sigUfs",
    "sitFor",
    "tipFor",
    "tipMer",
    "triIcm",
    "triIpi",
    "triIss",
    "usuAtu",
    "usuCad",
    "zipCod"
})
public class FornecedoresImportarInFornecedor {

    @XmlElementRef(name = "apeFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> apeFor;
    @XmlElementRef(name = "baiFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baiFor;
    @XmlElementRef(name = "cepFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cepFor;
    @XmlElementRef(name = "cgcCpf", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> cgcCpf;
    @XmlElementRef(name = "cidFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cidFor;
    @XmlElementRef(name = "cliFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cliFor;
    @XmlElementRef(name = "codCli", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codCli;
    @XmlElementRef(name = "codEmp", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codEmp;
    @XmlElementRef(name = "codFil", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFil;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "codMot", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codMot;
    @XmlElementRef(name = "codPai", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPai;
    @XmlElementRef(name = "codRam", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRam;
    @XmlElementRef(name = "codRoe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRoe;
    @XmlElementRef(name = "codRtr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codRtr;
    @XmlElementRef(name = "codSro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSro;
    @XmlElementRef(name = "codSuf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSuf;
    @XmlElementRef(name = "codTri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTri;
    @XmlElementRef(name = "cplEnd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cplEnd;
    @XmlElementRef(name = "cxaPst", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cxaPst;
    @XmlElementRef(name = "datAtu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datAtu;
    @XmlElementRef(name = "datCad", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datCad;
    @XmlElement(nillable = true)
    protected List<FornecedoresImportarInFornecedorDefinicao> definicao;
    @XmlElementRef(name = "emaNfe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emaNfe;
    @XmlElementRef(name = "endFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> endFor;
    @XmlElementRef(name = "faxFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> faxFor;
    @XmlElementRef(name = "fonFo2", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFo2;
    @XmlElementRef(name = "fonFo3", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFo3;
    @XmlElementRef(name = "fonFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFor;
    @XmlElementRef(name = "forRep", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> forRep;
    @XmlElementRef(name = "forTra", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> forTra;
    @XmlElementRef(name = "horAtu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> horAtu;
    @XmlElementRef(name = "horCad", type = JAXBElement.class, required = false)
    protected JAXBElement<String> horCad;
    @XmlElementRef(name = "ideExt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> ideExt;
    @XmlElementRef(name = "indFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indFor;
    @XmlElementRef(name = "insEst", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insEst;
    @XmlElementRef(name = "insMun", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insMun;
    @XmlElementRef(name = "intNet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> intNet;
    @XmlElementRef(name = "limRet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> limRet;
    @XmlElementRef(name = "nenFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nenFor;
    @XmlElementRef(name = "nomFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomFor;
    @XmlElementRef(name = "notFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> notFor;
    @XmlElementRef(name = "notSis", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> notSis;
    @XmlElementRef(name = "numIdf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numIdf;
    @XmlElementRef(name = "numRge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numRge;
    @XmlElementRef(name = "obsMot", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsMot;
    @XmlElementRef(name = "perCod", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perCod;
    @XmlElementRef(name = "perIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIcm;
    @XmlElementRef(name = "perPid", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perPid;
    @XmlElementRef(name = "perRin", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perRin;
    @XmlElementRef(name = "perRir", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perRir;
    @XmlElementRef(name = "recCof", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recCof;
    @XmlElementRef(name = "recIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recIcm;
    @XmlElementRef(name = "recIpi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recIpi;
    @XmlElementRef(name = "recPis", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recPis;
    @XmlElementRef(name = "regEst", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> regEst;
    @XmlElementRef(name = "retCof", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retCof;
    @XmlElementRef(name = "retCsl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retCsl;
    @XmlElementRef(name = "retIrf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retIrf;
    @XmlElementRef(name = "retOur", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retOur;
    @XmlElementRef(name = "retPis", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retPis;
    @XmlElementRef(name = "retPro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retPro;
    @XmlElementRef(name = "seqRoe", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqRoe;
    @XmlElementRef(name = "sigUfs", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sigUfs;
    @XmlElementRef(name = "sitFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sitFor;
    @XmlElementRef(name = "tipFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipFor;
    @XmlElementRef(name = "tipMer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipMer;
    @XmlElementRef(name = "triIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIcm;
    @XmlElementRef(name = "triIpi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIpi;
    @XmlElementRef(name = "triIss", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIss;
    @XmlElementRef(name = "usuAtu", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> usuAtu;
    @XmlElementRef(name = "usuCad", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> usuCad;
    @XmlElementRef(name = "zipCod", type = JAXBElement.class, required = false)
    protected JAXBElement<String> zipCod;

    /**
     * Obtem o valor da propriedade apeFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApeFor() {
        return apeFor;
    }

    /**
     * Define o valor da propriedade apeFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApeFor(JAXBElement<String> value) {
        this.apeFor = value;
    }

    /**
     * Obtem o valor da propriedade baiFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaiFor() {
        return baiFor;
    }

    /**
     * Define o valor da propriedade baiFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaiFor(JAXBElement<String> value) {
        this.baiFor = value;
    }

    /**
     * Obtem o valor da propriedade cepFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCepFor() {
        return cepFor;
    }

    /**
     * Define o valor da propriedade cepFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCepFor(JAXBElement<Integer> value) {
        this.cepFor = value;
    }

    /**
     * Obtem o valor da propriedade cgcCpf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCgcCpf() {
        return cgcCpf;
    }

    /**
     * Define o valor da propriedade cgcCpf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCgcCpf(JAXBElement<Double> value) {
        this.cgcCpf = value;
    }

    /**
     * Obtem o valor da propriedade cidFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCidFor() {
        return cidFor;
    }

    /**
     * Define o valor da propriedade cidFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCidFor(JAXBElement<String> value) {
        this.cidFor = value;
    }

    /**
     * Obtem o valor da propriedade cliFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCliFor() {
        return cliFor;
    }

    /**
     * Define o valor da propriedade cliFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCliFor(JAXBElement<String> value) {
        this.cliFor = value;
    }

    /**
     * Obtem o valor da propriedade codCli.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodCli() {
        return codCli;
    }

    /**
     * Define o valor da propriedade codCli.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodCli(JAXBElement<Integer> value) {
        this.codCli = value;
    }

    /**
     * Obtem o valor da propriedade codEmp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodEmp() {
        return codEmp;
    }

    /**
     * Define o valor da propriedade codEmp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodEmp(JAXBElement<Integer> value) {
        this.codEmp = value;
    }

    /**
     * Obtem o valor da propriedade codFil.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFil() {
        return codFil;
    }

    /**
     * Define o valor da propriedade codFil.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFil(JAXBElement<Integer> value) {
        this.codFil = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade codMot.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodMot() {
        return codMot;
    }

    /**
     * Define o valor da propriedade codMot.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodMot(JAXBElement<Integer> value) {
        this.codMot = value;
    }

    /**
     * Obtem o valor da propriedade codPai.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodPai() {
        return codPai;
    }

    /**
     * Define o valor da propriedade codPai.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodPai(JAXBElement<String> value) {
        this.codPai = value;
    }

    /**
     * Obtem o valor da propriedade codRam.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodRam() {
        return codRam;
    }

    /**
     * Define o valor da propriedade codRam.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodRam(JAXBElement<String> value) {
        this.codRam = value;
    }

    /**
     * Obtem o valor da propriedade codRoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodRoe() {
        return codRoe;
    }

    /**
     * Define o valor da propriedade codRoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodRoe(JAXBElement<String> value) {
        this.codRoe = value;
    }

    /**
     * Obtem o valor da propriedade codRtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodRtr() {
        return codRtr;
    }

    /**
     * Define o valor da propriedade codRtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodRtr(JAXBElement<Integer> value) {
        this.codRtr = value;
    }

    /**
     * Obtem o valor da propriedade codSro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSro() {
        return codSro;
    }

    /**
     * Define o valor da propriedade codSro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSro(JAXBElement<String> value) {
        this.codSro = value;
    }

    /**
     * Obtem o valor da propriedade codSuf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSuf() {
        return codSuf;
    }

    /**
     * Define o valor da propriedade codSuf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSuf(JAXBElement<String> value) {
        this.codSuf = value;
    }

    /**
     * Obtem o valor da propriedade codTri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTri() {
        return codTri;
    }

    /**
     * Define o valor da propriedade codTri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTri(JAXBElement<String> value) {
        this.codTri = value;
    }

    /**
     * Obtem o valor da propriedade cplEnd.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCplEnd() {
        return cplEnd;
    }

    /**
     * Define o valor da propriedade cplEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCplEnd(JAXBElement<String> value) {
        this.cplEnd = value;
    }

    /**
     * Obtem o valor da propriedade cxaPst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCxaPst() {
        return cxaPst;
    }

    /**
     * Define o valor da propriedade cxaPst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCxaPst(JAXBElement<Integer> value) {
        this.cxaPst = value;
    }

    /**
     * Obtem o valor da propriedade datAtu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatAtu() {
        return datAtu;
    }

    /**
     * Define o valor da propriedade datAtu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatAtu(JAXBElement<String> value) {
        this.datAtu = value;
    }

    /**
     * Obtem o valor da propriedade datCad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatCad() {
        return datCad;
    }

    /**
     * Define o valor da propriedade datCad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatCad(JAXBElement<String> value) {
        this.datCad = value;
    }

    /**
     * Gets the value of the definicao property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the definicao property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDefinicao().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresImportarInFornecedorDefinicao }
     * 
     * 
     */
    public List<FornecedoresImportarInFornecedorDefinicao> getDefinicao() {
        if (definicao == null) {
            definicao = new ArrayList<FornecedoresImportarInFornecedorDefinicao>();
        }
        return this.definicao;
    }

    /**
     * Obtem o valor da propriedade emaNfe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmaNfe() {
        return emaNfe;
    }

    /**
     * Define o valor da propriedade emaNfe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmaNfe(JAXBElement<String> value) {
        this.emaNfe = value;
    }

    /**
     * Obtem o valor da propriedade endFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEndFor() {
        return endFor;
    }

    /**
     * Define o valor da propriedade endFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEndFor(JAXBElement<String> value) {
        this.endFor = value;
    }

    /**
     * Obtem o valor da propriedade faxFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFaxFor() {
        return faxFor;
    }

    /**
     * Define o valor da propriedade faxFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFaxFor(JAXBElement<String> value) {
        this.faxFor = value;
    }

    /**
     * Obtem o valor da propriedade fonFo2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFo2() {
        return fonFo2;
    }

    /**
     * Define o valor da propriedade fonFo2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFo2(JAXBElement<String> value) {
        this.fonFo2 = value;
    }

    /**
     * Obtem o valor da propriedade fonFo3.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFo3() {
        return fonFo3;
    }

    /**
     * Define o valor da propriedade fonFo3.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFo3(JAXBElement<String> value) {
        this.fonFo3 = value;
    }

    /**
     * Obtem o valor da propriedade fonFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFor() {
        return fonFor;
    }

    /**
     * Define o valor da propriedade fonFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFor(JAXBElement<String> value) {
        this.fonFor = value;
    }

    /**
     * Obtem o valor da propriedade forRep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getForRep() {
        return forRep;
    }

    /**
     * Define o valor da propriedade forRep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setForRep(JAXBElement<Integer> value) {
        this.forRep = value;
    }

    /**
     * Obtem o valor da propriedade forTra.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getForTra() {
        return forTra;
    }

    /**
     * Define o valor da propriedade forTra.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setForTra(JAXBElement<Integer> value) {
        this.forTra = value;
    }

    /**
     * Obtem o valor da propriedade horAtu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHorAtu() {
        return horAtu;
    }

    /**
     * Define o valor da propriedade horAtu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHorAtu(JAXBElement<String> value) {
        this.horAtu = value;
    }

    /**
     * Obtem o valor da propriedade horCad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHorCad() {
        return horCad;
    }

    /**
     * Define o valor da propriedade horCad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHorCad(JAXBElement<String> value) {
        this.horCad = value;
    }

    /**
     * Obtem o valor da propriedade ideExt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIdeExt() {
        return ideExt;
    }

    /**
     * Define o valor da propriedade ideExt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIdeExt(JAXBElement<Integer> value) {
        this.ideExt = value;
    }

    /**
     * Obtem o valor da propriedade indFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndFor() {
        return indFor;
    }

    /**
     * Define o valor da propriedade indFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndFor(JAXBElement<String> value) {
        this.indFor = value;
    }

    /**
     * Obtem o valor da propriedade insEst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsEst() {
        return insEst;
    }

    /**
     * Define o valor da propriedade insEst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsEst(JAXBElement<String> value) {
        this.insEst = value;
    }

    /**
     * Obtem o valor da propriedade insMun.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsMun() {
        return insMun;
    }

    /**
     * Define o valor da propriedade insMun.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsMun(JAXBElement<String> value) {
        this.insMun = value;
    }

    /**
     * Obtem o valor da propriedade intNet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIntNet() {
        return intNet;
    }

    /**
     * Define o valor da propriedade intNet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIntNet(JAXBElement<String> value) {
        this.intNet = value;
    }

    /**
     * Obtem o valor da propriedade limRet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLimRet() {
        return limRet;
    }

    /**
     * Define o valor da propriedade limRet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLimRet(JAXBElement<String> value) {
        this.limRet = value;
    }

    /**
     * Obtem o valor da propriedade nenFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNenFor() {
        return nenFor;
    }

    /**
     * Define o valor da propriedade nenFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNenFor(JAXBElement<String> value) {
        this.nenFor = value;
    }

    /**
     * Obtem o valor da propriedade nomFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomFor() {
        return nomFor;
    }

    /**
     * Define o valor da propriedade nomFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomFor(JAXBElement<String> value) {
        this.nomFor = value;
    }

    /**
     * Obtem o valor da propriedade notFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getNotFor() {
        return notFor;
    }

    /**
     * Define o valor da propriedade notFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setNotFor(JAXBElement<Double> value) {
        this.notFor = value;
    }

    /**
     * Obtem o valor da propriedade notSis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getNotSis() {
        return notSis;
    }

    /**
     * Define o valor da propriedade notSis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setNotSis(JAXBElement<Double> value) {
        this.notSis = value;
    }

    /**
     * Obtem o valor da propriedade numIdf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumIdf() {
        return numIdf;
    }

    /**
     * Define o valor da propriedade numIdf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumIdf(JAXBElement<String> value) {
        this.numIdf = value;
    }

    /**
     * Obtem o valor da propriedade numRge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumRge() {
        return numRge;
    }

    /**
     * Define o valor da propriedade numRge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumRge(JAXBElement<String> value) {
        this.numRge = value;
    }

    /**
     * Obtem o valor da propriedade obsMot.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsMot() {
        return obsMot;
    }

    /**
     * Define o valor da propriedade obsMot.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsMot(JAXBElement<String> value) {
        this.obsMot = value;
    }

    /**
     * Obtem o valor da propriedade perCod.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerCod() {
        return perCod;
    }

    /**
     * Define o valor da propriedade perCod.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerCod(JAXBElement<Double> value) {
        this.perCod = value;
    }

    /**
     * Obtem o valor da propriedade perIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIcm() {
        return perIcm;
    }

    /**
     * Define o valor da propriedade perIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIcm(JAXBElement<Double> value) {
        this.perIcm = value;
    }

    /**
     * Obtem o valor da propriedade perPid.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerPid() {
        return perPid;
    }

    /**
     * Define o valor da propriedade perPid.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerPid(JAXBElement<Double> value) {
        this.perPid = value;
    }

    /**
     * Obtem o valor da propriedade perRin.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerRin() {
        return perRin;
    }

    /**
     * Define o valor da propriedade perRin.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerRin(JAXBElement<Double> value) {
        this.perRin = value;
    }

    /**
     * Obtem o valor da propriedade perRir.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerRir() {
        return perRir;
    }

    /**
     * Define o valor da propriedade perRir.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerRir(JAXBElement<Double> value) {
        this.perRir = value;
    }

    /**
     * Obtem o valor da propriedade recCof.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecCof() {
        return recCof;
    }

    /**
     * Define o valor da propriedade recCof.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecCof(JAXBElement<String> value) {
        this.recCof = value;
    }

    /**
     * Obtem o valor da propriedade recIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecIcm() {
        return recIcm;
    }

    /**
     * Define o valor da propriedade recIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecIcm(JAXBElement<String> value) {
        this.recIcm = value;
    }

    /**
     * Obtem o valor da propriedade recIpi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecIpi() {
        return recIpi;
    }

    /**
     * Define o valor da propriedade recIpi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecIpi(JAXBElement<String> value) {
        this.recIpi = value;
    }

    /**
     * Obtem o valor da propriedade recPis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecPis() {
        return recPis;
    }

    /**
     * Define o valor da propriedade recPis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecPis(JAXBElement<String> value) {
        this.recPis = value;
    }

    /**
     * Obtem o valor da propriedade regEst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRegEst() {
        return regEst;
    }

    /**
     * Define o valor da propriedade regEst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRegEst(JAXBElement<Integer> value) {
        this.regEst = value;
    }

    /**
     * Obtem o valor da propriedade retCof.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetCof() {
        return retCof;
    }

    /**
     * Define o valor da propriedade retCof.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetCof(JAXBElement<String> value) {
        this.retCof = value;
    }

    /**
     * Obtem o valor da propriedade retCsl.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetCsl() {
        return retCsl;
    }

    /**
     * Define o valor da propriedade retCsl.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetCsl(JAXBElement<String> value) {
        this.retCsl = value;
    }

    /**
     * Obtem o valor da propriedade retIrf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetIrf() {
        return retIrf;
    }

    /**
     * Define o valor da propriedade retIrf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetIrf(JAXBElement<String> value) {
        this.retIrf = value;
    }

    /**
     * Obtem o valor da propriedade retOur.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetOur() {
        return retOur;
    }

    /**
     * Define o valor da propriedade retOur.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetOur(JAXBElement<String> value) {
        this.retOur = value;
    }

    /**
     * Obtem o valor da propriedade retPis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetPis() {
        return retPis;
    }

    /**
     * Define o valor da propriedade retPis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetPis(JAXBElement<String> value) {
        this.retPis = value;
    }

    /**
     * Obtem o valor da propriedade retPro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetPro() {
        return retPro;
    }

    /**
     * Define o valor da propriedade retPro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetPro(JAXBElement<String> value) {
        this.retPro = value;
    }

    /**
     * Obtem o valor da propriedade seqRoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqRoe() {
        return seqRoe;
    }

    /**
     * Define o valor da propriedade seqRoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqRoe(JAXBElement<Integer> value) {
        this.seqRoe = value;
    }

    /**
     * Obtem o valor da propriedade sigUfs.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSigUfs() {
        return sigUfs;
    }

    /**
     * Define o valor da propriedade sigUfs.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSigUfs(JAXBElement<String> value) {
        this.sigUfs = value;
    }

    /**
     * Obtem o valor da propriedade sitFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSitFor() {
        return sitFor;
    }

    /**
     * Define o valor da propriedade sitFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSitFor(JAXBElement<String> value) {
        this.sitFor = value;
    }

    /**
     * Obtem o valor da propriedade tipFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipFor() {
        return tipFor;
    }

    /**
     * Define o valor da propriedade tipFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipFor(JAXBElement<String> value) {
        this.tipFor = value;
    }

    /**
     * Obtem o valor da propriedade tipMer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipMer() {
        return tipMer;
    }

    /**
     * Define o valor da propriedade tipMer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipMer(JAXBElement<String> value) {
        this.tipMer = value;
    }

    /**
     * Obtem o valor da propriedade triIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIcm() {
        return triIcm;
    }

    /**
     * Define o valor da propriedade triIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIcm(JAXBElement<String> value) {
        this.triIcm = value;
    }

    /**
     * Obtem o valor da propriedade triIpi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIpi() {
        return triIpi;
    }

    /**
     * Define o valor da propriedade triIpi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIpi(JAXBElement<String> value) {
        this.triIpi = value;
    }

    /**
     * Obtem o valor da propriedade triIss.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIss() {
        return triIss;
    }

    /**
     * Define o valor da propriedade triIss.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIss(JAXBElement<String> value) {
        this.triIss = value;
    }

    /**
     * Obtem o valor da propriedade usuAtu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getUsuAtu() {
        return usuAtu;
    }

    /**
     * Define o valor da propriedade usuAtu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setUsuAtu(JAXBElement<Double> value) {
        this.usuAtu = value;
    }

    /**
     * Obtem o valor da propriedade usuCad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getUsuCad() {
        return usuCad;
    }

    /**
     * Define o valor da propriedade usuCad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setUsuCad(JAXBElement<Double> value) {
        this.usuCad = value;
    }

    /**
     * Obtem o valor da propriedade zipCod.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getZipCod() {
        return zipCod;
    }

    /**
     * Define o valor da propriedade zipCod.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setZipCod(JAXBElement<String> value) {
        this.zipCod = value;
    }

}
