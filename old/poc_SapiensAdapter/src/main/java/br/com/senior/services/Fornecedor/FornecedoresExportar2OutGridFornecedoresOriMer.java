
package br.com.senior.services.Fornecedor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedoresOriMer complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedoresOriMer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="baiOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cepOrm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cgcOrm" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="cidOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cplOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eenOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniOrm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="insOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prxOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seqOrm" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sitOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedoresOriMer", propOrder = {
    "baiOrm",
    "cepOrm",
    "cgcOrm",
    "cidOrm",
    "codFor",
    "cplOrm",
    "eenOrm",
    "endOrm",
    "estOrm",
    "iniOrm",
    "insOrm",
    "numOrm",
    "prxOrm",
    "seqOrm",
    "sitOrm"
})
public class FornecedoresExportar2OutGridFornecedoresOriMer {

    @XmlElementRef(name = "baiOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baiOrm;
    @XmlElementRef(name = "cepOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cepOrm;
    @XmlElementRef(name = "cgcOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> cgcOrm;
    @XmlElementRef(name = "cidOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cidOrm;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "cplOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cplOrm;
    @XmlElementRef(name = "eenOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eenOrm;
    @XmlElementRef(name = "endOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> endOrm;
    @XmlElementRef(name = "estOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> estOrm;
    @XmlElementRef(name = "iniOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> iniOrm;
    @XmlElementRef(name = "insOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insOrm;
    @XmlElementRef(name = "numOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numOrm;
    @XmlElementRef(name = "prxOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prxOrm;
    @XmlElementRef(name = "seqOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqOrm;
    @XmlElementRef(name = "sitOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sitOrm;

    /**
     * Obtem o valor da propriedade baiOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaiOrm() {
        return baiOrm;
    }

    /**
     * Define o valor da propriedade baiOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaiOrm(JAXBElement<String> value) {
        this.baiOrm = value;
    }

    /**
     * Obtem o valor da propriedade cepOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCepOrm() {
        return cepOrm;
    }

    /**
     * Define o valor da propriedade cepOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCepOrm(JAXBElement<Integer> value) {
        this.cepOrm = value;
    }

    /**
     * Obtem o valor da propriedade cgcOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getCgcOrm() {
        return cgcOrm;
    }

    /**
     * Define o valor da propriedade cgcOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setCgcOrm(JAXBElement<Double> value) {
        this.cgcOrm = value;
    }

    /**
     * Obtem o valor da propriedade cidOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCidOrm() {
        return cidOrm;
    }

    /**
     * Define o valor da propriedade cidOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCidOrm(JAXBElement<String> value) {
        this.cidOrm = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade cplOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCplOrm() {
        return cplOrm;
    }

    /**
     * Define o valor da propriedade cplOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCplOrm(JAXBElement<String> value) {
        this.cplOrm = value;
    }

    /**
     * Obtem o valor da propriedade eenOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEenOrm() {
        return eenOrm;
    }

    /**
     * Define o valor da propriedade eenOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEenOrm(JAXBElement<String> value) {
        this.eenOrm = value;
    }

    /**
     * Obtem o valor da propriedade endOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEndOrm() {
        return endOrm;
    }

    /**
     * Define o valor da propriedade endOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEndOrm(JAXBElement<String> value) {
        this.endOrm = value;
    }

    /**
     * Obtem o valor da propriedade estOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEstOrm() {
        return estOrm;
    }

    /**
     * Define o valor da propriedade estOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEstOrm(JAXBElement<String> value) {
        this.estOrm = value;
    }

    /**
     * Obtem o valor da propriedade iniOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getIniOrm() {
        return iniOrm;
    }

    /**
     * Define o valor da propriedade iniOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setIniOrm(JAXBElement<Integer> value) {
        this.iniOrm = value;
    }

    /**
     * Obtem o valor da propriedade insOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsOrm() {
        return insOrm;
    }

    /**
     * Define o valor da propriedade insOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsOrm(JAXBElement<String> value) {
        this.insOrm = value;
    }

    /**
     * Obtem o valor da propriedade numOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumOrm() {
        return numOrm;
    }

    /**
     * Define o valor da propriedade numOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumOrm(JAXBElement<String> value) {
        this.numOrm = value;
    }

    /**
     * Obtem o valor da propriedade prxOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPrxOrm() {
        return prxOrm;
    }

    /**
     * Define o valor da propriedade prxOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPrxOrm(JAXBElement<String> value) {
        this.prxOrm = value;
    }

    /**
     * Obtem o valor da propriedade seqOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqOrm() {
        return seqOrm;
    }

    /**
     * Define o valor da propriedade seqOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqOrm(JAXBElement<Integer> value) {
        this.seqOrm = value;
    }

    /**
     * Obtem o valor da propriedade sitOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSitOrm() {
        return sitOrm;
    }

    /**
     * Define o valor da propriedade sitOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSitOrm(JAXBElement<String> value) {
        this.sitOrm = value;
    }

}
