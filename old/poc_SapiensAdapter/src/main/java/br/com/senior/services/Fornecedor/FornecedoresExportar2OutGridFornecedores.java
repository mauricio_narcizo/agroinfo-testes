
package br.com.senior.services.Fornecedor;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de fornecedoresExportar2OutGridFornecedores complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteedo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="fornecedoresExportar2OutGridFornecedores">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apeFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CNAE" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresCNAE" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="campoUsuario" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresCampoUsuario" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="caracteristicas" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresCaracteristicas" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cep" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresCep" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cgcCpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCli" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codFor" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codGre" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codMot" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codRam" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRoe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRtr" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codSro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSuf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conBan" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresConBan" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contatos" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresContatos" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cxaPst" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="datAtu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datCad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="defHis" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresDefHis" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="emaNfe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxVen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFo3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonVen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="forPag" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresForPag" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="forRep" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="forTra" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="forWms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="gerDir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horAtu" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horCad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSS" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresINSS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="indCoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insAnp" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="insEst" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insMun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intNet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="limRet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomVen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notAfo" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="notFor" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="notSis" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="numRge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obsMot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observacoes" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresObservacoes" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="oriMer" type="{http://services.senior.com.br}fornecedoresExportar2OutGridFornecedoresOriMer" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="perCod" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perIcm" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perPid" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perRin" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="perRir" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="qtdDep" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="recCof" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recIcm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recIpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recPis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="regEst" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="retCof" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retCsl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retIrf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retOur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retPis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retPro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rmlVen" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seqInt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="seqRoe" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sitFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="temOrm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipFav" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipFor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipMer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipPgt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIcm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIpi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="triIss" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fornecedoresExportar2OutGridFornecedores", propOrder = {
    "apeFor",
    "cnae",
    "campoUsuario",
    "caracteristicas",
    "cep",
    "cgcCpf",
    "cliFor",
    "codCli",
    "codFor",
    "codGre",
    "codMot",
    "codRam",
    "codRoe",
    "codRtr",
    "codSro",
    "codSuf",
    "codTri",
    "conBan",
    "contatos",
    "cxaPst",
    "datAtu",
    "datCad",
    "defHis",
    "emaNfe",
    "faxFor",
    "faxVen",
    "fonFo2",
    "fonFo3",
    "fonFor",
    "fonVen",
    "forPag",
    "forRep",
    "forTra",
    "forWms",
    "gerDir",
    "horAtu",
    "horCad",
    "inss",
    "indCoo",
    "indFor",
    "insAnp",
    "insEst",
    "insMun",
    "intNet",
    "limRet",
    "marFor",
    "nomFor",
    "nomVen",
    "notAfo",
    "notFor",
    "notSis",
    "numRge",
    "obsMot",
    "observacoes",
    "oriMer",
    "perCod",
    "perIcm",
    "perPid",
    "perRin",
    "perRir",
    "qtdDep",
    "recCof",
    "recIcm",
    "recIpi",
    "recPis",
    "regEst",
    "retCof",
    "retCsl",
    "retIrf",
    "retOur",
    "retPis",
    "retPro",
    "rmlVen",
    "seqInt",
    "seqRoe",
    "sitFor",
    "temOrm",
    "tipFav",
    "tipFor",
    "tipMer",
    "tipPgt",
    "triIcm",
    "triIpi",
    "triIss"
})
public class FornecedoresExportar2OutGridFornecedores {

    @XmlElementRef(name = "apeFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> apeFor;
    @XmlElement(name = "CNAE", nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresCNAE> cnae;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresCampoUsuario> campoUsuario;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresCaracteristicas> caracteristicas;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresCep> cep;
    @XmlElementRef(name = "cgcCpf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cgcCpf;
    @XmlElementRef(name = "cliFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cliFor;
    @XmlElementRef(name = "codCli", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codCli;
    @XmlElementRef(name = "codFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codFor;
    @XmlElementRef(name = "codGre", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codGre;
    @XmlElementRef(name = "codMot", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codMot;
    @XmlElementRef(name = "codRam", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRam;
    @XmlElementRef(name = "codRoe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRoe;
    @XmlElementRef(name = "codRtr", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> codRtr;
    @XmlElementRef(name = "codSro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSro;
    @XmlElementRef(name = "codSuf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codSuf;
    @XmlElementRef(name = "codTri", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codTri;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresConBan> conBan;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresContatos> contatos;
    @XmlElementRef(name = "cxaPst", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> cxaPst;
    @XmlElementRef(name = "datAtu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datAtu;
    @XmlElementRef(name = "datCad", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datCad;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresDefHis> defHis;
    @XmlElementRef(name = "emaNfe", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emaNfe;
    @XmlElementRef(name = "faxFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> faxFor;
    @XmlElementRef(name = "faxVen", type = JAXBElement.class, required = false)
    protected JAXBElement<String> faxVen;
    @XmlElementRef(name = "fonFo2", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFo2;
    @XmlElementRef(name = "fonFo3", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFo3;
    @XmlElementRef(name = "fonFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonFor;
    @XmlElementRef(name = "fonVen", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fonVen;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresForPag> forPag;
    @XmlElementRef(name = "forRep", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> forRep;
    @XmlElementRef(name = "forTra", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> forTra;
    @XmlElementRef(name = "forWms", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forWms;
    @XmlElementRef(name = "gerDir", type = JAXBElement.class, required = false)
    protected JAXBElement<String> gerDir;
    @XmlElementRef(name = "horAtu", type = JAXBElement.class, required = false)
    protected JAXBElement<String> horAtu;
    @XmlElementRef(name = "horCad", type = JAXBElement.class, required = false)
    protected JAXBElement<String> horCad;
    @XmlElement(name = "INSS", nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresINSS> inss;
    @XmlElementRef(name = "indCoo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indCoo;
    @XmlElementRef(name = "indFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> indFor;
    @XmlElementRef(name = "insAnp", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> insAnp;
    @XmlElementRef(name = "insEst", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insEst;
    @XmlElementRef(name = "insMun", type = JAXBElement.class, required = false)
    protected JAXBElement<String> insMun;
    @XmlElementRef(name = "intNet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> intNet;
    @XmlElementRef(name = "limRet", type = JAXBElement.class, required = false)
    protected JAXBElement<String> limRet;
    @XmlElementRef(name = "marFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> marFor;
    @XmlElementRef(name = "nomFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomFor;
    @XmlElementRef(name = "nomVen", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nomVen;
    @XmlElementRef(name = "notAfo", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> notAfo;
    @XmlElementRef(name = "notFor", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> notFor;
    @XmlElementRef(name = "notSis", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> notSis;
    @XmlElementRef(name = "numRge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numRge;
    @XmlElementRef(name = "obsMot", type = JAXBElement.class, required = false)
    protected JAXBElement<String> obsMot;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresObservacoes> observacoes;
    @XmlElement(nillable = true)
    protected List<FornecedoresExportar2OutGridFornecedoresOriMer> oriMer;
    @XmlElementRef(name = "perCod", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perCod;
    @XmlElementRef(name = "perIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perIcm;
    @XmlElementRef(name = "perPid", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perPid;
    @XmlElementRef(name = "perRin", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perRin;
    @XmlElementRef(name = "perRir", type = JAXBElement.class, required = false)
    protected JAXBElement<Double> perRir;
    @XmlElementRef(name = "qtdDep", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> qtdDep;
    @XmlElementRef(name = "recCof", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recCof;
    @XmlElementRef(name = "recIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recIcm;
    @XmlElementRef(name = "recIpi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recIpi;
    @XmlElementRef(name = "recPis", type = JAXBElement.class, required = false)
    protected JAXBElement<String> recPis;
    @XmlElementRef(name = "regEst", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> regEst;
    @XmlElementRef(name = "retCof", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retCof;
    @XmlElementRef(name = "retCsl", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retCsl;
    @XmlElementRef(name = "retIrf", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retIrf;
    @XmlElementRef(name = "retOur", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retOur;
    @XmlElementRef(name = "retPis", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retPis;
    @XmlElementRef(name = "retPro", type = JAXBElement.class, required = false)
    protected JAXBElement<String> retPro;
    @XmlElementRef(name = "rmlVen", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> rmlVen;
    @XmlElementRef(name = "seqInt", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqInt;
    @XmlElementRef(name = "seqRoe", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> seqRoe;
    @XmlElementRef(name = "sitFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sitFor;
    @XmlElementRef(name = "temOrm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> temOrm;
    @XmlElementRef(name = "tipFav", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipFav;
    @XmlElementRef(name = "tipFor", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipFor;
    @XmlElementRef(name = "tipMer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipMer;
    @XmlElementRef(name = "tipPgt", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tipPgt;
    @XmlElementRef(name = "triIcm", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIcm;
    @XmlElementRef(name = "triIpi", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIpi;
    @XmlElementRef(name = "triIss", type = JAXBElement.class, required = false)
    protected JAXBElement<String> triIss;

    /**
     * Obtem o valor da propriedade apeFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApeFor() {
        return apeFor;
    }

    /**
     * Define o valor da propriedade apeFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApeFor(JAXBElement<String> value) {
        this.apeFor = value;
    }

    /**
     * Gets the value of the cnae property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cnae property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCNAE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresCNAE }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresCNAE> getCNAE() {
        if (cnae == null) {
            cnae = new ArrayList<FornecedoresExportar2OutGridFornecedoresCNAE>();
        }
        return this.cnae;
    }

    /**
     * Gets the value of the campoUsuario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the campoUsuario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCampoUsuario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresCampoUsuario }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresCampoUsuario> getCampoUsuario() {
        if (campoUsuario == null) {
            campoUsuario = new ArrayList<FornecedoresExportar2OutGridFornecedoresCampoUsuario>();
        }
        return this.campoUsuario;
    }

    /**
     * Gets the value of the caracteristicas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caracteristicas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaracteristicas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresCaracteristicas }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresCaracteristicas> getCaracteristicas() {
        if (caracteristicas == null) {
            caracteristicas = new ArrayList<FornecedoresExportar2OutGridFornecedoresCaracteristicas>();
        }
        return this.caracteristicas;
    }

    /**
     * Gets the value of the cep property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cep property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCep().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresCep }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresCep> getCep() {
        if (cep == null) {
            cep = new ArrayList<FornecedoresExportar2OutGridFornecedoresCep>();
        }
        return this.cep;
    }

    /**
     * Obtem o valor da propriedade cgcCpf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCgcCpf() {
        return cgcCpf;
    }

    /**
     * Define o valor da propriedade cgcCpf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCgcCpf(JAXBElement<String> value) {
        this.cgcCpf = value;
    }

    /**
     * Obtem o valor da propriedade cliFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCliFor() {
        return cliFor;
    }

    /**
     * Define o valor da propriedade cliFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCliFor(JAXBElement<String> value) {
        this.cliFor = value;
    }

    /**
     * Obtem o valor da propriedade codCli.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodCli() {
        return codCli;
    }

    /**
     * Define o valor da propriedade codCli.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodCli(JAXBElement<Integer> value) {
        this.codCli = value;
    }

    /**
     * Obtem o valor da propriedade codFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodFor() {
        return codFor;
    }

    /**
     * Define o valor da propriedade codFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodFor(JAXBElement<Integer> value) {
        this.codFor = value;
    }

    /**
     * Obtem o valor da propriedade codGre.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodGre() {
        return codGre;
    }

    /**
     * Define o valor da propriedade codGre.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodGre(JAXBElement<Integer> value) {
        this.codGre = value;
    }

    /**
     * Obtem o valor da propriedade codMot.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodMot() {
        return codMot;
    }

    /**
     * Define o valor da propriedade codMot.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodMot(JAXBElement<Integer> value) {
        this.codMot = value;
    }

    /**
     * Obtem o valor da propriedade codRam.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodRam() {
        return codRam;
    }

    /**
     * Define o valor da propriedade codRam.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodRam(JAXBElement<String> value) {
        this.codRam = value;
    }

    /**
     * Obtem o valor da propriedade codRoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodRoe() {
        return codRoe;
    }

    /**
     * Define o valor da propriedade codRoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodRoe(JAXBElement<String> value) {
        this.codRoe = value;
    }

    /**
     * Obtem o valor da propriedade codRtr.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCodRtr() {
        return codRtr;
    }

    /**
     * Define o valor da propriedade codRtr.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCodRtr(JAXBElement<Integer> value) {
        this.codRtr = value;
    }

    /**
     * Obtem o valor da propriedade codSro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSro() {
        return codSro;
    }

    /**
     * Define o valor da propriedade codSro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSro(JAXBElement<String> value) {
        this.codSro = value;
    }

    /**
     * Obtem o valor da propriedade codSuf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodSuf() {
        return codSuf;
    }

    /**
     * Define o valor da propriedade codSuf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodSuf(JAXBElement<String> value) {
        this.codSuf = value;
    }

    /**
     * Obtem o valor da propriedade codTri.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodTri() {
        return codTri;
    }

    /**
     * Define o valor da propriedade codTri.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodTri(JAXBElement<String> value) {
        this.codTri = value;
    }

    /**
     * Gets the value of the conBan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conBan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConBan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresConBan }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresConBan> getConBan() {
        if (conBan == null) {
            conBan = new ArrayList<FornecedoresExportar2OutGridFornecedoresConBan>();
        }
        return this.conBan;
    }

    /**
     * Gets the value of the contatos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contatos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContatos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresContatos }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresContatos> getContatos() {
        if (contatos == null) {
            contatos = new ArrayList<FornecedoresExportar2OutGridFornecedoresContatos>();
        }
        return this.contatos;
    }

    /**
     * Obtem o valor da propriedade cxaPst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCxaPst() {
        return cxaPst;
    }

    /**
     * Define o valor da propriedade cxaPst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCxaPst(JAXBElement<Integer> value) {
        this.cxaPst = value;
    }

    /**
     * Obtem o valor da propriedade datAtu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatAtu() {
        return datAtu;
    }

    /**
     * Define o valor da propriedade datAtu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatAtu(JAXBElement<String> value) {
        this.datAtu = value;
    }

    /**
     * Obtem o valor da propriedade datCad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatCad() {
        return datCad;
    }

    /**
     * Define o valor da propriedade datCad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatCad(JAXBElement<String> value) {
        this.datCad = value;
    }

    /**
     * Gets the value of the defHis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the defHis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDefHis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresDefHis }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresDefHis> getDefHis() {
        if (defHis == null) {
            defHis = new ArrayList<FornecedoresExportar2OutGridFornecedoresDefHis>();
        }
        return this.defHis;
    }

    /**
     * Obtem o valor da propriedade emaNfe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmaNfe() {
        return emaNfe;
    }

    /**
     * Define o valor da propriedade emaNfe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmaNfe(JAXBElement<String> value) {
        this.emaNfe = value;
    }

    /**
     * Obtem o valor da propriedade faxFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFaxFor() {
        return faxFor;
    }

    /**
     * Define o valor da propriedade faxFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFaxFor(JAXBElement<String> value) {
        this.faxFor = value;
    }

    /**
     * Obtem o valor da propriedade faxVen.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFaxVen() {
        return faxVen;
    }

    /**
     * Define o valor da propriedade faxVen.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFaxVen(JAXBElement<String> value) {
        this.faxVen = value;
    }

    /**
     * Obtem o valor da propriedade fonFo2.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFo2() {
        return fonFo2;
    }

    /**
     * Define o valor da propriedade fonFo2.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFo2(JAXBElement<String> value) {
        this.fonFo2 = value;
    }

    /**
     * Obtem o valor da propriedade fonFo3.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFo3() {
        return fonFo3;
    }

    /**
     * Define o valor da propriedade fonFo3.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFo3(JAXBElement<String> value) {
        this.fonFo3 = value;
    }

    /**
     * Obtem o valor da propriedade fonFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonFor() {
        return fonFor;
    }

    /**
     * Define o valor da propriedade fonFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonFor(JAXBElement<String> value) {
        this.fonFor = value;
    }

    /**
     * Obtem o valor da propriedade fonVen.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFonVen() {
        return fonVen;
    }

    /**
     * Define o valor da propriedade fonVen.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFonVen(JAXBElement<String> value) {
        this.fonVen = value;
    }

    /**
     * Gets the value of the forPag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forPag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForPag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresForPag }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresForPag> getForPag() {
        if (forPag == null) {
            forPag = new ArrayList<FornecedoresExportar2OutGridFornecedoresForPag>();
        }
        return this.forPag;
    }

    /**
     * Obtem o valor da propriedade forRep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getForRep() {
        return forRep;
    }

    /**
     * Define o valor da propriedade forRep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setForRep(JAXBElement<Integer> value) {
        this.forRep = value;
    }

    /**
     * Obtem o valor da propriedade forTra.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getForTra() {
        return forTra;
    }

    /**
     * Define o valor da propriedade forTra.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setForTra(JAXBElement<Integer> value) {
        this.forTra = value;
    }

    /**
     * Obtem o valor da propriedade forWms.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForWms() {
        return forWms;
    }

    /**
     * Define o valor da propriedade forWms.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForWms(JAXBElement<String> value) {
        this.forWms = value;
    }

    /**
     * Obtem o valor da propriedade gerDir.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGerDir() {
        return gerDir;
    }

    /**
     * Define o valor da propriedade gerDir.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGerDir(JAXBElement<String> value) {
        this.gerDir = value;
    }

    /**
     * Obtem o valor da propriedade horAtu.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHorAtu() {
        return horAtu;
    }

    /**
     * Define o valor da propriedade horAtu.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHorAtu(JAXBElement<String> value) {
        this.horAtu = value;
    }

    /**
     * Obtem o valor da propriedade horCad.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHorCad() {
        return horCad;
    }

    /**
     * Define o valor da propriedade horCad.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHorCad(JAXBElement<String> value) {
        this.horCad = value;
    }

    /**
     * Gets the value of the inss property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inss property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getINSS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresINSS }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresINSS> getINSS() {
        if (inss == null) {
            inss = new ArrayList<FornecedoresExportar2OutGridFornecedoresINSS>();
        }
        return this.inss;
    }

    /**
     * Obtem o valor da propriedade indCoo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndCoo() {
        return indCoo;
    }

    /**
     * Define o valor da propriedade indCoo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndCoo(JAXBElement<String> value) {
        this.indCoo = value;
    }

    /**
     * Obtem o valor da propriedade indFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIndFor() {
        return indFor;
    }

    /**
     * Define o valor da propriedade indFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIndFor(JAXBElement<String> value) {
        this.indFor = value;
    }

    /**
     * Obtem o valor da propriedade insAnp.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getInsAnp() {
        return insAnp;
    }

    /**
     * Define o valor da propriedade insAnp.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setInsAnp(JAXBElement<Integer> value) {
        this.insAnp = value;
    }

    /**
     * Obtem o valor da propriedade insEst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsEst() {
        return insEst;
    }

    /**
     * Define o valor da propriedade insEst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsEst(JAXBElement<String> value) {
        this.insEst = value;
    }

    /**
     * Obtem o valor da propriedade insMun.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInsMun() {
        return insMun;
    }

    /**
     * Define o valor da propriedade insMun.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInsMun(JAXBElement<String> value) {
        this.insMun = value;
    }

    /**
     * Obtem o valor da propriedade intNet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIntNet() {
        return intNet;
    }

    /**
     * Define o valor da propriedade intNet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIntNet(JAXBElement<String> value) {
        this.intNet = value;
    }

    /**
     * Obtem o valor da propriedade limRet.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLimRet() {
        return limRet;
    }

    /**
     * Define o valor da propriedade limRet.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLimRet(JAXBElement<String> value) {
        this.limRet = value;
    }

    /**
     * Obtem o valor da propriedade marFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMarFor() {
        return marFor;
    }

    /**
     * Define o valor da propriedade marFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMarFor(JAXBElement<String> value) {
        this.marFor = value;
    }

    /**
     * Obtem o valor da propriedade nomFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomFor() {
        return nomFor;
    }

    /**
     * Define o valor da propriedade nomFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomFor(JAXBElement<String> value) {
        this.nomFor = value;
    }

    /**
     * Obtem o valor da propriedade nomVen.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomVen() {
        return nomVen;
    }

    /**
     * Define o valor da propriedade nomVen.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomVen(JAXBElement<String> value) {
        this.nomVen = value;
    }

    /**
     * Obtem o valor da propriedade notAfo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getNotAfo() {
        return notAfo;
    }

    /**
     * Define o valor da propriedade notAfo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setNotAfo(JAXBElement<Double> value) {
        this.notAfo = value;
    }

    /**
     * Obtem o valor da propriedade notFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getNotFor() {
        return notFor;
    }

    /**
     * Define o valor da propriedade notFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setNotFor(JAXBElement<Double> value) {
        this.notFor = value;
    }

    /**
     * Obtem o valor da propriedade notSis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getNotSis() {
        return notSis;
    }

    /**
     * Define o valor da propriedade notSis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setNotSis(JAXBElement<Double> value) {
        this.notSis = value;
    }

    /**
     * Obtem o valor da propriedade numRge.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumRge() {
        return numRge;
    }

    /**
     * Define o valor da propriedade numRge.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumRge(JAXBElement<String> value) {
        this.numRge = value;
    }

    /**
     * Obtem o valor da propriedade obsMot.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getObsMot() {
        return obsMot;
    }

    /**
     * Define o valor da propriedade obsMot.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setObsMot(JAXBElement<String> value) {
        this.obsMot = value;
    }

    /**
     * Gets the value of the observacoes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the observacoes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObservacoes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresObservacoes }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresObservacoes> getObservacoes() {
        if (observacoes == null) {
            observacoes = new ArrayList<FornecedoresExportar2OutGridFornecedoresObservacoes>();
        }
        return this.observacoes;
    }

    /**
     * Gets the value of the oriMer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oriMer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriMer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FornecedoresExportar2OutGridFornecedoresOriMer }
     * 
     * 
     */
    public List<FornecedoresExportar2OutGridFornecedoresOriMer> getOriMer() {
        if (oriMer == null) {
            oriMer = new ArrayList<FornecedoresExportar2OutGridFornecedoresOriMer>();
        }
        return this.oriMer;
    }

    /**
     * Obtem o valor da propriedade perCod.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerCod() {
        return perCod;
    }

    /**
     * Define o valor da propriedade perCod.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerCod(JAXBElement<Double> value) {
        this.perCod = value;
    }

    /**
     * Obtem o valor da propriedade perIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerIcm() {
        return perIcm;
    }

    /**
     * Define o valor da propriedade perIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerIcm(JAXBElement<Double> value) {
        this.perIcm = value;
    }

    /**
     * Obtem o valor da propriedade perPid.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerPid() {
        return perPid;
    }

    /**
     * Define o valor da propriedade perPid.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerPid(JAXBElement<Double> value) {
        this.perPid = value;
    }

    /**
     * Obtem o valor da propriedade perRin.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerRin() {
        return perRin;
    }

    /**
     * Define o valor da propriedade perRin.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerRin(JAXBElement<Double> value) {
        this.perRin = value;
    }

    /**
     * Obtem o valor da propriedade perRir.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public JAXBElement<Double> getPerRir() {
        return perRir;
    }

    /**
     * Define o valor da propriedade perRir.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Double }{@code >}
     *     
     */
    public void setPerRir(JAXBElement<Double> value) {
        this.perRir = value;
    }

    /**
     * Obtem o valor da propriedade qtdDep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getQtdDep() {
        return qtdDep;
    }

    /**
     * Define o valor da propriedade qtdDep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setQtdDep(JAXBElement<Integer> value) {
        this.qtdDep = value;
    }

    /**
     * Obtem o valor da propriedade recCof.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecCof() {
        return recCof;
    }

    /**
     * Define o valor da propriedade recCof.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecCof(JAXBElement<String> value) {
        this.recCof = value;
    }

    /**
     * Obtem o valor da propriedade recIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecIcm() {
        return recIcm;
    }

    /**
     * Define o valor da propriedade recIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecIcm(JAXBElement<String> value) {
        this.recIcm = value;
    }

    /**
     * Obtem o valor da propriedade recIpi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecIpi() {
        return recIpi;
    }

    /**
     * Define o valor da propriedade recIpi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecIpi(JAXBElement<String> value) {
        this.recIpi = value;
    }

    /**
     * Obtem o valor da propriedade recPis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRecPis() {
        return recPis;
    }

    /**
     * Define o valor da propriedade recPis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRecPis(JAXBElement<String> value) {
        this.recPis = value;
    }

    /**
     * Obtem o valor da propriedade regEst.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRegEst() {
        return regEst;
    }

    /**
     * Define o valor da propriedade regEst.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRegEst(JAXBElement<Integer> value) {
        this.regEst = value;
    }

    /**
     * Obtem o valor da propriedade retCof.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetCof() {
        return retCof;
    }

    /**
     * Define o valor da propriedade retCof.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetCof(JAXBElement<String> value) {
        this.retCof = value;
    }

    /**
     * Obtem o valor da propriedade retCsl.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetCsl() {
        return retCsl;
    }

    /**
     * Define o valor da propriedade retCsl.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetCsl(JAXBElement<String> value) {
        this.retCsl = value;
    }

    /**
     * Obtem o valor da propriedade retIrf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetIrf() {
        return retIrf;
    }

    /**
     * Define o valor da propriedade retIrf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetIrf(JAXBElement<String> value) {
        this.retIrf = value;
    }

    /**
     * Obtem o valor da propriedade retOur.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetOur() {
        return retOur;
    }

    /**
     * Define o valor da propriedade retOur.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetOur(JAXBElement<String> value) {
        this.retOur = value;
    }

    /**
     * Obtem o valor da propriedade retPis.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetPis() {
        return retPis;
    }

    /**
     * Define o valor da propriedade retPis.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetPis(JAXBElement<String> value) {
        this.retPis = value;
    }

    /**
     * Obtem o valor da propriedade retPro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRetPro() {
        return retPro;
    }

    /**
     * Define o valor da propriedade retPro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRetPro(JAXBElement<String> value) {
        this.retPro = value;
    }

    /**
     * Obtem o valor da propriedade rmlVen.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getRmlVen() {
        return rmlVen;
    }

    /**
     * Define o valor da propriedade rmlVen.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setRmlVen(JAXBElement<Integer> value) {
        this.rmlVen = value;
    }

    /**
     * Obtem o valor da propriedade seqInt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqInt() {
        return seqInt;
    }

    /**
     * Define o valor da propriedade seqInt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqInt(JAXBElement<Integer> value) {
        this.seqInt = value;
    }

    /**
     * Obtem o valor da propriedade seqRoe.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSeqRoe() {
        return seqRoe;
    }

    /**
     * Define o valor da propriedade seqRoe.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSeqRoe(JAXBElement<Integer> value) {
        this.seqRoe = value;
    }

    /**
     * Obtem o valor da propriedade sitFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSitFor() {
        return sitFor;
    }

    /**
     * Define o valor da propriedade sitFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSitFor(JAXBElement<String> value) {
        this.sitFor = value;
    }

    /**
     * Obtem o valor da propriedade temOrm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTemOrm() {
        return temOrm;
    }

    /**
     * Define o valor da propriedade temOrm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTemOrm(JAXBElement<String> value) {
        this.temOrm = value;
    }

    /**
     * Obtem o valor da propriedade tipFav.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipFav() {
        return tipFav;
    }

    /**
     * Define o valor da propriedade tipFav.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipFav(JAXBElement<String> value) {
        this.tipFav = value;
    }

    /**
     * Obtem o valor da propriedade tipFor.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipFor() {
        return tipFor;
    }

    /**
     * Define o valor da propriedade tipFor.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipFor(JAXBElement<String> value) {
        this.tipFor = value;
    }

    /**
     * Obtem o valor da propriedade tipMer.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipMer() {
        return tipMer;
    }

    /**
     * Define o valor da propriedade tipMer.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipMer(JAXBElement<String> value) {
        this.tipMer = value;
    }

    /**
     * Obtem o valor da propriedade tipPgt.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTipPgt() {
        return tipPgt;
    }

    /**
     * Define o valor da propriedade tipPgt.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTipPgt(JAXBElement<String> value) {
        this.tipPgt = value;
    }

    /**
     * Obtem o valor da propriedade triIcm.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIcm() {
        return triIcm;
    }

    /**
     * Define o valor da propriedade triIcm.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIcm(JAXBElement<String> value) {
        this.triIcm = value;
    }

    /**
     * Obtem o valor da propriedade triIpi.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIpi() {
        return triIpi;
    }

    /**
     * Define o valor da propriedade triIpi.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIpi(JAXBElement<String> value) {
        this.triIpi = value;
    }

    /**
     * Obtem o valor da propriedade triIss.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTriIss() {
        return triIss;
    }

    /**
     * Define o valor da propriedade triIss.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTriIss(JAXBElement<String> value) {
        this.triIss = value;
    }

}
