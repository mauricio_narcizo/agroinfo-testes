package br.com.senior.poc.dao;

import java.util.List;

import br.com.senior.poc.model.Usuario;
import br.com.senior.poc.model.Config;

public interface ConfigDAO {

	public void salvar(Config config);

	public void atualizar(Config config);

	public void excluir(Config config);

	public Config carregar(Integer codigo);

	public Config buscarPorNome(String nome);

	public List<Config> listar();
}