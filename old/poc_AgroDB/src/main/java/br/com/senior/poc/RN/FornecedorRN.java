package br.com.senior.poc.RN;

import java.util.List;

import javax.transaction.Transactional;

import br.com.senior.poc.dao.FornecedorDAO;
import br.com.senior.poc.dbutil.DAOFactory;
import br.com.senior.poc.model.Fornecedor;


@Transactional()
public class FornecedorRN {
	
	private static FornecedorDAO fornecedorDAO;

	public FornecedorRN() {
		FornecedorRN.fornecedorDAO = DAOFactory.criarFornecedorDAO();
	}

	public void salvar(Fornecedor fornecedor) {
		
		FornecedorRN.fornecedorDAO.salvar(fornecedor);
	}

	public void excluir(Fornecedor fornecedor) {
		FornecedorRN.fornecedorDAO.excluir(fornecedor);
	}

	public Fornecedor carregar(Integer fornecedor) {
		return FornecedorRN.fornecedorDAO.carregar(fornecedor);
	}

	public List<Fornecedor> listar() {
		return FornecedorRN.fornecedorDAO.listar();
	}

	public List<Fornecedor> listar(String fornecedor) {
		return FornecedorRN.fornecedorDAO.listar(fornecedor);
	}

	public Fornecedor buscaOuIniciaFornecedor(Integer value) {
		Fornecedor fornecedor = fornecedorDAO.buscar(value);
		if (fornecedor == null){
			fornecedor = new Fornecedor();
			fornecedor.setCodigo(value);
		}
		return fornecedor;
	}

	public Fornecedor buscaFornecedorCpfTel(String telefone, String cpf) {
	return fornecedorDAO.listar(telefone, cpf);
	}
	public Fornecedor buscaFornecedorTel(String telefone) {
	return fornecedorDAO.listarportel(telefone);
	}


}