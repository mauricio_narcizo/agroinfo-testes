package br.com.senior.poc.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.senior.poc.model.Fornecedor;

@Transactional
public class FornecedorDAOHibernate implements FornecedorDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	@Override
	public void salvar(Fornecedor fornecedor) {
		this.session.merge(fornecedor);
		session.getTransaction().commit();
	}

	@Override
	public void excluir(Fornecedor fornecedor) {
		this.session.delete(fornecedor);
	}

	@Override
	public Fornecedor carregar(Integer fornecedor) {
		return (Fornecedor) this.session.get(Fornecedor.class, fornecedor);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fornecedor> listar() {
		Criteria criteria = this.session.createCriteria(Fornecedor.class);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Fornecedor buscar(Integer codigo) {
		Criteria criteria = this.session.createCriteria(Fornecedor.class);
		if (codigo != null) {
			criteria.add(Restrictions.eq("codigo", codigo)).uniqueResult();
		}
		List<Fornecedor> lista = criteria.list();
		if (lista.isEmpty()) {
			return null;
		} else {
			return (Fornecedor) lista.get(0);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fornecedor> listar(String fornecedor) {

		Criteria criteria = this.session.createCriteria(Fornecedor.class);

		if (fornecedor != null) {
			criteria.add(Restrictions.ilike("nomeFantasia", fornecedor, MatchMode.ANYWHERE));
			// like("nomeFantasia", fornecedor));
		}

		return criteria.list();
	}

	@Override
	public void fechar() {
		this.session.clear();
	}

	@Override
	public Fornecedor listar(String telefone, String cpf) {
		Criteria criteria = this.session.createCriteria(Fornecedor.class);
		if (telefone != null && telefone.length()>=9) {
			criteria.add(Restrictions.ilike("telefone", telefone.substring(telefone.length()-9))).uniqueResult();
		}
		if (cpf != null ) {
			criteria.add(Restrictions.ilike("cpf", cpf)).uniqueResult();
		}

		List<Fornecedor> lista = criteria.list();
		if ((cpf == null && telefone ==null)|lista.isEmpty()) {
			return null;
		} else {
			return (Fornecedor) lista.get(0);
		}
	}
	
	
	@Override
	public Fornecedor listarportel(String telefone) {
		Criteria criteria = this.session.createCriteria(Fornecedor.class);
		if (telefone != null && telefone.length()>=9) {
			criteria.add(Restrictions.ilike("telefone", "%"+telefone.substring(telefone.length()-8)+"%")).uniqueResult();
		}

		List<Fornecedor> lista = criteria.list();
		if ((telefone ==null)|lista.isEmpty()) {
			return null;
		} else {
			return (Fornecedor) lista.get(0);
		}
	}
	
	
	
}