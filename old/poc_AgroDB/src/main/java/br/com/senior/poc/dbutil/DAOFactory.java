package br.com.senior.poc.dbutil;

import br.com.senior.poc.dao.CidadeDAO;
import br.com.senior.poc.dao.CidadeDAOHibernate;
import br.com.senior.poc.dao.ConfigDAO;
import br.com.senior.poc.dao.ContatoDAO;
import br.com.senior.poc.dao.ContatoDAOHibernate;
import br.com.senior.poc.dao.EnderecoDAO;
import br.com.senior.poc.dao.EnderecoDAOHibernate;
import br.com.senior.poc.dao.EstadoDAO;
import br.com.senior.poc.dao.EstadoDAOHibernate;
import br.com.senior.poc.dao.FornecedorDAO;
import br.com.senior.poc.dao.FornecedorDAOHibernate;
import br.com.senior.poc.dao.TituloDAO;
import br.com.senior.poc.dao.TituloDAOHibernate;
import br.com.senior.poc.dao.UsuarioDAO;
import br.com.senior.poc.dao.UsuarioDAOHibernate;
import br.com.senior.poc.dao.ConfigDAOHibernate;



public class DAOFactory {

	public static UsuarioDAO criarUsuarioDAO() {

		UsuarioDAOHibernate usuarioDAO = new UsuarioDAOHibernate();

		usuarioDAO.setSession(HibernateUtil.getCurrentSession());

		return usuarioDAO;
	}

	public static FornecedorDAO criarFornecedorDAO() {
		FornecedorDAOHibernate fornecedorDAO = new FornecedorDAOHibernate();
		fornecedorDAO.setSession(HibernateUtil.getCurrentSession());
		return fornecedorDAO;
	}
	
	public void fecharFornecedorDAO() {
		FornecedorDAOHibernate fornecedorDAO = new FornecedorDAOHibernate();
		fornecedorDAO.getSession().close();
	}

	public static CidadeDAO criarCidadeDAO() {

		CidadeDAOHibernate cidadeDAO = new CidadeDAOHibernate();

		cidadeDAO.setSession(HibernateUtil.getCurrentSession());

		return cidadeDAO;
	}

	public static EstadoDAO criarEstadoDAO() {

		EstadoDAOHibernate estadoDAO = new EstadoDAOHibernate();

		estadoDAO.setSession(HibernateUtil.getCurrentSession());

		return estadoDAO;
	}

	public static ContatoDAO criarContatoDAO() {

		ContatoDAOHibernate contatoDAO = new ContatoDAOHibernate();

		contatoDAO.setSession(HibernateUtil.getCurrentSession());

		return contatoDAO;

	}

	public static EnderecoDAO criarEnderecoDAO() {

		EnderecoDAOHibernate enderecoDAO = new EnderecoDAOHibernate();

		enderecoDAO.setSession(HibernateUtil.getCurrentSession());

		return enderecoDAO;
	}
	
	
	public static ConfigDAO criarConfigDAO() {
		ConfigDAOHibernate configDAO = new ConfigDAOHibernate();
		configDAO.setSession(HibernateUtil.getCurrentSession());
		return configDAO;
	}

	public static TituloDAO criaTituloDAO() {
		TituloDAOHibernate TituloDAO = new TituloDAOHibernate();
		TituloDAO.setSession(HibernateUtil.getCurrentSession());
		return TituloDAO;
	}

	


}