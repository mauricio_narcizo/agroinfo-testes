package br.com.senior.poc.sessao;

public interface SeniorObservable {

    public void addObserver(SeniorObserver observer);

    public void deleteObserver(SeniorObserver observer);

    public void notifyObservers();

    public void notifyObservers(Object objeto);

}
