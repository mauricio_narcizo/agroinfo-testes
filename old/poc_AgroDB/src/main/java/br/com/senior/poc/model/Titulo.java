package br.com.senior.poc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="tbTitulo")
public class Titulo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6269276424422686896L;
	
	@Id
	private Long idTitulo;
	
	private String descricao;
	
	private String descEstorno;
	

	private Long idTituloPai;
	
	private Double valor;
	
	private Double valorTitulo;

	private TipoTitulo tipoTitulo;
	
	@Temporal(TemporalType.DATE)
	private Date dataLancamento;
	
	@Temporal(TemporalType.DATE)
	private Date vencimento;
	
	@Temporal(TemporalType.DATE)
	private Date dataPagamento;
	

	
	@ManyToOne
	private Fornecedor fornecedor;
	


	public Long getIdTitulo() {
		return idTitulo;
	}

	public void setIdConta(Long idTitulo) {
		this.idTitulo = idTitulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setPessoa(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setIdTitulo(Long idTitulo) {
		this.idTitulo = idTitulo;
	}
	
	public TipoTitulo getTipoTitulo() {
		return tipoTitulo;
	}
	
	public void setTipoTitulo(TipoTitulo tipoTitulo) {
		this.tipoTitulo = tipoTitulo;
	}	

	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public Double getValorTitulo() {
		return valorTitulo;
	}

	public void setValorTitulo(Double valorTitulo) {
		this.valorTitulo = valorTitulo;
	}

	public Long getIdTituloPai() {
		return idTituloPai;
	}

	public void setIdTituloPai(Long idTituloPai) {
		this.idTituloPai = idTituloPai;
	}

	public String getDescEstorno() {
		return descEstorno;
	}

	public void setDescEstorno(String descEstorno) {
		this.descEstorno = descEstorno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTitulo == null) ? 0 : idTitulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Titulo other = (Titulo) obj;
		if (idTitulo == null) {
			if (other.idTitulo != null)
				return false;
		} else if (!idTitulo.equals(other.idTitulo))
			return false;
		return true;
	}

}
