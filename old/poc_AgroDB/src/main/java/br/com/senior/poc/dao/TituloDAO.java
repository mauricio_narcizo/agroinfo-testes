package br.com.senior.poc.dao;

import java.util.List;

import javax.transaction.Transactional;

import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.model.Titulo;

@Transactional
public interface TituloDAO {

	public void salvar(Titulo titulo) throws Exception;
	public List<Titulo> pesquisafornecedor(Fornecedor fornecedor) throws Exception;
	public void excluir(Titulo titulo) throws Exception;
}