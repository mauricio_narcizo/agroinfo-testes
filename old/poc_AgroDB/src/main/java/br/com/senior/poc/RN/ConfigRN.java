package br.com.senior.poc.RN;

import java.util.List;
import br.com.senior.poc.dao.ConfigDAO;


import br.com.senior.poc.dbutil.DAOFactory;
import br.com.senior.poc.model.Config;


public class ConfigRN {

	private ConfigDAO configDAO;

	public ConfigRN() {
		this.configDAO = DAOFactory.criarConfigDAO();
	}

	public Config carregar(Integer codigo) {
		return this.configDAO.carregar(codigo);
	}

	public Config buscarPorNome(String nome) {
		return this.configDAO.buscarPorNome(nome);
	}

	public void salvar(Config config) {

		Integer codigo = config.getCodigo();

		if (codigo == null || codigo == 0) {
			this.configDAO.salvar(config);
		} else {
			this.configDAO.atualizar(config);
		}
		
	}

	public void excluir(Config config) {
		this.configDAO.excluir(config);
	}

	public List<Config> listar() {
		return this.configDAO.listar();
	}
}