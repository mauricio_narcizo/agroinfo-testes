package br.com.senior.poc.dbutil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;

import org.hibernate.Session;

public class HibernateUtil {

	
	private static final Session session;
	@PersistenceContext(unitName = "agro", type = PersistenceContextType.EXTENDED)
	 private static EntityManagerFactory entityManagerFactory;
	 @PersistenceUnit(unitName = "agro")
	private static EntityManager entityManager;


	static {
		try {
			entityManagerFactory =Persistence.createEntityManagerFactory("agro");
			entityManager =  entityManagerFactory.createEntityManager();
			session = entityManager.unwrap(Session.class);
		} catch (Throwable ex) {
			// Log the exception.
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}


	public static Session beginTransaction() {

		Session hibernateSession = HibernateUtil.getSession();

		System.out.println("Transaction Factory: " + hibernateSession.getTransaction().getClass().toString());

		hibernateSession.beginTransaction();
		return hibernateSession;
	}

	public static void commitTransaction() {
		HibernateUtil.getSession().getTransaction().commit();
	}

	public static void rollbackTransaction() {
		HibernateUtil.getSession().getTransaction().rollback();
	}

	public static void closeSession() {
		HibernateUtil.getSession().close();
	}

	public static Session getCurrentSession() {
		Session currentSession = entityManager.unwrap(Session.class);
		if (!currentSession.getTransaction().isActive()) {
			currentSession.getTransaction().begin();
		}
		return currentSession;
	}

	public static Session getSession() {
		Session hibernateSession = entityManager.unwrap(Session.class);
		return hibernateSession;
	}
}