package br.com.senior.poc.RN;

import java.util.List;

import br.com.senior.poc.dao.TituloDAO;
import br.com.senior.poc.dbutil.DAOFactory;
import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.model.Titulo;


public class TituloRN {

	
	private static TituloDAO tituloDAO;
	public TituloRN() {
		TituloRN.tituloDAO = DAOFactory.criaTituloDAO();
	}
	

	public void atualizaTitulo(Titulo titulo) throws Exception {
		TituloRN.tituloDAO.salvar(titulo);
	}

	public List<Titulo> pesquisaFornecedor(Fornecedor fornecedor) throws Exception {
		return TituloRN.tituloDAO.pesquisafornecedor(fornecedor);
	}

	public void salvar(Titulo titulo) throws Exception {
		TituloRN.tituloDAO.salvar(titulo);
		
	}


	public int excluirTitulo(Titulo titulo) throws Exception {
		TituloRN.tituloDAO.excluir(titulo);
		return 0;
	}



}
