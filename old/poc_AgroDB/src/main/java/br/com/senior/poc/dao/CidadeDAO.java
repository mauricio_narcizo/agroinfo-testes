package br.com.senior.poc.dao;

import java.util.List;

import javax.transaction.Transactional;

import br.com.senior.poc.model.Cidade;
import br.com.senior.poc.model.Estado;

@Transactional
public interface CidadeDAO {
	
	public Cidade carregar(Integer cidade);
	public List<Cidade> listar(Estado estado);
}
