package br.com.senior.poc.sessao;


public interface SeniorObserver {

	public void update(Object objeto);
}
