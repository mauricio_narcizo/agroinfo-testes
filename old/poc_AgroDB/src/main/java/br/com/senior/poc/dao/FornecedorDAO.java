package br.com.senior.poc.dao;

import java.util.List;

import javax.transaction.Transactional;

import br.com.senior.poc.model.Fornecedor;
@Transactional
public interface FornecedorDAO {
	public void salvar(Fornecedor fornecedor);
	public void fechar();
	public void excluir(Fornecedor fornecedor);
	public Fornecedor carregar(Integer fornecedor);
	public List<Fornecedor> listar();
	public List<Fornecedor> listar(String fornecedor);
	Fornecedor buscar(Integer codigo);
	public Fornecedor listar(String telefone, String cpf);
	Fornecedor listarportel(String telefone);
}
