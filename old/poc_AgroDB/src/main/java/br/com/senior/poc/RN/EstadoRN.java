package br.com.senior.poc.RN;

import java.util.List;

import br.com.senior.poc.dao.EstadoDAO;
import br.com.senior.poc.dbutil.DAOFactory;
import br.com.senior.poc.model.Estado;



public class EstadoRN {

	private EstadoDAO estadoDAO;

	public EstadoRN() {
		this.estadoDAO = DAOFactory.criarEstadoDAO();
	}

	public Estado carregar(Integer estado) {
		return this.estadoDAO.carregar(estado);
	}

	public List<Estado> listar() {
		return this.estadoDAO.listar();
	}

}
