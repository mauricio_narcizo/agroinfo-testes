package br.com.senior.poc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.senior.poc.model.Config;

public class ConfigDAOHibernate implements ConfigDAO {

	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Config config) {
		Transaction tx = session.beginTransaction();
		this.session.merge(config);
		tx.commit();
	}

	@Override
	public void atualizar(Config config) {
		Config configPermissao = this.carregar(config.getCodigo());
		this.session.evict(configPermissao);
		this.session.update(config);
	}

	@Override
	public void excluir(Config config) {
		this.session.delete(config);
	}

	@Override
	public Config carregar(Integer codigo) {
		return (Config) this.session.get(Config.class, codigo);
	}

	@Override
	public Config buscarPorNome(String nome) {
		String hql = "select u from config u where u.nome = :nome";
		Query consulta = this.session.createQuery(hql);
		consulta.setString("nome", nome);
		return (Config) consulta.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Config> listar() {
		return this.session.createCriteria(Config.class).list();
	}

}