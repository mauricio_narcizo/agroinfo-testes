package br.com.senior.poc.RN;

import java.util.List;

import br.com.senior.poc.dao.CidadeDAO;
import br.com.senior.poc.dbutil.DAOFactory;
import br.com.senior.poc.model.Cidade;
import br.com.senior.poc.model.Estado;



public class CidadeRN {

	private CidadeDAO cidadeDAO;

	public CidadeRN() {
		this.cidadeDAO = DAOFactory.criarCidadeDAO();
	}

	public Cidade carregar(Integer cidade) {
		return this.cidadeDAO.carregar(cidade);
	}

	public List<Cidade> listar(Estado estado) {
		return this.cidadeDAO.listar(estado);
	}

}