package br.com.senior.poc.dao;

import java.util.List;

import br.com.senior.poc.model.Estado;

public interface EstadoDAO {

	public Estado carregar(Integer estado);
	public List<Estado> listar();
}
