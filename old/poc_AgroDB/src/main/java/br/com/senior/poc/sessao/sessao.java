package br.com.senior.poc.sessao;

import java.util.ArrayList;
import java.util.List;



public class sessao implements SeniorObservable {

	private static sessao instance;

	private int filial;
	private int empresa;
	private String sigla;
	private int encript;
	private String usuario;
	private String senha;
	private String url;
	

	private final List<SeniorObserver> observers = new ArrayList<>();

	public static sessao getInstance() {
		if (instance == null) {
			instance = new sessao();
			instance.initAppSession();
		}
		return instance;
	}

	public void initAppSession() {
		// vou colocar aqui agora depois tem que colocar no banco
		filial=1;
		empresa=1;
		sigla="AGRO";
		encript=0;
		usuario="suporte";
		senha="suporte";
		url="http://localhost:8080";
	}

	@Override
	public void notifyObservers(Object objeto) {
		for (SeniorObserver observer : observers) {
			observer.update(objeto);
		}
	}

	@Override
	public void addObserver(SeniorObserver observer) {
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	@Override
	public void deleteObserver(SeniorObserver observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObservers() {
	}

	public int getFilial() {
		return filial;
	}

	public void setFilial(int filial) {
		this.filial = filial;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getEncript() {
		return encript;
	}

	public void setEncript(int encript) {
		this.encript = encript;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}


	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
