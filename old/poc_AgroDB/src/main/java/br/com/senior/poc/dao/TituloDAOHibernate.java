package br.com.senior.poc.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.senior.poc.model.Fornecedor;
import br.com.senior.poc.model.Titulo;

@Transactional
public class TituloDAOHibernate  implements TituloDAO {

	
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	@Override
	public void salvar(Titulo titulo) throws Exception {
		this.session.merge(titulo);
		session.getTransaction().commit();
	
	}

	@Override
	public List<Titulo> pesquisafornecedor(Fornecedor fornecedor) throws Exception {
		Criteria criteria = this.session.createCriteria(Titulo.class);
		if (fornecedor != null) {
			criteria.add(Restrictions.eq("fornecedor", fornecedor));
			// like("nomeFantasia", fornecedor));
		}
		return criteria.list();
	}

	@Override
	public void excluir(Titulo titulo) throws Exception {
		this.session.delete(titulo);
		
	}


}




