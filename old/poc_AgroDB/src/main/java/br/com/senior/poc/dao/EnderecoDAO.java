package br.com.senior.poc.dao;

import br.com.senior.poc.model.Endereco;

public interface EnderecoDAO {
	public void salvar(Endereco endereco);
	public void excluir(Endereco endereco);
	public Endereco carregar(Integer fornecedor);
}
