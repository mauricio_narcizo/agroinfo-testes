/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senior.poc.sessao;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

/**
 * @author Clayton
 */
@SuppressWarnings("unchecked")
public class SortedProperties extends Properties {

	static final long serialVersionUID = 1L;

	private Map<Object, Object> originais = new HashMap<Object, Object>();

	@SuppressWarnings("rawtypes")
	@Override
	public synchronized Enumeration keys() {
		Set<Object> keySet = keySet();
		Vector keyList = new Vector();
		for (Object val : keySet) {
			keyList.add(val);
		}
		Collections.sort(keyList);
		return keyList.elements();
	}

	@SuppressWarnings("rawtypes")
	public void sortedStore(OutputStream paramOutputStream) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(paramOutputStream, "8859_1"));
		bufferedWriter.write("#" + new Date().toString());
		bufferedWriter.newLine();
		synchronized (this) {
			Enumeration localEnumeration = keys();
			while (localEnumeration.hasMoreElements()) {
				String key = (String) localEnumeration.nextElement();
				String value = getProperty(key);
				bufferedWriter.write(key + "=" + value);
				bufferedWriter.newLine();
			}
		}
		bufferedWriter.flush();
		bufferedWriter.close();
	}

	@Override
	public synchronized Object put(Object key, Object value) {
		if (key instanceof String) {
			Set<Object> keySet = originais.keySet();

			for (Object chaveO : keySet) {
				if (((String) chaveO).equalsIgnoreCase((String) key)) {
					key = chaveO;
					break;
				}
			}
		}
		originais.put(key, value);
		return super.put(key instanceof String ? ((String) key).toUpperCase() : key, value);
	}

	@Override
	public Set<Object> keySet() {
		return originais.keySet();
	}

	@Override
	public synchronized Object get(Object key) {
		if (key instanceof String) {
			return super.get(((String) key).toUpperCase());
		}
		return super.get(key);
	}

	@Override
	public synchronized void clear() {
		originais.clear();
		super.clear();
	}

	@Override
	public String getProperty(String key) {
		Object oval = get(key);
		String sval = (oval instanceof String) ? (String) oval : null;
		return ((sval == null) && (defaults != null)) ? defaults.getProperty(key) : sval;
	}

	@Override
	public synchronized boolean containsKey(Object key) {
		return super.containsKey(key instanceof String ? ((String) key).toUpperCase() : key);
	}
}
