package br.com.senior.poc.dao;

import br.com.senior.poc.model.Contato;

public interface ContatoDAO {
	
	public void salvar(Contato contato);
	public void excluir(Contato contato);
	public Contato carregar(Integer fornecedor);
}
