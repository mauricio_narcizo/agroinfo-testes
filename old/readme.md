# AgroinfoMiddleware

Agroinfo is a cloud-enabled, mobile-ready, offline-storage, AngularJS powered HTML5 with Ionic aplication to show agricultors money in cooperatives.

### Version
0.0.1

### Tech

AgroinfoMiddleware uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [QUARK] - awesome web-based text editor
* [Marked] - a super fast port of Markdown to JavaScript
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [Gulp] - the streaming build system
* [keymaster.js] - awesome keyboard handler lib by [@thomasfuchs]
* [jQuery] - duh

And of course Dillinger itself is open source with a [public repository](https://github.com/joemccann/dillinger) on GitHub.

### Installation
Instalar postgres 
Configurar posgres arquivo PG_HBA.conf


Configurando o datasource
https://gesker.wordpress.com/2015/03/13/postgresql-datasource-and-xadatasource-on-wildfly-8-2-0/




### Plugins


