angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('CoopCtrl', function($scope,$http) {
    // Get all posts
    $http.get('http://agroinfo.ddns.net:28080/server/get')
    .success(function(data, status, headers, config){
        console.log("**** SUCCESS ****");
        console.log(status);
    })
    .error(function(data, status, headers, config){
        console.log("**** ERROR ****");
        console.log(status);
    })
    .then(function(response){
        console.log("**** THEN ****");
        $scope.coopIp = response.data;
    })
})


.controller('LoginCtrl', function ($scope, LoginService, $ionicPopup, $state) {
    $scope.data = {};

    $scope.login = function () {
        LoginService.loginUser($scope.data.username, $scope.data.password, $scope.data.cooperativa).success(function (data) {
            $state.go('tab.dash');
        }).error(function (data) {
            var alertPopup = $ionicPopup.alert({
                title: 'Falha ao efetuar o login!',
                template: data
            });
        });
    }
});
