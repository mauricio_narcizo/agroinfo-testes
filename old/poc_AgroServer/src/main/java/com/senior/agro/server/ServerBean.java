package com.senior.agro.server;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ServerBean {

    @PersistenceContext
    EntityManager em;
    
    public List<Server> get() {
        return em.createNamedQuery("Server.findAll", Server.class).getResultList();
    }
}
