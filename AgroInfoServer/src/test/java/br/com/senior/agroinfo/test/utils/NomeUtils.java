package br.com.senior.agroinfo.test.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NomeUtils {
	static List<String> Nomes = populaNomes();

	static List<String> Sobrenomes = populaSobreNomes();

	
	public static List<String> populaSobreNomes() {
		Sobrenomes = new ArrayList<String>();
		Sobrenomes.add("Silva");
		Sobrenomes.add("Souza");
		Sobrenomes.add("Costa");
		Sobrenomes.add("Santos");
		Sobrenomes.add("Oliveira");
		Sobrenomes.add("Pereira");
		Sobrenomes.add("Rodrigues");
		Sobrenomes.add("Almeida");
		Sobrenomes.add("Nascimento");
		Sobrenomes.add("Lima");
		Sobrenomes.add("Araújo");
		Sobrenomes.add("Fernandes");
		Sobrenomes.add("Carvalho");
		Sobrenomes.add("Gomes");
		Sobrenomes.add("Martins");
		Sobrenomes.add("Rocha");
		Sobrenomes.add("Ribeiro");
		Sobrenomes.add("Alves");
		Sobrenomes.add("Monteiro");
		Sobrenomes.add("Mendes");
		Sobrenomes.add("Barros");
		Sobrenomes.add("Freitas");
		Sobrenomes.add("Barbosa");
		Sobrenomes.add("Pinto");
		Sobrenomes.add("Moura");
		Sobrenomes.add("Cavalcanti");
		Sobrenomes.add("Dias");
		Sobrenomes.add("Castro");
		Sobrenomes.add("Campos");
		Sobrenomes.add("Cardoso ");
		return Sobrenomes;
	}
	
	public static List<String> populaNomes() {
		Nomes = new ArrayList<String>();
		Nomes.add("Miguel");
		Nomes.add("Sophia");
		Nomes.add("Davi");
		Nomes.add("Alice");
		Nomes.add("Arthur");
		Nomes.add("Julia");
		Nomes.add("Pedro");
		Nomes.add("Isabella");
		Nomes.add("Gabriel");
		Nomes.add("Manuela");
		Nomes.add("Bernardo");
		Nomes.add("Laura");
		Nomes.add("Lucas");
		Nomes.add("Luiza");
		Nomes.add("Matheus");
		Nomes.add("Valentina");
		Nomes.add("Rafael");
		Nomes.add("Giovanna");
		Nomes.add("Heitor");
		Nomes.add("Maria Eduarda");
		Nomes.add("Enzo");
		Nomes.add("Helena");
		Nomes.add("Guilherme");
		Nomes.add("Beatriz");
		Nomes.add("Nicolas");
		Nomes.add("Maria Luiza");
		Nomes.add("Lorenzo");
		Nomes.add("Lara");
		Nomes.add("Gustavo");
		Nomes.add("Mariana");
		Nomes.add("Felipe");
		Nomes.add("Nicole");
		Nomes.add("Samuel");
		Nomes.add("Rafaela");
		Nomes.add("João Pedro");
		Nomes.add("Heloísa");
		Nomes.add("Daniel");
		Nomes.add("Isadora");
		Nomes.add("Vitor");
		Nomes.add("Lívia");
		Nomes.add("Leonardo");
		Nomes.add("Maria Clara");
		Nomes.add("Henrique");
		Nomes.add("Ana Clara");
		Nomes.add("Theo");
		Nomes.add("Lorena");
		Nomes.add("Murilo");
		Nomes.add("Gabriela");
		Nomes.add("Eduardo");
		Nomes.add("Yasmin");
		Nomes.add("Pedro Henrique");
		Nomes.add("Isabelly");
		Nomes.add("Pietro");
		Nomes.add("Sarah");
		Nomes.add("Cauã");
		Nomes.add("Ana Julia");
		Nomes.add("Isaac");
		Nomes.add("Letícia");
		Nomes.add("Caio");
		Nomes.add("Ana Luiza");
		Nomes.add("Vinicius");
		Nomes.add("Melissa");
		Nomes.add("Benjamin");
		Nomes.add("Marina");
		Nomes.add("João");
		Nomes.add("Clara");
		Nomes.add("Lucca");
		Nomes.add("Cecília");
		Nomes.add("João Miguel");
		Nomes.add("Esther");
		Nomes.add("Bryan");
		Nomes.add("Emanuelly");
		Nomes.add("Joaquim");
		Nomes.add("Rebeca");
		Nomes.add("João Vitor");
		Nomes.add("Ana Beatriz");
		Nomes.add("Thiago");
		Nomes.add("Lavínia");
		Nomes.add("Antônio");
		Nomes.add("Vitória");
		Nomes.add("Davi Lucas");
		Nomes.add("Bianca");
		Nomes.add("Francisco");
		Nomes.add("Catarina");
		Nomes.add("Enzo Gabriel");
		Nomes.add("Larissa");
		Nomes.add("Bruno");
		Nomes.add("Maria Fernanda");
		Nomes.add("Emanuel");
		Nomes.add("Fernanda");
		Nomes.add("João Gabriel");
		Nomes.add("Amanda");
		Nomes.add("Ian");
		Nomes.add("Alícia");
		Nomes.add("Davi Luiz");
		Nomes.add("Carolina");
		Nomes.add("Rodrigo");
		Nomes.add("Agatha");
		Nomes.add("Otávio");
		Nomes.add("Gabrielly");
		return Nomes;
	}

	
	public static String nomeAleatorio(){
		Random random = new Random();
		return Nomes.get(random.nextInt(Nomes.size()));
	}
	
	public static String sobreNomeAleatorio(){
		Random random = new Random();
		return Sobrenomes.get(random.nextInt(Sobrenomes.size()));
	}
	
	public static void main(String[] args) {
		
		System.out.println(nomeAleatorio()+" "+ sobreNomeAleatorio());
	}
	

}
