package br.com.senior.agroinfo.test.utils;

import java.util.ArrayList;
import java.util.InputMismatchException;

import javax.swing.text.MaskFormatter;

public class CpfUtils {

	private ArrayList<Integer> listaAleatoria = new ArrayList<Integer>();
	private ArrayList<Integer> listaNumMultiplicados = null;

	// Metodo para geracao de um numero aleatorio entre 0 e 9
	public int geraNumAleatorio() {
		// Note que foi preciso fazer um cast para int, ja que Math.random()
		// retorna um double
		int numero = (int) (Math.random() * 10);

		return numero;
	}

	// Metodo para geracao de parte do nosso CPF (aqui geramos apenas os 9
	// primeiros digitos)
	public ArrayList<Integer> geraCPFParcial() {
		for (int i = 0; i < 9; i++) {
			listaAleatoria.add(geraNumAleatorio());
		}

		return listaAleatoria;
	}

	// Metodo para geracao do primeiro digito verificador (para isso nos
	// baseamos nos 9 digitos aleatorios gerados anteriormente)
	public ArrayList<Integer> geraPrimeiroDigito() {
		listaNumMultiplicados = new ArrayList<Integer>();
		int primeiroDigito;
		int totalSomatoria = 0;
		int restoDivisao;
		int peso = 10;

		// Para cada item na lista multiplicamos seu valor pelo seu peso
		for (int item : listaAleatoria) {
			listaNumMultiplicados.add(item * peso);

			peso--;
		}

		// Agora somamos todos os itens que foram multiplicados
		for (int item : listaNumMultiplicados) {
			totalSomatoria += item;
		}

		restoDivisao = (totalSomatoria % 11);

		// Se o resto da divisao for menor que 2 o primeiro digito sera 0, senao
		// subtraimos o numero 11 pelo resto da divisao
		if (restoDivisao < 2) {
			primeiroDigito = 0;
		} else {
			primeiroDigito = 11 - restoDivisao;
		}

		// Apos gerar o primeiro digito o adicionamos a lista
		listaAleatoria.add(primeiroDigito);

		return listaAleatoria;
	}

	// Metodo para geracao do segundo digito verificador (para isso nos baseamos
	// nos 9 digitos aleatorios + o primeiro digito verificador)
	public ArrayList<Integer> geraSegundoDigito() {
		listaNumMultiplicados = new ArrayList<Integer>();
		int segundoDigito;
		int totalSomatoria = 0;
		int restoDivisao;
		int peso = 11;

		// Para cada item na lista multiplicamos seu valor pelo seu peso
		// (observe que com o aumento da lista o peso tambem aumenta)
		for (int item : listaAleatoria) {
			listaNumMultiplicados.add(item * peso);

			peso--;
		}

		// Agora somamos todos os itens que foram multiplicados
		for (int item : listaNumMultiplicados) {
			totalSomatoria += item;
		}

		restoDivisao = (totalSomatoria % 11);

		// Se o resto da divisao for menor que 2 o segundo digito sera 0, senao
		// subtraimos o numero 11 pelo resto da divisao
		if (restoDivisao < 2) {
			segundoDigito = 0;
		} else {
			segundoDigito = 11 - restoDivisao;
		}

		// Apos gerar o segundo digito o adicionamos a lista
		listaAleatoria.add(segundoDigito);

		return listaAleatoria;
	}

	// Agora que temos nossa lista com todos os digitos que precisamos vamos
	// formatar os valores de acordo com a mascara do CPF
	public String geraCPFFinal() {
		// Primeiro executamos os metodos de geracao
		geraCPFParcial();
		geraPrimeiroDigito();
		geraSegundoDigito();

		String cpf = "";
		String texto = "";

		/*
		 * Aqui vamos concatenar todos os valores da lista em uma string Por que
		 * isso? Porque a formatacao que o ArrayList gera me impossibilitaria de
		 * usar a mascara, pois junto com os numeros gerados ele tambem gera
		 * caracteres especias. Ex.: lista com inteiros (de 1 a 5) [1 , 2 , 3 ,
		 * 4 , 5] Dessa forma o sistema geraria a excecao ParseException
		 */
		for (int item : listaAleatoria) {
			texto += item;
		}

		// Dentro do bloco try.. catch.. tentaremos adicionar uma mascara ao
		// nosso CPF
		try {
			MaskFormatter mf = new MaskFormatter("###.###.###-##");
			mf.setValueContainsLiteralCharacters(false);
			cpf = mf.valueToString(texto);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return cpf;
	}

	public static boolean isCPF(String CPF) { 
		CPF = CPF.replace(".", "").replace("-", "");
		// considera-se erro CPF's
		// formados por uma sequencia de
		// numeros iguais
		if (CPF == null || CPF.length() != 11
				|| CPF.matches("^(0{11}|1{11}|2{11}|3{11}|4{11}|5{11}|6{11}|7{11}|9{11}|9{11})$")) {
			return false;
		}
		char dig10, dig11;
		int sm, i, r, num, peso;
		// "try" - protege o codigo para eventuais erros de conversao de tipo
		// (int)
		try { // Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i = 0; i < 9; i++) {
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0
				// (48 eh a posicao de '0' na tabela ASCII)
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}
			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig10 = '0';
			else
				dig10 = (char) (r + 48);
			// converte no respectivo caractere numerico
			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for (i = 0; i < 10; i++) {
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}
			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11)) {
				dig11 = '0';
			} else {
				dig11 = (char) (r + 48);
			}
			// Verifica se os digitos calculados conferem com os digitos
			// informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
				return (true);
			} else {
				return (false);
			}
		} catch (InputMismatchException erro) {
			return (false);
		}
	}
}
