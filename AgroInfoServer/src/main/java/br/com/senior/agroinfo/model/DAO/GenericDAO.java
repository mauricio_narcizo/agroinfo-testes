package br.com.senior.agroinfo.model.DAO;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Transactional
public abstract class GenericDAO<T> {

    @Inject
    protected EntityManager em;
    
    protected void adiciona(T t) {
        this.em.persist(t);
    }

    protected void remove(T t) {
        this.em.remove(t);
    }

    protected Optional<T> busca(Long id) {
		if (id==null){
			return Optional.empty();
		}
        return Optional.ofNullable(this.em.find(getEntityClass(), id));
    }

    protected T atualiza(T t) {
        return this.em.merge(t);
    }

    @SuppressWarnings("unchecked")
    public List<T> getLista() {
        return this.em.createQuery("from " + getEntityClass().getName()).getResultList();
    }

    protected abstract Class<T> getEntityClass();
}