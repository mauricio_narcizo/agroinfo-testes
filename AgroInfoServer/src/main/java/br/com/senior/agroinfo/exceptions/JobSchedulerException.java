package br.com.senior.agroinfo.exceptions;

public class JobSchedulerException extends Exception {

    private static final long serialVersionUID = 1L;

    public JobSchedulerException(final String msg) {
        super(msg);
    }

}
