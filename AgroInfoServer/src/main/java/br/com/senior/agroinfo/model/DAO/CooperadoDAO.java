package br.com.senior.agroinfo.model.DAO;

import java.util.List;
import java.util.Optional;

import br.com.senior.agroinfo.model.Cooperado;

public class CooperadoDAO extends GenericDAO<Cooperado> {

	public Cooperado delete(Cooperado cooperado) {
		Optional<Cooperado> optional = busca(cooperado.getId());
		optional.ifPresent(coop -> remove(coop));
		return cooperado;
	}

	public Cooperado create(Cooperado cooperado) {
		Optional<Cooperado> busca = busca(cooperado.getId());
		if (busca.isPresent()) {
			atualiza(cooperado);
		} else {
			adiciona(cooperado);
		}

		return cooperado;
	}

	public Cooperado update(Cooperado cooperado) {
		return atualiza(cooperado);
	}

	public Optional<Cooperado> recupera(Long id) {
		return busca(id);
	}

	@Override
	protected Class<Cooperado> getEntityClass() {
		return Cooperado.class;
	}

	public Optional<Cooperado> getCooperadosPorCnpj(String cnpj) {
		List<Cooperado> lista = getLista();
		//TODO: Resolver isso realmente ta bem feio
		return lista.stream().filter(cooperado -> cooperado.getCpfCnpj().equals(cnpj)).findFirst();
	}
}