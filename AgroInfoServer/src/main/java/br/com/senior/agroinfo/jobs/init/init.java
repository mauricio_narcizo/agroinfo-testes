package br.com.senior.agroinfo.jobs.init;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.JobFactory;

import br.com.senior.agroinfo.exceptions.JobSchedulerException;
import br.com.senior.agroinfo.service.BuscaCooperados;

@Startup
@Singleton
public class init {

	private static Scheduler scheduler;

	@Inject
	private JobFactory jobFactory;

	@PostConstruct
	public void iniciar() throws Exception {
		try {

			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.setJobFactory(jobFactory);

			JobDetail job1 = JobBuilder.newJob(BuscaCooperados.class).withIdentity("job1", "group1").build();

			Trigger trigger1 = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1")
					.withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(1)) // Executa,
																					// a
																					// cada
																					// 1
																					// minuto
					.build();

			scheduler.scheduleJob(job1, trigger1);
			scheduler.start();
		} catch (Exception e) {
			throw new JobSchedulerException(e.getMessage());
		}
	}

	@PreDestroy
	public void destroy() throws Exception {
		scheduler.shutdown(false);//TODO alterar para true
	}
}
