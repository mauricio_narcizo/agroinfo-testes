package br.com.senior.agroinfo.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Cooperado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8487769877594686402L;

	@Id
	private Long id;

	private String name;

	@NotNull
	private String cpfCnpj;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "cooperativa_id", referencedColumnName = "id")
	private List<Cooperativa> cooperativa;

	public List<Cooperativa> getCooperativa() {
		return cooperativa;
	}

	public void setCooperativa(List<Cooperativa> cooperativa) {
		this.cooperativa = cooperativa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cooperativa == null) ? 0 : cooperativa.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cooperado other = (Cooperado) obj;
		if (cooperativa == null) {
			if (other.cooperativa != null)
				return false;
		} else if (!cooperativa.equals(other.cooperativa))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}