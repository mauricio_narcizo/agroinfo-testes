package br.com.senior.agroinfo.beans;

import javax.ejb.Singleton;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

@Singleton
public class BuscaInfoCooperativasBeam {

	public void buscar() {

		try {
			// Grab the Scheduler instance from the Factory
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

			buscaJbos(scheduler);
			// and start it off
			scheduler.start();

			scheduler.shutdown();

		} catch (SchedulerException se) {
			se.printStackTrace();
		}

	}

	private void buscaJbos(Scheduler scheduler) {
		// TODO Auto-generated method stub
		
	}


}
