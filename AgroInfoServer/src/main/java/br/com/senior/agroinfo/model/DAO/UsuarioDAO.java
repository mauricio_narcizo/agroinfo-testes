package br.com.senior.agroinfo.model.DAO;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.senior.agroinfo.model.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario> {

	public Usuario delete(Usuario Usuario) {
		Optional<Usuario> optional = busca(Usuario.getId());
		optional.ifPresent(coop -> remove(coop));
		return Usuario;
	}

	public Usuario create(Usuario Usuario) {
		Optional<Usuario> busca = busca(Usuario.getId());
		if (busca.isPresent()) {
			atualiza(Usuario);
		} else {
			adiciona(Usuario);
		}

		return Usuario;
	}

	public Usuario update(Usuario Usuario) {
		return atualiza(Usuario);
	}

	public Optional<Usuario> recupera(Long id) {
		return busca(id);
	}

	@Override
	protected Class<Usuario> getEntityClass() {
		return Usuario.class;
	}

	public Usuario recuperaUser(String login, String senha) {
		List<Usuario> lista = getLista();
		//TODO: Resolver isso realmente ta bem feio
		lista = lista.stream().filter(Usuario -> Usuario.getLogin().equalsIgnoreCase(login) && Usuario.getSenha().equalsIgnoreCase(senha))
		.collect(Collectors.toList());
		
		if (lista.isEmpty()){
			return null;
		}
		return lista.get(0);
	}
}