package br.com.senior.agroinfo.service;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import br.com.senior.agroinfo.model.Cooperativa;
import br.com.senior.agroinfo.model.DAO.CooperadoDAO;
import br.com.senior.agroinfo.model.DAO.CooperativaDAO;

public class BuscaInfoCooperativas {
	
	@Inject
	private CooperativaDAO repository;

	private Logger logger = Logger.getLogger("com.sun.tutorial.javaee.ejb.timersession.TimerSessionBean");
	
	public List<Cooperativa> buscaCooperativas() {
		List<Cooperativa> cooperativas = repository.getLista();
		if (cooperativas == null || cooperativas.isEmpty()) {
			return null;
		}
		return cooperativas;
	}
	
	public void buscar(){
		List<Cooperativa> cooperativas = buscaCooperativas();
		if (cooperativas == null){
			logger.info("não foram encontradas cooperativas");
			return;
		}
		for (Cooperativa cooperativa : cooperativas) {
			logger.info("Consultando informação da cooperativa"+cooperativa);
			
		}
	}
}
