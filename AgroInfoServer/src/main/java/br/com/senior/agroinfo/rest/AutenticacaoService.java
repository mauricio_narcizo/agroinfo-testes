package br.com.senior.agroinfo.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

import br.com.senior.agroinfo.model.Usuario;
import br.com.senior.agroinfo.model.DAO.UsuarioDAO;
import br.com.senior.agroinfo.service.security.JWTUtil;

@Path("/autenticacao")
@RequestScoped
public class AutenticacaoService {

	@Inject
	UsuarioDAO usuarioDAO;

	private Gson gson = new Gson();
	
	@GET
	@Path("usuario")
	public void autenticar(@QueryParam("auth") String auth,@Context final HttpServletResponse response) throws IOException {
		String login = auth.substring(0, auth.indexOf("||"));
		String senha = auth.substring(auth.indexOf("||"));
		try {
			login = new String(Base64.decodeBase64(login), "UTF-8");
			senha = new String(Base64.decodeBase64(senha), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Usuario cliente = usuarioDAO.recuperaUser(login.toLowerCase(), senha);
		if (cliente != null){
			String token = JWTUtil.createToken(cliente.getLogin());
			response.setHeader("authorization", token);
			response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().print(token);
		} else {
			String jsonStr = "{\"msg\": \"Usuario ou senha incorreta.\"}";
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().print(jsonStr);
		}
	}
	
	@GET
	private Usuario getUserFromRequest(HttpServletRequest request)
			throws ServletException, IOException {

		BufferedReader reader = request.getReader();
		return gson.fromJson(reader, Usuario.class);
	}
	

	@GET
	@Path("sair")
	@Produces(MediaType.APPLICATION_JSON)
	public void doGet(@Context final HttpServletResponse response)
			throws IOException {
		response.setCharacterEncoding("utf-8");
		String jsonStr = "{\"msg\": \"Saindo!\"}";
		response.getWriter().print(jsonStr);
	}

}
