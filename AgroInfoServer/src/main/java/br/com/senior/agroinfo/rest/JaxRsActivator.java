/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.senior.agroinfo.rest;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import br.com.senior.agroinfo.service.security.JWTUtil;



/**
 * A class extending {@link Application} and annotated with @ApplicationPath is
 * the Java EE 7 "no XML" approach to activating JAX-RS.
 *
 * <p>
 * Resources are served relative to the servlet path specified in the
 * {@link ApplicationPath} annotation.
 * </p>
 */
@WebFilter("/rest/*")
@ApplicationPath("/rest")
public class JaxRsActivator extends Application implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		httpResponse.addHeader("Access-Control-Allow-Origin", "*");
		httpResponse.addHeader("Access-Control-Expose-Headers","Authorization, Link");
		httpResponse.addHeader("Access-Control-Allow-Methods", "POST, GET");
		httpResponse.addHeader("Access-Control-Max-Age", "3600");
		httpResponse.addHeader("Access-Control-Allow-Credentials", "true");
		httpResponse.addHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
		if (httpRequest.getPathInfo().startsWith("/autenticacao/usuario")){
			chain.doFilter(request, response);
			return;
		}
		HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper(
				httpRequest) {
			@Override
			public String getHeader(String name) {
				final String value = request.getParameter(name);
				if (value != null) {
					return value;
				}
				return super.getHeader(name);
			}
		};

		String token = wrapper.getHeader("authorization");
		System.out.println("tipo"+request.getContentType());   
		Enumeration<String> headerNames = httpRequest.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = httpRequest.getHeader(headerName);
			System.out.println("Header Name: <em>" + headerName+":"+ headerValue);
		}
		if(true){ //TODO: remover isso para validar
		chain.doFilter(wrapper, response);
		return;
		}
		if (token != null) {
			Map<String, Object> decode = JWTUtil.decode(token);
			decode.forEach((k, v) -> System.out.printf(k, v));
			chain.doFilter(wrapper, response);	
		} else {
			sendError(httpResponse);
		}
	}
		
		
		
		
		

		private void sendError(HttpServletResponse response)
				throws ServletException, IOException {
			sendError(response, "Unauthorized");
		}

		private void sendError(HttpServletResponse response, String msg)
				throws ServletException, IOException {
			String jsonStr = "{\"msg\": \"" + msg + "\"}";

			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.getWriter().print(jsonStr);
		}
		
		
		
		@Override
		public void destroy() {
		}	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}

}
