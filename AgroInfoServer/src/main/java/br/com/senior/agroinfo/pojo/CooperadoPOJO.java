package br.com.senior.agroinfo.pojo;

public class CooperadoPOJO {

	private Long id;
	private String cpfCnpj;
	private Boolean atualizar;

	/**
	 * 
	 * @return The id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            The id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return The cpfCnpj
	 */
	public String getCpfCnpj() {
		return cpfCnpj;
	}

	/**
	 * 
	 * @param cpfCnpj
	 *            The cpfCnpj
	 */
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	/**
	 * 
	 * @return The atualizar
	 */
	public Boolean getAtualizar() {
		return atualizar;
	}

	/**
	 * 
	 * @param atualizar
	 *            The atualizar
	 */
	public void setAtualizar(Boolean atualizar) {
		this.atualizar = atualizar;
	}

}