package br.com.senior.agroinfo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.gson.Gson;

import br.com.senior.agroinfo.model.Cooperado;
import br.com.senior.agroinfo.model.Cooperativa;
import br.com.senior.agroinfo.model.DAO.CooperadoDAO;
import br.com.senior.agroinfo.model.DAO.CooperativaDAO;
import br.com.senior.agroinfo.pojo.CooperadoPOJO;

public class BuscaCooperados implements Job {

	@Inject
	private CooperativaDAO cooperativaDAO;

	@Inject
	private CooperadoDAO cooperadoDAO;

	private Logger logger = Logger.getLogger("com.sun.tutorial.javaee.ejb.timersession.TimerSessionBean");

	public List<Cooperativa> buscaCooperativas() {
		List<Cooperativa> cooperativas = cooperativaDAO.getLista();
		if (cooperativas == null || cooperativas.isEmpty()) {
			return null;
		}
		return cooperativas;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		List<Cooperativa> cooperativas = buscaCooperativas();
		if (cooperativas == null) {
			logger.info("não foram encontradas cooperativas");
			return;
		}
		for (Cooperativa cooperativa : cooperativas) {
			logger.info("Consultando informação da cooperativa" + cooperativa.getName());

			try {
				Client client = ClientBuilder.newClient();
				System.out.println(cooperativa.getIp());
				Response res = client.target(cooperativa.getIp() + "/AgroInfoMiddleware/api/cooperado")
						.request("application/json").get();

				String retorno = res.readEntity(String.class);
				System.out.println("retorno"+ retorno);
				CooperadoPOJO[] pojo = new Gson().fromJson(retorno, CooperadoPOJO[].class);
				for (CooperadoPOJO cooperadoPOJO : pojo) {
					if (cooperadoPOJO.getCpfCnpj() == null) {
						continue;
					}
					Optional<Cooperado> cooperado = cooperadoDAO.getCooperadosPorCnpj(cooperadoPOJO.getCpfCnpj());
					if (!cooperado.isPresent()) {
						Cooperado coop = new Cooperado();
						coop.setCpfCnpj(cooperadoPOJO.getCpfCnpj());
						coop.setId(cooperadoPOJO.getId());

						List<Cooperativa> listCooperativas = new ArrayList<Cooperativa>();
						listCooperativas.add(cooperativa);
						coop.setCooperativa(listCooperativas);

						cooperado = Optional.of(cooperadoDAO.create(coop));
					}
					if (cooperado.get().getCooperativa() == null || cooperado.get().getCooperativa().isEmpty()) {
						List<Cooperativa> coop = new ArrayList<Cooperativa>();
						coop.add(cooperativa);
						cooperado.get().setCooperativa(coop);
						cooperadoDAO.update(cooperado.get());

					} else if (!cooperado.get().getCooperativa().contains(cooperativa)) {
						cooperado.get().getCooperativa().add(cooperativa);
						cooperadoDAO.update(cooperado.get());
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				// System.out.println(
				// "não foi possivel conectar no ip " + cooperativa.getIp() +
				// "/AgroInfoMiddleware/api/cooperado");
			}

		}
	}
}
