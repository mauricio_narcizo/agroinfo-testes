package br.com.senior.agroinfo.model.DAO;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.senior.agroinfo.model.Cooperativa;

public class CooperativaDAO extends GenericDAO<Cooperativa> {

	public Cooperativa delete(Cooperativa Cooperativa) {
		Optional<Cooperativa> optional = busca(Cooperativa.getId());
		optional.ifPresent(coop -> remove(coop));
		return Cooperativa;
	}

	public Cooperativa create(Cooperativa Cooperativa) {

		Optional<Cooperativa> busca = busca(Cooperativa.getId());
		if (busca.isPresent()) {
			atualiza(Cooperativa);
		} else {
			adiciona(Cooperativa);
		}

		return Cooperativa;
	}

	public Cooperativa update(Cooperativa Cooperativa) {
		return atualiza(Cooperativa);
	}

	public Optional<Cooperativa> recupera(Long id) {
		return busca(id);
	}

	@Override
	protected Class<Cooperativa> getEntityClass() {
		return Cooperativa.class;
	}

	public List<Cooperativa> getCooperativas() {
		return getLista();
	}

	public List<Cooperativa> getCooperativasCnpj(String Cnpj) {
		List<Cooperativa> lista = getLista();
		return lista.stream().filter(Cooperativa -> Cooperativa.getCnpj().equalsIgnoreCase(Cnpj))
				.collect(Collectors.toList());
	}
}