package br.com.senior.agroinfo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class logAcesso implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7474207052147300445L;

	@Id
	@GeneratedValue
	private Long id;
	
	private Cooperado cooperado;

	private Date acesso;
	private String os;
	private boolean sms;
	private boolean mobile;

	public Date getAcesso() {
		return acesso;
	}

	public void setAcesso(Date acesso) {
		this.acesso = acesso;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public boolean isSms() {
		return sms;
	}

	public void setSms(boolean sms) {
		this.sms = sms;
	}

	public boolean isMobile() {
		return mobile;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cooperado getCooperado() {
		return cooperado;
	}

	public void setCooperado(Cooperado cooperado) {
		this.cooperado = cooperado;
	}

}
