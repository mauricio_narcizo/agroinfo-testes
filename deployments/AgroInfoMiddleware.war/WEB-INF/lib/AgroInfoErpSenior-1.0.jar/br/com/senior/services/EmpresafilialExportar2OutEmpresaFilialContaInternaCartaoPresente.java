/**
 * EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente  implements java.io.Serializable {
    private java.lang.String desCco;

    private java.lang.String indPdv;

    private java.lang.String numCco;

    public EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente() {
    }

    public EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente(
           java.lang.String desCco,
           java.lang.String indPdv,
           java.lang.String numCco) {
           this.desCco = desCco;
           this.indPdv = indPdv;
           this.numCco = numCco;
    }


    /**
     * Gets the desCco value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @return desCco
     */
    public java.lang.String getDesCco() {
        return desCco;
    }


    /**
     * Sets the desCco value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @param desCco
     */
    public void setDesCco(java.lang.String desCco) {
        this.desCco = desCco;
    }


    /**
     * Gets the indPdv value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @return indPdv
     */
    public java.lang.String getIndPdv() {
        return indPdv;
    }


    /**
     * Sets the indPdv value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @param indPdv
     */
    public void setIndPdv(java.lang.String indPdv) {
        this.indPdv = indPdv;
    }


    /**
     * Gets the numCco value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @return numCco
     */
    public java.lang.String getNumCco() {
        return numCco;
    }


    /**
     * Sets the numCco value for this EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.
     * 
     * @param numCco
     */
    public void setNumCco(java.lang.String numCco) {
        this.numCco = numCco;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente)) return false;
        EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente other = (EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.desCco==null && other.getDesCco()==null) || 
             (this.desCco!=null &&
              this.desCco.equals(other.getDesCco()))) &&
            ((this.indPdv==null && other.getIndPdv()==null) || 
             (this.indPdv!=null &&
              this.indPdv.equals(other.getIndPdv()))) &&
            ((this.numCco==null && other.getNumCco()==null) || 
             (this.numCco!=null &&
              this.numCco.equals(other.getNumCco())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDesCco() != null) {
            _hashCode += getDesCco().hashCode();
        }
        if (getIndPdv() != null) {
            _hashCode += getIndPdv().hashCode();
        }
        if (getNumCco() != null) {
            _hashCode += getNumCco().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
