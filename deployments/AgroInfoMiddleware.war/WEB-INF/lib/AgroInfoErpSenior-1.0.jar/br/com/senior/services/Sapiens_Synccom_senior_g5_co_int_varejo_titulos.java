/**
 * Sapiens_Synccom_senior_g5_co_int_varejo_titulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Sapiens_Synccom_senior_g5_co_int_varejo_titulos extends java.rmi.Remote {
    public br.com.senior.services.TitulosConsultarTitulosCPOut consultarTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosConsultarTitulosCPIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosGravarTitulosCPVarejoOut gravarTitulosCPVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosGravarTitulosCPVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarTitulosCPOut exportarTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarTitulosCPIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosConsultarTitulosCP2Out consultarTitulosCP_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosConsultarTitulosCP2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosBaixarTitulosCPVarejoOut baixarTitulosCPVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosBaixarTitulosCPVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosBaixarTitulosCPVarejo2Out baixarTitulosCPVarejo_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosBaixarTitulosCPVarejo2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarBaixaTitulosCPOut exportarBaixaTitulosCP(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarBaixaTitulosCPIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosSubstituirTitulosCPVarejoOut substituirTitulosCPVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosSubstituirTitulosCPVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosSubstituirTitulosCRVarejoOut substituirTitulosCRVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosSubstituirTitulosCRVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosAproveitamentoCreditoCPVarejoOut aproveitamentoCreditoCPVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosAproveitamentoCreditoCPVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOut baixaCompensacaoCPCRVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarTitulosReceberOut exportarTitulosReceber(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarTitulosReceberIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarTitulosReceber2Out exportarTitulosReceber_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarTitulosReceber2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarTitulosReceber3Out exportarTitulosReceber_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarTitulosReceber3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosImportarTitulosReceberOut importarTitulosReceber(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosImportarTitulosReceberIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosImportarTitulosReceber2Out importarTitulosReceber_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosImportarTitulosReceber2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosExportarBaixaTitulosReceberOut exportarBaixaTitulosReceber(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosExportarBaixaTitulosReceberIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosImportarBaixaTitulosReceberOut importarBaixaTitulosReceber(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosImportarBaixaTitulosReceberIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosAlterarTitulosReceberOut alterarTitulosReceber(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosAlterarTitulosReceberIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosAlterarTitulosReceber2Out alterarTitulosReceber_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosAlterarTitulosReceber2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosAlterarTitulosPagarOut alterarTitulosPagar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosAlterarTitulosPagarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosEstornoBaixaTitulosCRVarejoOut estornoBaixaTitulosCRVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosEstornoBaixaTitulosCRVarejoIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoOut estornoBaixaTitulosCPVarejo(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.TitulosEstornoBaixaTitulosCPVarejoIn parameters) throws java.rmi.RemoteException;
}
