/**
 * FornecedoresImportarInFornecedor.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresImportarInFornecedor  implements java.io.Serializable {
    private java.lang.String apeFor;

    private java.lang.String baiFor;

    private java.lang.Integer cepFor;

    private java.lang.Double cgcCpf;

    private java.lang.String cidFor;

    private java.lang.String cliFor;

    private java.lang.Integer codCli;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.Integer codMot;

    private java.lang.String codPai;

    private java.lang.String codRam;

    private java.lang.String codRoe;

    private java.lang.Integer codRtr;

    private java.lang.String codSro;

    private java.lang.String codSuf;

    private java.lang.String codTri;

    private java.lang.String cplEnd;

    private java.lang.Integer cxaPst;

    private java.lang.String datAtu;

    private java.lang.String datCad;

    private br.com.senior.services.FornecedoresImportarInFornecedorDefinicao[] definicao;

    private java.lang.String emaNfe;

    private java.lang.String endFor;

    private java.lang.String faxFor;

    private java.lang.String fonFo2;

    private java.lang.String fonFo3;

    private java.lang.String fonFor;

    private java.lang.Integer forRep;

    private java.lang.Integer forTra;

    private java.lang.String horAtu;

    private java.lang.String horCad;

    private java.lang.Integer ideExt;

    private java.lang.String indFor;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.String limRet;

    private java.lang.String nenFor;

    private java.lang.String nomFor;

    private java.lang.Double notFor;

    private java.lang.Double notSis;

    private java.lang.String numIdf;

    private java.lang.String numRge;

    private java.lang.String obsMot;

    private java.lang.Double perCod;

    private java.lang.Double perIcm;

    private java.lang.Double perPid;

    private java.lang.Double perRin;

    private java.lang.Double perRir;

    private java.lang.String recCof;

    private java.lang.String recIcm;

    private java.lang.String recIpi;

    private java.lang.String recPis;

    private java.lang.Integer regEst;

    private java.lang.String retCof;

    private java.lang.String retCsl;

    private java.lang.String retIrf;

    private java.lang.String retOur;

    private java.lang.String retPis;

    private java.lang.String retPro;

    private java.lang.Integer seqRoe;

    private java.lang.String sigUfs;

    private java.lang.String sitFor;

    private java.lang.String tipFor;

    private java.lang.String tipMer;

    private java.lang.String triIcm;

    private java.lang.String triIpi;

    private java.lang.String triIss;

    private java.lang.Double usuAtu;

    private java.lang.Double usuCad;

    private java.lang.String zipCod;

    public FornecedoresImportarInFornecedor() {
    }

    public FornecedoresImportarInFornecedor(
           java.lang.String apeFor,
           java.lang.String baiFor,
           java.lang.Integer cepFor,
           java.lang.Double cgcCpf,
           java.lang.String cidFor,
           java.lang.String cliFor,
           java.lang.Integer codCli,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.Integer codMot,
           java.lang.String codPai,
           java.lang.String codRam,
           java.lang.String codRoe,
           java.lang.Integer codRtr,
           java.lang.String codSro,
           java.lang.String codSuf,
           java.lang.String codTri,
           java.lang.String cplEnd,
           java.lang.Integer cxaPst,
           java.lang.String datAtu,
           java.lang.String datCad,
           br.com.senior.services.FornecedoresImportarInFornecedorDefinicao[] definicao,
           java.lang.String emaNfe,
           java.lang.String endFor,
           java.lang.String faxFor,
           java.lang.String fonFo2,
           java.lang.String fonFo3,
           java.lang.String fonFor,
           java.lang.Integer forRep,
           java.lang.Integer forTra,
           java.lang.String horAtu,
           java.lang.String horCad,
           java.lang.Integer ideExt,
           java.lang.String indFor,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.String limRet,
           java.lang.String nenFor,
           java.lang.String nomFor,
           java.lang.Double notFor,
           java.lang.Double notSis,
           java.lang.String numIdf,
           java.lang.String numRge,
           java.lang.String obsMot,
           java.lang.Double perCod,
           java.lang.Double perIcm,
           java.lang.Double perPid,
           java.lang.Double perRin,
           java.lang.Double perRir,
           java.lang.String recCof,
           java.lang.String recIcm,
           java.lang.String recIpi,
           java.lang.String recPis,
           java.lang.Integer regEst,
           java.lang.String retCof,
           java.lang.String retCsl,
           java.lang.String retIrf,
           java.lang.String retOur,
           java.lang.String retPis,
           java.lang.String retPro,
           java.lang.Integer seqRoe,
           java.lang.String sigUfs,
           java.lang.String sitFor,
           java.lang.String tipFor,
           java.lang.String tipMer,
           java.lang.String triIcm,
           java.lang.String triIpi,
           java.lang.String triIss,
           java.lang.Double usuAtu,
           java.lang.Double usuCad,
           java.lang.String zipCod) {
           this.apeFor = apeFor;
           this.baiFor = baiFor;
           this.cepFor = cepFor;
           this.cgcCpf = cgcCpf;
           this.cidFor = cidFor;
           this.cliFor = cliFor;
           this.codCli = codCli;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codMot = codMot;
           this.codPai = codPai;
           this.codRam = codRam;
           this.codRoe = codRoe;
           this.codRtr = codRtr;
           this.codSro = codSro;
           this.codSuf = codSuf;
           this.codTri = codTri;
           this.cplEnd = cplEnd;
           this.cxaPst = cxaPst;
           this.datAtu = datAtu;
           this.datCad = datCad;
           this.definicao = definicao;
           this.emaNfe = emaNfe;
           this.endFor = endFor;
           this.faxFor = faxFor;
           this.fonFo2 = fonFo2;
           this.fonFo3 = fonFo3;
           this.fonFor = fonFor;
           this.forRep = forRep;
           this.forTra = forTra;
           this.horAtu = horAtu;
           this.horCad = horCad;
           this.ideExt = ideExt;
           this.indFor = indFor;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.limRet = limRet;
           this.nenFor = nenFor;
           this.nomFor = nomFor;
           this.notFor = notFor;
           this.notSis = notSis;
           this.numIdf = numIdf;
           this.numRge = numRge;
           this.obsMot = obsMot;
           this.perCod = perCod;
           this.perIcm = perIcm;
           this.perPid = perPid;
           this.perRin = perRin;
           this.perRir = perRir;
           this.recCof = recCof;
           this.recIcm = recIcm;
           this.recIpi = recIpi;
           this.recPis = recPis;
           this.regEst = regEst;
           this.retCof = retCof;
           this.retCsl = retCsl;
           this.retIrf = retIrf;
           this.retOur = retOur;
           this.retPis = retPis;
           this.retPro = retPro;
           this.seqRoe = seqRoe;
           this.sigUfs = sigUfs;
           this.sitFor = sitFor;
           this.tipFor = tipFor;
           this.tipMer = tipMer;
           this.triIcm = triIcm;
           this.triIpi = triIpi;
           this.triIss = triIss;
           this.usuAtu = usuAtu;
           this.usuCad = usuCad;
           this.zipCod = zipCod;
    }


    /**
     * Gets the apeFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return apeFor
     */
    public java.lang.String getApeFor() {
        return apeFor;
    }


    /**
     * Sets the apeFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param apeFor
     */
    public void setApeFor(java.lang.String apeFor) {
        this.apeFor = apeFor;
    }


    /**
     * Gets the baiFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return baiFor
     */
    public java.lang.String getBaiFor() {
        return baiFor;
    }


    /**
     * Sets the baiFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param baiFor
     */
    public void setBaiFor(java.lang.String baiFor) {
        this.baiFor = baiFor;
    }


    /**
     * Gets the cepFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return cepFor
     */
    public java.lang.Integer getCepFor() {
        return cepFor;
    }


    /**
     * Sets the cepFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param cepFor
     */
    public void setCepFor(java.lang.Integer cepFor) {
        this.cepFor = cepFor;
    }


    /**
     * Gets the cgcCpf value for this FornecedoresImportarInFornecedor.
     * 
     * @return cgcCpf
     */
    public java.lang.Double getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this FornecedoresImportarInFornecedor.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.Double cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the cidFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return cidFor
     */
    public java.lang.String getCidFor() {
        return cidFor;
    }


    /**
     * Sets the cidFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param cidFor
     */
    public void setCidFor(java.lang.String cidFor) {
        this.cidFor = cidFor;
    }


    /**
     * Gets the cliFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return cliFor
     */
    public java.lang.String getCliFor() {
        return cliFor;
    }


    /**
     * Sets the cliFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param cliFor
     */
    public void setCliFor(java.lang.String cliFor) {
        this.cliFor = cliFor;
    }


    /**
     * Gets the codCli value for this FornecedoresImportarInFornecedor.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this FornecedoresImportarInFornecedor.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codEmp value for this FornecedoresImportarInFornecedor.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this FornecedoresImportarInFornecedor.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this FornecedoresImportarInFornecedor.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FornecedoresImportarInFornecedor.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codMot value for this FornecedoresImportarInFornecedor.
     * 
     * @return codMot
     */
    public java.lang.Integer getCodMot() {
        return codMot;
    }


    /**
     * Sets the codMot value for this FornecedoresImportarInFornecedor.
     * 
     * @param codMot
     */
    public void setCodMot(java.lang.Integer codMot) {
        this.codMot = codMot;
    }


    /**
     * Gets the codPai value for this FornecedoresImportarInFornecedor.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this FornecedoresImportarInFornecedor.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codRam value for this FornecedoresImportarInFornecedor.
     * 
     * @return codRam
     */
    public java.lang.String getCodRam() {
        return codRam;
    }


    /**
     * Sets the codRam value for this FornecedoresImportarInFornecedor.
     * 
     * @param codRam
     */
    public void setCodRam(java.lang.String codRam) {
        this.codRam = codRam;
    }


    /**
     * Gets the codRoe value for this FornecedoresImportarInFornecedor.
     * 
     * @return codRoe
     */
    public java.lang.String getCodRoe() {
        return codRoe;
    }


    /**
     * Sets the codRoe value for this FornecedoresImportarInFornecedor.
     * 
     * @param codRoe
     */
    public void setCodRoe(java.lang.String codRoe) {
        this.codRoe = codRoe;
    }


    /**
     * Gets the codRtr value for this FornecedoresImportarInFornecedor.
     * 
     * @return codRtr
     */
    public java.lang.Integer getCodRtr() {
        return codRtr;
    }


    /**
     * Sets the codRtr value for this FornecedoresImportarInFornecedor.
     * 
     * @param codRtr
     */
    public void setCodRtr(java.lang.Integer codRtr) {
        this.codRtr = codRtr;
    }


    /**
     * Gets the codSro value for this FornecedoresImportarInFornecedor.
     * 
     * @return codSro
     */
    public java.lang.String getCodSro() {
        return codSro;
    }


    /**
     * Sets the codSro value for this FornecedoresImportarInFornecedor.
     * 
     * @param codSro
     */
    public void setCodSro(java.lang.String codSro) {
        this.codSro = codSro;
    }


    /**
     * Gets the codSuf value for this FornecedoresImportarInFornecedor.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this FornecedoresImportarInFornecedor.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the codTri value for this FornecedoresImportarInFornecedor.
     * 
     * @return codTri
     */
    public java.lang.String getCodTri() {
        return codTri;
    }


    /**
     * Sets the codTri value for this FornecedoresImportarInFornecedor.
     * 
     * @param codTri
     */
    public void setCodTri(java.lang.String codTri) {
        this.codTri = codTri;
    }


    /**
     * Gets the cplEnd value for this FornecedoresImportarInFornecedor.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this FornecedoresImportarInFornecedor.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the cxaPst value for this FornecedoresImportarInFornecedor.
     * 
     * @return cxaPst
     */
    public java.lang.Integer getCxaPst() {
        return cxaPst;
    }


    /**
     * Sets the cxaPst value for this FornecedoresImportarInFornecedor.
     * 
     * @param cxaPst
     */
    public void setCxaPst(java.lang.Integer cxaPst) {
        this.cxaPst = cxaPst;
    }


    /**
     * Gets the datAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @return datAtu
     */
    public java.lang.String getDatAtu() {
        return datAtu;
    }


    /**
     * Sets the datAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @param datAtu
     */
    public void setDatAtu(java.lang.String datAtu) {
        this.datAtu = datAtu;
    }


    /**
     * Gets the datCad value for this FornecedoresImportarInFornecedor.
     * 
     * @return datCad
     */
    public java.lang.String getDatCad() {
        return datCad;
    }


    /**
     * Sets the datCad value for this FornecedoresImportarInFornecedor.
     * 
     * @param datCad
     */
    public void setDatCad(java.lang.String datCad) {
        this.datCad = datCad;
    }


    /**
     * Gets the definicao value for this FornecedoresImportarInFornecedor.
     * 
     * @return definicao
     */
    public br.com.senior.services.FornecedoresImportarInFornecedorDefinicao[] getDefinicao() {
        return definicao;
    }


    /**
     * Sets the definicao value for this FornecedoresImportarInFornecedor.
     * 
     * @param definicao
     */
    public void setDefinicao(br.com.senior.services.FornecedoresImportarInFornecedorDefinicao[] definicao) {
        this.definicao = definicao;
    }

    public br.com.senior.services.FornecedoresImportarInFornecedorDefinicao getDefinicao(int i) {
        return this.definicao[i];
    }

    public void setDefinicao(int i, br.com.senior.services.FornecedoresImportarInFornecedorDefinicao _value) {
        this.definicao[i] = _value;
    }


    /**
     * Gets the emaNfe value for this FornecedoresImportarInFornecedor.
     * 
     * @return emaNfe
     */
    public java.lang.String getEmaNfe() {
        return emaNfe;
    }


    /**
     * Sets the emaNfe value for this FornecedoresImportarInFornecedor.
     * 
     * @param emaNfe
     */
    public void setEmaNfe(java.lang.String emaNfe) {
        this.emaNfe = emaNfe;
    }


    /**
     * Gets the endFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return endFor
     */
    public java.lang.String getEndFor() {
        return endFor;
    }


    /**
     * Sets the endFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param endFor
     */
    public void setEndFor(java.lang.String endFor) {
        this.endFor = endFor;
    }


    /**
     * Gets the faxFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return faxFor
     */
    public java.lang.String getFaxFor() {
        return faxFor;
    }


    /**
     * Sets the faxFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param faxFor
     */
    public void setFaxFor(java.lang.String faxFor) {
        this.faxFor = faxFor;
    }


    /**
     * Gets the fonFo2 value for this FornecedoresImportarInFornecedor.
     * 
     * @return fonFo2
     */
    public java.lang.String getFonFo2() {
        return fonFo2;
    }


    /**
     * Sets the fonFo2 value for this FornecedoresImportarInFornecedor.
     * 
     * @param fonFo2
     */
    public void setFonFo2(java.lang.String fonFo2) {
        this.fonFo2 = fonFo2;
    }


    /**
     * Gets the fonFo3 value for this FornecedoresImportarInFornecedor.
     * 
     * @return fonFo3
     */
    public java.lang.String getFonFo3() {
        return fonFo3;
    }


    /**
     * Sets the fonFo3 value for this FornecedoresImportarInFornecedor.
     * 
     * @param fonFo3
     */
    public void setFonFo3(java.lang.String fonFo3) {
        this.fonFo3 = fonFo3;
    }


    /**
     * Gets the fonFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return fonFor
     */
    public java.lang.String getFonFor() {
        return fonFor;
    }


    /**
     * Sets the fonFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param fonFor
     */
    public void setFonFor(java.lang.String fonFor) {
        this.fonFor = fonFor;
    }


    /**
     * Gets the forRep value for this FornecedoresImportarInFornecedor.
     * 
     * @return forRep
     */
    public java.lang.Integer getForRep() {
        return forRep;
    }


    /**
     * Sets the forRep value for this FornecedoresImportarInFornecedor.
     * 
     * @param forRep
     */
    public void setForRep(java.lang.Integer forRep) {
        this.forRep = forRep;
    }


    /**
     * Gets the forTra value for this FornecedoresImportarInFornecedor.
     * 
     * @return forTra
     */
    public java.lang.Integer getForTra() {
        return forTra;
    }


    /**
     * Sets the forTra value for this FornecedoresImportarInFornecedor.
     * 
     * @param forTra
     */
    public void setForTra(java.lang.Integer forTra) {
        this.forTra = forTra;
    }


    /**
     * Gets the horAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @return horAtu
     */
    public java.lang.String getHorAtu() {
        return horAtu;
    }


    /**
     * Sets the horAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @param horAtu
     */
    public void setHorAtu(java.lang.String horAtu) {
        this.horAtu = horAtu;
    }


    /**
     * Gets the horCad value for this FornecedoresImportarInFornecedor.
     * 
     * @return horCad
     */
    public java.lang.String getHorCad() {
        return horCad;
    }


    /**
     * Sets the horCad value for this FornecedoresImportarInFornecedor.
     * 
     * @param horCad
     */
    public void setHorCad(java.lang.String horCad) {
        this.horCad = horCad;
    }


    /**
     * Gets the ideExt value for this FornecedoresImportarInFornecedor.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this FornecedoresImportarInFornecedor.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the indFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return indFor
     */
    public java.lang.String getIndFor() {
        return indFor;
    }


    /**
     * Sets the indFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param indFor
     */
    public void setIndFor(java.lang.String indFor) {
        this.indFor = indFor;
    }


    /**
     * Gets the insEst value for this FornecedoresImportarInFornecedor.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this FornecedoresImportarInFornecedor.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this FornecedoresImportarInFornecedor.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this FornecedoresImportarInFornecedor.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this FornecedoresImportarInFornecedor.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this FornecedoresImportarInFornecedor.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the limRet value for this FornecedoresImportarInFornecedor.
     * 
     * @return limRet
     */
    public java.lang.String getLimRet() {
        return limRet;
    }


    /**
     * Sets the limRet value for this FornecedoresImportarInFornecedor.
     * 
     * @param limRet
     */
    public void setLimRet(java.lang.String limRet) {
        this.limRet = limRet;
    }


    /**
     * Gets the nenFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return nenFor
     */
    public java.lang.String getNenFor() {
        return nenFor;
    }


    /**
     * Sets the nenFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param nenFor
     */
    public void setNenFor(java.lang.String nenFor) {
        this.nenFor = nenFor;
    }


    /**
     * Gets the nomFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return nomFor
     */
    public java.lang.String getNomFor() {
        return nomFor;
    }


    /**
     * Sets the nomFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param nomFor
     */
    public void setNomFor(java.lang.String nomFor) {
        this.nomFor = nomFor;
    }


    /**
     * Gets the notFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return notFor
     */
    public java.lang.Double getNotFor() {
        return notFor;
    }


    /**
     * Sets the notFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param notFor
     */
    public void setNotFor(java.lang.Double notFor) {
        this.notFor = notFor;
    }


    /**
     * Gets the notSis value for this FornecedoresImportarInFornecedor.
     * 
     * @return notSis
     */
    public java.lang.Double getNotSis() {
        return notSis;
    }


    /**
     * Sets the notSis value for this FornecedoresImportarInFornecedor.
     * 
     * @param notSis
     */
    public void setNotSis(java.lang.Double notSis) {
        this.notSis = notSis;
    }


    /**
     * Gets the numIdf value for this FornecedoresImportarInFornecedor.
     * 
     * @return numIdf
     */
    public java.lang.String getNumIdf() {
        return numIdf;
    }


    /**
     * Sets the numIdf value for this FornecedoresImportarInFornecedor.
     * 
     * @param numIdf
     */
    public void setNumIdf(java.lang.String numIdf) {
        this.numIdf = numIdf;
    }


    /**
     * Gets the numRge value for this FornecedoresImportarInFornecedor.
     * 
     * @return numRge
     */
    public java.lang.String getNumRge() {
        return numRge;
    }


    /**
     * Sets the numRge value for this FornecedoresImportarInFornecedor.
     * 
     * @param numRge
     */
    public void setNumRge(java.lang.String numRge) {
        this.numRge = numRge;
    }


    /**
     * Gets the obsMot value for this FornecedoresImportarInFornecedor.
     * 
     * @return obsMot
     */
    public java.lang.String getObsMot() {
        return obsMot;
    }


    /**
     * Sets the obsMot value for this FornecedoresImportarInFornecedor.
     * 
     * @param obsMot
     */
    public void setObsMot(java.lang.String obsMot) {
        this.obsMot = obsMot;
    }


    /**
     * Gets the perCod value for this FornecedoresImportarInFornecedor.
     * 
     * @return perCod
     */
    public java.lang.Double getPerCod() {
        return perCod;
    }


    /**
     * Sets the perCod value for this FornecedoresImportarInFornecedor.
     * 
     * @param perCod
     */
    public void setPerCod(java.lang.Double perCod) {
        this.perCod = perCod;
    }


    /**
     * Gets the perIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @return perIcm
     */
    public java.lang.Double getPerIcm() {
        return perIcm;
    }


    /**
     * Sets the perIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @param perIcm
     */
    public void setPerIcm(java.lang.Double perIcm) {
        this.perIcm = perIcm;
    }


    /**
     * Gets the perPid value for this FornecedoresImportarInFornecedor.
     * 
     * @return perPid
     */
    public java.lang.Double getPerPid() {
        return perPid;
    }


    /**
     * Sets the perPid value for this FornecedoresImportarInFornecedor.
     * 
     * @param perPid
     */
    public void setPerPid(java.lang.Double perPid) {
        this.perPid = perPid;
    }


    /**
     * Gets the perRin value for this FornecedoresImportarInFornecedor.
     * 
     * @return perRin
     */
    public java.lang.Double getPerRin() {
        return perRin;
    }


    /**
     * Sets the perRin value for this FornecedoresImportarInFornecedor.
     * 
     * @param perRin
     */
    public void setPerRin(java.lang.Double perRin) {
        this.perRin = perRin;
    }


    /**
     * Gets the perRir value for this FornecedoresImportarInFornecedor.
     * 
     * @return perRir
     */
    public java.lang.Double getPerRir() {
        return perRir;
    }


    /**
     * Sets the perRir value for this FornecedoresImportarInFornecedor.
     * 
     * @param perRir
     */
    public void setPerRir(java.lang.Double perRir) {
        this.perRir = perRir;
    }


    /**
     * Gets the recCof value for this FornecedoresImportarInFornecedor.
     * 
     * @return recCof
     */
    public java.lang.String getRecCof() {
        return recCof;
    }


    /**
     * Sets the recCof value for this FornecedoresImportarInFornecedor.
     * 
     * @param recCof
     */
    public void setRecCof(java.lang.String recCof) {
        this.recCof = recCof;
    }


    /**
     * Gets the recIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @return recIcm
     */
    public java.lang.String getRecIcm() {
        return recIcm;
    }


    /**
     * Sets the recIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @param recIcm
     */
    public void setRecIcm(java.lang.String recIcm) {
        this.recIcm = recIcm;
    }


    /**
     * Gets the recIpi value for this FornecedoresImportarInFornecedor.
     * 
     * @return recIpi
     */
    public java.lang.String getRecIpi() {
        return recIpi;
    }


    /**
     * Sets the recIpi value for this FornecedoresImportarInFornecedor.
     * 
     * @param recIpi
     */
    public void setRecIpi(java.lang.String recIpi) {
        this.recIpi = recIpi;
    }


    /**
     * Gets the recPis value for this FornecedoresImportarInFornecedor.
     * 
     * @return recPis
     */
    public java.lang.String getRecPis() {
        return recPis;
    }


    /**
     * Sets the recPis value for this FornecedoresImportarInFornecedor.
     * 
     * @param recPis
     */
    public void setRecPis(java.lang.String recPis) {
        this.recPis = recPis;
    }


    /**
     * Gets the regEst value for this FornecedoresImportarInFornecedor.
     * 
     * @return regEst
     */
    public java.lang.Integer getRegEst() {
        return regEst;
    }


    /**
     * Sets the regEst value for this FornecedoresImportarInFornecedor.
     * 
     * @param regEst
     */
    public void setRegEst(java.lang.Integer regEst) {
        this.regEst = regEst;
    }


    /**
     * Gets the retCof value for this FornecedoresImportarInFornecedor.
     * 
     * @return retCof
     */
    public java.lang.String getRetCof() {
        return retCof;
    }


    /**
     * Sets the retCof value for this FornecedoresImportarInFornecedor.
     * 
     * @param retCof
     */
    public void setRetCof(java.lang.String retCof) {
        this.retCof = retCof;
    }


    /**
     * Gets the retCsl value for this FornecedoresImportarInFornecedor.
     * 
     * @return retCsl
     */
    public java.lang.String getRetCsl() {
        return retCsl;
    }


    /**
     * Sets the retCsl value for this FornecedoresImportarInFornecedor.
     * 
     * @param retCsl
     */
    public void setRetCsl(java.lang.String retCsl) {
        this.retCsl = retCsl;
    }


    /**
     * Gets the retIrf value for this FornecedoresImportarInFornecedor.
     * 
     * @return retIrf
     */
    public java.lang.String getRetIrf() {
        return retIrf;
    }


    /**
     * Sets the retIrf value for this FornecedoresImportarInFornecedor.
     * 
     * @param retIrf
     */
    public void setRetIrf(java.lang.String retIrf) {
        this.retIrf = retIrf;
    }


    /**
     * Gets the retOur value for this FornecedoresImportarInFornecedor.
     * 
     * @return retOur
     */
    public java.lang.String getRetOur() {
        return retOur;
    }


    /**
     * Sets the retOur value for this FornecedoresImportarInFornecedor.
     * 
     * @param retOur
     */
    public void setRetOur(java.lang.String retOur) {
        this.retOur = retOur;
    }


    /**
     * Gets the retPis value for this FornecedoresImportarInFornecedor.
     * 
     * @return retPis
     */
    public java.lang.String getRetPis() {
        return retPis;
    }


    /**
     * Sets the retPis value for this FornecedoresImportarInFornecedor.
     * 
     * @param retPis
     */
    public void setRetPis(java.lang.String retPis) {
        this.retPis = retPis;
    }


    /**
     * Gets the retPro value for this FornecedoresImportarInFornecedor.
     * 
     * @return retPro
     */
    public java.lang.String getRetPro() {
        return retPro;
    }


    /**
     * Sets the retPro value for this FornecedoresImportarInFornecedor.
     * 
     * @param retPro
     */
    public void setRetPro(java.lang.String retPro) {
        this.retPro = retPro;
    }


    /**
     * Gets the seqRoe value for this FornecedoresImportarInFornecedor.
     * 
     * @return seqRoe
     */
    public java.lang.Integer getSeqRoe() {
        return seqRoe;
    }


    /**
     * Sets the seqRoe value for this FornecedoresImportarInFornecedor.
     * 
     * @param seqRoe
     */
    public void setSeqRoe(java.lang.Integer seqRoe) {
        this.seqRoe = seqRoe;
    }


    /**
     * Gets the sigUfs value for this FornecedoresImportarInFornecedor.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this FornecedoresImportarInFornecedor.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }


    /**
     * Gets the sitFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return sitFor
     */
    public java.lang.String getSitFor() {
        return sitFor;
    }


    /**
     * Sets the sitFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param sitFor
     */
    public void setSitFor(java.lang.String sitFor) {
        this.sitFor = sitFor;
    }


    /**
     * Gets the tipFor value for this FornecedoresImportarInFornecedor.
     * 
     * @return tipFor
     */
    public java.lang.String getTipFor() {
        return tipFor;
    }


    /**
     * Sets the tipFor value for this FornecedoresImportarInFornecedor.
     * 
     * @param tipFor
     */
    public void setTipFor(java.lang.String tipFor) {
        this.tipFor = tipFor;
    }


    /**
     * Gets the tipMer value for this FornecedoresImportarInFornecedor.
     * 
     * @return tipMer
     */
    public java.lang.String getTipMer() {
        return tipMer;
    }


    /**
     * Sets the tipMer value for this FornecedoresImportarInFornecedor.
     * 
     * @param tipMer
     */
    public void setTipMer(java.lang.String tipMer) {
        this.tipMer = tipMer;
    }


    /**
     * Gets the triIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @return triIcm
     */
    public java.lang.String getTriIcm() {
        return triIcm;
    }


    /**
     * Sets the triIcm value for this FornecedoresImportarInFornecedor.
     * 
     * @param triIcm
     */
    public void setTriIcm(java.lang.String triIcm) {
        this.triIcm = triIcm;
    }


    /**
     * Gets the triIpi value for this FornecedoresImportarInFornecedor.
     * 
     * @return triIpi
     */
    public java.lang.String getTriIpi() {
        return triIpi;
    }


    /**
     * Sets the triIpi value for this FornecedoresImportarInFornecedor.
     * 
     * @param triIpi
     */
    public void setTriIpi(java.lang.String triIpi) {
        this.triIpi = triIpi;
    }


    /**
     * Gets the triIss value for this FornecedoresImportarInFornecedor.
     * 
     * @return triIss
     */
    public java.lang.String getTriIss() {
        return triIss;
    }


    /**
     * Sets the triIss value for this FornecedoresImportarInFornecedor.
     * 
     * @param triIss
     */
    public void setTriIss(java.lang.String triIss) {
        this.triIss = triIss;
    }


    /**
     * Gets the usuAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @return usuAtu
     */
    public java.lang.Double getUsuAtu() {
        return usuAtu;
    }


    /**
     * Sets the usuAtu value for this FornecedoresImportarInFornecedor.
     * 
     * @param usuAtu
     */
    public void setUsuAtu(java.lang.Double usuAtu) {
        this.usuAtu = usuAtu;
    }


    /**
     * Gets the usuCad value for this FornecedoresImportarInFornecedor.
     * 
     * @return usuCad
     */
    public java.lang.Double getUsuCad() {
        return usuCad;
    }


    /**
     * Sets the usuCad value for this FornecedoresImportarInFornecedor.
     * 
     * @param usuCad
     */
    public void setUsuCad(java.lang.Double usuCad) {
        this.usuCad = usuCad;
    }


    /**
     * Gets the zipCod value for this FornecedoresImportarInFornecedor.
     * 
     * @return zipCod
     */
    public java.lang.String getZipCod() {
        return zipCod;
    }


    /**
     * Sets the zipCod value for this FornecedoresImportarInFornecedor.
     * 
     * @param zipCod
     */
    public void setZipCod(java.lang.String zipCod) {
        this.zipCod = zipCod;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresImportarInFornecedor)) return false;
        FornecedoresImportarInFornecedor other = (FornecedoresImportarInFornecedor) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apeFor==null && other.getApeFor()==null) || 
             (this.apeFor!=null &&
              this.apeFor.equals(other.getApeFor()))) &&
            ((this.baiFor==null && other.getBaiFor()==null) || 
             (this.baiFor!=null &&
              this.baiFor.equals(other.getBaiFor()))) &&
            ((this.cepFor==null && other.getCepFor()==null) || 
             (this.cepFor!=null &&
              this.cepFor.equals(other.getCepFor()))) &&
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.cidFor==null && other.getCidFor()==null) || 
             (this.cidFor!=null &&
              this.cidFor.equals(other.getCidFor()))) &&
            ((this.cliFor==null && other.getCliFor()==null) || 
             (this.cliFor!=null &&
              this.cliFor.equals(other.getCliFor()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codMot==null && other.getCodMot()==null) || 
             (this.codMot!=null &&
              this.codMot.equals(other.getCodMot()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codRam==null && other.getCodRam()==null) || 
             (this.codRam!=null &&
              this.codRam.equals(other.getCodRam()))) &&
            ((this.codRoe==null && other.getCodRoe()==null) || 
             (this.codRoe!=null &&
              this.codRoe.equals(other.getCodRoe()))) &&
            ((this.codRtr==null && other.getCodRtr()==null) || 
             (this.codRtr!=null &&
              this.codRtr.equals(other.getCodRtr()))) &&
            ((this.codSro==null && other.getCodSro()==null) || 
             (this.codSro!=null &&
              this.codSro.equals(other.getCodSro()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.codTri==null && other.getCodTri()==null) || 
             (this.codTri!=null &&
              this.codTri.equals(other.getCodTri()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.cxaPst==null && other.getCxaPst()==null) || 
             (this.cxaPst!=null &&
              this.cxaPst.equals(other.getCxaPst()))) &&
            ((this.datAtu==null && other.getDatAtu()==null) || 
             (this.datAtu!=null &&
              this.datAtu.equals(other.getDatAtu()))) &&
            ((this.datCad==null && other.getDatCad()==null) || 
             (this.datCad!=null &&
              this.datCad.equals(other.getDatCad()))) &&
            ((this.definicao==null && other.getDefinicao()==null) || 
             (this.definicao!=null &&
              java.util.Arrays.equals(this.definicao, other.getDefinicao()))) &&
            ((this.emaNfe==null && other.getEmaNfe()==null) || 
             (this.emaNfe!=null &&
              this.emaNfe.equals(other.getEmaNfe()))) &&
            ((this.endFor==null && other.getEndFor()==null) || 
             (this.endFor!=null &&
              this.endFor.equals(other.getEndFor()))) &&
            ((this.faxFor==null && other.getFaxFor()==null) || 
             (this.faxFor!=null &&
              this.faxFor.equals(other.getFaxFor()))) &&
            ((this.fonFo2==null && other.getFonFo2()==null) || 
             (this.fonFo2!=null &&
              this.fonFo2.equals(other.getFonFo2()))) &&
            ((this.fonFo3==null && other.getFonFo3()==null) || 
             (this.fonFo3!=null &&
              this.fonFo3.equals(other.getFonFo3()))) &&
            ((this.fonFor==null && other.getFonFor()==null) || 
             (this.fonFor!=null &&
              this.fonFor.equals(other.getFonFor()))) &&
            ((this.forRep==null && other.getForRep()==null) || 
             (this.forRep!=null &&
              this.forRep.equals(other.getForRep()))) &&
            ((this.forTra==null && other.getForTra()==null) || 
             (this.forTra!=null &&
              this.forTra.equals(other.getForTra()))) &&
            ((this.horAtu==null && other.getHorAtu()==null) || 
             (this.horAtu!=null &&
              this.horAtu.equals(other.getHorAtu()))) &&
            ((this.horCad==null && other.getHorCad()==null) || 
             (this.horCad!=null &&
              this.horCad.equals(other.getHorCad()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.indFor==null && other.getIndFor()==null) || 
             (this.indFor!=null &&
              this.indFor.equals(other.getIndFor()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.limRet==null && other.getLimRet()==null) || 
             (this.limRet!=null &&
              this.limRet.equals(other.getLimRet()))) &&
            ((this.nenFor==null && other.getNenFor()==null) || 
             (this.nenFor!=null &&
              this.nenFor.equals(other.getNenFor()))) &&
            ((this.nomFor==null && other.getNomFor()==null) || 
             (this.nomFor!=null &&
              this.nomFor.equals(other.getNomFor()))) &&
            ((this.notFor==null && other.getNotFor()==null) || 
             (this.notFor!=null &&
              this.notFor.equals(other.getNotFor()))) &&
            ((this.notSis==null && other.getNotSis()==null) || 
             (this.notSis!=null &&
              this.notSis.equals(other.getNotSis()))) &&
            ((this.numIdf==null && other.getNumIdf()==null) || 
             (this.numIdf!=null &&
              this.numIdf.equals(other.getNumIdf()))) &&
            ((this.numRge==null && other.getNumRge()==null) || 
             (this.numRge!=null &&
              this.numRge.equals(other.getNumRge()))) &&
            ((this.obsMot==null && other.getObsMot()==null) || 
             (this.obsMot!=null &&
              this.obsMot.equals(other.getObsMot()))) &&
            ((this.perCod==null && other.getPerCod()==null) || 
             (this.perCod!=null &&
              this.perCod.equals(other.getPerCod()))) &&
            ((this.perIcm==null && other.getPerIcm()==null) || 
             (this.perIcm!=null &&
              this.perIcm.equals(other.getPerIcm()))) &&
            ((this.perPid==null && other.getPerPid()==null) || 
             (this.perPid!=null &&
              this.perPid.equals(other.getPerPid()))) &&
            ((this.perRin==null && other.getPerRin()==null) || 
             (this.perRin!=null &&
              this.perRin.equals(other.getPerRin()))) &&
            ((this.perRir==null && other.getPerRir()==null) || 
             (this.perRir!=null &&
              this.perRir.equals(other.getPerRir()))) &&
            ((this.recCof==null && other.getRecCof()==null) || 
             (this.recCof!=null &&
              this.recCof.equals(other.getRecCof()))) &&
            ((this.recIcm==null && other.getRecIcm()==null) || 
             (this.recIcm!=null &&
              this.recIcm.equals(other.getRecIcm()))) &&
            ((this.recIpi==null && other.getRecIpi()==null) || 
             (this.recIpi!=null &&
              this.recIpi.equals(other.getRecIpi()))) &&
            ((this.recPis==null && other.getRecPis()==null) || 
             (this.recPis!=null &&
              this.recPis.equals(other.getRecPis()))) &&
            ((this.regEst==null && other.getRegEst()==null) || 
             (this.regEst!=null &&
              this.regEst.equals(other.getRegEst()))) &&
            ((this.retCof==null && other.getRetCof()==null) || 
             (this.retCof!=null &&
              this.retCof.equals(other.getRetCof()))) &&
            ((this.retCsl==null && other.getRetCsl()==null) || 
             (this.retCsl!=null &&
              this.retCsl.equals(other.getRetCsl()))) &&
            ((this.retIrf==null && other.getRetIrf()==null) || 
             (this.retIrf!=null &&
              this.retIrf.equals(other.getRetIrf()))) &&
            ((this.retOur==null && other.getRetOur()==null) || 
             (this.retOur!=null &&
              this.retOur.equals(other.getRetOur()))) &&
            ((this.retPis==null && other.getRetPis()==null) || 
             (this.retPis!=null &&
              this.retPis.equals(other.getRetPis()))) &&
            ((this.retPro==null && other.getRetPro()==null) || 
             (this.retPro!=null &&
              this.retPro.equals(other.getRetPro()))) &&
            ((this.seqRoe==null && other.getSeqRoe()==null) || 
             (this.seqRoe!=null &&
              this.seqRoe.equals(other.getSeqRoe()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs()))) &&
            ((this.sitFor==null && other.getSitFor()==null) || 
             (this.sitFor!=null &&
              this.sitFor.equals(other.getSitFor()))) &&
            ((this.tipFor==null && other.getTipFor()==null) || 
             (this.tipFor!=null &&
              this.tipFor.equals(other.getTipFor()))) &&
            ((this.tipMer==null && other.getTipMer()==null) || 
             (this.tipMer!=null &&
              this.tipMer.equals(other.getTipMer()))) &&
            ((this.triIcm==null && other.getTriIcm()==null) || 
             (this.triIcm!=null &&
              this.triIcm.equals(other.getTriIcm()))) &&
            ((this.triIpi==null && other.getTriIpi()==null) || 
             (this.triIpi!=null &&
              this.triIpi.equals(other.getTriIpi()))) &&
            ((this.triIss==null && other.getTriIss()==null) || 
             (this.triIss!=null &&
              this.triIss.equals(other.getTriIss()))) &&
            ((this.usuAtu==null && other.getUsuAtu()==null) || 
             (this.usuAtu!=null &&
              this.usuAtu.equals(other.getUsuAtu()))) &&
            ((this.usuCad==null && other.getUsuCad()==null) || 
             (this.usuCad!=null &&
              this.usuCad.equals(other.getUsuCad()))) &&
            ((this.zipCod==null && other.getZipCod()==null) || 
             (this.zipCod!=null &&
              this.zipCod.equals(other.getZipCod())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApeFor() != null) {
            _hashCode += getApeFor().hashCode();
        }
        if (getBaiFor() != null) {
            _hashCode += getBaiFor().hashCode();
        }
        if (getCepFor() != null) {
            _hashCode += getCepFor().hashCode();
        }
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCidFor() != null) {
            _hashCode += getCidFor().hashCode();
        }
        if (getCliFor() != null) {
            _hashCode += getCliFor().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodMot() != null) {
            _hashCode += getCodMot().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodRam() != null) {
            _hashCode += getCodRam().hashCode();
        }
        if (getCodRoe() != null) {
            _hashCode += getCodRoe().hashCode();
        }
        if (getCodRtr() != null) {
            _hashCode += getCodRtr().hashCode();
        }
        if (getCodSro() != null) {
            _hashCode += getCodSro().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getCodTri() != null) {
            _hashCode += getCodTri().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getCxaPst() != null) {
            _hashCode += getCxaPst().hashCode();
        }
        if (getDatAtu() != null) {
            _hashCode += getDatAtu().hashCode();
        }
        if (getDatCad() != null) {
            _hashCode += getDatCad().hashCode();
        }
        if (getDefinicao() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDefinicao());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDefinicao(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEmaNfe() != null) {
            _hashCode += getEmaNfe().hashCode();
        }
        if (getEndFor() != null) {
            _hashCode += getEndFor().hashCode();
        }
        if (getFaxFor() != null) {
            _hashCode += getFaxFor().hashCode();
        }
        if (getFonFo2() != null) {
            _hashCode += getFonFo2().hashCode();
        }
        if (getFonFo3() != null) {
            _hashCode += getFonFo3().hashCode();
        }
        if (getFonFor() != null) {
            _hashCode += getFonFor().hashCode();
        }
        if (getForRep() != null) {
            _hashCode += getForRep().hashCode();
        }
        if (getForTra() != null) {
            _hashCode += getForTra().hashCode();
        }
        if (getHorAtu() != null) {
            _hashCode += getHorAtu().hashCode();
        }
        if (getHorCad() != null) {
            _hashCode += getHorCad().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getIndFor() != null) {
            _hashCode += getIndFor().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getLimRet() != null) {
            _hashCode += getLimRet().hashCode();
        }
        if (getNenFor() != null) {
            _hashCode += getNenFor().hashCode();
        }
        if (getNomFor() != null) {
            _hashCode += getNomFor().hashCode();
        }
        if (getNotFor() != null) {
            _hashCode += getNotFor().hashCode();
        }
        if (getNotSis() != null) {
            _hashCode += getNotSis().hashCode();
        }
        if (getNumIdf() != null) {
            _hashCode += getNumIdf().hashCode();
        }
        if (getNumRge() != null) {
            _hashCode += getNumRge().hashCode();
        }
        if (getObsMot() != null) {
            _hashCode += getObsMot().hashCode();
        }
        if (getPerCod() != null) {
            _hashCode += getPerCod().hashCode();
        }
        if (getPerIcm() != null) {
            _hashCode += getPerIcm().hashCode();
        }
        if (getPerPid() != null) {
            _hashCode += getPerPid().hashCode();
        }
        if (getPerRin() != null) {
            _hashCode += getPerRin().hashCode();
        }
        if (getPerRir() != null) {
            _hashCode += getPerRir().hashCode();
        }
        if (getRecCof() != null) {
            _hashCode += getRecCof().hashCode();
        }
        if (getRecIcm() != null) {
            _hashCode += getRecIcm().hashCode();
        }
        if (getRecIpi() != null) {
            _hashCode += getRecIpi().hashCode();
        }
        if (getRecPis() != null) {
            _hashCode += getRecPis().hashCode();
        }
        if (getRegEst() != null) {
            _hashCode += getRegEst().hashCode();
        }
        if (getRetCof() != null) {
            _hashCode += getRetCof().hashCode();
        }
        if (getRetCsl() != null) {
            _hashCode += getRetCsl().hashCode();
        }
        if (getRetIrf() != null) {
            _hashCode += getRetIrf().hashCode();
        }
        if (getRetOur() != null) {
            _hashCode += getRetOur().hashCode();
        }
        if (getRetPis() != null) {
            _hashCode += getRetPis().hashCode();
        }
        if (getRetPro() != null) {
            _hashCode += getRetPro().hashCode();
        }
        if (getSeqRoe() != null) {
            _hashCode += getSeqRoe().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        if (getSitFor() != null) {
            _hashCode += getSitFor().hashCode();
        }
        if (getTipFor() != null) {
            _hashCode += getTipFor().hashCode();
        }
        if (getTipMer() != null) {
            _hashCode += getTipMer().hashCode();
        }
        if (getTriIcm() != null) {
            _hashCode += getTriIcm().hashCode();
        }
        if (getTriIpi() != null) {
            _hashCode += getTriIpi().hashCode();
        }
        if (getTriIss() != null) {
            _hashCode += getTriIss().hashCode();
        }
        if (getUsuAtu() != null) {
            _hashCode += getUsuAtu().hashCode();
        }
        if (getUsuCad() != null) {
            _hashCode += getUsuCad().hashCode();
        }
        if (getZipCod() != null) {
            _hashCode += getZipCod().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresImportarInFornecedor.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarInFornecedor"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apeFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "apeFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cliFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cliFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRam");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRam"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cxaPst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cxaPst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("definicao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "definicao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarInFornecedorDefinicao"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFo2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFo2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFo3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFo3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forRep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forRep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notSis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notSis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numIdf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numIdf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numRge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numRge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perPid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perPid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perRin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perRin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perRir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perRir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recIpi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recIpi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCof");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCof"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retCsl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retCsl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retOur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retOur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqRoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqRoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipMer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipMer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIcm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIcm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIpi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIpi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("triIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "triIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuAtu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuAtu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuCad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuCad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zipCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
