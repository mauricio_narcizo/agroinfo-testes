package br.com.senior.agroinfo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;
import br.com.senior.services.G5SeniorServicesLocator;

public class UtilProxy {

	@SuppressWarnings("unchecked")
	public static <T> T getServiceProxy(Class<T> clazz, String url) throws ServiceException {
        br.com.senior.services.G5SeniorServices seniorServices = new G5SeniorServicesLocator();
        T wsExpSync = (T) seniorServices.getPort(clazz);
        javax.xml.rpc.Stub stub = (Stub) wsExpSync;
        String endPoint = (String) stub._getProperty("javax.xml.rpc.service.endpoint.address");
        String newEndPoint = endPoint.replaceAll("(^https?://[\\w.]+:\\d+)", url);
        stub._setProperty("javax.xml.rpc.service.endpoint.address", newEndPoint);
        return wsExpSync;
    }

    public static int indexOf(Pattern pattern, String s) {
        Matcher matcher = pattern.matcher(s);
        return matcher.find() ? matcher.start() : -1;
    }
}
