/**
 * EmpresafilialExportar3OutEmpresaFilialDadosPDV.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar3OutEmpresaFilialDadosPDV  implements java.io.Serializable {
    private java.lang.String chaPdv;

    private java.lang.String desPdv;

    private java.lang.String vlrPdv;

    public EmpresafilialExportar3OutEmpresaFilialDadosPDV() {
    }

    public EmpresafilialExportar3OutEmpresaFilialDadosPDV(
           java.lang.String chaPdv,
           java.lang.String desPdv,
           java.lang.String vlrPdv) {
           this.chaPdv = chaPdv;
           this.desPdv = desPdv;
           this.vlrPdv = vlrPdv;
    }


    /**
     * Gets the chaPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @return chaPdv
     */
    public java.lang.String getChaPdv() {
        return chaPdv;
    }


    /**
     * Sets the chaPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @param chaPdv
     */
    public void setChaPdv(java.lang.String chaPdv) {
        this.chaPdv = chaPdv;
    }


    /**
     * Gets the desPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @return desPdv
     */
    public java.lang.String getDesPdv() {
        return desPdv;
    }


    /**
     * Sets the desPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @param desPdv
     */
    public void setDesPdv(java.lang.String desPdv) {
        this.desPdv = desPdv;
    }


    /**
     * Gets the vlrPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @return vlrPdv
     */
    public java.lang.String getVlrPdv() {
        return vlrPdv;
    }


    /**
     * Sets the vlrPdv value for this EmpresafilialExportar3OutEmpresaFilialDadosPDV.
     * 
     * @param vlrPdv
     */
    public void setVlrPdv(java.lang.String vlrPdv) {
        this.vlrPdv = vlrPdv;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar3OutEmpresaFilialDadosPDV)) return false;
        EmpresafilialExportar3OutEmpresaFilialDadosPDV other = (EmpresafilialExportar3OutEmpresaFilialDadosPDV) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chaPdv==null && other.getChaPdv()==null) || 
             (this.chaPdv!=null &&
              this.chaPdv.equals(other.getChaPdv()))) &&
            ((this.desPdv==null && other.getDesPdv()==null) || 
             (this.desPdv!=null &&
              this.desPdv.equals(other.getDesPdv()))) &&
            ((this.vlrPdv==null && other.getVlrPdv()==null) || 
             (this.vlrPdv!=null &&
              this.vlrPdv.equals(other.getVlrPdv())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChaPdv() != null) {
            _hashCode += getChaPdv().hashCode();
        }
        if (getDesPdv() != null) {
            _hashCode += getDesPdv().hashCode();
        }
        if (getVlrPdv() != null) {
            _hashCode += getVlrPdv().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar3OutEmpresaFilialDadosPDV.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialDadosPDV"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chaPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "chaPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
