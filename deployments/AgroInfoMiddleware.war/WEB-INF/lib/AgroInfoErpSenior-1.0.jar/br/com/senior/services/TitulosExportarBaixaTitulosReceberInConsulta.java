/**
 * TitulosExportarBaixaTitulosReceberInConsulta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosExportarBaixaTitulosReceberInConsulta  implements java.io.Serializable {
    private java.lang.String codTpt;

    private java.lang.String numTit;

    private java.lang.Integer seqMov;

    private java.lang.String tipPen;

    public TitulosExportarBaixaTitulosReceberInConsulta() {
    }

    public TitulosExportarBaixaTitulosReceberInConsulta(
           java.lang.String codTpt,
           java.lang.String numTit,
           java.lang.Integer seqMov,
           java.lang.String tipPen) {
           this.codTpt = codTpt;
           this.numTit = numTit;
           this.seqMov = seqMov;
           this.tipPen = tipPen;
    }


    /**
     * Gets the codTpt value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the numTit value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the seqMov value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @return seqMov
     */
    public java.lang.Integer getSeqMov() {
        return seqMov;
    }


    /**
     * Sets the seqMov value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @param seqMov
     */
    public void setSeqMov(java.lang.Integer seqMov) {
        this.seqMov = seqMov;
    }


    /**
     * Gets the tipPen value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @return tipPen
     */
    public java.lang.String getTipPen() {
        return tipPen;
    }


    /**
     * Sets the tipPen value for this TitulosExportarBaixaTitulosReceberInConsulta.
     * 
     * @param tipPen
     */
    public void setTipPen(java.lang.String tipPen) {
        this.tipPen = tipPen;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExportarBaixaTitulosReceberInConsulta)) return false;
        TitulosExportarBaixaTitulosReceberInConsulta other = (TitulosExportarBaixaTitulosReceberInConsulta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.seqMov==null && other.getSeqMov()==null) || 
             (this.seqMov!=null &&
              this.seqMov.equals(other.getSeqMov()))) &&
            ((this.tipPen==null && other.getTipPen()==null) || 
             (this.tipPen!=null &&
              this.tipPen.equals(other.getTipPen())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getSeqMov() != null) {
            _hashCode += getSeqMov().hashCode();
        }
        if (getTipPen() != null) {
            _hashCode += getTipPen().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExportarBaixaTitulosReceberInConsulta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarBaixaTitulosReceberInConsulta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipPen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipPen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
