/**
 * TitulosBaixaCompensacaoCPCRVarejoOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosBaixaCompensacaoCPCRVarejoOutRetorno  implements java.io.Serializable {
    private java.lang.String chvLot;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe[] detalhe;

    private java.lang.Integer ideExt;

    private java.lang.String msgRet;

    private java.lang.Integer tipRet;

    public TitulosBaixaCompensacaoCPCRVarejoOutRetorno() {
    }

    public TitulosBaixaCompensacaoCPCRVarejoOutRetorno(
           java.lang.String chvLot,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe[] detalhe,
           java.lang.Integer ideExt,
           java.lang.String msgRet,
           java.lang.Integer tipRet) {
           this.chvLot = chvLot;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.detalhe = detalhe;
           this.ideExt = ideExt;
           this.msgRet = msgRet;
           this.tipRet = tipRet;
    }


    /**
     * Gets the chvLot value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return chvLot
     */
    public java.lang.String getChvLot() {
        return chvLot;
    }


    /**
     * Sets the chvLot value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param chvLot
     */
    public void setChvLot(java.lang.String chvLot) {
        this.chvLot = chvLot;
    }


    /**
     * Gets the codEmp value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the detalhe value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return detalhe
     */
    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe[] getDetalhe() {
        return detalhe;
    }


    /**
     * Sets the detalhe value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param detalhe
     */
    public void setDetalhe(br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe[] detalhe) {
        this.detalhe = detalhe;
    }

    public br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe getDetalhe(int i) {
        return this.detalhe[i];
    }

    public void setDetalhe(int i, br.com.senior.services.TitulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe _value) {
        this.detalhe[i] = _value;
    }


    /**
     * Gets the ideExt value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the msgRet value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the tipRet value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this TitulosBaixaCompensacaoCPCRVarejoOutRetorno.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosBaixaCompensacaoCPCRVarejoOutRetorno)) return false;
        TitulosBaixaCompensacaoCPCRVarejoOutRetorno other = (TitulosBaixaCompensacaoCPCRVarejoOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chvLot==null && other.getChvLot()==null) || 
             (this.chvLot!=null &&
              this.chvLot.equals(other.getChvLot()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.detalhe==null && other.getDetalhe()==null) || 
             (this.detalhe!=null &&
              java.util.Arrays.equals(this.detalhe, other.getDetalhe()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChvLot() != null) {
            _hashCode += getChvLot().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDetalhe() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalhe());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalhe(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosBaixaCompensacaoCPCRVarejoOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixaCompensacaoCPCRVarejoOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chvLot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "chvLot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalhe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalhe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixaCompensacaoCPCRVarejoOutRetornoDetalhe"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
