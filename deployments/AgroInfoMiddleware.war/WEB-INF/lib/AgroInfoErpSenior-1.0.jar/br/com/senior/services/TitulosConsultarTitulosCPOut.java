/**
 * TitulosConsultarTitulosCPOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosConsultarTitulosCPOut  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private java.lang.String tipoRetorno;

    private br.com.senior.services.TitulosConsultarTitulosCPOutTitulos[] titulos;

    public TitulosConsultarTitulosCPOut() {
    }

    public TitulosConsultarTitulosCPOut(
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           java.lang.String tipoRetorno,
           br.com.senior.services.TitulosConsultarTitulosCPOutTitulos[] titulos) {
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.tipoRetorno = tipoRetorno;
           this.titulos = titulos;
    }


    /**
     * Gets the erroExecucao value for this TitulosConsultarTitulosCPOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosConsultarTitulosCPOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosConsultarTitulosCPOut.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosConsultarTitulosCPOut.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the tipoRetorno value for this TitulosConsultarTitulosCPOut.
     * 
     * @return tipoRetorno
     */
    public java.lang.String getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosConsultarTitulosCPOut.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.String tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the titulos value for this TitulosConsultarTitulosCPOut.
     * 
     * @return titulos
     */
    public br.com.senior.services.TitulosConsultarTitulosCPOutTitulos[] getTitulos() {
        return titulos;
    }


    /**
     * Sets the titulos value for this TitulosConsultarTitulosCPOut.
     * 
     * @param titulos
     */
    public void setTitulos(br.com.senior.services.TitulosConsultarTitulosCPOutTitulos[] titulos) {
        this.titulos = titulos;
    }

    public br.com.senior.services.TitulosConsultarTitulosCPOutTitulos getTitulos(int i) {
        return this.titulos[i];
    }

    public void setTitulos(int i, br.com.senior.services.TitulosConsultarTitulosCPOutTitulos _value) {
        this.titulos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosCPOut)) return false;
        TitulosConsultarTitulosCPOut other = (TitulosConsultarTitulosCPOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno()))) &&
            ((this.titulos==null && other.getTitulos()==null) || 
             (this.titulos!=null &&
              java.util.Arrays.equals(this.titulos, other.getTitulos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        if (getTitulos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosCPOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosCPOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosCPOutTitulos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
