/**
 * TitulosConsultarTitulosCP2OutTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosConsultarTitulosCP2OutTitulos  implements java.io.Serializable {
    private java.lang.String antDsc;

    private java.lang.String codCrt;

    private java.lang.String codEmp;

    private java.lang.String codFil;

    private java.lang.String codFor;

    private java.lang.String codMpt;

    private java.lang.Integer codNtg;

    private java.lang.String codPor;

    private java.lang.String codTpt;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.String desMpt;

    private java.lang.String desPor;

    private java.lang.String desSit;

    private java.lang.String desTpt;

    private java.lang.String dscNeg;

    private java.lang.String filNfv;

    private java.lang.String jrsDia;

    private java.lang.String jrsNeg;

    private java.lang.String mulNeg;

    private java.lang.String nomFor;

    private java.lang.String numNfv;

    private java.lang.String numTit;

    private java.lang.String outNeg;

    private java.lang.String perDsc;

    private java.lang.String perJrs;

    private java.lang.String perMul;

    private java.lang.String proJrs;

    private java.lang.String sigFil;

    private java.lang.String sitTit;

    private java.lang.String snfNfv;

    private java.lang.String tipJrs;

    private java.lang.String tolDsc;

    private java.lang.String tolJrs;

    private java.lang.String tolMul;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.String vlrAbe;

    private java.lang.String vlrAcr;

    private java.lang.String vlrCor;

    private java.lang.String vlrDsc;

    private java.lang.String vlrMul;

    private java.lang.String vlrOde;

    private java.lang.String vlrOri;

    private java.lang.String vlrPag;

    private java.lang.String vlrTot;

    public TitulosConsultarTitulosCP2OutTitulos() {
    }

    public TitulosConsultarTitulosCP2OutTitulos(
           java.lang.String antDsc,
           java.lang.String codCrt,
           java.lang.String codEmp,
           java.lang.String codFil,
           java.lang.String codFor,
           java.lang.String codMpt,
           java.lang.Integer codNtg,
           java.lang.String codPor,
           java.lang.String codTpt,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.String desMpt,
           java.lang.String desPor,
           java.lang.String desSit,
           java.lang.String desTpt,
           java.lang.String dscNeg,
           java.lang.String filNfv,
           java.lang.String jrsDia,
           java.lang.String jrsNeg,
           java.lang.String mulNeg,
           java.lang.String nomFor,
           java.lang.String numNfv,
           java.lang.String numTit,
           java.lang.String outNeg,
           java.lang.String perDsc,
           java.lang.String perJrs,
           java.lang.String perMul,
           java.lang.String proJrs,
           java.lang.String sigFil,
           java.lang.String sitTit,
           java.lang.String snfNfv,
           java.lang.String tipJrs,
           java.lang.String tolDsc,
           java.lang.String tolJrs,
           java.lang.String tolMul,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.String vlrAbe,
           java.lang.String vlrAcr,
           java.lang.String vlrCor,
           java.lang.String vlrDsc,
           java.lang.String vlrMul,
           java.lang.String vlrOde,
           java.lang.String vlrOri,
           java.lang.String vlrPag,
           java.lang.String vlrTot) {
           this.antDsc = antDsc;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codMpt = codMpt;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codTpt = codTpt;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.desMpt = desMpt;
           this.desPor = desPor;
           this.desSit = desSit;
           this.desTpt = desTpt;
           this.dscNeg = dscNeg;
           this.filNfv = filNfv;
           this.jrsDia = jrsDia;
           this.jrsNeg = jrsNeg;
           this.mulNeg = mulNeg;
           this.nomFor = nomFor;
           this.numNfv = numNfv;
           this.numTit = numTit;
           this.outNeg = outNeg;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.proJrs = proJrs;
           this.sigFil = sigFil;
           this.sitTit = sitTit;
           this.snfNfv = snfNfv;
           this.tipJrs = tipJrs;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrAbe = vlrAbe;
           this.vlrAcr = vlrAcr;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrMul = vlrMul;
           this.vlrOde = vlrOde;
           this.vlrOri = vlrOri;
           this.vlrPag = vlrPag;
           this.vlrTot = vlrTot;
    }


    /**
     * Gets the antDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the codCrt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codEmp
     */
    public java.lang.String getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.String codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codFil
     */
    public java.lang.String getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.String codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codFor
     */
    public java.lang.String getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.String codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codMpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codMpt
     */
    public java.lang.String getCodMpt() {
        return codMpt;
    }


    /**
     * Sets the codMpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codMpt
     */
    public void setCodMpt(java.lang.String codMpt) {
        this.codMpt = codMpt;
    }


    /**
     * Gets the codNtg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codNtg
     */
    public java.lang.Integer getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.Integer codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the datDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the desMpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return desMpt
     */
    public java.lang.String getDesMpt() {
        return desMpt;
    }


    /**
     * Sets the desMpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param desMpt
     */
    public void setDesMpt(java.lang.String desMpt) {
        this.desMpt = desMpt;
    }


    /**
     * Gets the desPor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return desPor
     */
    public java.lang.String getDesPor() {
        return desPor;
    }


    /**
     * Sets the desPor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param desPor
     */
    public void setDesPor(java.lang.String desPor) {
        this.desPor = desPor;
    }


    /**
     * Gets the desSit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return desSit
     */
    public java.lang.String getDesSit() {
        return desSit;
    }


    /**
     * Sets the desSit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param desSit
     */
    public void setDesSit(java.lang.String desSit) {
        this.desSit = desSit;
    }


    /**
     * Gets the desTpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return desTpt
     */
    public java.lang.String getDesTpt() {
        return desTpt;
    }


    /**
     * Sets the desTpt value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param desTpt
     */
    public void setDesTpt(java.lang.String desTpt) {
        this.desTpt = desTpt;
    }


    /**
     * Gets the dscNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return dscNeg
     */
    public java.lang.String getDscNeg() {
        return dscNeg;
    }


    /**
     * Sets the dscNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param dscNeg
     */
    public void setDscNeg(java.lang.String dscNeg) {
        this.dscNeg = dscNeg;
    }


    /**
     * Gets the filNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return filNfv
     */
    public java.lang.String getFilNfv() {
        return filNfv;
    }


    /**
     * Sets the filNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param filNfv
     */
    public void setFilNfv(java.lang.String filNfv) {
        this.filNfv = filNfv;
    }


    /**
     * Gets the jrsDia value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return jrsDia
     */
    public java.lang.String getJrsDia() {
        return jrsDia;
    }


    /**
     * Sets the jrsDia value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param jrsDia
     */
    public void setJrsDia(java.lang.String jrsDia) {
        this.jrsDia = jrsDia;
    }


    /**
     * Gets the jrsNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return jrsNeg
     */
    public java.lang.String getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.String jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the mulNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return mulNeg
     */
    public java.lang.String getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.String mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the nomFor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return nomFor
     */
    public java.lang.String getNomFor() {
        return nomFor;
    }


    /**
     * Sets the nomFor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param nomFor
     */
    public void setNomFor(java.lang.String nomFor) {
        this.nomFor = nomFor;
    }


    /**
     * Gets the numNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return numNfv
     */
    public java.lang.String getNumNfv() {
        return numNfv;
    }


    /**
     * Sets the numNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param numNfv
     */
    public void setNumNfv(java.lang.String numNfv) {
        this.numNfv = numNfv;
    }


    /**
     * Gets the numTit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the outNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return outNeg
     */
    public java.lang.String getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.String outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return perDsc
     */
    public java.lang.String getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.String perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return perJrs
     */
    public java.lang.String getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.String perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return perMul
     */
    public java.lang.String getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.String perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the proJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return proJrs
     */
    public java.lang.String getProJrs() {
        return proJrs;
    }


    /**
     * Sets the proJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param proJrs
     */
    public void setProJrs(java.lang.String proJrs) {
        this.proJrs = proJrs;
    }


    /**
     * Gets the sigFil value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return sigFil
     */
    public java.lang.String getSigFil() {
        return sigFil;
    }


    /**
     * Sets the sigFil value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param sigFil
     */
    public void setSigFil(java.lang.String sigFil) {
        this.sigFil = sigFil;
    }


    /**
     * Gets the sitTit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return sitTit
     */
    public java.lang.String getSitTit() {
        return sitTit;
    }


    /**
     * Sets the sitTit value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param sitTit
     */
    public void setSitTit(java.lang.String sitTit) {
        this.sitTit = sitTit;
    }


    /**
     * Gets the snfNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return snfNfv
     */
    public java.lang.String getSnfNfv() {
        return snfNfv;
    }


    /**
     * Sets the snfNfv value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param snfNfv
     */
    public void setSnfNfv(java.lang.String snfNfv) {
        this.snfNfv = snfNfv;
    }


    /**
     * Gets the tipJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the tolDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return tolDsc
     */
    public java.lang.String getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.String tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return tolJrs
     */
    public java.lang.String getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.String tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return tolMul
     */
    public java.lang.String getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.String tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the vctOri value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrAbe value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrAbe
     */
    public java.lang.String getVlrAbe() {
        return vlrAbe;
    }


    /**
     * Sets the vlrAbe value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrAbe
     */
    public void setVlrAbe(java.lang.String vlrAbe) {
        this.vlrAbe = vlrAbe;
    }


    /**
     * Gets the vlrAcr value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrAcr
     */
    public java.lang.String getVlrAcr() {
        return vlrAcr;
    }


    /**
     * Sets the vlrAcr value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrAcr
     */
    public void setVlrAcr(java.lang.String vlrAcr) {
        this.vlrAcr = vlrAcr;
    }


    /**
     * Gets the vlrCor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrCor
     */
    public java.lang.String getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.String vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrDsc
     */
    public java.lang.String getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.String vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrMul
     */
    public java.lang.String getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.String vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOde value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrOde
     */
    public java.lang.String getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.String vlrOde) {
        this.vlrOde = vlrOde;
    }


    /**
     * Gets the vlrOri value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrOri
     */
    public java.lang.String getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.String vlrOri) {
        this.vlrOri = vlrOri;
    }


    /**
     * Gets the vlrPag value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrPag
     */
    public java.lang.String getVlrPag() {
        return vlrPag;
    }


    /**
     * Sets the vlrPag value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrPag
     */
    public void setVlrPag(java.lang.String vlrPag) {
        this.vlrPag = vlrPag;
    }


    /**
     * Gets the vlrTot value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @return vlrTot
     */
    public java.lang.String getVlrTot() {
        return vlrTot;
    }


    /**
     * Sets the vlrTot value for this TitulosConsultarTitulosCP2OutTitulos.
     * 
     * @param vlrTot
     */
    public void setVlrTot(java.lang.String vlrTot) {
        this.vlrTot = vlrTot;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosConsultarTitulosCP2OutTitulos)) return false;
        TitulosConsultarTitulosCP2OutTitulos other = (TitulosConsultarTitulosCP2OutTitulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codMpt==null && other.getCodMpt()==null) || 
             (this.codMpt!=null &&
              this.codMpt.equals(other.getCodMpt()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.desMpt==null && other.getDesMpt()==null) || 
             (this.desMpt!=null &&
              this.desMpt.equals(other.getDesMpt()))) &&
            ((this.desPor==null && other.getDesPor()==null) || 
             (this.desPor!=null &&
              this.desPor.equals(other.getDesPor()))) &&
            ((this.desSit==null && other.getDesSit()==null) || 
             (this.desSit!=null &&
              this.desSit.equals(other.getDesSit()))) &&
            ((this.desTpt==null && other.getDesTpt()==null) || 
             (this.desTpt!=null &&
              this.desTpt.equals(other.getDesTpt()))) &&
            ((this.dscNeg==null && other.getDscNeg()==null) || 
             (this.dscNeg!=null &&
              this.dscNeg.equals(other.getDscNeg()))) &&
            ((this.filNfv==null && other.getFilNfv()==null) || 
             (this.filNfv!=null &&
              this.filNfv.equals(other.getFilNfv()))) &&
            ((this.jrsDia==null && other.getJrsDia()==null) || 
             (this.jrsDia!=null &&
              this.jrsDia.equals(other.getJrsDia()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.nomFor==null && other.getNomFor()==null) || 
             (this.nomFor!=null &&
              this.nomFor.equals(other.getNomFor()))) &&
            ((this.numNfv==null && other.getNumNfv()==null) || 
             (this.numNfv!=null &&
              this.numNfv.equals(other.getNumNfv()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.proJrs==null && other.getProJrs()==null) || 
             (this.proJrs!=null &&
              this.proJrs.equals(other.getProJrs()))) &&
            ((this.sigFil==null && other.getSigFil()==null) || 
             (this.sigFil!=null &&
              this.sigFil.equals(other.getSigFil()))) &&
            ((this.sitTit==null && other.getSitTit()==null) || 
             (this.sitTit!=null &&
              this.sitTit.equals(other.getSitTit()))) &&
            ((this.snfNfv==null && other.getSnfNfv()==null) || 
             (this.snfNfv!=null &&
              this.snfNfv.equals(other.getSnfNfv()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrAbe==null && other.getVlrAbe()==null) || 
             (this.vlrAbe!=null &&
              this.vlrAbe.equals(other.getVlrAbe()))) &&
            ((this.vlrAcr==null && other.getVlrAcr()==null) || 
             (this.vlrAcr!=null &&
              this.vlrAcr.equals(other.getVlrAcr()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri()))) &&
            ((this.vlrPag==null && other.getVlrPag()==null) || 
             (this.vlrPag!=null &&
              this.vlrPag.equals(other.getVlrPag()))) &&
            ((this.vlrTot==null && other.getVlrTot()==null) || 
             (this.vlrTot!=null &&
              this.vlrTot.equals(other.getVlrTot())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodMpt() != null) {
            _hashCode += getCodMpt().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getDesMpt() != null) {
            _hashCode += getDesMpt().hashCode();
        }
        if (getDesPor() != null) {
            _hashCode += getDesPor().hashCode();
        }
        if (getDesSit() != null) {
            _hashCode += getDesSit().hashCode();
        }
        if (getDesTpt() != null) {
            _hashCode += getDesTpt().hashCode();
        }
        if (getDscNeg() != null) {
            _hashCode += getDscNeg().hashCode();
        }
        if (getFilNfv() != null) {
            _hashCode += getFilNfv().hashCode();
        }
        if (getJrsDia() != null) {
            _hashCode += getJrsDia().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNomFor() != null) {
            _hashCode += getNomFor().hashCode();
        }
        if (getNumNfv() != null) {
            _hashCode += getNumNfv().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getProJrs() != null) {
            _hashCode += getProJrs().hashCode();
        }
        if (getSigFil() != null) {
            _hashCode += getSigFil().hashCode();
        }
        if (getSitTit() != null) {
            _hashCode += getSitTit().hashCode();
        }
        if (getSnfNfv() != null) {
            _hashCode += getSnfNfv().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrAbe() != null) {
            _hashCode += getVlrAbe().hashCode();
        }
        if (getVlrAcr() != null) {
            _hashCode += getVlrAcr().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        if (getVlrPag() != null) {
            _hashCode += getVlrPag().hashCode();
        }
        if (getVlrTot() != null) {
            _hashCode += getVlrTot().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosConsultarTitulosCP2OutTitulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosConsultarTitulosCP2OutTitulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desSit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desSit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("proJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "proJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAbe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAbe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrAcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrAcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrPag");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrPag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrTot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrTot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
