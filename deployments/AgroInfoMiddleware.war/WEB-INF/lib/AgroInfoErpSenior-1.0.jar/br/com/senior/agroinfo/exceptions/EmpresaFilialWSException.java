package br.com.senior.agroinfo.exceptions;

public class EmpresaFilialWSException extends Exception {

	private static final long serialVersionUID = 1L;

	public EmpresaFilialWSException(final String msg) {
		super(msg);
	}
}
