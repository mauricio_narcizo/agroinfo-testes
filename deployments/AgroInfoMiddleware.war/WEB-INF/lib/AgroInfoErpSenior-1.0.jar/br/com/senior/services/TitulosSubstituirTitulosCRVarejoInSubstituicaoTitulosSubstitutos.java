/**
 * TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos  implements java.io.Serializable {
    private java.lang.String catExt;

    private java.lang.String catTef;

    private java.lang.String cheAge;

    private java.lang.String cheBan;

    private java.lang.String cheCta;

    private java.lang.String cheNum;

    private java.lang.String codBar;

    private java.lang.Integer codCli;

    private java.lang.Integer codCnv;

    private java.lang.String codCrt;

    private java.lang.Integer codEqu;

    private java.lang.Integer codFil;

    private java.lang.Integer codFin;

    private java.lang.Integer codFpg;

    private java.lang.Integer codNtg;

    private java.lang.Integer codOpe;

    private java.lang.String codPor;

    private java.lang.Double codSac;

    private java.lang.String codTpt;

    private java.lang.Integer croEcf;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.Double dscNeg;

    private java.lang.Integer filPfi;

    private java.lang.Double jrsNeg;

    private java.lang.String locTit;

    private java.lang.Double mulNeg;

    private java.lang.String nsuTef;

    private java.lang.String numCfi;

    private java.lang.String numInt;

    private java.lang.Integer numPar;

    private java.lang.String numPfi;

    private java.lang.String numTit;

    private java.lang.String obsTcr;

    private java.lang.Double outNeg;

    private java.lang.Integer perDsc;

    private java.lang.Integer perJrs;

    private java.lang.Integer perMul;

    private java.lang.Integer qtdPar;

    private java.lang.String tipCar;

    private java.lang.String tipJrs;

    private java.lang.String titBan;

    private java.lang.Integer tolDsc;

    private java.lang.Integer tolJrs;

    private java.lang.Integer tolMul;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrJrs;

    private java.lang.Double vlrOri;

    private java.lang.Double vlrTax;

    public TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos() {
    }

    public TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos(
           java.lang.String catExt,
           java.lang.String catTef,
           java.lang.String cheAge,
           java.lang.String cheBan,
           java.lang.String cheCta,
           java.lang.String cheNum,
           java.lang.String codBar,
           java.lang.Integer codCli,
           java.lang.Integer codCnv,
           java.lang.String codCrt,
           java.lang.Integer codEqu,
           java.lang.Integer codFil,
           java.lang.Integer codFin,
           java.lang.Integer codFpg,
           java.lang.Integer codNtg,
           java.lang.Integer codOpe,
           java.lang.String codPor,
           java.lang.Double codSac,
           java.lang.String codTpt,
           java.lang.Integer croEcf,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.Double dscNeg,
           java.lang.Integer filPfi,
           java.lang.Double jrsNeg,
           java.lang.String locTit,
           java.lang.Double mulNeg,
           java.lang.String nsuTef,
           java.lang.String numCfi,
           java.lang.String numInt,
           java.lang.Integer numPar,
           java.lang.String numPfi,
           java.lang.String numTit,
           java.lang.String obsTcr,
           java.lang.Double outNeg,
           java.lang.Integer perDsc,
           java.lang.Integer perJrs,
           java.lang.Integer perMul,
           java.lang.Integer qtdPar,
           java.lang.String tipCar,
           java.lang.String tipJrs,
           java.lang.String titBan,
           java.lang.Integer tolDsc,
           java.lang.Integer tolJrs,
           java.lang.Integer tolMul,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.Double vlrDsc,
           java.lang.Double vlrJrs,
           java.lang.Double vlrOri,
           java.lang.Double vlrTax) {
           this.catExt = catExt;
           this.catTef = catTef;
           this.cheAge = cheAge;
           this.cheBan = cheBan;
           this.cheCta = cheCta;
           this.cheNum = cheNum;
           this.codBar = codBar;
           this.codCli = codCli;
           this.codCnv = codCnv;
           this.codCrt = codCrt;
           this.codEqu = codEqu;
           this.codFil = codFil;
           this.codFin = codFin;
           this.codFpg = codFpg;
           this.codNtg = codNtg;
           this.codOpe = codOpe;
           this.codPor = codPor;
           this.codSac = codSac;
           this.codTpt = codTpt;
           this.croEcf = croEcf;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.dscNeg = dscNeg;
           this.filPfi = filPfi;
           this.jrsNeg = jrsNeg;
           this.locTit = locTit;
           this.mulNeg = mulNeg;
           this.nsuTef = nsuTef;
           this.numCfi = numCfi;
           this.numInt = numInt;
           this.numPar = numPar;
           this.numPfi = numPfi;
           this.numTit = numTit;
           this.obsTcr = obsTcr;
           this.outNeg = outNeg;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.qtdPar = qtdPar;
           this.tipCar = tipCar;
           this.tipJrs = tipJrs;
           this.titBan = titBan;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrDsc = vlrDsc;
           this.vlrJrs = vlrJrs;
           this.vlrOri = vlrOri;
           this.vlrTax = vlrTax;
    }


    /**
     * Gets the catExt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return catExt
     */
    public java.lang.String getCatExt() {
        return catExt;
    }


    /**
     * Sets the catExt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param catExt
     */
    public void setCatExt(java.lang.String catExt) {
        this.catExt = catExt;
    }


    /**
     * Gets the catTef value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return catTef
     */
    public java.lang.String getCatTef() {
        return catTef;
    }


    /**
     * Sets the catTef value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param catTef
     */
    public void setCatTef(java.lang.String catTef) {
        this.catTef = catTef;
    }


    /**
     * Gets the cheAge value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return cheAge
     */
    public java.lang.String getCheAge() {
        return cheAge;
    }


    /**
     * Sets the cheAge value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param cheAge
     */
    public void setCheAge(java.lang.String cheAge) {
        this.cheAge = cheAge;
    }


    /**
     * Gets the cheBan value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return cheBan
     */
    public java.lang.String getCheBan() {
        return cheBan;
    }


    /**
     * Sets the cheBan value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param cheBan
     */
    public void setCheBan(java.lang.String cheBan) {
        this.cheBan = cheBan;
    }


    /**
     * Gets the cheCta value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return cheCta
     */
    public java.lang.String getCheCta() {
        return cheCta;
    }


    /**
     * Sets the cheCta value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param cheCta
     */
    public void setCheCta(java.lang.String cheCta) {
        this.cheCta = cheCta;
    }


    /**
     * Gets the cheNum value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return cheNum
     */
    public java.lang.String getCheNum() {
        return cheNum;
    }


    /**
     * Sets the cheNum value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param cheNum
     */
    public void setCheNum(java.lang.String cheNum) {
        this.cheNum = cheNum;
    }


    /**
     * Gets the codBar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codBar
     */
    public java.lang.String getCodBar() {
        return codBar;
    }


    /**
     * Sets the codBar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codBar
     */
    public void setCodBar(java.lang.String codBar) {
        this.codBar = codBar;
    }


    /**
     * Gets the codCli value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCnv value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codCnv
     */
    public java.lang.Integer getCodCnv() {
        return codCnv;
    }


    /**
     * Sets the codCnv value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codCnv
     */
    public void setCodCnv(java.lang.Integer codCnv) {
        this.codCnv = codCnv;
    }


    /**
     * Gets the codCrt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEqu value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codEqu
     */
    public java.lang.Integer getCodEqu() {
        return codEqu;
    }


    /**
     * Sets the codEqu value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codEqu
     */
    public void setCodEqu(java.lang.Integer codEqu) {
        this.codEqu = codEqu;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFin value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codFin
     */
    public java.lang.Integer getCodFin() {
        return codFin;
    }


    /**
     * Sets the codFin value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codFin
     */
    public void setCodFin(java.lang.Integer codFin) {
        this.codFin = codFin;
    }


    /**
     * Gets the codFpg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codNtg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codNtg
     */
    public java.lang.Integer getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.Integer codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codOpe value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codOpe
     */
    public java.lang.Integer getCodOpe() {
        return codOpe;
    }


    /**
     * Sets the codOpe value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codOpe
     */
    public void setCodOpe(java.lang.Integer codOpe) {
        this.codOpe = codOpe;
    }


    /**
     * Gets the codPor value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codSac value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codSac
     */
    public java.lang.Double getCodSac() {
        return codSac;
    }


    /**
     * Sets the codSac value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codSac
     */
    public void setCodSac(java.lang.Double codSac) {
        this.codSac = codSac;
    }


    /**
     * Gets the codTpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the croEcf value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return croEcf
     */
    public java.lang.Integer getCroEcf() {
        return croEcf;
    }


    /**
     * Sets the croEcf value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param croEcf
     */
    public void setCroEcf(java.lang.Integer croEcf) {
        this.croEcf = croEcf;
    }


    /**
     * Gets the datDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the dscNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return dscNeg
     */
    public java.lang.Double getDscNeg() {
        return dscNeg;
    }


    /**
     * Sets the dscNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param dscNeg
     */
    public void setDscNeg(java.lang.Double dscNeg) {
        this.dscNeg = dscNeg;
    }


    /**
     * Gets the filPfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return filPfi
     */
    public java.lang.Integer getFilPfi() {
        return filPfi;
    }


    /**
     * Sets the filPfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param filPfi
     */
    public void setFilPfi(java.lang.Integer filPfi) {
        this.filPfi = filPfi;
    }


    /**
     * Gets the jrsNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return jrsNeg
     */
    public java.lang.Double getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.Double jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the locTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return locTit
     */
    public java.lang.String getLocTit() {
        return locTit;
    }


    /**
     * Sets the locTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param locTit
     */
    public void setLocTit(java.lang.String locTit) {
        this.locTit = locTit;
    }


    /**
     * Gets the mulNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return mulNeg
     */
    public java.lang.Double getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.Double mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the nsuTef value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return nsuTef
     */
    public java.lang.String getNsuTef() {
        return nsuTef;
    }


    /**
     * Sets the nsuTef value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param nsuTef
     */
    public void setNsuTef(java.lang.String nsuTef) {
        this.nsuTef = nsuTef;
    }


    /**
     * Gets the numCfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return numCfi
     */
    public java.lang.String getNumCfi() {
        return numCfi;
    }


    /**
     * Sets the numCfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param numCfi
     */
    public void setNumCfi(java.lang.String numCfi) {
        this.numCfi = numCfi;
    }


    /**
     * Gets the numInt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return numInt
     */
    public java.lang.String getNumInt() {
        return numInt;
    }


    /**
     * Sets the numInt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param numInt
     */
    public void setNumInt(java.lang.String numInt) {
        this.numInt = numInt;
    }


    /**
     * Gets the numPar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return numPar
     */
    public java.lang.Integer getNumPar() {
        return numPar;
    }


    /**
     * Sets the numPar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param numPar
     */
    public void setNumPar(java.lang.Integer numPar) {
        this.numPar = numPar;
    }


    /**
     * Gets the numPfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return numPfi
     */
    public java.lang.String getNumPfi() {
        return numPfi;
    }


    /**
     * Sets the numPfi value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param numPfi
     */
    public void setNumPfi(java.lang.String numPfi) {
        this.numPfi = numPfi;
    }


    /**
     * Gets the numTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsTcr value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return obsTcr
     */
    public java.lang.String getObsTcr() {
        return obsTcr;
    }


    /**
     * Sets the obsTcr value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param obsTcr
     */
    public void setObsTcr(java.lang.String obsTcr) {
        this.obsTcr = obsTcr;
    }


    /**
     * Gets the outNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return outNeg
     */
    public java.lang.Double getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.Double outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return perDsc
     */
    public java.lang.Integer getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Integer perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return perJrs
     */
    public java.lang.Integer getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.Integer perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return perMul
     */
    public java.lang.Integer getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.Integer perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the qtdPar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return qtdPar
     */
    public java.lang.Integer getQtdPar() {
        return qtdPar;
    }


    /**
     * Sets the qtdPar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param qtdPar
     */
    public void setQtdPar(java.lang.Integer qtdPar) {
        this.qtdPar = qtdPar;
    }


    /**
     * Gets the tipCar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return tipCar
     */
    public java.lang.String getTipCar() {
        return tipCar;
    }


    /**
     * Sets the tipCar value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param tipCar
     */
    public void setTipCar(java.lang.String tipCar) {
        this.tipCar = tipCar;
    }


    /**
     * Gets the tipJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the titBan value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return titBan
     */
    public java.lang.String getTitBan() {
        return titBan;
    }


    /**
     * Sets the titBan value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param titBan
     */
    public void setTitBan(java.lang.String titBan) {
        this.titBan = titBan;
    }


    /**
     * Gets the tolDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return tolJrs
     */
    public java.lang.Integer getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.Integer tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return tolMul
     */
    public java.lang.Integer getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.Integer tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the vctOri value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vlrJrs
     */
    public java.lang.Double getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.Double vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrOri value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vlrOri
     */
    public java.lang.Double getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.Double vlrOri) {
        this.vlrOri = vlrOri;
    }


    /**
     * Gets the vlrTax value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @return vlrTax
     */
    public java.lang.Double getVlrTax() {
        return vlrTax;
    }


    /**
     * Sets the vlrTax value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.
     * 
     * @param vlrTax
     */
    public void setVlrTax(java.lang.Double vlrTax) {
        this.vlrTax = vlrTax;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos)) return false;
        TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos other = (TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.catExt==null && other.getCatExt()==null) || 
             (this.catExt!=null &&
              this.catExt.equals(other.getCatExt()))) &&
            ((this.catTef==null && other.getCatTef()==null) || 
             (this.catTef!=null &&
              this.catTef.equals(other.getCatTef()))) &&
            ((this.cheAge==null && other.getCheAge()==null) || 
             (this.cheAge!=null &&
              this.cheAge.equals(other.getCheAge()))) &&
            ((this.cheBan==null && other.getCheBan()==null) || 
             (this.cheBan!=null &&
              this.cheBan.equals(other.getCheBan()))) &&
            ((this.cheCta==null && other.getCheCta()==null) || 
             (this.cheCta!=null &&
              this.cheCta.equals(other.getCheCta()))) &&
            ((this.cheNum==null && other.getCheNum()==null) || 
             (this.cheNum!=null &&
              this.cheNum.equals(other.getCheNum()))) &&
            ((this.codBar==null && other.getCodBar()==null) || 
             (this.codBar!=null &&
              this.codBar.equals(other.getCodBar()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCnv==null && other.getCodCnv()==null) || 
             (this.codCnv!=null &&
              this.codCnv.equals(other.getCodCnv()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEqu==null && other.getCodEqu()==null) || 
             (this.codEqu!=null &&
              this.codEqu.equals(other.getCodEqu()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFin==null && other.getCodFin()==null) || 
             (this.codFin!=null &&
              this.codFin.equals(other.getCodFin()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codOpe==null && other.getCodOpe()==null) || 
             (this.codOpe!=null &&
              this.codOpe.equals(other.getCodOpe()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codSac==null && other.getCodSac()==null) || 
             (this.codSac!=null &&
              this.codSac.equals(other.getCodSac()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.croEcf==null && other.getCroEcf()==null) || 
             (this.croEcf!=null &&
              this.croEcf.equals(other.getCroEcf()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.dscNeg==null && other.getDscNeg()==null) || 
             (this.dscNeg!=null &&
              this.dscNeg.equals(other.getDscNeg()))) &&
            ((this.filPfi==null && other.getFilPfi()==null) || 
             (this.filPfi!=null &&
              this.filPfi.equals(other.getFilPfi()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.locTit==null && other.getLocTit()==null) || 
             (this.locTit!=null &&
              this.locTit.equals(other.getLocTit()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.nsuTef==null && other.getNsuTef()==null) || 
             (this.nsuTef!=null &&
              this.nsuTef.equals(other.getNsuTef()))) &&
            ((this.numCfi==null && other.getNumCfi()==null) || 
             (this.numCfi!=null &&
              this.numCfi.equals(other.getNumCfi()))) &&
            ((this.numInt==null && other.getNumInt()==null) || 
             (this.numInt!=null &&
              this.numInt.equals(other.getNumInt()))) &&
            ((this.numPar==null && other.getNumPar()==null) || 
             (this.numPar!=null &&
              this.numPar.equals(other.getNumPar()))) &&
            ((this.numPfi==null && other.getNumPfi()==null) || 
             (this.numPfi!=null &&
              this.numPfi.equals(other.getNumPfi()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsTcr==null && other.getObsTcr()==null) || 
             (this.obsTcr!=null &&
              this.obsTcr.equals(other.getObsTcr()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.qtdPar==null && other.getQtdPar()==null) || 
             (this.qtdPar!=null &&
              this.qtdPar.equals(other.getQtdPar()))) &&
            ((this.tipCar==null && other.getTipCar()==null) || 
             (this.tipCar!=null &&
              this.tipCar.equals(other.getTipCar()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.titBan==null && other.getTitBan()==null) || 
             (this.titBan!=null &&
              this.titBan.equals(other.getTitBan()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri()))) &&
            ((this.vlrTax==null && other.getVlrTax()==null) || 
             (this.vlrTax!=null &&
              this.vlrTax.equals(other.getVlrTax())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCatExt() != null) {
            _hashCode += getCatExt().hashCode();
        }
        if (getCatTef() != null) {
            _hashCode += getCatTef().hashCode();
        }
        if (getCheAge() != null) {
            _hashCode += getCheAge().hashCode();
        }
        if (getCheBan() != null) {
            _hashCode += getCheBan().hashCode();
        }
        if (getCheCta() != null) {
            _hashCode += getCheCta().hashCode();
        }
        if (getCheNum() != null) {
            _hashCode += getCheNum().hashCode();
        }
        if (getCodBar() != null) {
            _hashCode += getCodBar().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCnv() != null) {
            _hashCode += getCodCnv().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEqu() != null) {
            _hashCode += getCodEqu().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFin() != null) {
            _hashCode += getCodFin().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodOpe() != null) {
            _hashCode += getCodOpe().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodSac() != null) {
            _hashCode += getCodSac().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCroEcf() != null) {
            _hashCode += getCroEcf().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getDscNeg() != null) {
            _hashCode += getDscNeg().hashCode();
        }
        if (getFilPfi() != null) {
            _hashCode += getFilPfi().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getLocTit() != null) {
            _hashCode += getLocTit().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNsuTef() != null) {
            _hashCode += getNsuTef().hashCode();
        }
        if (getNumCfi() != null) {
            _hashCode += getNumCfi().hashCode();
        }
        if (getNumInt() != null) {
            _hashCode += getNumInt().hashCode();
        }
        if (getNumPar() != null) {
            _hashCode += getNumPar().hashCode();
        }
        if (getNumPfi() != null) {
            _hashCode += getNumPfi().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsTcr() != null) {
            _hashCode += getObsTcr().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getQtdPar() != null) {
            _hashCode += getQtdPar().hashCode();
        }
        if (getTipCar() != null) {
            _hashCode += getTipCar().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTitBan() != null) {
            _hashCode += getTitBan().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        if (getVlrTax() != null) {
            _hashCode += getVlrTax().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCRVarejoInSubstituicaoTitulosSubstitutos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCnv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCnv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEqu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEqu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codOpe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codOpe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("croEcf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "croEcf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dscNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dscNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filPfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filPfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "locTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nsuTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nsuTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPfi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPfi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdPar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdPar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipCar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipCar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrTax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
