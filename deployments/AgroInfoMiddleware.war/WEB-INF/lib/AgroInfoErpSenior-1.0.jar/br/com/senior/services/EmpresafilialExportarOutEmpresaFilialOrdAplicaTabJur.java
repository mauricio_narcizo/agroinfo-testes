/**
 * EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur  implements java.io.Serializable {
    private java.lang.Integer aplTju;

    private java.lang.Integer ordTju;

    public EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur() {
    }

    public EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur(
           java.lang.Integer aplTju,
           java.lang.Integer ordTju) {
           this.aplTju = aplTju;
           this.ordTju = ordTju;
    }


    /**
     * Gets the aplTju value for this EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.
     * 
     * @return aplTju
     */
    public java.lang.Integer getAplTju() {
        return aplTju;
    }


    /**
     * Sets the aplTju value for this EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.
     * 
     * @param aplTju
     */
    public void setAplTju(java.lang.Integer aplTju) {
        this.aplTju = aplTju;
    }


    /**
     * Gets the ordTju value for this EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.
     * 
     * @return ordTju
     */
    public java.lang.Integer getOrdTju() {
        return ordTju;
    }


    /**
     * Sets the ordTju value for this EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.
     * 
     * @param ordTju
     */
    public void setOrdTju(java.lang.Integer ordTju) {
        this.ordTju = ordTju;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur)) return false;
        EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur other = (EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.aplTju==null && other.getAplTju()==null) || 
             (this.aplTju!=null &&
              this.aplTju.equals(other.getAplTju()))) &&
            ((this.ordTju==null && other.getOrdTju()==null) || 
             (this.ordTju!=null &&
              this.ordTju.equals(other.getOrdTju())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAplTju() != null) {
            _hashCode += getAplTju().hashCode();
        }
        if (getOrdTju() != null) {
            _hashCode += getOrdTju().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialOrdAplicaTabJur"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aplTju");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aplTju"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordTju");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordTju"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
