package br.com.senior.agroinfo.ws.client;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

import br.com.senior.agroinfo.enums.TipoIntegracao;
import br.com.senior.agroinfo.exceptions.FornecedorWSException;
import br.com.senior.agroinfo.ws.client.CooperadoClient;
import br.com.senior.services.FornecedoresExportarIn;
import br.com.senior.services.FornecedoresExportarOut;

public class CooperadoClientTest {

    private static final String URL = "http://teste33:8080";
    private static final String USUARIO = "suporte";
    private static final String SENHA = "suporte";

    @Test(enabled=false)
    public void deveRetornarCompletosFornecedores() throws RemoteException, ServiceException, FornecedorWSException {

        FornecedoresExportarOut cooperados = CooperadoClient.buscarCooperadosWS(USUARIO, SENHA, URL,
                obterFornecedoresExportarAtivo(TipoIntegracao.TODOS.getSigla()));

        assertNull(cooperados.getGridErros());
        assertNotNull(cooperados.getGridFornecedores());
    }

    @Test(enabled = false)
    public void deveRetornarParcialFornecedores() throws RemoteException, ServiceException, FornecedorWSException {

        FornecedoresExportarOut cooperados = CooperadoClient.buscarCooperadosWS(USUARIO, SENHA, URL,
                obterFornecedoresExportarAtivo(TipoIntegracao.ALTERADOS.getSigla()));

        assertNull(cooperados.getGridErros());
    }

    @Test(enabled=false)
    public void deveFinalizarRegistrosFornecedores() throws RemoteException, ServiceException, FornecedorWSException {
        FornecedoresExportarOut cooperados;
        do {
            cooperados = CooperadoClient.buscarCooperadosWS(USUARIO, SENHA, URL,
                    obterFornecedoresExportarAtivo(TipoIntegracao.ALTERADOS.getSigla()));

            assertNull(cooperados.getGridErros());
        } while (!"S".equals(cooperados.getFinalizaramRegistros()));
    }

    @Test(expectedExceptions = { FornecedorWSException.class },enabled=false)
    public void deveLancarExceptionFornecedorComErro() throws RemoteException, ServiceException, FornecedorWSException {

        CooperadoClient.buscarCooperadosWS(USUARIO, SENHA, URL,
                obterFornecedoresExportarInativo(TipoIntegracao.TODOS.getSigla()));

    }

    private FornecedoresExportarIn obterFornecedoresExportarAtivo(final String tipoIntegracao) {

        final String identificaoSistema = "VAREJOEM";

        return new FornecedoresExportarIn(1, 1, "", "", null, identificaoSistema, 500, tipoIntegracao);
    }

    private FornecedoresExportarIn obterFornecedoresExportarInativo(final String tipoIntegracao) {

        final String identificaoSistema = "MEGASUL";

        return new FornecedoresExportarIn(1, 1, "", "", null, identificaoSistema, 500, tipoIntegracao);
    }

}
