/**
 * FornecedoresImportarOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresImportarOutRetorno  implements java.io.Serializable {
    private java.lang.String cgcCpf;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private br.com.senior.services.FornecedoresImportarOutRetornoDetalhe[] detalhe;

    private java.lang.Integer ideExt;

    private java.lang.String msgRet;

    private java.lang.Integer tipRet;

    public FornecedoresImportarOutRetorno() {
    }

    public FornecedoresImportarOutRetorno(
           java.lang.String cgcCpf,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           br.com.senior.services.FornecedoresImportarOutRetornoDetalhe[] detalhe,
           java.lang.Integer ideExt,
           java.lang.String msgRet,
           java.lang.Integer tipRet) {
           this.cgcCpf = cgcCpf;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.detalhe = detalhe;
           this.ideExt = ideExt;
           this.msgRet = msgRet;
           this.tipRet = tipRet;
    }


    /**
     * Gets the cgcCpf value for this FornecedoresImportarOutRetorno.
     * 
     * @return cgcCpf
     */
    public java.lang.String getCgcCpf() {
        return cgcCpf;
    }


    /**
     * Sets the cgcCpf value for this FornecedoresImportarOutRetorno.
     * 
     * @param cgcCpf
     */
    public void setCgcCpf(java.lang.String cgcCpf) {
        this.cgcCpf = cgcCpf;
    }


    /**
     * Gets the codEmp value for this FornecedoresImportarOutRetorno.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this FornecedoresImportarOutRetorno.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this FornecedoresImportarOutRetorno.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FornecedoresImportarOutRetorno.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this FornecedoresImportarOutRetorno.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresImportarOutRetorno.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the detalhe value for this FornecedoresImportarOutRetorno.
     * 
     * @return detalhe
     */
    public br.com.senior.services.FornecedoresImportarOutRetornoDetalhe[] getDetalhe() {
        return detalhe;
    }


    /**
     * Sets the detalhe value for this FornecedoresImportarOutRetorno.
     * 
     * @param detalhe
     */
    public void setDetalhe(br.com.senior.services.FornecedoresImportarOutRetornoDetalhe[] detalhe) {
        this.detalhe = detalhe;
    }

    public br.com.senior.services.FornecedoresImportarOutRetornoDetalhe getDetalhe(int i) {
        return this.detalhe[i];
    }

    public void setDetalhe(int i, br.com.senior.services.FornecedoresImportarOutRetornoDetalhe _value) {
        this.detalhe[i] = _value;
    }


    /**
     * Gets the ideExt value for this FornecedoresImportarOutRetorno.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this FornecedoresImportarOutRetorno.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the msgRet value for this FornecedoresImportarOutRetorno.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this FornecedoresImportarOutRetorno.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the tipRet value for this FornecedoresImportarOutRetorno.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this FornecedoresImportarOutRetorno.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresImportarOutRetorno)) return false;
        FornecedoresImportarOutRetorno other = (FornecedoresImportarOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cgcCpf==null && other.getCgcCpf()==null) || 
             (this.cgcCpf!=null &&
              this.cgcCpf.equals(other.getCgcCpf()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.detalhe==null && other.getDetalhe()==null) || 
             (this.detalhe!=null &&
              java.util.Arrays.equals(this.detalhe, other.getDetalhe()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCgcCpf() != null) {
            _hashCode += getCgcCpf().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getDetalhe() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalhe());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalhe(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresImportarOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalhe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalhe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOutRetornoDetalhe"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
