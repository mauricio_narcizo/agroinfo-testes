/**
 * ExportacaoempresafilialExportarOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class ExportacaoempresafilialExportarOut  implements java.io.Serializable {
    private br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil[] empFil;

    private java.lang.String erroExecucao;

    public ExportacaoempresafilialExportarOut() {
    }

    public ExportacaoempresafilialExportarOut(
           br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil[] empFil,
           java.lang.String erroExecucao) {
           this.empFil = empFil;
           this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the empFil value for this ExportacaoempresafilialExportarOut.
     * 
     * @return empFil
     */
    public br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil[] getEmpFil() {
        return empFil;
    }


    /**
     * Sets the empFil value for this ExportacaoempresafilialExportarOut.
     * 
     * @param empFil
     */
    public void setEmpFil(br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil[] empFil) {
        this.empFil = empFil;
    }

    public br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil getEmpFil(int i) {
        return this.empFil[i];
    }

    public void setEmpFil(int i, br.com.senior.services.ExportacaoempresafilialExportarOutEmpFil _value) {
        this.empFil[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this ExportacaoempresafilialExportarOut.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this ExportacaoempresafilialExportarOut.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExportacaoempresafilialExportarOut)) return false;
        ExportacaoempresafilialExportarOut other = (ExportacaoempresafilialExportarOut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.empFil==null && other.getEmpFil()==null) || 
             (this.empFil!=null &&
              java.util.Arrays.equals(this.empFil, other.getEmpFil()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmpFil() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmpFil());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmpFil(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExportacaoempresafilialExportarOut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "exportacaoempresafilialExportarOut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "exportacaoempresafilialExportarOutEmpFil"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
