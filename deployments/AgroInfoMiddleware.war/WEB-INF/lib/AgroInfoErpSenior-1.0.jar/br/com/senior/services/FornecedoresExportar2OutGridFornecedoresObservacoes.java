/**
 * FornecedoresExportar2OutGridFornecedoresObservacoes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportar2OutGridFornecedoresObservacoes  implements java.io.Serializable {
    private java.lang.Integer codFor;

    private java.lang.String obsDat;

    private java.lang.String obsFor;

    private java.lang.String obsHor;

    private java.lang.Double obsUsu;

    private java.lang.Integer seqObs;

    private java.lang.String sitObs;

    private java.lang.String solDat;

    private java.lang.String solHor;

    private java.lang.String solObs;

    private java.lang.Double solUsu;

    private java.lang.String tipObs;

    public FornecedoresExportar2OutGridFornecedoresObservacoes() {
    }

    public FornecedoresExportar2OutGridFornecedoresObservacoes(
           java.lang.Integer codFor,
           java.lang.String obsDat,
           java.lang.String obsFor,
           java.lang.String obsHor,
           java.lang.Double obsUsu,
           java.lang.Integer seqObs,
           java.lang.String sitObs,
           java.lang.String solDat,
           java.lang.String solHor,
           java.lang.String solObs,
           java.lang.Double solUsu,
           java.lang.String tipObs) {
           this.codFor = codFor;
           this.obsDat = obsDat;
           this.obsFor = obsFor;
           this.obsHor = obsHor;
           this.obsUsu = obsUsu;
           this.seqObs = seqObs;
           this.sitObs = sitObs;
           this.solDat = solDat;
           this.solHor = solHor;
           this.solObs = solObs;
           this.solUsu = solUsu;
           this.tipObs = tipObs;
    }


    /**
     * Gets the codFor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the obsDat value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return obsDat
     */
    public java.lang.String getObsDat() {
        return obsDat;
    }


    /**
     * Sets the obsDat value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param obsDat
     */
    public void setObsDat(java.lang.String obsDat) {
        this.obsDat = obsDat;
    }


    /**
     * Gets the obsFor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return obsFor
     */
    public java.lang.String getObsFor() {
        return obsFor;
    }


    /**
     * Sets the obsFor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param obsFor
     */
    public void setObsFor(java.lang.String obsFor) {
        this.obsFor = obsFor;
    }


    /**
     * Gets the obsHor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return obsHor
     */
    public java.lang.String getObsHor() {
        return obsHor;
    }


    /**
     * Sets the obsHor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param obsHor
     */
    public void setObsHor(java.lang.String obsHor) {
        this.obsHor = obsHor;
    }


    /**
     * Gets the obsUsu value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return obsUsu
     */
    public java.lang.Double getObsUsu() {
        return obsUsu;
    }


    /**
     * Sets the obsUsu value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param obsUsu
     */
    public void setObsUsu(java.lang.Double obsUsu) {
        this.obsUsu = obsUsu;
    }


    /**
     * Gets the seqObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return seqObs
     */
    public java.lang.Integer getSeqObs() {
        return seqObs;
    }


    /**
     * Sets the seqObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param seqObs
     */
    public void setSeqObs(java.lang.Integer seqObs) {
        this.seqObs = seqObs;
    }


    /**
     * Gets the sitObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return sitObs
     */
    public java.lang.String getSitObs() {
        return sitObs;
    }


    /**
     * Sets the sitObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param sitObs
     */
    public void setSitObs(java.lang.String sitObs) {
        this.sitObs = sitObs;
    }


    /**
     * Gets the solDat value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return solDat
     */
    public java.lang.String getSolDat() {
        return solDat;
    }


    /**
     * Sets the solDat value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param solDat
     */
    public void setSolDat(java.lang.String solDat) {
        this.solDat = solDat;
    }


    /**
     * Gets the solHor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return solHor
     */
    public java.lang.String getSolHor() {
        return solHor;
    }


    /**
     * Sets the solHor value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param solHor
     */
    public void setSolHor(java.lang.String solHor) {
        this.solHor = solHor;
    }


    /**
     * Gets the solObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return solObs
     */
    public java.lang.String getSolObs() {
        return solObs;
    }


    /**
     * Sets the solObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param solObs
     */
    public void setSolObs(java.lang.String solObs) {
        this.solObs = solObs;
    }


    /**
     * Gets the solUsu value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return solUsu
     */
    public java.lang.Double getSolUsu() {
        return solUsu;
    }


    /**
     * Sets the solUsu value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param solUsu
     */
    public void setSolUsu(java.lang.Double solUsu) {
        this.solUsu = solUsu;
    }


    /**
     * Gets the tipObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @return tipObs
     */
    public java.lang.String getTipObs() {
        return tipObs;
    }


    /**
     * Sets the tipObs value for this FornecedoresExportar2OutGridFornecedoresObservacoes.
     * 
     * @param tipObs
     */
    public void setTipObs(java.lang.String tipObs) {
        this.tipObs = tipObs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportar2OutGridFornecedoresObservacoes)) return false;
        FornecedoresExportar2OutGridFornecedoresObservacoes other = (FornecedoresExportar2OutGridFornecedoresObservacoes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.obsDat==null && other.getObsDat()==null) || 
             (this.obsDat!=null &&
              this.obsDat.equals(other.getObsDat()))) &&
            ((this.obsFor==null && other.getObsFor()==null) || 
             (this.obsFor!=null &&
              this.obsFor.equals(other.getObsFor()))) &&
            ((this.obsHor==null && other.getObsHor()==null) || 
             (this.obsHor!=null &&
              this.obsHor.equals(other.getObsHor()))) &&
            ((this.obsUsu==null && other.getObsUsu()==null) || 
             (this.obsUsu!=null &&
              this.obsUsu.equals(other.getObsUsu()))) &&
            ((this.seqObs==null && other.getSeqObs()==null) || 
             (this.seqObs!=null &&
              this.seqObs.equals(other.getSeqObs()))) &&
            ((this.sitObs==null && other.getSitObs()==null) || 
             (this.sitObs!=null &&
              this.sitObs.equals(other.getSitObs()))) &&
            ((this.solDat==null && other.getSolDat()==null) || 
             (this.solDat!=null &&
              this.solDat.equals(other.getSolDat()))) &&
            ((this.solHor==null && other.getSolHor()==null) || 
             (this.solHor!=null &&
              this.solHor.equals(other.getSolHor()))) &&
            ((this.solObs==null && other.getSolObs()==null) || 
             (this.solObs!=null &&
              this.solObs.equals(other.getSolObs()))) &&
            ((this.solUsu==null && other.getSolUsu()==null) || 
             (this.solUsu!=null &&
              this.solUsu.equals(other.getSolUsu()))) &&
            ((this.tipObs==null && other.getTipObs()==null) || 
             (this.tipObs!=null &&
              this.tipObs.equals(other.getTipObs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getObsDat() != null) {
            _hashCode += getObsDat().hashCode();
        }
        if (getObsFor() != null) {
            _hashCode += getObsFor().hashCode();
        }
        if (getObsHor() != null) {
            _hashCode += getObsHor().hashCode();
        }
        if (getObsUsu() != null) {
            _hashCode += getObsUsu().hashCode();
        }
        if (getSeqObs() != null) {
            _hashCode += getSeqObs().hashCode();
        }
        if (getSitObs() != null) {
            _hashCode += getSitObs().hashCode();
        }
        if (getSolDat() != null) {
            _hashCode += getSolDat().hashCode();
        }
        if (getSolHor() != null) {
            _hashCode += getSolHor().hashCode();
        }
        if (getSolObs() != null) {
            _hashCode += getSolObs().hashCode();
        }
        if (getSolUsu() != null) {
            _hashCode += getSolUsu().hashCode();
        }
        if (getTipObs() != null) {
            _hashCode += getTipObs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportar2OutGridFornecedoresObservacoes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresObservacoes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsUsu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsUsu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solDat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "solDat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solHor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "solHor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "solObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solUsu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "solUsu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipObs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipObs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
