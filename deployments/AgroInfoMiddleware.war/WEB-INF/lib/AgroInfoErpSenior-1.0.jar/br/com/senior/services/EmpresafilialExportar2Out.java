/**
 * EmpresafilialExportar2Out.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar2Out  implements java.io.Serializable {
    private br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial[] empresaFilial;

    private java.lang.String erroExecucao;

    private java.lang.String finalizaramRegistros;

    private br.com.senior.services.EmpresafilialExportar2OutGridErros[] gridErros;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer numeroLote;

    private java.lang.Integer tipoRetorno;

    public EmpresafilialExportar2Out() {
    }

    public EmpresafilialExportar2Out(
           br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial[] empresaFilial,
           java.lang.String erroExecucao,
           java.lang.String finalizaramRegistros,
           br.com.senior.services.EmpresafilialExportar2OutGridErros[] gridErros,
           java.lang.String mensagemRetorno,
           java.lang.Integer numeroLote,
           java.lang.Integer tipoRetorno) {
           this.empresaFilial = empresaFilial;
           this.erroExecucao = erroExecucao;
           this.finalizaramRegistros = finalizaramRegistros;
           this.gridErros = gridErros;
           this.mensagemRetorno = mensagemRetorno;
           this.numeroLote = numeroLote;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the empresaFilial value for this EmpresafilialExportar2Out.
     * 
     * @return empresaFilial
     */
    public br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial[] getEmpresaFilial() {
        return empresaFilial;
    }


    /**
     * Sets the empresaFilial value for this EmpresafilialExportar2Out.
     * 
     * @param empresaFilial
     */
    public void setEmpresaFilial(br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial[] empresaFilial) {
        this.empresaFilial = empresaFilial;
    }

    public br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial getEmpresaFilial(int i) {
        return this.empresaFilial[i];
    }

    public void setEmpresaFilial(int i, br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial _value) {
        this.empresaFilial[i] = _value;
    }


    /**
     * Gets the erroExecucao value for this EmpresafilialExportar2Out.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this EmpresafilialExportar2Out.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the finalizaramRegistros value for this EmpresafilialExportar2Out.
     * 
     * @return finalizaramRegistros
     */
    public java.lang.String getFinalizaramRegistros() {
        return finalizaramRegistros;
    }


    /**
     * Sets the finalizaramRegistros value for this EmpresafilialExportar2Out.
     * 
     * @param finalizaramRegistros
     */
    public void setFinalizaramRegistros(java.lang.String finalizaramRegistros) {
        this.finalizaramRegistros = finalizaramRegistros;
    }


    /**
     * Gets the gridErros value for this EmpresafilialExportar2Out.
     * 
     * @return gridErros
     */
    public br.com.senior.services.EmpresafilialExportar2OutGridErros[] getGridErros() {
        return gridErros;
    }


    /**
     * Sets the gridErros value for this EmpresafilialExportar2Out.
     * 
     * @param gridErros
     */
    public void setGridErros(br.com.senior.services.EmpresafilialExportar2OutGridErros[] gridErros) {
        this.gridErros = gridErros;
    }

    public br.com.senior.services.EmpresafilialExportar2OutGridErros getGridErros(int i) {
        return this.gridErros[i];
    }

    public void setGridErros(int i, br.com.senior.services.EmpresafilialExportar2OutGridErros _value) {
        this.gridErros[i] = _value;
    }


    /**
     * Gets the mensagemRetorno value for this EmpresafilialExportar2Out.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this EmpresafilialExportar2Out.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the numeroLote value for this EmpresafilialExportar2Out.
     * 
     * @return numeroLote
     */
    public java.lang.Integer getNumeroLote() {
        return numeroLote;
    }


    /**
     * Sets the numeroLote value for this EmpresafilialExportar2Out.
     * 
     * @param numeroLote
     */
    public void setNumeroLote(java.lang.Integer numeroLote) {
        this.numeroLote = numeroLote;
    }


    /**
     * Gets the tipoRetorno value for this EmpresafilialExportar2Out.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this EmpresafilialExportar2Out.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar2Out)) return false;
        EmpresafilialExportar2Out other = (EmpresafilialExportar2Out) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.empresaFilial==null && other.getEmpresaFilial()==null) || 
             (this.empresaFilial!=null &&
              java.util.Arrays.equals(this.empresaFilial, other.getEmpresaFilial()))) &&
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.finalizaramRegistros==null && other.getFinalizaramRegistros()==null) || 
             (this.finalizaramRegistros!=null &&
              this.finalizaramRegistros.equals(other.getFinalizaramRegistros()))) &&
            ((this.gridErros==null && other.getGridErros()==null) || 
             (this.gridErros!=null &&
              java.util.Arrays.equals(this.gridErros, other.getGridErros()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.numeroLote==null && other.getNumeroLote()==null) || 
             (this.numeroLote!=null &&
              this.numeroLote.equals(other.getNumeroLote()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmpresaFilial() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmpresaFilial());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmpresaFilial(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getFinalizaramRegistros() != null) {
            _hashCode += getFinalizaramRegistros().hashCode();
        }
        if (getGridErros() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGridErros());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGridErros(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getNumeroLote() != null) {
            _hashCode += getNumeroLote().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar2Out.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2Out"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresaFilial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empresaFilial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilial"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finalizaramRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "finalizaramRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gridErros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gridErros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutGridErros"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroLote");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numeroLote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
