/**
 * TitulosGravarTitulosCPVarejoInTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosGravarTitulosCPVarejoInTitulos  implements java.io.Serializable {
    private java.lang.String ccbFor;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codBar;

    private java.lang.String codCcu;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFor;

    private java.lang.String codFpg;

    private java.lang.String codMoe;

    private java.lang.String codMpt;

    private java.lang.String codNtg;

    private java.lang.String codPor;

    private java.lang.String codTpt;

    private java.lang.String codTri;

    private java.lang.String cotEmi;

    private java.lang.String ctaFin;

    private java.lang.String ctaRed;

    private java.lang.String datDsc;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datNeg;

    private java.lang.String datPpt;

    private java.lang.Integer ideExt;

    private java.lang.Double jrsDia;

    private java.lang.Double jrsNeg;

    private java.lang.Double mulNeg;

    private java.lang.String numArb;

    private java.lang.String numTit;

    private java.lang.String obsTcp;

    private java.lang.Double outNeg;

    private java.lang.Double perDsc;

    private java.lang.Double perJrs;

    private java.lang.Double perMul;

    private java.lang.String tipJrs;

    private java.lang.String titBan;

    private java.lang.Integer tolDsc;

    private java.lang.Integer tolJrs;

    private java.lang.Integer tolMul;

    private java.lang.Integer usuSit;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrOri;

    public TitulosGravarTitulosCPVarejoInTitulos() {
    }

    public TitulosGravarTitulosCPVarejoInTitulos(
           java.lang.String ccbFor,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codBar,
           java.lang.String codCcu,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFor,
           java.lang.String codFpg,
           java.lang.String codMoe,
           java.lang.String codMpt,
           java.lang.String codNtg,
           java.lang.String codPor,
           java.lang.String codTpt,
           java.lang.String codTri,
           java.lang.String cotEmi,
           java.lang.String ctaFin,
           java.lang.String ctaRed,
           java.lang.String datDsc,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datNeg,
           java.lang.String datPpt,
           java.lang.Integer ideExt,
           java.lang.Double jrsDia,
           java.lang.Double jrsNeg,
           java.lang.Double mulNeg,
           java.lang.String numArb,
           java.lang.String numTit,
           java.lang.String obsTcp,
           java.lang.Double outNeg,
           java.lang.Double perDsc,
           java.lang.Double perJrs,
           java.lang.Double perMul,
           java.lang.String tipJrs,
           java.lang.String titBan,
           java.lang.Integer tolDsc,
           java.lang.Integer tolJrs,
           java.lang.Integer tolMul,
           java.lang.Integer usuSit,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.Double vlrDsc,
           java.lang.Double vlrOri) {
           this.ccbFor = ccbFor;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codBar = codBar;
           this.codCcu = codCcu;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFor = codFor;
           this.codFpg = codFpg;
           this.codMoe = codMoe;
           this.codMpt = codMpt;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codTpt = codTpt;
           this.codTri = codTri;
           this.cotEmi = cotEmi;
           this.ctaFin = ctaFin;
           this.ctaRed = ctaRed;
           this.datDsc = datDsc;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datNeg = datNeg;
           this.datPpt = datPpt;
           this.ideExt = ideExt;
           this.jrsDia = jrsDia;
           this.jrsNeg = jrsNeg;
           this.mulNeg = mulNeg;
           this.numArb = numArb;
           this.numTit = numTit;
           this.obsTcp = obsTcp;
           this.outNeg = outNeg;
           this.perDsc = perDsc;
           this.perJrs = perJrs;
           this.perMul = perMul;
           this.tipJrs = tipJrs;
           this.titBan = titBan;
           this.tolDsc = tolDsc;
           this.tolJrs = tolJrs;
           this.tolMul = tolMul;
           this.usuSit = usuSit;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrDsc = vlrDsc;
           this.vlrOri = vlrOri;
    }


    /**
     * Gets the ccbFor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the codAge value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codBar value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codBar
     */
    public java.lang.String getCodBar() {
        return codBar;
    }


    /**
     * Sets the codBar value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codBar
     */
    public void setCodBar(java.lang.String codBar) {
        this.codBar = codBar;
    }


    /**
     * Gets the codCcu value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codCcu
     */
    public java.lang.String getCodCcu() {
        return codCcu;
    }


    /**
     * Sets the codCcu value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codCcu
     */
    public void setCodCcu(java.lang.String codCcu) {
        this.codCcu = codCcu;
    }


    /**
     * Gets the codCrp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codEmp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codFpg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codFpg
     */
    public java.lang.String getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.String codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codMoe value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codMoe
     */
    public java.lang.String getCodMoe() {
        return codMoe;
    }


    /**
     * Sets the codMoe value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codMoe
     */
    public void setCodMoe(java.lang.String codMoe) {
        this.codMoe = codMoe;
    }


    /**
     * Gets the codMpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codMpt
     */
    public java.lang.String getCodMpt() {
        return codMpt;
    }


    /**
     * Sets the codMpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codMpt
     */
    public void setCodMpt(java.lang.String codMpt) {
        this.codMpt = codMpt;
    }


    /**
     * Gets the codNtg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codNtg
     */
    public java.lang.String getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.String codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the codTri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return codTri
     */
    public java.lang.String getCodTri() {
        return codTri;
    }


    /**
     * Sets the codTri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param codTri
     */
    public void setCodTri(java.lang.String codTri) {
        this.codTri = codTri;
    }


    /**
     * Gets the cotEmi value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return cotEmi
     */
    public java.lang.String getCotEmi() {
        return cotEmi;
    }


    /**
     * Sets the cotEmi value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param cotEmi
     */
    public void setCotEmi(java.lang.String cotEmi) {
        this.cotEmi = cotEmi;
    }


    /**
     * Gets the ctaFin value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return ctaFin
     */
    public java.lang.String getCtaFin() {
        return ctaFin;
    }


    /**
     * Sets the ctaFin value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param ctaFin
     */
    public void setCtaFin(java.lang.String ctaFin) {
        this.ctaFin = ctaFin;
    }


    /**
     * Gets the ctaRed value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return ctaRed
     */
    public java.lang.String getCtaRed() {
        return ctaRed;
    }


    /**
     * Sets the ctaRed value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param ctaRed
     */
    public void setCtaRed(java.lang.String ctaRed) {
        this.ctaRed = ctaRed;
    }


    /**
     * Gets the datDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return datDsc
     */
    public java.lang.String getDatDsc() {
        return datDsc;
    }


    /**
     * Sets the datDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param datDsc
     */
    public void setDatDsc(java.lang.String datDsc) {
        this.datDsc = datDsc;
    }


    /**
     * Gets the datEmi value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return datNeg
     */
    public java.lang.String getDatNeg() {
        return datNeg;
    }


    /**
     * Sets the datNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param datNeg
     */
    public void setDatNeg(java.lang.String datNeg) {
        this.datNeg = datNeg;
    }


    /**
     * Gets the datPpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the ideExt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the jrsDia value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return jrsDia
     */
    public java.lang.Double getJrsDia() {
        return jrsDia;
    }


    /**
     * Sets the jrsDia value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param jrsDia
     */
    public void setJrsDia(java.lang.Double jrsDia) {
        this.jrsDia = jrsDia;
    }


    /**
     * Gets the jrsNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return jrsNeg
     */
    public java.lang.Double getJrsNeg() {
        return jrsNeg;
    }


    /**
     * Sets the jrsNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param jrsNeg
     */
    public void setJrsNeg(java.lang.Double jrsNeg) {
        this.jrsNeg = jrsNeg;
    }


    /**
     * Gets the mulNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return mulNeg
     */
    public java.lang.Double getMulNeg() {
        return mulNeg;
    }


    /**
     * Sets the mulNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param mulNeg
     */
    public void setMulNeg(java.lang.Double mulNeg) {
        this.mulNeg = mulNeg;
    }


    /**
     * Gets the numArb value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return numArb
     */
    public java.lang.String getNumArb() {
        return numArb;
    }


    /**
     * Sets the numArb value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param numArb
     */
    public void setNumArb(java.lang.String numArb) {
        this.numArb = numArb;
    }


    /**
     * Gets the numTit value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsTcp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return obsTcp
     */
    public java.lang.String getObsTcp() {
        return obsTcp;
    }


    /**
     * Sets the obsTcp value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param obsTcp
     */
    public void setObsTcp(java.lang.String obsTcp) {
        this.obsTcp = obsTcp;
    }


    /**
     * Gets the outNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return outNeg
     */
    public java.lang.Double getOutNeg() {
        return outNeg;
    }


    /**
     * Sets the outNeg value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param outNeg
     */
    public void setOutNeg(java.lang.Double outNeg) {
        this.outNeg = outNeg;
    }


    /**
     * Gets the perDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return perJrs
     */
    public java.lang.Double getPerJrs() {
        return perJrs;
    }


    /**
     * Sets the perJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param perJrs
     */
    public void setPerJrs(java.lang.Double perJrs) {
        this.perJrs = perJrs;
    }


    /**
     * Gets the perMul value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return perMul
     */
    public java.lang.Double getPerMul() {
        return perMul;
    }


    /**
     * Sets the perMul value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param perMul
     */
    public void setPerMul(java.lang.Double perMul) {
        this.perMul = perMul;
    }


    /**
     * Gets the tipJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return tipJrs
     */
    public java.lang.String getTipJrs() {
        return tipJrs;
    }


    /**
     * Sets the tipJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param tipJrs
     */
    public void setTipJrs(java.lang.String tipJrs) {
        this.tipJrs = tipJrs;
    }


    /**
     * Gets the titBan value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return titBan
     */
    public java.lang.String getTitBan() {
        return titBan;
    }


    /**
     * Sets the titBan value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param titBan
     */
    public void setTitBan(java.lang.String titBan) {
        this.titBan = titBan;
    }


    /**
     * Gets the tolDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }


    /**
     * Gets the tolJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return tolJrs
     */
    public java.lang.Integer getTolJrs() {
        return tolJrs;
    }


    /**
     * Sets the tolJrs value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param tolJrs
     */
    public void setTolJrs(java.lang.Integer tolJrs) {
        this.tolJrs = tolJrs;
    }


    /**
     * Gets the tolMul value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return tolMul
     */
    public java.lang.Integer getTolMul() {
        return tolMul;
    }


    /**
     * Sets the tolMul value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param tolMul
     */
    public void setTolMul(java.lang.Integer tolMul) {
        this.tolMul = tolMul;
    }


    /**
     * Gets the usuSit value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return usuSit
     */
    public java.lang.Integer getUsuSit() {
        return usuSit;
    }


    /**
     * Sets the usuSit value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param usuSit
     */
    public void setUsuSit(java.lang.Integer usuSit) {
        this.usuSit = usuSit;
    }


    /**
     * Gets the vctOri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrOri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @return vlrOri
     */
    public java.lang.Double getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosGravarTitulosCPVarejoInTitulos.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.Double vlrOri) {
        this.vlrOri = vlrOri;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosGravarTitulosCPVarejoInTitulos)) return false;
        TitulosGravarTitulosCPVarejoInTitulos other = (TitulosGravarTitulosCPVarejoInTitulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codBar==null && other.getCodBar()==null) || 
             (this.codBar!=null &&
              this.codBar.equals(other.getCodBar()))) &&
            ((this.codCcu==null && other.getCodCcu()==null) || 
             (this.codCcu!=null &&
              this.codCcu.equals(other.getCodCcu()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codMoe==null && other.getCodMoe()==null) || 
             (this.codMoe!=null &&
              this.codMoe.equals(other.getCodMoe()))) &&
            ((this.codMpt==null && other.getCodMpt()==null) || 
             (this.codMpt!=null &&
              this.codMpt.equals(other.getCodMpt()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.codTri==null && other.getCodTri()==null) || 
             (this.codTri!=null &&
              this.codTri.equals(other.getCodTri()))) &&
            ((this.cotEmi==null && other.getCotEmi()==null) || 
             (this.cotEmi!=null &&
              this.cotEmi.equals(other.getCotEmi()))) &&
            ((this.ctaFin==null && other.getCtaFin()==null) || 
             (this.ctaFin!=null &&
              this.ctaFin.equals(other.getCtaFin()))) &&
            ((this.ctaRed==null && other.getCtaRed()==null) || 
             (this.ctaRed!=null &&
              this.ctaRed.equals(other.getCtaRed()))) &&
            ((this.datDsc==null && other.getDatDsc()==null) || 
             (this.datDsc!=null &&
              this.datDsc.equals(other.getDatDsc()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datNeg==null && other.getDatNeg()==null) || 
             (this.datNeg!=null &&
              this.datNeg.equals(other.getDatNeg()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.jrsDia==null && other.getJrsDia()==null) || 
             (this.jrsDia!=null &&
              this.jrsDia.equals(other.getJrsDia()))) &&
            ((this.jrsNeg==null && other.getJrsNeg()==null) || 
             (this.jrsNeg!=null &&
              this.jrsNeg.equals(other.getJrsNeg()))) &&
            ((this.mulNeg==null && other.getMulNeg()==null) || 
             (this.mulNeg!=null &&
              this.mulNeg.equals(other.getMulNeg()))) &&
            ((this.numArb==null && other.getNumArb()==null) || 
             (this.numArb!=null &&
              this.numArb.equals(other.getNumArb()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsTcp==null && other.getObsTcp()==null) || 
             (this.obsTcp!=null &&
              this.obsTcp.equals(other.getObsTcp()))) &&
            ((this.outNeg==null && other.getOutNeg()==null) || 
             (this.outNeg!=null &&
              this.outNeg.equals(other.getOutNeg()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perJrs==null && other.getPerJrs()==null) || 
             (this.perJrs!=null &&
              this.perJrs.equals(other.getPerJrs()))) &&
            ((this.perMul==null && other.getPerMul()==null) || 
             (this.perMul!=null &&
              this.perMul.equals(other.getPerMul()))) &&
            ((this.tipJrs==null && other.getTipJrs()==null) || 
             (this.tipJrs!=null &&
              this.tipJrs.equals(other.getTipJrs()))) &&
            ((this.titBan==null && other.getTitBan()==null) || 
             (this.titBan!=null &&
              this.titBan.equals(other.getTitBan()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc()))) &&
            ((this.tolJrs==null && other.getTolJrs()==null) || 
             (this.tolJrs!=null &&
              this.tolJrs.equals(other.getTolJrs()))) &&
            ((this.tolMul==null && other.getTolMul()==null) || 
             (this.tolMul!=null &&
              this.tolMul.equals(other.getTolMul()))) &&
            ((this.usuSit==null && other.getUsuSit()==null) || 
             (this.usuSit!=null &&
              this.usuSit.equals(other.getUsuSit()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodBar() != null) {
            _hashCode += getCodBar().hashCode();
        }
        if (getCodCcu() != null) {
            _hashCode += getCodCcu().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodMoe() != null) {
            _hashCode += getCodMoe().hashCode();
        }
        if (getCodMpt() != null) {
            _hashCode += getCodMpt().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getCodTri() != null) {
            _hashCode += getCodTri().hashCode();
        }
        if (getCotEmi() != null) {
            _hashCode += getCotEmi().hashCode();
        }
        if (getCtaFin() != null) {
            _hashCode += getCtaFin().hashCode();
        }
        if (getCtaRed() != null) {
            _hashCode += getCtaRed().hashCode();
        }
        if (getDatDsc() != null) {
            _hashCode += getDatDsc().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatNeg() != null) {
            _hashCode += getDatNeg().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getJrsDia() != null) {
            _hashCode += getJrsDia().hashCode();
        }
        if (getJrsNeg() != null) {
            _hashCode += getJrsNeg().hashCode();
        }
        if (getMulNeg() != null) {
            _hashCode += getMulNeg().hashCode();
        }
        if (getNumArb() != null) {
            _hashCode += getNumArb().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsTcp() != null) {
            _hashCode += getObsTcp().hashCode();
        }
        if (getOutNeg() != null) {
            _hashCode += getOutNeg().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerJrs() != null) {
            _hashCode += getPerJrs().hashCode();
        }
        if (getPerMul() != null) {
            _hashCode += getPerMul().hashCode();
        }
        if (getTipJrs() != null) {
            _hashCode += getTipJrs().hashCode();
        }
        if (getTitBan() != null) {
            _hashCode += getTitBan().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        if (getTolJrs() != null) {
            _hashCode += getTolJrs().hashCode();
        }
        if (getTolMul() != null) {
            _hashCode += getTolMul().hashCode();
        }
        if (getUsuSit() != null) {
            _hashCode += getUsuSit().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosGravarTitulosCPVarejoInTitulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosGravarTitulosCPVarejoInTitulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCcu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCcu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cotEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cotEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaFin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaFin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaRed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctaRed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsDia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsDia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jrsNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jrsNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numArb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numArb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outNeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outNeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuSit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuSit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
