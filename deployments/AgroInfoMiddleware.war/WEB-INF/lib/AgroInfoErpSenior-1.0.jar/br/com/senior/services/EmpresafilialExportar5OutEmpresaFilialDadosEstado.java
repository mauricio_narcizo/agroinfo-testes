/**
 * EmpresafilialExportar5OutEmpresaFilialDadosEstado.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar5OutEmpresaFilialDadosEstado  implements java.io.Serializable {
    private java.lang.String insEst;

    private java.lang.String sigUfs;

    public EmpresafilialExportar5OutEmpresaFilialDadosEstado() {
    }

    public EmpresafilialExportar5OutEmpresaFilialDadosEstado(
           java.lang.String insEst,
           java.lang.String sigUfs) {
           this.insEst = insEst;
           this.sigUfs = sigUfs;
    }


    /**
     * Gets the insEst value for this EmpresafilialExportar5OutEmpresaFilialDadosEstado.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this EmpresafilialExportar5OutEmpresaFilialDadosEstado.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the sigUfs value for this EmpresafilialExportar5OutEmpresaFilialDadosEstado.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this EmpresafilialExportar5OutEmpresaFilialDadosEstado.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar5OutEmpresaFilialDadosEstado)) return false;
        EmpresafilialExportar5OutEmpresaFilialDadosEstado other = (EmpresafilialExportar5OutEmpresaFilialDadosEstado) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar5OutEmpresaFilialDadosEstado.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialDadosEstado"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
