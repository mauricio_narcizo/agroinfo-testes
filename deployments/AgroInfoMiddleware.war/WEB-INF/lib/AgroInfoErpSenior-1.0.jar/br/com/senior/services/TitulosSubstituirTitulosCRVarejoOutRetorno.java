/**
 * TitulosSubstituirTitulosCRVarejoOutRetorno.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosSubstituirTitulosCRVarejoOutRetorno  implements java.io.Serializable {
    private java.lang.String chvlot;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes[] detalhes;

    private java.lang.Integer ideExt;

    private java.lang.String msgRet;

    private java.lang.Integer tipRet;

    public TitulosSubstituirTitulosCRVarejoOutRetorno() {
    }

    public TitulosSubstituirTitulosCRVarejoOutRetorno(
           java.lang.String chvlot,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes[] detalhes,
           java.lang.Integer ideExt,
           java.lang.String msgRet,
           java.lang.Integer tipRet) {
           this.chvlot = chvlot;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.detalhes = detalhes;
           this.ideExt = ideExt;
           this.msgRet = msgRet;
           this.tipRet = tipRet;
    }


    /**
     * Gets the chvlot value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return chvlot
     */
    public java.lang.String getChvlot() {
        return chvlot;
    }


    /**
     * Sets the chvlot value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param chvlot
     */
    public void setChvlot(java.lang.String chvlot) {
        this.chvlot = chvlot;
    }


    /**
     * Gets the codEmp value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the detalhes value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return detalhes
     */
    public br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes[] getDetalhes() {
        return detalhes;
    }


    /**
     * Sets the detalhes value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param detalhes
     */
    public void setDetalhes(br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes[] detalhes) {
        this.detalhes = detalhes;
    }

    public br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes getDetalhes(int i) {
        return this.detalhes[i];
    }

    public void setDetalhes(int i, br.com.senior.services.TitulosSubstituirTitulosCRVarejoOutRetornoDetalhes _value) {
        this.detalhes[i] = _value;
    }


    /**
     * Gets the ideExt value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the msgRet value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the tipRet value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this TitulosSubstituirTitulosCRVarejoOutRetorno.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCRVarejoOutRetorno)) return false;
        TitulosSubstituirTitulosCRVarejoOutRetorno other = (TitulosSubstituirTitulosCRVarejoOutRetorno) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chvlot==null && other.getChvlot()==null) || 
             (this.chvlot!=null &&
              this.chvlot.equals(other.getChvlot()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.detalhes==null && other.getDetalhes()==null) || 
             (this.detalhes!=null &&
              java.util.Arrays.equals(this.detalhes, other.getDetalhes()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChvlot() != null) {
            _hashCode += getChvlot().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDetalhes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalhes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalhes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCRVarejoOutRetorno.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCRVarejoOutRetorno"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chvlot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "chvlot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalhes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "detalhes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCRVarejoOutRetornoDetalhes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
