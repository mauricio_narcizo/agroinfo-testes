package br.com.senior.agroinfo.exceptions;

public class FornecedorWSException extends Exception {

    private static final long serialVersionUID = 1L;

    public FornecedorWSException(String msg) {
        super(msg);
    }
}
