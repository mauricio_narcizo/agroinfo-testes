package br.com.senior.agroinfo.ws.client;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.xml.rpc.ServiceException;

import br.com.senior.agroinfo.exceptions.EmpresaFilialWSException;
import br.com.senior.agroinfo.utils.UtilProxy;
import br.com.senior.services.EmpresafilialExportar7In;
import br.com.senior.services.EmpresafilialExportar7Out;
import br.com.senior.services.EmpresafilialExportar7OutEmpresaFilial;
import br.com.senior.services.ExportacaoempresafilialExportarIn;
import br.com.senior.services.ExportacaoempresafilialExportarOut;
import br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_empresafilial;
import br.com.senior.services.Sapiens_Synccom_senior_g5_co_mct_ctb_exportacaoempresafilial;

public class EmpresaFilialClient {

	private EmpresaFilialClient() {
	}

	public static List<EmpresafilialExportar7OutEmpresaFilial> obterEmpresaFilial(final String usuario,
			final String senha, final String url, final String identificacaoSistema) throws EmpresaFilialWSException {

		List<EmpresafilialExportar7OutEmpresaFilial> filiais = new LinkedList<>();

		Stream.of(obterEmpresaFilialId(usuario, senha, url,new ExportacaoempresafilialExportarIn()).getEmpFil())
				.forEach(filialId -> {
					try {
						EmpresafilialExportar7Out empresaFilial = obterEmpresaFilial(usuario, senha, url, 
								new EmpresafilialExportar7In(filialId.getCodEmp(), filialId.getCodFil(), null, null, identificacaoSistema));
						if(empresaFilial.getEmpresaFilial() != null){
							filiais.addAll(Arrays.asList(empresaFilial.getEmpresaFilial()));
						}
					} catch (Exception e) {
						Logger.getLogger("CooperadoClient").log(Level.SEVERE, e.getMessage());
					}
				});

		return filiais;
	}

	private static ExportacaoempresafilialExportarOut obterEmpresaFilialId(final String usuario, final String senha,
			final String url, ExportacaoempresafilialExportarIn parameters)
					throws EmpresaFilialWSException {

		final int encryption = 0;
		ExportacaoempresafilialExportarOut empresaFilialOut;
		try {

			empresaFilialOut = UtilProxy
					.getServiceProxy(Sapiens_Synccom_senior_g5_co_mct_ctb_exportacaoempresafilial.class, url)
					.exportar(usuario, senha, encryption, parameters);

			if (empresaFilialOut.getErroExecucao() != null && !empresaFilialOut.getErroExecucao().isEmpty()) {
				throw new EmpresaFilialWSException(empresaFilialOut.getErroExecucao());
			}
		} catch (RemoteException | ServiceException e) {
			Logger.getLogger("CooperadoClient").log(Level.SEVERE, e.getMessage());
			throw new EmpresaFilialWSException(e.getMessage());
		}
		return empresaFilialOut;
	}

	private static EmpresafilialExportar7Out obterEmpresaFilial(final String usuario, final String senha,
			final String url, EmpresafilialExportar7In parameters) throws EmpresaFilialWSException {

		final int encryption = 0;
		EmpresafilialExportar7Out empresafilialExportar7Out;
		try {
			empresafilialExportar7Out = UtilProxy
					.getServiceProxy(Sapiens_Synccom_senior_g5_co_int_varejo_empresafilial.class, url)
					.exportar_7(usuario, senha, encryption, parameters);
		} catch (RemoteException | ServiceException e) {
			Logger.getLogger("CooperadoClient").log(Level.SEVERE, e.getMessage());
			throw new EmpresaFilialWSException(e.getMessage());
		}
		return empresafilialExportar7Out;
	}
}