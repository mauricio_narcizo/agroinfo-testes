/**
 * Sapiens_Synccom_senior_g5_co_int_varejo_empresafilial.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Sapiens_Synccom_senior_g5_co_int_varejo_empresafilial extends java.rmi.Remote {
    public br.com.senior.services.EmpresafilialExportarOut exportar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar3Out exportar_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar3In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar2Out exportar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar2In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar4Out exportar_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar4In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar5Out exportar_5(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar5In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar7Out exportar_7(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar7In parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.EmpresafilialExportar6Out exportar_6(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar6In parameters) throws java.rmi.RemoteException;
}
