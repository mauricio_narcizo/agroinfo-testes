/**
 * TitulosBaixarTitulosCPVarejo2Out.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosBaixarTitulosCPVarejo2Out  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private java.lang.String mensagemRetorno;

    private br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado[] resultado;

    private java.lang.String tipoRetorno;

    public TitulosBaixarTitulosCPVarejo2Out() {
    }

    public TitulosBaixarTitulosCPVarejo2Out(
           java.lang.String erroExecucao,
           java.lang.String mensagemRetorno,
           br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado[] resultado,
           java.lang.String tipoRetorno) {
           this.erroExecucao = erroExecucao;
           this.mensagemRetorno = mensagemRetorno;
           this.resultado = resultado;
           this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the erroExecucao value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the resultado value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @return resultado
     */
    public br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado[] getResultado() {
        return resultado;
    }


    /**
     * Sets the resultado value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @param resultado
     */
    public void setResultado(br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado[] resultado) {
        this.resultado = resultado;
    }

    public br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado getResultado(int i) {
        return this.resultado[i];
    }

    public void setResultado(int i, br.com.senior.services.TitulosBaixarTitulosCPVarejo2OutResultado _value) {
        this.resultado[i] = _value;
    }


    /**
     * Gets the tipoRetorno value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @return tipoRetorno
     */
    public java.lang.String getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosBaixarTitulosCPVarejo2Out.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.String tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosBaixarTitulosCPVarejo2Out)) return false;
        TitulosBaixarTitulosCPVarejo2Out other = (TitulosBaixarTitulosCPVarejo2Out) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              java.util.Arrays.equals(this.resultado, other.getResultado()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getResultado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosBaixarTitulosCPVarejo2Out.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixarTitulosCPVarejo2Out"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosBaixarTitulosCPVarejo2OutResultado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
