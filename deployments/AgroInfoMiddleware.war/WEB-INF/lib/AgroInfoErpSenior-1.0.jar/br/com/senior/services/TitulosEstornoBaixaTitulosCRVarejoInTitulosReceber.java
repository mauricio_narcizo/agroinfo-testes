/**
 * TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber  implements java.io.Serializable {
    private java.lang.String chvLot;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codTpt;

    private java.lang.Integer ideExt;

    private java.lang.String numTit;

    private java.lang.Integer seqMov;

    public TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber() {
    }

    public TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber(
           java.lang.String chvLot,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codTpt,
           java.lang.Integer ideExt,
           java.lang.String numTit,
           java.lang.Integer seqMov) {
           this.chvLot = chvLot;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codTpt = codTpt;
           this.ideExt = ideExt;
           this.numTit = numTit;
           this.seqMov = seqMov;
    }


    /**
     * Gets the chvLot value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return chvLot
     */
    public java.lang.String getChvLot() {
        return chvLot;
    }


    /**
     * Sets the chvLot value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param chvLot
     */
    public void setChvLot(java.lang.String chvLot) {
        this.chvLot = chvLot;
    }


    /**
     * Gets the codEmp value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codTpt value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the ideExt value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the numTit value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the seqMov value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @return seqMov
     */
    public java.lang.Integer getSeqMov() {
        return seqMov;
    }


    /**
     * Sets the seqMov value for this TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.
     * 
     * @param seqMov
     */
    public void setSeqMov(java.lang.Integer seqMov) {
        this.seqMov = seqMov;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber)) return false;
        TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber other = (TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chvLot==null && other.getChvLot()==null) || 
             (this.chvLot!=null &&
              this.chvLot.equals(other.getChvLot()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.seqMov==null && other.getSeqMov()==null) || 
             (this.seqMov!=null &&
              this.seqMov.equals(other.getSeqMov())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChvLot() != null) {
            _hashCode += getChvLot().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getSeqMov() != null) {
            _hashCode += getSeqMov().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosEstornoBaixaTitulosCRVarejoInTitulosReceber.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosEstornoBaixaTitulosCRVarejoInTitulosReceber"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chvLot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "chvLot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
