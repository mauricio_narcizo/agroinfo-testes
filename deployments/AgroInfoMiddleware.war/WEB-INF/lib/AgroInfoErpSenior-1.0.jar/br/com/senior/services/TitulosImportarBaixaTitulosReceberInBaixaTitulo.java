/**
 * TitulosImportarBaixaTitulosReceberInBaixaTitulo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosImportarBaixaTitulosReceberInBaixaTitulo  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFpg;

    private java.lang.Integer codFpj;

    private java.lang.String codTpt;

    private java.lang.String datCco;

    private java.lang.String datLib;

    private java.lang.String datMov;

    private java.lang.String datPgt;

    private java.lang.Integer diaAtr;

    private java.lang.Integer diaJrs;

    private java.lang.Integer filCre;

    private java.lang.Integer ideExt;

    private java.lang.String indAbt;

    private java.lang.String indCan;

    private java.lang.String numCco;

    private java.lang.String numDoc;

    private java.lang.Integer numPdv;

    private java.lang.Integer numPrj;

    private java.lang.String numTit;

    private java.lang.Integer reqEmp;

    private java.lang.Integer reqFil;

    private java.lang.Integer seqCco;

    private java.lang.String titCre;

    private java.lang.String tptCre;

    private java.lang.Double vlrCor;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrEnc;

    private java.lang.Double vlrJrs;

    private java.lang.Double vlrLiq;

    private java.lang.Double vlrMov;

    private java.lang.Double vlrMul;

    private java.lang.Double vlrOac;

    private java.lang.Double vlrOde;

    public TitulosImportarBaixaTitulosReceberInBaixaTitulo() {
    }

    public TitulosImportarBaixaTitulosReceberInBaixaTitulo(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFpg,
           java.lang.Integer codFpj,
           java.lang.String codTpt,
           java.lang.String datCco,
           java.lang.String datLib,
           java.lang.String datMov,
           java.lang.String datPgt,
           java.lang.Integer diaAtr,
           java.lang.Integer diaJrs,
           java.lang.Integer filCre,
           java.lang.Integer ideExt,
           java.lang.String indAbt,
           java.lang.String indCan,
           java.lang.String numCco,
           java.lang.String numDoc,
           java.lang.Integer numPdv,
           java.lang.Integer numPrj,
           java.lang.String numTit,
           java.lang.Integer reqEmp,
           java.lang.Integer reqFil,
           java.lang.Integer seqCco,
           java.lang.String titCre,
           java.lang.String tptCre,
           java.lang.Double vlrCor,
           java.lang.Double vlrDsc,
           java.lang.Double vlrEnc,
           java.lang.Double vlrJrs,
           java.lang.Double vlrLiq,
           java.lang.Double vlrMov,
           java.lang.Double vlrMul,
           java.lang.Double vlrOac,
           java.lang.Double vlrOde) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codTpt = codTpt;
           this.datCco = datCco;
           this.datLib = datLib;
           this.datMov = datMov;
           this.datPgt = datPgt;
           this.diaAtr = diaAtr;
           this.diaJrs = diaJrs;
           this.filCre = filCre;
           this.ideExt = ideExt;
           this.indAbt = indAbt;
           this.indCan = indCan;
           this.numCco = numCco;
           this.numDoc = numDoc;
           this.numPdv = numPdv;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.reqEmp = reqEmp;
           this.reqFil = reqFil;
           this.seqCco = seqCco;
           this.titCre = titCre;
           this.tptCre = tptCre;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrEnc = vlrEnc;
           this.vlrJrs = vlrJrs;
           this.vlrLiq = vlrLiq;
           this.vlrMov = vlrMov;
           this.vlrMul = vlrMul;
           this.vlrOac = vlrOac;
           this.vlrOde = vlrOde;
    }


    /**
     * Gets the codEmp value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFpg value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codTpt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the datCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return datCco
     */
    public java.lang.String getDatCco() {
        return datCco;
    }


    /**
     * Sets the datCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param datCco
     */
    public void setDatCco(java.lang.String datCco) {
        this.datCco = datCco;
    }


    /**
     * Gets the datLib value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return datLib
     */
    public java.lang.String getDatLib() {
        return datLib;
    }


    /**
     * Sets the datLib value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param datLib
     */
    public void setDatLib(java.lang.String datLib) {
        this.datLib = datLib;
    }


    /**
     * Gets the datMov value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return datMov
     */
    public java.lang.String getDatMov() {
        return datMov;
    }


    /**
     * Sets the datMov value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param datMov
     */
    public void setDatMov(java.lang.String datMov) {
        this.datMov = datMov;
    }


    /**
     * Gets the datPgt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return datPgt
     */
    public java.lang.String getDatPgt() {
        return datPgt;
    }


    /**
     * Sets the datPgt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param datPgt
     */
    public void setDatPgt(java.lang.String datPgt) {
        this.datPgt = datPgt;
    }


    /**
     * Gets the diaAtr value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return diaAtr
     */
    public java.lang.Integer getDiaAtr() {
        return diaAtr;
    }


    /**
     * Sets the diaAtr value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param diaAtr
     */
    public void setDiaAtr(java.lang.Integer diaAtr) {
        this.diaAtr = diaAtr;
    }


    /**
     * Gets the diaJrs value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return diaJrs
     */
    public java.lang.Integer getDiaJrs() {
        return diaJrs;
    }


    /**
     * Sets the diaJrs value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param diaJrs
     */
    public void setDiaJrs(java.lang.Integer diaJrs) {
        this.diaJrs = diaJrs;
    }


    /**
     * Gets the filCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return filCre
     */
    public java.lang.Integer getFilCre() {
        return filCre;
    }


    /**
     * Sets the filCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param filCre
     */
    public void setFilCre(java.lang.Integer filCre) {
        this.filCre = filCre;
    }


    /**
     * Gets the ideExt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the indAbt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return indAbt
     */
    public java.lang.String getIndAbt() {
        return indAbt;
    }


    /**
     * Sets the indAbt value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param indAbt
     */
    public void setIndAbt(java.lang.String indAbt) {
        this.indAbt = indAbt;
    }


    /**
     * Gets the indCan value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return indCan
     */
    public java.lang.String getIndCan() {
        return indCan;
    }


    /**
     * Sets the indCan value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param indCan
     */
    public void setIndCan(java.lang.String indCan) {
        this.indCan = indCan;
    }


    /**
     * Gets the numCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return numCco
     */
    public java.lang.String getNumCco() {
        return numCco;
    }


    /**
     * Sets the numCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param numCco
     */
    public void setNumCco(java.lang.String numCco) {
        this.numCco = numCco;
    }


    /**
     * Gets the numDoc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return numDoc
     */
    public java.lang.String getNumDoc() {
        return numDoc;
    }


    /**
     * Sets the numDoc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param numDoc
     */
    public void setNumDoc(java.lang.String numDoc) {
        this.numDoc = numDoc;
    }


    /**
     * Gets the numPdv value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return numPdv
     */
    public java.lang.Integer getNumPdv() {
        return numPdv;
    }


    /**
     * Sets the numPdv value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param numPdv
     */
    public void setNumPdv(java.lang.Integer numPdv) {
        this.numPdv = numPdv;
    }


    /**
     * Gets the numPrj value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the reqEmp value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return reqEmp
     */
    public java.lang.Integer getReqEmp() {
        return reqEmp;
    }


    /**
     * Sets the reqEmp value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param reqEmp
     */
    public void setReqEmp(java.lang.Integer reqEmp) {
        this.reqEmp = reqEmp;
    }


    /**
     * Gets the reqFil value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return reqFil
     */
    public java.lang.Integer getReqFil() {
        return reqFil;
    }


    /**
     * Sets the reqFil value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param reqFil
     */
    public void setReqFil(java.lang.Integer reqFil) {
        this.reqFil = reqFil;
    }


    /**
     * Gets the seqCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return seqCco
     */
    public java.lang.Integer getSeqCco() {
        return seqCco;
    }


    /**
     * Sets the seqCco value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param seqCco
     */
    public void setSeqCco(java.lang.Integer seqCco) {
        this.seqCco = seqCco;
    }


    /**
     * Gets the titCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return titCre
     */
    public java.lang.String getTitCre() {
        return titCre;
    }


    /**
     * Sets the titCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param titCre
     */
    public void setTitCre(java.lang.String titCre) {
        this.titCre = titCre;
    }


    /**
     * Gets the tptCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return tptCre
     */
    public java.lang.String getTptCre() {
        return tptCre;
    }


    /**
     * Sets the tptCre value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param tptCre
     */
    public void setTptCre(java.lang.String tptCre) {
        this.tptCre = tptCre;
    }


    /**
     * Gets the vlrCor value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrCor
     */
    public java.lang.Double getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.Double vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEnc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrEnc
     */
    public java.lang.Double getVlrEnc() {
        return vlrEnc;
    }


    /**
     * Sets the vlrEnc value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrEnc
     */
    public void setVlrEnc(java.lang.Double vlrEnc) {
        this.vlrEnc = vlrEnc;
    }


    /**
     * Gets the vlrJrs value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrJrs
     */
    public java.lang.Double getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.Double vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrLiq value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrLiq
     */
    public java.lang.Double getVlrLiq() {
        return vlrLiq;
    }


    /**
     * Sets the vlrLiq value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrLiq
     */
    public void setVlrLiq(java.lang.Double vlrLiq) {
        this.vlrLiq = vlrLiq;
    }


    /**
     * Gets the vlrMov value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrMov
     */
    public java.lang.Double getVlrMov() {
        return vlrMov;
    }


    /**
     * Sets the vlrMov value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrMov
     */
    public void setVlrMov(java.lang.Double vlrMov) {
        this.vlrMov = vlrMov;
    }


    /**
     * Gets the vlrMul value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrMul
     */
    public java.lang.Double getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.Double vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOac value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrOac
     */
    public java.lang.Double getVlrOac() {
        return vlrOac;
    }


    /**
     * Sets the vlrOac value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrOac
     */
    public void setVlrOac(java.lang.Double vlrOac) {
        this.vlrOac = vlrOac;
    }


    /**
     * Gets the vlrOde value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @return vlrOde
     */
    public java.lang.Double getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosImportarBaixaTitulosReceberInBaixaTitulo.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.Double vlrOde) {
        this.vlrOde = vlrOde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosImportarBaixaTitulosReceberInBaixaTitulo)) return false;
        TitulosImportarBaixaTitulosReceberInBaixaTitulo other = (TitulosImportarBaixaTitulosReceberInBaixaTitulo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.datCco==null && other.getDatCco()==null) || 
             (this.datCco!=null &&
              this.datCco.equals(other.getDatCco()))) &&
            ((this.datLib==null && other.getDatLib()==null) || 
             (this.datLib!=null &&
              this.datLib.equals(other.getDatLib()))) &&
            ((this.datMov==null && other.getDatMov()==null) || 
             (this.datMov!=null &&
              this.datMov.equals(other.getDatMov()))) &&
            ((this.datPgt==null && other.getDatPgt()==null) || 
             (this.datPgt!=null &&
              this.datPgt.equals(other.getDatPgt()))) &&
            ((this.diaAtr==null && other.getDiaAtr()==null) || 
             (this.diaAtr!=null &&
              this.diaAtr.equals(other.getDiaAtr()))) &&
            ((this.diaJrs==null && other.getDiaJrs()==null) || 
             (this.diaJrs!=null &&
              this.diaJrs.equals(other.getDiaJrs()))) &&
            ((this.filCre==null && other.getFilCre()==null) || 
             (this.filCre!=null &&
              this.filCre.equals(other.getFilCre()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.indAbt==null && other.getIndAbt()==null) || 
             (this.indAbt!=null &&
              this.indAbt.equals(other.getIndAbt()))) &&
            ((this.indCan==null && other.getIndCan()==null) || 
             (this.indCan!=null &&
              this.indCan.equals(other.getIndCan()))) &&
            ((this.numCco==null && other.getNumCco()==null) || 
             (this.numCco!=null &&
              this.numCco.equals(other.getNumCco()))) &&
            ((this.numDoc==null && other.getNumDoc()==null) || 
             (this.numDoc!=null &&
              this.numDoc.equals(other.getNumDoc()))) &&
            ((this.numPdv==null && other.getNumPdv()==null) || 
             (this.numPdv!=null &&
              this.numPdv.equals(other.getNumPdv()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.reqEmp==null && other.getReqEmp()==null) || 
             (this.reqEmp!=null &&
              this.reqEmp.equals(other.getReqEmp()))) &&
            ((this.reqFil==null && other.getReqFil()==null) || 
             (this.reqFil!=null &&
              this.reqFil.equals(other.getReqFil()))) &&
            ((this.seqCco==null && other.getSeqCco()==null) || 
             (this.seqCco!=null &&
              this.seqCco.equals(other.getSeqCco()))) &&
            ((this.titCre==null && other.getTitCre()==null) || 
             (this.titCre!=null &&
              this.titCre.equals(other.getTitCre()))) &&
            ((this.tptCre==null && other.getTptCre()==null) || 
             (this.tptCre!=null &&
              this.tptCre.equals(other.getTptCre()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEnc==null && other.getVlrEnc()==null) || 
             (this.vlrEnc!=null &&
              this.vlrEnc.equals(other.getVlrEnc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrLiq==null && other.getVlrLiq()==null) || 
             (this.vlrLiq!=null &&
              this.vlrLiq.equals(other.getVlrLiq()))) &&
            ((this.vlrMov==null && other.getVlrMov()==null) || 
             (this.vlrMov!=null &&
              this.vlrMov.equals(other.getVlrMov()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOac==null && other.getVlrOac()==null) || 
             (this.vlrOac!=null &&
              this.vlrOac.equals(other.getVlrOac()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDatCco() != null) {
            _hashCode += getDatCco().hashCode();
        }
        if (getDatLib() != null) {
            _hashCode += getDatLib().hashCode();
        }
        if (getDatMov() != null) {
            _hashCode += getDatMov().hashCode();
        }
        if (getDatPgt() != null) {
            _hashCode += getDatPgt().hashCode();
        }
        if (getDiaAtr() != null) {
            _hashCode += getDiaAtr().hashCode();
        }
        if (getDiaJrs() != null) {
            _hashCode += getDiaJrs().hashCode();
        }
        if (getFilCre() != null) {
            _hashCode += getFilCre().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getIndAbt() != null) {
            _hashCode += getIndAbt().hashCode();
        }
        if (getIndCan() != null) {
            _hashCode += getIndCan().hashCode();
        }
        if (getNumCco() != null) {
            _hashCode += getNumCco().hashCode();
        }
        if (getNumDoc() != null) {
            _hashCode += getNumDoc().hashCode();
        }
        if (getNumPdv() != null) {
            _hashCode += getNumPdv().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getReqEmp() != null) {
            _hashCode += getReqEmp().hashCode();
        }
        if (getReqFil() != null) {
            _hashCode += getReqFil().hashCode();
        }
        if (getSeqCco() != null) {
            _hashCode += getSeqCco().hashCode();
        }
        if (getTitCre() != null) {
            _hashCode += getTitCre().hashCode();
        }
        if (getTptCre() != null) {
            _hashCode += getTptCre().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEnc() != null) {
            _hashCode += getVlrEnc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrLiq() != null) {
            _hashCode += getVlrLiq().hashCode();
        }
        if (getVlrMov() != null) {
            _hashCode += getVlrMov().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOac() != null) {
            _hashCode += getVlrOac().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosImportarBaixaTitulosReceberInBaixaTitulo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosImportarBaixaTitulosReceberInBaixaTitulo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLib");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLib"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAbt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAbt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indCan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indCan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reqEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reqFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reqFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tptCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tptCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLiq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLiq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
