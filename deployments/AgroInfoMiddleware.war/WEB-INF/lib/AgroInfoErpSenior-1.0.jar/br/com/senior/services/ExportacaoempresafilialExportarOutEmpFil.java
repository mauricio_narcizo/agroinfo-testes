/**
 * ExportacaoempresafilialExportarOutEmpFil.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class ExportacaoempresafilialExportarOutEmpFil  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String nomEmp;

    private java.lang.String nomFil;

    public ExportacaoempresafilialExportarOutEmpFil() {
    }

    public ExportacaoempresafilialExportarOutEmpFil(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String nomEmp,
           java.lang.String nomFil) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.nomEmp = nomEmp;
           this.nomFil = nomFil;
    }


    /**
     * Gets the codEmp value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the nomEmp value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @return nomEmp
     */
    public java.lang.String getNomEmp() {
        return nomEmp;
    }


    /**
     * Sets the nomEmp value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @param nomEmp
     */
    public void setNomEmp(java.lang.String nomEmp) {
        this.nomEmp = nomEmp;
    }


    /**
     * Gets the nomFil value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @return nomFil
     */
    public java.lang.String getNomFil() {
        return nomFil;
    }


    /**
     * Sets the nomFil value for this ExportacaoempresafilialExportarOutEmpFil.
     * 
     * @param nomFil
     */
    public void setNomFil(java.lang.String nomFil) {
        this.nomFil = nomFil;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExportacaoempresafilialExportarOutEmpFil)) return false;
        ExportacaoempresafilialExportarOutEmpFil other = (ExportacaoempresafilialExportarOutEmpFil) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.nomEmp==null && other.getNomEmp()==null) || 
             (this.nomEmp!=null &&
              this.nomEmp.equals(other.getNomEmp()))) &&
            ((this.nomFil==null && other.getNomFil()==null) || 
             (this.nomFil!=null &&
              this.nomFil.equals(other.getNomFil())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getNomEmp() != null) {
            _hashCode += getNomEmp().hashCode();
        }
        if (getNomFil() != null) {
            _hashCode += getNomFil().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExportacaoempresafilialExportarOutEmpFil.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "exportacaoempresafilialExportarOutEmpFil"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
