/**
 * McwfUsersAuthenticateJAASIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class McwfUsersAuthenticateJAASIn  implements java.io.Serializable {
    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.Integer pmEncrypted;

    private java.lang.String pmUserName;

    private java.lang.String pmUserPassword;

    public McwfUsersAuthenticateJAASIn() {
    }

    public McwfUsersAuthenticateJAASIn(
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.Integer pmEncrypted,
           java.lang.String pmUserName,
           java.lang.String pmUserPassword) {
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.pmEncrypted = pmEncrypted;
           this.pmUserName = pmUserName;
           this.pmUserPassword = pmUserPassword;
    }


    /**
     * Gets the flowInstanceID value for this McwfUsersAuthenticateJAASIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this McwfUsersAuthenticateJAASIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this McwfUsersAuthenticateJAASIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this McwfUsersAuthenticateJAASIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the pmEncrypted value for this McwfUsersAuthenticateJAASIn.
     * 
     * @return pmEncrypted
     */
    public java.lang.Integer getPmEncrypted() {
        return pmEncrypted;
    }


    /**
     * Sets the pmEncrypted value for this McwfUsersAuthenticateJAASIn.
     * 
     * @param pmEncrypted
     */
    public void setPmEncrypted(java.lang.Integer pmEncrypted) {
        this.pmEncrypted = pmEncrypted;
    }


    /**
     * Gets the pmUserName value for this McwfUsersAuthenticateJAASIn.
     * 
     * @return pmUserName
     */
    public java.lang.String getPmUserName() {
        return pmUserName;
    }


    /**
     * Sets the pmUserName value for this McwfUsersAuthenticateJAASIn.
     * 
     * @param pmUserName
     */
    public void setPmUserName(java.lang.String pmUserName) {
        this.pmUserName = pmUserName;
    }


    /**
     * Gets the pmUserPassword value for this McwfUsersAuthenticateJAASIn.
     * 
     * @return pmUserPassword
     */
    public java.lang.String getPmUserPassword() {
        return pmUserPassword;
    }


    /**
     * Sets the pmUserPassword value for this McwfUsersAuthenticateJAASIn.
     * 
     * @param pmUserPassword
     */
    public void setPmUserPassword(java.lang.String pmUserPassword) {
        this.pmUserPassword = pmUserPassword;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof McwfUsersAuthenticateJAASIn)) return false;
        McwfUsersAuthenticateJAASIn other = (McwfUsersAuthenticateJAASIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.pmEncrypted==null && other.getPmEncrypted()==null) || 
             (this.pmEncrypted!=null &&
              this.pmEncrypted.equals(other.getPmEncrypted()))) &&
            ((this.pmUserName==null && other.getPmUserName()==null) || 
             (this.pmUserName!=null &&
              this.pmUserName.equals(other.getPmUserName()))) &&
            ((this.pmUserPassword==null && other.getPmUserPassword()==null) || 
             (this.pmUserPassword!=null &&
              this.pmUserPassword.equals(other.getPmUserPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getPmEncrypted() != null) {
            _hashCode += getPmEncrypted().hashCode();
        }
        if (getPmUserName() != null) {
            _hashCode += getPmUserName().hashCode();
        }
        if (getPmUserPassword() != null) {
            _hashCode += getPmUserPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(McwfUsersAuthenticateJAASIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "mcwfUsersAuthenticateJAASIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmEncrypted");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmEncrypted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pmUserPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pmUserPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
