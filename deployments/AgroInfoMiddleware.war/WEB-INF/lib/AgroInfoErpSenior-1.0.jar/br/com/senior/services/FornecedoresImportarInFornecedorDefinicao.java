/**
 * FornecedoresImportarInFornecedorDefinicao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresImportarInFornecedorDefinicao  implements java.io.Serializable {
    private java.lang.String antDsc;

    private java.lang.String ccbFor;

    private java.lang.String cifFob;

    private java.lang.String codAge;

    private java.lang.String codBan;

    private java.lang.String codCrt;

    private java.lang.String codDep;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codPor;

    private java.lang.Integer codTra;

    private java.lang.String forMon;

    private java.lang.String indInd;

    private java.lang.String numCcc;

    private java.lang.Integer pagDtj;

    private java.lang.Integer pagDtm;

    private java.lang.Integer pagEev;

    private java.lang.Double pagJmm;

    private java.lang.Double pagMul;

    private java.lang.String pagTir;

    private java.lang.Double perDs1;

    private java.lang.Double perDs2;

    private java.lang.Double perDs3;

    private java.lang.Double perDs4;

    private java.lang.Double perDs5;

    private java.lang.Double perDsc;

    private java.lang.Double perEmb;

    private java.lang.Double perEnc;

    private java.lang.Double perFre;

    private java.lang.Double perFun;

    private java.lang.Double perIns;

    private java.lang.Double perIrf;

    private java.lang.Double perIss;

    private java.lang.Double perOut;

    private java.lang.Double perSeg;

    private java.lang.String pgtFre;

    private java.lang.String pgtMon;

    private java.lang.Integer przEnt;

    private java.lang.Integer qtdDcv;

    private java.lang.String tnsPro;

    private java.lang.String tnsSer;

    private java.lang.Integer tolDsc;

    public FornecedoresImportarInFornecedorDefinicao() {
    }

    public FornecedoresImportarInFornecedorDefinicao(
           java.lang.String antDsc,
           java.lang.String ccbFor,
           java.lang.String cifFob,
           java.lang.String codAge,
           java.lang.String codBan,
           java.lang.String codCrt,
           java.lang.String codDep,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codPor,
           java.lang.Integer codTra,
           java.lang.String forMon,
           java.lang.String indInd,
           java.lang.String numCcc,
           java.lang.Integer pagDtj,
           java.lang.Integer pagDtm,
           java.lang.Integer pagEev,
           java.lang.Double pagJmm,
           java.lang.Double pagMul,
           java.lang.String pagTir,
           java.lang.Double perDs1,
           java.lang.Double perDs2,
           java.lang.Double perDs3,
           java.lang.Double perDs4,
           java.lang.Double perDs5,
           java.lang.Double perDsc,
           java.lang.Double perEmb,
           java.lang.Double perEnc,
           java.lang.Double perFre,
           java.lang.Double perFun,
           java.lang.Double perIns,
           java.lang.Double perIrf,
           java.lang.Double perIss,
           java.lang.Double perOut,
           java.lang.Double perSeg,
           java.lang.String pgtFre,
           java.lang.String pgtMon,
           java.lang.Integer przEnt,
           java.lang.Integer qtdDcv,
           java.lang.String tnsPro,
           java.lang.String tnsSer,
           java.lang.Integer tolDsc) {
           this.antDsc = antDsc;
           this.ccbFor = ccbFor;
           this.cifFob = cifFob;
           this.codAge = codAge;
           this.codBan = codBan;
           this.codCrt = codCrt;
           this.codDep = codDep;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codPor = codPor;
           this.codTra = codTra;
           this.forMon = forMon;
           this.indInd = indInd;
           this.numCcc = numCcc;
           this.pagDtj = pagDtj;
           this.pagDtm = pagDtm;
           this.pagEev = pagEev;
           this.pagJmm = pagJmm;
           this.pagMul = pagMul;
           this.pagTir = pagTir;
           this.perDs1 = perDs1;
           this.perDs2 = perDs2;
           this.perDs3 = perDs3;
           this.perDs4 = perDs4;
           this.perDs5 = perDs5;
           this.perDsc = perDsc;
           this.perEmb = perEmb;
           this.perEnc = perEnc;
           this.perFre = perFre;
           this.perFun = perFun;
           this.perIns = perIns;
           this.perIrf = perIrf;
           this.perIss = perIss;
           this.perOut = perOut;
           this.perSeg = perSeg;
           this.pgtFre = pgtFre;
           this.pgtMon = pgtMon;
           this.przEnt = przEnt;
           this.qtdDcv = qtdDcv;
           this.tnsPro = tnsPro;
           this.tnsSer = tnsSer;
           this.tolDsc = tolDsc;
    }


    /**
     * Gets the antDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return antDsc
     */
    public java.lang.String getAntDsc() {
        return antDsc;
    }


    /**
     * Sets the antDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param antDsc
     */
    public void setAntDsc(java.lang.String antDsc) {
        this.antDsc = antDsc;
    }


    /**
     * Gets the ccbFor value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return ccbFor
     */
    public java.lang.String getCcbFor() {
        return ccbFor;
    }


    /**
     * Sets the ccbFor value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param ccbFor
     */
    public void setCcbFor(java.lang.String ccbFor) {
        this.ccbFor = ccbFor;
    }


    /**
     * Gets the cifFob value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return cifFob
     */
    public java.lang.String getCifFob() {
        return cifFob;
    }


    /**
     * Sets the cifFob value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param cifFob
     */
    public void setCifFob(java.lang.String cifFob) {
        this.cifFob = cifFob;
    }


    /**
     * Gets the codAge value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codAge
     */
    public java.lang.String getCodAge() {
        return codAge;
    }


    /**
     * Sets the codAge value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codAge
     */
    public void setCodAge(java.lang.String codAge) {
        this.codAge = codAge;
    }


    /**
     * Gets the codBan value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCrt value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codDep value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codDep
     */
    public java.lang.String getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.String codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the codEmp value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codPor value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codTra value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return codTra
     */
    public java.lang.Integer getCodTra() {
        return codTra;
    }


    /**
     * Sets the codTra value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param codTra
     */
    public void setCodTra(java.lang.Integer codTra) {
        this.codTra = codTra;
    }


    /**
     * Gets the forMon value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return forMon
     */
    public java.lang.String getForMon() {
        return forMon;
    }


    /**
     * Sets the forMon value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param forMon
     */
    public void setForMon(java.lang.String forMon) {
        this.forMon = forMon;
    }


    /**
     * Gets the indInd value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return indInd
     */
    public java.lang.String getIndInd() {
        return indInd;
    }


    /**
     * Sets the indInd value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param indInd
     */
    public void setIndInd(java.lang.String indInd) {
        this.indInd = indInd;
    }


    /**
     * Gets the numCcc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return numCcc
     */
    public java.lang.String getNumCcc() {
        return numCcc;
    }


    /**
     * Sets the numCcc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param numCcc
     */
    public void setNumCcc(java.lang.String numCcc) {
        this.numCcc = numCcc;
    }


    /**
     * Gets the pagDtj value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagDtj
     */
    public java.lang.Integer getPagDtj() {
        return pagDtj;
    }


    /**
     * Sets the pagDtj value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagDtj
     */
    public void setPagDtj(java.lang.Integer pagDtj) {
        this.pagDtj = pagDtj;
    }


    /**
     * Gets the pagDtm value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagDtm
     */
    public java.lang.Integer getPagDtm() {
        return pagDtm;
    }


    /**
     * Sets the pagDtm value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagDtm
     */
    public void setPagDtm(java.lang.Integer pagDtm) {
        this.pagDtm = pagDtm;
    }


    /**
     * Gets the pagEev value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagEev
     */
    public java.lang.Integer getPagEev() {
        return pagEev;
    }


    /**
     * Sets the pagEev value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagEev
     */
    public void setPagEev(java.lang.Integer pagEev) {
        this.pagEev = pagEev;
    }


    /**
     * Gets the pagJmm value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagJmm
     */
    public java.lang.Double getPagJmm() {
        return pagJmm;
    }


    /**
     * Sets the pagJmm value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagJmm
     */
    public void setPagJmm(java.lang.Double pagJmm) {
        this.pagJmm = pagJmm;
    }


    /**
     * Gets the pagMul value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagMul
     */
    public java.lang.Double getPagMul() {
        return pagMul;
    }


    /**
     * Sets the pagMul value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagMul
     */
    public void setPagMul(java.lang.Double pagMul) {
        this.pagMul = pagMul;
    }


    /**
     * Gets the pagTir value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pagTir
     */
    public java.lang.String getPagTir() {
        return pagTir;
    }


    /**
     * Sets the pagTir value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pagTir
     */
    public void setPagTir(java.lang.String pagTir) {
        this.pagTir = pagTir;
    }


    /**
     * Gets the perDs1 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDs1
     */
    public java.lang.Double getPerDs1() {
        return perDs1;
    }


    /**
     * Sets the perDs1 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDs1
     */
    public void setPerDs1(java.lang.Double perDs1) {
        this.perDs1 = perDs1;
    }


    /**
     * Gets the perDs2 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDs2
     */
    public java.lang.Double getPerDs2() {
        return perDs2;
    }


    /**
     * Sets the perDs2 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDs2
     */
    public void setPerDs2(java.lang.Double perDs2) {
        this.perDs2 = perDs2;
    }


    /**
     * Gets the perDs3 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDs3
     */
    public java.lang.Double getPerDs3() {
        return perDs3;
    }


    /**
     * Sets the perDs3 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDs3
     */
    public void setPerDs3(java.lang.Double perDs3) {
        this.perDs3 = perDs3;
    }


    /**
     * Gets the perDs4 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDs4
     */
    public java.lang.Double getPerDs4() {
        return perDs4;
    }


    /**
     * Sets the perDs4 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDs4
     */
    public void setPerDs4(java.lang.Double perDs4) {
        this.perDs4 = perDs4;
    }


    /**
     * Gets the perDs5 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDs5
     */
    public java.lang.Double getPerDs5() {
        return perDs5;
    }


    /**
     * Sets the perDs5 value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDs5
     */
    public void setPerDs5(java.lang.Double perDs5) {
        this.perDs5 = perDs5;
    }


    /**
     * Gets the perDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perDsc
     */
    public java.lang.Double getPerDsc() {
        return perDsc;
    }


    /**
     * Sets the perDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perDsc
     */
    public void setPerDsc(java.lang.Double perDsc) {
        this.perDsc = perDsc;
    }


    /**
     * Gets the perEmb value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perEmb
     */
    public java.lang.Double getPerEmb() {
        return perEmb;
    }


    /**
     * Sets the perEmb value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perEmb
     */
    public void setPerEmb(java.lang.Double perEmb) {
        this.perEmb = perEmb;
    }


    /**
     * Gets the perEnc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perEnc
     */
    public java.lang.Double getPerEnc() {
        return perEnc;
    }


    /**
     * Sets the perEnc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perEnc
     */
    public void setPerEnc(java.lang.Double perEnc) {
        this.perEnc = perEnc;
    }


    /**
     * Gets the perFre value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perFre
     */
    public java.lang.Double getPerFre() {
        return perFre;
    }


    /**
     * Sets the perFre value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perFre
     */
    public void setPerFre(java.lang.Double perFre) {
        this.perFre = perFre;
    }


    /**
     * Gets the perFun value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perFun
     */
    public java.lang.Double getPerFun() {
        return perFun;
    }


    /**
     * Sets the perFun value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perFun
     */
    public void setPerFun(java.lang.Double perFun) {
        this.perFun = perFun;
    }


    /**
     * Gets the perIns value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perIns
     */
    public java.lang.Double getPerIns() {
        return perIns;
    }


    /**
     * Sets the perIns value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perIns
     */
    public void setPerIns(java.lang.Double perIns) {
        this.perIns = perIns;
    }


    /**
     * Gets the perIrf value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perIrf
     */
    public java.lang.Double getPerIrf() {
        return perIrf;
    }


    /**
     * Sets the perIrf value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perIrf
     */
    public void setPerIrf(java.lang.Double perIrf) {
        this.perIrf = perIrf;
    }


    /**
     * Gets the perIss value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perIss
     */
    public java.lang.Double getPerIss() {
        return perIss;
    }


    /**
     * Sets the perIss value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perIss
     */
    public void setPerIss(java.lang.Double perIss) {
        this.perIss = perIss;
    }


    /**
     * Gets the perOut value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perOut
     */
    public java.lang.Double getPerOut() {
        return perOut;
    }


    /**
     * Sets the perOut value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perOut
     */
    public void setPerOut(java.lang.Double perOut) {
        this.perOut = perOut;
    }


    /**
     * Gets the perSeg value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return perSeg
     */
    public java.lang.Double getPerSeg() {
        return perSeg;
    }


    /**
     * Sets the perSeg value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param perSeg
     */
    public void setPerSeg(java.lang.Double perSeg) {
        this.perSeg = perSeg;
    }


    /**
     * Gets the pgtFre value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pgtFre
     */
    public java.lang.String getPgtFre() {
        return pgtFre;
    }


    /**
     * Sets the pgtFre value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pgtFre
     */
    public void setPgtFre(java.lang.String pgtFre) {
        this.pgtFre = pgtFre;
    }


    /**
     * Gets the pgtMon value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return pgtMon
     */
    public java.lang.String getPgtMon() {
        return pgtMon;
    }


    /**
     * Sets the pgtMon value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param pgtMon
     */
    public void setPgtMon(java.lang.String pgtMon) {
        this.pgtMon = pgtMon;
    }


    /**
     * Gets the przEnt value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return przEnt
     */
    public java.lang.Integer getPrzEnt() {
        return przEnt;
    }


    /**
     * Sets the przEnt value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param przEnt
     */
    public void setPrzEnt(java.lang.Integer przEnt) {
        this.przEnt = przEnt;
    }


    /**
     * Gets the qtdDcv value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return qtdDcv
     */
    public java.lang.Integer getQtdDcv() {
        return qtdDcv;
    }


    /**
     * Sets the qtdDcv value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param qtdDcv
     */
    public void setQtdDcv(java.lang.Integer qtdDcv) {
        this.qtdDcv = qtdDcv;
    }


    /**
     * Gets the tnsPro value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return tnsPro
     */
    public java.lang.String getTnsPro() {
        return tnsPro;
    }


    /**
     * Sets the tnsPro value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param tnsPro
     */
    public void setTnsPro(java.lang.String tnsPro) {
        this.tnsPro = tnsPro;
    }


    /**
     * Gets the tnsSer value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return tnsSer
     */
    public java.lang.String getTnsSer() {
        return tnsSer;
    }


    /**
     * Sets the tnsSer value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param tnsSer
     */
    public void setTnsSer(java.lang.String tnsSer) {
        this.tnsSer = tnsSer;
    }


    /**
     * Gets the tolDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @return tolDsc
     */
    public java.lang.Integer getTolDsc() {
        return tolDsc;
    }


    /**
     * Sets the tolDsc value for this FornecedoresImportarInFornecedorDefinicao.
     * 
     * @param tolDsc
     */
    public void setTolDsc(java.lang.Integer tolDsc) {
        this.tolDsc = tolDsc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresImportarInFornecedorDefinicao)) return false;
        FornecedoresImportarInFornecedorDefinicao other = (FornecedoresImportarInFornecedorDefinicao) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antDsc==null && other.getAntDsc()==null) || 
             (this.antDsc!=null &&
              this.antDsc.equals(other.getAntDsc()))) &&
            ((this.ccbFor==null && other.getCcbFor()==null) || 
             (this.ccbFor!=null &&
              this.ccbFor.equals(other.getCcbFor()))) &&
            ((this.cifFob==null && other.getCifFob()==null) || 
             (this.cifFob!=null &&
              this.cifFob.equals(other.getCifFob()))) &&
            ((this.codAge==null && other.getCodAge()==null) || 
             (this.codAge!=null &&
              this.codAge.equals(other.getCodAge()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codTra==null && other.getCodTra()==null) || 
             (this.codTra!=null &&
              this.codTra.equals(other.getCodTra()))) &&
            ((this.forMon==null && other.getForMon()==null) || 
             (this.forMon!=null &&
              this.forMon.equals(other.getForMon()))) &&
            ((this.indInd==null && other.getIndInd()==null) || 
             (this.indInd!=null &&
              this.indInd.equals(other.getIndInd()))) &&
            ((this.numCcc==null && other.getNumCcc()==null) || 
             (this.numCcc!=null &&
              this.numCcc.equals(other.getNumCcc()))) &&
            ((this.pagDtj==null && other.getPagDtj()==null) || 
             (this.pagDtj!=null &&
              this.pagDtj.equals(other.getPagDtj()))) &&
            ((this.pagDtm==null && other.getPagDtm()==null) || 
             (this.pagDtm!=null &&
              this.pagDtm.equals(other.getPagDtm()))) &&
            ((this.pagEev==null && other.getPagEev()==null) || 
             (this.pagEev!=null &&
              this.pagEev.equals(other.getPagEev()))) &&
            ((this.pagJmm==null && other.getPagJmm()==null) || 
             (this.pagJmm!=null &&
              this.pagJmm.equals(other.getPagJmm()))) &&
            ((this.pagMul==null && other.getPagMul()==null) || 
             (this.pagMul!=null &&
              this.pagMul.equals(other.getPagMul()))) &&
            ((this.pagTir==null && other.getPagTir()==null) || 
             (this.pagTir!=null &&
              this.pagTir.equals(other.getPagTir()))) &&
            ((this.perDs1==null && other.getPerDs1()==null) || 
             (this.perDs1!=null &&
              this.perDs1.equals(other.getPerDs1()))) &&
            ((this.perDs2==null && other.getPerDs2()==null) || 
             (this.perDs2!=null &&
              this.perDs2.equals(other.getPerDs2()))) &&
            ((this.perDs3==null && other.getPerDs3()==null) || 
             (this.perDs3!=null &&
              this.perDs3.equals(other.getPerDs3()))) &&
            ((this.perDs4==null && other.getPerDs4()==null) || 
             (this.perDs4!=null &&
              this.perDs4.equals(other.getPerDs4()))) &&
            ((this.perDs5==null && other.getPerDs5()==null) || 
             (this.perDs5!=null &&
              this.perDs5.equals(other.getPerDs5()))) &&
            ((this.perDsc==null && other.getPerDsc()==null) || 
             (this.perDsc!=null &&
              this.perDsc.equals(other.getPerDsc()))) &&
            ((this.perEmb==null && other.getPerEmb()==null) || 
             (this.perEmb!=null &&
              this.perEmb.equals(other.getPerEmb()))) &&
            ((this.perEnc==null && other.getPerEnc()==null) || 
             (this.perEnc!=null &&
              this.perEnc.equals(other.getPerEnc()))) &&
            ((this.perFre==null && other.getPerFre()==null) || 
             (this.perFre!=null &&
              this.perFre.equals(other.getPerFre()))) &&
            ((this.perFun==null && other.getPerFun()==null) || 
             (this.perFun!=null &&
              this.perFun.equals(other.getPerFun()))) &&
            ((this.perIns==null && other.getPerIns()==null) || 
             (this.perIns!=null &&
              this.perIns.equals(other.getPerIns()))) &&
            ((this.perIrf==null && other.getPerIrf()==null) || 
             (this.perIrf!=null &&
              this.perIrf.equals(other.getPerIrf()))) &&
            ((this.perIss==null && other.getPerIss()==null) || 
             (this.perIss!=null &&
              this.perIss.equals(other.getPerIss()))) &&
            ((this.perOut==null && other.getPerOut()==null) || 
             (this.perOut!=null &&
              this.perOut.equals(other.getPerOut()))) &&
            ((this.perSeg==null && other.getPerSeg()==null) || 
             (this.perSeg!=null &&
              this.perSeg.equals(other.getPerSeg()))) &&
            ((this.pgtFre==null && other.getPgtFre()==null) || 
             (this.pgtFre!=null &&
              this.pgtFre.equals(other.getPgtFre()))) &&
            ((this.pgtMon==null && other.getPgtMon()==null) || 
             (this.pgtMon!=null &&
              this.pgtMon.equals(other.getPgtMon()))) &&
            ((this.przEnt==null && other.getPrzEnt()==null) || 
             (this.przEnt!=null &&
              this.przEnt.equals(other.getPrzEnt()))) &&
            ((this.qtdDcv==null && other.getQtdDcv()==null) || 
             (this.qtdDcv!=null &&
              this.qtdDcv.equals(other.getQtdDcv()))) &&
            ((this.tnsPro==null && other.getTnsPro()==null) || 
             (this.tnsPro!=null &&
              this.tnsPro.equals(other.getTnsPro()))) &&
            ((this.tnsSer==null && other.getTnsSer()==null) || 
             (this.tnsSer!=null &&
              this.tnsSer.equals(other.getTnsSer()))) &&
            ((this.tolDsc==null && other.getTolDsc()==null) || 
             (this.tolDsc!=null &&
              this.tolDsc.equals(other.getTolDsc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntDsc() != null) {
            _hashCode += getAntDsc().hashCode();
        }
        if (getCcbFor() != null) {
            _hashCode += getCcbFor().hashCode();
        }
        if (getCifFob() != null) {
            _hashCode += getCifFob().hashCode();
        }
        if (getCodAge() != null) {
            _hashCode += getCodAge().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodTra() != null) {
            _hashCode += getCodTra().hashCode();
        }
        if (getForMon() != null) {
            _hashCode += getForMon().hashCode();
        }
        if (getIndInd() != null) {
            _hashCode += getIndInd().hashCode();
        }
        if (getNumCcc() != null) {
            _hashCode += getNumCcc().hashCode();
        }
        if (getPagDtj() != null) {
            _hashCode += getPagDtj().hashCode();
        }
        if (getPagDtm() != null) {
            _hashCode += getPagDtm().hashCode();
        }
        if (getPagEev() != null) {
            _hashCode += getPagEev().hashCode();
        }
        if (getPagJmm() != null) {
            _hashCode += getPagJmm().hashCode();
        }
        if (getPagMul() != null) {
            _hashCode += getPagMul().hashCode();
        }
        if (getPagTir() != null) {
            _hashCode += getPagTir().hashCode();
        }
        if (getPerDs1() != null) {
            _hashCode += getPerDs1().hashCode();
        }
        if (getPerDs2() != null) {
            _hashCode += getPerDs2().hashCode();
        }
        if (getPerDs3() != null) {
            _hashCode += getPerDs3().hashCode();
        }
        if (getPerDs4() != null) {
            _hashCode += getPerDs4().hashCode();
        }
        if (getPerDs5() != null) {
            _hashCode += getPerDs5().hashCode();
        }
        if (getPerDsc() != null) {
            _hashCode += getPerDsc().hashCode();
        }
        if (getPerEmb() != null) {
            _hashCode += getPerEmb().hashCode();
        }
        if (getPerEnc() != null) {
            _hashCode += getPerEnc().hashCode();
        }
        if (getPerFre() != null) {
            _hashCode += getPerFre().hashCode();
        }
        if (getPerFun() != null) {
            _hashCode += getPerFun().hashCode();
        }
        if (getPerIns() != null) {
            _hashCode += getPerIns().hashCode();
        }
        if (getPerIrf() != null) {
            _hashCode += getPerIrf().hashCode();
        }
        if (getPerIss() != null) {
            _hashCode += getPerIss().hashCode();
        }
        if (getPerOut() != null) {
            _hashCode += getPerOut().hashCode();
        }
        if (getPerSeg() != null) {
            _hashCode += getPerSeg().hashCode();
        }
        if (getPgtFre() != null) {
            _hashCode += getPgtFre().hashCode();
        }
        if (getPgtMon() != null) {
            _hashCode += getPgtMon().hashCode();
        }
        if (getPrzEnt() != null) {
            _hashCode += getPrzEnt().hashCode();
        }
        if (getQtdDcv() != null) {
            _hashCode += getQtdDcv().hashCode();
        }
        if (getTnsPro() != null) {
            _hashCode += getTnsPro().hashCode();
        }
        if (getTnsSer() != null) {
            _hashCode += getTnsSer().hashCode();
        }
        if (getTolDsc() != null) {
            _hashCode += getTolDsc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresImportarInFornecedorDefinicao.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarInFornecedorDefinicao"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "antDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccbFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ccbFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cifFob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cifFob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTra");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTra"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forMon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forMon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indInd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indInd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagEev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagEev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagTir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagTir"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDs5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDs5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEmb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEmb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perFun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perFun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perIss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perIss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perOut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perOut"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perSeg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perSeg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pgtFre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pgtFre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pgtMon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pgtMon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("przEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "przEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsSer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsSer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tolDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tolDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
