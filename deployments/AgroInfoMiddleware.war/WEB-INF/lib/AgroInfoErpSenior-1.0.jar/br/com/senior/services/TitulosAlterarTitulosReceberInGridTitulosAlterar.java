/**
 * TitulosAlterarTitulosReceberInGridTitulosAlterar.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosAlterarTitulosReceberInGridTitulosAlterar  implements java.io.Serializable {
    private java.lang.String catTef;

    private java.lang.String cheAge;

    private java.lang.String cheBan;

    private java.lang.String cheCta;

    private java.lang.String cheNum;

    private java.lang.Integer codCli;

    private java.lang.Integer codCnv;

    private java.lang.String codCrp;

    private java.lang.String codCrt;

    private java.lang.Integer codFil;

    private java.lang.Integer codFpg;

    private java.lang.String codMoe;

    private java.lang.Integer codNtg;

    private java.lang.String codPor;

    private java.lang.Double codSac;

    private java.lang.String codTpt;

    private java.lang.String datEmi;

    private java.lang.String datEnt;

    private java.lang.String datPpt;

    private java.lang.Integer ideExt;

    private java.lang.String nsuTef;

    private java.lang.String numTit;

    private java.lang.String obsTcr;

    private java.lang.String sitTit;

    private java.lang.String vctOri;

    private java.lang.String vctPro;

    private java.lang.Double vlrOri;

    public TitulosAlterarTitulosReceberInGridTitulosAlterar() {
    }

    public TitulosAlterarTitulosReceberInGridTitulosAlterar(
           java.lang.String catTef,
           java.lang.String cheAge,
           java.lang.String cheBan,
           java.lang.String cheCta,
           java.lang.String cheNum,
           java.lang.Integer codCli,
           java.lang.Integer codCnv,
           java.lang.String codCrp,
           java.lang.String codCrt,
           java.lang.Integer codFil,
           java.lang.Integer codFpg,
           java.lang.String codMoe,
           java.lang.Integer codNtg,
           java.lang.String codPor,
           java.lang.Double codSac,
           java.lang.String codTpt,
           java.lang.String datEmi,
           java.lang.String datEnt,
           java.lang.String datPpt,
           java.lang.Integer ideExt,
           java.lang.String nsuTef,
           java.lang.String numTit,
           java.lang.String obsTcr,
           java.lang.String sitTit,
           java.lang.String vctOri,
           java.lang.String vctPro,
           java.lang.Double vlrOri) {
           this.catTef = catTef;
           this.cheAge = cheAge;
           this.cheBan = cheBan;
           this.cheCta = cheCta;
           this.cheNum = cheNum;
           this.codCli = codCli;
           this.codCnv = codCnv;
           this.codCrp = codCrp;
           this.codCrt = codCrt;
           this.codFil = codFil;
           this.codFpg = codFpg;
           this.codMoe = codMoe;
           this.codNtg = codNtg;
           this.codPor = codPor;
           this.codSac = codSac;
           this.codTpt = codTpt;
           this.datEmi = datEmi;
           this.datEnt = datEnt;
           this.datPpt = datPpt;
           this.ideExt = ideExt;
           this.nsuTef = nsuTef;
           this.numTit = numTit;
           this.obsTcr = obsTcr;
           this.sitTit = sitTit;
           this.vctOri = vctOri;
           this.vctPro = vctPro;
           this.vlrOri = vlrOri;
    }


    /**
     * Gets the catTef value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return catTef
     */
    public java.lang.String getCatTef() {
        return catTef;
    }


    /**
     * Sets the catTef value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param catTef
     */
    public void setCatTef(java.lang.String catTef) {
        this.catTef = catTef;
    }


    /**
     * Gets the cheAge value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return cheAge
     */
    public java.lang.String getCheAge() {
        return cheAge;
    }


    /**
     * Sets the cheAge value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param cheAge
     */
    public void setCheAge(java.lang.String cheAge) {
        this.cheAge = cheAge;
    }


    /**
     * Gets the cheBan value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return cheBan
     */
    public java.lang.String getCheBan() {
        return cheBan;
    }


    /**
     * Sets the cheBan value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param cheBan
     */
    public void setCheBan(java.lang.String cheBan) {
        this.cheBan = cheBan;
    }


    /**
     * Gets the cheCta value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return cheCta
     */
    public java.lang.String getCheCta() {
        return cheCta;
    }


    /**
     * Sets the cheCta value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param cheCta
     */
    public void setCheCta(java.lang.String cheCta) {
        this.cheCta = cheCta;
    }


    /**
     * Gets the cheNum value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return cheNum
     */
    public java.lang.String getCheNum() {
        return cheNum;
    }


    /**
     * Sets the cheNum value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param cheNum
     */
    public void setCheNum(java.lang.String cheNum) {
        this.cheNum = cheNum;
    }


    /**
     * Gets the codCli value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codCnv value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codCnv
     */
    public java.lang.Integer getCodCnv() {
        return codCnv;
    }


    /**
     * Sets the codCnv value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codCnv
     */
    public void setCodCnv(java.lang.Integer codCnv) {
        this.codCnv = codCnv;
    }


    /**
     * Gets the codCrp value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codCrp
     */
    public java.lang.String getCodCrp() {
        return codCrp;
    }


    /**
     * Sets the codCrp value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codCrp
     */
    public void setCodCrp(java.lang.String codCrp) {
        this.codCrp = codCrp;
    }


    /**
     * Gets the codCrt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codCrt
     */
    public java.lang.String getCodCrt() {
        return codCrt;
    }


    /**
     * Sets the codCrt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codCrt
     */
    public void setCodCrt(java.lang.String codCrt) {
        this.codCrt = codCrt;
    }


    /**
     * Gets the codFil value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFpg value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codMoe value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codMoe
     */
    public java.lang.String getCodMoe() {
        return codMoe;
    }


    /**
     * Sets the codMoe value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codMoe
     */
    public void setCodMoe(java.lang.String codMoe) {
        this.codMoe = codMoe;
    }


    /**
     * Gets the codNtg value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codNtg
     */
    public java.lang.Integer getCodNtg() {
        return codNtg;
    }


    /**
     * Sets the codNtg value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codNtg
     */
    public void setCodNtg(java.lang.Integer codNtg) {
        this.codNtg = codNtg;
    }


    /**
     * Gets the codPor value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codPor
     */
    public java.lang.String getCodPor() {
        return codPor;
    }


    /**
     * Sets the codPor value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codPor
     */
    public void setCodPor(java.lang.String codPor) {
        this.codPor = codPor;
    }


    /**
     * Gets the codSac value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codSac
     */
    public java.lang.Double getCodSac() {
        return codSac;
    }


    /**
     * Sets the codSac value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codSac
     */
    public void setCodSac(java.lang.Double codSac) {
        this.codSac = codSac;
    }


    /**
     * Gets the codTpt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the datEmi value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return datEmi
     */
    public java.lang.String getDatEmi() {
        return datEmi;
    }


    /**
     * Sets the datEmi value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param datEmi
     */
    public void setDatEmi(java.lang.String datEmi) {
        this.datEmi = datEmi;
    }


    /**
     * Gets the datEnt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return datEnt
     */
    public java.lang.String getDatEnt() {
        return datEnt;
    }


    /**
     * Sets the datEnt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param datEnt
     */
    public void setDatEnt(java.lang.String datEnt) {
        this.datEnt = datEnt;
    }


    /**
     * Gets the datPpt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return datPpt
     */
    public java.lang.String getDatPpt() {
        return datPpt;
    }


    /**
     * Sets the datPpt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param datPpt
     */
    public void setDatPpt(java.lang.String datPpt) {
        this.datPpt = datPpt;
    }


    /**
     * Gets the ideExt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the nsuTef value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return nsuTef
     */
    public java.lang.String getNsuTef() {
        return nsuTef;
    }


    /**
     * Sets the nsuTef value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param nsuTef
     */
    public void setNsuTef(java.lang.String nsuTef) {
        this.nsuTef = nsuTef;
    }


    /**
     * Gets the numTit value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsTcr value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return obsTcr
     */
    public java.lang.String getObsTcr() {
        return obsTcr;
    }


    /**
     * Sets the obsTcr value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param obsTcr
     */
    public void setObsTcr(java.lang.String obsTcr) {
        this.obsTcr = obsTcr;
    }


    /**
     * Gets the sitTit value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return sitTit
     */
    public java.lang.String getSitTit() {
        return sitTit;
    }


    /**
     * Sets the sitTit value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param sitTit
     */
    public void setSitTit(java.lang.String sitTit) {
        this.sitTit = sitTit;
    }


    /**
     * Gets the vctOri value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return vctOri
     */
    public java.lang.String getVctOri() {
        return vctOri;
    }


    /**
     * Sets the vctOri value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param vctOri
     */
    public void setVctOri(java.lang.String vctOri) {
        this.vctOri = vctOri;
    }


    /**
     * Gets the vctPro value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return vctPro
     */
    public java.lang.String getVctPro() {
        return vctPro;
    }


    /**
     * Sets the vctPro value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param vctPro
     */
    public void setVctPro(java.lang.String vctPro) {
        this.vctPro = vctPro;
    }


    /**
     * Gets the vlrOri value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @return vlrOri
     */
    public java.lang.Double getVlrOri() {
        return vlrOri;
    }


    /**
     * Sets the vlrOri value for this TitulosAlterarTitulosReceberInGridTitulosAlterar.
     * 
     * @param vlrOri
     */
    public void setVlrOri(java.lang.Double vlrOri) {
        this.vlrOri = vlrOri;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosAlterarTitulosReceberInGridTitulosAlterar)) return false;
        TitulosAlterarTitulosReceberInGridTitulosAlterar other = (TitulosAlterarTitulosReceberInGridTitulosAlterar) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.catTef==null && other.getCatTef()==null) || 
             (this.catTef!=null &&
              this.catTef.equals(other.getCatTef()))) &&
            ((this.cheAge==null && other.getCheAge()==null) || 
             (this.cheAge!=null &&
              this.cheAge.equals(other.getCheAge()))) &&
            ((this.cheBan==null && other.getCheBan()==null) || 
             (this.cheBan!=null &&
              this.cheBan.equals(other.getCheBan()))) &&
            ((this.cheCta==null && other.getCheCta()==null) || 
             (this.cheCta!=null &&
              this.cheCta.equals(other.getCheCta()))) &&
            ((this.cheNum==null && other.getCheNum()==null) || 
             (this.cheNum!=null &&
              this.cheNum.equals(other.getCheNum()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codCnv==null && other.getCodCnv()==null) || 
             (this.codCnv!=null &&
              this.codCnv.equals(other.getCodCnv()))) &&
            ((this.codCrp==null && other.getCodCrp()==null) || 
             (this.codCrp!=null &&
              this.codCrp.equals(other.getCodCrp()))) &&
            ((this.codCrt==null && other.getCodCrt()==null) || 
             (this.codCrt!=null &&
              this.codCrt.equals(other.getCodCrt()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codMoe==null && other.getCodMoe()==null) || 
             (this.codMoe!=null &&
              this.codMoe.equals(other.getCodMoe()))) &&
            ((this.codNtg==null && other.getCodNtg()==null) || 
             (this.codNtg!=null &&
              this.codNtg.equals(other.getCodNtg()))) &&
            ((this.codPor==null && other.getCodPor()==null) || 
             (this.codPor!=null &&
              this.codPor.equals(other.getCodPor()))) &&
            ((this.codSac==null && other.getCodSac()==null) || 
             (this.codSac!=null &&
              this.codSac.equals(other.getCodSac()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.datEmi==null && other.getDatEmi()==null) || 
             (this.datEmi!=null &&
              this.datEmi.equals(other.getDatEmi()))) &&
            ((this.datEnt==null && other.getDatEnt()==null) || 
             (this.datEnt!=null &&
              this.datEnt.equals(other.getDatEnt()))) &&
            ((this.datPpt==null && other.getDatPpt()==null) || 
             (this.datPpt!=null &&
              this.datPpt.equals(other.getDatPpt()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.nsuTef==null && other.getNsuTef()==null) || 
             (this.nsuTef!=null &&
              this.nsuTef.equals(other.getNsuTef()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsTcr==null && other.getObsTcr()==null) || 
             (this.obsTcr!=null &&
              this.obsTcr.equals(other.getObsTcr()))) &&
            ((this.sitTit==null && other.getSitTit()==null) || 
             (this.sitTit!=null &&
              this.sitTit.equals(other.getSitTit()))) &&
            ((this.vctOri==null && other.getVctOri()==null) || 
             (this.vctOri!=null &&
              this.vctOri.equals(other.getVctOri()))) &&
            ((this.vctPro==null && other.getVctPro()==null) || 
             (this.vctPro!=null &&
              this.vctPro.equals(other.getVctPro()))) &&
            ((this.vlrOri==null && other.getVlrOri()==null) || 
             (this.vlrOri!=null &&
              this.vlrOri.equals(other.getVlrOri())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCatTef() != null) {
            _hashCode += getCatTef().hashCode();
        }
        if (getCheAge() != null) {
            _hashCode += getCheAge().hashCode();
        }
        if (getCheBan() != null) {
            _hashCode += getCheBan().hashCode();
        }
        if (getCheCta() != null) {
            _hashCode += getCheCta().hashCode();
        }
        if (getCheNum() != null) {
            _hashCode += getCheNum().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodCnv() != null) {
            _hashCode += getCodCnv().hashCode();
        }
        if (getCodCrp() != null) {
            _hashCode += getCodCrp().hashCode();
        }
        if (getCodCrt() != null) {
            _hashCode += getCodCrt().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodMoe() != null) {
            _hashCode += getCodMoe().hashCode();
        }
        if (getCodNtg() != null) {
            _hashCode += getCodNtg().hashCode();
        }
        if (getCodPor() != null) {
            _hashCode += getCodPor().hashCode();
        }
        if (getCodSac() != null) {
            _hashCode += getCodSac().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDatEmi() != null) {
            _hashCode += getDatEmi().hashCode();
        }
        if (getDatEnt() != null) {
            _hashCode += getDatEnt().hashCode();
        }
        if (getDatPpt() != null) {
            _hashCode += getDatPpt().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getNsuTef() != null) {
            _hashCode += getNsuTef().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsTcr() != null) {
            _hashCode += getObsTcr().hashCode();
        }
        if (getSitTit() != null) {
            _hashCode += getSitTit().hashCode();
        }
        if (getVctOri() != null) {
            _hashCode += getVctOri().hashCode();
        }
        if (getVctPro() != null) {
            _hashCode += getVctPro().hashCode();
        }
        if (getVlrOri() != null) {
            _hashCode += getVlrOri().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosAlterarTitulosReceberInGridTitulosAlterar.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosAlterarTitulosReceberInGridTitulosAlterar"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheAge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheAge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheCta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheCta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cheNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cheNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCnv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCnv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCrt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCrt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codMoe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codMoe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNtg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNtg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEmi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEmi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nsuTef");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nsuTef"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsTcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsTcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
