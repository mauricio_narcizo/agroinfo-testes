package br.com.senior.agroinfo.enums;

public enum TipoIntegracao {

    TODOS("T", "Todos"), ALTERADOS("A", "Somente alterados"), ESPECIFICO("E", "Específico");
    
    private final String sigla;
    private final String descricao;
    
    private TipoIntegracao(final String sigla, final String descricao){
        this.sigla = sigla;
        this.descricao = descricao;
    }

    public String getSigla() {
        return this.sigla;
    }

    public String getDescricao() {
        return this.descricao;
    }
    

}
