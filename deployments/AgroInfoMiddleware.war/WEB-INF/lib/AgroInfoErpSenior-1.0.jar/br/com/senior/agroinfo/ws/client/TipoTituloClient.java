package br.com.senior.agroinfo.ws.client;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import br.com.senior.agroinfo.exceptions.TipoTituloWSException;
import br.com.senior.agroinfo.utils.UtilProxy;
import br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo;
import br.com.senior.services.TipotituloExportarIn;
import br.com.senior.services.TipotituloExportarOut;

public class TipoTituloClient {

	private TipoTituloClient() {
	}

	public static TipotituloExportarOut buscarTipoTitulo(final String usuario, final String senha, final String url,
			final TipotituloExportarIn in) throws TipoTituloWSException {
		TipotituloExportarOut out = null;
		try {
			out = UtilProxy.getServiceProxy(Sapiens_Synccom_senior_g5_co_int_varejo_tipotitulo.class, url)
					.exportar(usuario, senha, 0, in);

			if (out.getErroExecucao() != null && !out.getErroExecucao().isEmpty() || out.getTipoTitulo() == null) {
				String msgErro = out.getMensagemRetorno() != null ? out.getMensagemRetorno() : out.getErroExecucao();
				throw new TipoTituloWSException(msgErro);
			}
		} catch (ServiceException | RemoteException e) {
			throw new TipoTituloWSException(e.getMessage());
		}

		return out;
	}
}
