/**
 * TitulosImportarBaixaTitulosReceberIn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosImportarBaixaTitulosReceberIn  implements java.io.Serializable {
    private br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo[] baixaTitulo;

    private java.lang.String flowInstanceID;

    private java.lang.String flowName;

    private java.lang.String sistemaIntegracao;

    public TitulosImportarBaixaTitulosReceberIn() {
    }

    public TitulosImportarBaixaTitulosReceberIn(
           br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo[] baixaTitulo,
           java.lang.String flowInstanceID,
           java.lang.String flowName,
           java.lang.String sistemaIntegracao) {
           this.baixaTitulo = baixaTitulo;
           this.flowInstanceID = flowInstanceID;
           this.flowName = flowName;
           this.sistemaIntegracao = sistemaIntegracao;
    }


    /**
     * Gets the baixaTitulo value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @return baixaTitulo
     */
    public br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo[] getBaixaTitulo() {
        return baixaTitulo;
    }


    /**
     * Sets the baixaTitulo value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @param baixaTitulo
     */
    public void setBaixaTitulo(br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo[] baixaTitulo) {
        this.baixaTitulo = baixaTitulo;
    }

    public br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo getBaixaTitulo(int i) {
        return this.baixaTitulo[i];
    }

    public void setBaixaTitulo(int i, br.com.senior.services.TitulosImportarBaixaTitulosReceberInBaixaTitulo _value) {
        this.baixaTitulo[i] = _value;
    }


    /**
     * Gets the flowInstanceID value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @return flowInstanceID
     */
    public java.lang.String getFlowInstanceID() {
        return flowInstanceID;
    }


    /**
     * Sets the flowInstanceID value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @param flowInstanceID
     */
    public void setFlowInstanceID(java.lang.String flowInstanceID) {
        this.flowInstanceID = flowInstanceID;
    }


    /**
     * Gets the flowName value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @return flowName
     */
    public java.lang.String getFlowName() {
        return flowName;
    }


    /**
     * Sets the flowName value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @param flowName
     */
    public void setFlowName(java.lang.String flowName) {
        this.flowName = flowName;
    }


    /**
     * Gets the sistemaIntegracao value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @return sistemaIntegracao
     */
    public java.lang.String getSistemaIntegracao() {
        return sistemaIntegracao;
    }


    /**
     * Sets the sistemaIntegracao value for this TitulosImportarBaixaTitulosReceberIn.
     * 
     * @param sistemaIntegracao
     */
    public void setSistemaIntegracao(java.lang.String sistemaIntegracao) {
        this.sistemaIntegracao = sistemaIntegracao;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosImportarBaixaTitulosReceberIn)) return false;
        TitulosImportarBaixaTitulosReceberIn other = (TitulosImportarBaixaTitulosReceberIn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baixaTitulo==null && other.getBaixaTitulo()==null) || 
             (this.baixaTitulo!=null &&
              java.util.Arrays.equals(this.baixaTitulo, other.getBaixaTitulo()))) &&
            ((this.flowInstanceID==null && other.getFlowInstanceID()==null) || 
             (this.flowInstanceID!=null &&
              this.flowInstanceID.equals(other.getFlowInstanceID()))) &&
            ((this.flowName==null && other.getFlowName()==null) || 
             (this.flowName!=null &&
              this.flowName.equals(other.getFlowName()))) &&
            ((this.sistemaIntegracao==null && other.getSistemaIntegracao()==null) || 
             (this.sistemaIntegracao!=null &&
              this.sistemaIntegracao.equals(other.getSistemaIntegracao())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaixaTitulo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBaixaTitulo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBaixaTitulo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFlowInstanceID() != null) {
            _hashCode += getFlowInstanceID().hashCode();
        }
        if (getFlowName() != null) {
            _hashCode += getFlowName().hashCode();
        }
        if (getSistemaIntegracao() != null) {
            _hashCode += getSistemaIntegracao().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosImportarBaixaTitulosReceberIn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosImportarBaixaTitulosReceberIn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baixaTitulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baixaTitulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosImportarBaixaTitulosReceberInBaixaTitulo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowInstanceID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowInstanceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flowName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "flowName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sistemaIntegracao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sistemaIntegracao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
