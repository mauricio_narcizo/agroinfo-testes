/**
 * FornecedoresExportar2OutGridFornecedoresContatos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresExportar2OutGridFornecedoresContatos  implements java.io.Serializable {
    private java.lang.String carCto;

    private java.lang.Integer codFor;

    private java.lang.Integer codNiv;

    private java.lang.Double cpfCto;

    private java.lang.String datNas;

    private java.lang.String faxCto;

    private java.lang.String fonCto;

    private java.lang.String hobCon;

    private java.lang.String intNet;

    private java.lang.String nivCto;

    private java.lang.String nomCto;

    private java.lang.Integer ramCto;

    private java.lang.Integer seqCto;

    private java.lang.String setCto;

    private java.lang.String sitCto;

    private java.lang.String timCon;

    private java.lang.Integer tipInt;

    public FornecedoresExportar2OutGridFornecedoresContatos() {
    }

    public FornecedoresExportar2OutGridFornecedoresContatos(
           java.lang.String carCto,
           java.lang.Integer codFor,
           java.lang.Integer codNiv,
           java.lang.Double cpfCto,
           java.lang.String datNas,
           java.lang.String faxCto,
           java.lang.String fonCto,
           java.lang.String hobCon,
           java.lang.String intNet,
           java.lang.String nivCto,
           java.lang.String nomCto,
           java.lang.Integer ramCto,
           java.lang.Integer seqCto,
           java.lang.String setCto,
           java.lang.String sitCto,
           java.lang.String timCon,
           java.lang.Integer tipInt) {
           this.carCto = carCto;
           this.codFor = codFor;
           this.codNiv = codNiv;
           this.cpfCto = cpfCto;
           this.datNas = datNas;
           this.faxCto = faxCto;
           this.fonCto = fonCto;
           this.hobCon = hobCon;
           this.intNet = intNet;
           this.nivCto = nivCto;
           this.nomCto = nomCto;
           this.ramCto = ramCto;
           this.seqCto = seqCto;
           this.setCto = setCto;
           this.sitCto = sitCto;
           this.timCon = timCon;
           this.tipInt = tipInt;
    }


    /**
     * Gets the carCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return carCto
     */
    public java.lang.String getCarCto() {
        return carCto;
    }


    /**
     * Sets the carCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param carCto
     */
    public void setCarCto(java.lang.String carCto) {
        this.carCto = carCto;
    }


    /**
     * Gets the codFor value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return codFor
     */
    public java.lang.Integer getCodFor() {
        return codFor;
    }


    /**
     * Sets the codFor value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param codFor
     */
    public void setCodFor(java.lang.Integer codFor) {
        this.codFor = codFor;
    }


    /**
     * Gets the codNiv value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return codNiv
     */
    public java.lang.Integer getCodNiv() {
        return codNiv;
    }


    /**
     * Sets the codNiv value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param codNiv
     */
    public void setCodNiv(java.lang.Integer codNiv) {
        this.codNiv = codNiv;
    }


    /**
     * Gets the cpfCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return cpfCto
     */
    public java.lang.Double getCpfCto() {
        return cpfCto;
    }


    /**
     * Sets the cpfCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param cpfCto
     */
    public void setCpfCto(java.lang.Double cpfCto) {
        this.cpfCto = cpfCto;
    }


    /**
     * Gets the datNas value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return datNas
     */
    public java.lang.String getDatNas() {
        return datNas;
    }


    /**
     * Sets the datNas value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param datNas
     */
    public void setDatNas(java.lang.String datNas) {
        this.datNas = datNas;
    }


    /**
     * Gets the faxCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return faxCto
     */
    public java.lang.String getFaxCto() {
        return faxCto;
    }


    /**
     * Sets the faxCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param faxCto
     */
    public void setFaxCto(java.lang.String faxCto) {
        this.faxCto = faxCto;
    }


    /**
     * Gets the fonCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return fonCto
     */
    public java.lang.String getFonCto() {
        return fonCto;
    }


    /**
     * Sets the fonCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param fonCto
     */
    public void setFonCto(java.lang.String fonCto) {
        this.fonCto = fonCto;
    }


    /**
     * Gets the hobCon value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return hobCon
     */
    public java.lang.String getHobCon() {
        return hobCon;
    }


    /**
     * Sets the hobCon value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param hobCon
     */
    public void setHobCon(java.lang.String hobCon) {
        this.hobCon = hobCon;
    }


    /**
     * Gets the intNet value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the nivCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return nivCto
     */
    public java.lang.String getNivCto() {
        return nivCto;
    }


    /**
     * Sets the nivCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param nivCto
     */
    public void setNivCto(java.lang.String nivCto) {
        this.nivCto = nivCto;
    }


    /**
     * Gets the nomCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return nomCto
     */
    public java.lang.String getNomCto() {
        return nomCto;
    }


    /**
     * Sets the nomCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param nomCto
     */
    public void setNomCto(java.lang.String nomCto) {
        this.nomCto = nomCto;
    }


    /**
     * Gets the ramCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return ramCto
     */
    public java.lang.Integer getRamCto() {
        return ramCto;
    }


    /**
     * Sets the ramCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param ramCto
     */
    public void setRamCto(java.lang.Integer ramCto) {
        this.ramCto = ramCto;
    }


    /**
     * Gets the seqCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return seqCto
     */
    public java.lang.Integer getSeqCto() {
        return seqCto;
    }


    /**
     * Sets the seqCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param seqCto
     */
    public void setSeqCto(java.lang.Integer seqCto) {
        this.seqCto = seqCto;
    }


    /**
     * Gets the setCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return setCto
     */
    public java.lang.String getSetCto() {
        return setCto;
    }


    /**
     * Sets the setCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param setCto
     */
    public void setSetCto(java.lang.String setCto) {
        this.setCto = setCto;
    }


    /**
     * Gets the sitCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return sitCto
     */
    public java.lang.String getSitCto() {
        return sitCto;
    }


    /**
     * Sets the sitCto value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param sitCto
     */
    public void setSitCto(java.lang.String sitCto) {
        this.sitCto = sitCto;
    }


    /**
     * Gets the timCon value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return timCon
     */
    public java.lang.String getTimCon() {
        return timCon;
    }


    /**
     * Sets the timCon value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param timCon
     */
    public void setTimCon(java.lang.String timCon) {
        this.timCon = timCon;
    }


    /**
     * Gets the tipInt value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @return tipInt
     */
    public java.lang.Integer getTipInt() {
        return tipInt;
    }


    /**
     * Sets the tipInt value for this FornecedoresExportar2OutGridFornecedoresContatos.
     * 
     * @param tipInt
     */
    public void setTipInt(java.lang.Integer tipInt) {
        this.tipInt = tipInt;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresExportar2OutGridFornecedoresContatos)) return false;
        FornecedoresExportar2OutGridFornecedoresContatos other = (FornecedoresExportar2OutGridFornecedoresContatos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.carCto==null && other.getCarCto()==null) || 
             (this.carCto!=null &&
              this.carCto.equals(other.getCarCto()))) &&
            ((this.codFor==null && other.getCodFor()==null) || 
             (this.codFor!=null &&
              this.codFor.equals(other.getCodFor()))) &&
            ((this.codNiv==null && other.getCodNiv()==null) || 
             (this.codNiv!=null &&
              this.codNiv.equals(other.getCodNiv()))) &&
            ((this.cpfCto==null && other.getCpfCto()==null) || 
             (this.cpfCto!=null &&
              this.cpfCto.equals(other.getCpfCto()))) &&
            ((this.datNas==null && other.getDatNas()==null) || 
             (this.datNas!=null &&
              this.datNas.equals(other.getDatNas()))) &&
            ((this.faxCto==null && other.getFaxCto()==null) || 
             (this.faxCto!=null &&
              this.faxCto.equals(other.getFaxCto()))) &&
            ((this.fonCto==null && other.getFonCto()==null) || 
             (this.fonCto!=null &&
              this.fonCto.equals(other.getFonCto()))) &&
            ((this.hobCon==null && other.getHobCon()==null) || 
             (this.hobCon!=null &&
              this.hobCon.equals(other.getHobCon()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.nivCto==null && other.getNivCto()==null) || 
             (this.nivCto!=null &&
              this.nivCto.equals(other.getNivCto()))) &&
            ((this.nomCto==null && other.getNomCto()==null) || 
             (this.nomCto!=null &&
              this.nomCto.equals(other.getNomCto()))) &&
            ((this.ramCto==null && other.getRamCto()==null) || 
             (this.ramCto!=null &&
              this.ramCto.equals(other.getRamCto()))) &&
            ((this.seqCto==null && other.getSeqCto()==null) || 
             (this.seqCto!=null &&
              this.seqCto.equals(other.getSeqCto()))) &&
            ((this.setCto==null && other.getSetCto()==null) || 
             (this.setCto!=null &&
              this.setCto.equals(other.getSetCto()))) &&
            ((this.sitCto==null && other.getSitCto()==null) || 
             (this.sitCto!=null &&
              this.sitCto.equals(other.getSitCto()))) &&
            ((this.timCon==null && other.getTimCon()==null) || 
             (this.timCon!=null &&
              this.timCon.equals(other.getTimCon()))) &&
            ((this.tipInt==null && other.getTipInt()==null) || 
             (this.tipInt!=null &&
              this.tipInt.equals(other.getTipInt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCarCto() != null) {
            _hashCode += getCarCto().hashCode();
        }
        if (getCodFor() != null) {
            _hashCode += getCodFor().hashCode();
        }
        if (getCodNiv() != null) {
            _hashCode += getCodNiv().hashCode();
        }
        if (getCpfCto() != null) {
            _hashCode += getCpfCto().hashCode();
        }
        if (getDatNas() != null) {
            _hashCode += getDatNas().hashCode();
        }
        if (getFaxCto() != null) {
            _hashCode += getFaxCto().hashCode();
        }
        if (getFonCto() != null) {
            _hashCode += getFonCto().hashCode();
        }
        if (getHobCon() != null) {
            _hashCode += getHobCon().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getNivCto() != null) {
            _hashCode += getNivCto().hashCode();
        }
        if (getNomCto() != null) {
            _hashCode += getNomCto().hashCode();
        }
        if (getRamCto() != null) {
            _hashCode += getRamCto().hashCode();
        }
        if (getSeqCto() != null) {
            _hashCode += getSeqCto().hashCode();
        }
        if (getSetCto() != null) {
            _hashCode += getSetCto().hashCode();
        }
        if (getSitCto() != null) {
            _hashCode += getSitCto().hashCode();
        }
        if (getTimCon() != null) {
            _hashCode += getTimCon().hashCode();
        }
        if (getTipInt() != null) {
            _hashCode += getTipInt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresExportar2OutGridFornecedoresContatos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresExportar2OutGridFornecedoresContatos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "carCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codNiv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codNiv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cpfCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cpfCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datNas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datNas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hobCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hobCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nivCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nivCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ramCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ramCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("setCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "setCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitCto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitCto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timCon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timCon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
