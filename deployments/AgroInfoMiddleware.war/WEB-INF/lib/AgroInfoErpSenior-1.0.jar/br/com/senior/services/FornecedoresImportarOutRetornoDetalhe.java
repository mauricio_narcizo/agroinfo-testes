/**
 * FornecedoresImportarOutRetornoDetalhe.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class FornecedoresImportarOutRetornoDetalhe  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String msgRet;

    private java.lang.Integer tipRet;

    public FornecedoresImportarOutRetornoDetalhe() {
    }

    public FornecedoresImportarOutRetornoDetalhe(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String msgRet,
           java.lang.Integer tipRet) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.msgRet = msgRet;
           this.tipRet = tipRet;
    }


    /**
     * Gets the codEmp value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the msgRet value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @return msgRet
     */
    public java.lang.String getMsgRet() {
        return msgRet;
    }


    /**
     * Sets the msgRet value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @param msgRet
     */
    public void setMsgRet(java.lang.String msgRet) {
        this.msgRet = msgRet;
    }


    /**
     * Gets the tipRet value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @return tipRet
     */
    public java.lang.Integer getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this FornecedoresImportarOutRetornoDetalhe.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.Integer tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FornecedoresImportarOutRetornoDetalhe)) return false;
        FornecedoresImportarOutRetornoDetalhe other = (FornecedoresImportarOutRetornoDetalhe) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.msgRet==null && other.getMsgRet()==null) || 
             (this.msgRet!=null &&
              this.msgRet.equals(other.getMsgRet()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getMsgRet() != null) {
            _hashCode += getMsgRet().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FornecedoresImportarOutRetornoDetalhe.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "fornecedoresImportarOutRetornoDetalhe"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
