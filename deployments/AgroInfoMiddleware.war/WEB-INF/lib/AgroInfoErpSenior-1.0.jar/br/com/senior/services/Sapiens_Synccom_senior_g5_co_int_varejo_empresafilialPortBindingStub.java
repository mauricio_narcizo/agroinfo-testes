/**
 * Sapiens_Synccom_senior_g5_co_int_varejo_empresafilialPortBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class Sapiens_Synccom_senior_g5_co_int_varejo_empresafilialPortBindingStub extends org.apache.axis.client.Stub implements br.com.senior.services.Sapiens_Synccom_senior_g5_co_int_varejo_empresafilial {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[7];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarIn"), br.com.senior.services.EmpresafilialExportarIn.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOut"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportarOut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_3");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3In"), br.com.senior.services.EmpresafilialExportar3In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar3Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2In"), br.com.senior.services.EmpresafilialExportar2In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar2Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_4");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4In"), br.com.senior.services.EmpresafilialExportar4In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar4Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_5");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5In"), br.com.senior.services.EmpresafilialExportar5In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar5Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_7");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7In"), br.com.senior.services.EmpresafilialExportar7In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar7Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Exportar_6");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "encryption"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "parameters"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6In"), br.com.senior.services.EmpresafilialExportar6In.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6Out"));
        oper.setReturnClass(br.com.senior.services.EmpresafilialExportar6Out.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "result"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_empresafilialPortBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_empresafilialPortBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Sapiens_Synccom_senior_g5_co_int_varejo_empresafilialPortBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar2OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar2OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar3OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar4OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar4OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar5OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar5OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar6OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7In");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7In.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7Out");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7Out.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialContaInternaCaixas");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialContaInternaCaixas.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialContaInternaCartaoPresente");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialContaInternaCartaoPresente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialDepositoLoja");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialDepositoLoja.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar7OutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportar7OutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarIn");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarIn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOut");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilial");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutEmpresaFilial.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialDadosEstado");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialDadosPDV");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialEnderecoRetirada");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialOrdAplicaTabJur");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutGridErros");
            cachedSerQNames.add(qName);
            cls = br.com.senior.services.EmpresafilialExportarOutGridErros.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public br.com.senior.services.EmpresafilialExportarOut exportar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportarIn parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportarOut) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportarOut) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportarOut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar3Out exportar_3(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar3In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_3"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar3Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar3Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar3Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar2Out exportar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar2In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar2Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar2Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar2Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar4Out exportar_4(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar4In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_4"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar4Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar4Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar4Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar5Out exportar_5(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar5In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_5"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar5Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar5Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar5Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar7Out exportar_7(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar7In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_7"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar7Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar7Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar7Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public br.com.senior.services.EmpresafilialExportar6Out exportar_6(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.EmpresafilialExportar6In parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://services.senior.com.br", "Exportar_6"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {user, password, new java.lang.Integer(encryption), parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (br.com.senior.services.EmpresafilialExportar6Out) _resp;
            } catch (java.lang.Exception _exception) {
                return (br.com.senior.services.EmpresafilialExportar6Out) org.apache.axis.utils.JavaUtils.convert(_resp, br.com.senior.services.EmpresafilialExportar6Out.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
