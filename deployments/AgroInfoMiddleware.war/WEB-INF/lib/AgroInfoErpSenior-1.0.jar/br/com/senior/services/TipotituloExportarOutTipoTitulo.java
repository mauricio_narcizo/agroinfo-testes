/**
 * TipotituloExportarOutTipoTitulo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TipotituloExportarOutTipoTitulo  implements java.io.Serializable {
    private java.lang.String abrTpt;

    private java.lang.String aplTpt;

    private java.lang.String codImp;

    private java.lang.Integer codPdv;

    private java.lang.String codTpt;

    private java.lang.String desTpt;

    private java.lang.String excVar;

    private java.lang.String gerDct;

    private java.lang.Integer pagEev;

    private java.lang.String pagSom;

    private java.lang.String recSom;

    private java.lang.String seqInt;

    private java.lang.String sitTpt;

    private java.lang.String somIrf;

    private java.lang.Integer titFis;

    private java.lang.String venCac;

    public TipotituloExportarOutTipoTitulo() {
    }

    public TipotituloExportarOutTipoTitulo(
           java.lang.String abrTpt,
           java.lang.String aplTpt,
           java.lang.String codImp,
           java.lang.Integer codPdv,
           java.lang.String codTpt,
           java.lang.String desTpt,
           java.lang.String excVar,
           java.lang.String gerDct,
           java.lang.Integer pagEev,
           java.lang.String pagSom,
           java.lang.String recSom,
           java.lang.String seqInt,
           java.lang.String sitTpt,
           java.lang.String somIrf,
           java.lang.Integer titFis,
           java.lang.String venCac) {
           this.abrTpt = abrTpt;
           this.aplTpt = aplTpt;
           this.codImp = codImp;
           this.codPdv = codPdv;
           this.codTpt = codTpt;
           this.desTpt = desTpt;
           this.excVar = excVar;
           this.gerDct = gerDct;
           this.pagEev = pagEev;
           this.pagSom = pagSom;
           this.recSom = recSom;
           this.seqInt = seqInt;
           this.sitTpt = sitTpt;
           this.somIrf = somIrf;
           this.titFis = titFis;
           this.venCac = venCac;
    }


    /**
     * Gets the abrTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return abrTpt
     */
    public java.lang.String getAbrTpt() {
        return abrTpt;
    }


    /**
     * Sets the abrTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param abrTpt
     */
    public void setAbrTpt(java.lang.String abrTpt) {
        this.abrTpt = abrTpt;
    }


    /**
     * Gets the aplTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return aplTpt
     */
    public java.lang.String getAplTpt() {
        return aplTpt;
    }


    /**
     * Sets the aplTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param aplTpt
     */
    public void setAplTpt(java.lang.String aplTpt) {
        this.aplTpt = aplTpt;
    }


    /**
     * Gets the codImp value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return codImp
     */
    public java.lang.String getCodImp() {
        return codImp;
    }


    /**
     * Sets the codImp value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param codImp
     */
    public void setCodImp(java.lang.String codImp) {
        this.codImp = codImp;
    }


    /**
     * Gets the codPdv value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return codPdv
     */
    public java.lang.Integer getCodPdv() {
        return codPdv;
    }


    /**
     * Sets the codPdv value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param codPdv
     */
    public void setCodPdv(java.lang.Integer codPdv) {
        this.codPdv = codPdv;
    }


    /**
     * Gets the codTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the desTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return desTpt
     */
    public java.lang.String getDesTpt() {
        return desTpt;
    }


    /**
     * Sets the desTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param desTpt
     */
    public void setDesTpt(java.lang.String desTpt) {
        this.desTpt = desTpt;
    }


    /**
     * Gets the excVar value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return excVar
     */
    public java.lang.String getExcVar() {
        return excVar;
    }


    /**
     * Sets the excVar value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param excVar
     */
    public void setExcVar(java.lang.String excVar) {
        this.excVar = excVar;
    }


    /**
     * Gets the gerDct value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return gerDct
     */
    public java.lang.String getGerDct() {
        return gerDct;
    }


    /**
     * Sets the gerDct value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param gerDct
     */
    public void setGerDct(java.lang.String gerDct) {
        this.gerDct = gerDct;
    }


    /**
     * Gets the pagEev value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return pagEev
     */
    public java.lang.Integer getPagEev() {
        return pagEev;
    }


    /**
     * Sets the pagEev value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param pagEev
     */
    public void setPagEev(java.lang.Integer pagEev) {
        this.pagEev = pagEev;
    }


    /**
     * Gets the pagSom value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return pagSom
     */
    public java.lang.String getPagSom() {
        return pagSom;
    }


    /**
     * Sets the pagSom value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param pagSom
     */
    public void setPagSom(java.lang.String pagSom) {
        this.pagSom = pagSom;
    }


    /**
     * Gets the recSom value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return recSom
     */
    public java.lang.String getRecSom() {
        return recSom;
    }


    /**
     * Sets the recSom value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param recSom
     */
    public void setRecSom(java.lang.String recSom) {
        this.recSom = recSom;
    }


    /**
     * Gets the seqInt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return seqInt
     */
    public java.lang.String getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.String seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the sitTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return sitTpt
     */
    public java.lang.String getSitTpt() {
        return sitTpt;
    }


    /**
     * Sets the sitTpt value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param sitTpt
     */
    public void setSitTpt(java.lang.String sitTpt) {
        this.sitTpt = sitTpt;
    }


    /**
     * Gets the somIrf value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return somIrf
     */
    public java.lang.String getSomIrf() {
        return somIrf;
    }


    /**
     * Sets the somIrf value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param somIrf
     */
    public void setSomIrf(java.lang.String somIrf) {
        this.somIrf = somIrf;
    }


    /**
     * Gets the titFis value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return titFis
     */
    public java.lang.Integer getTitFis() {
        return titFis;
    }


    /**
     * Sets the titFis value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param titFis
     */
    public void setTitFis(java.lang.Integer titFis) {
        this.titFis = titFis;
    }


    /**
     * Gets the venCac value for this TipotituloExportarOutTipoTitulo.
     * 
     * @return venCac
     */
    public java.lang.String getVenCac() {
        return venCac;
    }


    /**
     * Sets the venCac value for this TipotituloExportarOutTipoTitulo.
     * 
     * @param venCac
     */
    public void setVenCac(java.lang.String venCac) {
        this.venCac = venCac;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TipotituloExportarOutTipoTitulo)) return false;
        TipotituloExportarOutTipoTitulo other = (TipotituloExportarOutTipoTitulo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.abrTpt==null && other.getAbrTpt()==null) || 
             (this.abrTpt!=null &&
              this.abrTpt.equals(other.getAbrTpt()))) &&
            ((this.aplTpt==null && other.getAplTpt()==null) || 
             (this.aplTpt!=null &&
              this.aplTpt.equals(other.getAplTpt()))) &&
            ((this.codImp==null && other.getCodImp()==null) || 
             (this.codImp!=null &&
              this.codImp.equals(other.getCodImp()))) &&
            ((this.codPdv==null && other.getCodPdv()==null) || 
             (this.codPdv!=null &&
              this.codPdv.equals(other.getCodPdv()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.desTpt==null && other.getDesTpt()==null) || 
             (this.desTpt!=null &&
              this.desTpt.equals(other.getDesTpt()))) &&
            ((this.excVar==null && other.getExcVar()==null) || 
             (this.excVar!=null &&
              this.excVar.equals(other.getExcVar()))) &&
            ((this.gerDct==null && other.getGerDct()==null) || 
             (this.gerDct!=null &&
              this.gerDct.equals(other.getGerDct()))) &&
            ((this.pagEev==null && other.getPagEev()==null) || 
             (this.pagEev!=null &&
              this.pagEev.equals(other.getPagEev()))) &&
            ((this.pagSom==null && other.getPagSom()==null) || 
             (this.pagSom!=null &&
              this.pagSom.equals(other.getPagSom()))) &&
            ((this.recSom==null && other.getRecSom()==null) || 
             (this.recSom!=null &&
              this.recSom.equals(other.getRecSom()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.sitTpt==null && other.getSitTpt()==null) || 
             (this.sitTpt!=null &&
              this.sitTpt.equals(other.getSitTpt()))) &&
            ((this.somIrf==null && other.getSomIrf()==null) || 
             (this.somIrf!=null &&
              this.somIrf.equals(other.getSomIrf()))) &&
            ((this.titFis==null && other.getTitFis()==null) || 
             (this.titFis!=null &&
              this.titFis.equals(other.getTitFis()))) &&
            ((this.venCac==null && other.getVenCac()==null) || 
             (this.venCac!=null &&
              this.venCac.equals(other.getVenCac())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAbrTpt() != null) {
            _hashCode += getAbrTpt().hashCode();
        }
        if (getAplTpt() != null) {
            _hashCode += getAplTpt().hashCode();
        }
        if (getCodImp() != null) {
            _hashCode += getCodImp().hashCode();
        }
        if (getCodPdv() != null) {
            _hashCode += getCodPdv().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDesTpt() != null) {
            _hashCode += getDesTpt().hashCode();
        }
        if (getExcVar() != null) {
            _hashCode += getExcVar().hashCode();
        }
        if (getGerDct() != null) {
            _hashCode += getGerDct().hashCode();
        }
        if (getPagEev() != null) {
            _hashCode += getPagEev().hashCode();
        }
        if (getPagSom() != null) {
            _hashCode += getPagSom().hashCode();
        }
        if (getRecSom() != null) {
            _hashCode += getRecSom().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSitTpt() != null) {
            _hashCode += getSitTpt().hashCode();
        }
        if (getSomIrf() != null) {
            _hashCode += getSomIrf().hashCode();
        }
        if (getTitFis() != null) {
            _hashCode += getTitFis().hashCode();
        }
        if (getVenCac() != null) {
            _hashCode += getVenCac().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipotituloExportarOutTipoTitulo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "tipotituloExportarOutTipoTitulo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abrTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abrTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("aplTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "aplTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codImp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codImp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excVar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "excVar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gerDct");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gerDct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagEev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagEev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagSom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagSom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recSom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recSom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("somIrf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "somIrf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titFis");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titFis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venCac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venCac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
