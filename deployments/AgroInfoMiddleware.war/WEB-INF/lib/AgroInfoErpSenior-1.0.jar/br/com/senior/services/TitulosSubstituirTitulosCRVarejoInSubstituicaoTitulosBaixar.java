/**
 * TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar  implements java.io.Serializable {
    private java.lang.Integer codCli;

    private java.lang.Integer codFil;

    private java.lang.String codTpt;

    private java.lang.String datLib;

    private java.lang.String numInt;

    private java.lang.String numTit;

    private java.lang.String obsMcr;

    private java.lang.Double vlrBai;

    private java.lang.Double vlrCor;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrEnc;

    private java.lang.Double vlrJrs;

    private java.lang.Double vlrMul;

    private java.lang.Double vlrOac;

    private java.lang.Double vlrOde;

    public TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar() {
    }

    public TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar(
           java.lang.Integer codCli,
           java.lang.Integer codFil,
           java.lang.String codTpt,
           java.lang.String datLib,
           java.lang.String numInt,
           java.lang.String numTit,
           java.lang.String obsMcr,
           java.lang.Double vlrBai,
           java.lang.Double vlrCor,
           java.lang.Double vlrDsc,
           java.lang.Double vlrEnc,
           java.lang.Double vlrJrs,
           java.lang.Double vlrMul,
           java.lang.Double vlrOac,
           java.lang.Double vlrOde) {
           this.codCli = codCli;
           this.codFil = codFil;
           this.codTpt = codTpt;
           this.datLib = datLib;
           this.numInt = numInt;
           this.numTit = numTit;
           this.obsMcr = obsMcr;
           this.vlrBai = vlrBai;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrEnc = vlrEnc;
           this.vlrJrs = vlrJrs;
           this.vlrMul = vlrMul;
           this.vlrOac = vlrOac;
           this.vlrOde = vlrOde;
    }


    /**
     * Gets the codCli value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codTpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the datLib value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return datLib
     */
    public java.lang.String getDatLib() {
        return datLib;
    }


    /**
     * Sets the datLib value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param datLib
     */
    public void setDatLib(java.lang.String datLib) {
        this.datLib = datLib;
    }


    /**
     * Gets the numInt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return numInt
     */
    public java.lang.String getNumInt() {
        return numInt;
    }


    /**
     * Sets the numInt value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param numInt
     */
    public void setNumInt(java.lang.String numInt) {
        this.numInt = numInt;
    }


    /**
     * Gets the numTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the obsMcr value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return obsMcr
     */
    public java.lang.String getObsMcr() {
        return obsMcr;
    }


    /**
     * Sets the obsMcr value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param obsMcr
     */
    public void setObsMcr(java.lang.String obsMcr) {
        this.obsMcr = obsMcr;
    }


    /**
     * Gets the vlrBai value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrBai
     */
    public java.lang.Double getVlrBai() {
        return vlrBai;
    }


    /**
     * Sets the vlrBai value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrBai
     */
    public void setVlrBai(java.lang.Double vlrBai) {
        this.vlrBai = vlrBai;
    }


    /**
     * Gets the vlrCor value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrCor
     */
    public java.lang.Double getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.Double vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEnc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrEnc
     */
    public java.lang.Double getVlrEnc() {
        return vlrEnc;
    }


    /**
     * Sets the vlrEnc value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrEnc
     */
    public void setVlrEnc(java.lang.Double vlrEnc) {
        this.vlrEnc = vlrEnc;
    }


    /**
     * Gets the vlrJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrJrs
     */
    public java.lang.Double getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.Double vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrMul
     */
    public java.lang.Double getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.Double vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOac value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrOac
     */
    public java.lang.Double getVlrOac() {
        return vlrOac;
    }


    /**
     * Sets the vlrOac value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrOac
     */
    public void setVlrOac(java.lang.Double vlrOac) {
        this.vlrOac = vlrOac;
    }


    /**
     * Gets the vlrOde value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @return vlrOde
     */
    public java.lang.Double getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.Double vlrOde) {
        this.vlrOde = vlrOde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar)) return false;
        TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar other = (TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.datLib==null && other.getDatLib()==null) || 
             (this.datLib!=null &&
              this.datLib.equals(other.getDatLib()))) &&
            ((this.numInt==null && other.getNumInt()==null) || 
             (this.numInt!=null &&
              this.numInt.equals(other.getNumInt()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.obsMcr==null && other.getObsMcr()==null) || 
             (this.obsMcr!=null &&
              this.obsMcr.equals(other.getObsMcr()))) &&
            ((this.vlrBai==null && other.getVlrBai()==null) || 
             (this.vlrBai!=null &&
              this.vlrBai.equals(other.getVlrBai()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEnc==null && other.getVlrEnc()==null) || 
             (this.vlrEnc!=null &&
              this.vlrEnc.equals(other.getVlrEnc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOac==null && other.getVlrOac()==null) || 
             (this.vlrOac!=null &&
              this.vlrOac.equals(other.getVlrOac()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDatLib() != null) {
            _hashCode += getDatLib().hashCode();
        }
        if (getNumInt() != null) {
            _hashCode += getNumInt().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getObsMcr() != null) {
            _hashCode += getObsMcr().hashCode();
        }
        if (getVlrBai() != null) {
            _hashCode += getVlrBai().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEnc() != null) {
            _hashCode += getVlrEnc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOac() != null) {
            _hashCode += getVlrOac().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCRVarejoInSubstituicaoTitulosBaixar"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLib");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLib"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obsMcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obsMcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
