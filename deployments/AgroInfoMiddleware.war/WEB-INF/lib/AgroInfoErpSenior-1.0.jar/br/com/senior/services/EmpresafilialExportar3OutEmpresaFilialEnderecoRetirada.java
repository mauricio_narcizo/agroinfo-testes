/**
 * EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada  implements java.io.Serializable {
    private java.lang.String baiRet;

    private java.lang.Integer cepRet;

    private java.lang.String cgcRet;

    private java.lang.String cidRet;

    private java.lang.Integer codRet;

    private java.lang.String cplRet;

    private java.lang.String emaRet;

    private java.lang.String endRet;

    private java.lang.String estRet;

    private java.lang.String faxRet;

    private java.lang.String fonRet;

    private java.lang.Integer iniRet;

    private java.lang.String nenRet;

    private java.lang.String pesRet;

    private java.lang.String sitReg;

    private java.lang.String tipRet;

    public EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada() {
    }

    public EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada(
           java.lang.String baiRet,
           java.lang.Integer cepRet,
           java.lang.String cgcRet,
           java.lang.String cidRet,
           java.lang.Integer codRet,
           java.lang.String cplRet,
           java.lang.String emaRet,
           java.lang.String endRet,
           java.lang.String estRet,
           java.lang.String faxRet,
           java.lang.String fonRet,
           java.lang.Integer iniRet,
           java.lang.String nenRet,
           java.lang.String pesRet,
           java.lang.String sitReg,
           java.lang.String tipRet) {
           this.baiRet = baiRet;
           this.cepRet = cepRet;
           this.cgcRet = cgcRet;
           this.cidRet = cidRet;
           this.codRet = codRet;
           this.cplRet = cplRet;
           this.emaRet = emaRet;
           this.endRet = endRet;
           this.estRet = estRet;
           this.faxRet = faxRet;
           this.fonRet = fonRet;
           this.iniRet = iniRet;
           this.nenRet = nenRet;
           this.pesRet = pesRet;
           this.sitReg = sitReg;
           this.tipRet = tipRet;
    }


    /**
     * Gets the baiRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return baiRet
     */
    public java.lang.String getBaiRet() {
        return baiRet;
    }


    /**
     * Sets the baiRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param baiRet
     */
    public void setBaiRet(java.lang.String baiRet) {
        this.baiRet = baiRet;
    }


    /**
     * Gets the cepRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return cepRet
     */
    public java.lang.Integer getCepRet() {
        return cepRet;
    }


    /**
     * Sets the cepRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param cepRet
     */
    public void setCepRet(java.lang.Integer cepRet) {
        this.cepRet = cepRet;
    }


    /**
     * Gets the cgcRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return cgcRet
     */
    public java.lang.String getCgcRet() {
        return cgcRet;
    }


    /**
     * Sets the cgcRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param cgcRet
     */
    public void setCgcRet(java.lang.String cgcRet) {
        this.cgcRet = cgcRet;
    }


    /**
     * Gets the cidRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return cidRet
     */
    public java.lang.String getCidRet() {
        return cidRet;
    }


    /**
     * Sets the cidRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param cidRet
     */
    public void setCidRet(java.lang.String cidRet) {
        this.cidRet = cidRet;
    }


    /**
     * Gets the codRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return codRet
     */
    public java.lang.Integer getCodRet() {
        return codRet;
    }


    /**
     * Sets the codRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param codRet
     */
    public void setCodRet(java.lang.Integer codRet) {
        this.codRet = codRet;
    }


    /**
     * Gets the cplRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return cplRet
     */
    public java.lang.String getCplRet() {
        return cplRet;
    }


    /**
     * Sets the cplRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param cplRet
     */
    public void setCplRet(java.lang.String cplRet) {
        this.cplRet = cplRet;
    }


    /**
     * Gets the emaRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return emaRet
     */
    public java.lang.String getEmaRet() {
        return emaRet;
    }


    /**
     * Sets the emaRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param emaRet
     */
    public void setEmaRet(java.lang.String emaRet) {
        this.emaRet = emaRet;
    }


    /**
     * Gets the endRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return endRet
     */
    public java.lang.String getEndRet() {
        return endRet;
    }


    /**
     * Sets the endRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param endRet
     */
    public void setEndRet(java.lang.String endRet) {
        this.endRet = endRet;
    }


    /**
     * Gets the estRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return estRet
     */
    public java.lang.String getEstRet() {
        return estRet;
    }


    /**
     * Sets the estRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param estRet
     */
    public void setEstRet(java.lang.String estRet) {
        this.estRet = estRet;
    }


    /**
     * Gets the faxRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return faxRet
     */
    public java.lang.String getFaxRet() {
        return faxRet;
    }


    /**
     * Sets the faxRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param faxRet
     */
    public void setFaxRet(java.lang.String faxRet) {
        this.faxRet = faxRet;
    }


    /**
     * Gets the fonRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return fonRet
     */
    public java.lang.String getFonRet() {
        return fonRet;
    }


    /**
     * Sets the fonRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param fonRet
     */
    public void setFonRet(java.lang.String fonRet) {
        this.fonRet = fonRet;
    }


    /**
     * Gets the iniRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return iniRet
     */
    public java.lang.Integer getIniRet() {
        return iniRet;
    }


    /**
     * Sets the iniRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param iniRet
     */
    public void setIniRet(java.lang.Integer iniRet) {
        this.iniRet = iniRet;
    }


    /**
     * Gets the nenRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return nenRet
     */
    public java.lang.String getNenRet() {
        return nenRet;
    }


    /**
     * Sets the nenRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param nenRet
     */
    public void setNenRet(java.lang.String nenRet) {
        this.nenRet = nenRet;
    }


    /**
     * Gets the pesRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return pesRet
     */
    public java.lang.String getPesRet() {
        return pesRet;
    }


    /**
     * Sets the pesRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param pesRet
     */
    public void setPesRet(java.lang.String pesRet) {
        this.pesRet = pesRet;
    }


    /**
     * Gets the sitReg value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return sitReg
     */
    public java.lang.String getSitReg() {
        return sitReg;
    }


    /**
     * Sets the sitReg value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param sitReg
     */
    public void setSitReg(java.lang.String sitReg) {
        this.sitReg = sitReg;
    }


    /**
     * Gets the tipRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @return tipRet
     */
    public java.lang.String getTipRet() {
        return tipRet;
    }


    /**
     * Sets the tipRet value for this EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.
     * 
     * @param tipRet
     */
    public void setTipRet(java.lang.String tipRet) {
        this.tipRet = tipRet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada)) return false;
        EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada other = (EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.baiRet==null && other.getBaiRet()==null) || 
             (this.baiRet!=null &&
              this.baiRet.equals(other.getBaiRet()))) &&
            ((this.cepRet==null && other.getCepRet()==null) || 
             (this.cepRet!=null &&
              this.cepRet.equals(other.getCepRet()))) &&
            ((this.cgcRet==null && other.getCgcRet()==null) || 
             (this.cgcRet!=null &&
              this.cgcRet.equals(other.getCgcRet()))) &&
            ((this.cidRet==null && other.getCidRet()==null) || 
             (this.cidRet!=null &&
              this.cidRet.equals(other.getCidRet()))) &&
            ((this.codRet==null && other.getCodRet()==null) || 
             (this.codRet!=null &&
              this.codRet.equals(other.getCodRet()))) &&
            ((this.cplRet==null && other.getCplRet()==null) || 
             (this.cplRet!=null &&
              this.cplRet.equals(other.getCplRet()))) &&
            ((this.emaRet==null && other.getEmaRet()==null) || 
             (this.emaRet!=null &&
              this.emaRet.equals(other.getEmaRet()))) &&
            ((this.endRet==null && other.getEndRet()==null) || 
             (this.endRet!=null &&
              this.endRet.equals(other.getEndRet()))) &&
            ((this.estRet==null && other.getEstRet()==null) || 
             (this.estRet!=null &&
              this.estRet.equals(other.getEstRet()))) &&
            ((this.faxRet==null && other.getFaxRet()==null) || 
             (this.faxRet!=null &&
              this.faxRet.equals(other.getFaxRet()))) &&
            ((this.fonRet==null && other.getFonRet()==null) || 
             (this.fonRet!=null &&
              this.fonRet.equals(other.getFonRet()))) &&
            ((this.iniRet==null && other.getIniRet()==null) || 
             (this.iniRet!=null &&
              this.iniRet.equals(other.getIniRet()))) &&
            ((this.nenRet==null && other.getNenRet()==null) || 
             (this.nenRet!=null &&
              this.nenRet.equals(other.getNenRet()))) &&
            ((this.pesRet==null && other.getPesRet()==null) || 
             (this.pesRet!=null &&
              this.pesRet.equals(other.getPesRet()))) &&
            ((this.sitReg==null && other.getSitReg()==null) || 
             (this.sitReg!=null &&
              this.sitReg.equals(other.getSitReg()))) &&
            ((this.tipRet==null && other.getTipRet()==null) || 
             (this.tipRet!=null &&
              this.tipRet.equals(other.getTipRet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBaiRet() != null) {
            _hashCode += getBaiRet().hashCode();
        }
        if (getCepRet() != null) {
            _hashCode += getCepRet().hashCode();
        }
        if (getCgcRet() != null) {
            _hashCode += getCgcRet().hashCode();
        }
        if (getCidRet() != null) {
            _hashCode += getCidRet().hashCode();
        }
        if (getCodRet() != null) {
            _hashCode += getCodRet().hashCode();
        }
        if (getCplRet() != null) {
            _hashCode += getCplRet().hashCode();
        }
        if (getEmaRet() != null) {
            _hashCode += getEmaRet().hashCode();
        }
        if (getEndRet() != null) {
            _hashCode += getEndRet().hashCode();
        }
        if (getEstRet() != null) {
            _hashCode += getEstRet().hashCode();
        }
        if (getFaxRet() != null) {
            _hashCode += getFaxRet().hashCode();
        }
        if (getFonRet() != null) {
            _hashCode += getFonRet().hashCode();
        }
        if (getIniRet() != null) {
            _hashCode += getIniRet().hashCode();
        }
        if (getNenRet() != null) {
            _hashCode += getNenRet().hashCode();
        }
        if (getPesRet() != null) {
            _hashCode += getPesRet().hashCode();
        }
        if (getSitReg() != null) {
            _hashCode += getSitReg().hashCode();
        }
        if (getTipRet() != null) {
            _hashCode += getTipRet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar3OutEmpresaFilialEnderecoRetirada.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar3OutEmpresaFilialEnderecoRetirada"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emaRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emaRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iniRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iniRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pesRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipRet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipRet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
