/**
 * EmpresafilialExportarOutEmpresaFilial.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportarOutEmpresaFilial  implements java.io.Serializable {
    private java.lang.String acrCca;

    private java.lang.String acrLcc;

    private java.lang.Integer acrMap;

    private java.lang.Integer acrQdd;

    private java.lang.Integer acrTcc;

    private java.lang.Integer ambNfe;

    private java.lang.String arrTrc;

    private java.lang.String baiCtd;

    private java.lang.Integer baiDev;

    private java.lang.String baiFil;

    private java.lang.Integer cepCob;

    private java.lang.Integer cepCtd;

    private java.lang.Integer cepEnt;

    private java.lang.Integer cepFil;

    private java.lang.String cgcCtd;

    private java.lang.String cidCob;

    private java.lang.String cidCtd;

    private java.lang.String cidEnt;

    private java.lang.String cidFil;

    private java.lang.String codBan;

    private java.lang.Integer codCli;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String codPai;

    private java.lang.Integer codRai;

    private java.lang.String codSuf;

    private java.lang.String codTpr;

    private java.lang.String cplCob;

    private java.lang.String cplCtd;

    private java.lang.String cplEnd;

    private java.lang.String cplEnt;

    private java.lang.String ctbCcr;

    private java.lang.Integer ctbCff;

    private java.lang.String ctbCrc;

    private java.lang.String ctbNcr;

    private java.lang.String ctbNsr;

    private br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado[] dadosEstado;

    private br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV[] dadosPDV;

    private java.lang.Integer depAec;

    private java.lang.String depRec;

    private java.lang.Integer diaDev;

    private java.lang.String efiFtr;

    private java.lang.String endCob;

    private java.lang.String endCtd;

    private java.lang.String endEnt;

    private java.lang.String endFil;

    private java.lang.String endNet;

    private br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada[] enderecoRetirada;

    private java.lang.String estCob;

    private java.lang.String estEnt;

    private java.lang.String faxCtd;

    private java.lang.Integer filCli;

    private java.lang.Integer filFor;

    private java.lang.String fonCtd;

    private java.lang.Integer forRcp;

    private java.lang.Integer impCfr;

    private java.lang.Integer impCtm;

    private java.lang.Integer impDav;

    private java.lang.String impPad;

    private java.lang.Integer incCul;

    private java.lang.Integer indAtc;

    private java.lang.Integer indBpf;

    private java.lang.Integer indBtt;

    private java.lang.Integer indClc;

    private java.lang.Integer indInt;

    private java.lang.Integer indPcc;

    private java.lang.Integer indPtm;

    private java.lang.String insEst;

    private java.lang.String insMun;

    private java.lang.String intNet;

    private java.lang.Double limApr;

    private java.lang.String logEmp;

    private java.lang.Integer maxIcf;

    private java.lang.Integer motBle;

    private java.lang.Integer motCre;

    private java.lang.Integer motDsb;

    private java.lang.Integer motRes;

    private java.lang.String nenFil;

    private java.lang.String netCtb;

    private java.lang.String nomEmp;

    private java.lang.String nomFil;

    private java.lang.String numCgc;

    private java.lang.Integer numCtd;

    private java.lang.String numFax;

    private java.lang.String numFon;

    private java.lang.Integer obmBes;

    private java.lang.Integer obmMvt;

    private java.lang.Integer obmRes;

    private br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur[] ordAplicaTabJur;

    private java.lang.Integer pagEng;

    private java.lang.Double perCdi;

    private java.lang.Double perDop;

    private java.lang.String prfTrb;

    private java.lang.Integer qtdDec;

    private java.lang.Integer reaIsv;

    private java.lang.Integer recDbc;

    private java.lang.Integer recDtj;

    private java.lang.Integer recDtm;

    private java.lang.Double recJmm;

    private java.lang.Integer recMbc;

    private java.lang.Double recMul;

    private java.lang.Double recPda;

    private java.lang.Integer recTda;

    private java.lang.String recTjr;

    private java.lang.Integer rvePdv;

    private java.lang.Integer seqInt;

    private java.lang.String serNce;

    private java.lang.String sigEmp;

    private java.lang.String sigFil;

    private java.lang.String sigUfs;

    private java.lang.String snfDev;

    private java.lang.String snfIva;

    private java.lang.String snfMan;

    private java.lang.String snfNfc;

    private java.lang.Integer tipEmp;

    private java.lang.String tmpVre;

    private java.lang.String tnsBcr;

    private java.lang.String tnsBrc;

    private java.lang.String tnsBrs;

    private java.lang.String tnsBtc;

    private java.lang.String tnsCcp;

    private java.lang.String tnsCdl;

    private java.lang.String tnsCdt;

    private java.lang.String tnsDcp;

    private java.lang.String tnsDdl;

    private java.lang.String tnsDdt;

    private java.lang.String tnsDev;

    private java.lang.String tnsDmc;

    private java.lang.String tnsDpc;

    private java.lang.String tnsDpn;

    private java.lang.String tnsDsc;

    private java.lang.String tnsDsi;

    private java.lang.String tnsIsv;

    private java.lang.String tnsMan;

    private java.lang.String tnsMns;

    private java.lang.String tnsNfc;

    private java.lang.String tnsNfs;

    private java.lang.String tnsPro;

    private java.lang.String tnsRco;

    private java.lang.String tnsRen;

    private java.lang.String tnsRes;

    private java.lang.String tnsRfu;

    private java.lang.String tnsRue;

    private java.lang.String tnsSer;

    private java.lang.String tnsSfe;

    private java.lang.String tnsSie;

    private java.lang.String tnsTcr;

    private java.lang.String tnsTcs;

    private java.lang.Integer tpcRcv;

    private java.lang.Integer tpmCpd;

    private java.lang.String tptScf;

    private java.lang.String tptSub;

    private java.lang.String ufsCtd;

    private java.lang.Integer utiTju;

    private java.lang.String valPad;

    private java.lang.String venCcc;

    private java.lang.Integer venCcr;

    private java.lang.Integer venCep;

    private java.lang.Integer venRec;

    public EmpresafilialExportarOutEmpresaFilial() {
    }

    public EmpresafilialExportarOutEmpresaFilial(
           java.lang.String acrCca,
           java.lang.String acrLcc,
           java.lang.Integer acrMap,
           java.lang.Integer acrQdd,
           java.lang.Integer acrTcc,
           java.lang.Integer ambNfe,
           java.lang.String arrTrc,
           java.lang.String baiCtd,
           java.lang.Integer baiDev,
           java.lang.String baiFil,
           java.lang.Integer cepCob,
           java.lang.Integer cepCtd,
           java.lang.Integer cepEnt,
           java.lang.Integer cepFil,
           java.lang.String cgcCtd,
           java.lang.String cidCob,
           java.lang.String cidCtd,
           java.lang.String cidEnt,
           java.lang.String cidFil,
           java.lang.String codBan,
           java.lang.Integer codCli,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String codPai,
           java.lang.Integer codRai,
           java.lang.String codSuf,
           java.lang.String codTpr,
           java.lang.String cplCob,
           java.lang.String cplCtd,
           java.lang.String cplEnd,
           java.lang.String cplEnt,
           java.lang.String ctbCcr,
           java.lang.Integer ctbCff,
           java.lang.String ctbCrc,
           java.lang.String ctbNcr,
           java.lang.String ctbNsr,
           br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado[] dadosEstado,
           br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV[] dadosPDV,
           java.lang.Integer depAec,
           java.lang.String depRec,
           java.lang.Integer diaDev,
           java.lang.String efiFtr,
           java.lang.String endCob,
           java.lang.String endCtd,
           java.lang.String endEnt,
           java.lang.String endFil,
           java.lang.String endNet,
           br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada[] enderecoRetirada,
           java.lang.String estCob,
           java.lang.String estEnt,
           java.lang.String faxCtd,
           java.lang.Integer filCli,
           java.lang.Integer filFor,
           java.lang.String fonCtd,
           java.lang.Integer forRcp,
           java.lang.Integer impCfr,
           java.lang.Integer impCtm,
           java.lang.Integer impDav,
           java.lang.String impPad,
           java.lang.Integer incCul,
           java.lang.Integer indAtc,
           java.lang.Integer indBpf,
           java.lang.Integer indBtt,
           java.lang.Integer indClc,
           java.lang.Integer indInt,
           java.lang.Integer indPcc,
           java.lang.Integer indPtm,
           java.lang.String insEst,
           java.lang.String insMun,
           java.lang.String intNet,
           java.lang.Double limApr,
           java.lang.String logEmp,
           java.lang.Integer maxIcf,
           java.lang.Integer motBle,
           java.lang.Integer motCre,
           java.lang.Integer motDsb,
           java.lang.Integer motRes,
           java.lang.String nenFil,
           java.lang.String netCtb,
           java.lang.String nomEmp,
           java.lang.String nomFil,
           java.lang.String numCgc,
           java.lang.Integer numCtd,
           java.lang.String numFax,
           java.lang.String numFon,
           java.lang.Integer obmBes,
           java.lang.Integer obmMvt,
           java.lang.Integer obmRes,
           br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur[] ordAplicaTabJur,
           java.lang.Integer pagEng,
           java.lang.Double perCdi,
           java.lang.Double perDop,
           java.lang.String prfTrb,
           java.lang.Integer qtdDec,
           java.lang.Integer reaIsv,
           java.lang.Integer recDbc,
           java.lang.Integer recDtj,
           java.lang.Integer recDtm,
           java.lang.Double recJmm,
           java.lang.Integer recMbc,
           java.lang.Double recMul,
           java.lang.Double recPda,
           java.lang.Integer recTda,
           java.lang.String recTjr,
           java.lang.Integer rvePdv,
           java.lang.Integer seqInt,
           java.lang.String serNce,
           java.lang.String sigEmp,
           java.lang.String sigFil,
           java.lang.String sigUfs,
           java.lang.String snfDev,
           java.lang.String snfIva,
           java.lang.String snfMan,
           java.lang.String snfNfc,
           java.lang.Integer tipEmp,
           java.lang.String tmpVre,
           java.lang.String tnsBcr,
           java.lang.String tnsBrc,
           java.lang.String tnsBrs,
           java.lang.String tnsBtc,
           java.lang.String tnsCcp,
           java.lang.String tnsCdl,
           java.lang.String tnsCdt,
           java.lang.String tnsDcp,
           java.lang.String tnsDdl,
           java.lang.String tnsDdt,
           java.lang.String tnsDev,
           java.lang.String tnsDmc,
           java.lang.String tnsDpc,
           java.lang.String tnsDpn,
           java.lang.String tnsDsc,
           java.lang.String tnsDsi,
           java.lang.String tnsIsv,
           java.lang.String tnsMan,
           java.lang.String tnsMns,
           java.lang.String tnsNfc,
           java.lang.String tnsNfs,
           java.lang.String tnsPro,
           java.lang.String tnsRco,
           java.lang.String tnsRen,
           java.lang.String tnsRes,
           java.lang.String tnsRfu,
           java.lang.String tnsRue,
           java.lang.String tnsSer,
           java.lang.String tnsSfe,
           java.lang.String tnsSie,
           java.lang.String tnsTcr,
           java.lang.String tnsTcs,
           java.lang.Integer tpcRcv,
           java.lang.Integer tpmCpd,
           java.lang.String tptScf,
           java.lang.String tptSub,
           java.lang.String ufsCtd,
           java.lang.Integer utiTju,
           java.lang.String valPad,
           java.lang.String venCcc,
           java.lang.Integer venCcr,
           java.lang.Integer venCep,
           java.lang.Integer venRec) {
           this.acrCca = acrCca;
           this.acrLcc = acrLcc;
           this.acrMap = acrMap;
           this.acrQdd = acrQdd;
           this.acrTcc = acrTcc;
           this.ambNfe = ambNfe;
           this.arrTrc = arrTrc;
           this.baiCtd = baiCtd;
           this.baiDev = baiDev;
           this.baiFil = baiFil;
           this.cepCob = cepCob;
           this.cepCtd = cepCtd;
           this.cepEnt = cepEnt;
           this.cepFil = cepFil;
           this.cgcCtd = cgcCtd;
           this.cidCob = cidCob;
           this.cidCtd = cidCtd;
           this.cidEnt = cidEnt;
           this.cidFil = cidFil;
           this.codBan = codBan;
           this.codCli = codCli;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codPai = codPai;
           this.codRai = codRai;
           this.codSuf = codSuf;
           this.codTpr = codTpr;
           this.cplCob = cplCob;
           this.cplCtd = cplCtd;
           this.cplEnd = cplEnd;
           this.cplEnt = cplEnt;
           this.ctbCcr = ctbCcr;
           this.ctbCff = ctbCff;
           this.ctbCrc = ctbCrc;
           this.ctbNcr = ctbNcr;
           this.ctbNsr = ctbNsr;
           this.dadosEstado = dadosEstado;
           this.dadosPDV = dadosPDV;
           this.depAec = depAec;
           this.depRec = depRec;
           this.diaDev = diaDev;
           this.efiFtr = efiFtr;
           this.endCob = endCob;
           this.endCtd = endCtd;
           this.endEnt = endEnt;
           this.endFil = endFil;
           this.endNet = endNet;
           this.enderecoRetirada = enderecoRetirada;
           this.estCob = estCob;
           this.estEnt = estEnt;
           this.faxCtd = faxCtd;
           this.filCli = filCli;
           this.filFor = filFor;
           this.fonCtd = fonCtd;
           this.forRcp = forRcp;
           this.impCfr = impCfr;
           this.impCtm = impCtm;
           this.impDav = impDav;
           this.impPad = impPad;
           this.incCul = incCul;
           this.indAtc = indAtc;
           this.indBpf = indBpf;
           this.indBtt = indBtt;
           this.indClc = indClc;
           this.indInt = indInt;
           this.indPcc = indPcc;
           this.indPtm = indPtm;
           this.insEst = insEst;
           this.insMun = insMun;
           this.intNet = intNet;
           this.limApr = limApr;
           this.logEmp = logEmp;
           this.maxIcf = maxIcf;
           this.motBle = motBle;
           this.motCre = motCre;
           this.motDsb = motDsb;
           this.motRes = motRes;
           this.nenFil = nenFil;
           this.netCtb = netCtb;
           this.nomEmp = nomEmp;
           this.nomFil = nomFil;
           this.numCgc = numCgc;
           this.numCtd = numCtd;
           this.numFax = numFax;
           this.numFon = numFon;
           this.obmBes = obmBes;
           this.obmMvt = obmMvt;
           this.obmRes = obmRes;
           this.ordAplicaTabJur = ordAplicaTabJur;
           this.pagEng = pagEng;
           this.perCdi = perCdi;
           this.perDop = perDop;
           this.prfTrb = prfTrb;
           this.qtdDec = qtdDec;
           this.reaIsv = reaIsv;
           this.recDbc = recDbc;
           this.recDtj = recDtj;
           this.recDtm = recDtm;
           this.recJmm = recJmm;
           this.recMbc = recMbc;
           this.recMul = recMul;
           this.recPda = recPda;
           this.recTda = recTda;
           this.recTjr = recTjr;
           this.rvePdv = rvePdv;
           this.seqInt = seqInt;
           this.serNce = serNce;
           this.sigEmp = sigEmp;
           this.sigFil = sigFil;
           this.sigUfs = sigUfs;
           this.snfDev = snfDev;
           this.snfIva = snfIva;
           this.snfMan = snfMan;
           this.snfNfc = snfNfc;
           this.tipEmp = tipEmp;
           this.tmpVre = tmpVre;
           this.tnsBcr = tnsBcr;
           this.tnsBrc = tnsBrc;
           this.tnsBrs = tnsBrs;
           this.tnsBtc = tnsBtc;
           this.tnsCcp = tnsCcp;
           this.tnsCdl = tnsCdl;
           this.tnsCdt = tnsCdt;
           this.tnsDcp = tnsDcp;
           this.tnsDdl = tnsDdl;
           this.tnsDdt = tnsDdt;
           this.tnsDev = tnsDev;
           this.tnsDmc = tnsDmc;
           this.tnsDpc = tnsDpc;
           this.tnsDpn = tnsDpn;
           this.tnsDsc = tnsDsc;
           this.tnsDsi = tnsDsi;
           this.tnsIsv = tnsIsv;
           this.tnsMan = tnsMan;
           this.tnsMns = tnsMns;
           this.tnsNfc = tnsNfc;
           this.tnsNfs = tnsNfs;
           this.tnsPro = tnsPro;
           this.tnsRco = tnsRco;
           this.tnsRen = tnsRen;
           this.tnsRes = tnsRes;
           this.tnsRfu = tnsRfu;
           this.tnsRue = tnsRue;
           this.tnsSer = tnsSer;
           this.tnsSfe = tnsSfe;
           this.tnsSie = tnsSie;
           this.tnsTcr = tnsTcr;
           this.tnsTcs = tnsTcs;
           this.tpcRcv = tpcRcv;
           this.tpmCpd = tpmCpd;
           this.tptScf = tptScf;
           this.tptSub = tptSub;
           this.ufsCtd = ufsCtd;
           this.utiTju = utiTju;
           this.valPad = valPad;
           this.venCcc = venCcc;
           this.venCcr = venCcr;
           this.venCep = venCep;
           this.venRec = venRec;
    }


    /**
     * Gets the acrCca value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return acrCca
     */
    public java.lang.String getAcrCca() {
        return acrCca;
    }


    /**
     * Sets the acrCca value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param acrCca
     */
    public void setAcrCca(java.lang.String acrCca) {
        this.acrCca = acrCca;
    }


    /**
     * Gets the acrLcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return acrLcc
     */
    public java.lang.String getAcrLcc() {
        return acrLcc;
    }


    /**
     * Sets the acrLcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param acrLcc
     */
    public void setAcrLcc(java.lang.String acrLcc) {
        this.acrLcc = acrLcc;
    }


    /**
     * Gets the acrMap value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return acrMap
     */
    public java.lang.Integer getAcrMap() {
        return acrMap;
    }


    /**
     * Sets the acrMap value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param acrMap
     */
    public void setAcrMap(java.lang.Integer acrMap) {
        this.acrMap = acrMap;
    }


    /**
     * Gets the acrQdd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return acrQdd
     */
    public java.lang.Integer getAcrQdd() {
        return acrQdd;
    }


    /**
     * Sets the acrQdd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param acrQdd
     */
    public void setAcrQdd(java.lang.Integer acrQdd) {
        this.acrQdd = acrQdd;
    }


    /**
     * Gets the acrTcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return acrTcc
     */
    public java.lang.Integer getAcrTcc() {
        return acrTcc;
    }


    /**
     * Sets the acrTcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param acrTcc
     */
    public void setAcrTcc(java.lang.Integer acrTcc) {
        this.acrTcc = acrTcc;
    }


    /**
     * Gets the ambNfe value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ambNfe
     */
    public java.lang.Integer getAmbNfe() {
        return ambNfe;
    }


    /**
     * Sets the ambNfe value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ambNfe
     */
    public void setAmbNfe(java.lang.Integer ambNfe) {
        this.ambNfe = ambNfe;
    }


    /**
     * Gets the arrTrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return arrTrc
     */
    public java.lang.String getArrTrc() {
        return arrTrc;
    }


    /**
     * Sets the arrTrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param arrTrc
     */
    public void setArrTrc(java.lang.String arrTrc) {
        this.arrTrc = arrTrc;
    }


    /**
     * Gets the baiCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return baiCtd
     */
    public java.lang.String getBaiCtd() {
        return baiCtd;
    }


    /**
     * Sets the baiCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param baiCtd
     */
    public void setBaiCtd(java.lang.String baiCtd) {
        this.baiCtd = baiCtd;
    }


    /**
     * Gets the baiDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return baiDev
     */
    public java.lang.Integer getBaiDev() {
        return baiDev;
    }


    /**
     * Sets the baiDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param baiDev
     */
    public void setBaiDev(java.lang.Integer baiDev) {
        this.baiDev = baiDev;
    }


    /**
     * Gets the baiFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return baiFil
     */
    public java.lang.String getBaiFil() {
        return baiFil;
    }


    /**
     * Sets the baiFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param baiFil
     */
    public void setBaiFil(java.lang.String baiFil) {
        this.baiFil = baiFil;
    }


    /**
     * Gets the cepCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cepCob
     */
    public java.lang.Integer getCepCob() {
        return cepCob;
    }


    /**
     * Sets the cepCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cepCob
     */
    public void setCepCob(java.lang.Integer cepCob) {
        this.cepCob = cepCob;
    }


    /**
     * Gets the cepCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cepCtd
     */
    public java.lang.Integer getCepCtd() {
        return cepCtd;
    }


    /**
     * Sets the cepCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cepCtd
     */
    public void setCepCtd(java.lang.Integer cepCtd) {
        this.cepCtd = cepCtd;
    }


    /**
     * Gets the cepEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cepEnt
     */
    public java.lang.Integer getCepEnt() {
        return cepEnt;
    }


    /**
     * Sets the cepEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cepEnt
     */
    public void setCepEnt(java.lang.Integer cepEnt) {
        this.cepEnt = cepEnt;
    }


    /**
     * Gets the cepFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cepFil
     */
    public java.lang.Integer getCepFil() {
        return cepFil;
    }


    /**
     * Sets the cepFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cepFil
     */
    public void setCepFil(java.lang.Integer cepFil) {
        this.cepFil = cepFil;
    }


    /**
     * Gets the cgcCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cgcCtd
     */
    public java.lang.String getCgcCtd() {
        return cgcCtd;
    }


    /**
     * Sets the cgcCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cgcCtd
     */
    public void setCgcCtd(java.lang.String cgcCtd) {
        this.cgcCtd = cgcCtd;
    }


    /**
     * Gets the cidCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cidCob
     */
    public java.lang.String getCidCob() {
        return cidCob;
    }


    /**
     * Sets the cidCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cidCob
     */
    public void setCidCob(java.lang.String cidCob) {
        this.cidCob = cidCob;
    }


    /**
     * Gets the cidCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cidCtd
     */
    public java.lang.String getCidCtd() {
        return cidCtd;
    }


    /**
     * Sets the cidCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cidCtd
     */
    public void setCidCtd(java.lang.String cidCtd) {
        this.cidCtd = cidCtd;
    }


    /**
     * Gets the cidEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cidEnt
     */
    public java.lang.String getCidEnt() {
        return cidEnt;
    }


    /**
     * Sets the cidEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cidEnt
     */
    public void setCidEnt(java.lang.String cidEnt) {
        this.cidEnt = cidEnt;
    }


    /**
     * Gets the cidFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cidFil
     */
    public java.lang.String getCidFil() {
        return cidFil;
    }


    /**
     * Sets the cidFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cidFil
     */
    public void setCidFil(java.lang.String cidFil) {
        this.cidFil = cidFil;
    }


    /**
     * Gets the codBan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codBan
     */
    public java.lang.String getCodBan() {
        return codBan;
    }


    /**
     * Sets the codBan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codBan
     */
    public void setCodBan(java.lang.String codBan) {
        this.codBan = codBan;
    }


    /**
     * Gets the codCli value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codCli
     */
    public java.lang.Integer getCodCli() {
        return codCli;
    }


    /**
     * Sets the codCli value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codCli
     */
    public void setCodCli(java.lang.Integer codCli) {
        this.codCli = codCli;
    }


    /**
     * Gets the codEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codPai value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codPai
     */
    public java.lang.String getCodPai() {
        return codPai;
    }


    /**
     * Sets the codPai value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codPai
     */
    public void setCodPai(java.lang.String codPai) {
        this.codPai = codPai;
    }


    /**
     * Gets the codRai value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codRai
     */
    public java.lang.Integer getCodRai() {
        return codRai;
    }


    /**
     * Sets the codRai value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codRai
     */
    public void setCodRai(java.lang.Integer codRai) {
        this.codRai = codRai;
    }


    /**
     * Gets the codSuf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codSuf
     */
    public java.lang.String getCodSuf() {
        return codSuf;
    }


    /**
     * Sets the codSuf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codSuf
     */
    public void setCodSuf(java.lang.String codSuf) {
        this.codSuf = codSuf;
    }


    /**
     * Gets the codTpr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return codTpr
     */
    public java.lang.String getCodTpr() {
        return codTpr;
    }


    /**
     * Sets the codTpr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param codTpr
     */
    public void setCodTpr(java.lang.String codTpr) {
        this.codTpr = codTpr;
    }


    /**
     * Gets the cplCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cplCob
     */
    public java.lang.String getCplCob() {
        return cplCob;
    }


    /**
     * Sets the cplCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cplCob
     */
    public void setCplCob(java.lang.String cplCob) {
        this.cplCob = cplCob;
    }


    /**
     * Gets the cplCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cplCtd
     */
    public java.lang.String getCplCtd() {
        return cplCtd;
    }


    /**
     * Sets the cplCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cplCtd
     */
    public void setCplCtd(java.lang.String cplCtd) {
        this.cplCtd = cplCtd;
    }


    /**
     * Gets the cplEnd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cplEnd
     */
    public java.lang.String getCplEnd() {
        return cplEnd;
    }


    /**
     * Sets the cplEnd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cplEnd
     */
    public void setCplEnd(java.lang.String cplEnd) {
        this.cplEnd = cplEnd;
    }


    /**
     * Gets the cplEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return cplEnt
     */
    public java.lang.String getCplEnt() {
        return cplEnt;
    }


    /**
     * Sets the cplEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param cplEnt
     */
    public void setCplEnt(java.lang.String cplEnt) {
        this.cplEnt = cplEnt;
    }


    /**
     * Gets the ctbCcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ctbCcr
     */
    public java.lang.String getCtbCcr() {
        return ctbCcr;
    }


    /**
     * Sets the ctbCcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ctbCcr
     */
    public void setCtbCcr(java.lang.String ctbCcr) {
        this.ctbCcr = ctbCcr;
    }


    /**
     * Gets the ctbCff value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ctbCff
     */
    public java.lang.Integer getCtbCff() {
        return ctbCff;
    }


    /**
     * Sets the ctbCff value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ctbCff
     */
    public void setCtbCff(java.lang.Integer ctbCff) {
        this.ctbCff = ctbCff;
    }


    /**
     * Gets the ctbCrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ctbCrc
     */
    public java.lang.String getCtbCrc() {
        return ctbCrc;
    }


    /**
     * Sets the ctbCrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ctbCrc
     */
    public void setCtbCrc(java.lang.String ctbCrc) {
        this.ctbCrc = ctbCrc;
    }


    /**
     * Gets the ctbNcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ctbNcr
     */
    public java.lang.String getCtbNcr() {
        return ctbNcr;
    }


    /**
     * Sets the ctbNcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ctbNcr
     */
    public void setCtbNcr(java.lang.String ctbNcr) {
        this.ctbNcr = ctbNcr;
    }


    /**
     * Gets the ctbNsr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ctbNsr
     */
    public java.lang.String getCtbNsr() {
        return ctbNsr;
    }


    /**
     * Sets the ctbNsr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ctbNsr
     */
    public void setCtbNsr(java.lang.String ctbNsr) {
        this.ctbNsr = ctbNsr;
    }


    /**
     * Gets the dadosEstado value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return dadosEstado
     */
    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado[] getDadosEstado() {
        return dadosEstado;
    }


    /**
     * Sets the dadosEstado value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param dadosEstado
     */
    public void setDadosEstado(br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado[] dadosEstado) {
        this.dadosEstado = dadosEstado;
    }

    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado getDadosEstado(int i) {
        return this.dadosEstado[i];
    }

    public void setDadosEstado(int i, br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosEstado _value) {
        this.dadosEstado[i] = _value;
    }


    /**
     * Gets the dadosPDV value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return dadosPDV
     */
    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV[] getDadosPDV() {
        return dadosPDV;
    }


    /**
     * Sets the dadosPDV value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param dadosPDV
     */
    public void setDadosPDV(br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV[] dadosPDV) {
        this.dadosPDV = dadosPDV;
    }

    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV getDadosPDV(int i) {
        return this.dadosPDV[i];
    }

    public void setDadosPDV(int i, br.com.senior.services.EmpresafilialExportarOutEmpresaFilialDadosPDV _value) {
        this.dadosPDV[i] = _value;
    }


    /**
     * Gets the depAec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return depAec
     */
    public java.lang.Integer getDepAec() {
        return depAec;
    }


    /**
     * Sets the depAec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param depAec
     */
    public void setDepAec(java.lang.Integer depAec) {
        this.depAec = depAec;
    }


    /**
     * Gets the depRec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return depRec
     */
    public java.lang.String getDepRec() {
        return depRec;
    }


    /**
     * Sets the depRec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param depRec
     */
    public void setDepRec(java.lang.String depRec) {
        this.depRec = depRec;
    }


    /**
     * Gets the diaDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return diaDev
     */
    public java.lang.Integer getDiaDev() {
        return diaDev;
    }


    /**
     * Sets the diaDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param diaDev
     */
    public void setDiaDev(java.lang.Integer diaDev) {
        this.diaDev = diaDev;
    }


    /**
     * Gets the efiFtr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return efiFtr
     */
    public java.lang.String getEfiFtr() {
        return efiFtr;
    }


    /**
     * Sets the efiFtr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param efiFtr
     */
    public void setEfiFtr(java.lang.String efiFtr) {
        this.efiFtr = efiFtr;
    }


    /**
     * Gets the endCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return endCob
     */
    public java.lang.String getEndCob() {
        return endCob;
    }


    /**
     * Sets the endCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param endCob
     */
    public void setEndCob(java.lang.String endCob) {
        this.endCob = endCob;
    }


    /**
     * Gets the endCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return endCtd
     */
    public java.lang.String getEndCtd() {
        return endCtd;
    }


    /**
     * Sets the endCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param endCtd
     */
    public void setEndCtd(java.lang.String endCtd) {
        this.endCtd = endCtd;
    }


    /**
     * Gets the endEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return endEnt
     */
    public java.lang.String getEndEnt() {
        return endEnt;
    }


    /**
     * Sets the endEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param endEnt
     */
    public void setEndEnt(java.lang.String endEnt) {
        this.endEnt = endEnt;
    }


    /**
     * Gets the endFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return endFil
     */
    public java.lang.String getEndFil() {
        return endFil;
    }


    /**
     * Sets the endFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param endFil
     */
    public void setEndFil(java.lang.String endFil) {
        this.endFil = endFil;
    }


    /**
     * Gets the endNet value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return endNet
     */
    public java.lang.String getEndNet() {
        return endNet;
    }


    /**
     * Sets the endNet value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param endNet
     */
    public void setEndNet(java.lang.String endNet) {
        this.endNet = endNet;
    }


    /**
     * Gets the enderecoRetirada value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return enderecoRetirada
     */
    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada[] getEnderecoRetirada() {
        return enderecoRetirada;
    }


    /**
     * Sets the enderecoRetirada value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param enderecoRetirada
     */
    public void setEnderecoRetirada(br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada[] enderecoRetirada) {
        this.enderecoRetirada = enderecoRetirada;
    }

    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada getEnderecoRetirada(int i) {
        return this.enderecoRetirada[i];
    }

    public void setEnderecoRetirada(int i, br.com.senior.services.EmpresafilialExportarOutEmpresaFilialEnderecoRetirada _value) {
        this.enderecoRetirada[i] = _value;
    }


    /**
     * Gets the estCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return estCob
     */
    public java.lang.String getEstCob() {
        return estCob;
    }


    /**
     * Sets the estCob value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param estCob
     */
    public void setEstCob(java.lang.String estCob) {
        this.estCob = estCob;
    }


    /**
     * Gets the estEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return estEnt
     */
    public java.lang.String getEstEnt() {
        return estEnt;
    }


    /**
     * Sets the estEnt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param estEnt
     */
    public void setEstEnt(java.lang.String estEnt) {
        this.estEnt = estEnt;
    }


    /**
     * Gets the faxCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return faxCtd
     */
    public java.lang.String getFaxCtd() {
        return faxCtd;
    }


    /**
     * Sets the faxCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param faxCtd
     */
    public void setFaxCtd(java.lang.String faxCtd) {
        this.faxCtd = faxCtd;
    }


    /**
     * Gets the filCli value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return filCli
     */
    public java.lang.Integer getFilCli() {
        return filCli;
    }


    /**
     * Sets the filCli value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param filCli
     */
    public void setFilCli(java.lang.Integer filCli) {
        this.filCli = filCli;
    }


    /**
     * Gets the filFor value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return filFor
     */
    public java.lang.Integer getFilFor() {
        return filFor;
    }


    /**
     * Sets the filFor value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param filFor
     */
    public void setFilFor(java.lang.Integer filFor) {
        this.filFor = filFor;
    }


    /**
     * Gets the fonCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return fonCtd
     */
    public java.lang.String getFonCtd() {
        return fonCtd;
    }


    /**
     * Sets the fonCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param fonCtd
     */
    public void setFonCtd(java.lang.String fonCtd) {
        this.fonCtd = fonCtd;
    }


    /**
     * Gets the forRcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return forRcp
     */
    public java.lang.Integer getForRcp() {
        return forRcp;
    }


    /**
     * Sets the forRcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param forRcp
     */
    public void setForRcp(java.lang.Integer forRcp) {
        this.forRcp = forRcp;
    }


    /**
     * Gets the impCfr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return impCfr
     */
    public java.lang.Integer getImpCfr() {
        return impCfr;
    }


    /**
     * Sets the impCfr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param impCfr
     */
    public void setImpCfr(java.lang.Integer impCfr) {
        this.impCfr = impCfr;
    }


    /**
     * Gets the impCtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return impCtm
     */
    public java.lang.Integer getImpCtm() {
        return impCtm;
    }


    /**
     * Sets the impCtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param impCtm
     */
    public void setImpCtm(java.lang.Integer impCtm) {
        this.impCtm = impCtm;
    }


    /**
     * Gets the impDav value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return impDav
     */
    public java.lang.Integer getImpDav() {
        return impDav;
    }


    /**
     * Sets the impDav value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param impDav
     */
    public void setImpDav(java.lang.Integer impDav) {
        this.impDav = impDav;
    }


    /**
     * Gets the impPad value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return impPad
     */
    public java.lang.String getImpPad() {
        return impPad;
    }


    /**
     * Sets the impPad value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param impPad
     */
    public void setImpPad(java.lang.String impPad) {
        this.impPad = impPad;
    }


    /**
     * Gets the incCul value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return incCul
     */
    public java.lang.Integer getIncCul() {
        return incCul;
    }


    /**
     * Sets the incCul value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param incCul
     */
    public void setIncCul(java.lang.Integer incCul) {
        this.incCul = incCul;
    }


    /**
     * Gets the indAtc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indAtc
     */
    public java.lang.Integer getIndAtc() {
        return indAtc;
    }


    /**
     * Sets the indAtc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indAtc
     */
    public void setIndAtc(java.lang.Integer indAtc) {
        this.indAtc = indAtc;
    }


    /**
     * Gets the indBpf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indBpf
     */
    public java.lang.Integer getIndBpf() {
        return indBpf;
    }


    /**
     * Sets the indBpf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indBpf
     */
    public void setIndBpf(java.lang.Integer indBpf) {
        this.indBpf = indBpf;
    }


    /**
     * Gets the indBtt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indBtt
     */
    public java.lang.Integer getIndBtt() {
        return indBtt;
    }


    /**
     * Sets the indBtt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indBtt
     */
    public void setIndBtt(java.lang.Integer indBtt) {
        this.indBtt = indBtt;
    }


    /**
     * Gets the indClc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indClc
     */
    public java.lang.Integer getIndClc() {
        return indClc;
    }


    /**
     * Sets the indClc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indClc
     */
    public void setIndClc(java.lang.Integer indClc) {
        this.indClc = indClc;
    }


    /**
     * Gets the indInt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indInt
     */
    public java.lang.Integer getIndInt() {
        return indInt;
    }


    /**
     * Sets the indInt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indInt
     */
    public void setIndInt(java.lang.Integer indInt) {
        this.indInt = indInt;
    }


    /**
     * Gets the indPcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indPcc
     */
    public java.lang.Integer getIndPcc() {
        return indPcc;
    }


    /**
     * Sets the indPcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indPcc
     */
    public void setIndPcc(java.lang.Integer indPcc) {
        this.indPcc = indPcc;
    }


    /**
     * Gets the indPtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return indPtm
     */
    public java.lang.Integer getIndPtm() {
        return indPtm;
    }


    /**
     * Sets the indPtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param indPtm
     */
    public void setIndPtm(java.lang.Integer indPtm) {
        this.indPtm = indPtm;
    }


    /**
     * Gets the insEst value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return insEst
     */
    public java.lang.String getInsEst() {
        return insEst;
    }


    /**
     * Sets the insEst value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param insEst
     */
    public void setInsEst(java.lang.String insEst) {
        this.insEst = insEst;
    }


    /**
     * Gets the insMun value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return insMun
     */
    public java.lang.String getInsMun() {
        return insMun;
    }


    /**
     * Sets the insMun value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param insMun
     */
    public void setInsMun(java.lang.String insMun) {
        this.insMun = insMun;
    }


    /**
     * Gets the intNet value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return intNet
     */
    public java.lang.String getIntNet() {
        return intNet;
    }


    /**
     * Sets the intNet value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param intNet
     */
    public void setIntNet(java.lang.String intNet) {
        this.intNet = intNet;
    }


    /**
     * Gets the limApr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return limApr
     */
    public java.lang.Double getLimApr() {
        return limApr;
    }


    /**
     * Sets the limApr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param limApr
     */
    public void setLimApr(java.lang.Double limApr) {
        this.limApr = limApr;
    }


    /**
     * Gets the logEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return logEmp
     */
    public java.lang.String getLogEmp() {
        return logEmp;
    }


    /**
     * Sets the logEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param logEmp
     */
    public void setLogEmp(java.lang.String logEmp) {
        this.logEmp = logEmp;
    }


    /**
     * Gets the maxIcf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return maxIcf
     */
    public java.lang.Integer getMaxIcf() {
        return maxIcf;
    }


    /**
     * Sets the maxIcf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param maxIcf
     */
    public void setMaxIcf(java.lang.Integer maxIcf) {
        this.maxIcf = maxIcf;
    }


    /**
     * Gets the motBle value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return motBle
     */
    public java.lang.Integer getMotBle() {
        return motBle;
    }


    /**
     * Sets the motBle value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param motBle
     */
    public void setMotBle(java.lang.Integer motBle) {
        this.motBle = motBle;
    }


    /**
     * Gets the motCre value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return motCre
     */
    public java.lang.Integer getMotCre() {
        return motCre;
    }


    /**
     * Sets the motCre value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param motCre
     */
    public void setMotCre(java.lang.Integer motCre) {
        this.motCre = motCre;
    }


    /**
     * Gets the motDsb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return motDsb
     */
    public java.lang.Integer getMotDsb() {
        return motDsb;
    }


    /**
     * Sets the motDsb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param motDsb
     */
    public void setMotDsb(java.lang.Integer motDsb) {
        this.motDsb = motDsb;
    }


    /**
     * Gets the motRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return motRes
     */
    public java.lang.Integer getMotRes() {
        return motRes;
    }


    /**
     * Sets the motRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param motRes
     */
    public void setMotRes(java.lang.Integer motRes) {
        this.motRes = motRes;
    }


    /**
     * Gets the nenFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return nenFil
     */
    public java.lang.String getNenFil() {
        return nenFil;
    }


    /**
     * Sets the nenFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param nenFil
     */
    public void setNenFil(java.lang.String nenFil) {
        this.nenFil = nenFil;
    }


    /**
     * Gets the netCtb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return netCtb
     */
    public java.lang.String getNetCtb() {
        return netCtb;
    }


    /**
     * Sets the netCtb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param netCtb
     */
    public void setNetCtb(java.lang.String netCtb) {
        this.netCtb = netCtb;
    }


    /**
     * Gets the nomEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return nomEmp
     */
    public java.lang.String getNomEmp() {
        return nomEmp;
    }


    /**
     * Sets the nomEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param nomEmp
     */
    public void setNomEmp(java.lang.String nomEmp) {
        this.nomEmp = nomEmp;
    }


    /**
     * Gets the nomFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return nomFil
     */
    public java.lang.String getNomFil() {
        return nomFil;
    }


    /**
     * Sets the nomFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param nomFil
     */
    public void setNomFil(java.lang.String nomFil) {
        this.nomFil = nomFil;
    }


    /**
     * Gets the numCgc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return numCgc
     */
    public java.lang.String getNumCgc() {
        return numCgc;
    }


    /**
     * Sets the numCgc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param numCgc
     */
    public void setNumCgc(java.lang.String numCgc) {
        this.numCgc = numCgc;
    }


    /**
     * Gets the numCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return numCtd
     */
    public java.lang.Integer getNumCtd() {
        return numCtd;
    }


    /**
     * Sets the numCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param numCtd
     */
    public void setNumCtd(java.lang.Integer numCtd) {
        this.numCtd = numCtd;
    }


    /**
     * Gets the numFax value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return numFax
     */
    public java.lang.String getNumFax() {
        return numFax;
    }


    /**
     * Sets the numFax value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param numFax
     */
    public void setNumFax(java.lang.String numFax) {
        this.numFax = numFax;
    }


    /**
     * Gets the numFon value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return numFon
     */
    public java.lang.String getNumFon() {
        return numFon;
    }


    /**
     * Sets the numFon value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param numFon
     */
    public void setNumFon(java.lang.String numFon) {
        this.numFon = numFon;
    }


    /**
     * Gets the obmBes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return obmBes
     */
    public java.lang.Integer getObmBes() {
        return obmBes;
    }


    /**
     * Sets the obmBes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param obmBes
     */
    public void setObmBes(java.lang.Integer obmBes) {
        this.obmBes = obmBes;
    }


    /**
     * Gets the obmMvt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return obmMvt
     */
    public java.lang.Integer getObmMvt() {
        return obmMvt;
    }


    /**
     * Sets the obmMvt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param obmMvt
     */
    public void setObmMvt(java.lang.Integer obmMvt) {
        this.obmMvt = obmMvt;
    }


    /**
     * Gets the obmRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return obmRes
     */
    public java.lang.Integer getObmRes() {
        return obmRes;
    }


    /**
     * Sets the obmRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param obmRes
     */
    public void setObmRes(java.lang.Integer obmRes) {
        this.obmRes = obmRes;
    }


    /**
     * Gets the ordAplicaTabJur value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ordAplicaTabJur
     */
    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur[] getOrdAplicaTabJur() {
        return ordAplicaTabJur;
    }


    /**
     * Sets the ordAplicaTabJur value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ordAplicaTabJur
     */
    public void setOrdAplicaTabJur(br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur[] ordAplicaTabJur) {
        this.ordAplicaTabJur = ordAplicaTabJur;
    }

    public br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur getOrdAplicaTabJur(int i) {
        return this.ordAplicaTabJur[i];
    }

    public void setOrdAplicaTabJur(int i, br.com.senior.services.EmpresafilialExportarOutEmpresaFilialOrdAplicaTabJur _value) {
        this.ordAplicaTabJur[i] = _value;
    }


    /**
     * Gets the pagEng value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return pagEng
     */
    public java.lang.Integer getPagEng() {
        return pagEng;
    }


    /**
     * Sets the pagEng value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param pagEng
     */
    public void setPagEng(java.lang.Integer pagEng) {
        this.pagEng = pagEng;
    }


    /**
     * Gets the perCdi value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return perCdi
     */
    public java.lang.Double getPerCdi() {
        return perCdi;
    }


    /**
     * Sets the perCdi value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param perCdi
     */
    public void setPerCdi(java.lang.Double perCdi) {
        this.perCdi = perCdi;
    }


    /**
     * Gets the perDop value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return perDop
     */
    public java.lang.Double getPerDop() {
        return perDop;
    }


    /**
     * Sets the perDop value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param perDop
     */
    public void setPerDop(java.lang.Double perDop) {
        this.perDop = perDop;
    }


    /**
     * Gets the prfTrb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return prfTrb
     */
    public java.lang.String getPrfTrb() {
        return prfTrb;
    }


    /**
     * Sets the prfTrb value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param prfTrb
     */
    public void setPrfTrb(java.lang.String prfTrb) {
        this.prfTrb = prfTrb;
    }


    /**
     * Gets the qtdDec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return qtdDec
     */
    public java.lang.Integer getQtdDec() {
        return qtdDec;
    }


    /**
     * Sets the qtdDec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param qtdDec
     */
    public void setQtdDec(java.lang.Integer qtdDec) {
        this.qtdDec = qtdDec;
    }


    /**
     * Gets the reaIsv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return reaIsv
     */
    public java.lang.Integer getReaIsv() {
        return reaIsv;
    }


    /**
     * Sets the reaIsv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param reaIsv
     */
    public void setReaIsv(java.lang.Integer reaIsv) {
        this.reaIsv = reaIsv;
    }


    /**
     * Gets the recDbc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recDbc
     */
    public java.lang.Integer getRecDbc() {
        return recDbc;
    }


    /**
     * Sets the recDbc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recDbc
     */
    public void setRecDbc(java.lang.Integer recDbc) {
        this.recDbc = recDbc;
    }


    /**
     * Gets the recDtj value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recDtj
     */
    public java.lang.Integer getRecDtj() {
        return recDtj;
    }


    /**
     * Sets the recDtj value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recDtj
     */
    public void setRecDtj(java.lang.Integer recDtj) {
        this.recDtj = recDtj;
    }


    /**
     * Gets the recDtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recDtm
     */
    public java.lang.Integer getRecDtm() {
        return recDtm;
    }


    /**
     * Sets the recDtm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recDtm
     */
    public void setRecDtm(java.lang.Integer recDtm) {
        this.recDtm = recDtm;
    }


    /**
     * Gets the recJmm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recJmm
     */
    public java.lang.Double getRecJmm() {
        return recJmm;
    }


    /**
     * Sets the recJmm value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recJmm
     */
    public void setRecJmm(java.lang.Double recJmm) {
        this.recJmm = recJmm;
    }


    /**
     * Gets the recMbc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recMbc
     */
    public java.lang.Integer getRecMbc() {
        return recMbc;
    }


    /**
     * Sets the recMbc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recMbc
     */
    public void setRecMbc(java.lang.Integer recMbc) {
        this.recMbc = recMbc;
    }


    /**
     * Gets the recMul value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recMul
     */
    public java.lang.Double getRecMul() {
        return recMul;
    }


    /**
     * Sets the recMul value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recMul
     */
    public void setRecMul(java.lang.Double recMul) {
        this.recMul = recMul;
    }


    /**
     * Gets the recPda value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recPda
     */
    public java.lang.Double getRecPda() {
        return recPda;
    }


    /**
     * Sets the recPda value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recPda
     */
    public void setRecPda(java.lang.Double recPda) {
        this.recPda = recPda;
    }


    /**
     * Gets the recTda value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recTda
     */
    public java.lang.Integer getRecTda() {
        return recTda;
    }


    /**
     * Sets the recTda value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recTda
     */
    public void setRecTda(java.lang.Integer recTda) {
        this.recTda = recTda;
    }


    /**
     * Gets the recTjr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return recTjr
     */
    public java.lang.String getRecTjr() {
        return recTjr;
    }


    /**
     * Sets the recTjr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param recTjr
     */
    public void setRecTjr(java.lang.String recTjr) {
        this.recTjr = recTjr;
    }


    /**
     * Gets the rvePdv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return rvePdv
     */
    public java.lang.Integer getRvePdv() {
        return rvePdv;
    }


    /**
     * Sets the rvePdv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param rvePdv
     */
    public void setRvePdv(java.lang.Integer rvePdv) {
        this.rvePdv = rvePdv;
    }


    /**
     * Gets the seqInt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return seqInt
     */
    public java.lang.Integer getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.Integer seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the serNce value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return serNce
     */
    public java.lang.String getSerNce() {
        return serNce;
    }


    /**
     * Sets the serNce value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param serNce
     */
    public void setSerNce(java.lang.String serNce) {
        this.serNce = serNce;
    }


    /**
     * Gets the sigEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return sigEmp
     */
    public java.lang.String getSigEmp() {
        return sigEmp;
    }


    /**
     * Sets the sigEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param sigEmp
     */
    public void setSigEmp(java.lang.String sigEmp) {
        this.sigEmp = sigEmp;
    }


    /**
     * Gets the sigFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return sigFil
     */
    public java.lang.String getSigFil() {
        return sigFil;
    }


    /**
     * Sets the sigFil value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param sigFil
     */
    public void setSigFil(java.lang.String sigFil) {
        this.sigFil = sigFil;
    }


    /**
     * Gets the sigUfs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return sigUfs
     */
    public java.lang.String getSigUfs() {
        return sigUfs;
    }


    /**
     * Sets the sigUfs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param sigUfs
     */
    public void setSigUfs(java.lang.String sigUfs) {
        this.sigUfs = sigUfs;
    }


    /**
     * Gets the snfDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return snfDev
     */
    public java.lang.String getSnfDev() {
        return snfDev;
    }


    /**
     * Sets the snfDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param snfDev
     */
    public void setSnfDev(java.lang.String snfDev) {
        this.snfDev = snfDev;
    }


    /**
     * Gets the snfIva value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return snfIva
     */
    public java.lang.String getSnfIva() {
        return snfIva;
    }


    /**
     * Sets the snfIva value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param snfIva
     */
    public void setSnfIva(java.lang.String snfIva) {
        this.snfIva = snfIva;
    }


    /**
     * Gets the snfMan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return snfMan
     */
    public java.lang.String getSnfMan() {
        return snfMan;
    }


    /**
     * Sets the snfMan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param snfMan
     */
    public void setSnfMan(java.lang.String snfMan) {
        this.snfMan = snfMan;
    }


    /**
     * Gets the snfNfc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return snfNfc
     */
    public java.lang.String getSnfNfc() {
        return snfNfc;
    }


    /**
     * Sets the snfNfc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param snfNfc
     */
    public void setSnfNfc(java.lang.String snfNfc) {
        this.snfNfc = snfNfc;
    }


    /**
     * Gets the tipEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tipEmp
     */
    public java.lang.Integer getTipEmp() {
        return tipEmp;
    }


    /**
     * Sets the tipEmp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tipEmp
     */
    public void setTipEmp(java.lang.Integer tipEmp) {
        this.tipEmp = tipEmp;
    }


    /**
     * Gets the tmpVre value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tmpVre
     */
    public java.lang.String getTmpVre() {
        return tmpVre;
    }


    /**
     * Sets the tmpVre value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tmpVre
     */
    public void setTmpVre(java.lang.String tmpVre) {
        this.tmpVre = tmpVre;
    }


    /**
     * Gets the tnsBcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsBcr
     */
    public java.lang.String getTnsBcr() {
        return tnsBcr;
    }


    /**
     * Sets the tnsBcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsBcr
     */
    public void setTnsBcr(java.lang.String tnsBcr) {
        this.tnsBcr = tnsBcr;
    }


    /**
     * Gets the tnsBrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsBrc
     */
    public java.lang.String getTnsBrc() {
        return tnsBrc;
    }


    /**
     * Sets the tnsBrc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsBrc
     */
    public void setTnsBrc(java.lang.String tnsBrc) {
        this.tnsBrc = tnsBrc;
    }


    /**
     * Gets the tnsBrs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsBrs
     */
    public java.lang.String getTnsBrs() {
        return tnsBrs;
    }


    /**
     * Sets the tnsBrs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsBrs
     */
    public void setTnsBrs(java.lang.String tnsBrs) {
        this.tnsBrs = tnsBrs;
    }


    /**
     * Gets the tnsBtc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsBtc
     */
    public java.lang.String getTnsBtc() {
        return tnsBtc;
    }


    /**
     * Sets the tnsBtc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsBtc
     */
    public void setTnsBtc(java.lang.String tnsBtc) {
        this.tnsBtc = tnsBtc;
    }


    /**
     * Gets the tnsCcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsCcp
     */
    public java.lang.String getTnsCcp() {
        return tnsCcp;
    }


    /**
     * Sets the tnsCcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsCcp
     */
    public void setTnsCcp(java.lang.String tnsCcp) {
        this.tnsCcp = tnsCcp;
    }


    /**
     * Gets the tnsCdl value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsCdl
     */
    public java.lang.String getTnsCdl() {
        return tnsCdl;
    }


    /**
     * Sets the tnsCdl value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsCdl
     */
    public void setTnsCdl(java.lang.String tnsCdl) {
        this.tnsCdl = tnsCdl;
    }


    /**
     * Gets the tnsCdt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsCdt
     */
    public java.lang.String getTnsCdt() {
        return tnsCdt;
    }


    /**
     * Sets the tnsCdt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsCdt
     */
    public void setTnsCdt(java.lang.String tnsCdt) {
        this.tnsCdt = tnsCdt;
    }


    /**
     * Gets the tnsDcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDcp
     */
    public java.lang.String getTnsDcp() {
        return tnsDcp;
    }


    /**
     * Sets the tnsDcp value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDcp
     */
    public void setTnsDcp(java.lang.String tnsDcp) {
        this.tnsDcp = tnsDcp;
    }


    /**
     * Gets the tnsDdl value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDdl
     */
    public java.lang.String getTnsDdl() {
        return tnsDdl;
    }


    /**
     * Sets the tnsDdl value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDdl
     */
    public void setTnsDdl(java.lang.String tnsDdl) {
        this.tnsDdl = tnsDdl;
    }


    /**
     * Gets the tnsDdt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDdt
     */
    public java.lang.String getTnsDdt() {
        return tnsDdt;
    }


    /**
     * Sets the tnsDdt value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDdt
     */
    public void setTnsDdt(java.lang.String tnsDdt) {
        this.tnsDdt = tnsDdt;
    }


    /**
     * Gets the tnsDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDev
     */
    public java.lang.String getTnsDev() {
        return tnsDev;
    }


    /**
     * Sets the tnsDev value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDev
     */
    public void setTnsDev(java.lang.String tnsDev) {
        this.tnsDev = tnsDev;
    }


    /**
     * Gets the tnsDmc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDmc
     */
    public java.lang.String getTnsDmc() {
        return tnsDmc;
    }


    /**
     * Sets the tnsDmc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDmc
     */
    public void setTnsDmc(java.lang.String tnsDmc) {
        this.tnsDmc = tnsDmc;
    }


    /**
     * Gets the tnsDpc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDpc
     */
    public java.lang.String getTnsDpc() {
        return tnsDpc;
    }


    /**
     * Sets the tnsDpc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDpc
     */
    public void setTnsDpc(java.lang.String tnsDpc) {
        this.tnsDpc = tnsDpc;
    }


    /**
     * Gets the tnsDpn value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDpn
     */
    public java.lang.String getTnsDpn() {
        return tnsDpn;
    }


    /**
     * Sets the tnsDpn value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDpn
     */
    public void setTnsDpn(java.lang.String tnsDpn) {
        this.tnsDpn = tnsDpn;
    }


    /**
     * Gets the tnsDsc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDsc
     */
    public java.lang.String getTnsDsc() {
        return tnsDsc;
    }


    /**
     * Sets the tnsDsc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDsc
     */
    public void setTnsDsc(java.lang.String tnsDsc) {
        this.tnsDsc = tnsDsc;
    }


    /**
     * Gets the tnsDsi value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsDsi
     */
    public java.lang.String getTnsDsi() {
        return tnsDsi;
    }


    /**
     * Sets the tnsDsi value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsDsi
     */
    public void setTnsDsi(java.lang.String tnsDsi) {
        this.tnsDsi = tnsDsi;
    }


    /**
     * Gets the tnsIsv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsIsv
     */
    public java.lang.String getTnsIsv() {
        return tnsIsv;
    }


    /**
     * Sets the tnsIsv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsIsv
     */
    public void setTnsIsv(java.lang.String tnsIsv) {
        this.tnsIsv = tnsIsv;
    }


    /**
     * Gets the tnsMan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsMan
     */
    public java.lang.String getTnsMan() {
        return tnsMan;
    }


    /**
     * Sets the tnsMan value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsMan
     */
    public void setTnsMan(java.lang.String tnsMan) {
        this.tnsMan = tnsMan;
    }


    /**
     * Gets the tnsMns value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsMns
     */
    public java.lang.String getTnsMns() {
        return tnsMns;
    }


    /**
     * Sets the tnsMns value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsMns
     */
    public void setTnsMns(java.lang.String tnsMns) {
        this.tnsMns = tnsMns;
    }


    /**
     * Gets the tnsNfc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsNfc
     */
    public java.lang.String getTnsNfc() {
        return tnsNfc;
    }


    /**
     * Sets the tnsNfc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsNfc
     */
    public void setTnsNfc(java.lang.String tnsNfc) {
        this.tnsNfc = tnsNfc;
    }


    /**
     * Gets the tnsNfs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsNfs
     */
    public java.lang.String getTnsNfs() {
        return tnsNfs;
    }


    /**
     * Sets the tnsNfs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsNfs
     */
    public void setTnsNfs(java.lang.String tnsNfs) {
        this.tnsNfs = tnsNfs;
    }


    /**
     * Gets the tnsPro value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsPro
     */
    public java.lang.String getTnsPro() {
        return tnsPro;
    }


    /**
     * Sets the tnsPro value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsPro
     */
    public void setTnsPro(java.lang.String tnsPro) {
        this.tnsPro = tnsPro;
    }


    /**
     * Gets the tnsRco value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsRco
     */
    public java.lang.String getTnsRco() {
        return tnsRco;
    }


    /**
     * Sets the tnsRco value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsRco
     */
    public void setTnsRco(java.lang.String tnsRco) {
        this.tnsRco = tnsRco;
    }


    /**
     * Gets the tnsRen value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsRen
     */
    public java.lang.String getTnsRen() {
        return tnsRen;
    }


    /**
     * Sets the tnsRen value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsRen
     */
    public void setTnsRen(java.lang.String tnsRen) {
        this.tnsRen = tnsRen;
    }


    /**
     * Gets the tnsRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsRes
     */
    public java.lang.String getTnsRes() {
        return tnsRes;
    }


    /**
     * Sets the tnsRes value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsRes
     */
    public void setTnsRes(java.lang.String tnsRes) {
        this.tnsRes = tnsRes;
    }


    /**
     * Gets the tnsRfu value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsRfu
     */
    public java.lang.String getTnsRfu() {
        return tnsRfu;
    }


    /**
     * Sets the tnsRfu value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsRfu
     */
    public void setTnsRfu(java.lang.String tnsRfu) {
        this.tnsRfu = tnsRfu;
    }


    /**
     * Gets the tnsRue value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsRue
     */
    public java.lang.String getTnsRue() {
        return tnsRue;
    }


    /**
     * Sets the tnsRue value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsRue
     */
    public void setTnsRue(java.lang.String tnsRue) {
        this.tnsRue = tnsRue;
    }


    /**
     * Gets the tnsSer value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsSer
     */
    public java.lang.String getTnsSer() {
        return tnsSer;
    }


    /**
     * Sets the tnsSer value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsSer
     */
    public void setTnsSer(java.lang.String tnsSer) {
        this.tnsSer = tnsSer;
    }


    /**
     * Gets the tnsSfe value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsSfe
     */
    public java.lang.String getTnsSfe() {
        return tnsSfe;
    }


    /**
     * Sets the tnsSfe value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsSfe
     */
    public void setTnsSfe(java.lang.String tnsSfe) {
        this.tnsSfe = tnsSfe;
    }


    /**
     * Gets the tnsSie value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsSie
     */
    public java.lang.String getTnsSie() {
        return tnsSie;
    }


    /**
     * Sets the tnsSie value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsSie
     */
    public void setTnsSie(java.lang.String tnsSie) {
        this.tnsSie = tnsSie;
    }


    /**
     * Gets the tnsTcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsTcr
     */
    public java.lang.String getTnsTcr() {
        return tnsTcr;
    }


    /**
     * Sets the tnsTcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsTcr
     */
    public void setTnsTcr(java.lang.String tnsTcr) {
        this.tnsTcr = tnsTcr;
    }


    /**
     * Gets the tnsTcs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tnsTcs
     */
    public java.lang.String getTnsTcs() {
        return tnsTcs;
    }


    /**
     * Sets the tnsTcs value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tnsTcs
     */
    public void setTnsTcs(java.lang.String tnsTcs) {
        this.tnsTcs = tnsTcs;
    }


    /**
     * Gets the tpcRcv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tpcRcv
     */
    public java.lang.Integer getTpcRcv() {
        return tpcRcv;
    }


    /**
     * Sets the tpcRcv value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tpcRcv
     */
    public void setTpcRcv(java.lang.Integer tpcRcv) {
        this.tpcRcv = tpcRcv;
    }


    /**
     * Gets the tpmCpd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tpmCpd
     */
    public java.lang.Integer getTpmCpd() {
        return tpmCpd;
    }


    /**
     * Sets the tpmCpd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tpmCpd
     */
    public void setTpmCpd(java.lang.Integer tpmCpd) {
        this.tpmCpd = tpmCpd;
    }


    /**
     * Gets the tptScf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tptScf
     */
    public java.lang.String getTptScf() {
        return tptScf;
    }


    /**
     * Sets the tptScf value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tptScf
     */
    public void setTptScf(java.lang.String tptScf) {
        this.tptScf = tptScf;
    }


    /**
     * Gets the tptSub value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return tptSub
     */
    public java.lang.String getTptSub() {
        return tptSub;
    }


    /**
     * Sets the tptSub value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param tptSub
     */
    public void setTptSub(java.lang.String tptSub) {
        this.tptSub = tptSub;
    }


    /**
     * Gets the ufsCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return ufsCtd
     */
    public java.lang.String getUfsCtd() {
        return ufsCtd;
    }


    /**
     * Sets the ufsCtd value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param ufsCtd
     */
    public void setUfsCtd(java.lang.String ufsCtd) {
        this.ufsCtd = ufsCtd;
    }


    /**
     * Gets the utiTju value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return utiTju
     */
    public java.lang.Integer getUtiTju() {
        return utiTju;
    }


    /**
     * Sets the utiTju value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param utiTju
     */
    public void setUtiTju(java.lang.Integer utiTju) {
        this.utiTju = utiTju;
    }


    /**
     * Gets the valPad value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return valPad
     */
    public java.lang.String getValPad() {
        return valPad;
    }


    /**
     * Sets the valPad value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param valPad
     */
    public void setValPad(java.lang.String valPad) {
        this.valPad = valPad;
    }


    /**
     * Gets the venCcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return venCcc
     */
    public java.lang.String getVenCcc() {
        return venCcc;
    }


    /**
     * Sets the venCcc value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param venCcc
     */
    public void setVenCcc(java.lang.String venCcc) {
        this.venCcc = venCcc;
    }


    /**
     * Gets the venCcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return venCcr
     */
    public java.lang.Integer getVenCcr() {
        return venCcr;
    }


    /**
     * Sets the venCcr value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param venCcr
     */
    public void setVenCcr(java.lang.Integer venCcr) {
        this.venCcr = venCcr;
    }


    /**
     * Gets the venCep value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return venCep
     */
    public java.lang.Integer getVenCep() {
        return venCep;
    }


    /**
     * Sets the venCep value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param venCep
     */
    public void setVenCep(java.lang.Integer venCep) {
        this.venCep = venCep;
    }


    /**
     * Gets the venRec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @return venRec
     */
    public java.lang.Integer getVenRec() {
        return venRec;
    }


    /**
     * Sets the venRec value for this EmpresafilialExportarOutEmpresaFilial.
     * 
     * @param venRec
     */
    public void setVenRec(java.lang.Integer venRec) {
        this.venRec = venRec;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportarOutEmpresaFilial)) return false;
        EmpresafilialExportarOutEmpresaFilial other = (EmpresafilialExportarOutEmpresaFilial) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.acrCca==null && other.getAcrCca()==null) || 
             (this.acrCca!=null &&
              this.acrCca.equals(other.getAcrCca()))) &&
            ((this.acrLcc==null && other.getAcrLcc()==null) || 
             (this.acrLcc!=null &&
              this.acrLcc.equals(other.getAcrLcc()))) &&
            ((this.acrMap==null && other.getAcrMap()==null) || 
             (this.acrMap!=null &&
              this.acrMap.equals(other.getAcrMap()))) &&
            ((this.acrQdd==null && other.getAcrQdd()==null) || 
             (this.acrQdd!=null &&
              this.acrQdd.equals(other.getAcrQdd()))) &&
            ((this.acrTcc==null && other.getAcrTcc()==null) || 
             (this.acrTcc!=null &&
              this.acrTcc.equals(other.getAcrTcc()))) &&
            ((this.ambNfe==null && other.getAmbNfe()==null) || 
             (this.ambNfe!=null &&
              this.ambNfe.equals(other.getAmbNfe()))) &&
            ((this.arrTrc==null && other.getArrTrc()==null) || 
             (this.arrTrc!=null &&
              this.arrTrc.equals(other.getArrTrc()))) &&
            ((this.baiCtd==null && other.getBaiCtd()==null) || 
             (this.baiCtd!=null &&
              this.baiCtd.equals(other.getBaiCtd()))) &&
            ((this.baiDev==null && other.getBaiDev()==null) || 
             (this.baiDev!=null &&
              this.baiDev.equals(other.getBaiDev()))) &&
            ((this.baiFil==null && other.getBaiFil()==null) || 
             (this.baiFil!=null &&
              this.baiFil.equals(other.getBaiFil()))) &&
            ((this.cepCob==null && other.getCepCob()==null) || 
             (this.cepCob!=null &&
              this.cepCob.equals(other.getCepCob()))) &&
            ((this.cepCtd==null && other.getCepCtd()==null) || 
             (this.cepCtd!=null &&
              this.cepCtd.equals(other.getCepCtd()))) &&
            ((this.cepEnt==null && other.getCepEnt()==null) || 
             (this.cepEnt!=null &&
              this.cepEnt.equals(other.getCepEnt()))) &&
            ((this.cepFil==null && other.getCepFil()==null) || 
             (this.cepFil!=null &&
              this.cepFil.equals(other.getCepFil()))) &&
            ((this.cgcCtd==null && other.getCgcCtd()==null) || 
             (this.cgcCtd!=null &&
              this.cgcCtd.equals(other.getCgcCtd()))) &&
            ((this.cidCob==null && other.getCidCob()==null) || 
             (this.cidCob!=null &&
              this.cidCob.equals(other.getCidCob()))) &&
            ((this.cidCtd==null && other.getCidCtd()==null) || 
             (this.cidCtd!=null &&
              this.cidCtd.equals(other.getCidCtd()))) &&
            ((this.cidEnt==null && other.getCidEnt()==null) || 
             (this.cidEnt!=null &&
              this.cidEnt.equals(other.getCidEnt()))) &&
            ((this.cidFil==null && other.getCidFil()==null) || 
             (this.cidFil!=null &&
              this.cidFil.equals(other.getCidFil()))) &&
            ((this.codBan==null && other.getCodBan()==null) || 
             (this.codBan!=null &&
              this.codBan.equals(other.getCodBan()))) &&
            ((this.codCli==null && other.getCodCli()==null) || 
             (this.codCli!=null &&
              this.codCli.equals(other.getCodCli()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codPai==null && other.getCodPai()==null) || 
             (this.codPai!=null &&
              this.codPai.equals(other.getCodPai()))) &&
            ((this.codRai==null && other.getCodRai()==null) || 
             (this.codRai!=null &&
              this.codRai.equals(other.getCodRai()))) &&
            ((this.codSuf==null && other.getCodSuf()==null) || 
             (this.codSuf!=null &&
              this.codSuf.equals(other.getCodSuf()))) &&
            ((this.codTpr==null && other.getCodTpr()==null) || 
             (this.codTpr!=null &&
              this.codTpr.equals(other.getCodTpr()))) &&
            ((this.cplCob==null && other.getCplCob()==null) || 
             (this.cplCob!=null &&
              this.cplCob.equals(other.getCplCob()))) &&
            ((this.cplCtd==null && other.getCplCtd()==null) || 
             (this.cplCtd!=null &&
              this.cplCtd.equals(other.getCplCtd()))) &&
            ((this.cplEnd==null && other.getCplEnd()==null) || 
             (this.cplEnd!=null &&
              this.cplEnd.equals(other.getCplEnd()))) &&
            ((this.cplEnt==null && other.getCplEnt()==null) || 
             (this.cplEnt!=null &&
              this.cplEnt.equals(other.getCplEnt()))) &&
            ((this.ctbCcr==null && other.getCtbCcr()==null) || 
             (this.ctbCcr!=null &&
              this.ctbCcr.equals(other.getCtbCcr()))) &&
            ((this.ctbCff==null && other.getCtbCff()==null) || 
             (this.ctbCff!=null &&
              this.ctbCff.equals(other.getCtbCff()))) &&
            ((this.ctbCrc==null && other.getCtbCrc()==null) || 
             (this.ctbCrc!=null &&
              this.ctbCrc.equals(other.getCtbCrc()))) &&
            ((this.ctbNcr==null && other.getCtbNcr()==null) || 
             (this.ctbNcr!=null &&
              this.ctbNcr.equals(other.getCtbNcr()))) &&
            ((this.ctbNsr==null && other.getCtbNsr()==null) || 
             (this.ctbNsr!=null &&
              this.ctbNsr.equals(other.getCtbNsr()))) &&
            ((this.dadosEstado==null && other.getDadosEstado()==null) || 
             (this.dadosEstado!=null &&
              java.util.Arrays.equals(this.dadosEstado, other.getDadosEstado()))) &&
            ((this.dadosPDV==null && other.getDadosPDV()==null) || 
             (this.dadosPDV!=null &&
              java.util.Arrays.equals(this.dadosPDV, other.getDadosPDV()))) &&
            ((this.depAec==null && other.getDepAec()==null) || 
             (this.depAec!=null &&
              this.depAec.equals(other.getDepAec()))) &&
            ((this.depRec==null && other.getDepRec()==null) || 
             (this.depRec!=null &&
              this.depRec.equals(other.getDepRec()))) &&
            ((this.diaDev==null && other.getDiaDev()==null) || 
             (this.diaDev!=null &&
              this.diaDev.equals(other.getDiaDev()))) &&
            ((this.efiFtr==null && other.getEfiFtr()==null) || 
             (this.efiFtr!=null &&
              this.efiFtr.equals(other.getEfiFtr()))) &&
            ((this.endCob==null && other.getEndCob()==null) || 
             (this.endCob!=null &&
              this.endCob.equals(other.getEndCob()))) &&
            ((this.endCtd==null && other.getEndCtd()==null) || 
             (this.endCtd!=null &&
              this.endCtd.equals(other.getEndCtd()))) &&
            ((this.endEnt==null && other.getEndEnt()==null) || 
             (this.endEnt!=null &&
              this.endEnt.equals(other.getEndEnt()))) &&
            ((this.endFil==null && other.getEndFil()==null) || 
             (this.endFil!=null &&
              this.endFil.equals(other.getEndFil()))) &&
            ((this.endNet==null && other.getEndNet()==null) || 
             (this.endNet!=null &&
              this.endNet.equals(other.getEndNet()))) &&
            ((this.enderecoRetirada==null && other.getEnderecoRetirada()==null) || 
             (this.enderecoRetirada!=null &&
              java.util.Arrays.equals(this.enderecoRetirada, other.getEnderecoRetirada()))) &&
            ((this.estCob==null && other.getEstCob()==null) || 
             (this.estCob!=null &&
              this.estCob.equals(other.getEstCob()))) &&
            ((this.estEnt==null && other.getEstEnt()==null) || 
             (this.estEnt!=null &&
              this.estEnt.equals(other.getEstEnt()))) &&
            ((this.faxCtd==null && other.getFaxCtd()==null) || 
             (this.faxCtd!=null &&
              this.faxCtd.equals(other.getFaxCtd()))) &&
            ((this.filCli==null && other.getFilCli()==null) || 
             (this.filCli!=null &&
              this.filCli.equals(other.getFilCli()))) &&
            ((this.filFor==null && other.getFilFor()==null) || 
             (this.filFor!=null &&
              this.filFor.equals(other.getFilFor()))) &&
            ((this.fonCtd==null && other.getFonCtd()==null) || 
             (this.fonCtd!=null &&
              this.fonCtd.equals(other.getFonCtd()))) &&
            ((this.forRcp==null && other.getForRcp()==null) || 
             (this.forRcp!=null &&
              this.forRcp.equals(other.getForRcp()))) &&
            ((this.impCfr==null && other.getImpCfr()==null) || 
             (this.impCfr!=null &&
              this.impCfr.equals(other.getImpCfr()))) &&
            ((this.impCtm==null && other.getImpCtm()==null) || 
             (this.impCtm!=null &&
              this.impCtm.equals(other.getImpCtm()))) &&
            ((this.impDav==null && other.getImpDav()==null) || 
             (this.impDav!=null &&
              this.impDav.equals(other.getImpDav()))) &&
            ((this.impPad==null && other.getImpPad()==null) || 
             (this.impPad!=null &&
              this.impPad.equals(other.getImpPad()))) &&
            ((this.incCul==null && other.getIncCul()==null) || 
             (this.incCul!=null &&
              this.incCul.equals(other.getIncCul()))) &&
            ((this.indAtc==null && other.getIndAtc()==null) || 
             (this.indAtc!=null &&
              this.indAtc.equals(other.getIndAtc()))) &&
            ((this.indBpf==null && other.getIndBpf()==null) || 
             (this.indBpf!=null &&
              this.indBpf.equals(other.getIndBpf()))) &&
            ((this.indBtt==null && other.getIndBtt()==null) || 
             (this.indBtt!=null &&
              this.indBtt.equals(other.getIndBtt()))) &&
            ((this.indClc==null && other.getIndClc()==null) || 
             (this.indClc!=null &&
              this.indClc.equals(other.getIndClc()))) &&
            ((this.indInt==null && other.getIndInt()==null) || 
             (this.indInt!=null &&
              this.indInt.equals(other.getIndInt()))) &&
            ((this.indPcc==null && other.getIndPcc()==null) || 
             (this.indPcc!=null &&
              this.indPcc.equals(other.getIndPcc()))) &&
            ((this.indPtm==null && other.getIndPtm()==null) || 
             (this.indPtm!=null &&
              this.indPtm.equals(other.getIndPtm()))) &&
            ((this.insEst==null && other.getInsEst()==null) || 
             (this.insEst!=null &&
              this.insEst.equals(other.getInsEst()))) &&
            ((this.insMun==null && other.getInsMun()==null) || 
             (this.insMun!=null &&
              this.insMun.equals(other.getInsMun()))) &&
            ((this.intNet==null && other.getIntNet()==null) || 
             (this.intNet!=null &&
              this.intNet.equals(other.getIntNet()))) &&
            ((this.limApr==null && other.getLimApr()==null) || 
             (this.limApr!=null &&
              this.limApr.equals(other.getLimApr()))) &&
            ((this.logEmp==null && other.getLogEmp()==null) || 
             (this.logEmp!=null &&
              this.logEmp.equals(other.getLogEmp()))) &&
            ((this.maxIcf==null && other.getMaxIcf()==null) || 
             (this.maxIcf!=null &&
              this.maxIcf.equals(other.getMaxIcf()))) &&
            ((this.motBle==null && other.getMotBle()==null) || 
             (this.motBle!=null &&
              this.motBle.equals(other.getMotBle()))) &&
            ((this.motCre==null && other.getMotCre()==null) || 
             (this.motCre!=null &&
              this.motCre.equals(other.getMotCre()))) &&
            ((this.motDsb==null && other.getMotDsb()==null) || 
             (this.motDsb!=null &&
              this.motDsb.equals(other.getMotDsb()))) &&
            ((this.motRes==null && other.getMotRes()==null) || 
             (this.motRes!=null &&
              this.motRes.equals(other.getMotRes()))) &&
            ((this.nenFil==null && other.getNenFil()==null) || 
             (this.nenFil!=null &&
              this.nenFil.equals(other.getNenFil()))) &&
            ((this.netCtb==null && other.getNetCtb()==null) || 
             (this.netCtb!=null &&
              this.netCtb.equals(other.getNetCtb()))) &&
            ((this.nomEmp==null && other.getNomEmp()==null) || 
             (this.nomEmp!=null &&
              this.nomEmp.equals(other.getNomEmp()))) &&
            ((this.nomFil==null && other.getNomFil()==null) || 
             (this.nomFil!=null &&
              this.nomFil.equals(other.getNomFil()))) &&
            ((this.numCgc==null && other.getNumCgc()==null) || 
             (this.numCgc!=null &&
              this.numCgc.equals(other.getNumCgc()))) &&
            ((this.numCtd==null && other.getNumCtd()==null) || 
             (this.numCtd!=null &&
              this.numCtd.equals(other.getNumCtd()))) &&
            ((this.numFax==null && other.getNumFax()==null) || 
             (this.numFax!=null &&
              this.numFax.equals(other.getNumFax()))) &&
            ((this.numFon==null && other.getNumFon()==null) || 
             (this.numFon!=null &&
              this.numFon.equals(other.getNumFon()))) &&
            ((this.obmBes==null && other.getObmBes()==null) || 
             (this.obmBes!=null &&
              this.obmBes.equals(other.getObmBes()))) &&
            ((this.obmMvt==null && other.getObmMvt()==null) || 
             (this.obmMvt!=null &&
              this.obmMvt.equals(other.getObmMvt()))) &&
            ((this.obmRes==null && other.getObmRes()==null) || 
             (this.obmRes!=null &&
              this.obmRes.equals(other.getObmRes()))) &&
            ((this.ordAplicaTabJur==null && other.getOrdAplicaTabJur()==null) || 
             (this.ordAplicaTabJur!=null &&
              java.util.Arrays.equals(this.ordAplicaTabJur, other.getOrdAplicaTabJur()))) &&
            ((this.pagEng==null && other.getPagEng()==null) || 
             (this.pagEng!=null &&
              this.pagEng.equals(other.getPagEng()))) &&
            ((this.perCdi==null && other.getPerCdi()==null) || 
             (this.perCdi!=null &&
              this.perCdi.equals(other.getPerCdi()))) &&
            ((this.perDop==null && other.getPerDop()==null) || 
             (this.perDop!=null &&
              this.perDop.equals(other.getPerDop()))) &&
            ((this.prfTrb==null && other.getPrfTrb()==null) || 
             (this.prfTrb!=null &&
              this.prfTrb.equals(other.getPrfTrb()))) &&
            ((this.qtdDec==null && other.getQtdDec()==null) || 
             (this.qtdDec!=null &&
              this.qtdDec.equals(other.getQtdDec()))) &&
            ((this.reaIsv==null && other.getReaIsv()==null) || 
             (this.reaIsv!=null &&
              this.reaIsv.equals(other.getReaIsv()))) &&
            ((this.recDbc==null && other.getRecDbc()==null) || 
             (this.recDbc!=null &&
              this.recDbc.equals(other.getRecDbc()))) &&
            ((this.recDtj==null && other.getRecDtj()==null) || 
             (this.recDtj!=null &&
              this.recDtj.equals(other.getRecDtj()))) &&
            ((this.recDtm==null && other.getRecDtm()==null) || 
             (this.recDtm!=null &&
              this.recDtm.equals(other.getRecDtm()))) &&
            ((this.recJmm==null && other.getRecJmm()==null) || 
             (this.recJmm!=null &&
              this.recJmm.equals(other.getRecJmm()))) &&
            ((this.recMbc==null && other.getRecMbc()==null) || 
             (this.recMbc!=null &&
              this.recMbc.equals(other.getRecMbc()))) &&
            ((this.recMul==null && other.getRecMul()==null) || 
             (this.recMul!=null &&
              this.recMul.equals(other.getRecMul()))) &&
            ((this.recPda==null && other.getRecPda()==null) || 
             (this.recPda!=null &&
              this.recPda.equals(other.getRecPda()))) &&
            ((this.recTda==null && other.getRecTda()==null) || 
             (this.recTda!=null &&
              this.recTda.equals(other.getRecTda()))) &&
            ((this.recTjr==null && other.getRecTjr()==null) || 
             (this.recTjr!=null &&
              this.recTjr.equals(other.getRecTjr()))) &&
            ((this.rvePdv==null && other.getRvePdv()==null) || 
             (this.rvePdv!=null &&
              this.rvePdv.equals(other.getRvePdv()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.serNce==null && other.getSerNce()==null) || 
             (this.serNce!=null &&
              this.serNce.equals(other.getSerNce()))) &&
            ((this.sigEmp==null && other.getSigEmp()==null) || 
             (this.sigEmp!=null &&
              this.sigEmp.equals(other.getSigEmp()))) &&
            ((this.sigFil==null && other.getSigFil()==null) || 
             (this.sigFil!=null &&
              this.sigFil.equals(other.getSigFil()))) &&
            ((this.sigUfs==null && other.getSigUfs()==null) || 
             (this.sigUfs!=null &&
              this.sigUfs.equals(other.getSigUfs()))) &&
            ((this.snfDev==null && other.getSnfDev()==null) || 
             (this.snfDev!=null &&
              this.snfDev.equals(other.getSnfDev()))) &&
            ((this.snfIva==null && other.getSnfIva()==null) || 
             (this.snfIva!=null &&
              this.snfIva.equals(other.getSnfIva()))) &&
            ((this.snfMan==null && other.getSnfMan()==null) || 
             (this.snfMan!=null &&
              this.snfMan.equals(other.getSnfMan()))) &&
            ((this.snfNfc==null && other.getSnfNfc()==null) || 
             (this.snfNfc!=null &&
              this.snfNfc.equals(other.getSnfNfc()))) &&
            ((this.tipEmp==null && other.getTipEmp()==null) || 
             (this.tipEmp!=null &&
              this.tipEmp.equals(other.getTipEmp()))) &&
            ((this.tmpVre==null && other.getTmpVre()==null) || 
             (this.tmpVre!=null &&
              this.tmpVre.equals(other.getTmpVre()))) &&
            ((this.tnsBcr==null && other.getTnsBcr()==null) || 
             (this.tnsBcr!=null &&
              this.tnsBcr.equals(other.getTnsBcr()))) &&
            ((this.tnsBrc==null && other.getTnsBrc()==null) || 
             (this.tnsBrc!=null &&
              this.tnsBrc.equals(other.getTnsBrc()))) &&
            ((this.tnsBrs==null && other.getTnsBrs()==null) || 
             (this.tnsBrs!=null &&
              this.tnsBrs.equals(other.getTnsBrs()))) &&
            ((this.tnsBtc==null && other.getTnsBtc()==null) || 
             (this.tnsBtc!=null &&
              this.tnsBtc.equals(other.getTnsBtc()))) &&
            ((this.tnsCcp==null && other.getTnsCcp()==null) || 
             (this.tnsCcp!=null &&
              this.tnsCcp.equals(other.getTnsCcp()))) &&
            ((this.tnsCdl==null && other.getTnsCdl()==null) || 
             (this.tnsCdl!=null &&
              this.tnsCdl.equals(other.getTnsCdl()))) &&
            ((this.tnsCdt==null && other.getTnsCdt()==null) || 
             (this.tnsCdt!=null &&
              this.tnsCdt.equals(other.getTnsCdt()))) &&
            ((this.tnsDcp==null && other.getTnsDcp()==null) || 
             (this.tnsDcp!=null &&
              this.tnsDcp.equals(other.getTnsDcp()))) &&
            ((this.tnsDdl==null && other.getTnsDdl()==null) || 
             (this.tnsDdl!=null &&
              this.tnsDdl.equals(other.getTnsDdl()))) &&
            ((this.tnsDdt==null && other.getTnsDdt()==null) || 
             (this.tnsDdt!=null &&
              this.tnsDdt.equals(other.getTnsDdt()))) &&
            ((this.tnsDev==null && other.getTnsDev()==null) || 
             (this.tnsDev!=null &&
              this.tnsDev.equals(other.getTnsDev()))) &&
            ((this.tnsDmc==null && other.getTnsDmc()==null) || 
             (this.tnsDmc!=null &&
              this.tnsDmc.equals(other.getTnsDmc()))) &&
            ((this.tnsDpc==null && other.getTnsDpc()==null) || 
             (this.tnsDpc!=null &&
              this.tnsDpc.equals(other.getTnsDpc()))) &&
            ((this.tnsDpn==null && other.getTnsDpn()==null) || 
             (this.tnsDpn!=null &&
              this.tnsDpn.equals(other.getTnsDpn()))) &&
            ((this.tnsDsc==null && other.getTnsDsc()==null) || 
             (this.tnsDsc!=null &&
              this.tnsDsc.equals(other.getTnsDsc()))) &&
            ((this.tnsDsi==null && other.getTnsDsi()==null) || 
             (this.tnsDsi!=null &&
              this.tnsDsi.equals(other.getTnsDsi()))) &&
            ((this.tnsIsv==null && other.getTnsIsv()==null) || 
             (this.tnsIsv!=null &&
              this.tnsIsv.equals(other.getTnsIsv()))) &&
            ((this.tnsMan==null && other.getTnsMan()==null) || 
             (this.tnsMan!=null &&
              this.tnsMan.equals(other.getTnsMan()))) &&
            ((this.tnsMns==null && other.getTnsMns()==null) || 
             (this.tnsMns!=null &&
              this.tnsMns.equals(other.getTnsMns()))) &&
            ((this.tnsNfc==null && other.getTnsNfc()==null) || 
             (this.tnsNfc!=null &&
              this.tnsNfc.equals(other.getTnsNfc()))) &&
            ((this.tnsNfs==null && other.getTnsNfs()==null) || 
             (this.tnsNfs!=null &&
              this.tnsNfs.equals(other.getTnsNfs()))) &&
            ((this.tnsPro==null && other.getTnsPro()==null) || 
             (this.tnsPro!=null &&
              this.tnsPro.equals(other.getTnsPro()))) &&
            ((this.tnsRco==null && other.getTnsRco()==null) || 
             (this.tnsRco!=null &&
              this.tnsRco.equals(other.getTnsRco()))) &&
            ((this.tnsRen==null && other.getTnsRen()==null) || 
             (this.tnsRen!=null &&
              this.tnsRen.equals(other.getTnsRen()))) &&
            ((this.tnsRes==null && other.getTnsRes()==null) || 
             (this.tnsRes!=null &&
              this.tnsRes.equals(other.getTnsRes()))) &&
            ((this.tnsRfu==null && other.getTnsRfu()==null) || 
             (this.tnsRfu!=null &&
              this.tnsRfu.equals(other.getTnsRfu()))) &&
            ((this.tnsRue==null && other.getTnsRue()==null) || 
             (this.tnsRue!=null &&
              this.tnsRue.equals(other.getTnsRue()))) &&
            ((this.tnsSer==null && other.getTnsSer()==null) || 
             (this.tnsSer!=null &&
              this.tnsSer.equals(other.getTnsSer()))) &&
            ((this.tnsSfe==null && other.getTnsSfe()==null) || 
             (this.tnsSfe!=null &&
              this.tnsSfe.equals(other.getTnsSfe()))) &&
            ((this.tnsSie==null && other.getTnsSie()==null) || 
             (this.tnsSie!=null &&
              this.tnsSie.equals(other.getTnsSie()))) &&
            ((this.tnsTcr==null && other.getTnsTcr()==null) || 
             (this.tnsTcr!=null &&
              this.tnsTcr.equals(other.getTnsTcr()))) &&
            ((this.tnsTcs==null && other.getTnsTcs()==null) || 
             (this.tnsTcs!=null &&
              this.tnsTcs.equals(other.getTnsTcs()))) &&
            ((this.tpcRcv==null && other.getTpcRcv()==null) || 
             (this.tpcRcv!=null &&
              this.tpcRcv.equals(other.getTpcRcv()))) &&
            ((this.tpmCpd==null && other.getTpmCpd()==null) || 
             (this.tpmCpd!=null &&
              this.tpmCpd.equals(other.getTpmCpd()))) &&
            ((this.tptScf==null && other.getTptScf()==null) || 
             (this.tptScf!=null &&
              this.tptScf.equals(other.getTptScf()))) &&
            ((this.tptSub==null && other.getTptSub()==null) || 
             (this.tptSub!=null &&
              this.tptSub.equals(other.getTptSub()))) &&
            ((this.ufsCtd==null && other.getUfsCtd()==null) || 
             (this.ufsCtd!=null &&
              this.ufsCtd.equals(other.getUfsCtd()))) &&
            ((this.utiTju==null && other.getUtiTju()==null) || 
             (this.utiTju!=null &&
              this.utiTju.equals(other.getUtiTju()))) &&
            ((this.valPad==null && other.getValPad()==null) || 
             (this.valPad!=null &&
              this.valPad.equals(other.getValPad()))) &&
            ((this.venCcc==null && other.getVenCcc()==null) || 
             (this.venCcc!=null &&
              this.venCcc.equals(other.getVenCcc()))) &&
            ((this.venCcr==null && other.getVenCcr()==null) || 
             (this.venCcr!=null &&
              this.venCcr.equals(other.getVenCcr()))) &&
            ((this.venCep==null && other.getVenCep()==null) || 
             (this.venCep!=null &&
              this.venCep.equals(other.getVenCep()))) &&
            ((this.venRec==null && other.getVenRec()==null) || 
             (this.venRec!=null &&
              this.venRec.equals(other.getVenRec())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAcrCca() != null) {
            _hashCode += getAcrCca().hashCode();
        }
        if (getAcrLcc() != null) {
            _hashCode += getAcrLcc().hashCode();
        }
        if (getAcrMap() != null) {
            _hashCode += getAcrMap().hashCode();
        }
        if (getAcrQdd() != null) {
            _hashCode += getAcrQdd().hashCode();
        }
        if (getAcrTcc() != null) {
            _hashCode += getAcrTcc().hashCode();
        }
        if (getAmbNfe() != null) {
            _hashCode += getAmbNfe().hashCode();
        }
        if (getArrTrc() != null) {
            _hashCode += getArrTrc().hashCode();
        }
        if (getBaiCtd() != null) {
            _hashCode += getBaiCtd().hashCode();
        }
        if (getBaiDev() != null) {
            _hashCode += getBaiDev().hashCode();
        }
        if (getBaiFil() != null) {
            _hashCode += getBaiFil().hashCode();
        }
        if (getCepCob() != null) {
            _hashCode += getCepCob().hashCode();
        }
        if (getCepCtd() != null) {
            _hashCode += getCepCtd().hashCode();
        }
        if (getCepEnt() != null) {
            _hashCode += getCepEnt().hashCode();
        }
        if (getCepFil() != null) {
            _hashCode += getCepFil().hashCode();
        }
        if (getCgcCtd() != null) {
            _hashCode += getCgcCtd().hashCode();
        }
        if (getCidCob() != null) {
            _hashCode += getCidCob().hashCode();
        }
        if (getCidCtd() != null) {
            _hashCode += getCidCtd().hashCode();
        }
        if (getCidEnt() != null) {
            _hashCode += getCidEnt().hashCode();
        }
        if (getCidFil() != null) {
            _hashCode += getCidFil().hashCode();
        }
        if (getCodBan() != null) {
            _hashCode += getCodBan().hashCode();
        }
        if (getCodCli() != null) {
            _hashCode += getCodCli().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodPai() != null) {
            _hashCode += getCodPai().hashCode();
        }
        if (getCodRai() != null) {
            _hashCode += getCodRai().hashCode();
        }
        if (getCodSuf() != null) {
            _hashCode += getCodSuf().hashCode();
        }
        if (getCodTpr() != null) {
            _hashCode += getCodTpr().hashCode();
        }
        if (getCplCob() != null) {
            _hashCode += getCplCob().hashCode();
        }
        if (getCplCtd() != null) {
            _hashCode += getCplCtd().hashCode();
        }
        if (getCplEnd() != null) {
            _hashCode += getCplEnd().hashCode();
        }
        if (getCplEnt() != null) {
            _hashCode += getCplEnt().hashCode();
        }
        if (getCtbCcr() != null) {
            _hashCode += getCtbCcr().hashCode();
        }
        if (getCtbCff() != null) {
            _hashCode += getCtbCff().hashCode();
        }
        if (getCtbCrc() != null) {
            _hashCode += getCtbCrc().hashCode();
        }
        if (getCtbNcr() != null) {
            _hashCode += getCtbNcr().hashCode();
        }
        if (getCtbNsr() != null) {
            _hashCode += getCtbNsr().hashCode();
        }
        if (getDadosEstado() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDadosEstado());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDadosEstado(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDadosPDV() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDadosPDV());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDadosPDV(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDepAec() != null) {
            _hashCode += getDepAec().hashCode();
        }
        if (getDepRec() != null) {
            _hashCode += getDepRec().hashCode();
        }
        if (getDiaDev() != null) {
            _hashCode += getDiaDev().hashCode();
        }
        if (getEfiFtr() != null) {
            _hashCode += getEfiFtr().hashCode();
        }
        if (getEndCob() != null) {
            _hashCode += getEndCob().hashCode();
        }
        if (getEndCtd() != null) {
            _hashCode += getEndCtd().hashCode();
        }
        if (getEndEnt() != null) {
            _hashCode += getEndEnt().hashCode();
        }
        if (getEndFil() != null) {
            _hashCode += getEndFil().hashCode();
        }
        if (getEndNet() != null) {
            _hashCode += getEndNet().hashCode();
        }
        if (getEnderecoRetirada() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEnderecoRetirada());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEnderecoRetirada(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEstCob() != null) {
            _hashCode += getEstCob().hashCode();
        }
        if (getEstEnt() != null) {
            _hashCode += getEstEnt().hashCode();
        }
        if (getFaxCtd() != null) {
            _hashCode += getFaxCtd().hashCode();
        }
        if (getFilCli() != null) {
            _hashCode += getFilCli().hashCode();
        }
        if (getFilFor() != null) {
            _hashCode += getFilFor().hashCode();
        }
        if (getFonCtd() != null) {
            _hashCode += getFonCtd().hashCode();
        }
        if (getForRcp() != null) {
            _hashCode += getForRcp().hashCode();
        }
        if (getImpCfr() != null) {
            _hashCode += getImpCfr().hashCode();
        }
        if (getImpCtm() != null) {
            _hashCode += getImpCtm().hashCode();
        }
        if (getImpDav() != null) {
            _hashCode += getImpDav().hashCode();
        }
        if (getImpPad() != null) {
            _hashCode += getImpPad().hashCode();
        }
        if (getIncCul() != null) {
            _hashCode += getIncCul().hashCode();
        }
        if (getIndAtc() != null) {
            _hashCode += getIndAtc().hashCode();
        }
        if (getIndBpf() != null) {
            _hashCode += getIndBpf().hashCode();
        }
        if (getIndBtt() != null) {
            _hashCode += getIndBtt().hashCode();
        }
        if (getIndClc() != null) {
            _hashCode += getIndClc().hashCode();
        }
        if (getIndInt() != null) {
            _hashCode += getIndInt().hashCode();
        }
        if (getIndPcc() != null) {
            _hashCode += getIndPcc().hashCode();
        }
        if (getIndPtm() != null) {
            _hashCode += getIndPtm().hashCode();
        }
        if (getInsEst() != null) {
            _hashCode += getInsEst().hashCode();
        }
        if (getInsMun() != null) {
            _hashCode += getInsMun().hashCode();
        }
        if (getIntNet() != null) {
            _hashCode += getIntNet().hashCode();
        }
        if (getLimApr() != null) {
            _hashCode += getLimApr().hashCode();
        }
        if (getLogEmp() != null) {
            _hashCode += getLogEmp().hashCode();
        }
        if (getMaxIcf() != null) {
            _hashCode += getMaxIcf().hashCode();
        }
        if (getMotBle() != null) {
            _hashCode += getMotBle().hashCode();
        }
        if (getMotCre() != null) {
            _hashCode += getMotCre().hashCode();
        }
        if (getMotDsb() != null) {
            _hashCode += getMotDsb().hashCode();
        }
        if (getMotRes() != null) {
            _hashCode += getMotRes().hashCode();
        }
        if (getNenFil() != null) {
            _hashCode += getNenFil().hashCode();
        }
        if (getNetCtb() != null) {
            _hashCode += getNetCtb().hashCode();
        }
        if (getNomEmp() != null) {
            _hashCode += getNomEmp().hashCode();
        }
        if (getNomFil() != null) {
            _hashCode += getNomFil().hashCode();
        }
        if (getNumCgc() != null) {
            _hashCode += getNumCgc().hashCode();
        }
        if (getNumCtd() != null) {
            _hashCode += getNumCtd().hashCode();
        }
        if (getNumFax() != null) {
            _hashCode += getNumFax().hashCode();
        }
        if (getNumFon() != null) {
            _hashCode += getNumFon().hashCode();
        }
        if (getObmBes() != null) {
            _hashCode += getObmBes().hashCode();
        }
        if (getObmMvt() != null) {
            _hashCode += getObmMvt().hashCode();
        }
        if (getObmRes() != null) {
            _hashCode += getObmRes().hashCode();
        }
        if (getOrdAplicaTabJur() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrdAplicaTabJur());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrdAplicaTabJur(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPagEng() != null) {
            _hashCode += getPagEng().hashCode();
        }
        if (getPerCdi() != null) {
            _hashCode += getPerCdi().hashCode();
        }
        if (getPerDop() != null) {
            _hashCode += getPerDop().hashCode();
        }
        if (getPrfTrb() != null) {
            _hashCode += getPrfTrb().hashCode();
        }
        if (getQtdDec() != null) {
            _hashCode += getQtdDec().hashCode();
        }
        if (getReaIsv() != null) {
            _hashCode += getReaIsv().hashCode();
        }
        if (getRecDbc() != null) {
            _hashCode += getRecDbc().hashCode();
        }
        if (getRecDtj() != null) {
            _hashCode += getRecDtj().hashCode();
        }
        if (getRecDtm() != null) {
            _hashCode += getRecDtm().hashCode();
        }
        if (getRecJmm() != null) {
            _hashCode += getRecJmm().hashCode();
        }
        if (getRecMbc() != null) {
            _hashCode += getRecMbc().hashCode();
        }
        if (getRecMul() != null) {
            _hashCode += getRecMul().hashCode();
        }
        if (getRecPda() != null) {
            _hashCode += getRecPda().hashCode();
        }
        if (getRecTda() != null) {
            _hashCode += getRecTda().hashCode();
        }
        if (getRecTjr() != null) {
            _hashCode += getRecTjr().hashCode();
        }
        if (getRvePdv() != null) {
            _hashCode += getRvePdv().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSerNce() != null) {
            _hashCode += getSerNce().hashCode();
        }
        if (getSigEmp() != null) {
            _hashCode += getSigEmp().hashCode();
        }
        if (getSigFil() != null) {
            _hashCode += getSigFil().hashCode();
        }
        if (getSigUfs() != null) {
            _hashCode += getSigUfs().hashCode();
        }
        if (getSnfDev() != null) {
            _hashCode += getSnfDev().hashCode();
        }
        if (getSnfIva() != null) {
            _hashCode += getSnfIva().hashCode();
        }
        if (getSnfMan() != null) {
            _hashCode += getSnfMan().hashCode();
        }
        if (getSnfNfc() != null) {
            _hashCode += getSnfNfc().hashCode();
        }
        if (getTipEmp() != null) {
            _hashCode += getTipEmp().hashCode();
        }
        if (getTmpVre() != null) {
            _hashCode += getTmpVre().hashCode();
        }
        if (getTnsBcr() != null) {
            _hashCode += getTnsBcr().hashCode();
        }
        if (getTnsBrc() != null) {
            _hashCode += getTnsBrc().hashCode();
        }
        if (getTnsBrs() != null) {
            _hashCode += getTnsBrs().hashCode();
        }
        if (getTnsBtc() != null) {
            _hashCode += getTnsBtc().hashCode();
        }
        if (getTnsCcp() != null) {
            _hashCode += getTnsCcp().hashCode();
        }
        if (getTnsCdl() != null) {
            _hashCode += getTnsCdl().hashCode();
        }
        if (getTnsCdt() != null) {
            _hashCode += getTnsCdt().hashCode();
        }
        if (getTnsDcp() != null) {
            _hashCode += getTnsDcp().hashCode();
        }
        if (getTnsDdl() != null) {
            _hashCode += getTnsDdl().hashCode();
        }
        if (getTnsDdt() != null) {
            _hashCode += getTnsDdt().hashCode();
        }
        if (getTnsDev() != null) {
            _hashCode += getTnsDev().hashCode();
        }
        if (getTnsDmc() != null) {
            _hashCode += getTnsDmc().hashCode();
        }
        if (getTnsDpc() != null) {
            _hashCode += getTnsDpc().hashCode();
        }
        if (getTnsDpn() != null) {
            _hashCode += getTnsDpn().hashCode();
        }
        if (getTnsDsc() != null) {
            _hashCode += getTnsDsc().hashCode();
        }
        if (getTnsDsi() != null) {
            _hashCode += getTnsDsi().hashCode();
        }
        if (getTnsIsv() != null) {
            _hashCode += getTnsIsv().hashCode();
        }
        if (getTnsMan() != null) {
            _hashCode += getTnsMan().hashCode();
        }
        if (getTnsMns() != null) {
            _hashCode += getTnsMns().hashCode();
        }
        if (getTnsNfc() != null) {
            _hashCode += getTnsNfc().hashCode();
        }
        if (getTnsNfs() != null) {
            _hashCode += getTnsNfs().hashCode();
        }
        if (getTnsPro() != null) {
            _hashCode += getTnsPro().hashCode();
        }
        if (getTnsRco() != null) {
            _hashCode += getTnsRco().hashCode();
        }
        if (getTnsRen() != null) {
            _hashCode += getTnsRen().hashCode();
        }
        if (getTnsRes() != null) {
            _hashCode += getTnsRes().hashCode();
        }
        if (getTnsRfu() != null) {
            _hashCode += getTnsRfu().hashCode();
        }
        if (getTnsRue() != null) {
            _hashCode += getTnsRue().hashCode();
        }
        if (getTnsSer() != null) {
            _hashCode += getTnsSer().hashCode();
        }
        if (getTnsSfe() != null) {
            _hashCode += getTnsSfe().hashCode();
        }
        if (getTnsSie() != null) {
            _hashCode += getTnsSie().hashCode();
        }
        if (getTnsTcr() != null) {
            _hashCode += getTnsTcr().hashCode();
        }
        if (getTnsTcs() != null) {
            _hashCode += getTnsTcs().hashCode();
        }
        if (getTpcRcv() != null) {
            _hashCode += getTpcRcv().hashCode();
        }
        if (getTpmCpd() != null) {
            _hashCode += getTpmCpd().hashCode();
        }
        if (getTptScf() != null) {
            _hashCode += getTptScf().hashCode();
        }
        if (getTptSub() != null) {
            _hashCode += getTptSub().hashCode();
        }
        if (getUfsCtd() != null) {
            _hashCode += getUfsCtd().hashCode();
        }
        if (getUtiTju() != null) {
            _hashCode += getUtiTju().hashCode();
        }
        if (getValPad() != null) {
            _hashCode += getValPad().hashCode();
        }
        if (getVenCcc() != null) {
            _hashCode += getVenCcc().hashCode();
        }
        if (getVenCcr() != null) {
            _hashCode += getVenCcr().hashCode();
        }
        if (getVenCep() != null) {
            _hashCode += getVenCep().hashCode();
        }
        if (getVenRec() != null) {
            _hashCode += getVenRec().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportarOutEmpresaFilial.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilial"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrCca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrCca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrLcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrLcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrMap");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrMap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrQdd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrQdd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acrTcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acrTcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ambNfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ambNfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arrTrc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "arrTrc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiDev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiDev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("baiFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "baiFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cepFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cepFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cgcCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cgcCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codBan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codBan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codPai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codPai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codRai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codRai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSuf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codSuf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cplEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cplEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctbCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctbCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctbCff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctbCff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctbCrc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctbCrc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctbNcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctbNcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctbNsr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ctbNsr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dadosEstado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dadosEstado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialDadosEstado"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dadosPDV");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dadosPDV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialDadosPDV"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depAec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depAec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depRec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "depRec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaDev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaDev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("efiFtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "efiFtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enderecoRetirada");
        elemField.setXmlName(new javax.xml.namespace.QName("", "enderecoRetirada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialEnderecoRetirada"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estCob");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estCob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("estEnt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "estEnt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filCli");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filCli"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filFor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filFor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fonCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fonCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forRcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "forRcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impCfr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impCfr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impCtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impCtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impDav");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impDav"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incCul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incCul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indAtc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indAtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indBpf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indBpf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indBtt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indBtt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indClc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indClc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indPtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indPtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insEst");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insEst"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insMun");
        elemField.setXmlName(new javax.xml.namespace.QName("", "insMun"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intNet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intNet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limApr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limApr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxIcf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maxIcf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motBle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motBle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motCre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motCre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motDsb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motDsb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nenFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nenFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netCtb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "netCtb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCgc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCgc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numFax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numFax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numFon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numFon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obmBes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obmBes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obmMvt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obmMvt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("obmRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "obmRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordAplicaTabJur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordAplicaTabJur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportarOutEmpresaFilialOrdAplicaTabJur"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagEng");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pagEng"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perCdi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perCdi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perDop");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perDop"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prfTrb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prfTrb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("qtdDec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "qtdDec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reaIsv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reaIsv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDbc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDbc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recDtm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recDtm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recJmm");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recJmm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recMbc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recMbc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recPda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recPda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recTda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recTda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recTjr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recTjr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rvePdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rvePdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serNce");
        elemField.setXmlName(new javax.xml.namespace.QName("", "serNce"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigUfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sigUfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfDev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfDev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfIva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfIva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfMan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfMan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snfNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snfNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tmpVre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tmpVre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsBcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsBcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsBrc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsBrc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsBrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsBrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsBtc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsBtc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCdl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCdl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsCdt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsCdt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDcp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDcp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDdl");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDdl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDdt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDdt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDev");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDev"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDmc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDmc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDpc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDpc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDpn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDpn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsDsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsDsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsIsv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsIsv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsMan");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsMan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsMns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsMns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsNfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsNfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsNfs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsNfs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsPro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsPro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsRco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsRco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsRen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsRen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsRfu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsRfu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsRue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsRue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsSer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsSer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsSfe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsSfe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsSie");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsSie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsTcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsTcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tnsTcs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tnsTcs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tpcRcv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tpcRcv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tpmCpd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tpmCpd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tptScf");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tptScf"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tptSub");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tptSub"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ufsCtd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ufsCtd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("utiTju");
        elemField.setXmlName(new javax.xml.namespace.QName("", "utiTju"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valPad");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valPad"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venCcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venCcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venCcr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venCcr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venCep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venCep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("venRec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "venRec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
