package br.com.senior.agroinfo.exceptions;

public class TitulosNotFoundException extends ContasPagarWSException {

	public TitulosNotFoundException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = 1L;

}
