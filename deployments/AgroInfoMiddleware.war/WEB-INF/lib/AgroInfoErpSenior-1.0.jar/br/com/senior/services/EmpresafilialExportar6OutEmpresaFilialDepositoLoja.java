/**
 * EmpresafilialExportar6OutEmpresaFilialDepositoLoja.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class EmpresafilialExportar6OutEmpresaFilialDepositoLoja  implements java.io.Serializable {
    private java.lang.String codDep;

    private java.lang.String desDep;

    private java.lang.String sitDep;

    public EmpresafilialExportar6OutEmpresaFilialDepositoLoja() {
    }

    public EmpresafilialExportar6OutEmpresaFilialDepositoLoja(
           java.lang.String codDep,
           java.lang.String desDep,
           java.lang.String sitDep) {
           this.codDep = codDep;
           this.desDep = desDep;
           this.sitDep = sitDep;
    }


    /**
     * Gets the codDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @return codDep
     */
    public java.lang.String getCodDep() {
        return codDep;
    }


    /**
     * Sets the codDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @param codDep
     */
    public void setCodDep(java.lang.String codDep) {
        this.codDep = codDep;
    }


    /**
     * Gets the desDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @return desDep
     */
    public java.lang.String getDesDep() {
        return desDep;
    }


    /**
     * Sets the desDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @param desDep
     */
    public void setDesDep(java.lang.String desDep) {
        this.desDep = desDep;
    }


    /**
     * Gets the sitDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @return sitDep
     */
    public java.lang.String getSitDep() {
        return sitDep;
    }


    /**
     * Sets the sitDep value for this EmpresafilialExportar6OutEmpresaFilialDepositoLoja.
     * 
     * @param sitDep
     */
    public void setSitDep(java.lang.String sitDep) {
        this.sitDep = sitDep;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmpresafilialExportar6OutEmpresaFilialDepositoLoja)) return false;
        EmpresafilialExportar6OutEmpresaFilialDepositoLoja other = (EmpresafilialExportar6OutEmpresaFilialDepositoLoja) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codDep==null && other.getCodDep()==null) || 
             (this.codDep!=null &&
              this.codDep.equals(other.getCodDep()))) &&
            ((this.desDep==null && other.getDesDep()==null) || 
             (this.desDep!=null &&
              this.desDep.equals(other.getDesDep()))) &&
            ((this.sitDep==null && other.getSitDep()==null) || 
             (this.sitDep!=null &&
              this.sitDep.equals(other.getSitDep())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodDep() != null) {
            _hashCode += getCodDep().hashCode();
        }
        if (getDesDep() != null) {
            _hashCode += getDesDep().hashCode();
        }
        if (getSitDep() != null) {
            _hashCode += getSitDep().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmpresafilialExportar6OutEmpresaFilialDepositoLoja.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "empresafilialExportar6OutEmpresaFilialDepositoLoja"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("desDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "desDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitDep");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitDep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
