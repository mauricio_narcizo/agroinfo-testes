/**
 * TitulosExportarBaixaTitulosReceberOutBaixaTitulos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosExportarBaixaTitulosReceberOutBaixaTitulos  implements java.io.Serializable {
    private java.lang.String bxaCpt;

    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.Integer codFpg;

    private java.lang.Integer codFpj;

    private java.lang.String codTpt;

    private java.lang.String datAlt;

    private java.lang.String datCco;

    private java.lang.String datLib;

    private java.lang.String datMov;

    private java.lang.String datPgt;

    private java.lang.Integer diaAtr;

    private java.lang.Integer diaJrs;

    private java.lang.Integer filOri;

    private java.lang.String horAlt;

    private java.lang.String numCco;

    private java.lang.String numDoc;

    private java.lang.Integer numPrj;

    private java.lang.String numTit;

    private java.lang.String segAlt;

    private java.lang.Integer seqCco;

    private java.lang.Integer seqInt;

    private java.lang.Integer seqMov;

    private java.lang.String sitReg;

    private java.lang.String tipBai;

    private java.lang.Double vlrCor;

    private java.lang.Double vlrDsc;

    private java.lang.Double vlrEnc;

    private java.lang.Double vlrJrs;

    private java.lang.Double vlrLiq;

    private java.lang.Double vlrMov;

    private java.lang.Double vlrMul;

    private java.lang.Double vlrOac;

    private java.lang.Double vlrOde;

    public TitulosExportarBaixaTitulosReceberOutBaixaTitulos() {
    }

    public TitulosExportarBaixaTitulosReceberOutBaixaTitulos(
           java.lang.String bxaCpt,
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.Integer codFpg,
           java.lang.Integer codFpj,
           java.lang.String codTpt,
           java.lang.String datAlt,
           java.lang.String datCco,
           java.lang.String datLib,
           java.lang.String datMov,
           java.lang.String datPgt,
           java.lang.Integer diaAtr,
           java.lang.Integer diaJrs,
           java.lang.Integer filOri,
           java.lang.String horAlt,
           java.lang.String numCco,
           java.lang.String numDoc,
           java.lang.Integer numPrj,
           java.lang.String numTit,
           java.lang.String segAlt,
           java.lang.Integer seqCco,
           java.lang.Integer seqInt,
           java.lang.Integer seqMov,
           java.lang.String sitReg,
           java.lang.String tipBai,
           java.lang.Double vlrCor,
           java.lang.Double vlrDsc,
           java.lang.Double vlrEnc,
           java.lang.Double vlrJrs,
           java.lang.Double vlrLiq,
           java.lang.Double vlrMov,
           java.lang.Double vlrMul,
           java.lang.Double vlrOac,
           java.lang.Double vlrOde) {
           this.bxaCpt = bxaCpt;
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.codFpg = codFpg;
           this.codFpj = codFpj;
           this.codTpt = codTpt;
           this.datAlt = datAlt;
           this.datCco = datCco;
           this.datLib = datLib;
           this.datMov = datMov;
           this.datPgt = datPgt;
           this.diaAtr = diaAtr;
           this.diaJrs = diaJrs;
           this.filOri = filOri;
           this.horAlt = horAlt;
           this.numCco = numCco;
           this.numDoc = numDoc;
           this.numPrj = numPrj;
           this.numTit = numTit;
           this.segAlt = segAlt;
           this.seqCco = seqCco;
           this.seqInt = seqInt;
           this.seqMov = seqMov;
           this.sitReg = sitReg;
           this.tipBai = tipBai;
           this.vlrCor = vlrCor;
           this.vlrDsc = vlrDsc;
           this.vlrEnc = vlrEnc;
           this.vlrJrs = vlrJrs;
           this.vlrLiq = vlrLiq;
           this.vlrMov = vlrMov;
           this.vlrMul = vlrMul;
           this.vlrOac = vlrOac;
           this.vlrOde = vlrOde;
    }


    /**
     * Gets the bxaCpt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return bxaCpt
     */
    public java.lang.String getBxaCpt() {
        return bxaCpt;
    }


    /**
     * Sets the bxaCpt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param bxaCpt
     */
    public void setBxaCpt(java.lang.String bxaCpt) {
        this.bxaCpt = bxaCpt;
    }


    /**
     * Gets the codEmp value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the codFpg value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return codFpg
     */
    public java.lang.Integer getCodFpg() {
        return codFpg;
    }


    /**
     * Sets the codFpg value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param codFpg
     */
    public void setCodFpg(java.lang.Integer codFpg) {
        this.codFpg = codFpg;
    }


    /**
     * Gets the codFpj value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return codFpj
     */
    public java.lang.Integer getCodFpj() {
        return codFpj;
    }


    /**
     * Sets the codFpj value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param codFpj
     */
    public void setCodFpj(java.lang.Integer codFpj) {
        this.codFpj = codFpj;
    }


    /**
     * Gets the codTpt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return codTpt
     */
    public java.lang.String getCodTpt() {
        return codTpt;
    }


    /**
     * Sets the codTpt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param codTpt
     */
    public void setCodTpt(java.lang.String codTpt) {
        this.codTpt = codTpt;
    }


    /**
     * Gets the datAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return datAlt
     */
    public java.lang.String getDatAlt() {
        return datAlt;
    }


    /**
     * Sets the datAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param datAlt
     */
    public void setDatAlt(java.lang.String datAlt) {
        this.datAlt = datAlt;
    }


    /**
     * Gets the datCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return datCco
     */
    public java.lang.String getDatCco() {
        return datCco;
    }


    /**
     * Sets the datCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param datCco
     */
    public void setDatCco(java.lang.String datCco) {
        this.datCco = datCco;
    }


    /**
     * Gets the datLib value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return datLib
     */
    public java.lang.String getDatLib() {
        return datLib;
    }


    /**
     * Sets the datLib value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param datLib
     */
    public void setDatLib(java.lang.String datLib) {
        this.datLib = datLib;
    }


    /**
     * Gets the datMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return datMov
     */
    public java.lang.String getDatMov() {
        return datMov;
    }


    /**
     * Sets the datMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param datMov
     */
    public void setDatMov(java.lang.String datMov) {
        this.datMov = datMov;
    }


    /**
     * Gets the datPgt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return datPgt
     */
    public java.lang.String getDatPgt() {
        return datPgt;
    }


    /**
     * Sets the datPgt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param datPgt
     */
    public void setDatPgt(java.lang.String datPgt) {
        this.datPgt = datPgt;
    }


    /**
     * Gets the diaAtr value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return diaAtr
     */
    public java.lang.Integer getDiaAtr() {
        return diaAtr;
    }


    /**
     * Sets the diaAtr value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param diaAtr
     */
    public void setDiaAtr(java.lang.Integer diaAtr) {
        this.diaAtr = diaAtr;
    }


    /**
     * Gets the diaJrs value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return diaJrs
     */
    public java.lang.Integer getDiaJrs() {
        return diaJrs;
    }


    /**
     * Sets the diaJrs value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param diaJrs
     */
    public void setDiaJrs(java.lang.Integer diaJrs) {
        this.diaJrs = diaJrs;
    }


    /**
     * Gets the filOri value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return filOri
     */
    public java.lang.Integer getFilOri() {
        return filOri;
    }


    /**
     * Sets the filOri value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param filOri
     */
    public void setFilOri(java.lang.Integer filOri) {
        this.filOri = filOri;
    }


    /**
     * Gets the horAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return horAlt
     */
    public java.lang.String getHorAlt() {
        return horAlt;
    }


    /**
     * Sets the horAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param horAlt
     */
    public void setHorAlt(java.lang.String horAlt) {
        this.horAlt = horAlt;
    }


    /**
     * Gets the numCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return numCco
     */
    public java.lang.String getNumCco() {
        return numCco;
    }


    /**
     * Sets the numCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param numCco
     */
    public void setNumCco(java.lang.String numCco) {
        this.numCco = numCco;
    }


    /**
     * Gets the numDoc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return numDoc
     */
    public java.lang.String getNumDoc() {
        return numDoc;
    }


    /**
     * Sets the numDoc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param numDoc
     */
    public void setNumDoc(java.lang.String numDoc) {
        this.numDoc = numDoc;
    }


    /**
     * Gets the numPrj value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return numPrj
     */
    public java.lang.Integer getNumPrj() {
        return numPrj;
    }


    /**
     * Sets the numPrj value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param numPrj
     */
    public void setNumPrj(java.lang.Integer numPrj) {
        this.numPrj = numPrj;
    }


    /**
     * Gets the numTit value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return numTit
     */
    public java.lang.String getNumTit() {
        return numTit;
    }


    /**
     * Sets the numTit value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param numTit
     */
    public void setNumTit(java.lang.String numTit) {
        this.numTit = numTit;
    }


    /**
     * Gets the segAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return segAlt
     */
    public java.lang.String getSegAlt() {
        return segAlt;
    }


    /**
     * Sets the segAlt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param segAlt
     */
    public void setSegAlt(java.lang.String segAlt) {
        this.segAlt = segAlt;
    }


    /**
     * Gets the seqCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return seqCco
     */
    public java.lang.Integer getSeqCco() {
        return seqCco;
    }


    /**
     * Sets the seqCco value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param seqCco
     */
    public void setSeqCco(java.lang.Integer seqCco) {
        this.seqCco = seqCco;
    }


    /**
     * Gets the seqInt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return seqInt
     */
    public java.lang.Integer getSeqInt() {
        return seqInt;
    }


    /**
     * Sets the seqInt value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param seqInt
     */
    public void setSeqInt(java.lang.Integer seqInt) {
        this.seqInt = seqInt;
    }


    /**
     * Gets the seqMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return seqMov
     */
    public java.lang.Integer getSeqMov() {
        return seqMov;
    }


    /**
     * Sets the seqMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param seqMov
     */
    public void setSeqMov(java.lang.Integer seqMov) {
        this.seqMov = seqMov;
    }


    /**
     * Gets the sitReg value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return sitReg
     */
    public java.lang.String getSitReg() {
        return sitReg;
    }


    /**
     * Sets the sitReg value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param sitReg
     */
    public void setSitReg(java.lang.String sitReg) {
        this.sitReg = sitReg;
    }


    /**
     * Gets the tipBai value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return tipBai
     */
    public java.lang.String getTipBai() {
        return tipBai;
    }


    /**
     * Sets the tipBai value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param tipBai
     */
    public void setTipBai(java.lang.String tipBai) {
        this.tipBai = tipBai;
    }


    /**
     * Gets the vlrCor value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrCor
     */
    public java.lang.Double getVlrCor() {
        return vlrCor;
    }


    /**
     * Sets the vlrCor value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrCor
     */
    public void setVlrCor(java.lang.Double vlrCor) {
        this.vlrCor = vlrCor;
    }


    /**
     * Gets the vlrDsc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrDsc
     */
    public java.lang.Double getVlrDsc() {
        return vlrDsc;
    }


    /**
     * Sets the vlrDsc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrDsc
     */
    public void setVlrDsc(java.lang.Double vlrDsc) {
        this.vlrDsc = vlrDsc;
    }


    /**
     * Gets the vlrEnc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrEnc
     */
    public java.lang.Double getVlrEnc() {
        return vlrEnc;
    }


    /**
     * Sets the vlrEnc value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrEnc
     */
    public void setVlrEnc(java.lang.Double vlrEnc) {
        this.vlrEnc = vlrEnc;
    }


    /**
     * Gets the vlrJrs value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrJrs
     */
    public java.lang.Double getVlrJrs() {
        return vlrJrs;
    }


    /**
     * Sets the vlrJrs value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrJrs
     */
    public void setVlrJrs(java.lang.Double vlrJrs) {
        this.vlrJrs = vlrJrs;
    }


    /**
     * Gets the vlrLiq value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrLiq
     */
    public java.lang.Double getVlrLiq() {
        return vlrLiq;
    }


    /**
     * Sets the vlrLiq value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrLiq
     */
    public void setVlrLiq(java.lang.Double vlrLiq) {
        this.vlrLiq = vlrLiq;
    }


    /**
     * Gets the vlrMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrMov
     */
    public java.lang.Double getVlrMov() {
        return vlrMov;
    }


    /**
     * Sets the vlrMov value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrMov
     */
    public void setVlrMov(java.lang.Double vlrMov) {
        this.vlrMov = vlrMov;
    }


    /**
     * Gets the vlrMul value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrMul
     */
    public java.lang.Double getVlrMul() {
        return vlrMul;
    }


    /**
     * Sets the vlrMul value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrMul
     */
    public void setVlrMul(java.lang.Double vlrMul) {
        this.vlrMul = vlrMul;
    }


    /**
     * Gets the vlrOac value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrOac
     */
    public java.lang.Double getVlrOac() {
        return vlrOac;
    }


    /**
     * Sets the vlrOac value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrOac
     */
    public void setVlrOac(java.lang.Double vlrOac) {
        this.vlrOac = vlrOac;
    }


    /**
     * Gets the vlrOde value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @return vlrOde
     */
    public java.lang.Double getVlrOde() {
        return vlrOde;
    }


    /**
     * Sets the vlrOde value for this TitulosExportarBaixaTitulosReceberOutBaixaTitulos.
     * 
     * @param vlrOde
     */
    public void setVlrOde(java.lang.Double vlrOde) {
        this.vlrOde = vlrOde;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExportarBaixaTitulosReceberOutBaixaTitulos)) return false;
        TitulosExportarBaixaTitulosReceberOutBaixaTitulos other = (TitulosExportarBaixaTitulosReceberOutBaixaTitulos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bxaCpt==null && other.getBxaCpt()==null) || 
             (this.bxaCpt!=null &&
              this.bxaCpt.equals(other.getBxaCpt()))) &&
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.codFpg==null && other.getCodFpg()==null) || 
             (this.codFpg!=null &&
              this.codFpg.equals(other.getCodFpg()))) &&
            ((this.codFpj==null && other.getCodFpj()==null) || 
             (this.codFpj!=null &&
              this.codFpj.equals(other.getCodFpj()))) &&
            ((this.codTpt==null && other.getCodTpt()==null) || 
             (this.codTpt!=null &&
              this.codTpt.equals(other.getCodTpt()))) &&
            ((this.datAlt==null && other.getDatAlt()==null) || 
             (this.datAlt!=null &&
              this.datAlt.equals(other.getDatAlt()))) &&
            ((this.datCco==null && other.getDatCco()==null) || 
             (this.datCco!=null &&
              this.datCco.equals(other.getDatCco()))) &&
            ((this.datLib==null && other.getDatLib()==null) || 
             (this.datLib!=null &&
              this.datLib.equals(other.getDatLib()))) &&
            ((this.datMov==null && other.getDatMov()==null) || 
             (this.datMov!=null &&
              this.datMov.equals(other.getDatMov()))) &&
            ((this.datPgt==null && other.getDatPgt()==null) || 
             (this.datPgt!=null &&
              this.datPgt.equals(other.getDatPgt()))) &&
            ((this.diaAtr==null && other.getDiaAtr()==null) || 
             (this.diaAtr!=null &&
              this.diaAtr.equals(other.getDiaAtr()))) &&
            ((this.diaJrs==null && other.getDiaJrs()==null) || 
             (this.diaJrs!=null &&
              this.diaJrs.equals(other.getDiaJrs()))) &&
            ((this.filOri==null && other.getFilOri()==null) || 
             (this.filOri!=null &&
              this.filOri.equals(other.getFilOri()))) &&
            ((this.horAlt==null && other.getHorAlt()==null) || 
             (this.horAlt!=null &&
              this.horAlt.equals(other.getHorAlt()))) &&
            ((this.numCco==null && other.getNumCco()==null) || 
             (this.numCco!=null &&
              this.numCco.equals(other.getNumCco()))) &&
            ((this.numDoc==null && other.getNumDoc()==null) || 
             (this.numDoc!=null &&
              this.numDoc.equals(other.getNumDoc()))) &&
            ((this.numPrj==null && other.getNumPrj()==null) || 
             (this.numPrj!=null &&
              this.numPrj.equals(other.getNumPrj()))) &&
            ((this.numTit==null && other.getNumTit()==null) || 
             (this.numTit!=null &&
              this.numTit.equals(other.getNumTit()))) &&
            ((this.segAlt==null && other.getSegAlt()==null) || 
             (this.segAlt!=null &&
              this.segAlt.equals(other.getSegAlt()))) &&
            ((this.seqCco==null && other.getSeqCco()==null) || 
             (this.seqCco!=null &&
              this.seqCco.equals(other.getSeqCco()))) &&
            ((this.seqInt==null && other.getSeqInt()==null) || 
             (this.seqInt!=null &&
              this.seqInt.equals(other.getSeqInt()))) &&
            ((this.seqMov==null && other.getSeqMov()==null) || 
             (this.seqMov!=null &&
              this.seqMov.equals(other.getSeqMov()))) &&
            ((this.sitReg==null && other.getSitReg()==null) || 
             (this.sitReg!=null &&
              this.sitReg.equals(other.getSitReg()))) &&
            ((this.tipBai==null && other.getTipBai()==null) || 
             (this.tipBai!=null &&
              this.tipBai.equals(other.getTipBai()))) &&
            ((this.vlrCor==null && other.getVlrCor()==null) || 
             (this.vlrCor!=null &&
              this.vlrCor.equals(other.getVlrCor()))) &&
            ((this.vlrDsc==null && other.getVlrDsc()==null) || 
             (this.vlrDsc!=null &&
              this.vlrDsc.equals(other.getVlrDsc()))) &&
            ((this.vlrEnc==null && other.getVlrEnc()==null) || 
             (this.vlrEnc!=null &&
              this.vlrEnc.equals(other.getVlrEnc()))) &&
            ((this.vlrJrs==null && other.getVlrJrs()==null) || 
             (this.vlrJrs!=null &&
              this.vlrJrs.equals(other.getVlrJrs()))) &&
            ((this.vlrLiq==null && other.getVlrLiq()==null) || 
             (this.vlrLiq!=null &&
              this.vlrLiq.equals(other.getVlrLiq()))) &&
            ((this.vlrMov==null && other.getVlrMov()==null) || 
             (this.vlrMov!=null &&
              this.vlrMov.equals(other.getVlrMov()))) &&
            ((this.vlrMul==null && other.getVlrMul()==null) || 
             (this.vlrMul!=null &&
              this.vlrMul.equals(other.getVlrMul()))) &&
            ((this.vlrOac==null && other.getVlrOac()==null) || 
             (this.vlrOac!=null &&
              this.vlrOac.equals(other.getVlrOac()))) &&
            ((this.vlrOde==null && other.getVlrOde()==null) || 
             (this.vlrOde!=null &&
              this.vlrOde.equals(other.getVlrOde())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBxaCpt() != null) {
            _hashCode += getBxaCpt().hashCode();
        }
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getCodFpg() != null) {
            _hashCode += getCodFpg().hashCode();
        }
        if (getCodFpj() != null) {
            _hashCode += getCodFpj().hashCode();
        }
        if (getCodTpt() != null) {
            _hashCode += getCodTpt().hashCode();
        }
        if (getDatAlt() != null) {
            _hashCode += getDatAlt().hashCode();
        }
        if (getDatCco() != null) {
            _hashCode += getDatCco().hashCode();
        }
        if (getDatLib() != null) {
            _hashCode += getDatLib().hashCode();
        }
        if (getDatMov() != null) {
            _hashCode += getDatMov().hashCode();
        }
        if (getDatPgt() != null) {
            _hashCode += getDatPgt().hashCode();
        }
        if (getDiaAtr() != null) {
            _hashCode += getDiaAtr().hashCode();
        }
        if (getDiaJrs() != null) {
            _hashCode += getDiaJrs().hashCode();
        }
        if (getFilOri() != null) {
            _hashCode += getFilOri().hashCode();
        }
        if (getHorAlt() != null) {
            _hashCode += getHorAlt().hashCode();
        }
        if (getNumCco() != null) {
            _hashCode += getNumCco().hashCode();
        }
        if (getNumDoc() != null) {
            _hashCode += getNumDoc().hashCode();
        }
        if (getNumPrj() != null) {
            _hashCode += getNumPrj().hashCode();
        }
        if (getNumTit() != null) {
            _hashCode += getNumTit().hashCode();
        }
        if (getSegAlt() != null) {
            _hashCode += getSegAlt().hashCode();
        }
        if (getSeqCco() != null) {
            _hashCode += getSeqCco().hashCode();
        }
        if (getSeqInt() != null) {
            _hashCode += getSeqInt().hashCode();
        }
        if (getSeqMov() != null) {
            _hashCode += getSeqMov().hashCode();
        }
        if (getSitReg() != null) {
            _hashCode += getSitReg().hashCode();
        }
        if (getTipBai() != null) {
            _hashCode += getTipBai().hashCode();
        }
        if (getVlrCor() != null) {
            _hashCode += getVlrCor().hashCode();
        }
        if (getVlrDsc() != null) {
            _hashCode += getVlrDsc().hashCode();
        }
        if (getVlrEnc() != null) {
            _hashCode += getVlrEnc().hashCode();
        }
        if (getVlrJrs() != null) {
            _hashCode += getVlrJrs().hashCode();
        }
        if (getVlrLiq() != null) {
            _hashCode += getVlrLiq().hashCode();
        }
        if (getVlrMov() != null) {
            _hashCode += getVlrMov().hashCode();
        }
        if (getVlrMul() != null) {
            _hashCode += getVlrMul().hashCode();
        }
        if (getVlrOac() != null) {
            _hashCode += getVlrOac().hashCode();
        }
        if (getVlrOde() != null) {
            _hashCode += getVlrOde().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExportarBaixaTitulosReceberOutBaixaTitulos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarBaixaTitulosReceberOutBaixaTitulos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bxaCpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bxaCpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFpj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFpj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codTpt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codTpt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datLib");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datLib"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datPgt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datPgt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaAtr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaAtr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diaJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diaJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filOri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filOri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("horAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "horAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPrj");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPrj"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numTit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numTit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("segAlt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "segAlt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqCco");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqCco"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqInt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqInt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seqMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seqMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sitReg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sitReg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrCor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrCor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrDsc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrDsc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrEnc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrEnc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrJrs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrJrs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrLiq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrLiq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrMul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrMul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vlrOde");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vlrOde"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
