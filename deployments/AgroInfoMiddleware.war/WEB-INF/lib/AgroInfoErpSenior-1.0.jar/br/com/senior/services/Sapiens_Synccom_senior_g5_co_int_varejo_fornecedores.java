/**
 * Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public interface Sapiens_Synccom_senior_g5_co_int_varejo_fornecedores extends java.rmi.Remote {
    public br.com.senior.services.FornecedoresExportarOut exportar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresExportarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.FornecedoresImportarOut importar(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresImportarIn parameters) throws java.rmi.RemoteException;
    public br.com.senior.services.FornecedoresExportar2Out exportar_2(java.lang.String user, java.lang.String password, int encryption, br.com.senior.services.FornecedoresExportar2In parameters) throws java.rmi.RemoteException;
}
