/**
 * TitulosExportarTitulosReceber3Out.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosExportarTitulosReceber3Out  implements java.io.Serializable {
    private java.lang.String erroExecucao;

    private br.com.senior.services.TitulosExportarTitulosReceber3OutErros[] erros;

    private java.lang.String finalizaramRegistros;

    private java.lang.String mensagemRetorno;

    private java.lang.Integer numeroLote;

    private java.lang.Integer tipoRetorno;

    private br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber[] titulosReceber;

    public TitulosExportarTitulosReceber3Out() {
    }

    public TitulosExportarTitulosReceber3Out(
           java.lang.String erroExecucao,
           br.com.senior.services.TitulosExportarTitulosReceber3OutErros[] erros,
           java.lang.String finalizaramRegistros,
           java.lang.String mensagemRetorno,
           java.lang.Integer numeroLote,
           java.lang.Integer tipoRetorno,
           br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber[] titulosReceber) {
           this.erroExecucao = erroExecucao;
           this.erros = erros;
           this.finalizaramRegistros = finalizaramRegistros;
           this.mensagemRetorno = mensagemRetorno;
           this.numeroLote = numeroLote;
           this.tipoRetorno = tipoRetorno;
           this.titulosReceber = titulosReceber;
    }


    /**
     * Gets the erroExecucao value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return erroExecucao
     */
    public java.lang.String getErroExecucao() {
        return erroExecucao;
    }


    /**
     * Sets the erroExecucao value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param erroExecucao
     */
    public void setErroExecucao(java.lang.String erroExecucao) {
        this.erroExecucao = erroExecucao;
    }


    /**
     * Gets the erros value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return erros
     */
    public br.com.senior.services.TitulosExportarTitulosReceber3OutErros[] getErros() {
        return erros;
    }


    /**
     * Sets the erros value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param erros
     */
    public void setErros(br.com.senior.services.TitulosExportarTitulosReceber3OutErros[] erros) {
        this.erros = erros;
    }

    public br.com.senior.services.TitulosExportarTitulosReceber3OutErros getErros(int i) {
        return this.erros[i];
    }

    public void setErros(int i, br.com.senior.services.TitulosExportarTitulosReceber3OutErros _value) {
        this.erros[i] = _value;
    }


    /**
     * Gets the finalizaramRegistros value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return finalizaramRegistros
     */
    public java.lang.String getFinalizaramRegistros() {
        return finalizaramRegistros;
    }


    /**
     * Sets the finalizaramRegistros value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param finalizaramRegistros
     */
    public void setFinalizaramRegistros(java.lang.String finalizaramRegistros) {
        this.finalizaramRegistros = finalizaramRegistros;
    }


    /**
     * Gets the mensagemRetorno value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return mensagemRetorno
     */
    public java.lang.String getMensagemRetorno() {
        return mensagemRetorno;
    }


    /**
     * Sets the mensagemRetorno value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param mensagemRetorno
     */
    public void setMensagemRetorno(java.lang.String mensagemRetorno) {
        this.mensagemRetorno = mensagemRetorno;
    }


    /**
     * Gets the numeroLote value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return numeroLote
     */
    public java.lang.Integer getNumeroLote() {
        return numeroLote;
    }


    /**
     * Sets the numeroLote value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param numeroLote
     */
    public void setNumeroLote(java.lang.Integer numeroLote) {
        this.numeroLote = numeroLote;
    }


    /**
     * Gets the tipoRetorno value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return tipoRetorno
     */
    public java.lang.Integer getTipoRetorno() {
        return tipoRetorno;
    }


    /**
     * Sets the tipoRetorno value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param tipoRetorno
     */
    public void setTipoRetorno(java.lang.Integer tipoRetorno) {
        this.tipoRetorno = tipoRetorno;
    }


    /**
     * Gets the titulosReceber value for this TitulosExportarTitulosReceber3Out.
     * 
     * @return titulosReceber
     */
    public br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber[] getTitulosReceber() {
        return titulosReceber;
    }


    /**
     * Sets the titulosReceber value for this TitulosExportarTitulosReceber3Out.
     * 
     * @param titulosReceber
     */
    public void setTitulosReceber(br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber[] titulosReceber) {
        this.titulosReceber = titulosReceber;
    }

    public br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber getTitulosReceber(int i) {
        return this.titulosReceber[i];
    }

    public void setTitulosReceber(int i, br.com.senior.services.TitulosExportarTitulosReceber3OutTitulosReceber _value) {
        this.titulosReceber[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosExportarTitulosReceber3Out)) return false;
        TitulosExportarTitulosReceber3Out other = (TitulosExportarTitulosReceber3Out) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.erroExecucao==null && other.getErroExecucao()==null) || 
             (this.erroExecucao!=null &&
              this.erroExecucao.equals(other.getErroExecucao()))) &&
            ((this.erros==null && other.getErros()==null) || 
             (this.erros!=null &&
              java.util.Arrays.equals(this.erros, other.getErros()))) &&
            ((this.finalizaramRegistros==null && other.getFinalizaramRegistros()==null) || 
             (this.finalizaramRegistros!=null &&
              this.finalizaramRegistros.equals(other.getFinalizaramRegistros()))) &&
            ((this.mensagemRetorno==null && other.getMensagemRetorno()==null) || 
             (this.mensagemRetorno!=null &&
              this.mensagemRetorno.equals(other.getMensagemRetorno()))) &&
            ((this.numeroLote==null && other.getNumeroLote()==null) || 
             (this.numeroLote!=null &&
              this.numeroLote.equals(other.getNumeroLote()))) &&
            ((this.tipoRetorno==null && other.getTipoRetorno()==null) || 
             (this.tipoRetorno!=null &&
              this.tipoRetorno.equals(other.getTipoRetorno()))) &&
            ((this.titulosReceber==null && other.getTitulosReceber()==null) || 
             (this.titulosReceber!=null &&
              java.util.Arrays.equals(this.titulosReceber, other.getTitulosReceber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErroExecucao() != null) {
            _hashCode += getErroExecucao().hashCode();
        }
        if (getErros() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErros());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErros(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFinalizaramRegistros() != null) {
            _hashCode += getFinalizaramRegistros().hashCode();
        }
        if (getMensagemRetorno() != null) {
            _hashCode += getMensagemRetorno().hashCode();
        }
        if (getNumeroLote() != null) {
            _hashCode += getNumeroLote().hashCode();
        }
        if (getTipoRetorno() != null) {
            _hashCode += getTipoRetorno().hashCode();
        }
        if (getTitulosReceber() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosReceber());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosReceber(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosExportarTitulosReceber3Out.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarTitulosReceber3Out"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erroExecucao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erroExecucao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("erros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "erros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarTitulosReceber3OutErros"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finalizaramRegistros");
        elemField.setXmlName(new javax.xml.namespace.QName("", "finalizaramRegistros"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensagemRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mensagemRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroLote");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numeroLote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoRetorno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipoRetorno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosReceber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosReceber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosExportarTitulosReceber3OutTitulosReceber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
