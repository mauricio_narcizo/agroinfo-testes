/**
 * TitulosSubstituirTitulosCPVarejoInSubstituicao.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.senior.services;

public class TitulosSubstituirTitulosCPVarejoInSubstituicao  implements java.io.Serializable {
    private java.lang.Integer codEmp;

    private java.lang.Integer codFil;

    private java.lang.String datBai;

    private java.lang.Integer ideExt;

    private java.lang.Integer numPdv;

    private br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar[] titulosBaixar;

    private br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos[] titulosSubstitutos;

    public TitulosSubstituirTitulosCPVarejoInSubstituicao() {
    }

    public TitulosSubstituirTitulosCPVarejoInSubstituicao(
           java.lang.Integer codEmp,
           java.lang.Integer codFil,
           java.lang.String datBai,
           java.lang.Integer ideExt,
           java.lang.Integer numPdv,
           br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar[] titulosBaixar,
           br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos[] titulosSubstitutos) {
           this.codEmp = codEmp;
           this.codFil = codFil;
           this.datBai = datBai;
           this.ideExt = ideExt;
           this.numPdv = numPdv;
           this.titulosBaixar = titulosBaixar;
           this.titulosSubstitutos = titulosSubstitutos;
    }


    /**
     * Gets the codEmp value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return codEmp
     */
    public java.lang.Integer getCodEmp() {
        return codEmp;
    }


    /**
     * Sets the codEmp value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param codEmp
     */
    public void setCodEmp(java.lang.Integer codEmp) {
        this.codEmp = codEmp;
    }


    /**
     * Gets the codFil value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return codFil
     */
    public java.lang.Integer getCodFil() {
        return codFil;
    }


    /**
     * Sets the codFil value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param codFil
     */
    public void setCodFil(java.lang.Integer codFil) {
        this.codFil = codFil;
    }


    /**
     * Gets the datBai value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return datBai
     */
    public java.lang.String getDatBai() {
        return datBai;
    }


    /**
     * Sets the datBai value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param datBai
     */
    public void setDatBai(java.lang.String datBai) {
        this.datBai = datBai;
    }


    /**
     * Gets the ideExt value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return ideExt
     */
    public java.lang.Integer getIdeExt() {
        return ideExt;
    }


    /**
     * Sets the ideExt value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param ideExt
     */
    public void setIdeExt(java.lang.Integer ideExt) {
        this.ideExt = ideExt;
    }


    /**
     * Gets the numPdv value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return numPdv
     */
    public java.lang.Integer getNumPdv() {
        return numPdv;
    }


    /**
     * Sets the numPdv value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param numPdv
     */
    public void setNumPdv(java.lang.Integer numPdv) {
        this.numPdv = numPdv;
    }


    /**
     * Gets the titulosBaixar value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return titulosBaixar
     */
    public br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar[] getTitulosBaixar() {
        return titulosBaixar;
    }


    /**
     * Sets the titulosBaixar value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param titulosBaixar
     */
    public void setTitulosBaixar(br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar[] titulosBaixar) {
        this.titulosBaixar = titulosBaixar;
    }

    public br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar getTitulosBaixar(int i) {
        return this.titulosBaixar[i];
    }

    public void setTitulosBaixar(int i, br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar _value) {
        this.titulosBaixar[i] = _value;
    }


    /**
     * Gets the titulosSubstitutos value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @return titulosSubstitutos
     */
    public br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos[] getTitulosSubstitutos() {
        return titulosSubstitutos;
    }


    /**
     * Sets the titulosSubstitutos value for this TitulosSubstituirTitulosCPVarejoInSubstituicao.
     * 
     * @param titulosSubstitutos
     */
    public void setTitulosSubstitutos(br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos[] titulosSubstitutos) {
        this.titulosSubstitutos = titulosSubstitutos;
    }

    public br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos getTitulosSubstitutos(int i) {
        return this.titulosSubstitutos[i];
    }

    public void setTitulosSubstitutos(int i, br.com.senior.services.TitulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos _value) {
        this.titulosSubstitutos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TitulosSubstituirTitulosCPVarejoInSubstituicao)) return false;
        TitulosSubstituirTitulosCPVarejoInSubstituicao other = (TitulosSubstituirTitulosCPVarejoInSubstituicao) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codEmp==null && other.getCodEmp()==null) || 
             (this.codEmp!=null &&
              this.codEmp.equals(other.getCodEmp()))) &&
            ((this.codFil==null && other.getCodFil()==null) || 
             (this.codFil!=null &&
              this.codFil.equals(other.getCodFil()))) &&
            ((this.datBai==null && other.getDatBai()==null) || 
             (this.datBai!=null &&
              this.datBai.equals(other.getDatBai()))) &&
            ((this.ideExt==null && other.getIdeExt()==null) || 
             (this.ideExt!=null &&
              this.ideExt.equals(other.getIdeExt()))) &&
            ((this.numPdv==null && other.getNumPdv()==null) || 
             (this.numPdv!=null &&
              this.numPdv.equals(other.getNumPdv()))) &&
            ((this.titulosBaixar==null && other.getTitulosBaixar()==null) || 
             (this.titulosBaixar!=null &&
              java.util.Arrays.equals(this.titulosBaixar, other.getTitulosBaixar()))) &&
            ((this.titulosSubstitutos==null && other.getTitulosSubstitutos()==null) || 
             (this.titulosSubstitutos!=null &&
              java.util.Arrays.equals(this.titulosSubstitutos, other.getTitulosSubstitutos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodEmp() != null) {
            _hashCode += getCodEmp().hashCode();
        }
        if (getCodFil() != null) {
            _hashCode += getCodFil().hashCode();
        }
        if (getDatBai() != null) {
            _hashCode += getDatBai().hashCode();
        }
        if (getIdeExt() != null) {
            _hashCode += getIdeExt().hashCode();
        }
        if (getNumPdv() != null) {
            _hashCode += getNumPdv().hashCode();
        }
        if (getTitulosBaixar() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosBaixar());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosBaixar(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTitulosSubstitutos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTitulosSubstitutos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTitulosSubstitutos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TitulosSubstituirTitulosCPVarejoInSubstituicao.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPVarejoInSubstituicao"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codEmp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codEmp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFil");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codFil"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datBai");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datBai"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ideExt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ideExt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPdv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPdv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosBaixar");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosBaixar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPVarejoInSubstituicaoTitulosBaixar"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulosSubstitutos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulosSubstitutos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.senior.com.br", "titulosSubstituirTitulosCPVarejoInSubstituicaoTitulosSubstitutos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
