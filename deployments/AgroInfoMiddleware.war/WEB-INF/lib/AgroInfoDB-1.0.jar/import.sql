--INSERT INTO agendamento(id,ativo, cargacompleta, dtupdate, executando, grupo, periodicidade, tempocargacompleta, tempoultimaexecucao, tipoagendamento, tipocargacompleta, tipoperiodicidade) VALUES (1, true, null, now(), false, null, 1, 2, 0, 'BUSCASMS', 'SEMANAS', 'SEMANAS');
--INSERT INTO agendamento(id,ativo, cargacompleta, dtupdate, executando, grupo, periodicidade, tempocargacompleta, tempoultimaexecucao, tipoagendamento, tipocargacompleta, tipoperiodicidade) VALUES (2, true, 2, now(), false, null, 1, 2, 0, 'COOPERADO', 'MINUTOS', 'MINUTOS');
--INSERT INTO agendamento(id,ativo, cargacompleta, dtupdate, executando, grupo, periodicidade, tempocargacompleta, tempoultimaexecucao, tipoagendamento, tipocargacompleta, tipoperiodicidade) VALUES (3, true, 2, now(), false, null, 1, 2, 0, 'CONTAS_PAGAR', 'MINUTOS', 'MINUTOS');
--INSERT INTO agendamento(id,ativo, cargacompleta, dtupdate, executando, grupo, periodicidade, tempocargacompleta, tempoultimaexecucao, tipoagendamento, tipocargacompleta, tipoperiodicidade) VALUES (4, true, 2, now(), false, null, 1, 2, 0, 'CONTAS_RECEBER', 'MINUTOS', 'MINUTOS');
INSERT INTO empresafilial(id, ativa, codigoempresa, dddpadrao, dtupdate, nomeempresa, nomefilial) VALUES (1, true, 1, '047', null, 'Senior Sistemad', 'AgroInfo');
--INSERT INTO empresafilial(id, ativa, codigoempresa, dddpadrao, dtupdate, nomeempresa, nomefilial) VALUES (2, true, 0, '047', null, 'Senior Sistemad', 'AgroInfo 2');
--INSERT INTO empresafilial(id, ativa, codigoempresa, dddpadrao, dtupdate, nomeempresa, nomefilial) VALUES (3, true, 0, '047', null, 'Senior Sistemad', 'AgroInfo 3');
--INSERT INTO empresafilial(id, ativa, codigoempresa, dddpadrao, dtupdate, nomeempresa, nomefilial) VALUES (4, true, 1, '047', null, 'Teste Sistemas', 'Teste 1');
--INSERT INTO empresafilial(id, ativa, codigoempresa, dddpadrao, dtupdate, nomeempresa, nomefilial) VALUES (5, true, 1, '047', null, 'Teste Sistemas', 'Teste 2');   
--INSERT INTO cooperado(id, ativo, atualizar, bairro, cep, cidade, codibge, cpfcnpj,dtupdate, email, endereco, nome, nomefantasia, numero) VALUES (1, true, true, '', '12345678', '', 1, '21598745698', NOW(), '', '', 'teste', 'zandonai fetiche', 1);
--INSERT INTO configuracoes( id, dtupdate, endereco, ipsms, porta, portasms, senha, senhasms, sigla, tiposistema, tiposms, usuario, usuariosms, utilizasms) VALUES (1, now(), 'http://teste33', 'http://10.1.44.90', '8080', '1688', 'suporte', null, 'VAREJOEM', 'SENIOR_ERP_587', NULL, 'suporte', null,false);
--INSERT INTO empresafilial( codigofilial, ativa, codigoempresa, dtupdate, nomeempresa, nomefilial,dddpadrao) VALUES (1, true, 1, now(), 'VAREJOEM', 'VAREJOEM','47');

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (1, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João1', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (2, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João2', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (3, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João3', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (4, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João4', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (5, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João5', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (6, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João6', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (7, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João7', 'João Rodrigo', 15);


INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (8, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João8', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (9, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João9', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (10, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João10', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (11, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João11', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (12, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João12', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (13, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João13', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (14, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João14', 'João Rodrigo', 15);                                                                        


--------------------------------------------------------------------------------------------------------

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (25, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João25', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (26, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João26', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (27, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João27', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (28, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João28', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (29, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João29', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (30, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João30', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (31, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João31', 'João Rodrigo', 15);


INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (32, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João32', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (33, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João33', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (34, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João34', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (35, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João35', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (36, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João36', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (37, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João37', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (38, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João38', 'João Rodrigo', 15);

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (41, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João41', 'João Rodrigo', 15);   

                        INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (51, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João51', 'João Rodrigo', 15);   

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (61, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João61', 'João Rodrigo', 15);   

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (71, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João71', 'João Rodrigo', 15);   

INSERT INTO cooperado(
            id, ativo, atualizar, bairro, cep, cidade, codibge, codigomatricula, 
            cpfcnpj, dtupdate, email, endereco, nome, nomefantasia, numero)
    VALUES (142, true, true, 'Testo Central', '89107000', 'Pomerode', '8911', '136542', 
            '123456', now(), 'joao@gmail.com', 'Rua XV de Novembro', 'João81', 'João Rodrigo', 15);              
