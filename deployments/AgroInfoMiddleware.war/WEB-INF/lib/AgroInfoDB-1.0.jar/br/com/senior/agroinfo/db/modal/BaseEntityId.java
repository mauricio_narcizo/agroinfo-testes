package br.com.senior.agroinfo.db.modal;

import java.io.Serializable;

public interface BaseEntityId extends Serializable{

	void setId(Long id);
	
	Long getId();	
	
}
