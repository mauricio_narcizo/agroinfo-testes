package br.com.senior.agroinfo.db.dao;

import br.com.senior.agroinfo.db.modal.QUsuario;
import br.com.senior.agroinfo.db.modal.Usuario;

public class UsuarioDAO extends GenericQueryDSLDAO<Usuario, QUsuario> {

/*
    public Usuario recuperaPorLogin(String login) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
        Root<Usuario> root = criteria.from(Usuario.class);
        Predicate predicate = builder.equal(root.get("login"), login);
        criteria.select(root).where(predicate);

        return em.createQuery(criteria).getSingleResult();
    }

*/

	@Override
	protected QUsuario getPath() {
		return QUsuario.usuario;
	}
}