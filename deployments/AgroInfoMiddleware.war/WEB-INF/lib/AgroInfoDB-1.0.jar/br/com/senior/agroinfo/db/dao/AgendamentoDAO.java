package br.com.senior.agroinfo.db.dao;

import javax.ejb.Stateless;

import br.com.senior.agroinfo.db.modal.Agendamento;
import br.com.senior.agroinfo.db.modal.QAgendamento;

@Stateless
public class AgendamentoDAO extends GenericQueryDSLDAO<Agendamento, QAgendamento> {

	
	@Override
	protected QAgendamento getPath() {
		return QAgendamento.agendamento;
	}	
	
}