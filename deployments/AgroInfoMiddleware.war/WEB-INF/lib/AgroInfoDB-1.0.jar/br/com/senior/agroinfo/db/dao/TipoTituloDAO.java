package br.com.senior.agroinfo.db.dao;

import java.util.Optional;

import javax.ejb.Stateless;

import com.querydsl.core.types.Predicate;

import br.com.senior.agroinfo.db.modal.QTipoTitulo;
import br.com.senior.agroinfo.db.modal.TipoTitulo;

@Stateless
public class TipoTituloDAO extends GenericQueryDSLDAO<TipoTitulo, QTipoTitulo> {

	@Override
	public TipoTitulo adiciona(TipoTitulo entity) {
		Optional<TipoTitulo> busca = buscarObjeto(
											Optional.ofNullable(
													new Predicate[]{QTipoTitulo.tipoTitulo.codTipoTitulo.eq(entity.getCodTipoTitulo())}
											)
									);
		if (busca.isPresent()) {
			entity.setId(busca.get().getId());
			return super.atualiza(entity);
		}
		return super.adiciona(entity);
	}

	@Override
	protected QTipoTitulo getPath() {
		return QTipoTitulo.tipoTitulo;
	}

}