package br.com.senior.agroinfo.db.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.com.senior.agroinfo.db.modal.Mensagem;
import br.com.senior.agroinfo.db.modal.QCategoriaMensagem;
import br.com.senior.agroinfo.db.modal.QCooperado;
import br.com.senior.agroinfo.db.modal.QGrupo;
import br.com.senior.agroinfo.db.modal.QMensagem;
import br.com.senior.agroinfo.db.modal.QMensagemDestinatario;

@Stateless
public class MensagemDAO extends GenericQueryDSLDAO<Mensagem, QMensagem> {
	
	public List<Mensagem> buscaPaginada(int offset, int limit) {
		
		JPAQuery<Mensagem> query = new JPAQueryFactory(em)
											.select(QMensagem.mensagem)
											.distinct()
											.from(QMensagem.mensagem)											
											.join(QMensagem.mensagem.categoria, QCategoriaMensagem.categoriaMensagem).fetchJoin()
											.join(QMensagem.mensagem.destinatarios, QMensagemDestinatario.mensagemDestinatario).fetchJoin()
											.leftJoin(QMensagemDestinatario.mensagemDestinatario.cooperado, QCooperado.cooperado).fetchJoin()
											.leftJoin(QMensagemDestinatario.mensagemDestinatario.grupo, QGrupo.grupo).fetchJoin()
											.offset(offset)
											.limit(limit)
											.orderBy(QMensagem.mensagem.id.desc());
		
		return query.fetch();
	}

	public Optional<Mensagem> buscaProximaMensagem(int currentId) {		
		JPAQuery<Mensagem> query = new JPAQuery<Mensagem>(em)
											.from(QMensagem.mensagem)
											.where(QMensagem.mensagem.id.gt(currentId))
											.limit(1);		
		return Optional.ofNullable(query.fetchFirst());
	}

	public Optional<Mensagem> buscaMensagemAnterior(int currentId) {
		JPAQuery<Mensagem> query = new JPAQuery<Mensagem>(em)
				.from(QMensagem.mensagem)
				.where(QMensagem.mensagem.id.lt(currentId))
				.orderBy(QMensagem.mensagem.id.desc())
				.limit(1);
		return Optional.ofNullable(query.fetchFirst());	
	}

	@Override
	protected QMensagem getPath() {
		return QMensagem.mensagem;
	}	

}
