package br.com.senior.agroinfo.db.dao;

import java.util.List;
import java.util.Optional;

import br.com.senior.agroinfo.db.modal.BaseEntityId;

public interface InterfaceDAO <T extends BaseEntityId>{
	
	T adiciona(T entity);

	void remove(T entity, Object id); 
	
	void remove(BaseEntityId entity);	

	Optional<T> busca(Object id);
			
	T atualiza(T entity);

	List<T> getLista();

}
