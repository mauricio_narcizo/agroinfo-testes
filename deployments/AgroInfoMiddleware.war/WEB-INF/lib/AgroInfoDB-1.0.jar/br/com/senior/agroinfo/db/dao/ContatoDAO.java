package br.com.senior.agroinfo.db.dao;

import java.util.Date;
import java.util.Optional;

import com.querydsl.core.types.Predicate;

import br.com.senior.agroinfo.db.modal.Contato;
import br.com.senior.agroinfo.db.modal.QContato;

public class ContatoDAO extends GenericQueryDSLDAO<Contato, QContato> {

    public Contato create(Contato contato) {
    	Predicate[] predicados = new Predicate[]{QContato.contato.telefone.like(contato.getTelefone())
										.and(QContato.contato.ddd.like(contato.getDdd()))};
        Optional<Contato> optional = buscarObjeto(Optional.ofNullable(predicados)); 
        if (!optional.isPresent()) {
        	contato.setId(optional.get().getId());
            adiciona(contato);
        }
        contato.setDtUpdate(new Date());
        return contato;
    }

	@Override
	protected QContato getPath() {
		return QContato.contato;
	}

}
