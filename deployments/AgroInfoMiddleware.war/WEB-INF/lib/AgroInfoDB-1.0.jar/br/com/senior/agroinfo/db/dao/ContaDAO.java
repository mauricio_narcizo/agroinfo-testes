package br.com.senior.agroinfo.db.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.QConta;
import br.com.senior.agroinfo.db.modal.QEmpresaFilial;

@Stateless
public class ContaDAO extends GenericQueryDSLDAO<Conta, QConta> {

	public Conta create(Conta conta) {		
		conta.setDtUpdate(new Date());
		Optional<Conta> busca = buscarConta(conta.getTipoTitulo().getCodTipoTitulo(),conta.getCooperado().getCodigoMatricula(),conta.getTipoConta() );
		if (busca.isPresent()) {
			conta.setId(busca.get().getId());
			atualiza(conta);
		} else {
			adiciona(conta);
		}
		return conta;
	}
	
	public Optional<Conta> buscarConta(final String codigoTitulo, final Integer idCooperado, TipoConta tipoConta ) {
		JPAQuery<Conta> query = new JPAQuery<Conta>(em).from(QConta.conta)
				.where(QConta.conta.tipoTitulo.codTipoTitulo.eq(codigoTitulo)
				.and(QConta.conta.cooperado.codigoMatricula.eq(idCooperado)
				.and(QConta.conta.tipoConta.eq(tipoConta))));
				

		return Optional.ofNullable(query.fetchFirst());
	}
	/*
	public List<Conta> getListaContaAberta() {		
		List<Conta> listaContaAberta = getLista()
											.stream()
											.filter(conta -> conta.isContaAberta())
											.collect(Collectors.toList());
		return listaContaAberta;
	}
	*/
	public List<Conta> buscaContaCooperado(Long idCooperado) {
		JPAQuery<Conta> query = new JPAQuery<Conta>(em)
				.from(QConta.conta, QEmpresaFilial.empresaFilial)
				.where(QConta.conta.empresaFilial.id.eq(QEmpresaFilial.empresaFilial.id))
				.where(QConta.conta.cooperado.id.eq(idCooperado));

		return query.fetch();
	}
/*	
	public List<Conta> buscaContas(Long idCooperado) {
		List<Conta> lista = getLista();

		return lista.stream().filter(conta -> conta.getCooperado().getId().equals(idCooperado))
				.collect(Collectors.toList());
	}

	public List<Conta> buscaContasPeriodo(Long idCooperado, Date dataInicio, Date dataFim) {
		
		BooleanExpression expression = QConta.conta.cooperado.id.eq(idCooperado);
		if (dataInicio != null) {
			expression = expression.and(QConta.conta.dtVencimentoOriginal.eq(dataInicio)
					.or(QConta.conta.dtVencimentoOriginal.after(dataInicio)));
		}
		
		if(dataFim != null){
			expression = expression.and(QConta.conta.dtVencimentoOriginal.eq(dataFim)
					.or(QConta.conta.dtVencimentoOriginal.before(dataFim)));
		}
		
		return buscarContas(expression);
	}
	
	public List<Conta> buscarContas(Predicate predicate) {

		JPAQuery<Conta> query = new JPAQuery<Conta>(em)
				.from(QConta.conta, QEmpresaFilial.empresaFilial)
				//TODO necessário realizar join entre este dois ????? .where(QConta.conta.empresaFilial.id.eq(QEmpresaFilial.empresaFilial.id))
				.where(QConta.conta.contaAberta.isTrue())
				.where(predicate);

		return query.fetch();
	}
*/
	@Override
	protected QConta getPath() {
		return QConta.conta;
	}
}
