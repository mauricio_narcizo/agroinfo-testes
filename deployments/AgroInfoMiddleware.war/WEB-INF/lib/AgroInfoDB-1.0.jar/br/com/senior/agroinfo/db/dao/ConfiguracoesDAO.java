package br.com.senior.agroinfo.db.dao;

import java.util.Optional;

import com.querydsl.jpa.impl.JPAQueryFactory;

import br.com.senior.agroinfo.db.modal.Configuracoes;
import br.com.senior.agroinfo.db.modal.QConfiguracoes;

public class ConfiguracoesDAO extends GenericQueryDSLDAO<Configuracoes, QConfiguracoes> {

	@Override
	protected QConfiguracoes getPath() {
		return QConfiguracoes.configuracoes;
	}
	
	public Optional<Configuracoes> retrieve() {
		return Optional.ofNullable(
					new JPAQueryFactory(em)
							.select(QConfiguracoes.configuracoes)
							.from(QConfiguracoes.configuracoes)
							.limit(1)
							.fetchOne()
				);
	}

}
