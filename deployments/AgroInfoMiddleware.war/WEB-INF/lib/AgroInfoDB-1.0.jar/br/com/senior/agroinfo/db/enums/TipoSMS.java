package br.com.senior.agroinfo.db.enums;

import br.com.senior.agroinfo.exception.EnumNotFoundException;

public enum TipoSMS {
	
    ANDROID_SMS("Android SMS"),
    NAO_UTILIZAR("Não Utilizar"),
    CLOUD_SMS("Cloud SMS"); 
    
    private final String descricao;

    private TipoSMS(final String descricao) {
        this.descricao = descricao;
    }

	public String getDescricao() {
		return descricao;
	}	
	
	public static TipoSMS getByDescricao(String descricao) throws EnumNotFoundException {
		for (TipoSMS t: TipoSMS.values()){
			if (t.getDescricao().equals(descricao)) {
				return t;
			}
		}
		throw new EnumNotFoundException(String.format("Tipo SMS %s não encontrado.", descricao));
	}	

}
