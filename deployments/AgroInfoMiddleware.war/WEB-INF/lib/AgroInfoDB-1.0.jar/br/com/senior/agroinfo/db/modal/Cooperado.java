package br.com.senior.agroinfo.db.modal;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.google.common.collect.Lists;

@Entity
@Table(name = "cooperado")
public class Cooperado implements BaseEntityId, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
    @SequenceGenerator(name = "cooperado_id", sequenceName = "seq_cooperado", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cooperado_id")
	private Long id;
	
	@NotNull
	@Column(name = "codigomatricula", nullable = false)	
	private Integer codigoMatricula;
	
	@Column(name = "cpfcnpj", length = 14)
	private String cpfCnpj;
	
	@Column(name = "nome", length = 100)
	private String nome;
	
	@Column(name = "nomefantasia", length = 50)	
	private String nomeFantasia;
	
	@Column(name = "endereco", length = 100)
	private String endereco;
	
	@Column(name = "cep", length = 8)	
	private String cep;
	
	@Column(name = "bairro", length = 75)	
	private String bairro;
	
	@Column(name = "cidade", length = 60)
	private String cidade;
	
	@Column(name = "numero", length = 60)
	private String numero;
	
	@Column(name = "email", length = 100)	
	private String email;
	
	@Column(name = "atualizar", nullable = false)	
	private boolean atualizar;
	
	@Column(name = "ativo", nullable = false)
	private boolean ativo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtupdate", nullable = false, length = 29)	
	private Date dtUpdate;
	
	@Column(name = "codibge")
	private Integer codIbge;
	
	@OneToMany(mappedBy = "grupo", fetch=FetchType.LAZY)
	private List<CooperadoGrupo> cooperadosGrupos;	

	Cooperado() {}	
	
	public Cooperado clone() throws CloneNotSupportedException {
		Cooperado cooperado = (Cooperado) super.clone();
		cooperado.setId(null);
		cooperado.setCooperadosGrupos(Lists.newArrayList());
		return cooperado;		
	}

	public Cooperado(Builder builder) {
		this.setId(builder.id);
		this.setAtivo(builder.ativo);
		this.setAtualizar(builder.atualizar);
		this.setBairro(builder.bairro);
		this.setCep(builder.cep);
		this.setCidade(builder.cidade);
		this.setCodIbge(builder.codIbge);
		this.setCpfCnpj(builder.cpfCnpj);
		this.setDtUpdate(builder.dtUpdate);
		this.setEmail(builder.email);
		this.setEndereco(builder.endereco);
		this.setId(builder.id);
		this.setNome(builder.nome);
		this.setNomeFantasia(builder.nomeFantasia);
		this.setNumero(builder.numero);
		this.setCodigoMatricula(builder.codigoMatricula);
		
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpfCnpj() {
		return this.cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return this.nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAtualizar() {
		return this.atualizar;
	}

	public void setAtualizar(boolean atualizar) {
		this.atualizar = atualizar;
	}

	public boolean isAtivo() {
		return this.ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDtUpdate() {
		return this.dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Integer getCodIbge() {
		return this.codIbge;
	}

	public void setCodIbge(Integer codIbge) {
		this.codIbge = codIbge;
	}

	@PrePersist
	public void prePersist() {
		this.atualizar = Boolean.FALSE;
	}

	public void atualizarDtUpdate(Date date) {
		this.dtUpdate = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cooperado other = (Cooperado) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public Integer getCodigoMatricula() {
		return codigoMatricula;
	}


	public void setCodigoMatricula(Integer codigoMatricula) {
		this.codigoMatricula = codigoMatricula;
	}

	public List<CooperadoGrupo> getCooperadosGrupos() {
		return cooperadosGrupos;
	}


	public void setCooperadosGrupos(List<CooperadoGrupo> cooperadosGrupos) {
		this.cooperadosGrupos = cooperadosGrupos;
	}

	public static class Builder implements br.com.senior.agroinfo.db.modal.Builder<Cooperado>{
	    
		Long id;
		String cpfCnpj;
		String nome;
		String nomeFantasia;
		String endereco;
		String cep;
		String bairro;
		String cidade;
		String numero;
		String email;
		boolean atualizar;
		boolean ativo;
		Date dtUpdate;
		Integer codIbge;
		Integer codigoMatricula;

        public Builder id(Long val) { 
        	this.id = val;      
            return this; 
        }
        
        public Builder cpfCnpj(String val) { 
        	this.cpfCnpj = val;           
        	return this; 
        }
        
        public Builder nome(String val) { 
        	this.nome = val;  
        	return this; 
        }
        
        public Builder nomeFantasia(String val) { 
        	this.nomeFantasia = val;        
        	return this; 
        }
        
        public Builder endereco(String val) { 
        	this.endereco = val;        
        	return this; 
        }
        
        public Builder cep(String val) { 
        	this.cep = val;        
        	return this; 
        }
        
        public Builder bairro(String val) { 
        	this.bairro = val;        
        	return this; 
        }
        
        public Builder cidade(String val) { 
        	this.cidade = val;        
        	return this; 
        }
        
        public Builder numero(String val) { 
        	this.numero = val;
        	return this; 
        }
        
        public Builder email(String val) { 
        	this.email = val;
        	return this; 
        }
        
        public Builder atualizar(boolean val) { 
        	this.atualizar = val;        
        	return this; 
        }
        
        public Builder ativo(boolean val) { 
        	this.ativo = val;        
        	return this; 
        }
        
        public Builder dtUpdate(Date val) { 
        	this.dtUpdate = val; 
        	return this; 
        }
        
        public Builder codIbge(Integer val) { 
        	this.codIbge = val;
        	return this; 
        }
        
        public Builder codigoMatricula(Integer val) {
        	this.codigoMatricula = val;
        	return this;
        }

        public Cooperado build() {
            return new Cooperado(this);
        }
    }	
	
	
}