package br.com.senior.agroinfo.db.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "empresafilial")
public class EmpresaFilial implements BaseEntityId {

	private static final long serialVersionUID = 1400751369326791791L;

	@Id
	@SequenceGenerator(name = "empresafilial_id", sequenceName = "seq_empresafilial", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "empresafilial_id")
	private Long id;

	@Column(name = "codigoempresa")
	private Integer codigoEmpresa;

	@Column
	private Integer codigoFilial;

	@Column(name = "nomeempresa")
	private String nomeEmpresa;

	@Column(name = "nomefilial")
	private String nomeFilial;

	@Column(name = "ativa", nullable = false)
	private boolean ativa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtupdate", length = 29)
	private Date dtUpdate;

	@Column(name = "dddpadrao", length = 3)
	private String dddPadrao;

	EmpresaFilial() {
	}

	public EmpresaFilial(Builder builder) {
		// this.id = builder.id;
		this.codigoEmpresa = builder.codigoEmpresa;
		this.nomeEmpresa = builder.nomeEmpresa;
		this.nomeFilial = builder.nomeFilial;
		this.ativa = builder.ativa;
		this.dtUpdate = builder.dtUpdate;
		this.dddPadrao = builder.dddPadrao;
		this.codigoFilial = builder.codigoFilial;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigoEmpresa() {
		return this.codigoEmpresa;
	}

	public void setCodigoEmpresa(Integer codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}

	public String getNomeEmpresa() {
		return this.nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getNomeFilial() {
		return this.nomeFilial;
	}

	public void setNomeFilial(String nomeFilial) {
		this.nomeFilial = nomeFilial;
	}

	public boolean isAtiva() {
		return this.ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	public Date getDtUpdate() {
		return this.dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public String getDddPadrao() {
		return dddPadrao;
	}

	public void setDddPadrao(String dddPadrao) {
		this.dddPadrao = dddPadrao;
	}

	public Integer getCodigoFilial() {
		return codigoFilial;
	}

	public void setCodigoFilial(Integer codigoFilial) {
		this.codigoFilial = codigoFilial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaFilial other = (EmpresaFilial) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static class Builder implements br.com.senior.agroinfo.db.modal.Builder<EmpresaFilial> {

		// Long id;
		Integer codigoEmpresa;
		String nomeEmpresa;
		String nomeFilial;
		boolean ativa;
		Date dtUpdate;
		String dddPadrao;
		Integer codigoFilial;

		// public Builder id(Long val) {
		// this.id = val;
		// return this;
		// }

		public Builder codigoEmpresa(Integer val) {
			this.codigoEmpresa = val;
			return this;
		}

		public Builder codigoFilial(Integer codigoFilial) {
			this.codigoFilial = codigoFilial;
			return this;
		}

		public Builder nomeEmpresa(String val) {
			this.nomeEmpresa = val;
			return this;
		}

		public Builder nomeFilial(String val) {
			this.nomeFilial = val;
			return this;
		}

		public Builder ativa(boolean val) {
			this.ativa = val;
			return this;
		}

		public Builder dtUpdate(Date val) {
			this.dtUpdate = val;
			return this;
		}

		public Builder dddPadrao(String val) {
			this.dddPadrao = val;
			return this;
		}

		public EmpresaFilial build() {
			return new EmpresaFilial(this);
		}
	}

}
