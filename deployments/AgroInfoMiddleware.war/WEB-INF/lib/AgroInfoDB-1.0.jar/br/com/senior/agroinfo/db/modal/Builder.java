package br.com.senior.agroinfo.db.modal;

public interface Builder<T extends BaseEntityId> {
	
	public T build();

}
