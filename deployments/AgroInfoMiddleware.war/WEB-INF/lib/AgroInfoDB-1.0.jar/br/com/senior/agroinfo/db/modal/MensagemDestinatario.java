package br.com.senior.agroinfo.db.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "mensagemdestinatario")
public class MensagemDestinatario implements BaseEntityId {

	private static final long serialVersionUID = 4618514901892391456L;

	@Id
	@SequenceGenerator(name = "mensagemdest_id", sequenceName = "seq_mensagemdest", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mensagemdest_id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcooperado")
	private Cooperado cooperado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idgrupo")
	private Grupo grupo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idmensagem")
	private Mensagem mensagem;

	@Column(nullable = false)
	private Boolean lido = Boolean.FALSE;
	
	@Column(nullable = false)
	private Boolean excluido = Boolean.FALSE;

	MensagemDestinatario() {}

	public static MensagemDestinatario of(Cooperado cooperado, Mensagem mensagem, Grupo grupo) {
		MensagemDestinatario m = new MensagemDestinatario();
		m.setCooperado(cooperado);
		m.setMensagem(mensagem);
		m.setGrupo(grupo);
		return m;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MensagemDestinatario other = (MensagemDestinatario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Boolean isLido() {
		return lido;
	}

	public void setLido(Boolean lido) {
		this.lido = lido;
	}

	public Boolean isExcluido() {
		return excluido;
	}

	public void setExcluido(Boolean excluido) {
		this.excluido = excluido;
	}

	public Cooperado getCooperado() {
		return cooperado;
	}

	public void setCooperado(Cooperado cooperado) {
		this.cooperado = cooperado;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

}