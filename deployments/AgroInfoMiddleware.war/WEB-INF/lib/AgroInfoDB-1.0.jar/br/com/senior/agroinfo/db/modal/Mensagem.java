package br.com.senior.agroinfo.db.modal;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "mensagem")
public class Mensagem implements BaseEntityId {

    private static final long serialVersionUID = 1L;
    
	@Id
    @SequenceGenerator(name = "mensagem_id", sequenceName = "seq_mensagem", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mensagem_id")
	private Long id;
    
    @Column(name = "dtcriacao", nullable = false)    
    private Date dtCriacao = new Date();

    @Column(name = "dtupdate", nullable = false, length = 29)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtUpdate;
	
	@Column(name = "titulo")
	private String titulo;
	
	@Size(min=1, max=1000)
	@Column(name = "corpo", length = 1000)
	private String corpo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcategoriamensagem", nullable = false)
	private CategoriaMensagem categoria;	
	
	@OneToMany(mappedBy = "mensagem", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	private List<MensagemDestinatario> destinatarios;
    
    Mensagem() {}
    
    public static Mensagem of(Date dtCriacao, Date dtUpdate, String titulo, String corpo, Long idCategoria) {
    	Mensagem m = new Mensagem();
    	m.setDtupdate(dtUpdate);
    	m.setDtCriacao(dtCriacao);
    	m.setTitulo(titulo);
    	m.setCorpo(corpo);
    	m.setCategoria(CategoriaMensagem.of(idCategoria));
    	return m;
    }

    public Mensagem(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getDtupdate() {
        return this.dtUpdate;
    }

    public void setDtupdate(Date dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public List<MensagemDestinatario> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(List<MensagemDestinatario> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public CategoriaMensagem getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMensagem categoria) {
		this.categoria = categoria;
	}    
    
}
