package br.com.senior.agroinfo.db.dao;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAUpdateClause;

import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.QCooperado;

@Stateless
public class CooperadoDAO extends GenericQueryDSLDAO<Cooperado, QCooperado> {

    @PersistenceContext
    private EntityManager em;

    public Cooperado create(Cooperado cooperado) {
        cooperado.setDtUpdate(new Date());
        Optional<Cooperado> busca = buscarObjeto(
        								Optional.ofNullable(
        											new Predicate[]{QCooperado.cooperado.codigoMatricula.eq(cooperado.getCodigoMatricula())}
        											)
        								);
        if (busca.isPresent()) {
        	cooperado.setId(busca.get().getId());
            atualiza(cooperado);
        } else {
            adiciona(cooperado);
        }
        return cooperado;
    }

	public void atualizaSituacao(boolean situacao, Long id) {
		new JPAUpdateClause(em, QCooperado.cooperado)
					.where(QCooperado.cooperado.id.eq(id))
					.set(QCooperado.cooperado.ativo, situacao)
					.execute();
	}

	@Override
	protected QCooperado getPath() {
		return QCooperado.cooperado;
	}

	public List<Cooperado> getCooperadosAtivos() {
		return buscar(Optional.ofNullable(new Predicate[]{QCooperado.cooperado.ativo.isTrue()}), 
				Optional.ofNullable(QCooperado.cooperado.id.asc()));
	}
}