package br.com.senior.agroinfo.db.dao;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.querydsl.core.types.Predicate;

import br.com.senior.agroinfo.db.enums.SituacaoTipoTitulo;
import br.com.senior.agroinfo.db.enums.TipoConta;
import br.com.senior.agroinfo.db.modal.Conta;
import br.com.senior.agroinfo.db.modal.Cooperado;
import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.QConta;
import br.com.senior.agroinfo.db.modal.QCooperado;
import br.com.senior.agroinfo.db.modal.QEmpresaFilial;
import br.com.senior.agroinfo.db.modal.QTipoTitulo;
import br.com.senior.agroinfo.db.modal.TipoTitulo;

@RunWith(Arquillian.class)
public class ContaDAOTest extends DaoAbstractTest{
    
    @Inject
    private ContaDAO dao;
    
    @Inject
    private CooperadoDAO cooperadoDAO;
    
    @Inject
    private EmpresaFilialDAO empresaFilialDAO;
    
    @Inject
    private TipoTituloDAO tipoTituloDAO;
    
    private Cooperado cooperado = new Cooperado.Builder()
                                                    .ativo(true)
                                                    .atualizar(true)
                                                    .bairro("Figueira")
                                                    .cep("89110")
                                                    .nome("joao")
                                                    .dtUpdate(new Date())
                                                    .build();
    
    private EmpresaFilial empresaFilial = new EmpresaFilial.Builder()
                                                    .ativa(true)
                                                    .codigoEmpresa(1)
                                                    .dddPadrao("047")
                                                    .dtUpdate(new Date())
                                                    .nomeEmpresa("Empresa 1")
                                                    .build();
    
    private TipoTitulo tipoTitulo = new TipoTitulo.Builder()
                                                    .abreviacaoTipoTitulo("Abrev")
                                                    .codTipoTitulo("codigo")
                                                    .descricaoTitulo("Descricao")
                                                    .situacao(SituacaoTipoTitulo.SomaDuplicatas)
                                                    .build();
    
    private Conta contaPagar = new Conta.Builder(TipoConta.PAGAR)
                                                    
                                                    .contaAberta(true)
                                                    .cooperado(cooperado)
                                                    .descricao("contapagar1")
                                                    .dtEmissao(new Date())
                                                    .dtUpdate(new Date())
                                                    .dtVencimento(new Date())
                                                    .dtVencimentoOriginal(new Date())
                                                    .empresaFilial(empresaFilial)
                                                    .integrado(true)
                                                    .juros(BigDecimal.ZERO)
                                                    .multa(BigDecimal.ZERO)
                                                    .tipoTitulo(tipoTitulo)
                                                    .valor(BigDecimal.TEN)
                                                    .valorAberto(BigDecimal.TEN)
                                                    .build();
    
    private Conta contaReceber = new Conta.Builder(TipoConta.RECEBER)
                                                    .contaAberta(true)
                                                    .cooperado(cooperado)
                                                    .descricao("contapagar1")
                                                    .dtEmissao(new Date())
                                                    .dtUpdate(new Date())
                                                    .dtVencimento(new Date())
                                                    .dtVencimentoOriginal(new Date())
                                                    .empresaFilial(empresaFilial)
                                                    .integrado(true)
                                                    .juros(BigDecimal.ZERO)
                                                    .multa(BigDecimal.ZERO)
                                                    .tipoTitulo(tipoTitulo)
                                                    .valor(BigDecimal.TEN)
                                                    .valorAberto(BigDecimal.TEN)
                                                    .build();

    
    @Test
    public void deveRetornarListaPreenchida() {
        assertTrue("Lista deve retornar 2 elementos", dao.buscar(Optional.empty(), Optional.empty()).size() == 2);
    }
    
    @Test
    public void deveRetornarContaPagarPorId() {
        Optional<Conta> optional = dao.buscarObjeto(Optional.of(new Predicate[]{QConta.conta.id.eq(1L)}));
        assertTrue("Não retornou conaPagar com id 1", optional.isPresent());
    }
    
    @Before
    public void before() throws CloneNotSupportedException {        
        cooperado = cooperadoDAO.adiciona(cooperado);
        empresaFilial = empresaFilialDAO.adiciona(empresaFilial);
        tipoTitulo = tipoTituloDAO.adiciona(tipoTitulo);
        contaPagar = dao.adiciona(contaPagar);
        contaReceber = dao.adiciona(contaReceber);
    }
    
    @After
    public void tearDown() {
        dao.remove(Optional.of(new Predicate[]{QConta.conta.id.eq(contaPagar.getId())}));
        dao.remove(Optional.of(new Predicate[]{QConta.conta.id.eq(contaReceber.getId())}));
        cooperadoDAO.remove(Optional.of(new Predicate[]{QCooperado.cooperado.id.eq(cooperado.getId())}));
        empresaFilialDAO.remove(Optional.of(new Predicate[]{QEmpresaFilial.empresaFilial.id.eq(empresaFilial.getId())}));
        tipoTituloDAO.remove(Optional.of(new Predicate[]{QTipoTitulo.tipoTitulo.codTipoTitulo.eq(tipoTitulo.getCodTipoTitulo())}));
    }

}
