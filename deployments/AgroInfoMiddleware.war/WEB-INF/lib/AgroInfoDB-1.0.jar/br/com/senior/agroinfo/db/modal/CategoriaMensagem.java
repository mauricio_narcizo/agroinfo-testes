package br.com.senior.agroinfo.db.modal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="categoriamensagem")
public class CategoriaMensagem implements BaseEntityId{

	private static final long serialVersionUID = 1L;	
	
	@Id
    @SequenceGenerator(name = "categoriamensagem_id", sequenceName = "seq_categoriamensagem", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categoriamensagem_id")
	private Long id;
	
	@Column(nullable=false, length = 100)
	private String descricao;
	
	@Column(nullable=false, length = 50)
	private String cor;
	
	CategoriaMensagem(){}
	
	public static CategoriaMensagem of(Long id, String descricao, String cor) {
		CategoriaMensagem categoria = new CategoriaMensagem();
		categoria.setId(id);
		categoria.setDescricao(descricao);
		categoria.setCor(cor);
		return categoria;
	}
	public static CategoriaMensagem of(Long id) {
		CategoriaMensagem categoria = new CategoriaMensagem();
		categoria.setId(id);
		return categoria;
	}	
	
	@Override
	public void setId(Long id) {		
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoriaMensagem other = (CategoriaMensagem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
