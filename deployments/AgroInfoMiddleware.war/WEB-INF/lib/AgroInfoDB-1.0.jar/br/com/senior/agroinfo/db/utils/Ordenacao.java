package br.com.senior.agroinfo.db.utils;

public class Ordenacao {
	
	private String orderBy = "id";

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

}
