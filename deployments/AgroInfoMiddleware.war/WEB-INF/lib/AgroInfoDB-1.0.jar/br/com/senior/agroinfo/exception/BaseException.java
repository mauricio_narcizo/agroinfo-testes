package br.com.senior.agroinfo.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class BaseException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public BaseException(String msg) {
		super(msg);
	}	

}
