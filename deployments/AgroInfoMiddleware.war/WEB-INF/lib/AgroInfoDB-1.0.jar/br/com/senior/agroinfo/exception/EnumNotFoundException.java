package br.com.senior.agroinfo.exception;

public class EnumNotFoundException extends BaseException{

	private static final long serialVersionUID = 1L;
	
	public EnumNotFoundException(String msg) {
		super(msg);
	}


}
