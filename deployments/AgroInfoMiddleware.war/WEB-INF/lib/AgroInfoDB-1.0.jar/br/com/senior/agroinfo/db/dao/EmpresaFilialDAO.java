package br.com.senior.agroinfo.db.dao;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.agroinfo.db.modal.EmpresaFilial;
import br.com.senior.agroinfo.db.modal.QEmpresaFilial;

@Stateless
public class EmpresaFilialDAO extends GenericQueryDSLDAO<EmpresaFilial, QEmpresaFilial> {

	/*
	public List<EmpresaFilial> getListaAtiva() {
		BooleanExpression predicate = QEmpresaFilial.empresaFilial.ativa.isTrue();
		return buscarEmpresasFiliais(predicate);
	}
	*/
	
	@Override
	public EmpresaFilial adiciona(EmpresaFilial entity) {
		BooleanExpression predicate = QEmpresaFilial.empresaFilial.codigoFilial.eq(entity.getCodigoFilial())
				.and(QEmpresaFilial.empresaFilial.codigoEmpresa.eq(entity.getCodigoEmpresa()));
		
		Optional<EmpresaFilial> empresaFilial = buscarObjeto(Optional.ofNullable(new Predicate[]{predicate}));
		if (empresaFilial.isPresent()) {
			return atualiza(entity);
		} 
		return super.adiciona(entity);
	}
/*
	public Optional<EmpresaFilial> buscarEmpresaFilial(Predicate predicate) {
		JPAQuery<EmpresaFilial> query = new JPAQuery<EmpresaFilial>(em)
				.from(QEmpresaFilial.empresaFilial)
				.where(predicate);
		return Optional.ofNullable(query.fetchFirst());
	}
	
	public List<EmpresaFilial> buscarEmpresasFiliais(Predicate predicate) {
		JPAQuery<EmpresaFilial> query = new JPAQuery<EmpresaFilial>(em)
				.from(QEmpresaFilial.empresaFilial)
				.where(predicate);
		return query.fetch();
	}
*/
	@Override
	protected QEmpresaFilial getPath() {
		return QEmpresaFilial.empresaFilial;
	}
}