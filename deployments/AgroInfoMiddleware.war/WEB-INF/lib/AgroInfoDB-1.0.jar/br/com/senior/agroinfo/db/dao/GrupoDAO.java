package br.com.senior.agroinfo.db.dao;

import java.util.List;

import javax.ejb.Stateless;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.com.senior.agroinfo.db.modal.CooperadoGrupo;
import br.com.senior.agroinfo.db.modal.Grupo;
import br.com.senior.agroinfo.db.modal.QCooperado;
import br.com.senior.agroinfo.db.modal.QCooperadoGrupo;
import br.com.senior.agroinfo.db.modal.QGrupo;

@Stateless
public class GrupoDAO extends GenericQueryDSLDAO<Grupo, QGrupo>{
	
	
	@Override
	protected QGrupo getPath() {
		return QGrupo.grupo;
	}
	
	public List<CooperadoGrupo> localizarPorCooperado(Long idCooperado) {
				JPAQuery<CooperadoGrupo> query = new JPAQueryFactory(em).select(QCooperadoGrupo.cooperadoGrupo)
						.from(QCooperadoGrupo.cooperadoGrupo)
						.join(QCooperadoGrupo.cooperadoGrupo.grupo, QGrupo.grupo)
						.join(QCooperadoGrupo.cooperadoGrupo.cooperado, QCooperado.cooperado)
						.where(QCooperado.cooperado.id.eq(idCooperado))
						.orderBy(QGrupo.grupo.id.asc());
				return query.fetch();	
	}
}
