package br.com.senior.agroinfo.db.dao;

import javax.ejb.Stateless;

import br.com.senior.agroinfo.db.modal.CategoriaMensagem;
import br.com.senior.agroinfo.db.modal.QCategoriaMensagem;

@Stateless
public class CategoriaMensagemDAO extends GenericQueryDSLDAO<CategoriaMensagem, QCategoriaMensagem>{
	
	@Override
	protected QCategoriaMensagem getPath() {
		return QCategoriaMensagem.categoriaMensagem;
	}
	
	
}
