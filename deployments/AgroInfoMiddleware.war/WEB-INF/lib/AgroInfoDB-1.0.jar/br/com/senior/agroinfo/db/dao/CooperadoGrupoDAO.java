package br.com.senior.agroinfo.db.dao;

import com.querydsl.jpa.impl.JPADeleteClause;

import br.com.senior.agroinfo.db.modal.CooperadoGrupo;
import br.com.senior.agroinfo.db.modal.QCooperadoGrupo;

public class CooperadoGrupoDAO extends GenericQueryDSLDAO<CooperadoGrupo, QCooperadoGrupo>{

	
	public void removeGruposDoUsuario(Long id) {
		new JPADeleteClause(em, QCooperadoGrupo.cooperadoGrupo)
									.where(QCooperadoGrupo.cooperadoGrupo.cooperado.id.eq(id))
									.execute();
		
	}
	/*
	public List<CooperadoGrupo> buscaCooperadosPorGrupo(Long idGrupo) {
		JPAQuery<CooperadoGrupo> query = new JPAQuery<CooperadoGrupo>(em)
											.from(QCooperadoGrupo.cooperadoGrupo)
											.where(QCooperadoGrupo.cooperadoGrupo.grupo.id.eq(idGrupo));		
		return query.fetch();
	}
	 */
	@Override
	protected QCooperadoGrupo getPath() {
		return QCooperadoGrupo.cooperadoGrupo;
	}

}
