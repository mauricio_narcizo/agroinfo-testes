package br.com.senior.agroinfo.db.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.agroinfo.db.modal.BaseEntityId;

public abstract class GenericQueryDSLDAO<T extends BaseEntityId, E extends EntityPath<T>> {

	@PersistenceContext
	protected transient EntityManager em;	
	
	protected abstract E getPath();	
	
	public T adiciona(T entity) {
		em.persist(entity);
		return entity;
	}
	
	public void remove(Optional<Predicate[]> predicates) {
		JPADeleteClause delete = new JPADeleteClause(em, getPath());
		predicates.ifPresent(t -> delete.where(t));
		delete.execute();
	}
	
    @SuppressWarnings("unchecked")
	protected Class<T> getEntityBeanType() {
    	 return (Class<T>) ((ParameterizedType)(super.getClass().getGenericSuperclass())).getActualTypeArguments()[0];
    }	
	
	public Optional<T> buscarObjeto(Optional<Predicate[]> predicates) {
		JPAQuery<T> query = new JPAQuery<>(em);
		query.from(getPath());
		predicates.ifPresent(t -> query.where(t));
		return Optional
					.ofNullable(query.fetchOne());
	}
	
	public T atualiza(T entity) {
		 entity = em.merge(entity);
		 return entity;
	}
	
	public List<T> buscar(Optional<Predicate[]> predicates, Optional<OrderSpecifier<?>> orderBy, int offset, int limit) {
		JPAQuery<T> query = new JPAQuery<>(em);
		query.from(getPath());
		query
			.offset(offset)
			.limit(limit);
		predicates.ifPresent(t -> query.where(t));
		orderBy.ifPresent(t -> query.orderBy(t));		
		return 	query.fetch();
	}
	
	public List<T> buscar(Optional<Predicate[]> predicates, Optional<OrderSpecifier<?>> orderBy) {
		JPAQuery<T> query = new JPAQuery<>(em);
		query.from(getPath());
		predicates.ifPresent(t -> query.where(t));
		orderBy.ifPresent(t -> query.orderBy(t));		
		return 	query.fetch();
	}	
	
	public Long count(Optional<Predicate[]> predicates) {
		JPAQuery<T> query = new JPAQuery<>(em);
		query.from(getPath());
		predicates.ifPresent(t -> query.where(t));
		return 	query.fetchCount();
	}	
	
}
