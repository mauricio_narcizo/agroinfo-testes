package br.com.senior.agroinfo.db.modal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cooperadogrupo")
public class CooperadoGrupo implements BaseEntityId {

	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name = "cooperadogrupo_id", sequenceName = "seq_cooperadogrupo", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cooperadogrupo_id")	
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcooperado")	
	private Cooperado cooperado;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idgrupo")	
	private Grupo grupo;

	CooperadoGrupo() {}
	
	public static CooperadoGrupo of(Cooperado cooperado, Grupo grupo) {
		CooperadoGrupo cg = new CooperadoGrupo();
		cg.setCooperado(cooperado);
		cg.setGrupo(grupo);
		return cg;
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idcooperado")
	public Cooperado getCooperado() {
		return this.cooperado;
	}

	public void setCooperado(Cooperado cooperado) {
		this.cooperado = cooperado;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idgrupo")
	public Grupo getGrupo() {
		return this.grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CooperadoGrupo other = (CooperadoGrupo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
