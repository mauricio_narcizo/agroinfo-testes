package br.com.senior.agroinfo.db.dao;

import br.com.senior.agroinfo.db.modal.MensagemDestinatario;
import br.com.senior.agroinfo.db.modal.QMensagemDestinatario;

public class MensagemDestinatarioDAO extends GenericQueryDSLDAO<MensagemDestinatario, QMensagemDestinatario> {

	@Override
	protected QMensagemDestinatario getPath() {
		return QMensagemDestinatario.mensagemDestinatario;
	}

}
