System.register(['angular2/core', './mensagemService', './mensagem', '../categoriaMensagem/categoriaMensagem', 'angular2/router', '../Component/AbstractComponent', '../categoriaMensagem/categoriaMensagemService'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, mensagemService_1, mensagem_1, categoriaMensagem_1, router_1, AbstractComponent_1, router_2, categoriaMensagemService_1;
    var ComposeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (mensagemService_1_1) {
                mensagemService_1 = mensagemService_1_1;
            },
            function (mensagem_1_1) {
                mensagem_1 = mensagem_1_1;
            },
            function (categoriaMensagem_1_1) {
                categoriaMensagem_1 = categoriaMensagem_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            },
            function (categoriaMensagemService_1_1) {
                categoriaMensagemService_1 = categoriaMensagemService_1_1;
            }],
        execute: function() {
            ComposeComponent = (function (_super) {
                __extends(ComposeComponent, _super);
                function ComposeComponent(service, routeParams, mensagem, categoriaMensagem, categoriaService) {
                    _super.call(this);
                    this.service = service;
                    this.mensagem = mensagem;
                    this.categoriaMensagem = categoriaMensagem;
                    this.categoriaService = categoriaService;
                    this.service = service;
                    this.id = routeParams.get('id');
                    if (this.id != null && this.id > 0) {
                        this._getDados();
                    }
                }
                ComposeComponent.prototype._getDados = function () {
                    var _this = this;
                    this.service.getMensagem(this.id)
                        .subscribe(function (res) { _this._loadData(res.json()); }, function (error) { return _this.onError(error); });
                };
                ComposeComponent.prototype._loadData = function (json) {
                    this.mensagem.titulo = json.assunto;
                    this.mensagem.idCategoria = json.categoria.id;
                    this.mensagem.corpoMensagem = json.corpo;
                };
                ComposeComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._selectMultiple = $(".select2").select2();
                    this.service.getDestinatarios()
                        .subscribe(function (res) { _this.destinatarios = res.json(); }, function (error) { _this.onError(error); });
                    this.categoriaService.getCategorias()
                        .subscribe(function (res) { _this.categorias = res.json(); }, function (error) { return _this.onError(error); });
                };
                ComposeComponent.prototype.clean = function () {
                    this._selectMultiple
                        .val(null)
                        .trigger("change");
                };
                ComposeComponent.prototype.onSubmit = function () {
                    var _this = this;
                    _super.prototype.cleanMessages.call(this);
                    var listTo = [];
                    var selected = this._selectMultiple.select2('data');
                    if (selected.length == 0) {
                        this.showError('Selecione um destinatário');
                        return;
                    }
                    for (var _i = 0; _i < selected.length; _i++) {
                        var item = selected[_i];
                        var to = { id: item.element.attributes[1].value, tipo: item.element.attributes[0].value };
                        listTo.push(to);
                    }
                    this.mensagem.destinatarios = listTo;
                    this.service.salvar(this.mensagem)
                        .subscribe(function (res) { _this.afterSave(res); }, function (error) { _this.onError(error); });
                };
                ComposeComponent.prototype.afterSave = function (res) {
                    _super.prototype.afterSave.call(this, res);
                    this.clean();
                    this.mensagem = new mensagem_1.Mensagem();
                };
                ComposeComponent = __decorate([
                    core_1.Component({
                        selector: 'compose',
                        directives: [router_2.ROUTER_DIRECTIVES],
                        providers: [mensagemService_1.MensagemService, categoriaMensagemService_1.CategoriaMensagemService, mensagem_1.Mensagem, categoriaMensagem_1.CategoriaMensagem],
                        templateUrl: './src/app/mensagem/compose-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [mensagemService_1.MensagemService, router_1.RouteParams, mensagem_1.Mensagem, categoriaMensagem_1.CategoriaMensagem, categoriaMensagemService_1.CategoriaMensagemService])
                ], ComposeComponent);
                return ComposeComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("ComposeComponent", ComposeComponent);
        }
    }
});
