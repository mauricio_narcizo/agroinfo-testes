System.register(['angular2/core', './mensagemService', '../Component/AbstractComponent', 'angular2/router'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, mensagemService_1, AbstractComponent_1, router_1, router_2;
    var ReadMailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (mensagemService_1_1) {
                mensagemService_1 = mensagemService_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
            }],
        execute: function() {
            ReadMailComponent = (function (_super) {
                __extends(ReadMailComponent, _super);
                function ReadMailComponent(service, routeParams) {
                    _super.call(this);
                    this.mensagem = new Object();
                    this.service = service;
                    this.id = routeParams.get('id');
                    this.getDados();
                }
                ReadMailComponent.prototype.getDados = function () {
                    var _this = this;
                    this.service.getMensagem(this.id)
                        .subscribe(function (res) { _this._loadData(res.json()); }, function (error) { return _this.onError(error); });
                };
                ReadMailComponent.prototype.onProximo = function (id) {
                    var _this = this;
                    this.id = id;
                    this.service.getProximaMensagem(this.id)
                        .subscribe(function (res) { _this._loadData(res.json()); }, function (error) { return _this.onError(error); });
                };
                ReadMailComponent.prototype.onAnterior = function (id) {
                    var _this = this;
                    this.id = id;
                    this.service.getMensagemAnterior(this.id)
                        .subscribe(function (res) { _this._loadData(res.json()); }, function (error) { return _this.onError(error); });
                };
                ReadMailComponent.prototype._loadData = function (json) {
                    if (json.id != null) {
                        this.mensagem = json;
                    }
                };
                ReadMailComponent = __decorate([
                    core_1.Component({
                        selector: 'readMail',
                        directives: [router_2.ROUTER_DIRECTIVES],
                        providers: [mensagemService_1.MensagemService],
                        templateUrl: './src/app/mensagem/readMail-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [mensagemService_1.MensagemService, router_1.RouteParams])
                ], ReadMailComponent);
                return ReadMailComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("ReadMailComponent", ReadMailComponent);
        }
    }
});
