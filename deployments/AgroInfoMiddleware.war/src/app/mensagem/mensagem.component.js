System.register(['angular2/core', './mensagemService', './../dataTable/tabelaService', '../Component/FormPaginacao', 'angular2/router'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, mensagemService_1, tabelaService_1, FormPaginacao_1, router_1;
    var MensagemComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (mensagemService_1_1) {
                mensagemService_1 = mensagemService_1_1;
            },
            function (tabelaService_1_1) {
                tabelaService_1 = tabelaService_1_1;
            },
            function (FormPaginacao_1_1) {
                FormPaginacao_1 = FormPaginacao_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            MensagemComponent = (function (_super) {
                __extends(MensagemComponent, _super);
                function MensagemComponent(service, tabelaService) {
                    this.service = service;
                    _super.call(this, tabelaService);
                }
                MensagemComponent.prototype.getDados = function () {
                    var _this = this;
                    this.service.getMensagens(this.pesquisa, this.tabelaService.inicioListagem - 1, this.tabelaService.qtdPorPagina)
                        .subscribe(function (res) { _this.tabelaService.preencheLista(res.json()); }, function (error) { return _this.onError(error); });
                };
                MensagemComponent.prototype.onPesquisar = function () {
                    this.tabelaService.resetValues();
                    if (this.pesquisa != null && this.pesquisa.length == 0) {
                        this.pesquisa = null;
                    }
                    this.getDados();
                };
                MensagemComponent = __decorate([
                    core_1.Component({
                        selector: 'mensagem',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [mensagemService_1.MensagemService, tabelaService_1.TabelaService],
                        templateUrl: './src/app/mensagem/mensagem-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [mensagemService_1.MensagemService, tabelaService_1.TabelaService])
                ], MensagemComponent);
                return MensagemComponent;
            })(FormPaginacao_1.FormPaginacao);
            exports_1("MensagemComponent", MensagemComponent);
        }
    }
});
