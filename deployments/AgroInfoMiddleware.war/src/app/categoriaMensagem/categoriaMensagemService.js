System.register(['angular2/core', 'angular2/http', './../values/config', 'angular2-jwt'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, config_1, angular2_jwt_1;
    var CategoriaMensagemService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (angular2_jwt_1_1) {
                angular2_jwt_1 = angular2_jwt_1_1;
            }],
        execute: function() {
            CategoriaMensagemService = (function () {
                function CategoriaMensagemService(authHttp) {
                    this.authHttp = authHttp;
                    this.path = new config_1.Config().BASEURI + '/categoriaMensagem';
                }
                CategoriaMensagemService.prototype.getTipos = function (nome, offset, limit) {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    var path = this.path + '/pesquisa/' + nome + '/offset/' + offset + '/limit/' + limit;
                    return this.authHttp.get(path, { headers: headers });
                };
                CategoriaMensagemService.prototype.salvar = function (tipoMensagem) {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    return this.authHttp.post(this.path, JSON.stringify(tipoMensagem), {
                        headers: headers
                    });
                };
                CategoriaMensagemService.prototype.getCategorias = function () {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    var path = this.path;
                    return this.authHttp.get(path, { headers: headers });
                };
                CategoriaMensagemService.prototype.delete = function (id) {
                    return this.authHttp.delete(this.path + '/' + id);
                };
                CategoriaMensagemService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [angular2_jwt_1.AuthHttp])
                ], CategoriaMensagemService);
                return CategoriaMensagemService;
            })();
            exports_1("CategoriaMensagemService", CategoriaMensagemService);
        }
    }
});
