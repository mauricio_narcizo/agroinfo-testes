System.register(['angular2/core', './categoriaMensagem', './categoriaMensagemService', './../dataTable/tabelaService', '../Component/AbstractComponent'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, categoriaMensagem_1, categoriaMensagemService_1, tabelaService_1, AbstractComponent_1;
    var CategoriaMensagemComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (categoriaMensagem_1_1) {
                categoriaMensagem_1 = categoriaMensagem_1_1;
            },
            function (categoriaMensagemService_1_1) {
                categoriaMensagemService_1 = categoriaMensagemService_1_1;
            },
            function (tabelaService_1_1) {
                tabelaService_1 = tabelaService_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            }],
        execute: function() {
            CategoriaMensagemComponent = (function (_super) {
                __extends(CategoriaMensagemComponent, _super);
                function CategoriaMensagemComponent(service, tabelaService, categoria) {
                    _super.call(this);
                    this.categoria = categoria;
                    this.isModalOpen = false;
                    this.service = service;
                    this.tabelaService = tabelaService;
                }
                CategoriaMensagemComponent.prototype.ngOnInit = function () {
                    this.getDados();
                };
                CategoriaMensagemComponent.prototype.onExcluir = function ($event, id) {
                    var _this = this;
                    if (confirm("Você realmente deseja excluir o grupo?")) {
                        this.cleanMessages();
                        this.service.delete(id)
                            .subscribe(function (data) { _this.afterDelete(data), _this.getDados(); }, function (error) { return _this.onError(error); });
                    }
                    $event.stopPropagation();
                };
                CategoriaMensagemComponent.prototype.getDados = function () {
                    var _this = this;
                    this.service.getTipos(this.pesquisa, this.tabelaService.inicioListagem - 1, this.tabelaService.qtdPorPagina)
                        .subscribe(function (res) { _this.tabelaService.preencheLista(res.json()); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                CategoriaMensagemComponent.prototype.showErroModal = function () {
                    return this.isModalOpen;
                };
                CategoriaMensagemComponent.prototype.onAvancar = function () {
                    this.tabelaService.onAvancar();
                    this.getDados();
                };
                CategoriaMensagemComponent.prototype.onVoltar = function () {
                    this.tabelaService.onVoltar();
                    this.getDados();
                };
                CategoriaMensagemComponent.prototype.onLocalizar = function () {
                    this.tabelaService.resetValues();
                    if (this.pesquisa != null && this.pesquisa.length == 0) {
                        this.pesquisa = null;
                    }
                    this.getDados();
                };
                CategoriaMensagemComponent.prototype.onNovo = function () {
                    this.categoria = new categoriaMensagem_1.CategoriaMensagem();
                    this.showModal();
                };
                CategoriaMensagemComponent.prototype.showModal = function () {
                    this.cleanMessages();
                    this.isModalOpen = true;
                    $('#myModal').modal({ backdrop: 'static', keyboard: false });
                };
                CategoriaMensagemComponent.prototype.cancel = function () {
                    this.closeModal();
                };
                CategoriaMensagemComponent.prototype.closeModal = function () {
                    this.isModalOpen = false;
                    $('#myModal').modal('hide');
                };
                CategoriaMensagemComponent.prototype.afterSave = function (data) {
                    _super.prototype.afterSave.call(this, data);
                    this.getDados();
                    this.closeModal();
                };
                CategoriaMensagemComponent.prototype.onEditar = function (categoria) {
                    this.categoria = new categoriaMensagem_1.CategoriaMensagem();
                    this.categoria.id = categoria.id;
                    this.categoria.descricao = categoria.descricao;
                    this.categoria.cor = categoria.cor;
                    this.cleanMessages();
                    this.showModal();
                };
                CategoriaMensagemComponent.prototype.onSalvar = function () {
                    var _this = this;
                    _super.prototype.cleanMessages.call(this);
                    if (!this.categoria.descricao) {
                        this.showError('Informar descrição para a categoria');
                        return;
                    }
                    if (!this.categoria.cor) {
                        this.showError('Informar cor para a categoria');
                        return;
                    }
                    this.closeModal();
                    this.service.salvar(this.categoria)
                        .subscribe(function (data) { return _this.afterSave(data); }, function (err) { return _this.onError('Erro ao salvar tipo de mensagem'); });
                };
                CategoriaMensagemComponent = __decorate([
                    core_1.Component({
                        selector: 'categoriaMensagem',
                        providers: [categoriaMensagem_1.CategoriaMensagem, categoriaMensagemService_1.CategoriaMensagemService, tabelaService_1.TabelaService],
                        templateUrl: './src/app/categoriaMensagem/categoriaMensagem-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [categoriaMensagemService_1.CategoriaMensagemService, tabelaService_1.TabelaService, categoriaMensagem_1.CategoriaMensagem])
                ], CategoriaMensagemComponent);
                return CategoriaMensagemComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("CategoriaMensagemComponent", CategoriaMensagemComponent);
        }
    }
});
