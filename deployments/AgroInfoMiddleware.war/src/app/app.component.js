System.register(['angular2/core', 'angular2/common', 'angular2/router', './agendamento/agendamento.component', './integracao/integracao.component', './dashboard/dashboard.component', './mensagem/readMail.component', './cooperado/cooperado.component', './mensagem/mensagem.component', './mensagem/compose.component', './login/login.component', './login/loginService', './grupo/grupo.component', './categoriaMensagem/categoriaMensagem.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, router_1, agendamento_component_1, integracao_component_1, dashboard_component_1, readMail_component_1, cooperado_component_1, mensagem_component_1, compose_component_1, login_component_1, loginService_1, grupo_component_1, categoriaMensagem_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (agendamento_component_1_1) {
                agendamento_component_1 = agendamento_component_1_1;
            },
            function (integracao_component_1_1) {
                integracao_component_1 = integracao_component_1_1;
            },
            function (dashboard_component_1_1) {
                dashboard_component_1 = dashboard_component_1_1;
            },
            function (readMail_component_1_1) {
                readMail_component_1 = readMail_component_1_1;
            },
            function (cooperado_component_1_1) {
                cooperado_component_1 = cooperado_component_1_1;
            },
            function (mensagem_component_1_1) {
                mensagem_component_1 = mensagem_component_1_1;
            },
            function (compose_component_1_1) {
                compose_component_1 = compose_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (loginService_1_1) {
                loginService_1 = loginService_1_1;
            },
            function (grupo_component_1_1) {
                grupo_component_1 = grupo_component_1_1;
            },
            function (categoriaMensagem_component_1_1) {
                categoriaMensagem_component_1 = categoriaMensagem_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(loginService) {
                    this.loginService = loginService;
                }
                AppComponent.prototype.isLogged = function () {
                    return localStorage.getItem('id_token') != null;
                };
                AppComponent.prototype.logout = function () {
                    this.loginService.logout();
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'agroInfo',
                        providers: [loginService_1.LoginService],
                        template: "\n    <div class=\"wrapper\" *ngIf=\"isLogged()\">\n\n      <!-- Main Header -->\n      <header class=\"main-header\">\n        <!-- Logo -->\n        <a href=\"#\" class=\"logo\">\n          <!-- mini logo for sidebar mini 50x50 pixels -->\n          <span class=\"logo-mini\"><b>P</b>A</span>\n          <!-- logo for regular state and mobile devices -->\n          <span class=\"logo-lg\">Portal <b>AgroInfo</b></span>\n        </a>\n\n        <!-- Header Navbar -->\n        <nav class=\"navbar navbar-static-top\" role=\"navigation\">\n          <!-- Sidebar toggle button-->\n          <!--\n          <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">\n            <span class=\"sr-only\">Toggle navigation</span>          \n          </a>\n          -->\n          <!-- Navbar Right Menu -->\n          <div class=\"navbar-custom-menu\">\n            <ul class=\"nav navbar-nav\">\n\n              <!-- Notifications Menu -->\n              <li class=\"dropdown notifications-menu\">\n                <!-- Menu toggle button -->\n                <ul class=\"dropdown-menu\">\n                  <li class=\"header\">You have 10 notifications</li>\n                  <li>\n                    <!-- Inner Menu: contains the notifications -->\n                    <ul class=\"menu\">\n                      <li><!-- start notification -->\n                        <a href=\"#\">\n                          <i class=\"fa fa-users text-aqua\"></i> 5 new members joined today\n                        </a>\n                      </li><!-- end notification -->\n                    </ul>\n                  </li>\n                  <li class=\"footer\"><a href=\"#\">View all</a></li>\n                </ul>\n              </li>\n              <!-- Tasks Menu -->\n              <li class=\"dropdown tasks-menu\">\n              </li>\n              <!-- User Account Menu -->\n              <li class=\"dropdown user user-menu\">\n                <!-- Menu Toggle Button -->\n                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\n                  <!-- hidden-xs hides the username on small devices so only the image appears. -->\n                  <span>Administrador</span>\n                </a>\n                <ul class=\"dropdown-menu\">\n                  <!-- Menu Footer-->\n                  <li class=\"user-footer\">\n                    <div class=\"pull-right\">\n                      <a (click)=\"logout()\" class=\"btn btn-default btn-flat\">Sair</a>\n                    </div>\n                  </li>\n                </ul>\n              </li>\n            </ul>\n          </div>\n        </nav>\n      </header>\n      <!-- Left side column. contains the logo and sidebar -->\n      <aside class=\"main-sidebar\">\n\n        <!-- sidebar: style can be found in sidebar.less -->\n        <section class=\"sidebar\">\n\n        <!-- Sidebar Menu -->\n        <ul class=\"sidebar-menu\">\n            <li class=\"header\">MENU</li>\n            <!-- Optionally, you can add icons to the links -->\n            <li class=\"active\">\n                <a [routerLink]=\"['Inicio']\">\n                    <i class=\"fa fa-dashboard\"></i> <span>Inicio</span>\n                </a>\n            </li>\n            <li>\n                <a [routerLink]=\"['Cooperado']\">\n                    <i class=\"fa fa-user\"></i> <span>Cooperado</span>\n                </a>\n            </li>\n            <li>\n                <a [routerLink]=\"['Grupo']\">\n                    <i class=\"fa fa-users\"></i> <span>Grupo</span>\n                </a>\n            </li>            \n            <li>\n                <a [routerLink]=\"['Mensagem']\">\n                    <i class=\"fa fa-envelope\"></i> <span>Mensagem</span>\n                </a>\n            </li>\n            <li>\n                <a [routerLink]=\"['CategoriaMensagem']\">\n                    <i class=\"fa fa-ticket\"></i> <span>Categoria Mensagem</span>\n                </a>\n            </li>            \n            <li>\n                <a href=\"#\">\n                    <i class=\"fa fa-files-o\"></i> <span>Relat\u00F3rios</span>\n                </a>\n            </li>            \n            <li class=\"treeview\">\n                <a href=\"#\"><i class=\"fa fa-cogs\"></i> <span>Configura\u00E7\u00F5es</span> <i class=\"fa fa-angle-left pull-right\"></i></a>\n                <ul class=\"treeview-menu\">\n                    <li><a [routerLink]=\"['Agendamento']\"><i class=\"fa fa-clock-o\"></i><span>Agendamento</span></a></li>\n                    <li><a [routerLink]=\"['Integracao']\"><i class=\"fa fa-bars\"></i><span>Integra\u00E7\u00E3o</span></a></li>\n                </ul>\n            </li>\n        </ul><!-- /.sidebar-menu -->\n        </section>\n        <!-- /.sidebar -->\n      </aside>\n\n      <!-- Content Wrapper. Contains page content -->\n      <div class=\"content-wrapper\">\n        <!-- Main content -->\n        <section>\n\n          <router-outlet></router-outlet>\n\n        </section><!-- /.content -->\n      </div><!-- /.content-wrapper -->\n\n      <!-- Main Footer -->\n      <footer class=\"main-footer\">\n        <!-- To the right -->\n        <div class=\"pull-right hidden-xs\">\n          AgroInfo\n        </div>\n        <!-- Default to the left -->\n        <strong>Copyright &copy; 2016 <a href=\"http://www.senior.com.br\">Senior Sistemas</a>.</strong> Todos os direitos reservados.\n      </footer>\n\n      <!-- Control Sidebar -->\n      <aside class=\"control-sidebar control-sidebar-dark\">\n        <!-- Create the tabs -->\n        <ul class=\"nav nav-tabs nav-justified control-sidebar-tabs\">\n          <li class=\"active\"><a href=\"#control-sidebar-home-tab\" data-toggle=\"tab\"><i class=\"fa fa-home\"></i></a></li>\n          <li><a href=\"#control-sidebar-settings-tab\" data-toggle=\"tab\"><i class=\"fa fa-gears\"></i></a></li>\n        </ul>\n        <!-- Tab panes -->\n        <div class=\"tab-content\">\n          <!-- Home tab content -->\n          <div class=\"tab-pane active\" id=\"control-sidebar-home-tab\">\n            <h3 class=\"control-sidebar-heading\">Recent Activity</h3>\n            <ul class=\"control-sidebar-menu\">\n              <li>\n                <a href=\"javascript::;\">\n                  <i class=\"menu-icon fa fa-birthday-cake bg-red\"></i>\n                  <div class=\"menu-info\">\n                    <h4 class=\"control-sidebar-subheading\">Langdon's Birthday</h4>\n                    <p>Will be 23 on April 24th</p>\n                  </div>\n                </a>\n              </li>\n            </ul><!-- /.control-sidebar-menu -->\n\n            <h3 class=\"control-sidebar-heading\">Tasks Progress</h3>\n            <ul class=\"control-sidebar-menu\">\n              <li>\n                <a href=\"javascript::;\">\n                  <h4 class=\"control-sidebar-subheading\">\n                    Custom Template Design\n                    <span class=\"label label-danger pull-right\">70%</span>\n                  </h4>\n                  <div class=\"progress progress-xxs\">\n                    <div class=\"progress-bar progress-bar-danger\" style=\"width: 70%\"></div>\n                  </div>\n                </a>\n              </li>\n            </ul><!-- /.control-sidebar-menu -->\n\n          </div><!-- /.tab-pane -->\n          <!-- Stats tab content -->\n          <div class=\"tab-pane\" id=\"control-sidebar-stats-tab\">Stats Tab Content</div><!-- /.tab-pane -->\n          <!-- Settings tab content -->\n          <div class=\"tab-pane\" id=\"control-sidebar-settings-tab\">\n            <form method=\"post\">\n              <h3 class=\"control-sidebar-heading\">General Settings</h3>\n              <div class=\"form-group\">\n                <label class=\"control-sidebar-subheading\">\n                  Report panel usage\n                  <input type=\"checkbox\" class=\"pull-right\" checked>\n                </label>\n                <p>\n                  Some information about this general settings option\n                </p>\n              </div><!-- /.form-group -->\n            </form>\n          </div><!-- /.tab-pane -->\n        </div>\n      </aside><!-- /.control-sidebar -->\n      <!-- Add the sidebar's background. This div must be placed\n    </div><!-- ./wrapper -->\n    </div>\n      <div *ngIf=\"!isLogged()\" class=login-page>\n          <login>loading...</login>\n      </div>    \n  ",
                        directives: [common_1.CORE_DIRECTIVES, router_1.ROUTER_DIRECTIVES, login_component_1.LoginComponent, agendamento_component_1.AgendamentoComponent, categoriaMensagem_component_1.CategoriaMensagemComponent,
                            integracao_component_1.IntegracaoComponent, dashboard_component_1.DashboardComponent, cooperado_component_1.CooperadoComponent, mensagem_component_1.MensagemComponent, grupo_component_1.GrupoComponent]
                    }),
                    router_1.RouteConfig([
                        { path: '/agendamento', component: agendamento_component_1.AgendamentoComponent, name: 'Agendamento' },
                        { path: '/integracao', component: integracao_component_1.IntegracaoComponent, name: 'Integracao' },
                        { path: '/inicio', component: dashboard_component_1.DashboardComponent, name: 'Inicio', useAsDefault: true },
                        { path: '/cooperado', component: cooperado_component_1.CooperadoComponent, name: 'Cooperado' },
                        { path: '/mensagem', component: mensagem_component_1.MensagemComponent, name: 'Mensagem' },
                        { path: '/compose/:id', component: compose_component_1.ComposeComponent, name: 'Compose' },
                        { path: '/read-mail/:id', component: readMail_component_1.ReadMailComponent, name: 'ReadMail' },
                        { path: '/categoriaMensagem', component: categoriaMensagem_component_1.CategoriaMensagemComponent, name: 'CategoriaMensagem' },
                        { path: '/grupo', component: grupo_component_1.GrupoComponent, name: 'Grupo' }
                    ]), 
                    __metadata('design:paramtypes', [loginService_1.LoginService])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
