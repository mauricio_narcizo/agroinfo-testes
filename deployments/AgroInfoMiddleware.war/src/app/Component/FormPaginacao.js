System.register(['./AbstractComponent'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var AbstractComponent_1;
    var FormPaginacao;
    return {
        setters:[
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            }],
        execute: function() {
            FormPaginacao = (function (_super) {
                __extends(FormPaginacao, _super);
                function FormPaginacao(service) {
                    _super.call(this);
                    this.tabelaService = service;
                }
                FormPaginacao.prototype.onAvancar = function () {
                    this.tabelaService.onAvancar();
                    this.getDados();
                };
                FormPaginacao.prototype.onVoltar = function () {
                    this.tabelaService.onVoltar();
                    this.getDados();
                };
                FormPaginacao.prototype.ngOnInit = function () {
                    this.getDados();
                };
                return FormPaginacao;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("FormPaginacao", FormPaginacao);
        }
    }
});
