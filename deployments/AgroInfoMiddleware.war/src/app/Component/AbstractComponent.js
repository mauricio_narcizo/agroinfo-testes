System.register([], function(exports_1) {
    var AbstractComponent;
    return {
        setters:[],
        execute: function() {
            AbstractComponent = (function () {
                function AbstractComponent() {
                }
                AbstractComponent.prototype.onError = function (error) {
                    if (error.status) {
                        if (error.status == 404) {
                            this.showError('Não foi possível comunicar com o servidor do portal ou serviço não existente');
                            return;
                        }
                    }
                    else {
                        this.showError(error);
                    }
                    console.log(error);
                };
                AbstractComponent.prototype.showError = function (message) {
                    this.errorMessage = message;
                };
                AbstractComponent.prototype.showSuccess = function (message) {
                    this.successMessage = message;
                };
                AbstractComponent.prototype.showSuccessMessage = function () {
                    return this.successMessage != null;
                };
                AbstractComponent.prototype.showErrorMessage = function () {
                    return this.errorMessage != null;
                };
                AbstractComponent.prototype.cleanMessages = function () {
                    this.successMessage = null;
                    this.errorMessage = null;
                };
                AbstractComponent.prototype.afterSave = function (data) {
                    if (data._body != null && data._body != "") {
                        if (JSON.parse(data._body).mensagemErro != null) {
                            this.onError(JSON.parse(data._body).mensagemErro);
                            return;
                        }
                    }
                    if (data.status != 200 && data.status != 204 && data.status != 201) {
                        this.onError(JSON.parse(data._body).mensagemErro);
                        return;
                    }
                    this.successMessage = 'Registro salvo.';
                };
                AbstractComponent.prototype.afterDelete = function (data) {
                    if (data._body != null && data._body != "") {
                        if (JSON.parse(data._body).mensagemErro != null) {
                            this.onError(JSON.parse(data._body).mensagemErro);
                            return;
                        }
                    }
                    this.successMessage = 'Registro Excluído.';
                };
                return AbstractComponent;
            })();
            exports_1("AbstractComponent", AbstractComponent);
        }
    }
});
