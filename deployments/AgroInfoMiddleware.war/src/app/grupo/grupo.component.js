System.register(['angular2/core', './grupo', './grupoService', './../dataTable/tabelaService', '../Component/AbstractComponent'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, grupo_1, grupoService_1, tabelaService_1, AbstractComponent_1;
    var GrupoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (grupo_1_1) {
                grupo_1 = grupo_1_1;
            },
            function (grupoService_1_1) {
                grupoService_1 = grupoService_1_1;
            },
            function (tabelaService_1_1) {
                tabelaService_1 = tabelaService_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            }],
        execute: function() {
            GrupoComponent = (function (_super) {
                __extends(GrupoComponent, _super);
                function GrupoComponent(service, tabelaService, grupo) {
                    _super.call(this);
                    this.grupo = grupo;
                    this.isModalOpen = false;
                    this.service = service;
                    this.tabelaService = tabelaService;
                }
                GrupoComponent.prototype.ngOnInit = function () {
                    this._selectMultiple = $(".select2").select2();
                    this.getDados();
                };
                GrupoComponent.prototype.getDados = function () {
                    var _this = this;
                    this.service.getGrupos(this.pesquisa, this.tabelaService.inicioListagem - 1, this.tabelaService.qtdPorPagina)
                        .subscribe(function (res) { _this.tabelaService.preencheLista(res.json()); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                GrupoComponent.prototype.onAvancar = function () {
                    this.tabelaService.onAvancar();
                    this.getDados();
                };
                GrupoComponent.prototype.onVoltar = function () {
                    this.tabelaService.onVoltar();
                    this.getDados();
                };
                GrupoComponent.prototype.onLocalizar = function () {
                    this.tabelaService.resetValues();
                    if (this.pesquisa != null && this.pesquisa.length == 0) {
                        this.pesquisa = null;
                    }
                    this.getDados();
                };
                GrupoComponent.prototype.onNovo = function () {
                    this.grupo = new grupo_1.Grupo();
                    this.showModal();
                };
                GrupoComponent.prototype.onEditar = function (grupo) {
                    this.cleanMessages();
                    this.grupo = new grupo_1.Grupo();
                    this.grupo.id = grupo.id;
                    this.grupo.nome = grupo.nome;
                    this.showModal();
                };
                GrupoComponent.prototype.onExcluir = function ($event, id) {
                    var _this = this;
                    if (confirm("Você realmente deseja excluir o grupo?")) {
                        this.cleanMessages();
                        this.service.delete(id)
                            .subscribe(function (data) { _this.afterDelete(data), _this.getDados(); }, function (error) { return _this.onError(error); });
                    }
                    $event.stopPropagation();
                };
                GrupoComponent.prototype.showErroModal = function () {
                    return this.isModalOpen;
                };
                GrupoComponent.prototype.showModal = function () {
                    this.isModalOpen = true;
                    $('#myModal').modal({ backdrop: 'static', keyboard: false });
                };
                GrupoComponent.prototype.cancel = function () {
                    this.cleanMessages();
                    this.closeModal();
                };
                GrupoComponent.prototype.closeModal = function () {
                    this.isModalOpen = false;
                    $('#myModal').modal('hide');
                };
                GrupoComponent.prototype.afterSave = function (data) {
                    _super.prototype.afterSave.call(this, data);
                    this.getDados();
                    this.closeModal();
                };
                GrupoComponent.prototype.onSalvar = function () {
                    var _this = this;
                    _super.prototype.cleanMessages.call(this);
                    if (!this.grupo.nome) {
                        this.showError('Informar descrição para o grupo');
                        return;
                    }
                    console.log(JSON.stringify(this.grupo));
                    this.service.salvar(this.grupo)
                        .subscribe(function (data) { return _this.afterSave(data); }, function (err) { return _this.onError('Erro ao salvar agendamentos'); });
                };
                GrupoComponent = __decorate([
                    core_1.Component({
                        selector: 'grupo',
                        providers: [grupo_1.Grupo, grupoService_1.GrupoService, tabelaService_1.TabelaService],
                        templateUrl: './src/app/grupo/grupo-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [grupoService_1.GrupoService, tabelaService_1.TabelaService, grupo_1.Grupo])
                ], GrupoComponent);
                return GrupoComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("GrupoComponent", GrupoComponent);
        }
    }
});
