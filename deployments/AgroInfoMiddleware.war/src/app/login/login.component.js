System.register(['angular2/core', './loginService'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, loginService_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (loginService_1_1) {
                loginService_1 = loginService_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                function LoginComponent(service) {
                    this.service = service;
                    this.withErrors = false;
                }
                LoginComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.service.login(this.usuario, this.senha)
                        .subscribe(function (res) { _this.service.salvarToken(res), _this.withErrors = false; }, function (error) { console.log(error), _this.withErrors = true; });
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: 'login',
                        providers: [loginService_1.LoginService],
                        templateUrl: './src/app/login/login-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [loginService_1.LoginService])
                ], LoginComponent);
                return LoginComponent;
            })();
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
