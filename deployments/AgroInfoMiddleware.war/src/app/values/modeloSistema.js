System.register([], function(exports_1) {
    var ModeloSistema;
    return {
        setters:[],
        execute: function() {
            ModeloSistema = (function () {
                function ModeloSistema() {
                    this.modelos = ['Senior ERP 5.8.7', 'Senior ERP 5.8.8'];
                }
                return ModeloSistema;
            })();
            exports_1("ModeloSistema", ModeloSistema);
        }
    }
});
