System.register([], function(exports_1) {
    var FormatoPeriodoIntegracao;
    return {
        setters:[],
        execute: function() {
            FormatoPeriodoIntegracao = (function () {
                function FormatoPeriodoIntegracao() {
                    this.periodo = ['Minutos', 'Horas', 'Dias', 'Semanas'];
                }
                return FormatoPeriodoIntegracao;
            })();
            exports_1("FormatoPeriodoIntegracao", FormatoPeriodoIntegracao);
        }
    }
});
