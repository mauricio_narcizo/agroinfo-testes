System.register([], function(exports_1) {
    var Config;
    return {
        setters:[],
        execute: function() {
            Config = (function () {
                function Config() {
                    this.BASEURI = './api';
                    //public BASEURI = 'http://localhost:9090/AgroInfoMiddleware/api';
                    this.TOKEN = 'id_token';
                }
                return Config;
            })();
            exports_1("Config", Config);
        }
    }
});
