System.register(['angular2/core', './IntegracaoService', 'angular2/router', '../Component/AbstractComponent'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, IntegracaoService_1, router_1, AbstractComponent_1;
    var IntegracaoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (IntegracaoService_1_1) {
                IntegracaoService_1 = IntegracaoService_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            }],
        execute: function() {
            IntegracaoComponent = (function (_super) {
                __extends(IntegracaoComponent, _super);
                function IntegracaoComponent(service) {
                    _super.call(this);
                    this.service = service;
                    this.configuracao = new Object;
                    this.modelosSMS = this.service.modelosSMS;
                }
                IntegracaoComponent.prototype.validar = function () {
                    if (this.configuracao.endereco) {
                        if (!this.configuracao.porta) {
                            this.showError('Informar porta para integração');
                            return false;
                        }
                        if (!this.configuracao.usuario) {
                            this.showError('Informar usuário para integração');
                            return false;
                        }
                        if (!this.configuracao.senha) {
                            this.showError('Informar senha para integração');
                            return false;
                        }
                        if (!this.configuracao.sigla) {
                            this.showError('Informar Sigla para integração');
                        }
                    }
                    if (this.configuracao.ipsms) {
                        if (!this.configuracao.portasms) {
                            this.showError('Informar porta SMS para integração');
                            return false;
                        }
                        if (!this.configuracao.usuariosms) {
                            this.showError('Informar usuário SMS para integração');
                            return false;
                        }
                        if (!this.configuracao.senhasms) {
                            this.showError('Informar senha SMS para integração');
                            return false;
                        }
                    }
                    return true;
                };
                IntegracaoComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.modelosSistema = this.service.modelosSistema;
                    this.service.getConfiguracao()
                        .subscribe(function (res) { _this.loadData(res.json()); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                IntegracaoComponent.prototype.loadData = function (data) {
                    this.configuracao = this.service.resolveTipoSistema(data);
                };
                IntegracaoComponent.prototype.onSubmit = function () {
                    var _this = this;
                    _super.prototype.cleanMessages.call(this);
                    if (this.validar()) {
                        console.log('joao');
                        this.service.salvar(this.configuracao)
                            .subscribe(function (data) { _this.afterSave(data), _this.validar(); }, function (err) { return _this.onError('Erro ao salvar agendamentos'); });
                    }
                };
                IntegracaoComponent = __decorate([
                    core_1.Component({
                        selector: 'integracao',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        templateUrl: './src/app/integracao/integracao-form.component.html',
                        providers: [IntegracaoService_1.IntegracaoService, AbstractComponent_1.AbstractComponent]
                    }), 
                    __metadata('design:paramtypes', [IntegracaoService_1.IntegracaoService])
                ], IntegracaoComponent);
                return IntegracaoComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("IntegracaoComponent", IntegracaoComponent);
        }
    }
});
