System.register(['angular2/core', 'angular2/http', './../values/config', './../values/modeloSistema', './../values/modeloSMS', 'angular2-jwt'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, config_1, modeloSistema_1, modeloSMS_1, angular2_jwt_1;
    var IntegracaoService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (modeloSistema_1_1) {
                modeloSistema_1 = modeloSistema_1_1;
            },
            function (modeloSMS_1_1) {
                modeloSMS_1 = modeloSMS_1_1;
            },
            function (angular2_jwt_1_1) {
                angular2_jwt_1 = angular2_jwt_1_1;
            }],
        execute: function() {
            IntegracaoService = (function () {
                function IntegracaoService(authHttp) {
                    this.authHttp = authHttp;
                    this.modelosSMS = new modeloSMS_1.ModeloSMS().modelos;
                    this.modelosSistema = new modeloSistema_1.ModeloSistema().modelos;
                    this.path = new config_1.Config().BASEURI + '/integracao';
                }
                IntegracaoService.prototype.getConfiguracao = function () {
                    return this.authHttp.get(this.path);
                };
                IntegracaoService.prototype.resolveTipoSistema = function (value) {
                    if (value.tipoSistemaSMS == null) {
                        value.tipoSistemaSMS = this.modelosSMS[0];
                    }
                    if (value.tiposistema == null) {
                        value.tiposistema = this.modelosSistema[0];
                    }
                    return value;
                };
                IntegracaoService.prototype.salvar = function (dados) {
                    var headers = new http_1.Headers();
                    headers.append('Content-Type', 'application/json');
                    return this.authHttp.put(this.path, JSON.stringify(dados), {
                        headers: headers
                    });
                };
                IntegracaoService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [angular2_jwt_1.AuthHttp])
                ], IntegracaoService);
                return IntegracaoService;
            })();
            exports_1("IntegracaoService", IntegracaoService);
        }
    }
});
