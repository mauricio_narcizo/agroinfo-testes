System.register(['angular2/core', 'angular2-jwt'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, angular2_jwt_1;
    var TabelaService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (angular2_jwt_1_1) {
                angular2_jwt_1 = angular2_jwt_1_1;
            }],
        execute: function() {
            TabelaService = (function () {
                function TabelaService(authHttp) {
                    this.authHttp = authHttp;
                    this.qtdPorPagina = 10;
                    this.objetosListagem = [];
                    this.inicioListagem = 1;
                    this.fimListagem = 10;
                    this.total = 0;
                    this.resetValues();
                }
                TabelaService.prototype.resetValues = function () {
                    this.qtdPorPagina = 10;
                    this.inicioListagem = 1;
                    this.total = 0;
                    this.fimListagem = this.qtdPorPagina;
                };
                TabelaService.prototype.preencheLista = function (lista) {
                    this.total = lista.count;
                    this.objetosListagem = lista.itens;
                };
                TabelaService.prototype.setPath = function (path) {
                    this.path = path;
                };
                TabelaService.prototype.onAvancar = function () {
                    this.inicioListagem = this.fimListagem + 1;
                    this.calculaFimListagem();
                };
                TabelaService.prototype.calculaFimListagem = function () {
                    this.fimListagem = this.inicioListagem + this.qtdPorPagina - 1;
                };
                TabelaService.prototype.onVoltar = function () {
                    this.inicioListagem = this.inicioListagem - this.qtdPorPagina;
                    this.calculaFimListagem();
                };
                TabelaService.prototype.disableBack = function () {
                    return this.inicioListagem <= 1;
                };
                TabelaService.prototype.disableNext = function () {
                    return this.fimListagem > this.total;
                };
                TabelaService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [angular2_jwt_1.AuthHttp])
                ], TabelaService);
                return TabelaService;
            })();
            exports_1("TabelaService", TabelaService);
        }
    }
});
