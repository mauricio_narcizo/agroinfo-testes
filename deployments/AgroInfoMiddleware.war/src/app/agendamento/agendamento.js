System.register([], function(exports_1) {
    var Agendamento;
    return {
        setters:[],
        execute: function() {
            Agendamento = (function () {
                function Agendamento(id, nome, descricao, periodicidade, tempoUltimaExecucao, cargaCompleta, ativo, executando, grupo, dtUpdate, formatoPeriodicidade, formatoCargaCompleta, tempoCargaCompleta) {
                    this.id = id;
                    this.nome = nome;
                    this.descricao = descricao;
                    this.periodicidade = periodicidade;
                    this.tempoUltimaExecucao = tempoUltimaExecucao;
                    this.cargaCompleta = cargaCompleta;
                    this.ativo = ativo;
                    this.executando = executando;
                    this.grupo = grupo;
                    this.dtUpdate = dtUpdate;
                    this.formatoPeriodicidade = formatoPeriodicidade;
                    this.formatoCargaCompleta = formatoCargaCompleta;
                    this.tempoCargaCompleta = tempoCargaCompleta;
                }
                return Agendamento;
            })();
            exports_1("Agendamento", Agendamento);
        }
    }
});
