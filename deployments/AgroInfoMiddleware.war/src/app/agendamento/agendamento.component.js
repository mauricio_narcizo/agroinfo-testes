System.register(['angular2/core', './agendamentoService', 'angular2/http', './../values/config', 'angular2-jwt', '../Component/AbstractComponent'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, agendamentoService_1, http_1, config_1, angular2_jwt_1, AbstractComponent_1;
    var AgendamentoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (agendamentoService_1_1) {
                agendamentoService_1 = agendamentoService_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (angular2_jwt_1_1) {
                angular2_jwt_1 = angular2_jwt_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            }],
        execute: function() {
            AgendamentoComponent = (function (_super) {
                __extends(AgendamentoComponent, _super);
                function AgendamentoComponent(service, authHttp) {
                    _super.call(this);
                    this.authHttp = authHttp;
                    this.path = new config_1.Config().BASEURI + '/agendamento';
                    this.service = service;
                }
                AgendamentoComponent.prototype.buscaAgendamentos = function () {
                    var _this = this;
                    this.authHttp.get(this.path)
                        .subscribe(function (res) { _this.agendamentos = res.json(); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                AgendamentoComponent.prototype.ngOnInit = function () {
                    this.periodo = this.service.periodos;
                    this.buscaAgendamentos();
                };
                AgendamentoComponent.prototype.validar = function () {
                    this.cleanMessages();
                    if (this.agendamentos) {
                        for (var _i = 0, _a = this.agendamentos; _i < _a.length; _i++) {
                            var obj = _a[_i];
                            if (obj.ativo) {
                                if (obj.periodicidade <= 0) {
                                    this.showError('Para ativar agendamento ' + obj.descricao + ' informe Periodicidade');
                                    return false;
                                }
                                if (obj.cargaCompleta <= 0) {
                                    this.showError('Para ativar agendamento ' + obj.descricao + ' informe Carga completa');
                                    return false;
                                }
                            }
                        }
                    }
                    return true;
                };
                AgendamentoComponent.prototype.onSubmit = function () {
                    var _this = this;
                    if (this.validar()) {
                        var headers = new http_1.Headers();
                        headers.append('Content-Type', 'application/json');
                        this.authHttp.put(this.path, JSON.stringify(this.agendamentos), {
                            headers: headers
                        }).subscribe(function (data) { return _this.afterSave(data); }, function (err) { return _this.onError('erro'); });
                    }
                };
                AgendamentoComponent = __decorate([
                    core_1.Component({
                        selector: 'agendamento',
                        providers: [agendamentoService_1.AgendamentoService, AbstractComponent_1.AbstractComponent],
                        templateUrl: './src/app/agendamento/agendamento-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [agendamentoService_1.AgendamentoService, angular2_jwt_1.AuthHttp])
                ], AgendamentoComponent);
                return AgendamentoComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("AgendamentoComponent", AgendamentoComponent);
        }
    }
});
