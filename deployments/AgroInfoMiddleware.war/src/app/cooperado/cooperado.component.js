System.register(['angular2/core', './CooperadoService', './../dataTable/tabelaService', '../grupo/grupoService', '../grupo/grupo', '../Component/AbstractComponent', './cooperado'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, CooperadoService_1, tabelaService_1, grupoService_1, grupo_1, AbstractComponent_1, cooperado_1;
    var CooperadoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (CooperadoService_1_1) {
                CooperadoService_1 = CooperadoService_1_1;
            },
            function (tabelaService_1_1) {
                tabelaService_1 = tabelaService_1_1;
            },
            function (grupoService_1_1) {
                grupoService_1 = grupoService_1_1;
            },
            function (grupo_1_1) {
                grupo_1 = grupo_1_1;
            },
            function (AbstractComponent_1_1) {
                AbstractComponent_1 = AbstractComponent_1_1;
            },
            function (cooperado_1_1) {
                cooperado_1 = cooperado_1_1;
            }],
        execute: function() {
            CooperadoComponent = (function (_super) {
                __extends(CooperadoComponent, _super);
                function CooperadoComponent(service, tabelaService, grupoService, cooperado) {
                    _super.call(this);
                    this.service = service;
                    this.tabelaService = tabelaService;
                    this.grupoService = grupoService;
                    this.cooperado = cooperado;
                    this.grupos = new Array();
                }
                CooperadoComponent.prototype.ngOnInit = function () {
                    this._selectMultiple = $(".select2").select2();
                    this.getDados();
                    this.getGrupos();
                };
                CooperadoComponent.prototype.getGrupos = function () {
                    var _this = this;
                    this.grupoService.getTodosGrupos()
                        .subscribe(function (res) { return _this.afterGetGrupos(res); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                CooperadoComponent.prototype.afterGetGrupos = function (json) {
                    this.grupos = json.json();
                    this.initSelect2();
                };
                CooperadoComponent.prototype.getDados = function () {
                    var _this = this;
                    this.service.getCooperados(this.pesquisa, this.tabelaService.inicioListagem - 1, this.tabelaService.qtdPorPagina)
                        .subscribe(function (res) { _this.tabelaService.preencheLista(res.json()); }, function (error) { return _this.onError('Houve um erro ao conectar com o servidor'); });
                };
                CooperadoComponent.prototype.onAvancar = function () {
                    this.tabelaService.onAvancar();
                    this.getDados();
                };
                CooperadoComponent.prototype.onVoltar = function () {
                    this.tabelaService.onVoltar();
                    this.getDados();
                };
                CooperadoComponent.prototype.onLocalizar = function () {
                    this.tabelaService.resetValues();
                    if (this.pesquisa != null && this.pesquisa.length == 0) {
                        this.pesquisa = null;
                    }
                    this.getDados();
                };
                CooperadoComponent.prototype.recuperaGruposDoCooperado = function (cooperado) {
                    var _this = this;
                    this.grupoService.getGruposPorCooperado(cooperado.id)
                        .subscribe(function (res) { return _this.afterRecuperarGruposUsuario(res.json()); }, function (error) { return _this.onError("Houve um erro ao conectar com o servidor"); });
                };
                CooperadoComponent.prototype.afterRecuperarGruposUsuario = function (json) {
                    this.cleanSelect2();
                    this.initSelect2();
                    for (var _i = 0; _i < json.length; _i++) {
                        var key = json[_i];
                        for (var _a = 0, _b = this._selectMultiple.children(); _a < _b.length; _a++) {
                            var obj = _b[_a];
                            if (obj.value == key.id) {
                                obj.selected = true;
                            }
                        }
                        this._selectMultiple.trigger('change');
                    }
                };
                CooperadoComponent.prototype.onSelect = function (cooperado) {
                    this.recuperaGruposDoCooperado(cooperado);
                    this.cooperado = cooperado;
                    $('#myModal').modal({ backdrop: 'static', keyboard: false });
                };
                CooperadoComponent.prototype.cleanSelect2 = function () {
                    $('.select2').empty().trigger('change');
                };
                CooperadoComponent.prototype.initSelect2 = function () {
                    this._selectMultiple = $(".select2").select2();
                    $(".select2").select2({
                        data: this.grupos
                    });
                };
                CooperadoComponent.prototype.closeModal = function () {
                    $('#myModal').modal('hide');
                };
                CooperadoComponent.prototype.onAtivar = function (cooperado) {
                    cooperado.ativo = !cooperado.ativo;
                    this.salvar(cooperado, 'situacao');
                };
                CooperadoComponent.prototype.salvar = function (cooperado, caminho) {
                    var _this = this;
                    _super.prototype.cleanMessages.call(this);
                    this.service.salvar(cooperado, caminho)
                        .subscribe(function (data) { return _this.afterSave(data); }, function (err) { _this.onError('Erro ao salvar agendamentos'), _this.getDados(); });
                };
                CooperadoComponent.prototype.onSalvarGrupo = function () {
                    var grupos = new Array();
                    for (var _i = 0, _a = this._selectMultiple.select2('data'); _i < _a.length; _i++) {
                        var key = _a[_i];
                        var grupo = { id: key.id, nome: key.nome };
                        grupos.push(grupo);
                    }
                    this.cooperado.grupos = grupos;
                    this.salvar(this.cooperado, 'grupo');
                    this.closeModal();
                };
                CooperadoComponent = __decorate([
                    core_1.Component({
                        selector: 'cooperado',
                        providers: [CooperadoService_1.CooperadoService, tabelaService_1.TabelaService, grupoService_1.GrupoService, cooperado_1.Cooperado, grupo_1.Grupo],
                        templateUrl: './src/app/cooperado/cooperado-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [CooperadoService_1.CooperadoService, tabelaService_1.TabelaService, grupoService_1.GrupoService, cooperado_1.Cooperado])
                ], CooperadoComponent);
                return CooperadoComponent;
            })(AbstractComponent_1.AbstractComponent);
            exports_1("CooperadoComponent", CooperadoComponent);
        }
    }
});
